 

// Core library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtImplInclude.h


#include "Dc1elementType.h"
#include "Dc1elementsGroup_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IDc1elementsGroup_LocalType::IDc1elementsGroup_LocalType()
{

// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtPropInit.h

}

IDc1elementsGroup_LocalType::~IDc1elementsGroup_LocalType()
{
// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtPropCleanup.h

}

Dc1elementsGroup_LocalType::Dc1elementsGroup_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Dc1elementsGroup_LocalType::~Dc1elementsGroup_LocalType()
{
	Cleanup();
}

void Dc1elementsGroup_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_title = Dc1elementPtr(); // Class
	m_creator = Dc1elementPtr(); // Class
	m_subject = Dc1elementPtr(); // Class
	m_description = Dc1elementPtr(); // Class
	m_publisher = Dc1elementPtr(); // Class
	m_contributor = Dc1elementPtr(); // Class
	m_date = Dc1elementPtr(); // Class
	m_type = Dc1elementPtr(); // Class
	m_format = Dc1elementPtr(); // Class
	m_identifier = Dc1elementPtr(); // Class
	m_source = Dc1elementPtr(); // Class
	m_language = Dc1elementPtr(); // Class
	m_relation = Dc1elementPtr(); // Class
	m_coverage = Dc1elementPtr(); // Class
	m_rights = Dc1elementPtr(); // Class


// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMyPropInit.h

}

void Dc1elementsGroup_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_title);
	// Dc1Factory::DeleteObject(m_creator);
	// Dc1Factory::DeleteObject(m_subject);
	// Dc1Factory::DeleteObject(m_description);
	// Dc1Factory::DeleteObject(m_publisher);
	// Dc1Factory::DeleteObject(m_contributor);
	// Dc1Factory::DeleteObject(m_date);
	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_format);
	// Dc1Factory::DeleteObject(m_identifier);
	// Dc1Factory::DeleteObject(m_source);
	// Dc1Factory::DeleteObject(m_language);
	// Dc1Factory::DeleteObject(m_relation);
	// Dc1Factory::DeleteObject(m_coverage);
	// Dc1Factory::DeleteObject(m_rights);
}

void Dc1elementsGroup_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use elementsGroup_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IelementsGroup_LocalType
	const Dc1Ptr< Dc1elementsGroup_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_title);
		this->Settitle(Dc1Factory::CloneObject(tmp->Gettitle()));
		// Dc1Factory::DeleteObject(m_creator);
		this->Setcreator(Dc1Factory::CloneObject(tmp->Getcreator()));
		// Dc1Factory::DeleteObject(m_subject);
		this->Setsubject(Dc1Factory::CloneObject(tmp->Getsubject()));
		// Dc1Factory::DeleteObject(m_description);
		this->Setdescription(Dc1Factory::CloneObject(tmp->Getdescription()));
		// Dc1Factory::DeleteObject(m_publisher);
		this->Setpublisher(Dc1Factory::CloneObject(tmp->Getpublisher()));
		// Dc1Factory::DeleteObject(m_contributor);
		this->Setcontributor(Dc1Factory::CloneObject(tmp->Getcontributor()));
		// Dc1Factory::DeleteObject(m_date);
		this->Setdate(Dc1Factory::CloneObject(tmp->Getdate()));
		// Dc1Factory::DeleteObject(m_type);
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
		// Dc1Factory::DeleteObject(m_format);
		this->Setformat(Dc1Factory::CloneObject(tmp->Getformat()));
		// Dc1Factory::DeleteObject(m_identifier);
		this->Setidentifier(Dc1Factory::CloneObject(tmp->Getidentifier()));
		// Dc1Factory::DeleteObject(m_source);
		this->Setsource(Dc1Factory::CloneObject(tmp->Getsource()));
		// Dc1Factory::DeleteObject(m_language);
		this->Setlanguage(Dc1Factory::CloneObject(tmp->Getlanguage()));
		// Dc1Factory::DeleteObject(m_relation);
		this->Setrelation(Dc1Factory::CloneObject(tmp->Getrelation()));
		// Dc1Factory::DeleteObject(m_coverage);
		this->Setcoverage(Dc1Factory::CloneObject(tmp->Getcoverage()));
		// Dc1Factory::DeleteObject(m_rights);
		this->Setrights(Dc1Factory::CloneObject(tmp->Getrights()));
}

Dc1NodePtr Dc1elementsGroup_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Dc1elementsGroup_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1elementPtr Dc1elementsGroup_LocalType::Gettitle() const
{
		return m_title;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getcreator() const
{
		return m_creator;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getsubject() const
{
		return m_subject;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getdescription() const
{
		return m_description;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getpublisher() const
{
		return m_publisher;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getcontributor() const
{
		return m_contributor;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getdate() const
{
		return m_date;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Gettype() const
{
		return m_type;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getformat() const
{
		return m_format;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getidentifier() const
{
		return m_identifier;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getsource() const
{
		return m_source;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getlanguage() const
{
		return m_language;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getrelation() const
{
		return m_relation;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getcoverage() const
{
		return m_coverage;
}

Dc1elementPtr Dc1elementsGroup_LocalType::Getrights() const
{
		return m_rights;
}

// implementing setter for choice 
/* element */
void Dc1elementsGroup_LocalType::Settitle(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Settitle().");
	}
	if (m_title != item)
	{
		// Dc1Factory::DeleteObject(m_title);
		m_title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_title) m_title->SetParent(m_myself.getPointer());
		if (m_title && m_title->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_title->UseTypeAttribute = true;
		}
		if(m_title != Dc1elementPtr())
		{
			m_title->SetContentName(XMLString::transcode("title"));
		}
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setcreator(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setcreator().");
	}
	if (m_creator != item)
	{
		// Dc1Factory::DeleteObject(m_creator);
		m_creator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_creator) m_creator->SetParent(m_myself.getPointer());
		if (m_creator && m_creator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_creator->UseTypeAttribute = true;
		}
		if(m_creator != Dc1elementPtr())
		{
			m_creator->SetContentName(XMLString::transcode("creator"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setsubject(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setsubject().");
	}
	if (m_subject != item)
	{
		// Dc1Factory::DeleteObject(m_subject);
		m_subject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_subject) m_subject->SetParent(m_myself.getPointer());
		if (m_subject && m_subject->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_subject->UseTypeAttribute = true;
		}
		if(m_subject != Dc1elementPtr())
		{
			m_subject->SetContentName(XMLString::transcode("subject"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setdescription(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setdescription().");
	}
	if (m_description != item)
	{
		// Dc1Factory::DeleteObject(m_description);
		m_description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_description) m_description->SetParent(m_myself.getPointer());
		if (m_description && m_description->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_description->UseTypeAttribute = true;
		}
		if(m_description != Dc1elementPtr())
		{
			m_description->SetContentName(XMLString::transcode("description"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setpublisher(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setpublisher().");
	}
	if (m_publisher != item)
	{
		// Dc1Factory::DeleteObject(m_publisher);
		m_publisher = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_publisher) m_publisher->SetParent(m_myself.getPointer());
		if (m_publisher && m_publisher->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_publisher->UseTypeAttribute = true;
		}
		if(m_publisher != Dc1elementPtr())
		{
			m_publisher->SetContentName(XMLString::transcode("publisher"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setcontributor(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setcontributor().");
	}
	if (m_contributor != item)
	{
		// Dc1Factory::DeleteObject(m_contributor);
		m_contributor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_contributor) m_contributor->SetParent(m_myself.getPointer());
		if (m_contributor && m_contributor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_contributor->UseTypeAttribute = true;
		}
		if(m_contributor != Dc1elementPtr())
		{
			m_contributor->SetContentName(XMLString::transcode("contributor"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setdate(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setdate().");
	}
	if (m_date != item)
	{
		// Dc1Factory::DeleteObject(m_date);
		m_date = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_date) m_date->SetParent(m_myself.getPointer());
		if (m_date && m_date->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_date->UseTypeAttribute = true;
		}
		if(m_date != Dc1elementPtr())
		{
			m_date->SetContentName(XMLString::transcode("date"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Settype(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Settype().");
	}
	if (m_type != item)
	{
		// Dc1Factory::DeleteObject(m_type);
		m_type = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_type) m_type->SetParent(m_myself.getPointer());
		if (m_type && m_type->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_type->UseTypeAttribute = true;
		}
		if(m_type != Dc1elementPtr())
		{
			m_type->SetContentName(XMLString::transcode("type"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setformat(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setformat().");
	}
	if (m_format != item)
	{
		// Dc1Factory::DeleteObject(m_format);
		m_format = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_format) m_format->SetParent(m_myself.getPointer());
		if (m_format && m_format->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_format->UseTypeAttribute = true;
		}
		if(m_format != Dc1elementPtr())
		{
			m_format->SetContentName(XMLString::transcode("format"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setidentifier(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setidentifier().");
	}
	if (m_identifier != item)
	{
		// Dc1Factory::DeleteObject(m_identifier);
		m_identifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_identifier) m_identifier->SetParent(m_myself.getPointer());
		if (m_identifier && m_identifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_identifier->UseTypeAttribute = true;
		}
		if(m_identifier != Dc1elementPtr())
		{
			m_identifier->SetContentName(XMLString::transcode("identifier"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setsource(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setsource().");
	}
	if (m_source != item)
	{
		// Dc1Factory::DeleteObject(m_source);
		m_source = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_source) m_source->SetParent(m_myself.getPointer());
		if (m_source && m_source->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_source->UseTypeAttribute = true;
		}
		if(m_source != Dc1elementPtr())
		{
			m_source->SetContentName(XMLString::transcode("source"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setlanguage(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setlanguage().");
	}
	if (m_language != item)
	{
		// Dc1Factory::DeleteObject(m_language);
		m_language = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_language) m_language->SetParent(m_myself.getPointer());
		if (m_language && m_language->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_language->UseTypeAttribute = true;
		}
		if(m_language != Dc1elementPtr())
		{
			m_language->SetContentName(XMLString::transcode("language"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setrelation(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setrelation().");
	}
	if (m_relation != item)
	{
		// Dc1Factory::DeleteObject(m_relation);
		m_relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relation) m_relation->SetParent(m_myself.getPointer());
		if (m_relation && m_relation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_relation->UseTypeAttribute = true;
		}
		if(m_relation != Dc1elementPtr())
		{
			m_relation->SetContentName(XMLString::transcode("relation"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setcoverage(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setcoverage().");
	}
	if (m_coverage != item)
	{
		// Dc1Factory::DeleteObject(m_coverage);
		m_coverage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_coverage) m_coverage->SetParent(m_myself.getPointer());
		if (m_coverage && m_coverage->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_coverage->UseTypeAttribute = true;
		}
		if(m_coverage != Dc1elementPtr())
		{
			m_coverage->SetContentName(XMLString::transcode("coverage"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_rights);
	m_rights = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Dc1elementsGroup_LocalType::Setrights(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup_LocalType::Setrights().");
	}
	if (m_rights != item)
	{
		// Dc1Factory::DeleteObject(m_rights);
		m_rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rights) m_rights->SetParent(m_myself.getPointer());
		if (m_rights && m_rights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_rights->UseTypeAttribute = true;
		}
		if(m_rights != Dc1elementPtr())
		{
			m_rights->SetContentName(XMLString::transcode("rights"));
		}
	// Dc1Factory::DeleteObject(m_title);
	m_title = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_creator);
	m_creator = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_subject);
	m_subject = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_description);
	m_description = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_publisher);
	m_publisher = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_contributor);
	m_contributor = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_date);
	m_date = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_type);
	m_type = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_format);
	m_format = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_identifier);
	m_identifier = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_source);
	m_source = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_language);
	m_language = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_relation);
	m_relation = Dc1elementPtr();
	// Dc1Factory::DeleteObject(m_coverage);
	m_coverage = Dc1elementPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: elementsGroup_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Dc1elementsGroup_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Dc1elementsGroup_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Dc1elementsGroup_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Dc1elementsGroup_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_title != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_title->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("title"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_title->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_creator != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_creator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("creator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_creator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_subject != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_subject->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("subject"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_subject->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_description != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_description->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("description"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_description->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_publisher != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_publisher->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("publisher"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_publisher->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_contributor != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_contributor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("contributor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_contributor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_date != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_date->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("date"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_date->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_type != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_type->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("type"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_type->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_format != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_format->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("format"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_format->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_identifier != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_identifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("identifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_identifier->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_source != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_source->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("source"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_source->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_language != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_language->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("language"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_language->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_relation != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_relation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("relation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_relation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_coverage != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_coverage->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("coverage"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_coverage->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_rights != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_rights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("rights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_rights->Serialize(doc, element, &elem);
	}

}

bool Dc1elementsGroup_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("title"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("title")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Settitle(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("creator"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("creator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcreator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("subject"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("subject")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setsubject(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("description"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("description")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setdescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("publisher"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("publisher")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setpublisher(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("contributor"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("contributor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcontributor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("date"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("date")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setdate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("type"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("type")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Settype(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("format"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("format")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setformat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("identifier"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("identifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setidentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("source"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("source")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setsource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("language"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("language")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setlanguage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("relation"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("relation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setrelation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("coverage"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("coverage")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setcoverage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("rights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setrights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Dc1elementsGroup_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("title")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Settitle(child);
  }
  if (XMLString::compareString(elementname, X("creator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setcreator(child);
  }
  if (XMLString::compareString(elementname, X("subject")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setsubject(child);
  }
  if (XMLString::compareString(elementname, X("description")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setdescription(child);
  }
  if (XMLString::compareString(elementname, X("publisher")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setpublisher(child);
  }
  if (XMLString::compareString(elementname, X("contributor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setcontributor(child);
  }
  if (XMLString::compareString(elementname, X("date")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setdate(child);
  }
  if (XMLString::compareString(elementname, X("type")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Settype(child);
  }
  if (XMLString::compareString(elementname, X("format")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setformat(child);
  }
  if (XMLString::compareString(elementname, X("identifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setidentifier(child);
  }
  if (XMLString::compareString(elementname, X("source")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setsource(child);
  }
  if (XMLString::compareString(elementname, X("language")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setlanguage(child);
  }
  if (XMLString::compareString(elementname, X("relation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setrelation(child);
  }
  if (XMLString::compareString(elementname, X("coverage")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setcoverage(child);
  }
  if (XMLString::compareString(elementname, X("rights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setrights(child);
  }
  return child;
 
}

Dc1NodeEnum Dc1elementsGroup_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Gettitle() != Dc1NodePtr())
		result.Insert(Gettitle());
	if (Getcreator() != Dc1NodePtr())
		result.Insert(Getcreator());
	if (Getsubject() != Dc1NodePtr())
		result.Insert(Getsubject());
	if (Getdescription() != Dc1NodePtr())
		result.Insert(Getdescription());
	if (Getpublisher() != Dc1NodePtr())
		result.Insert(Getpublisher());
	if (Getcontributor() != Dc1NodePtr())
		result.Insert(Getcontributor());
	if (Getdate() != Dc1NodePtr())
		result.Insert(Getdate());
	if (Gettype() != Dc1NodePtr())
		result.Insert(Gettype());
	if (Getformat() != Dc1NodePtr())
		result.Insert(Getformat());
	if (Getidentifier() != Dc1NodePtr())
		result.Insert(Getidentifier());
	if (Getsource() != Dc1NodePtr())
		result.Insert(Getsource());
	if (Getlanguage() != Dc1NodePtr())
		result.Insert(Getlanguage());
	if (Getrelation() != Dc1NodePtr())
		result.Insert(Getrelation());
	if (Getcoverage() != Dc1NodePtr())
		result.Insert(Getcoverage());
	if (Getrights() != Dc1NodePtr())
		result.Insert(Getrights());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Dc1elementsGroup_LocalType_ExtMethodImpl.h


