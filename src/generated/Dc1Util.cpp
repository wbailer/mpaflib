
// Based on Util_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include "Dc1Util.h"
#include <string.h> // memset
#include <string> // FTT TODO Get rid of STL string!!!
#include <sstream> // FTT TODO Get rid of STL string stream!!!

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/dom/DOMDocument.hpp>

XMLCh * Dc1Util::allocate(long length)
{
	XMLCh *result = (XMLCh*)NULL;
	if(length > 0)
	{
		char * buf = new char[length];
		memset(buf, ' ' , length);
		buf[length -1] = '\0';
		// Now Xerces can use its heap
		result = XMLString::transcode(buf);
		delete [] buf;
	}
	return result;
}

void Dc1Util::deallocate(XMLCh** src)
{
	// Delete Xerces allocated heap
	XMLString::release(src);	
}

XMLCh * Dc1Util::CatString(const XMLCh * const target, const XMLCh * const src)
{
	XMLCh * buf = (XMLCh*)NULL;
	if(target && src)
	{
		unsigned int len = (unsigned int)(XMLString::stringLen(target) + XMLString::stringLen(src));
		// We call XMLString::release in other places, so we must get
		// the Xerces DLL to allocate the string from its own heap.
		buf = allocate(len + 1);
		XMLString::copyNString(buf, target, len);
		XMLString::catString(buf, src);
	} else {
		// If only one non-empty string supplied, return a copy.
		const XMLCh *str = target;
		if (!str) str = src;
		if (str) {
			unsigned int len = (unsigned int) XMLString::stringLen(str);
			buf = allocate(len + 1);
			
			// FTT shouldnt it be len + 1 to catch the terminating '\0' as well?
			// FTT This only seems to work by accident, since allocate is setting the last char to '\0'
			XMLString::copyNString(buf, str, len); 
		}
	}
	return buf;
}

void Dc1Util::CatString(XMLCh ** target, const XMLCh * const src)
{
	if(target != NULL) {
		XMLCh * buf = CatString(*target, src);
		XMLString::release(target);
		(*target) = buf;
	}
}

XMLCh* Dc1Util::ConvertToXMLCh(XMLSSize_t src)
{
  std::ostringstream oss;
  oss << src;
  return XMLString::transcode( oss.str().c_str() );
}

#if XERCES_VERSION_MAJOR < 3
#else
XMLCh* Dc1Util::ConvertToXMLCh(XMLFileLoc src)
{
  std::ostringstream oss;
  oss << src;
  return XMLString::transcode( oss.str().c_str() );
}
#endif // XERCES_VERSION_MAJOR < 3	


// fill must contain ONE XMLCh
XMLCh * Dc1Util::PadLeft(const XMLCh * const target, const XMLCh * const fill, const unsigned int maxlen)
{
	XMLCh * buf = (XMLCh*)NULL;
	if(target && fill && XMLString::stringLen(fill) == 1)
	{
		if(XMLString::stringLen(target) < maxlen)
		{
			unsigned int len = (unsigned int)(maxlen - XMLString::stringLen(target));
			buf = allocate(maxlen + 1);
			*buf = chNull;
			while(len-- > 0)
			{
				XMLString::catString(buf, fill);
			}
			XMLString::catString(buf, target);
		}
		else
		{
			buf = XMLString::replicate(target);
		}
	}
	return buf;
}

// fill must contain ONE XMLCh
XMLCh * Dc1Util::PadRight(const XMLCh * const target, const XMLCh * const fill, const unsigned int maxlen)
{
	XMLCh * buf = (XMLCh*)NULL;
	if(target && fill && XMLString::stringLen(fill) == 1)
	{
		if(XMLString::stringLen(target) < maxlen)
		{
			// unsigned int len = maxlen - XMLString::stringLen(target);
			buf = allocate(maxlen + 1);
			XMLString::copyNString(buf, target, maxlen);
			while(XMLString::stringLen(buf) < maxlen)
			{
				XMLString::catString(buf, fill);
			}
		}
		else
		{
			buf = XMLString::replicate(target);
		}
	}
	return buf;
}

DOMElement * Dc1Util::GetNextAkin(DOMElement * currentnode)
{
	DOMNode * node  = currentnode;	
	while(node != NULL)
	{
		node = node->getNextSibling();
		if(node && node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			return (DOMElement*) node;
		}
		else
		{
			// TODO: Check what we are throwing away...
		}
	}
	return (DOMElement*) NULL;
}

DOMElement * Dc1Util::GetNextChild(DOMElement * currentnode)
{
	if(currentnode == NULL)
	{
		return NULL;
	}

	DOMNode * node = currentnode->getFirstChild();
	while(node != NULL)
	{
		if(node && node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			return (DOMElement*) node;
		}
		node = node->getNextSibling();
	}
	return (DOMElement*) NULL;
}

// The Xerces text of an element is stored within a childnode!!!
// and must be trimmed before using it
XMLCh * Dc1Util::GetElementText(DOMElement * currentnode)
{
	if(currentnode == NULL)
	{
		return NULL;
	}

	DOMNode * node = currentnode->getFirstChild();
	if(node != NULL)
	{
		if(node->getNodeType() == DOMNode::TEXT_NODE)
		{
			XMLCh * txt = XMLString::replicate(node->getNodeValue());
			XMLString::trim(txt);
			if(XMLString::stringLen(txt) > 0)
			{
				return txt;
			}
			XMLString::release(&txt);
		}
	}
	return (XMLCh*)NULL;
}


int Dc1Util::GetNextEndPos(Match & m, int i)
{
	int p1 = m.getStartPos(i);
	int p2 = m.getEndPos(i);

	if(i < m.getNoGroups() - 1)
	{
		for(int j = i + 1; j < m.getNoGroups() /* - 1 FTT Fixed missing the last existing group */; ++j)
		{
			if(m.getStartPos(j) > p1)
			{
				return m.getStartPos(j);
			}
		}
	}
	return p2;

}

const XMLCh * Dc1Util::LookupNamespacePrefix(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, const XMLCh * ns)
{
	if(doc) {
		DOMElement * root = doc->getDocumentElement();
		if(root) {
#			if XERCES_VERSION_MAJOR < 3
			return root->lookupNamespacePrefix(ns, false);
#			else
			return root->lookupPrefix(ns);
#			endif
    }
	}
	return 0;
}

const XMLCh * Dc1Util::LookupNamespaceUri(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, const XMLCh * prefix)
{
	if(doc) {
		DOMElement * root = doc->getDocumentElement();
		if(root) {
		  if(!prefix || XMLString::stringLen(prefix) == 0) // Get default namespace if defined
	  		return root->getAttributeNS(X("http://www.w3.org/2000/xmlns/"), X("xmlns"));
	  	else
  			return root->getAttributeNS(X("http://www.w3.org/2000/xmlns/"), prefix);
		}
	}
	return 0;
}


bool Dc1Util::HandleXsiType(DOMElement * element
	, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc
	, const bool usexsitype	
	, const XMLCh * ns
	, const XMLCh * elementtype)
{
	// For now, we assume prefix for is "xsi"
	// In case xsi prefix can not be assumed...
	// const XMLCh * xsi = Dc1Util::LookupNamespacePrefix(doc, X("http://www.w3.org/2001/XMLSchema-instance"));
	// XMLCh * xsiattr = Dc1Util::CatString(xsi, X(":type"));

	if(usexsitype && ! element->hasAttributeNS(X("http://www.w3.org/2001/XMLSchema-instance"), X("type"))) // xsi:type
	{
		const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, ns);
		if(prefix) {
			XMLCh * xsitype = Dc1Util::CatString(prefix, X(":"));
			Dc1Util::CatString(&xsitype, elementtype);
			element->setAttributeNS(X("http://www.w3.org/2001/XMLSchema-instance"), X("xsi:type"), xsitype);
			Dc1Util::deallocate(&xsitype);
		}
		else {
			element->setAttributeNS(X("http://www.w3.org/2001/XMLSchema-instance"), X("xsi:type"), elementtype);
		}
		return true;
		return true;
	}
	return false;
}

bool Dc1Util::SerializeTextNode(DOMElement * element, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc
, const XMLCh * data, const XMLCh * ns, const XMLCh * name, bool isoptional /* = false */)
{
	if(isoptional && data == 0) return false;
	
	// Here we assume, that an optional textnode is already handled and hence the function is not called if data is null
	DOMElement * elem = doc->createElementNS(ns, name);
 	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, ns);
	elem->setPrefix(prefix);
	
	DOMText * text = doc->createTextNode(data ? data : X(""));
	elem->appendChild((DOMNode*)text);
	element->appendChild(elem);
	return true;
}

/* Do not deallocate the result */
const XMLCh * Dc1Util::LocalName(const XMLCh * name)
{
	const XMLCh * result = XMLString::findAny(name, X(":"));
	if(result) {
		return ++result;
	}
	return name;
}

/* Deallocate the result */
XMLCh * Dc1Util::Prefix(const XMLCh * name)
{
  XMLCh * prefix = 0;
	if(name) {
		int i = XMLString::lastIndexOf(name, chColon); // From XMLUniDefs.hpp
		if(i > 0) {
			prefix = Dc1Util::allocate(i + 1);
			XMLString::subString(prefix, name, 0, i);
		}
		// ":name" is considered to be the same as "name"
	}
  return prefix;
}


bool Dc1Util::HasNodeName(DOMNode * node, const XMLCh * name, const XMLCh * ns)
{
#if 0 // _DEBUG // Just a test, if we have missed something
if(ns && XMLString::startsWith(ns, X("http://www.w3.org")))
{
	throw ns;
}
#endif

	if(node)
	{
		const XMLCh * name1 = Dc1Util::LocalName(node->getNodeName());
		const XMLCh * ns1 = node->getNamespaceURI();
		
		return XMLString::compareString(name, name1) == 0 
			&& XMLString::compareString(ns, ns1) == 0;
	}
	return false;
}

bool Dc1Util::GetXsiType(DOMElement * node, XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, NamespaceHelper * xsitype)
{
	if(node && doc && xsitype) {
		const XMLCh * prefixedtype = node->getAttributeNS(X("http://www.w3.org/2001/XMLSchema-instance"), X("type"));
		if(prefixedtype &&  XMLString::stringLen(prefixedtype) > 0) {
			xsitype->Reset();
			xsitype->LocalName = XMLString::replicate(Dc1Util::LocalName(prefixedtype));
			xsitype->Prefix = Dc1Util::Prefix(prefixedtype); // Already allocated
			xsitype->NamespaceUri = XMLString::replicate(Dc1Util::LookupNamespaceUri(doc, xsitype->Prefix));
			return true;
		}
	}
	return false;
}

XMLCh * Dc1Util::QualifiedName(const NamespaceHelper * val)
{
	XMLCh * result = 0;
	if(val && val->IsValid()) {
		if(val->NamespaceUri && XMLString::stringLen(val->NamespaceUri) > 0) {
			result = Dc1Util::CatString(val->NamespaceUri, X(":" ));
			Dc1Util::CatString(&result, val->LocalName);
		}
		else {
			result = XMLString::replicate(val->LocalName);
		}
	}
	return result;
}

/*static*/ XMLCh * Dc1Util::QualifiedName(const XMLCh * nsuri, const XMLCh * name)
{
	XMLCh * result = 0;
	if(nsuri && name) {
		result = Dc1Util::CatString(nsuri, X(":" ));
		Dc1Util::CatString(&result, name);
	}
	return result;
}
