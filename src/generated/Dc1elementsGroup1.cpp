 

// Core library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// no includefile for extension defined 
// file Dc1elementsGroup1_ExtImplInclude.h


#include "Dc1elementsGroup1_CollectionType.h"
#include "Dc1elementsGroup1.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementsGroup1_LocalType.h" // Choice collection rights
#include "Dc1elementType.h" // Choice collection element rights

#include <assert.h>
IDc1elementsGroup1::IDc1elementsGroup1()
{

// no includefile for extension defined 
// file Dc1elementsGroup1_ExtPropInit.h

}

IDc1elementsGroup1::~IDc1elementsGroup1()
{
// no includefile for extension defined 
// file Dc1elementsGroup1_ExtPropCleanup.h

}

Dc1elementsGroup1::Dc1elementsGroup1()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Dc1elementsGroup1::~Dc1elementsGroup1()
{
	Cleanup();
}

void Dc1elementsGroup1::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_elementsGroup1_LocalType = Dc1elementsGroup1_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Dc1elementsGroup1_ExtMyPropInit.h

}

void Dc1elementsGroup1::Cleanup()
{
// no includefile for extension defined 
// file Dc1elementsGroup1_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_elementsGroup1_LocalType);
}

void Dc1elementsGroup1::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use elementsGroup1Ptr, since we
	// might need GetBase(), which isn't defined in IelementsGroup1
	const Dc1Ptr< Dc1elementsGroup1 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_elementsGroup1_LocalType);
		this->SetelementsGroup1_LocalType(Dc1Factory::CloneObject(tmp->GetelementsGroup1_LocalType()));
}

Dc1NodePtr Dc1elementsGroup1::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Dc1elementsGroup1::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1elementsGroup1_CollectionPtr Dc1elementsGroup1::GetelementsGroup1_LocalType() const
{
		return m_elementsGroup1_LocalType;
}

void Dc1elementsGroup1::SetelementsGroup1_LocalType(const Dc1elementsGroup1_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup1::SetelementsGroup1_LocalType().");
	}
	if (m_elementsGroup1_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_elementsGroup1_LocalType);
		m_elementsGroup1_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_elementsGroup1_LocalType) m_elementsGroup1_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Dc1elementsGroup1::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("rights")) == 0)
	{
		// rights is contained in itemtype elementsGroup1_LocalType
		// in choice collection elementsGroup1_CollectionType

		context->Found = true;
		Dc1elementsGroup1_CollectionPtr coll = GetelementsGroup1_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup1_CollectionType; // FTT, check this
				SetelementsGroup1_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup1_LocalPtr)coll->elementAt(i))->Getrights()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup1_LocalPtr item = CreateelementsGroup1_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup1_LocalPtr)coll->elementAt(i))->Setrights(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup1_LocalPtr)coll->elementAt(i))->Getrights()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for elementsGroup1
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "elementsGroup1");
	}
	return result;
}

XMLCh * Dc1elementsGroup1::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Dc1elementsGroup1::Parse(const XMLCh * const txt)
{
	return false;
}


void Dc1elementsGroup1::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("http://purl.org/dc/elements/1.1/"), X("elementsGroup1"));
	// Element serialization:
	if (m_elementsGroup1_LocalType != Dc1elementsGroup1_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_elementsGroup1_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Dc1elementsGroup1::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// http://purl.org/dc/elements/1.1/:rights
			Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:rights")) == 0)
				))
		{
			// Deserialize factory type
			Dc1elementsGroup1_CollectionPtr tmp = CreateelementsGroup1_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetelementsGroup1_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Dc1elementsGroup1_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Dc1elementsGroup1::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("rights")) == 0)
	)
  {
	Dc1elementsGroup1_CollectionPtr tmp = CreateelementsGroup1_CollectionType; // FTT, check this
	this->SetelementsGroup1_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Dc1elementsGroup1::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetelementsGroup1_LocalType() != Dc1NodePtr())
		result.Insert(GetelementsGroup1_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Dc1elementsGroup1_ExtMethodImpl.h


