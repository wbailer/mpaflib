 

// Core library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// no includefile for extension defined 
// file Dc1elementsGroup_ExtImplInclude.h


#include "Dc1elementsGroup_CollectionType.h"
#include "Dc1elementsGroup.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementsGroup_LocalType.h" // Choice collection title
#include "Dc1elementType.h" // Choice collection element title

#include <assert.h>
IDc1elementsGroup::IDc1elementsGroup()
{

// no includefile for extension defined 
// file Dc1elementsGroup_ExtPropInit.h

}

IDc1elementsGroup::~IDc1elementsGroup()
{
// no includefile for extension defined 
// file Dc1elementsGroup_ExtPropCleanup.h

}

Dc1elementsGroup::Dc1elementsGroup()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Dc1elementsGroup::~Dc1elementsGroup()
{
	Cleanup();
}

void Dc1elementsGroup::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_elementsGroup_LocalType = Dc1elementsGroup_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Dc1elementsGroup_ExtMyPropInit.h

}

void Dc1elementsGroup::Cleanup()
{
// no includefile for extension defined 
// file Dc1elementsGroup_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_elementsGroup_LocalType);
}

void Dc1elementsGroup::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use elementsGroupPtr, since we
	// might need GetBase(), which isn't defined in IelementsGroup
	const Dc1Ptr< Dc1elementsGroup > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_elementsGroup_LocalType);
		this->SetelementsGroup_LocalType(Dc1Factory::CloneObject(tmp->GetelementsGroup_LocalType()));
}

Dc1NodePtr Dc1elementsGroup::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Dc1elementsGroup::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1elementsGroup_CollectionPtr Dc1elementsGroup::GetelementsGroup_LocalType() const
{
		return m_elementsGroup_LocalType;
}

void Dc1elementsGroup::SetelementsGroup_LocalType(const Dc1elementsGroup_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup::SetelementsGroup_LocalType().");
	}
	if (m_elementsGroup_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_elementsGroup_LocalType);
		m_elementsGroup_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_elementsGroup_LocalType) m_elementsGroup_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Dc1elementsGroup::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("title")) == 0)
	{
		// title is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Gettitle()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Settitle(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Gettitle()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("creator")) == 0)
	{
		// creator is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcreator()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setcreator(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcreator()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("subject")) == 0)
	{
		// subject is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getsubject()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setsubject(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getsubject()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("description")) == 0)
	{
		// description is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getdescription()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setdescription(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getdescription()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("publisher")) == 0)
	{
		// publisher is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getpublisher()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setpublisher(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getpublisher()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("contributor")) == 0)
	{
		// contributor is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcontributor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setcontributor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcontributor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("date")) == 0)
	{
		// date is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getdate()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setdate(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getdate()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Gettype()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Settype(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Gettype()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("format")) == 0)
	{
		// format is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getformat()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setformat(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getformat()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("identifier")) == 0)
	{
		// identifier is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getidentifier()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setidentifier(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getidentifier()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("source")) == 0)
	{
		// source is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getsource()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setsource(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getsource()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("language")) == 0)
	{
		// language is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getlanguage()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setlanguage(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getlanguage()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relation")) == 0)
	{
		// relation is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getrelation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setrelation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getrelation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("coverage")) == 0)
	{
		// coverage is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcoverage()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setcoverage(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getcoverage()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("rights")) == 0)
	{
		// rights is contained in itemtype elementsGroup_LocalType
		// in choice collection elementsGroup_CollectionType

		context->Found = true;
		Dc1elementsGroup_CollectionPtr coll = GetelementsGroup_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateelementsGroup_CollectionType; // FTT, check this
				SetelementsGroup_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getrights()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Dc1elementsGroup_LocalPtr item = CreateelementsGroup_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Setrights(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Dc1elementsGroup_LocalPtr)coll->elementAt(i))->Getrights()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for elementsGroup
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "elementsGroup");
	}
	return result;
}

XMLCh * Dc1elementsGroup::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Dc1elementsGroup::Parse(const XMLCh * const txt)
{
	return false;
}


void Dc1elementsGroup::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("http://purl.org/dc/elements/1.1/"), X("elementsGroup"));
	// Element serialization:
	if (m_elementsGroup_LocalType != Dc1elementsGroup_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_elementsGroup_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Dc1elementsGroup::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// http://purl.org/dc/elements/1.1/:title
			Dc1Util::HasNodeName(parent, X("title"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:title")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:creator
			Dc1Util::HasNodeName(parent, X("creator"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:creator")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:subject
			Dc1Util::HasNodeName(parent, X("subject"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:subject")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:description
			Dc1Util::HasNodeName(parent, X("description"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:description")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:publisher
			Dc1Util::HasNodeName(parent, X("publisher"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:publisher")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:contributor
			Dc1Util::HasNodeName(parent, X("contributor"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:contributor")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:date
			Dc1Util::HasNodeName(parent, X("date"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:date")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:type
			Dc1Util::HasNodeName(parent, X("type"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:type")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:format
			Dc1Util::HasNodeName(parent, X("format"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:format")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:identifier
			Dc1Util::HasNodeName(parent, X("identifier"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:identifier")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:source
			Dc1Util::HasNodeName(parent, X("source"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:source")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:language
			Dc1Util::HasNodeName(parent, X("language"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:language")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:relation
			Dc1Util::HasNodeName(parent, X("relation"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:relation")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:coverage
			Dc1Util::HasNodeName(parent, X("coverage"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:coverage")) == 0)
				||
			// http://purl.org/dc/elements/1.1/:rights
			Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("http://purl.org/dc/elements/1.1/:rights")) == 0)
				))
		{
			// Deserialize factory type
			Dc1elementsGroup_CollectionPtr tmp = CreateelementsGroup_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetelementsGroup_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Dc1elementsGroup_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Dc1elementsGroup::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("title"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("title")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("creator"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("creator")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("subject"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("subject")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("description"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("description")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("publisher"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("publisher")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("contributor"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("contributor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("date"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("date")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("type"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("type")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("format"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("format")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("identifier"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("identifier")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("source"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("source")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("language"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("language")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("relation"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("relation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("coverage"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("coverage")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("rights")) == 0)
	)
  {
	Dc1elementsGroup_CollectionPtr tmp = CreateelementsGroup_CollectionType; // FTT, check this
	this->SetelementsGroup_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Dc1elementsGroup::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetelementsGroup_LocalType() != Dc1NodePtr())
		result.Insert(GetelementsGroup_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Dc1elementsGroup_ExtMethodImpl.h


