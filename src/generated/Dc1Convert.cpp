
// Based on Convert_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include "Dc1Convert.h"
#include "Dc1Util.h"


#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32 and XML_VISUALCPP
#	if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP)
#		include <stdio.h> // sprintf
#		include <stdlib.h> // atoi, atof
#	endif
#elif defined(XML_MACOS) || defined(APPLE) || defined(__APPLE__) // Xerces 3.0 doesn't know XML_MACOS
#		include <stdio.h> // sprintf
#		include <stdlib.h> // atoi, atof
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
#		include <stdio.h> // sprintf
#		include <stdlib.h> // atoi, atof
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for conversion funcs."
#endif


XMLCh * Dc1Convert::BinToText(int src)
{
	XMLCh * buf = Dc1Util::allocate(32); // new XMLCh[32];
	XMLString::binToText((int)src, buf, 31, 10);
	return buf;
}

XMLCh * Dc1Convert::BinToText(unsigned int src)
{
	XMLCh * buf = Dc1Util::allocate(32); // new XMLCh[32];
	XMLString::binToText(src, buf, 31, 10);
	return buf;
}

XMLCh * Dc1Convert::BinToText(float src)
{
#if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP)
	char buf[64] = {0};
	_snprintf_s(buf, 64, 63, "%f", src);
	return XMLString::transcode(buf);
#elif defined (XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)	
	char buf[64] = {0};
	snprintf(buf, 64, "%f", src);
	return XMLString::transcode(buf);
#else	
#	error "TODO: Add platform specific code to convert data"
#endif
}

XMLCh * Dc1Convert::BinToText(double src)
{
#if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP)
	char buf[128] = {0};
	_snprintf_s(buf, 128, 127, "%lf", src);
	return XMLString::transcode(buf);
#elif defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
	char buf[128] = {0};
	snprintf(buf, 128, "%lf", src);
	return XMLString::transcode(buf);
#else	
#	error "TODO: Add platform specific code to convert data"
#endif
}

char Dc1Convert::TextToByte(const XMLCh* text)
{
	int result = 0;
	if(text != NULL)
	{
#if	defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		char * buf = XMLString::transcode(text);
		result = atoi(buf);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return (char)result;
}

int Dc1Convert::TextToInt(const XMLCh* text)
{
	int result = 0;
	if(text != NULL)
	{
#if	defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		char * buf = XMLString::transcode(text);
		result = atoi(buf);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return result;
}

unsigned int Dc1Convert::TextToUnsignedInt(const XMLCh * text)
{
	double result = 0.0;
	if(text != NULL)
	{
#		if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		char * buf = XMLString::transcode(text);
		result = atof(buf);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return (unsigned int) result;
}

bool Dc1Convert::TextToBool(const XMLCh* text)
{
	bool result = false;
	if(text != NULL)
	{
#		if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		if(XMLString::compareString(text, X("1")) == 0 
		|| XMLString::compareString(text, X("true")) == 0)
			return true;
		else 
		if(XMLString::compareString(text, X("0")) == 0
		|| XMLString::compareString(text, X("false")) == 0)
			return false;
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return result;
}

XMLCh * Dc1Convert::TextToString(const XMLCh * text)
{
	if(text != NULL)
	{
		return XMLString::replicate(text);
	}
	return (XMLCh*) NULL;
}

float Dc1Convert::TextToFloat(const XMLCh * text)
{
	double result = 0.0;
	if(text != NULL)
	{
#		if defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		char * buf = XMLString::transcode(text);
		result = atof(buf);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return (float)result;
}

double Dc1Convert::TextToDouble(const XMLCh * text)
{
	double result = 0.0;
	if(text != NULL)
	{
#		if  defined(WIN32) || defined(XML_BORLAND) || defined(XML_VISUALCPP) || defined(XML_MACOS) || defined(__APPLE__) || defined(XML_LINUX) || defined(linux)
		char * buf = XMLString::transcode(text);
		result = atof(buf);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
#		else
#			error "TODO: Add platform specific code to convert data"		
#		endif
	}
	return result;
}

// Convert "-" or "+" to -1 or +1, must be able to deal with strings like "-123"
int Dc1Convert::TextToSign(const XMLCh * text)
{
	int result = +1; // default is positive sign
	if(text != NULL)
	{
		if(XMLString::startsWith(text, X("-")))
		{
			return -1;
		}
	}
	return result;
}
