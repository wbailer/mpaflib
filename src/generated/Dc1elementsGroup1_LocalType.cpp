 

// Core library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtImplInclude.h


#include "Dc1elementType.h"
#include "Dc1elementsGroup1_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IDc1elementsGroup1_LocalType::IDc1elementsGroup1_LocalType()
{

// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtPropInit.h

}

IDc1elementsGroup1_LocalType::~IDc1elementsGroup1_LocalType()
{
// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtPropCleanup.h

}

Dc1elementsGroup1_LocalType::Dc1elementsGroup1_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Dc1elementsGroup1_LocalType::~Dc1elementsGroup1_LocalType()
{
	Cleanup();
}

void Dc1elementsGroup1_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_rights = Dc1elementPtr(); // Class


// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtMyPropInit.h

}

void Dc1elementsGroup1_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_rights);
}

void Dc1elementsGroup1_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use elementsGroup1_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IelementsGroup1_LocalType
	const Dc1Ptr< Dc1elementsGroup1_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_rights);
		this->Setrights(Dc1Factory::CloneObject(tmp->Getrights()));
}

Dc1NodePtr Dc1elementsGroup1_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Dc1elementsGroup1_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1elementPtr Dc1elementsGroup1_LocalType::Getrights() const
{
		return m_rights;
}

// implementing setter for choice 
/* element */
void Dc1elementsGroup1_LocalType::Setrights(const Dc1elementPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Dc1elementsGroup1_LocalType::Setrights().");
	}
	if (m_rights != item)
	{
		// Dc1Factory::DeleteObject(m_rights);
		m_rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_rights) m_rights->SetParent(m_myself.getPointer());
		if (m_rights && m_rights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_rights->UseTypeAttribute = true;
		}
		if(m_rights != Dc1elementPtr())
		{
			m_rights->SetContentName(XMLString::transcode("rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: elementsGroup1_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Dc1elementsGroup1_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Dc1elementsGroup1_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Dc1elementsGroup1_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Dc1elementsGroup1_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_rights != Dc1elementPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_rights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("http://purl.org/dc/elements/1.1/"), X("rights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("http://purl.org/dc/elements/1.1/"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_rights->Serialize(doc, element, &elem);
	}

}

bool Dc1elementsGroup1_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("rights"), X("http://purl.org/dc/elements/1.1/")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("rights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateelementType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setrights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Dc1elementsGroup1_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("rights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateelementType; // FTT, check this
	}
	this->Setrights(child);
  }
  return child;
 
}

Dc1NodeEnum Dc1elementsGroup1_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getrights() != Dc1NodePtr())
		result.Insert(Getrights());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Dc1elementsGroup1_LocalType_ExtMethodImpl.h


