
// Based on Document_cpp.template

#include "Dc1Document.h"

Dc1Document::Dc1Document() :
	_root()
{}

Dc1Document::Dc1Document(Dc1NodePtr root) : 
	_root(root) 
{}

Dc1Document::~Dc1Document() { 
	Dc1Factory::DeleteObject(_root); 
}

void Dc1Document::SetRoot(Dc1NodePtr root) { 
	_root = root; 
}

Dc1NodePtr Dc1Document::GetRoot() { 
	return _root; 
}

Dc1NodePtr Dc1Document::GetNode(const XMLCh* xpath) { 
	return _root->NodeFromXPath(xpath); 
}
