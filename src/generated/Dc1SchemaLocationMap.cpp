
// Based on SchemaLocationMap_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include "Dc1SchemaLocationMap.h"
#include "Dc1Util.h"

// TODO parameter consistency ! uri, prefix vs prefix, uri

Dc1SchemaLocationMap::Dc1SchemaLocationMap(void) {
	int n = 20;
	_locationMap = new Dc1RefVectorOf<SchemaData>(n);
	_nsIter = 0;
	_newNs = true;
	_defaultNS = 0;
	_defaultNSPrefix = 0;
}

Dc1SchemaLocationMap::~Dc1SchemaLocationMap(void) {
	unsigned int i;
	for (i=0;i<_locationMap->size();i++) {
		SchemaData* sd = _locationMap->elementAt(i);
		SchemaProperties* sp = (SchemaProperties*) sd->getValue();
		sd->setValue(NULL);
		_DeleteSchemaProperties(&sp);
		
		// gcc 4 does not like this line, so we split it into two
		//XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&((XMLCh*) sd->getKey()));
			
		XMLCh* key = (XMLCh*) sd->getKey();
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&key);
	}
	_locationMap->removeAllElements();
	delete _locationMap;
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&_defaultNS);
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&_defaultNSPrefix);
}

void Dc1SchemaLocationMap::SetSchemaLocation(const XMLCh* namespaceURI,const  XMLCh* prefix,const  XMLCh* schemaLocation) {
	// check if schema already in list
	SchemaData* sl = _GetSchemaData(namespaceURI);
	// if so, delete old location and set new
	if (sl) {
		SchemaProperties* oldProps = (SchemaProperties*) sl->getValue();
		SchemaProperties* sp = new SchemaProperties;
		int n = 3;
		sp->prefixes = new Dc1RefVectorOf<XMLCh>(n);
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(prefix));
		sp->location = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(schemaLocation);
		sp->prefix_it = 0;		
		sl->setValue(sp);
		_DeleteSchemaProperties(&oldProps);
	}
	// otherwise add new pair
	else {
		SchemaProperties* sp = new SchemaProperties;
		int n = 3;
		sp->prefixes = new Dc1RefVectorOf<XMLCh>(n);
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(prefix));
		sp->location = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(schemaLocation);
		sp->prefix_it = 0;
		sl = new SchemaData(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(namespaceURI),sp);
		_locationMap->addElement(sl);
	}
}

void Dc1SchemaLocationMap::AddPrefixAlias(XMLCh* namespaceURI, XMLCh* prefix) {
	// check if schema already in list
	SchemaData* sl = _GetSchemaData(namespaceURI);
	if (sl) {
		SchemaProperties* sp = (SchemaProperties*) sl->getValue();
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(prefix));	
	}
}

void Dc1SchemaLocationMap::SetSchemaLocation(const char* namespaceURI, const char* prefix, const char* schemaLocation) {
	// check if schema already in list
	SchemaData* sl = _GetSchemaData(X(namespaceURI));
	// if so, delete old location and set new
	if (sl) {
		SchemaProperties* oldProps = (SchemaProperties*) sl->getValue();
		SchemaProperties* sp = new SchemaProperties;
		int n = 3;
		sp->prefixes = new Dc1RefVectorOf<XMLCh>(n);
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(prefix));
		sp->location = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(schemaLocation);
		sl->setValue(sp);
		_DeleteSchemaProperties(&oldProps);
	}
	// otherwise add new pair
	else {
		SchemaProperties* sp = new SchemaProperties;
		int n = 3;
		sp->prefixes = new Dc1RefVectorOf<XMLCh>(n);
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(prefix));
		sp->location = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(schemaLocation);
		sl = new SchemaData(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(namespaceURI),sp);
		_locationMap->addElement(sl);
	}
}

void Dc1SchemaLocationMap::AddPrefixAlias(const char* namespaceURI, const char* prefix) {
	// check if schema already in list
	SchemaData* sl = _GetSchemaData(X(namespaceURI));
	if (sl) {
		// SchemaProperties* oldProps = (SchemaProperties*) sl->getValue();
		SchemaProperties* sp = new SchemaProperties;
		sp->prefixes->addElement(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(prefix));	
	}
}

const XMLCh* Dc1SchemaLocationMap::GetSchemaLocation(const XMLCh* namespaceURI) {
	SchemaData* sl = _GetSchemaData(namespaceURI);
	if (!sl) return NULL;
	return sl->getValue()->location;
}

const XMLCh* Dc1SchemaLocationMap::GetFirstSchemaPrefix(const XMLCh* namespaceURI) {
	SchemaData* sl = _GetSchemaData(namespaceURI);
	if (!sl) return NULL;
	if (sl->getValue()->prefixes->size() == 0) return NULL;
	sl->getValue()->prefix_it = 0;
	return sl->getValue()->prefixes->elementAt(0);
}

const XMLCh* Dc1SchemaLocationMap::GetNextSchemaPrefix(const XMLCh* namespaceURI) {
	SchemaData* sl = _GetSchemaData(namespaceURI);
	if (!sl) return NULL;
	if (sl->getValue()->prefix_it +1 >= (int)sl->getValue()->prefixes->size()) return NULL;
	sl->getValue()->prefix_it++;
	return sl->getValue()->prefixes->elementAt(sl->getValue()->prefix_it);
}

/**
* Retrieves a key value list of namespaceURI and its corresponding schemalocation.
*
*/
XMLCh* Dc1SchemaLocationMap::ToText() const {
	int nElems = _locationMap->size();
	int i;
	XMLCh* schemaList = NULL;
	
	for (i = 0; i < nElems; ++i) {
		SchemaData * sl = _locationMap->elementAt(i);
		// Do not include if location is empty
		if (XMLString::stringLen(sl->getValue()->location) != 0) {
		  Dc1Util::CatString(&schemaList, sl->getKey());
		  Dc1Util::CatString(&schemaList, X(" "));
			Dc1Util::CatString(&schemaList, sl->getValue()->location);
		}
		Dc1Util::CatString(&schemaList, X(" "));
	}
	return schemaList;
}

bool Dc1SchemaLocationMap::GetFirstNamespace(const XMLCh* * prefix, const XMLCh* * ns) {
	_nsIter = 0;
	_newNs = true; // TODO _newNS is deprecated
	return GetNextNamespace(prefix,ns);
}

bool Dc1SchemaLocationMap::GetNextNamespace(const XMLCh * * prefix, const XMLCh* * ns) {
	if (_nsIter>=_locationMap->size()) return false;
	SchemaData* sd = _locationMap->elementAt(_nsIter);
	if (!sd) return false;
	*ns = sd->getKey();
	*prefix = GetFirstSchemaPrefix(*ns);
	
	_nsIter++;
	return (*ns != 0);
}

void Dc1SchemaLocationMap::SetDefaultNamespace(const char* namespaceURI, const char* prefix) {
	XMLCh* prefix1 = XMLString::transcode(prefix);
	XMLCh* ns1 = XMLString::transcode(namespaceURI);
	SetDefaultNamespace(ns1, prefix1);
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&prefix1);
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&ns1);
}


void Dc1SchemaLocationMap::SetDefaultNamespace(const XMLCh* namespaceURI, const XMLCh* prefix) {
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&_defaultNS);
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&_defaultNSPrefix);
	_defaultNSPrefix = XMLString::replicate(prefix);
	_defaultNS = XMLString::replicate(namespaceURI);
}

bool Dc1SchemaLocationMap::GetDefaultNamespace(const XMLCh* * namespaceURI, const XMLCh* * prefix) {
	if(_defaultNS != 0){
		*namespaceURI = _defaultNS;
		*prefix = _defaultNSPrefix;
	}
	return _defaultNS != 0;
}

//////////////////////////////////////////////////////////////////////////////////////////

SchemaData* Dc1SchemaLocationMap::_GetSchemaData(const XMLCh* namespaceURI) {
	if(!namespaceURI) return NULL;
	int nElems = _locationMap->size();
	int i;
	for (i=0;i<nElems;i++) {
		SchemaData* sl = _locationMap->elementAt(i);
		if (sl && XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(sl->getKey(),namespaceURI)==0) return sl;
	}
	return NULL;
}


void Dc1SchemaLocationMap::_DeleteSchemaProperties(SchemaProperties** sp) {
	// remove all XMLCh*s in prefixes list manually
	while ((*sp)->prefixes->size()>0) {
		XMLCh* pref = (*sp)->prefixes->orphanElementAt(0);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&pref);
	}
	(*sp)->prefixes->removeAllElements();
	delete (*sp)->prefixes;
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&((*sp)->location));
	delete (*sp);
	*sp = NULL;
}
