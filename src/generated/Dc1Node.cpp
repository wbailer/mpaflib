
// Based on Node_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include "Dc1Node.h"


#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
#	include <typeinfo.h>
#elif defined(XML_MACOS) || defined(APPLE) || defined(__APPLE__) // Xerces 3.0 doesn't know XML_MACOS
#	include <typeinfo>
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
# include <typeinfo>
#else
#	error "TODO: Add platform specific header for RTTI"
#endif


#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>

#include <assert.h>

#include "Dc1ClientManager.h"
#include "Dc1Convert.h"
#include "Dc1NodeCollection.h"
#include "Dc1Enumerator.h"
#include "Dc1Exception.h"
#include "Dc1XPathParseContext.h"

Dc1Node::Dc1Node(void)
{
	m_ContentName = (XMLCh*)NULL;
	UseTypeAttribute = false;
	lockOwner = DC1_UNDEFINED_CLIENT;
}

Dc1Node::~Dc1Node(void)
{
	if (m_ContentName)
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&m_ContentName);
}

const char * Dc1Node::GetTypeName() const
{
	// MSVC help says:
	// The type_info::name member function returns a const char* to a null-terminated string 
	// representing the human-readable name of the type. 
	// The memory pointed to is cached and should never be directly deallocated.
	
	// So if we temporarily create a class and return its typename, 
	// then the class goes out of scope, the lifetime of its name depends on the caching mode
	// and we might have troubles on different compilers...
	
	// FTT TODO: Check this, we strip any prefix like 'class ', 'struct '...
	const char * fullname = typeid(*this).name();
	const char * name = strrchr(fullname, ' ');
	if(name)
	  return ++name; // Skip blank
	return fullname;
}

const unsigned int Dc1Node::GetClassId() const
{
	return m_ClassId;
}


void Dc1Node::SetContentName(XMLCh * contentname)
{
	if (GetBaseRoot() == m_myself.getPointer()) {
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&m_ContentName);
		m_ContentName = contentname;
	} else {
		GetBaseRoot()->SetContentName(contentname);
	}
}

const XMLCh * Dc1Node::GetContentName() const
{
	if (GetBaseRoot() == m_myself.getPointer()) {
		return m_ContentName;
	} else {
		return GetBaseRoot()->GetContentName();
	}
}

bool Dc1Node::ContentFromString(const XMLCh * const txt)
{
	return false; // ComplextTypes with SimpleContent and SimpleTypes are implementing their own code.
}

XMLCh * Dc1Node::ContentToString() const
{
	return (XMLCh*)NULL; // ComplextTypes with SimpleContent and SimpleTypes are implementing their own code.
}


Dc1NodePtr Dc1Node::CreateChild(const XMLCh * const /*type*/, const XMLCh * const /*xsitype*/)
{
	// types should override this method
	throw Dc1Exception( DC1_FATAL_ERROR, "Dc1Node::CreateChild() not implemented. Subclasses can override this." );
}

XMLCh *Dc1Node::GetXPath() const
{
	// FIXME: fix the memory leaks in here.
	// FTT fixed one leak, dont know whether there are more of them...
	
	XMLCh *result;
	Dc1NodePtr me = m_myself.getPointer();
	Dc1NodeCollectionPtr me_as_collection = me;
	Dc1NodePtr parent = GetParent();
	Dc1NodePtr grandparent;
	if (parent == Dc1NodePtr()) {
		// Arrived at root.
		
		// FTT added just to avoid stack overflow
		if(!GetContentName())
		{
			throw Dc1Exception( DC1_FATAL_ERROR, "Dc1Node: method GetXPath(...) missing content name for root node." );
		}
		
		result = Dc1Util::CatString(X("/"), GetContentName());
	} else {
		grandparent = parent->GetParent();
		Dc1NodeCollectionPtr parent_coll = parent;
		Dc1NodeCollectionPtr grandparent_coll = grandparent;
		if (strstr(parent->GetTypeName(), "LocalType")
			&& grandparent_coll != Dc1NodeCollectionPtr())
		{
			// We're an element in a LocalType in a collection.  The
			// LocalType knows the position in the collection, we know
			// the correct node name.

			// XPath indices start at 1 -- adjust.
			unsigned int pos = grandparent_coll->elementIndexOf(parent) + 1;
			result = grandparent->GetXPath();
			Dc1Util::CatString(&result, X("/"));
			Dc1Util::CatString(&result, GetContentName());
			Dc1Util::CatString(&result, X("["));
			Dc1Util::CatString(&result, Dc1Convert::BinToText(pos));
			Dc1Util::CatString(&result, X("]"));
		} else if(parent_coll != Dc1NodeCollectionPtr()) {
			// We're an element in a collection.

			// XPath indices start at 1 -- adjust.
			unsigned int pos = parent_coll->elementIndexOf(
			m_myself.getPointer()) + 1;
			result = parent->GetXPath();
			if (GetContentName()) {
				// The right ContentName is set in the collection
				// (hopefully) :)
				Dc1Util::CatString(&result, X("/"));
				Dc1Util::CatString(&result, GetContentName());
			}
			Dc1Util::CatString(&result, X("["));
			Dc1Util::CatString(&result, Dc1Convert::BinToText(pos));
			Dc1Util::CatString(&result, X("]"));
		} else {
			// We're not in a collection.
			result = parent->GetXPath();
			if (GetContentName() && !me_as_collection) {
				Dc1Util::CatString(&result, X("/"));
				Dc1Util::CatString(&result, GetContentName());
			}
		}
	}

	return result;
}

Dc1NodePtr Dc1Node::NodeFromXPath(const char *xpath) const
{
	return NodeFromXPath(X(xpath));
}

Dc1NodePtr Dc1Node::NodeFromXPath(const XMLCh *xpath) const
{
	const XMLCh *work_string = xpath;
	const XMLCh *content_name = GetContentName();
	Dc1NodePtr me = m_myself.getPointer();
	Dc1NodeCollectionPtr me_as_collection = me;
	Dc1NodePtr parent = GetParent();

	assert(!me_as_collection);	  // collections have overridden method

	if (XMLString::startsWith(work_string, X("/"))) {
		if (!parent)
			work_string++;		  // consume leading slash
		else {
			Dc1NodePtr root = parent;
			while (root->GetParent())
				root = root->GetParent();
			return root->NodeFromXPath(work_string); // EXIT: parse from root
		}
	}

	if (XMLString::stringLen(work_string) == 0)
		return me;

	if (XMLString::startsWith(work_string, X("..")))
		return UpwardWithXPath(work_string);

	XMLCh slash = X("/")[0];
	//XMLCh bracket = X("[")[0];
	//XMLCh close_bracket = X("]")[0];
	int next_slash = XMLString::indexOf(work_string, slash);
	//int next_bracket = XMLString::indexOf(work_string, bracket);
	//int next_close_bracket = XMLString::indexOf(work_string, close_bracket);
	
// This will not be fixed anymore, since its deprecated!!!	
// FTT this is a hack, it doesn't work with extension libs, will be fixed after my holidays...
const char * classtype = me->GetTypeName();
bool workaround_for_local_element_coll_itemtypes = 
!
(
strstr(classtype, "AffectiveType_Score_LocalType") // Element "Score" in AffectiveType_Score_CollectionType

|| strstr(classtype, "AudioLLDScalarType_SeriesOfScalar_LocalType") // Element "SeriesOfScalar" in AudioLLDScalarType_SeriesOfScalar_CollectionType

|| strstr(classtype, "AudioLLDVectorType_SeriesOfVector_LocalType") // Element "SeriesOfVector" in AudioLLDVectorType_SeriesOfVector_CollectionType

|| strstr(classtype, "AvailabilityType_AvailabilityPeriod_LocalType") // Element "AvailabilityPeriod" in AvailabilityType_AvailabilityPeriod_CollectionType

|| strstr(classtype, "BrowsingPreferencesType_PreferenceCondition_LocalType") // Element "PreferenceCondition" in BrowsingPreferencesType_PreferenceCondition_CollectionType

|| strstr(classtype, "ClassificationPreferencesType_Country_LocalType") // Element "Country" in ClassificationPreferencesType_Country_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_DatePeriod_LocalType") // Element "DatePeriod" in ClassificationPreferencesType_DatePeriod_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_LanguageFormat_LocalType") // Element "LanguageFormat" in ClassificationPreferencesType_LanguageFormat_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_Language_LocalType") // Element "Language" in ClassificationPreferencesType_Language_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_CaptionLanguage_LocalType") // Element "CaptionLanguage" in ClassificationPreferencesType_CaptionLanguage_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_Form_LocalType") // Element "Form" in ClassificationPreferencesType_Form_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_Genre_LocalType") // Element "Genre" in ClassificationPreferencesType_Genre_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_Subject_LocalType") // Element "Subject" in ClassificationPreferencesType_Subject_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_Review_LocalType") // Element "Review" in ClassificationPreferencesType_Review_CollectionType
|| strstr(classtype, "ClassificationPreferencesType_ParentalGuidance_LocalType") // Element "ParentalGuidance" in ClassificationPreferencesType_ParentalGuidance_CollectionType

|| strstr(classtype, "ClassificationType_Genre_LocalType") // Element "Genre" in ClassificationType_Genre_CollectionType
|| strstr(classtype, "ClassificationType_CaptionLanguage_LocalType") // Element "CaptionLanguage" in ClassificationType_CaptionLanguage_CollectionType
|| strstr(classtype, "ClassificationType_SignLanguage_LocalType") // Element "SignLanguage" in ClassificationType_SignLanguage_CollectionType

|| strstr(classtype, "ColorSamplingType_Field_LocalType") // Element "Field" in ColorSamplingType_Field_CollectionType

|| strstr(classtype, "ColorSamplingType_Field_Component_LocalType") // Element "Component" in ColorSamplingType_Field_Component_CollectionType

|| strstr(classtype, "ContourShapeType_Peak_LocalType") // Element "Peak" in ContourShapeType_Peak_CollectionType

|| strstr(classtype, "CreationPreferencesType_Title_LocalType") // Element "Title" in CreationPreferencesType_Title_CollectionType
|| strstr(classtype, "CreationPreferencesType_Creator_LocalType") // Element "Creator" in CreationPreferencesType_Creator_CollectionType
|| strstr(classtype, "CreationPreferencesType_Keyword_LocalType") // Element "Keyword" in CreationPreferencesType_Keyword_CollectionType
|| strstr(classtype, "CreationPreferencesType_Location_LocalType") // Element "Location" in CreationPreferencesType_Location_CollectionType
|| strstr(classtype, "CreationPreferencesType_DatePeriod_LocalType") // Element "DatePeriod" in CreationPreferencesType_DatePeriod_CollectionType
|| strstr(classtype, "CreationPreferencesType_Tool_LocalType") // Element "Tool" in CreationPreferencesType_Tool_CollectionType

|| strstr(classtype, "CreationToolType_Setting_LocalType") // Element "Setting" in CreationToolType_Setting_CollectionType

|| strstr(classtype, "CreationType_CreationCoordinates_LocalType") // Element "CreationCoordinates" in CreationType_CreationCoordinates_CollectionType

|| strstr(classtype, "DominantColorType_Value_LocalType") // Element "Value" in DominantColorType_Value_CollectionType

|| strstr(classtype, "FinancialType_AccountItem_LocalType") // Element "AccountItem" in FinancialType_AccountItem_CollectionType

|| strstr(classtype, "HandWritingRecogInformationType_InkLexicon_Entry_LocalType") // Element "Entry" in HandWritingRecogInformationType_InkLexicon_Entry_CollectionType

|| strstr(classtype, "HandWritingRecogResultType_Result_LocalType") // Element "Result" in HandWritingRecogResultType_Result_CollectionType

|| strstr(classtype, "InkMediaInformationType_OverlaidMedia_LocalType") // Element "OverlaidMedia" in InkMediaInformationType_OverlaidMedia_CollectionType
|| strstr(classtype, "InkMediaInformationType_Brush_LocalType") // Element "Brush" in InkMediaInformationType_Brush_CollectionType
|| strstr(classtype, "InkMediaInformationType_Param_LocalType") // Element "Param" in InkMediaInformationType_Param_CollectionType

|| strstr(classtype, "InlineTermDefinitionType_Name_LocalType") // Element "Name" in InlineTermDefinitionType_Name_CollectionType
|| strstr(classtype, "InlineTermDefinitionType_Term_LocalType") // Element "Term" in InlineTermDefinitionType_Term_CollectionType

|| strstr(classtype, "KeywordAnnotationType_Keyword_LocalType") // Element "Keyword" in KeywordAnnotationType_Keyword_CollectionType

|| strstr(classtype, "MatchingHintType_Hint_LocalType") // Element "Hint" in MatchingHintType_Hint_CollectionType

|| strstr(classtype, "MediaQualityType_QualityRating_LocalType") // Element "QualityRating" in MediaQualityType_QualityRating_CollectionType

|| strstr(classtype, "MediaSpaceMaskType_SubInterval_LocalType") // Element "SubInterval" in MediaSpaceMaskType_SubInterval_CollectionType

|| strstr(classtype, "MelodySequenceType_NoteArray_LocalType") // Element "NoteArray" in MelodySequenceType_NoteArray_CollectionType

|| strstr(classtype, "MelodySequenceType_NoteArray_Note_LocalType") // Element "Note" in MelodySequenceType_NoteArray_Note_CollectionType

|| strstr(classtype, "OrderedGroupDataSetMaskType_SubInterval_LocalType") // Element "SubInterval" in OrderedGroupDataSetMaskType_SubInterval_CollectionType

|| strstr(classtype, "OrderingKeyType_Field_LocalType") // Element "Field" in OrderingKeyType_Field_CollectionType

|| strstr(classtype, "OrganizationType_Name_LocalType") // Element "Name" in OrganizationType_Name_CollectionType
|| strstr(classtype, "OrganizationType_NameTerm_LocalType") // Element "NameTerm" in OrganizationType_NameTerm_CollectionType

|| strstr(classtype, "PersonGroupType_Name_LocalType") // Element "Name" in PersonGroupType_Name_CollectionType
|| strstr(classtype, "PersonGroupType_NameTerm_LocalType") // Element "NameTerm" in PersonGroupType_NameTerm_CollectionType

|| strstr(classtype, "PersonType_Affiliation_LocalType") // Element "Affiliation" in PersonType_Affiliation_CollectionType

|| strstr(classtype, "PhoneticTranscriptionLexiconType_Token_LocalType") // Element "Token" in PhoneticTranscriptionLexiconType_Token_CollectionType

|| strstr(classtype, "PlaceType_AdministrativeUnit_LocalType") // Element "AdministrativeUnit" in PlaceType_AdministrativeUnit_CollectionType

|| strstr(classtype, "PointOfViewType_Importance_LocalType") // Element "Importance" in PointOfViewType_Importance_CollectionType

|| strstr(classtype, "PreferenceConditionType_Time_LocalType") // Element "Time" in PreferenceConditionType_Time_CollectionType

|| strstr(classtype, "ProbabilityDistributionType_Moment_LocalType") // Element "Moment" in ProbabilityDistributionType_Moment_CollectionType
|| strstr(classtype, "ProbabilityDistributionType_Cumulant_LocalType") // Element "Cumulant" in ProbabilityDistributionType_Cumulant_CollectionType

|| strstr(classtype, "RegionLocatorType_Box_LocalType") // Element "Box" in RegionLocatorType_Box_CollectionType
|| strstr(classtype, "RegionLocatorType_Polygon_LocalType") // Element "Polygon" in RegionLocatorType_Polygon_CollectionType

|| strstr(classtype, "ScalableSeriesType_Scaling_LocalType") // Element "Scaling" in ScalableSeriesType_Scaling_CollectionType

|| strstr(classtype, "SegmentType_TextAnnotation_LocalType") // Element "TextAnnotation" in SegmentType_TextAnnotation_CollectionType

|| strstr(classtype, "SemanticBaseType_MediaOccurrence_LocalType") // Element "MediaOccurrence" in SemanticBaseType_MediaOccurrence_CollectionType

|| strstr(classtype, "SemanticPlaceType_SemanticPlaceInterval_LocalType") // Element "SemanticPlaceInterval" in SemanticPlaceType_SemanticPlaceInterval_CollectionType

|| strstr(classtype, "SemanticStateType_AttributeValuePair_LocalType0") // Element "AttributeValuePair" in SemanticStateType_AttributeValuePair_CollectionType0

|| strstr(classtype, "SemanticTimeType_SemanticTimeInterval_LocalType") // Element "SemanticTimeInterval" in SemanticTimeType_SemanticTimeInterval_CollectionType

|| strstr(classtype, "SourcePreferencesType_DisseminationFormat_LocalType") // Element "DisseminationFormat" in SourcePreferencesType_DisseminationFormat_CollectionType
|| strstr(classtype, "SourcePreferencesType_DisseminationSource_LocalType") // Element "DisseminationSource" in SourcePreferencesType_DisseminationSource_CollectionType
|| strstr(classtype, "SourcePreferencesType_DisseminationLocation_LocalType") // Element "DisseminationLocation" in SourcePreferencesType_DisseminationLocation_CollectionType
|| strstr(classtype, "SourcePreferencesType_DisseminationDate_LocalType") // Element "DisseminationDate" in SourcePreferencesType_DisseminationDate_CollectionType
|| strstr(classtype, "SourcePreferencesType_Disseminator_LocalType") // Element "Disseminator" in SourcePreferencesType_Disseminator_CollectionType
|| strstr(classtype, "SourcePreferencesType_MediaFormat_LocalType") // Element "MediaFormat" in SourcePreferencesType_MediaFormat_CollectionType

|| strstr(classtype, "SpeakerInfoType_PhoneIndex_PhoneIndexEntry_LocalType") // Element "PhoneIndexEntry" in SpeakerInfoType_PhoneIndex_PhoneIndexEntry_CollectionType

|| strstr(classtype, "SpeakerInfoType_WordIndex_WordIndexEntry_LocalType") // Element "WordIndexEntry" in SpeakerInfoType_WordIndex_WordIndexEntry_CollectionType

|| strstr(classtype, "SpokenContentLatticeType_Block_LocalType") // Element "Block" in SpokenContentLatticeType_Block_CollectionType

|| strstr(classtype, "SpokenContentLatticeType_Block_Node_LocalType") // Element "Node" in SpokenContentLatticeType_Block_Node_CollectionType

|| strstr(classtype, "SpokenContentLatticeType_Block_Node_WordLink_LocalType") // Element "WordLink" in SpokenContentLatticeType_Block_Node_WordLink_CollectionType
|| strstr(classtype, "SpokenContentLatticeType_Block_Node_PhoneLink_LocalType") // Element "PhoneLink" in SpokenContentLatticeType_Block_Node_PhoneLink_CollectionType

|| strstr(classtype, "SummaryPreferencesType_SummaryType_LocalType") // Element "SummaryType" in SummaryPreferencesType_SummaryType_CollectionType
|| strstr(classtype, "SummaryPreferencesType_SummaryTheme_LocalType") // Element "SummaryTheme" in SummaryPreferencesType_SummaryTheme_CollectionType

|| strstr(classtype, "SummaryThemeListType_SummaryTheme_LocalType") // Element "SummaryTheme" in SummaryThemeListType_SummaryTheme_CollectionType

|| strstr(classtype, "TemporalInterpolationType_InterpolationFunctions_LocalType") // Element "InterpolationFunctions" in TemporalInterpolationType_InterpolationFunctions_CollectionType

|| strstr(classtype, "TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType") // Element "KeyValue" in TemporalInterpolationType_InterpolationFunctions_KeyValue_CollectionType

|| strstr(classtype, "TermDefinitionBaseType_Name_LocalType") // Element "Name" in TermDefinitionBaseType_Name_CollectionType

|| strstr(classtype, "TermDefinitionType_Term_LocalType") // Element "Term" in TermDefinitionType_Term_CollectionType

|| strstr(classtype, "VariationType_VariationRelationship_LocalType2") // Element "VariationRelationship" in VariationType_VariationRelationship_CollectionType
)
;


	if (workaround_for_local_element_coll_itemtypes
		&& strstr(classtype, "LocalType")
		&& !strstr(classtype, ""))
	{
		// We're a LocalType, so we don't appear in the XPath query.
		// Try all child elements; take care to check the content name
		// in the children (NodeFromXPath for NodeRef collections
		// checks if the parent was a LocalType and makes sure the
		// content name matches in that case)
		
		// FTT correction, some localtypes DO appear!

//			  if ((next_bracket < 0
//							  || (next_slash >= 0 && next_bracket > next_slash))
//					  && (next_close_bracket < 0
//							  || (next_slash >= 0 && next_close_bracket > next_slash)))
//			  {
						Dc1NodeEnum children = GetAllChildElements();
						while (children.HasNext()) {
								Dc1NodePtr result = children.GetNext()->NodeFromXPath(xpath);
								if (result != Dc1NodePtr())
										return result;  // EXIT: success!
						}
//			  }
			return Dc1NodePtr();   // EXIT: no child matched the query, or we looked for a collection element.
		} else {
			// We're a normal node -- check if our ContentName matches.
			// If yes, truncate our part of the path and let each child
		// try to follow the remainder.
		if (content_name
			&& XMLString::startsWith(work_string, content_name)
			&& !XMLString::isAlphaNum(
				work_string[XMLString::stringLen(content_name)]))
		{
			if (next_slash == -1)
				return me;			  // EXIT: success!
			else {
				// We're on the right path, but not at the end.
				work_string += next_slash + 1;
				if (XMLString::startsWith(work_string, X(".."))) {
					// Need to go upwards from here.
					return UpwardWithXPath(work_string);
			} else {
				Dc1NodeEnum children = GetAllChildElements();
				while (children.HasNext()) {
					Dc1NodePtr result = children.GetNext()->NodeFromXPath(work_string);
					if (result != Dc1NodePtr())
						return result;  // EXIT: success!
				}
			}
			return Dc1NodePtr();   // EXIT: no child matched the query.
			}
		}
		else if (!content_name)
		{
			// We don't have a ContentName -- let's try our luck with
			// the children ...
			Dc1NodeEnum children = GetAllChildElements();
			while (children.HasNext()) {
				Dc1NodePtr result
					= children.GetNext()->NodeFromXPath(xpath);
				if (result != Dc1NodePtr())
					return result;  // EXIT: success!
			}
			return Dc1NodePtr();   // EXIT: no child matched the query.
		}
		else
			// We have a non-matching ContentName -- failure.
			return Dc1NodePtr();   // EXIT: no child matched the query.
	}
}


Dc1NodeEnum Dc1Node::GetOrCreateFromXPath(const char *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */)
{
	return GetOrCreateFromXPath(X(xpath), client, flag);
}

Dc1NodeEnum Dc1Node::GetOrCreateFromXPath(const XMLCh * xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation /*flag*/ /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{
	if(!context) Dc1XPathParseContext::ErrorWrongCaller(xpath);
	
	Dc1NodeEnum result;
	Dc1XPathParseContext::Parse(context);
	if(context->Operation == DC1_XPATH_UP)
	{	
		return Dc1XPathParseContext::Up(context, m_myself.getPointer(), client);
	}
	if(context->Operation == DC1_XPATH_ROOT)
	{
		return Dc1XPathParseContext::Root(context, m_myself.getPointer(), client);
	}
	return result;
}

Dc1NodeEnum Dc1Node::GetFromXPath(const char *xpath) /* const */
{
	return GetFromXPath(X(xpath));
}

Dc1NodeEnum Dc1Node::GetFromXPath(const XMLCh *xpath) /* const */
{
	return GetOrCreateFromXPath(xpath, DC1_UNDEFINED_CLIENT, DC1_NOCREATE);
}


Dc1NodePtr Dc1Node::UpwardWithXPath(const XMLCh *xpath) const
{
	const XMLCh *work_string = xpath;
	Dc1NodePtr ancestor = GetParent();

	//ASSERT(XMLString::startsWith(work_string, X("..")));
	if (!ancestor) return Dc1NodePtr(); // EXIT: no going up from root
	Dc1NodeCollectionPtr ancestor_as_collection = ancestor;
	while (ancestor) {
		// Look upward for a "meaningful" node to continue parsing.
		if (!ancestor_as_collection
			&& (!strstr(ancestor->GetTypeName(), "LocalType")
			|| strstr(ancestor->GetTypeName(), ""))
			// TODO: auf ContentName pruefen? -> mit Testcases abklaeren
			//&& ancestor->GetContentName()
		)
			break;
		ancestor = ancestor->GetParent();
		ancestor_as_collection = ancestor;
	}
	work_string += 2;			   // consume ".."
	if (ancestor && XMLString::startsWith(work_string, X("/")))
		// Consume leading slash, continue parsing at ancestor
		return ancestor->NodeFromXPath(work_string + 1);
	else if (XMLString::stringLen(work_string) == 0)
		return ancestor;		// EXIT: success or no ancestor found
	else
		return Dc1NodePtr(); // EXIT: incorrect search string
		// or no ancestor found
}



Dc1NodePtr Dc1Node::GetParent() const
{
	return m_Parent.getPointer();
}

void Dc1Node::SetParent(const Dc1NodePtr &new_parent)
{
	m_Parent = new_parent;
}

//
// Locking
//

bool Dc1Node::Lock(Dc1ClientID id, bool receiveNotification) { 
	if (!Dc1ClientManager::IsLockingEnabled()) return true;

	bool canLock = true;
	Dc1NodePtr parent = GetParent();
	if (parent) return parent->Lock(id,receiveNotification);
	else {
		// locking is done on top-most base class
		Dc1NodePtr baseRoot = GetBaseRoot();
		
		// check if locked
		if (baseRoot->IsLocked(DC1_UNDEFINED_CLIENT)) {
			canLock = Dc1ClientManager::SendLockRequestNotification(m_myself.getPointer(),id,false,receiveNotification);
				
		}
		else canLock = false;

		if (!canLock) {
				// if client wants to be wait, add to queue
				if (receiveNotification) {
						Dc1ClientManager::QueueForLock(m_myself.getPointer(),id,false);
				}
				return false;
		}
		
		// set lock on top-most base class
		baseRoot->lockOwner = id;

		// send notification about lock
		Dc1ClientManager::SendLockNotification(m_myself.getPointer(),id,false);

		return true;
	}
}

bool Dc1Node::Lock4Add(Dc1ClientID id, bool receiveNotification) { 
	if (!Dc1ClientManager::IsLockingEnabled()) return true;

	// currently, this is the same than a real lock
	return Lock(id,receiveNotification);
}

void Dc1Node::Unlock(Dc1ClientID id) {
	if (!Dc1ClientManager::IsLockingEnabled()) return;
	
	Dc1NodePtr parent = GetParent();
	if (parent) parent->Unlock(id);
	else {
		GetBaseRoot()->lockOwner = DC1_UNDEFINED_CLIENT;
	
		Dc1ClientManager::SendUnlockNotification(m_myself.getPointer());
	}
}

bool Dc1Node::IsLocked(Dc1ClientID id) {
	if (!Dc1ClientManager::IsLockingEnabled()) return true;
	
	Dc1NodePtr parent = GetParent();
	if (parent) return parent->IsLocked(id);
	
	return (GetBaseRoot()->lockOwner == id);
}


//
// Notifications
//

// TODO
void Dc1Node::Register(Dc1ClientID /*id*/, Dc1NotificationAction /*action*/, bool /*subtree*/) {
	//Dc1ClientManager::Register(id,action,this,subtree);
}

// TODO
void Dc1Node::Unregister(Dc1ClientID /*id*/, Dc1NotificationAction /*action*/) {
	//Dc1ClientManager::Unregister(id,action,this);
}
