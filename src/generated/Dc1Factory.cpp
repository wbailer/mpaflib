
/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Based on CoreFactory_cpp.template

#include <stdio.h> // sprintf
#include <xercesc/util/ValueHashTableOf.hpp>
#include <xercesc/util/ValueVectorOf.hpp> // For FactoryVector
#include <xercesc/util/RefVectorOf.hpp> // For NameVector
#include <xercesc/util/XMLString.hpp> // (hashable) GUID type

#include "Dc1Factory.h"
#include "Dc1Node.h"

#include "Dc1FactoryForward.h"

#include <xercesc/util/XMLUniDefs.hpp>
#include "Dc1SchemaLocationMap.h"


int Dc1Factory::initialized = 0;

// The array of registerable factories
typedef ValueVectorOf< Dc1Factory * > Dc1FactoryVector;
static Dc1FactoryVector * FactoryVector;

typedef ValueHashTableOf<unsigned int> Dc1HashTable;
// The hashtable to map a GUID to an index in factory array
static Dc1HashTable * GuidHashTable;
// The hashtable to map a classname to an index in factory array
static Dc1HashTable * NameHashTable;

// The array to map an index to a classname
typedef RefVectorOf< char > Dc1NameVector;
static Dc1NameVector * NameVector;

static Dc1SchemaLocationMap * SchemaLocationMap; // For prefix namespace mapping without XML DOM

// Map a type id to an index within the factory vector
// Returns the ordered index. Index 0 remains unused.
unsigned int Dc1Factory::MapGuidToIndex(SimpleGuid id)
{
	if(!(GuidHashTable && FactoryVector))
		throw Dc1Exception(DC1_ERROR,"Dc1Factory is not initialised.");
		
	unsigned int i = 0; // Missing entry
	try
	{
	  // FTT TODO in next major release, change id to const XMLCh* instead of SimpleGuid
		i = GuidHashTable->get(X(id)); // FTT Must be X() otherwise we are doomed with extension libs
	}
	catch(...)
	{
		i = (unsigned int)FactoryVector->size(); // unknown type -> to register
	}		
	if(i == 0) // Index 0 is reserved
	{
		i = 1;
	}
	return i;
}



// Register a new factory which can handle a class with that type id
Dc1Factory::Dc1Factory(SimpleGuid id, const char * classname)
{
	if(!FactoryVector) // Dont use ::initialized here!
		throw Dc1Exception(DC1_ERROR,"Dc1Factory is not initialized.");

	if(!classname) 
		throw Dc1Exception(DC1_ERROR,"Missing classname, registering failed.");

	// User must decide: overwrite DeleteFactory() or let the base func do its job
	m_ToDelete = false; 
	
	unsigned int i = MapGuidToIndex(id);
	
	if(i >= FactoryVector->size()) // Make room for unregistered factory
	{
		unsigned int oldsize = (unsigned int) FactoryVector->size();
		unsigned needed = i - oldsize + 1;
		unsigned int j, k = oldsize + needed;
		for(j = oldsize; j < k; ++j)
		{
			FactoryVector->addElement((Dc1Factory *)0);
			NameVector->addElement((char *)0);
		}
	}
	
	if(FactoryVector->elementAt(i)) // Replace existing entries
	{
		// printf("Removing factory for class %s, id %s, index %u\n", NameVector->elementAt(i), id, i);
		
		FactoryVector->elementAt(i)->DeleteFactory();
		FactoryVector->setElementAt((Dc1Factory *)0, i);
		// We only remove identical name keys, Note: must be done before removing the name
		try{if(NameHashTable->get(X(NameVector->elementAt(i))))NameHashTable->removeKey(X(NameVector->elementAt(i)));}catch(...){}
		delete [] NameVector->elementAt(i); // We use delete[] instead of delete()
		NameVector->setElementAt((char *) 0, i);
		try{if(GuidHashTable->get(id))GuidHashTable->removeKey(id);}catch(...){}
		
	}
	FactoryVector->setElementAt(this, i);
	XMLCh * key = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(classname); // Xerces valuehashtable cannot deal with non const keys
	NameHashTable->put(key, i);
	int len = (int)strlen(classname) + 1;
	char * name = new char[len]; // Vector uses delete(), so we must use new() not malloc()
	memcpy(name, classname, len);
	NameVector->setElementAt(name, i); 
	key = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(id); // Xerces valuehashtable cannot deal with non const keys
	GuidHashTable->put(key, i);
	// printf("Adding factory for class %s, id %s, index %u\n", classname, id, i);
		
}

// Don't use delete, use DeleteFactory() instead;
Dc1Factory::~Dc1Factory(void)
{
}

void Dc1Factory::Initialize()
{
	if(++initialized > 1)
		return;

	// Create static vectors and tables
	bool created = false;
	if(!FactoryVector)
	{
		FactoryVector = new Dc1FactoryVector(128);
		created = true;
	}
	
	if(!GuidHashTable)
	{
		GuidHashTable = new Dc1HashTable(3457); // Prime modulus
	}
	
	if(!NameHashTable)
	{
		NameHashTable = new Dc1HashTable(3457); // Prime modulus
	}
	
	if(!NameVector)
	{
		// Since Xerces uses delete() instead of delete[](), 
		// it can't adopt the strings
		NameVector = new Dc1NameVector(128, false); 
	}

	if(!SchemaLocationMap)
	{
		SchemaLocationMap = new Dc1SchemaLocationMap();
	}
	
	if(created)
	{	
		// Insert the huge amount of core factories.
#		include "Dc1FactoryInitialize.h"
	}
}


void Dc1Factory::Terminate()
{
	if(--initialized > 0)
		return;

	unsigned int i;
	if(FactoryVector)
	{
		for(i = 0; i < FactoryVector->size(); ++i)
		{	
			if(FactoryVector->elementAt(i))
			{
			  // For unregistering extension libs in dlls, use the #include "myextlibDeinitialize.h"
				FactoryVector->elementAt(i)->DeleteFactory();
				FactoryVector->elementAt(i) = (Dc1Factory *)0;
			}
		}
	}
	delete FactoryVector;
	FactoryVector = (Dc1FactoryVector *)0;
		
	XMLCh* key;
	ValueHashTableOfEnumerator<unsigned int> e1(NameHashTable);
	while(e1.hasMoreElements())
	{
		key = (XMLCh*)e1.nextElementKey();
		//char * t = XMLString::transcode(key);
		//printf("Key = %s\n", t);
		//XMLString::release(&t);
		XMLString::release(&key);
	}
	delete NameHashTable;
	NameHashTable = (Dc1HashTable *)0;
		
	ValueHashTableOfEnumerator<unsigned int> e2(GuidHashTable);
	while(e2.hasMoreElements())
	{
		key = (XMLCh*)e2.nextElementKey();
		//char * t = XMLString::transcode(key);
		//printf("Key = %s\n", t);
		//XMLString::release(&t);
		XMLString::release(&key);
	}
	delete GuidHashTable;
	GuidHashTable = (Dc1HashTable *)0;

	for(i = 0; i < NameVector->size(); ++i)
	{	
		delete [] NameVector->elementAt(i); // We use delete[] instead of delete()
	}
	delete NameVector;
	NameVector = (Dc1NameVector *)0;

	delete SchemaLocationMap;
	SchemaLocationMap = (Dc1SchemaLocationMap *)0;
}

const char * Dc1Factory::GetVersion()
{
	return "1.0.0";
}


const char * Dc1Factory::GetModelVersion()
{
	return "2.1";
}

void Dc1Factory::DeleteFactory()
{
	if(m_ToDelete)
	{
		delete this;
	}
}

Dc1Ptr<Dc1Node> Dc1Factory::CreateObject(unsigned int index)
{
	Dc1Ptr<Dc1Node> result;
	if (!initialized) return result; // It's a null pointer at this point

	if(index > 0 && index < FactoryVector->size())
	{
		result = (FactoryVector->elementAt(index))->CreateObject();
		result->m_ClassId = index;

		// The node needs access to its own reference count sometimes -> set it.
		result->m_myself = result;
		result->Init();	// Call this after setting m_myself
	}
	else
	{
		char msg[512] = {0};
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
		_snprintf_s(msg, 512, 511, "Dc1Factory::CreateObject(): Sorry, no class registered at index %u.", index);
#elif defined(__APPLE__)// Xerces 3.0 doesn't know XML_APPLE
		sprintf(msg, "Dc1Factory::CreateObject(): Sorry, no class registered at index %u.", index);
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
		sprintf(msg, "Dc1Factory::CreateObject(): Sorry, no class registered at index %u.", index);
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif		
		throw Dc1Exception(DC1_ERROR, msg);
	}
	return result;
}

Dc1NodePtr Dc1Factory::CloneObject(const Dc1NodePtr &original)
{
	if(original != NULL)
	{
		Dc1NodePtr clone = Dc1Factory::CreateObject(original->GetClassId());
		clone->DeepCopy(original);

		// Add Dc1Node attribs
		if(original->GetContentName() != NULL)
		{
			clone->SetContentName(XMLString::replicate(original->GetContentName()));
		}
		clone->UseTypeAttribute = original->UseTypeAttribute;

		return clone;
	}
	else return Dc1NodePtr();
}

unsigned int Dc1Factory::GetTypeIndex(const XMLCh * qname)
{
	if(initialized)
	{
		unsigned int i = 1; // Unknown
		try
		{
			return NameHashTable->get(qname);
		}catch(...)
		{
			// Unknown	
		}
		return i;
	}
	throw Dc1Exception(DC1_ERROR,"Dc1Factory is not initialised.");
}

const char * Dc1Factory::GetTypeName(unsigned int index)
{
	if(initialized)
	{
		if(index < NameVector->size())
		{
			return NameVector->elementAt(index);
		}
		return GetTypeName(1); // Index 1 represents Unknown
	}
	throw Dc1Exception(DC1_ERROR,"Dc1Factory is not initialised.");
}

void Dc1Factory::DeleteObject(Dc1Node * item)
{
	if(item)
	{
		(FactoryVector->elementAt(item->m_ClassId))->DeleteRawObject(item);
	}
}

void Dc1Factory::DeleteRawObject(Dc1Node * item)
{
	delete item;
}

//////////////////////////////////////////////////////////////////////
/*static*/ void Dc1Factory::SetNamespaceUri(const XMLCh * prefix, const XMLCh * nsuri)
{
	SchemaLocationMap->SetSchemaLocation(nsuri, prefix, (XMLCh*)NULL);
}

/*static*/const XMLCh* Dc1Factory::GetNamespaceUri(const XMLCh * prefix)
{
	const XMLCh* p = NULL;
	const XMLCh* ns = NULL;
	bool ok = SchemaLocationMap->GetFirstNamespace(&p, &ns);
	while (ok) {
		if(XMLString::compareString(p, prefix) == 0) {
			return ns;
		}
	if((p = SchemaLocationMap->GetNextSchemaPrefix(ns)) != 0) {
	  continue;
	}
		
		ok = SchemaLocationMap->GetNextNamespace(&p,&ns);
	}
	// Unknown types are mapped to default ns (which must be the first in list
	SchemaLocationMap->GetFirstNamespace(&p, &ns);
	return ns;
}

/*static*/ void Dc1Factory::UnregisterFactory(SimpleGuid id)
{
	if(!FactoryVector) return;

	unsigned int i = MapGuidToIndex(id);
	if(i > 0 && i < FactoryVector->size())
	{
		if(FactoryVector->elementAt(i) && FactoryVector->elementAt(i)->GetToDelete())
		{
			FactoryVector->elementAt(i)->DeleteFactory();
			FactoryVector->elementAt(i) = (Dc1Factory *)0;
		}
	}
}

