
// Based on XPathParseContext_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <xercesc/dom/DOM.hpp>

#include "Dc1Util.h"
#include "Dc1Factory.h"
#include "Dc1MissingElementException.h" // Extended exception
#include "Dc1XPathParseContext.h"
#include "Dc1NodeCollection.h" // FTT Try to get rid of this

#include <stdio.h>
#include <assert.h>

// Intro of API documentation
#include "docintro.h"

Dc1XPathParseContext::Dc1XPathParseContext()
	: ElementName(NULL), XsiType(NULL), FreeIndexList(1), QualifiedXsiType(NULL)
{

}

/* virtual */ Dc1XPathParseContext::~Dc1XPathParseContext()
{
	XMLString::release(&ElementName); // reset to NULL is done in release()
	XMLString::release(&XsiType); // reset to NULL is done in release()
	XMLString::release(&QualifiedXsiType); // reset to NULL is done in release()
	XPath = NULL; // Not our buffer!
	UnparsedXPath = NULL; // Not our buffer!
}

/* static */ bool Dc1XPathParseContext::Initialise(Dc1XPathParseContext & c
	, Dc1XPathParseContext ** context
	, const XMLCh *xpath
	, Dc1XPathCreation flag)
{
	bool isentrypoint = false;
	if(!*context) // We are starting here
	{
		isentrypoint = true;
		
		c.Operation = DC1_XPATH_UNKNOWN;
		c.Found = false;
		c.ElementName = NULL;
		c.XsiType = NULL;
		c.QualifiedXsiType = NULL;
		c.XPath = xpath;
		c.UnparsedXPath = NULL;
		c.Create = (flag == DC1_FORCECREATE ? true : false);
		c.Id = 0; // 0 = Use default type
		c.CollectionSize = 0;
		c.UpperBoundary = -1; // -1 = Unbounded or not used
		
		c.PreferType = false;
		c.IsAttribute = false;
		c.StartIndex = -3; // Take everything
		c.StopIndex = -3; // Take everything
	
		c.ItemCount = 0; 
		c.ValidItemCount = 0;
		c.LastItemIndex = -1; // -1 = Unknown
		c.LastValidItemIndex = -1; // -1 = Unknown
		c.LastNode = Dc1NodePtr();
		// c.FreeIndexList initialised automatically;
			
		*context = &c;
	}
	return isentrypoint;	
}


/* static */ void Dc1XPathParseContext::Reset(Dc1XPathParseContext * context)
{
	assert(context);
	context->Operation = DC1_XPATH_UNKNOWN;
	XMLString::release(&context->ElementName); // reset to NULL is done in release()
	XMLString::release(&context->XsiType); // reset to NULL is done in release()
	XMLString::release(&context->QualifiedXsiType); // reset to NULL is done in release()
	// Dont touch XPath (global)
	context->UnparsedXPath = NULL;
	// Dont touch Create (global)
	// Dont touch IsAttribute
	// Dont touch Id;
	// Dont touch CollectionSize;
	// Dont touch UpperBoundary;
	
	context->PreferType = false;
	context->StartIndex = -3; // Take everything
	context->StopIndex = -3; // Take everything
	// Dont touch ItemCount;
	// Dont touch ValidItemCount;
	// Dont touch LastItemIndex;
	// Dont touch LastValidItemIndex;
	// Dont touch LastNode;
	context->FreeIndexList.removeAllElements();
}

/* static */ void Dc1XPathParseContext::Parse(Dc1XPathParseContext * context)
{
	if(!context) return;
	Dc1XPathParseContext::Reset(context);
	
	if(!context->XPath) return;
	if(XMLString::stringLen(context->XPath) == 0) return;

#if 0
// XMLString::subString() test:
// Contrary to documentation subString end index is not inclusive in 2.7.0 and 2.2.0 !!!
	XMLCh * t1 = XMLString::transcode("01234");
	XMLCh * t2 = XMLString::transcode("	 ");
	XMLString::subString(t2, t1, 0, 3); // According to docu should reveal "0123"
	if(! XMLString::endsWith(t2, X("3")))
	{
		bool bloody_docu = true;
	}
#endif	
	
	context->UnparsedXPath = const_cast<XMLCh *> (context->XPath);
	int state = 1;
	
	while(state > 0)
	{
		switch(state)
		{
			case 1:	state = Dc1XPathParseContext::ScanSlash(context) ? 0 : 2; break;
			case 2: state = Dc1XPathParseContext::ScanUp(context) ? 0 : 3; break;
			case 3: Dc1XPathParseContext::ScanAttribute(context); state = 4; break;
			case 4: state = Dc1XPathParseContext::ScanElement(context) ? 5 : -1; break;
			case 5: state = Dc1XPathParseContext::ScanTail(context) ? 0: 6; break;
			case 6: state = Dc1XPathParseContext::ScanIndex(context) ? 7 : 8; break;
			case 7: state = Dc1XPathParseContext::ScanXsiType(context) ? 11 : 11; break;
			case 8: state = Dc1XPathParseContext::ScanXsiType(context) ? 9 : 10; break;
			case 9: state = Dc1XPathParseContext::ScanIndex(context) ? 10 : 10; break;
			
			case 10: 
				context->PreferType = true;
				state = 11;
				 break;
				 
			case 11:
			 	if(!Dc1XPathParseContext::ScanTail(context))
			 	{
					XMLCh * msg = XMLString::transcode("XPath error:\n");
					Dc1Util::CatString(&msg, context->XPath);
					Dc1Util::CatString(&msg, X("\nSorry, unexpected expression \""));
					Dc1Util::CatString(&msg, context->UnparsedXPath);
					Dc1Util::CatString(&msg, X("\"."));
					Dc1Exception e(DC1_ERROR, msg);
					XMLString::release(&msg);
					throw e;
				}
				state = 0;
				break;

			default: break; // Exit
		}
	}
}

/* static */ bool Dc1XPathParseContext::ScanSlash(Dc1XPathParseContext * context)
{
	assert(context);
	if(XMLString::startsWith(context->UnparsedXPath, X("/")))
	{
		if(XMLString::startsWith(context->UnparsedXPath, X("//")))
		{
			XMLCh * msg = XMLString::transcode("XPath error:\n");
			Dc1Util::CatString(&msg, context->XPath);
			Dc1Util::CatString(&msg, X("\nSorry, \"//\" is not supported."));
			Dc1Exception e(DC1_ERROR, msg);
			XMLString::release(&msg);
			throw e;
		}
		context->Operation = DC1_XPATH_ROOT;
		++context->UnparsedXPath; // Eat up /
		while(XMLString::startsWith(context->UnparsedXPath, X(" "))) // And blanks
			++context->UnparsedXPath;
		
		return true;
	}
	return false;
}

/* static */ bool Dc1XPathParseContext::ScanUp(Dc1XPathParseContext * context)
{
	assert(context);
	if(XMLString::startsWith(context->UnparsedXPath, X("..")))
	{
		context->UnparsedXPath += 2;
		if(XMLString::startsWith(context->UnparsedXPath, X("/")))
		{
			if(XMLString::startsWith(context->UnparsedXPath, X("//")))
			{
				XMLCh * msg = XMLString::transcode("XPath error:\n");
				Dc1Util::CatString(&msg, context->XPath);
				Dc1Util::CatString(&msg, X("\nSorry, \"//\" is not supported."));
				Dc1Exception e(DC1_ERROR, msg);
				XMLString::release(&msg);
				throw e;
			}
			++context->UnparsedXPath; // Eat up /
		}
		while(XMLString::startsWith(context->UnparsedXPath, X(" "))) // And blanks
			++context->UnparsedXPath;
		context->Operation = DC1_XPATH_UP;
		return true;
	}
	return false;
}

/* static */ bool Dc1XPathParseContext::ScanAttribute(Dc1XPathParseContext * context)
{
	assert(context);
	
	Match m;
	RegularExpression r("^@"); // Matches '@' to indicate an attribute
	if(r.matches(context->UnparsedXPath, &m))
	{
		context->IsAttribute = true;
		context->Operation = DC1_XPATH_SELECT_ALL;
		context->UnparsedXPath += 1;
		return true;
	}
	return false;
}

/* static */ bool Dc1XPathParseContext::ScanElement(Dc1XPathParseContext * context)
{
	assert(context);
	
	Match m;
	RegularExpression r("^([0-9a-zA-Z_:]+)"); // Matches 'bla', where bla can be prefixed
	if(r.matches(context->UnparsedXPath, &m))
	{
		context->Operation = DC1_XPATH_SELECT_ALL;
		int len = m.getEndPos(1) - m.getStartPos(1) + 1;
		context->ElementName = Dc1Util::allocate(len);
		XMLString::subString(context->ElementName, context->UnparsedXPath, m.getStartPos(1), m.getEndPos(1));
		// Strip prefix if exists, TODO: this is hacky
		XMLCh * tmp = XMLString::replicate(Dc1Util::LocalName(context->ElementName));
		Dc1Util::deallocate(&context->ElementName);
		context->ElementName = tmp;
		context->UnparsedXPath += m.getEndPos(0);
	}
	else
	{
		XMLCh * msg = XMLString::transcode("XPath error:\n");
		Dc1Util::CatString(&msg, context->XPath);
		Dc1Util::CatString(&msg, X("\nValid element name [0-9a-zA-Z_:]+ expected."));
		Dc1Exception e(DC1_ERROR, msg);
		XMLString::release(&msg);
		throw e;			
	}
	return true;
}

/* static */ bool Dc1XPathParseContext::ScanTail(Dc1XPathParseContext * context)
{
	assert(context);

	if(XMLString::stringLen(context->UnparsedXPath) > 0)
	{
		if(context->IsAttribute)
		{
			XMLCh * msg = XMLString::transcode("XPath error:\n");
			Dc1Util::CatString(&msg, context->XPath);
			Dc1Util::CatString(&msg, X("\nSorry, only plain attributes (e.g. @foo) are supported."));
			Dc1Exception e(DC1_ERROR, msg);
			XMLString::release(&msg);
			throw e;
		}
		if(XMLString::startsWith(context->UnparsedXPath, X("/")))
		{
			if(XMLString::startsWith(context->UnparsedXPath, X("//")))
			{
				XMLCh * msg = XMLString::transcode("XPath error:\n");
				Dc1Util::CatString(&msg, context->XPath);
				Dc1Util::CatString(&msg, X("\nSorry, \"//\" is not supported."));
				Dc1Exception e(DC1_ERROR, msg);
				XMLString::release(&msg);
				throw e;			
			}
			if(XMLString::endsWith(context->UnparsedXPath, X("/")))
			{
				XMLCh * msg = XMLString::transcode("XPath error:\n");
				Dc1Util::CatString(&msg, context->XPath);
				Dc1Util::CatString(&msg, X("\nSorry, unexpected end."));
				Dc1Exception e(DC1_ERROR, msg);
				XMLString::release(&msg);
				throw e;			
			}
			++context->UnparsedXPath; // Consume slash
		}
		else
		{
			return false; // Maybe an index or xsi:type
		}
	}
	return true;	
}

/* static */ bool Dc1XPathParseContext::ScanIndex(Dc1XPathParseContext * context)
{
	assert(context);
	
// This is odd! Xerces 2.7. can deal with this regular expression, whilst 2.2 can manage just half of the cases
// I don't have time to dig into this, so here are two solutions,
// the smart one for 2.7 and the dumb for 2.2!
#if _XERCES_VERSION >= 20700

  	Match m;
  	// Matches: [123] or [last()] or [position()>1 | position()<123] 
  	// or [position()<123] or [position()>1] or [position()>last()]
	// Note, order is important. The long position() expression must come first!
	RegularExpression r(
		"^[\t ]*\\[[\t ]*"
		"("
		"([0-9]+)"
		"|(last[\t ]*\\([\t ]*\\))"
		"|(position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*([0-9]+)[\t ]*\\|[\t ]*position\\(\\)[\t ]*<[\t ]*([0-9]+))" // Must come first
		"|(position[\t ]*\\([\t ]*\\)[\t ]*<[\t ]*([0-9]+))"
		"|(position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*([0-9]+))"
		"|(position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*last[\t ]*\\([\t ]*\\))"
		")"
		"[\t ]*\\][\t ]*"); 
	
	if(r.matches(context->UnparsedXPath, &m))
	{
		// Indexes for getStartPos():
		// Group index 0 is for the whole expression
		// Group index 1 is for the first (..) term (i.e. (
		// Group index 2 is for the second (..) term  (i.e ([0-9]+) ...
	
		context->Operation = DC1_XPATH_SELECT_ALL;
		if(m.getEndPos(2) - m.getStartPos(2) > 0) // [123] group
		{
			int n;
			if(Dc1XPathParseContext::ConvertIndex(m.getStartPos(2), m.getEndPos(2), 0, context, n))
			{
				context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_INDEX : DC1_XPATH_SELECT_INDEX;
				context->StartIndex = context->StopIndex = n;
				context->UnparsedXPath += m.getEndPos(0);
				return true;
			}
			return false;
		}
		else if(m.getEndPos(3) - m.getStartPos(3) > 0) // [last()] group
		{
			context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_LAST : DC1_XPATH_SELECT_LAST;
			context->StartIndex = context->StopIndex = -2; // -2 Indicates 'last()'
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
		else if(m.getEndPos(4) - m.getStartPos(4) > 0) // [position()>123 | position()<123] group
		{
			if(context->Create)
			{
				Dc1XPathParseContext::ErrorUnallowedExpression(context);
			}
			int n;
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(5), m.getEndPos(5), -1, context, n))
			{
				return false;
			}
			context->StartIndex = n;
			
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(6), m.getEndPos(6), -1, context, n))
			{
				return false;
			}
			context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_RANGE : DC1_XPATH_SELECT_POSITION_RANGE;
			context->StopIndex = n;
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
		else if(m.getEndPos(7) - m.getStartPos(7) > 0) // [position()<123] group
		{
			if(context->Create)
			{
				Dc1XPathParseContext::ErrorUnallowedExpression(context);
			}
			int n;
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(8), m.getEndPos(8), -1, context, n))
			{
				return false;
			}
			context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_LT : DC1_XPATH_SELECT_POSITION_LT;
			context->StartIndex = -1;
			context->StopIndex = n; 
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
		else if(m.getEndPos(9) - m.getStartPos(9) > 0) // [position()>123] group
		{
			if(context->Create)
			{
				Dc1XPathParseContext::ErrorUnallowedExpression(context);
			}
			int n;
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(10), m.getEndPos(10), -1, context, n))
			{
				return false;
			}
			context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_GT : DC1_XPATH_SELECT_POSITION_GT;
			context->StartIndex = n;
			context->StopIndex = -2; // last()
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
		else if(m.getEndPos(11) - m.getStartPos(11) > 0) // [position()>last()] group
		{
			if(context->Create)
			{
				context->Operation = DC1_XPATH_FORCE_CREATE;
			}
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
		else assert(false);
	}
#else // Xerces 2.2.0
  	Match m;
  	// Matches: [123] or [last()] or [position()>1 | position()<123] 
  	// or [position()<123] or [position()>1] or [position()>last()]
	// Note, order is important. The long position() expression must come first!
	RegularExpression r1(
		"^[\t ]*\\[[\t ]*"
		"([0-9]+)"
		"[\t ]*\\][\t ]*"); 
	if(r1.matches(context->UnparsedXPath, &m))
	{
		context->Operation = DC1_XPATH_SELECT_ALL;
		if(m.getEndPos(1) - m.getStartPos(1) > 0) // [123] group
		{
			int n;
			if(Dc1XPathParseContext::ConvertIndex(m.getStartPos(1), m.getEndPos(1), 0, context, n))
			{
				context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_INDEX : DC1_XPATH_SELECT_INDEX;
				context->StartIndex = context->StopIndex = n;
				context->UnparsedXPath += m.getEndPos(0);
				return true;
			}
			return false;
		}
	}
	RegularExpression r2(
		"^[\t ]*\\[[\t ]*"
		"last[\t ]*\\([\t ]*\\)"
		"[\t ]*\\][\t ]*"); 
	if(r2.matches(context->UnparsedXPath, &m)) // [last()]
	{
		context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_LAST : DC1_XPATH_SELECT_LAST;
		context->StartIndex = context->StopIndex = -2; // -2 Indicates 'last()'
		context->UnparsedXPath += m.getEndPos(0);
		return true;
	}
	RegularExpression r3(
		"^[\t ]*\\[[\t ]*"
		"(position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*([0-9]+)[\t ]*\\|[\t ]*position\\(\\)[\t ]*<[\t ]*([0-9]+))"
		"[\t ]*\\][\t ]*"); 
	if(r3.matches(context->UnparsedXPath, &m)) // [position()>123 | position()<123]
	{
		if(m.getEndPos(1) - m.getStartPos(1) > 0) // [position()>123 | position()<123] group
		{
			if(context->Create)
			{
				Dc1XPathParseContext::ErrorUnallowedExpression(context);
			}
			int n;
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(2), m.getEndPos(2), -1, context, n))
			{
				return false;
			}
			context->StartIndex = n;
			
			if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(3), m.getEndPos(3), -1, context, n))
			{
				return false;
			}
			context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_RANGE : DC1_XPATH_SELECT_POSITION_RANGE;
			context->StopIndex = n;
			context->UnparsedXPath += m.getEndPos(0);
			return true;
		}
	}
	RegularExpression r4(
		"^[\t ]*\\[[\t ]*"
		"position[\t ]*\\([\t ]*\\)[\t ]*<[\t ]*([0-9]+)"
		"[\t ]*\\][\t ]*"); 
	if(r4.matches(context->UnparsedXPath, &m)) // [position()<123]
	{
		if(context->Create)
		{
			Dc1XPathParseContext::ErrorUnallowedExpression(context);
		}
		int n;
		if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(1), m.getEndPos(1), -1, context, n))
		{
			return false;
		}
		context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_LT : DC1_XPATH_SELECT_POSITION_LT;
		context->StartIndex = -1;
		context->StopIndex = n; 
		context->UnparsedXPath += m.getEndPos(0);
		return true;
	}
	RegularExpression r5(
		"^[\t ]*\\[[\t ]*"
		"position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*([0-9]+)"
		"[\t ]*\\][\t ]*"); 
	if(r5.matches(context->UnparsedXPath, &m)) // [position()>123] 
	{
		if(context->Create)
		{
			Dc1XPathParseContext::ErrorUnallowedExpression(context);
		}
		int n;
		if(!Dc1XPathParseContext::ConvertIndex(m.getStartPos(1), m.getEndPos(1), -1, context, n))
		{
			return false;
		}
		context->Operation = context->XsiType ? DC1_XPATH_SELECT_TYPED_POSITION_GT : DC1_XPATH_SELECT_POSITION_GT;
		context->StartIndex = n;
		context->StopIndex = -2; // last()
		context->UnparsedXPath += m.getEndPos(0);
		return true;
	}
	RegularExpression r6(
		"^[\t ]*\\[[\t ]*"
		"position[\t ]*\\([\t ]*\\)[\t ]*>[\t ]*last[\t ]*\\([\t ]*\\)"
		"[\t ]*\\][\t ]*"); 
	if(r6.matches(context->UnparsedXPath, &m)) // [position()>last()]
	{
		if(context->Create)
		{
			context->Operation = DC1_XPATH_FORCE_CREATE;
		}
		context->UnparsedXPath += m.getEndPos(0);
		return true;
	}
#endif // 2.7 vs 2.2

   	return false;
}

/* static */ bool Dc1XPathParseContext::ScanXsiType(Dc1XPathParseContext * context)
{
  assert(context);
	
  Match m; // Matches: [@xsi:type = 'bla'] where 'bla' can be prefixed
	RegularExpression r(
		"^[\t ]*\\[[\t ]*"
//		"\\@xsi\\:type[\t ]*=[\t ]*[\\'\\\"]([0-9a-zA-Z_]+)[\\'\\\"]" // Works in 2.7.0, but not in 3.0.1 anymore
		"@xsi:type[\t ]*=[\t ]*[\'\"]([0-9a-zA-Z_:]+)[\'\"]"
		"[\t ]*\\][\t ]*");

  if(r.matches(context->UnparsedXPath, &m))
	{
		context->Operation = (
			context->Operation == DC1_XPATH_SELECT_ALL ? DC1_XPATH_SELECT_TYPED_ALL
			: context->Operation == DC1_XPATH_SELECT_INDEX ? DC1_XPATH_SELECT_INDEXED_TYPE
			: context->Operation == DC1_XPATH_SELECT_LAST ? DC1_XPATH_SELECT_LAST_TYPE
			: context->Operation == DC1_XPATH_SELECT_POSITION_RANGE ? DC1_XPATH_SELECT_POSITION_RANGE_TYPE
			: context->Operation == DC1_XPATH_SELECT_POSITION_LT ? DC1_XPATH_SELECT_POSITION_LT_TYPE
			: context->Operation == DC1_XPATH_SELECT_POSITION_GT ? DC1_XPATH_SELECT_POSITION_GT_TYPE
			: context->Operation == DC1_XPATH_FORCE_CREATE ? DC1_XPATH_FORCE_CREATE
			: DC1_XPATH_UNKNOWN);
		
		int len = m.getEndPos(1) - m.getStartPos(1) + 1;
		context->XsiType = Dc1Util::allocate(len);
		XMLString::subString(context->XsiType, context->UnparsedXPath, m.getStartPos(1), m.getEndPos(1));
		if(context->XsiType != NULL && XMLString::stringLen(context->XsiType) > 0){
		  if(context->QualifiedXsiType){
			XMLString::release(&context->QualifiedXsiType);
		  }
		  context->QualifiedXsiType = Dc1XPathParseContext::ResolveTypeName(context->XsiType);
		}
		context->UnparsedXPath += m.getEndPos(0);
		return true;
	}
   	return false;
}

/* static */ bool Dc1XPathParseContext::ConvertIndex(int startpos, int endpos, int minval, Dc1XPathParseContext * context, int & n)
{
	assert(context);
	
	int len = endpos - startpos + 1;
	XMLCh * tmp = Dc1Util::allocate(len);
	XMLString::subString(tmp, context->UnparsedXPath, startpos, endpos);
	n = XMLString::parseInt(tmp) - 1; // Convert to 0 based C++ index
	if(n < minval)
	{
		char buf[32] = {0};
		XMLCh * msg = XMLString::transcode("XPath error:\n");
		Dc1Util::CatString(&msg, context->XPath);
		Dc1Util::CatString(&msg, X("\nSorry, invalid index "));
		Dc1Util::CatString(&msg, tmp);
		Dc1Util::CatString(&msg, X("\nValid minimum value: "));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
		_snprintf_s(buf, 32, 31, "%d", minval + 1); // Back to (1 based) XPath notation
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
		sprintf(buf, "%d", minval + 1); // Back to (1 based) XPath notation
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
		sprintf(buf, "%d", minval + 1); // Back to (1 based) XPath notation
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif
		Dc1Util::CatString(&msg, X(buf));
		Dc1Exception e(DC1_ERROR, msg);
		XMLString::release(&msg);
		XMLString::release(&tmp);
		throw e;			
	}
	XMLString::release(&tmp);
	return true;
}


/* static */ Dc1NodePtr Dc1XPathParseContext::CreateNode(Dc1XPathParseContext * context, unsigned int defaulttype)
{
	assert(context);
	
	assert(defaulttype != 1); // Must not be Unkown
	unsigned int id = defaulttype;
	if(context->XsiType)
	{
		id = Dc1Factory::GetTypeIndex(context->QualifiedXsiType);
		if(id == 1) // Oh oh, unknown
		{
			// Is it our Unknown type then ok, we create an Unknown node
			// Otherwise it might be a typo and we issue an error
			XMLCh * unknown = XMLString::transcode(Dc1Factory::GetTypeName(1)); // Our Unknown is always at index 1
			if(XMLString::compareString(context->XsiType, unknown) != 0)
			{
				XMLString::release(&unknown);
				XMLCh * msg = XMLString::transcode("XPath error:\n");
				Dc1Util::CatString(&msg, context->XPath);
				Dc1Util::CatString(&msg, X("\nSorry, unknown xsi:type \""));
				Dc1Util::CatString(&msg, context->XsiType);
				Dc1Util::CatString(&msg, X("\"\nCannot create this type."));
				Dc1Exception e(DC1_ERROR, msg);
				XMLString::release(&msg);
				throw e;
			}
			XMLString::release(&unknown);
		}
	}
	else if(id < 1) // We have an abstract default type
	{
		Dc1XPathParseContext::ErrorAbstractType(context);
	}
	
	Dc1NodePtr node = Dc1Factory::CreateObject(id);	
	if(node == Dc1NodePtr())
	{
		char buf[32] = {0};
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
		_snprintf_s(buf, 32, 31, "%u", defaulttype);
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
		sprintf(buf, "%u", defaulttype);
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
		sprintf(buf, "%u", defaulttype);
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif		
		XMLCh * msg = XMLString::transcode("XPath error:\n");
		Dc1Util::CatString(&msg, context->XPath);
		Dc1Util::CatString(&msg, X("\nSorry, unknown default type with id "));
		Dc1Util::CatString(&msg, X(buf));
		Dc1Util::CatString(&msg, X(".\nCannot create this node."));
		Dc1Exception e(DC1_ERROR, msg);
		XMLString::release(&msg);
		throw e;
	}
	if(context->XsiType)
	{
		node->UseTypeAttribute = true;
	}
	return node;
}

/* static */ void Dc1XPathParseContext::PreProcessSimpleAttribute(Dc1XPathParseContext * context)
{
	Dc1XPathParseContext::PreProcessSimpleElement(context); // All handled in PreProcessSimpleElement();
}

/* static */ void Dc1XPathParseContext::PreProcessSimpleElement(Dc1XPathParseContext * context)
{
	assert(context);
	
	switch(context->Operation)
	{
		case DC1_XPATH_SELECT_ALL: // 
		case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']
			break; // Ok
			
		case DC1_XPATH_SELECT_INDEX: // [i]
		case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type]
		case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
			if(context->Create && context->StartIndex != 0)
			{
				Dc1XPathParseContext::ErrorInvalidIndex(context);
			}
			break;
		
		case DC1_XPATH_SELECT_LAST: // [last()]
		case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type]
		case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			break; // ok
					
		case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
			if(context->Create && (context->StartIndex > -1 || context->StopIndex < 1))
			{
				Dc1XPathParseContext::ErrorInvalidIndex(context);
			}
			break;
		
		case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
		case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
			if(context->Create && context->StopIndex < 1)
			{
				Dc1XPathParseContext::ErrorInvalidIndex(context);
			}
			break;
		
		case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
		case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
			if(context->Create && context->StartIndex > 0)
			{
				Dc1XPathParseContext::ErrorInvalidIndex(context);
			}
			break;

		//case DC1_XPATH_FORCE_CREATE:
		
		default:;
	}	
}

/* static */ bool Dc1XPathParseContext::HandleSimpleAttribute(Dc1XPathParseContext * context, Dc1NodePtr element)
{
	return Dc1XPathParseContext::HandleSimpleElement(context, element); // All handled in HandleSimpleElement();
}

/* static */ bool Dc1XPathParseContext::HandleSimpleElement(Dc1XPathParseContext * context, Dc1NodePtr element)
{
	assert(context);
	
	switch(context->Operation)
	{
		case DC1_XPATH_SELECT_ALL: //
		case DC1_XPATH_SELECT_INDEX: // [i]
		case DC1_XPATH_SELECT_LAST: // [last()]
			return true;
			
		case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']
		case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type]
		case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
		case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type]
		case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			return(element->GetClassId() == Dc1Factory::GetTypeIndex(context->QualifiedXsiType));
					
		case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
		case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
		case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
		case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
			return true; // Already checked in PreProcessSimpleElement

		case DC1_XPATH_FORCE_CREATE: // [position() > last()]
			if(context->Create) // You cant beat last()
			{
				context->UpperBoundary = 1;
				Dc1XPathParseContext::ErrorBoundary(context);
			}
			return false;
			break;
		default:;
	}
	return true;
}

/* static */ bool Dc1XPathParseContext::PreProcessCollection(Dc1XPathParseContext * context, unsigned int collsize, int upperboundary)
{
	assert(context);
	
	context->CollectionSize = collsize;
	context->Id = context->XsiType ? Dc1Factory::GetTypeIndex(context->QualifiedXsiType) : 0;
	context->UpperBoundary = upperboundary;
	
	switch(context->Operation)
	{
		case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
		case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
			context->StartIndex = -1; 
			break;
		case DC1_XPATH_SELECT_POSITION_GT: // [position()>B]
		case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>B xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>B]
			context->StopIndex = collsize; 
			break;
		case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
			if(context->StartIndex >= context->StopIndex) return false; // Logical exclusion
			break;
		default:;
	}
	return true;
}
/* static */ int Dc1XPathParseContext::HandleCollection(Dc1XPathParseContext * context, int i, Dc1NodePtr element, Dc1NodeEnum & result)
{
	assert(context);
	
	if(context->Operation == DC1_XPATH_FORCE_CREATE)
	{
		return context->CollectionSize; // Finished
	}

	++context->ItemCount;
	context->LastItemIndex = i;
	
	switch(context->Operation)
	{
		case DC1_XPATH_SELECT_ALL: // 
		case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']
			if(!context->Id || context->Id == element->GetClassId())
			{
				result.Insert(element);
				context->LastValidItemIndex = i;
				++context->ValidItemCount;
				return ++i;
			}
			break;
		
		case DC1_XPATH_SELECT_INDEX: // [i]
		case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type]
			if(context->ItemCount == context->StartIndex + 1)
			{
				if(!context->Id || context->Id == element->GetClassId())
				{
					result.Insert(element);
					context->LastValidItemIndex = i;
					++context->ValidItemCount;
				}
				return context->CollectionSize; // Finished
			}
			break;
		
		case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
			if(!context->Id || context->Id == element->GetClassId())
			{
				context->LastValidItemIndex = i;
				++context->ValidItemCount;
				if(context->ValidItemCount == context->StartIndex + 1)
				{
					result.Insert(element);
					return context->CollectionSize; // Finished
				}
			}
			break;
		
		case DC1_XPATH_SELECT_LAST: // [last()]
		case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type]
		case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			if(!context->Id || context->Id == element->GetClassId())
			{
				context->LastNode = element; // Evaluated at PostProcessCollection()
				context->LastValidItemIndex = i;
				++context->ValidItemCount;
			}
			break;
					
		case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
		case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
		case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
		case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
		case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
		case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
			if(i > context->StartIndex)
			{
				if(i < context->StopIndex)
				{
					if(!context->Id || context->Id == element->GetClassId())
					{
						result.Insert(element);
						context->LastValidItemIndex = i;
						++context->ValidItemCount;
					}
				} else return context->CollectionSize; // Finished
			}
			break;
			
		default:;	
	}		
	return ++i;
}

/* static */ bool Dc1XPathParseContext::PostProcessElementCollection(Dc1XPathParseContext * context, int & i, Dc1NodeEnum & result)
{
	assert(context);
	
	i = -1; // Dont create an item
	
	if(context->LastNode != Dc1NodePtr())
	{
		result.Insert(context->LastNode);
	}

	if(context->Create && result.Size() == 0)
	{
		switch(context->Operation)
		{
			case DC1_XPATH_SELECT_ALL: // 
			case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']
			case DC1_XPATH_SELECT_LAST: // [last()]
			case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type] <----- FTT append if different type!
			case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			case DC1_XPATH_FORCE_CREATE: // [position()>last()]
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				i = context->CollectionSize; // Append
				return true;
				break;
			
			case DC1_XPATH_SELECT_INDEX: // [i]
 			case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type] <----- FTT insert or append if different type!
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				if(context->StartIndex > context->CollectionSize)
				{
					Dc1XPathParseContext::ErrorNoFillingGaps(context, context->CollectionSize);
				}
				i = context->StartIndex;
#if 0			
				if((context->ItemCount + 1) >= context->StartIndex)
				{
					if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
					{
						Dc1XPathParseContext::ErrorBoundary(context);
					}
					i = context->Operation == Dc1_XPATH_SELECT_INDEXED_TYPE
						? context->LastValidItemIndex >= 0 ? context->LastValidItemIndex : context->CollectionSize // Insert or append
						: context->CollectionSize; // Append
					return true;
				}
				else
				{
					Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ItemCount);
				}
#endif				
				return true;
				break;

			case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				if(context->StartIndex - context->ValidItemCount > 0)
				{
					Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ValidItemCount);
				}
				i = context->CollectionSize; // Append
				return true;
				break;
				
			// FTT: These are not used for Create!
			case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
			case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
			case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
			case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
				break;
			default:;			
		}
	}
	return false; // Don't create a new node
}

/* static */ bool Dc1XPathParseContext::PostProcessSequenceCollection(Dc1XPathParseContext * context, int & i, int & start, int & stop, Dc1NodeEnum & result)
{
	assert(context);
	
	i = -1; // Dont create an additional sequence item
	start = stop = -1; // Dont shift elements
	
	if(context->LastNode != Dc1NodePtr())
	{
		result.Insert(context->LastNode);
	}
	
	if(context->Create && result.Size() == 0)
	{
		switch(context->Operation)
		{
			case DC1_XPATH_SELECT_ALL: // 
			case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']; Dont care about position
				if(context->FreeIndexList.size() == 0)
				{
					if(context->UpperBoundary > -1 && i >= context->UpperBoundary)
					{
						Dc1XPathParseContext::ErrorBoundary(context);
					}
					i = context->CollectionSize; // Create and append another sequence item
				}
				else
				{
					start = stop = context->FreeIndexList.elementAt(0);
				}
				return true; // Something to do
				break;				
			
			case DC1_XPATH_SELECT_LAST: // [last()]
			case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type] <----- FTT append if different type!
			case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			case DC1_XPATH_FORCE_CREATE: // [position()>last()]
				if(context->FreeIndexList.size() == 0)
				{
					if(context->UpperBoundary > -1 && i >= context->UpperBoundary)
					{
						Dc1XPathParseContext::ErrorBoundary(context);
					}
					i = context->CollectionSize; // Create and append another sequence item
				}
				else
				{
					start = stop = context->FreeIndexList.elementAt(0);
				}				
				return true; // Something to do
				break;
	// TODO merge the two case sections!
	
			case DC1_XPATH_SELECT_INDEX: // [i]
 			case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type] <----- FTT insert or append if different type!
				if(context->FreeIndexList.size() == 0) // We have to create and append another sequence item.
				{
					if(context->UpperBoundary > -1 && i >= context->UpperBoundary)
					{
						Dc1XPathParseContext::ErrorBoundary(context);
					}
					if(context->StartIndex - context->ItemCount > 0)
					{
						Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ItemCount);
					}
					i = context->StartIndex; // Insert or append another sequence item
				}
				else // Find gap and reorganise existing elements
				{
					if(context->StartIndex - context->ItemCount > 0)
					{
						Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ItemCount);
					}
					start = stop = context->FreeIndexList.elementAt(0);
					if(context->LastItemIndex > -1)
					{
						int j, k = -1, freeindex;
						for(j = 0; j < (int)context->FreeIndexList.size(); ++j)
						{
							freeindex = context->FreeIndexList.elementAt(j);
							if(freeindex < context->LastItemIndex)
							{
								k = freeindex;
							}
							else
							{
								if(k < 0)
								{
									k = freeindex;
								}
								break;
							}
						}
						start = k;
						stop = context->LastItemIndex;
					}
				}
				return true;
				break;
 			
			case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
				if(context->FreeIndexList.size() == 0) // We have to create and append another sequence item.
				{
					if(context->UpperBoundary > -1 && i >= context->UpperBoundary)
					{
						Dc1XPathParseContext::ErrorBoundary(context);
					}
					if(context->StartIndex - context->ValidItemCount > 0)
					{
						Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ValidItemCount);
					}
					i = context->StartIndex; // Insert or append another choice item
				}
				else // Find gap and reorganise existing elements
				{
					if(context->StartIndex - context->ValidItemCount > 0)
					{
						Dc1XPathParseContext::ErrorNoFillingGaps(context, context->ValidItemCount);
					}
					start = stop = context->FreeIndexList.elementAt(0);
					if(context->LastValidItemIndex > -1)
					{
						int j, k = -1, freeindex;
						for(j = 0; j < (int)context->FreeIndexList.size(); ++j)
						{
							freeindex = context->FreeIndexList.elementAt(j);
							if(freeindex < context->LastValidItemIndex)
							{
								k = freeindex;
							}
							else
							{
								if(k < 0)
								{
									k = freeindex;
								}
								break;
							}
						}
						start = k;
						stop = context->LastValidItemIndex;
					}
				}
				return true;
				break;
				
			// FTT: These are not used for Create!
			case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
			case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
			case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
			case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
				break;
			default:;			
		}
	}
	return false;
}

/* static */ bool Dc1XPathParseContext::PostProcessChoiceCollection(Dc1XPathParseContext * context, int & i, Dc1NodeEnum & result)
{
	assert(context);
	
	i = -1; // Dont create an additional sequence item
	
	if(context->LastNode != Dc1NodePtr())
	{
		result.Insert(context->LastNode);
	}
	
	if(context->Create && result.Size() == 0)
	{
		switch(context->Operation)
		{
			case DC1_XPATH_SELECT_ALL: // 
			case DC1_XPATH_SELECT_TYPED_ALL: // [@xsi:type='bla']; Dont care about position
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				i = context->CollectionSize; // Create and append another sequence item
				return true; // Something to do
				break;				
			
			case DC1_XPATH_SELECT_LAST: // [last()]
			case DC1_XPATH_SELECT_LAST_TYPE: // [last() xsi:type] <----- FTT append if different type!
			case DC1_XPATH_SELECT_TYPED_LAST: // [xsi:type last()]
			case DC1_XPATH_FORCE_CREATE: // [position()>last()]
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				i = context->CollectionSize; // Create and append another choice item
				return true; // Something to do
				break;
	// TODO merge the two case sections!
	
			case DC1_XPATH_SELECT_INDEX: // [i]
 			case DC1_XPATH_SELECT_INDEXED_TYPE: // [i @xsi:type] <----- FTT insert or append if different type!
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				if(context->StartIndex > context->CollectionSize)
				{
					Dc1XPathParseContext::ErrorNoFillingGaps(context, context->CollectionSize);
				}
				i = context->StartIndex; // Insert or append another choice item
				return true;
				break;
 			
			case DC1_XPATH_SELECT_TYPED_INDEX: // [@xsi:type i]
				if(context->UpperBoundary > -1 && context->CollectionSize >= context->UpperBoundary)
				{
					Dc1XPathParseContext::ErrorBoundary(context);
				}
				if(context->StartIndex > context->CollectionSize)
				{
					Dc1XPathParseContext::ErrorNoFillingGaps(context, context->CollectionSize);
				}
				i = context->CollectionSize;
				return true;
				break;
				
			// FTT: These are not used for Create!
			case DC1_XPATH_SELECT_POSITION_RANGE: // [position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_RANGE_TYPE: // [position()>A | position()<B xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_RANGE: // [xsi:type position()>A | position()<B]
			case DC1_XPATH_SELECT_POSITION_LT: // [position()<A]
			case DC1_XPATH_SELECT_POSITION_LT_TYPE: // [position()<A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_LT: // [xsi:type position()<A]
			case DC1_XPATH_SELECT_POSITION_GT: // [position()>A]
			case DC1_XPATH_SELECT_POSITION_GT_TYPE: // [position()>A xsi:type]
			case DC1_XPATH_SELECT_TYPED_POSITION_GT: // [xsi:type position()>A]
				break;
			default:;			
		}
	}
	return false;
}

/* static */ Dc1NodeEnum Dc1XPathParseContext::Root(Dc1XPathParseContext * context, const Dc1NodePtr node, Dc1ClientID client)
{
	assert(context);
	
	Dc1NodeEnum result;
	const Dc1NodePtr empty = Dc1NodePtr();
	if(node == empty) return result;
	
	unsigned int rootid = Dc1Factory::GetTypeIndex(X(":"));
	
	Dc1NodePtr p = node;
	while(p != empty)
	{
		p = p->GetParent();
		if(p != empty && p->GetClassId() == rootid)
		{
			if(XMLString::stringLen(context->UnparsedXPath) > 0)
			{
				Dc1XPathCreation flag = context->Create ? DC1_FORCECREATE : DC1_NOCREATE;
				result = p->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, context);
			}
			else // Just "/"
			{
				context->Found = true;
				result.Insert(p);
			}
			break;
		}
	}
	return result;
}

/* static */ Dc1NodeEnum Dc1XPathParseContext::Up(Dc1XPathParseContext * context, const Dc1NodePtr node, Dc1ClientID client)
{
	/* FTT this source is copied verbatim from Node::UpwardWithXPath() */
	/* TODO: Replace string stuff with type ids */
	/* TODO: Refactor this */
	Dc1NodeEnum result;
	const Dc1NodePtr empty = Dc1NodePtr();
	if(node == empty) return result;

	Dc1NodePtr ancestor = node->GetParent();
	if(ancestor)
	{
		Dc1NodeCollectionPtr ancestor_as_collection = ancestor;
		while (ancestor) 
		{
			// Look upward for a "meaningful" node to continue parsing.
			if (!ancestor_as_collection
				&& (!strstr(ancestor->GetTypeName(), "LocalType") // FTT This is bogus...
				// TODO: auf ContentName pruefen? -> mit Testcases abklaeren
				|| strstr(ancestor->GetTypeName(), ""))
//				&& ancestor->GetContentName()
			)
				break;
			ancestor = ancestor->GetParent();
			ancestor_as_collection = ancestor;
		}
	}
	if(ancestor != empty)
	{
		if(XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1XPathCreation flag = context->Create ? DC1_FORCECREATE : DC1_NOCREATE;
			result = ancestor->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, context);
		}
		else // Just ".."
		{
			context->Found = true;
			result.Insert(ancestor);
		}
	}
	return result;
}



/* static */ void Dc1XPathParseContext::ErrorBoundary(Dc1XPathParseContext * context)
{
	assert(context);
	
	char buf[32] = {0};
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nCannot append/insert element. Upper boundary ("));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	_snprintf_s(buf, 32, 31, "%d", context->UpperBoundary);
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->UpperBoundary);
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->UpperBoundary);
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif		
	Dc1Util::CatString(&msg, X(buf));
	Dc1Util::CatString(&msg, X(") is reached."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorUnallowedExpression(Dc1XPathParseContext * context)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, this expression is allowed only in Get() -without- OrCreate() calls."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;			
}

/* static */ void Dc1XPathParseContext::ErrorAbstractType(Dc1XPathParseContext * context)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, the type of \""));
	Dc1Util::CatString(&msg, context->ElementName);
	Dc1Util::CatString(&msg, X("\" is abstract and needs @xsi:type.\nCannot create this node."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorInvalidIndex(Dc1XPathParseContext * context)
{
	assert(context);
	
	char buf[32] = {0};
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, invalid index "));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	_snprintf_s(buf, 32, 31, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif	
	Dc1Util::CatString(&msg, X(buf));
	Dc1Util::CatString(&msg, X(".\nCannot create node."));
	Dc1Exception e(DC1_ERROR, msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorInvalidEmptyItem(Dc1XPathParseContext * context, int i)
{
	assert(context);
	
	char buf[32] = {0};
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nEmpty node in collection at C++ index "));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	_snprintf_s(buf, 32, 31, "%d", i); // We want clean collections with always non empty items inside!
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", i); // We want clean collections with always non empty items inside!
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", i); // We want clean collections with always non empty items inside!
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif	
	Dc1Util::CatString(&msg, X(buf));
	Dc1Util::CatString(&msg, X("detected.\nIn order to keep collections clean, empty nodes are not allowed."));
	Dc1Exception e(DC1_ERROR, msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorNoFillingGaps(Dc1XPathParseContext * context, unsigned int lastvalidindex)
{
	assert(context);
	
	char buf[32] = {0};
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, XPath index "));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	_snprintf_s(buf, 32, 31, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%d", context->StartIndex + 1); // Switch back to (1 based) XPath indexes 
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif	
	Dc1Util::CatString(&msg, X(buf));
	Dc1Util::CatString(&msg, X(" is too high.\nCurrently there are "));
#if defined(XML_WIN32) || defined(WIN32) // Xerces 3.0 doesn't know XML_WIN32
	_snprintf_s(buf, 32, 31, "%u", lastvalidindex);
#elif defined(__APPLE__) || defined(APPLE) || defined(_APPLE) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%u", lastvalidindex);
#elif defined(XML_LINUX) || defined(LINUX) || defined(linux) // Xerces 3.0 doesn't know XML_LINUX
	sprintf(buf, "%u", lastvalidindex);
#else // XML_LINUX ...
#	error "TODO: Add platform specific headers for xpathparsecontext."
#endif	
	Dc1Util::CatString(&msg, X(buf));
	Dc1Util::CatString(&msg, X(" elements available for this request. Filling gaps is not allowed (only appending).\nCannot create this node."));
	// Give the user a change to correct his params...
	Dc1MissingElementException e(DC1_ERROR, msg, lastvalidindex);
	Dc1XPathParseContext::Reset(context);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorCannotSetAttribute(Dc1XPathParseContext * context)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, cannot set attribute \""));
	Dc1Util::CatString(&msg, context->ElementName);
	if(context->XsiType)
	{
		Dc1Util::CatString(&msg, X("\" with type \""));
		Dc1Util::CatString(&msg, context->XsiType);
		Dc1Util::CatString(&msg, X("\"."));
	}
	else
	{
		Dc1Util::CatString(&msg, X("\" with default type."));
	}
	Dc1Exception e(DC1_ERROR, msg);
	Dc1XPathParseContext::Reset(context);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorCannotSetElement(Dc1XPathParseContext * context)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, cannot set element \""));
	Dc1Util::CatString(&msg, context->ElementName);
	if(context->XsiType)
	{
		Dc1Util::CatString(&msg, X("\" with type \""));
		Dc1Util::CatString(&msg, context->XsiType);
		Dc1Util::CatString(&msg, X("\"."));
	}
	else
	{
		Dc1Util::CatString(&msg, X("\" with default type."));
	}
	Dc1Exception e(DC1_ERROR, msg);
	Dc1XPathParseContext::Reset(context);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorUnknownSubElement(Dc1XPathParseContext * context, const char * type)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, \""));
	Dc1Util::CatString(&msg, X(type));
	Dc1Util::CatString(&msg, X("\" does not contain element \""));
	if(context->ElementName)
		Dc1Util::CatString(&msg, context->ElementName);
	Dc1Util::CatString(&msg, X("\" (or it is not of type \"Dc1NodePtr\")."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorNoSubElement(Dc1XPathParseContext * context, const char * type)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, \""));
	Dc1Util::CatString(&msg, X(type));
	Dc1Util::CatString(&msg, X("\" does not contain any element of type \"Dc1NodePtr\"." ));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorUnsupportedType(Dc1XPathParseContext * context, const char * type)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, cannot deal with element of type \""));
	Dc1Util::CatString(&msg, X(type));
	Dc1Util::CatString(&msg, X("\"."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorUnsupportedAttributeType(Dc1XPathParseContext * context, const char * type)
{
	assert(context);
	
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	Dc1Util::CatString(&msg, context->XPath);
	Dc1Util::CatString(&msg, X("\nSorry, XPATH expressions with simple attributes of type \""));
	Dc1Util::CatString(&msg, X(type));
	Dc1Util::CatString(&msg, X("\" are not supported yet."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorWrongCaller(const XMLCh * xpath)
{
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	if(xpath)
	{
		Dc1Util::CatString(&msg, xpath);
		Dc1Util::CatString(&msg, X("\n"));
	}
	Dc1Util::CatString(&msg, X("Sorry, calling class is not allowed to query XPaths (element classes only)!"));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}

/* static */ void Dc1XPathParseContext::ErrorNotImplemented(const XMLCh * xpath, const char * type)
{
	XMLCh * msg = XMLString::transcode("XPath error:\n");
	if(xpath)
	{
		Dc1Util::CatString(&msg, xpath);
		Dc1Util::CatString(&msg, X("\n"));
	}
	Dc1Util::CatString(&msg, X("Sorry, XPath queries for class "));
	Dc1Util::CatString(&msg, X(type));
	Dc1Util::CatString(&msg, X(" is currently not supported."));
	Dc1Exception e(DC1_ERROR, msg);
	XMLString::release(&msg);
	throw e;
}


/* static */ bool Dc1XPathParseContext::ContentFromString(Dc1Node * node, const XMLCh * const txt)
{
	if(!(node && txt)) return false;
	
	XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementation * impl;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * root;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * current;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMText * text;
	
	
	impl = XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationRegistry::getDOMImplementation(X("Core"));
	if(!impl) return false;
	
	try
	{
		doc = impl->createDocument(0		  // root element namespace URI.
			, X("dummy") // root element name
			, 0);	   // document type object (DTD).

		root = doc->getDocumentElement();
		parent = doc->createElement(X("parent"));
		root->appendChild(parent);
		text = doc->createTextNode(txt);
		parent->appendChild(text);
		bool res = node->Deserialize(doc, parent, &current);			
		doc->release();
		return res;
	}
	catch(...)
	{
	}			
	return false;
}

/* static */ XMLCh * Dc1XPathParseContext::ContentToString(const Dc1Node * node)
{
	XMLCh * res = (XMLCh*)NULL;
	if(!(node)) return res;
	
	XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementation * impl;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * root;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent;
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * newelement;
	
	
	impl = XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationRegistry::getDOMImplementation(X("Core"));
	if(!impl) return res;
	
	try
	{
		doc = impl->createDocument(0		  // root element namespace URI.
			, X("dummy") // root element name
			, 0);	   // document type object (DTD).

		root = doc->getDocumentElement();
		parent = doc->createElement(X("parent"));
		newelement = doc->createElement(X("element"));
		root->appendChild(parent);
		(const_cast< Dc1Node* >(node))->Serialize(doc, parent, &newelement);
		res = Dc1Util::GetElementText(newelement);	// Replication is done within GetElementText()
		doc->release();
		return res;
	}
	catch(...)
	{
	}			
	return res;
}

/* static */  XMLCh * Dc1XPathParseContext::ResolveTypeName(const XMLCh * xsitype)
{
  if(!xsitype) return NULL;

  XMLCh * result = NULL;
  
  XMLCh * prefix = Dc1Util::Prefix(xsitype); // Must be deallocated
  if(prefix && XMLString::stringLen(prefix) > 0) {
	// FTT NEW:
	const XMLCh * nsuri = Dc1Factory::GetNamespaceUri(prefix);// Do not free this string!
	const XMLCh * localname = Dc1Util::LocalName(xsitype);  // Do not free this string!
	result = Dc1Util::QualifiedName(nsuri, localname);
  } else{ // Use default namespace uri
	result = Dc1Util::CatString(X(":"), xsitype);
  }
  if(prefix) Dc1Util::deallocate(&prefix);	
  
  return result;
}
