
// Based on Unknown_cpp.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include "Dc1Unknown.h"

Dc1Unknown::Dc1Unknown()
{
	Init();
}

Dc1Unknown::~Dc1Unknown()
{
	Cleanup();
}

void Dc1Unknown::Init()
{
	XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementation * impl = XERCES_CPP_NAMESPACE_QUALIFIER DOMImplementationRegistry::getDOMImplementation(X("Core"));
	m_Document = impl->createDocument(0,X("dummyroot"),0);
	m_DocumentFragment = m_Document->createDocumentFragment();
}

void Dc1Unknown::Cleanup()
{
	if(m_DocumentFragment != NULL)
	{
		m_DocumentFragment->release();
		m_DocumentFragment = (XERCES_CPP_NAMESPACE_QUALIFIER DOMDocumentFragment *)NULL;
	}
	if(m_Document != NULL)
	{
		m_Document->release();
		m_Document = (XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *)NULL;
	}
}

void Dc1Unknown::DeepCopy(const Dc1NodePtr &original)
{
	const Dc1Unknown* tmp = dynamic_cast<const Dc1Unknown *> (original.Ptr());
	if (!tmp) return; // EXIT: wrong argument
	DOMNode * unknownxml = (DOMNode *)tmp->GetUnknownXml();
	if(unknownxml != NULL)
	{
		if(m_DocumentFragment->getFirstChild() != NULL)
		{
			m_DocumentFragment->getFirstChild()->release();
		}

		bool deep = true;
		// create clone with m_Document as responsible xml doc
		DOMNode * clone = m_Document->importNode
			(unknownxml, deep);
		m_DocumentFragment->appendChild(clone);
	}
}

const DOMElement * Dc1Unknown::GetUnknownXml() const
{
	// User must clone the result
	if(m_DocumentFragment != NULL)
	{
		return (DOMElement *)m_DocumentFragment->getFirstChild();
	}
	return NULL;
}

void Dc1Unknown::SetUnknownXml(const XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * unknownxml)
{
	// We create a clone of the data
	if(m_DocumentFragment->getFirstChild() != NULL)
	{
		m_DocumentFragment->getFirstChild()->release();
	}
	if(unknownxml != NULL)
	{
		bool deep = true;
		// create clone with m_Document as responsible xml doc
		XERCES_CPP_NAMESPACE_QUALIFIER DOMNode * clone = m_Document->importNode
			((XERCES_CPP_NAMESPACE_QUALIFIER DOMNode *)unknownxml, deep);
		m_DocumentFragment->appendChild(clone);
	}
}

void Dc1Unknown::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument *doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** /*newElem*/)
{
	// doc MUST be valid!

	if(parent == NULL)
	{
		parent = doc->getDocumentElement();
	}

	if(m_DocumentFragment->getFirstChild() != NULL)
	{
		bool deep = true;
		// create clone with doc as responsible xml doc
		XERCES_CPP_NAMESPACE_QUALIFIER DOMNode * clone = doc->importNode
			(m_DocumentFragment->getFirstChild(), deep);
		parent->appendChild(clone);
	}
}

bool Dc1Unknown::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * /*doc*/, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** /*current*/)
{
	if(parent == NULL)
	{
		return false;
	}
	if(m_DocumentFragment->getFirstChild() != NULL)
	{
		m_DocumentFragment->getFirstChild()->release();
	}
	bool deep = true;
	// create clone with m_Document as responsible xml doc
	XERCES_CPP_NAMESPACE_QUALIFIER DOMNode * clone = m_Document->importNode
		(parent, deep);
	m_DocumentFragment->appendChild(clone);
	return true;
}

Dc1NodeEnum Dc1Unknown::GetAllChildElements() const {
	Dc1NodeEnum result;
	return result;
}
