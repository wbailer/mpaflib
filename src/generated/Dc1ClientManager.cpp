
/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Based on ClientManager_cpp.template

#include "Dc1ClientManager.h"

#include "Dc1NodeLockNotification.h"
#include "Dc1NodeUpdateNotification.h"

Dc1ClientID Dc1ClientManager::_maxID = 0;
ClientMap Dc1ClientManager::_clientMap(20);

LockQueue Dc1ClientManager::_lockQueue(20);

Dc1ListenerList Dc1ClientManager::_lockRequested;
Dc1ListenerList Dc1ClientManager::_lockGranted;
Dc1ListenerList Dc1ClientManager::_unlocked;
Dc1ListenerList Dc1ClientManager::_added;
Dc1ListenerList Dc1ClientManager::_inserted;
Dc1ListenerList Dc1ClientManager::_replaced;

bool Dc1ClientManager::_lockingEnabled = false;
bool Dc1ClientManager::_notificationsEnabled = false;

Dc1ListenerList::Dc1ListenerList() :
	Dc1ValueVectorOf<Dc1ClientRNodePair>(20)
{}

Dc1ClientList Dc1ListenerList::GetClientsToBeNotified(Dc1NodePtr node) {
	Dc1ClientList toBeNotified(20);

	int nElems = size();
	int i;
	for (i=0;i<nElems;i++) {
		Dc1ClientRNodePair& crnPair = elementAt(i);
		// check if node itself is affected
		bool notify = false;
		if (crnPair.getValue().node==node) notify = true;
		// check if parent is affected if registration is recursive
		if ((!notify) && (crnPair.getValue().recursive)) {
			Dc1NodePtr tmpNode = node->GetParent();
			while (tmpNode!=NULL) {
				if (tmpNode==crnPair.getValue().node) {
					notify = true;
					break;
				}
				else tmpNode = tmpNode->GetParent();
			}
		}

		// add to list
		if (notify) toBeNotified.addElement(crnPair.getKey());
	}
	
	return toBeNotified;
}


Dc1ClientID Dc1ClientManager::GetNewID(Dc1NotificationListener* listener) {
	Dc1ClientID newID = ++_maxID;

	ClientListenerPair clPair(newID,listener);
	Dc1ClientManager::_clientMap.addElement(clPair);

	return newID;
}

Dc1NotificationListener* Dc1ClientManager::GetListener(Dc1ClientID id) {
	int nElems = Dc1ClientManager::_clientMap.size();
	int i;
	for (i=0;i<nElems;i++) {
		ClientListenerPair& clPair = _clientMap.elementAt(i);
		if (clPair.getKey()==id) return clPair.getValue();
	}
	return NULL;
}


void Dc1ClientManager::Unregister(Dc1ClientID id) {
	int nElems = Dc1ClientManager::_clientMap.size();
	int i;
	for (i=0;i<nElems;i++) {
		ClientListenerPair& clPair = _clientMap.elementAt(i);
		if (clPair.getKey()==id) {
			_clientMap.removeElementAt(i);
			return;
		}
	}	
}

void Dc1ClientManager::QueueForLock(Dc1NodePtr sender, Dc1ClientID client, bool /*forAdd*/) {
	// check if already queued
	int nElems = Dc1ClientManager::_lockQueue.size();
	int i;
	bool found = false;

	for (i=0;i<nElems;i++) {
		ClientNodePair& cnPair = _lockQueue.elementAt(i);
		if (cnPair.getKey()==client) {
			found = true;
			break;
		}
	}	

	// if not found, queue
	if (!found) {
		ClientNodePair cnPair(client,sender);
		_lockQueue.addElement(cnPair);
	}

}

bool Dc1ClientManager::SendLockRequestNotification(Dc1NodePtr sender, Dc1ClientID client, bool forAdd, bool notify) {
	if (!_notificationsEnabled) return true;
	
	// bool queued = false;
	
	// create and send notification about request

	Dc1NodeLockNotification lockNotif;
	lockNotif.action = DC1_NA_LOCK_REQUESTED;
	lockNotif.clientId = client;
	lockNotif.node = sender;
	lockNotif.lock4add = forAdd;
	lockNotif.notifyLater = notify;
	lockNotif.result = false;

	bool grantLock = true;

	Dc1ClientList toBeNotified = _lockRequested.GetClientsToBeNotified(sender);
	unsigned int i;
	for (i=0;i<toBeNotified.size();i++) {
		Dc1NotificationListener* listener = GetListener(toBeNotified.elementAt(i));
		if (listener) {
			listener->Notify(lockNotif);
			if (!lockNotif.result) grantLock = false;
		}
	}
	
	return grantLock;
}

void Dc1ClientManager::SendLockNotification(Dc1NodePtr sender, Dc1ClientID client, bool forAdd) {
	if (!_notificationsEnabled) return;

	Dc1NodeLockNotification lockNotif;
	lockNotif.action = DC1_NA_LOCK_GRANTED;
	lockNotif.clientId = client;
	lockNotif.node = sender;
	lockNotif.lock4add = forAdd;

	Dc1ClientList toBeNotified = _lockGranted.GetClientsToBeNotified(sender);
	unsigned int i;
	for (i=0;i<toBeNotified.size();i++) {
		Dc1NotificationListener* listener = GetListener(toBeNotified.elementAt(i));
		if (listener) {
			listener->Notify(lockNotif);
		}
	}
}


void Dc1ClientManager::SendUnlockNotification(Dc1NodePtr sender) {
	if (!_notificationsEnabled) return;

	// first grant lock to next in queue
	if (_lockQueue.size()>0) {
		// check if the client is already registered for notification,
		// if not, register it temporarily
		bool registerTemp = true;

		int nElems = _lockGranted.size();
		int i;
		for (i=0;i<nElems;i++) {
			Dc1ClientRNodePair& crnPair = _lockGranted.elementAt(i);
			if (crnPair.getKey()==_lockQueue.elementAt(0).getKey()) {
				registerTemp = false;
				break;
			}	
		}

		if (registerTemp) Register(_lockQueue.elementAt(0).getKey(),DC1_NA_LOCK_GRANTED,_lockQueue.elementAt(0).getValue(),false);

		// request the lock at the node
		_lockQueue.elementAt(0).getValue()->Lock(_lockQueue.elementAt(0).getKey(),false);
		
		if (registerTemp) Unregister(_lockQueue.elementAt(0).getKey(),DC1_NA_LOCK_GRANTED,_lockQueue.elementAt(0).getValue());
		
		// remove from queue
		_lockQueue.removeElementAt(0);
	}

	// then inform others about unlock
	Dc1NodeLockNotification lockNotif;
	lockNotif.action = DC1_NA_UNLOCK;
	lockNotif.node = sender;

	Dc1ClientList toBeNotified = _unlocked.GetClientsToBeNotified(sender);
	unsigned int i;
	for (i=0;i<toBeNotified.size();i++) {
		Dc1NotificationListener* listener = GetListener(toBeNotified.elementAt(i));
		if (listener) {
			listener->Notify(lockNotif);
		}
	}

}
	
void Dc1ClientManager::SendUpdateNotification(Dc1NodePtr sender, Dc1NotificationAction action, Dc1ClientID client, Dc1NodePtr second, int index) {
	if (!_notificationsEnabled) return;
	
	Dc1NodeUpdateNotification updateNotif;
	updateNotif.action = action;
	updateNotif.clientId = client;
	updateNotif.node = sender;
	updateNotif.newNode = second;
	updateNotif.index = index;

	Dc1ListenerList& ll = _added;
	
	switch (action) {
	case DC1_NA_NODE_ADDED: ll = Dc1ClientManager::_added; break;
	case DC1_NA_NODE_INSERTED: ll = Dc1ClientManager::_inserted; break;
	case DC1_NA_NODE_UPDATED: ll = Dc1ClientManager::_replaced; break;
	default:;
	};


	Dc1ClientList toBeNotified = ll.GetClientsToBeNotified(sender);
	unsigned int i;
	for (i=0;i<toBeNotified.size();i++) {
		Dc1NotificationListener* listener = GetListener(toBeNotified.elementAt(i));
		if (listener) {
			listener->Notify(updateNotif);
		}
	}
}


void Dc1ClientManager::Register(Dc1ClientID id, Dc1NotificationAction action, Dc1NodePtr node, bool recursive) {
	Dc1ListenerList& ll = _lockRequested;
	
	switch (action) {
	case DC1_NA_LOCK_REQUESTED: ll = Dc1ClientManager::_lockRequested; break;
	case DC1_NA_LOCK_GRANTED: ll = Dc1ClientManager::_lockGranted; break;
	case DC1_NA_UNLOCK: ll = Dc1ClientManager::_unlocked; break;
	case DC1_NA_NODE_ADDED: ll = Dc1ClientManager::_added; break;
	case DC1_NA_NODE_INSERTED: ll = Dc1ClientManager::_inserted; break;
	case DC1_NA_NODE_UPDATED: ll = Dc1ClientManager::_replaced; break;
	};

	Subtree st;
	st.node = node;
	st.recursive = recursive;
	Dc1ClientRNodePair crnPair(id,st);

	ll.addElement(crnPair);
}
	
void Dc1ClientManager::Unregister(Dc1ClientID id, Dc1NotificationAction action, Dc1NodePtr node) {
	Dc1ListenerList& ll = _lockRequested;
	
	switch (action) {
	case DC1_NA_LOCK_REQUESTED: ll = Dc1ClientManager::_lockRequested; break;
	case DC1_NA_LOCK_GRANTED: ll = Dc1ClientManager::_lockGranted; break;
	case DC1_NA_UNLOCK: ll = Dc1ClientManager::_unlocked; break;
	case DC1_NA_NODE_ADDED: ll = Dc1ClientManager::_added; break;
	case DC1_NA_NODE_INSERTED: ll = Dc1ClientManager::_inserted; break;
	case DC1_NA_NODE_UPDATED: ll = Dc1ClientManager::_replaced; break;
	};

	int nElems = ll.size();
	int i;
	for (i=0;i<nElems;i++) {
		Dc1ClientRNodePair& crnPair = ll.elementAt(i);
		if ((crnPair.getKey()==id) && (crnPair.getValue().node==node)) {
			ll.removeElementAt(i);
			return;
		}
	}

}

// FTT moved from headerfile to allows extension DLLs

void Dc1ClientManager::SetNotificationsEnabled(bool enabled) { _notificationsEnabled = enabled; }
void Dc1ClientManager::SetLockingEnabled(bool enabled) { _lockingEnabled = enabled; }
bool Dc1ClientManager::IsNotificationsEnabled() { return _notificationsEnabled; };
bool Dc1ClientManager::IsLockingEnabled() { return _lockingEnabled; };

