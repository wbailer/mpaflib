
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtImplInclude.h


#include "Mp7JrsLexiconType.h"
#include "Mp7JrsWordLexiconType_Token_LocalType.h"
#include "Mp7JrsWordLexiconType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsWordLexiconType::IMp7JrsWordLexiconType()
{

// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtPropInit.h

}

IMp7JrsWordLexiconType::~IMp7JrsWordLexiconType()
{
// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtPropCleanup.h

}

Mp7JrsWordLexiconType::Mp7JrsWordLexiconType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsWordLexiconType::~Mp7JrsWordLexiconType()
{
	Cleanup();
}

void Mp7JrsWordLexiconType::Init()
{
	// Init base
	m_Base = CreateLexiconType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_phoneticAlphabet = Mp7JrsphoneticAlphabetType::UninitializedEnumeration;
	m_phoneticAlphabet_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Token = Mp7JrsWordLexiconType_Token_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtMyPropInit.h

}

void Mp7JrsWordLexiconType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Token);
}

void Mp7JrsWordLexiconType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use WordLexiconTypePtr, since we
	// might need GetBase(), which isn't defined in IWordLexiconType
	const Dc1Ptr< Mp7JrsWordLexiconType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsLexiconPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsWordLexiconType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistphoneticAlphabet())
	{
		this->SetphoneticAlphabet(tmp->GetphoneticAlphabet());
	}
	else
	{
		InvalidatephoneticAlphabet();
	}
	}
		// Dc1Factory::DeleteObject(m_Token);
		this->SetToken(Dc1Factory::CloneObject(tmp->GetToken()));
}

Dc1NodePtr Mp7JrsWordLexiconType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsWordLexiconType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsLexiconType > Mp7JrsWordLexiconType::GetBase() const
{
	return m_Base;
}

Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsWordLexiconType::GetphoneticAlphabet() const
{
	return m_phoneticAlphabet;
}

bool Mp7JrsWordLexiconType::ExistphoneticAlphabet() const
{
	return m_phoneticAlphabet_Exist;
}
void Mp7JrsWordLexiconType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordLexiconType::SetphoneticAlphabet().");
	}
	m_phoneticAlphabet = item;
	m_phoneticAlphabet_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordLexiconType::InvalidatephoneticAlphabet()
{
	m_phoneticAlphabet_Exist = false;
}
Mp7JrsWordLexiconType_Token_LocalPtr Mp7JrsWordLexiconType::GetToken() const
{
		return m_Token;
}

void Mp7JrsWordLexiconType::SetToken(const Mp7JrsWordLexiconType_Token_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordLexiconType::SetToken().");
	}
	if (m_Token != item)
	{
		// Dc1Factory::DeleteObject(m_Token);
		m_Token = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Token) m_Token->SetParent(m_myself.getPointer());
		if (m_Token && m_Token->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Token->UseTypeAttribute = true;
		}
		if(m_Token != Mp7JrsWordLexiconType_Token_LocalPtr())
		{
			m_Token->SetContentName(XMLString::transcode("Token"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsWordLexiconType::GetnumOfOriginalEntries() const
{
	return GetBase()->GetnumOfOriginalEntries();
}

bool Mp7JrsWordLexiconType::ExistnumOfOriginalEntries() const
{
	return GetBase()->ExistnumOfOriginalEntries();
}
void Mp7JrsWordLexiconType::SetnumOfOriginalEntries(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordLexiconType::SetnumOfOriginalEntries().");
	}
	GetBase()->SetnumOfOriginalEntries(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordLexiconType::InvalidatenumOfOriginalEntries()
{
	GetBase()->InvalidatenumOfOriginalEntries();
}
XMLCh * Mp7JrsWordLexiconType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsWordLexiconType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsWordLexiconType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordLexiconType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordLexiconType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsWordLexiconType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticAlphabet")) == 0)
	{
		// phoneticAlphabet is simple attribute phoneticAlphabetType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsphoneticAlphabetType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Token")) == 0)
	{
		// Token is simple element WordLexiconType_Token_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetToken()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType_Token_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsWordLexiconType_Token_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetToken(p, client);
					if((p = GetToken()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for WordLexiconType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "WordLexiconType");
	}
	return result;
}

XMLCh * Mp7JrsWordLexiconType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsWordLexiconType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsWordLexiconType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("WordLexiconType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneticAlphabet_Exist)
	{
	// Enumeration
	if(m_phoneticAlphabet != Mp7JrsphoneticAlphabetType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsphoneticAlphabetType::ToText(m_phoneticAlphabet);
		element->setAttributeNS(X(""), X("phoneticAlphabet"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Token != Mp7JrsWordLexiconType_Token_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Token->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Token"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Token->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsWordLexiconType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticAlphabet")))
	{
		this->SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Parse(parent->getAttribute(X("phoneticAlphabet"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsLexiconType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Token"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Token")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateWordLexiconType_Token_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetToken(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsWordLexiconType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Token")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateWordLexiconType_Token_LocalType; // FTT, check this
	}
	this->SetToken(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsWordLexiconType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetToken() != Dc1NodePtr())
		result.Insert(GetToken());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsWordLexiconType_ExtMethodImpl.h


