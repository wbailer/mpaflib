
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtImplInclude.h


#include "Mp7JrsFilterType.h"
#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7JrsFilter1DType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFilter1DType::IMp7JrsFilter1DType()
{

// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtPropInit.h

}

IMp7JrsFilter1DType::~IMp7JrsFilter1DType()
{
// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtPropCleanup.h

}

Mp7JrsFilter1DType::Mp7JrsFilter1DType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFilter1DType::~Mp7JrsFilter1DType()
{
	Cleanup();
}

void Mp7JrsFilter1DType::Init()
{
	// Init base
	m_Base = CreateFilterType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_phase = 0; // Value
	m_phase_Default = 0; // Default value
	m_phase_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Terms = Mp7JrsDoubleMatrixPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtMyPropInit.h

}

void Mp7JrsFilter1DType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Terms);
}

void Mp7JrsFilter1DType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Filter1DTypePtr, since we
	// might need GetBase(), which isn't defined in IFilter1DType
	const Dc1Ptr< Mp7JrsFilter1DType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsFilterPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsFilter1DType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existphase())
	{
		this->Setphase(tmp->Getphase());
	}
	else
	{
		Invalidatephase();
	}
	}
		// Dc1Factory::DeleteObject(m_Terms);
		this->SetTerms(Dc1Factory::CloneObject(tmp->GetTerms()));
}

Dc1NodePtr Mp7JrsFilter1DType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsFilter1DType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsFilterType > Mp7JrsFilter1DType::GetBase() const
{
	return m_Base;
}

int Mp7JrsFilter1DType::Getphase() const
{
	if (this->Existphase()) {
		return m_phase;
	} else {
		return m_phase_Default;
	}
}

bool Mp7JrsFilter1DType::Existphase() const
{
	return m_phase_Exist;
}
void Mp7JrsFilter1DType::Setphase(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilter1DType::Setphase().");
	}
	m_phase = item;
	m_phase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilter1DType::Invalidatephase()
{
	m_phase_Exist = false;
}
Mp7JrsDoubleMatrixPtr Mp7JrsFilter1DType::GetTerms() const
{
		return m_Terms;
}

void Mp7JrsFilter1DType::SetTerms(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilter1DType::SetTerms().");
	}
	if (m_Terms != item)
	{
		// Dc1Factory::DeleteObject(m_Terms);
		m_Terms = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Terms) m_Terms->SetParent(m_myself.getPointer());
		if (m_Terms && m_Terms->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Terms->UseTypeAttribute = true;
		}
		if(m_Terms != Mp7JrsDoubleMatrixPtr())
		{
			m_Terms->SetContentName(XMLString::transcode("Terms"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsFilter1DType::Getdim() const
{
	return GetBase()->Getdim();
}

bool Mp7JrsFilter1DType::Existdim() const
{
	return GetBase()->Existdim();
}
void Mp7JrsFilter1DType::Setdim(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilter1DType::Setdim().");
	}
	GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilter1DType::Invalidatedim()
{
	GetBase()->Invalidatedim();
}

Dc1NodeEnum Mp7JrsFilter1DType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phase")) == 0)
	{
		// phase is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Terms")) == 0)
	{
		// Terms is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTerms()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTerms(p, client);
					if((p = GetTerms()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Filter1DType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Filter1DType");
	}
	return result;
}

XMLCh * Mp7JrsFilter1DType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFilter1DType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFilter1DType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Filter1DType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phase_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_phase);
		element->setAttributeNS(X(""), X("phase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Terms != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Terms->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Terms"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Terms->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFilter1DType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phase")))
	{
		// deserialize value type
		this->Setphase(Dc1Convert::TextToInt(parent->getAttribute(X("phase"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsFilterType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Terms"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Terms")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTerms(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFilter1DType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Terms")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetTerms(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFilter1DType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTerms() != Dc1NodePtr())
		result.Insert(GetTerms());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFilter1DType_ExtMethodImpl.h


