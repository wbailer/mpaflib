
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtImplInclude.h


#include "Mp7JrsVisualDSType.h"
#include "Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType.h"
#include "Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType.h"
#include "Mp7JrsMotionActivityType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsVideoSegmentFeatureType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsVisualTimeSeriesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TemporalTransition
#include "Mp7JrsGofGopFeatureType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RepresentativeFeature

#include <assert.h>
IMp7JrsVideoSegmentFeatureType::IMp7JrsVideoSegmentFeatureType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtPropInit.h

}

IMp7JrsVideoSegmentFeatureType::~IMp7JrsVideoSegmentFeatureType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtPropCleanup.h

}

Mp7JrsVideoSegmentFeatureType::Mp7JrsVideoSegmentFeatureType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSegmentFeatureType::~Mp7JrsVideoSegmentFeatureType()
{
	Cleanup();
}

void Mp7JrsVideoSegmentFeatureType::Init()
{
	// Init base
	m_Base = CreateVisualDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_TemporalTransition = Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr(); // Collection
	m_RepresentativeFeature = Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr(); // Collection
	m_MotionActivity = Mp7JrsMotionActivityPtr(); // Class
	m_MotionActivity_Exist = false;


// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtMyPropInit.h

}

void Mp7JrsVideoSegmentFeatureType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_TemporalTransition);
	// Dc1Factory::DeleteObject(m_RepresentativeFeature);
	// Dc1Factory::DeleteObject(m_MotionActivity);
}

void Mp7JrsVideoSegmentFeatureType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSegmentFeatureTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSegmentFeatureType
	const Dc1Ptr< Mp7JrsVideoSegmentFeatureType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsVideoSegmentFeatureType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_TemporalTransition);
		this->SetTemporalTransition(Dc1Factory::CloneObject(tmp->GetTemporalTransition()));
		// Dc1Factory::DeleteObject(m_RepresentativeFeature);
		this->SetRepresentativeFeature(Dc1Factory::CloneObject(tmp->GetRepresentativeFeature()));
	if (tmp->IsValidMotionActivity())
	{
		// Dc1Factory::DeleteObject(m_MotionActivity);
		this->SetMotionActivity(Dc1Factory::CloneObject(tmp->GetMotionActivity()));
	}
	else
	{
		InvalidateMotionActivity();
	}
}

Dc1NodePtr Mp7JrsVideoSegmentFeatureType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsVideoSegmentFeatureType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDSType > Mp7JrsVideoSegmentFeatureType::GetBase() const
{
	return m_Base;
}

Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr Mp7JrsVideoSegmentFeatureType::GetTemporalTransition() const
{
		return m_TemporalTransition;
}

Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr Mp7JrsVideoSegmentFeatureType::GetRepresentativeFeature() const
{
		return m_RepresentativeFeature;
}

Mp7JrsMotionActivityPtr Mp7JrsVideoSegmentFeatureType::GetMotionActivity() const
{
		return m_MotionActivity;
}

// Element is optional
bool Mp7JrsVideoSegmentFeatureType::IsValidMotionActivity() const
{
	return m_MotionActivity_Exist;
}

void Mp7JrsVideoSegmentFeatureType::SetTemporalTransition(const Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetTemporalTransition().");
	}
	if (m_TemporalTransition != item)
	{
		// Dc1Factory::DeleteObject(m_TemporalTransition);
		m_TemporalTransition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalTransition) m_TemporalTransition->SetParent(m_myself.getPointer());
		if(m_TemporalTransition != Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr())
		{
			m_TemporalTransition->SetContentName(XMLString::transcode("TemporalTransition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::SetRepresentativeFeature(const Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetRepresentativeFeature().");
	}
	if (m_RepresentativeFeature != item)
	{
		// Dc1Factory::DeleteObject(m_RepresentativeFeature);
		m_RepresentativeFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RepresentativeFeature) m_RepresentativeFeature->SetParent(m_myself.getPointer());
		if(m_RepresentativeFeature != Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr())
		{
			m_RepresentativeFeature->SetContentName(XMLString::transcode("RepresentativeFeature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::SetMotionActivity(const Mp7JrsMotionActivityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetMotionActivity().");
	}
	if (m_MotionActivity != item || m_MotionActivity_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MotionActivity);
		m_MotionActivity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionActivity) m_MotionActivity->SetParent(m_myself.getPointer());
		if (m_MotionActivity && m_MotionActivity->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MotionActivity->UseTypeAttribute = true;
		}
		m_MotionActivity_Exist = true;
		if(m_MotionActivity != Mp7JrsMotionActivityPtr())
		{
			m_MotionActivity->SetContentName(XMLString::transcode("MotionActivity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::InvalidateMotionActivity()
{
	m_MotionActivity_Exist = false;
}
XMLCh * Mp7JrsVideoSegmentFeatureType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsVideoSegmentFeatureType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsVideoSegmentFeatureType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsVideoSegmentFeatureType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsVideoSegmentFeatureType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsVideoSegmentFeatureType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsVideoSegmentFeatureType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsVideoSegmentFeatureType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsVideoSegmentFeatureType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsVideoSegmentFeatureType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsVideoSegmentFeatureType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsVideoSegmentFeatureType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsVideoSegmentFeatureType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsVideoSegmentFeatureType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsVideoSegmentFeatureType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentFeatureType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsVideoSegmentFeatureType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsVideoSegmentFeatureType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentFeatureType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSegmentFeatureType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TemporalTransition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:TemporalTransition is item of abstract type VisualTimeSeriesType
		// in element collection VideoSegmentFeatureType_TemporalTransition_CollectionType
		
		context->Found = true;
		Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr coll = GetTemporalTransition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentFeatureType_TemporalTransition_CollectionType; // FTT, check this
				SetTemporalTransition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualTimeSeriesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RepresentativeFeature")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:RepresentativeFeature is item of type GofGopFeatureType
		// in element collection VideoSegmentFeatureType_RepresentativeFeature_CollectionType
		
		context->Found = true;
		Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr coll = GetRepresentativeFeature();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentFeatureType_RepresentativeFeature_CollectionType; // FTT, check this
				SetRepresentativeFeature(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGofGopFeaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MotionActivity")) == 0)
	{
		// MotionActivity is simple element MotionActivityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMotionActivity()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMotionActivity(p, client);
					if((p = GetMotionActivity()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSegmentFeatureType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSegmentFeatureType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSegmentFeatureType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsVideoSegmentFeatureType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsVideoSegmentFeatureType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSegmentFeatureType"));
	// Element serialization:
	if (m_TemporalTransition != Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TemporalTransition->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_RepresentativeFeature != Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RepresentativeFeature->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MotionActivity != Mp7JrsMotionActivityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MotionActivity->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MotionActivity"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MotionActivity->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVideoSegmentFeatureType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("TemporalTransition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("TemporalTransition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr tmp = CreateVideoSegmentFeatureType_TemporalTransition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTemporalTransition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RepresentativeFeature"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RepresentativeFeature")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr tmp = CreateVideoSegmentFeatureType_RepresentativeFeature_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRepresentativeFeature(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MotionActivity"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MotionActivity")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMotionActivity(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSegmentFeatureType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("TemporalTransition")) == 0))
  {
	Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr tmp = CreateVideoSegmentFeatureType_TemporalTransition_CollectionType; // FTT, check this
	this->SetTemporalTransition(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RepresentativeFeature")) == 0))
  {
	Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr tmp = CreateVideoSegmentFeatureType_RepresentativeFeature_CollectionType; // FTT, check this
	this->SetRepresentativeFeature(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MotionActivity")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType; // FTT, check this
	}
	this->SetMotionActivity(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSegmentFeatureType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTemporalTransition() != Dc1NodePtr())
		result.Insert(GetTemporalTransition());
	if (GetRepresentativeFeature() != Dc1NodePtr())
		result.Insert(GetRepresentativeFeature());
	if (GetMotionActivity() != Dc1NodePtr())
		result.Insert(GetMotionActivity());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSegmentFeatureType_ExtMethodImpl.h


