
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// begin extension included
// file Mp7JrsMediaLocatorType_ExtImplInclude.h

#include "Dc1Settings.h" // For TreatRelIncrTimeAsAbsolute flag


// end extension included


#include "Mp7JrsInlineMediaType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaLocatorType::IMp7JrsMediaLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsMediaLocatorType_ExtPropInit.h

}

IMp7JrsMediaLocatorType::~IMp7JrsMediaLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsMediaLocatorType_ExtPropCleanup.h

}

Mp7JrsMediaLocatorType::Mp7JrsMediaLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaLocatorType::~Mp7JrsMediaLocatorType()
{
	Cleanup();
}

void Mp7JrsMediaLocatorType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaUri = XMLString::transcode(""); //  Mandatory String
	m_InlineMedia = Mp7JrsInlineMediaPtr(); // Class
	m_StreamID = (0); // Value

	m_StreamID_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaLocatorType_ExtMyPropInit.h

}

void Mp7JrsMediaLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaLocatorType_ExtMyPropCleanup.h


	XMLString::release(&this->m_MediaUri);
	this->m_MediaUri = NULL;
	// Dc1Factory::DeleteObject(m_InlineMedia);
}

void Mp7JrsMediaLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaLocatorType
	const Dc1Ptr< Mp7JrsMediaLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetMediaUri(XMLString::replicate(tmp->GetMediaUri()));
		// Dc1Factory::DeleteObject(m_InlineMedia);
		this->SetInlineMedia(Dc1Factory::CloneObject(tmp->GetInlineMedia()));
	if (tmp->IsValidStreamID())
	{
		this->SetStreamID(tmp->GetStreamID());
	}
	else
	{
		InvalidateStreamID();
	}
}

Dc1NodePtr Mp7JrsMediaLocatorType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaLocatorType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsMediaLocatorType::GetMediaUri() const
{
		return m_MediaUri;
}

Mp7JrsInlineMediaPtr Mp7JrsMediaLocatorType::GetInlineMedia() const
{
		return m_InlineMedia;
}

unsigned Mp7JrsMediaLocatorType::GetStreamID() const
{
		return m_StreamID;
}

// Element is optional
bool Mp7JrsMediaLocatorType::IsValidStreamID() const
{
	return m_StreamID_Exist;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMediaLocatorType::SetMediaUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaLocatorType::SetMediaUri().");
	}
	if (m_MediaUri != item)
	{
		XMLString::release(&m_MediaUri);
		m_MediaUri = item;
	// Dc1Factory::DeleteObject(m_InlineMedia);
	m_InlineMedia = Mp7JrsInlineMediaPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMediaLocatorType::SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaLocatorType::SetInlineMedia().");
	}
	if (m_InlineMedia != item)
	{
		// Dc1Factory::DeleteObject(m_InlineMedia);
		m_InlineMedia = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InlineMedia) m_InlineMedia->SetParent(m_myself.getPointer());
		if (m_InlineMedia && m_InlineMedia->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineMediaType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InlineMedia->UseTypeAttribute = true;
		}
		if(m_InlineMedia != Mp7JrsInlineMediaPtr())
		{
			m_InlineMedia->SetContentName(XMLString::transcode("InlineMedia"));
		}
	XMLString::release(&this->m_MediaUri);
	m_MediaUri = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaLocatorType::SetStreamID(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaLocatorType::SetStreamID().");
	}
	if (m_StreamID != item || m_StreamID_Exist == false)
	{
		m_StreamID = item;
		m_StreamID_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaLocatorType::InvalidateStreamID()
{
	m_StreamID_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("InlineMedia")) == 0)
	{
		// InlineMedia is simple element InlineMediaType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInlineMedia()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineMediaType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInlineMediaPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInlineMedia(p, client);
					if((p = GetInlineMedia()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsMediaLocatorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaLocatorType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaLocatorType"));
	// Element serialization:
	 // String
	if(m_MediaUri != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_MediaUri, X("urn:mpeg:mpeg7:schema:2004"), X("MediaUri"), false);
	// Class
	
	if (m_InlineMedia != Mp7JrsInlineMediaPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InlineMedia->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InlineMedia"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InlineMedia->Serialize(doc, element, &elem);
	}
	if(m_StreamID_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_StreamID);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("StreamID"), true);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsMediaLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("MediaUri"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetMediaUri(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetMediaUri(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InlineMedia"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InlineMedia")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateInlineMediaType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInlineMedia(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("StreamID"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetStreamID(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsMediaLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("InlineMedia")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateInlineMediaType; // FTT, check this
	}
	this->SetInlineMedia(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInlineMedia() != Dc1NodePtr())
		result.Insert(GetInlineMedia());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsMediaLocatorType_ExtMethodImpl.h
Dc1Ptr<class IMp7JrsmediaTimePointType>  Mp7JrsMediaLocatorType::GetMediaTimeBase() const {
	if(!Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute)
		throw Dc1Exception(DC1_ERROR, X("Mp7JrsMediaLocatorType::GetMediaTimeBase(): You must enable extension by setting Dc1Settings.TreatRelIncrTimeAsAbsolute to true."));
	return _mediaTimeBase;
}

void Mp7JrsMediaLocatorType::SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client)
{
	if(!Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute)
		throw Dc1Exception(DC1_ERROR, X("Mp7JrsMediaLocatorType::SetMediaTimeBase(): You must enable extension by setting Dc1Settings.TreatRelIncrTimeAsAbsolute to true."));

	if (!IsLocked(client))
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaLocatorType::SetMediaTimeBase().");

	_mediaTimeBase = timePoint;

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}
// end extension included


