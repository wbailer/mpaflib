
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtImplInclude.h


#include "Mp7JrsClassificationModelType.h"
#include "Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsProbabilityClassificationModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsProbabilityModelClassType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ProbabilityModelClass

#include <assert.h>
IMp7JrsProbabilityClassificationModelType::IMp7JrsProbabilityClassificationModelType()
{

// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtPropInit.h

}

IMp7JrsProbabilityClassificationModelType::~IMp7JrsProbabilityClassificationModelType()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtPropCleanup.h

}

Mp7JrsProbabilityClassificationModelType::Mp7JrsProbabilityClassificationModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsProbabilityClassificationModelType::~Mp7JrsProbabilityClassificationModelType()
{
	Cleanup();
}

void Mp7JrsProbabilityClassificationModelType::Init()
{
	// Init base
	m_Base = CreateClassificationModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ProbabilityModelClass = Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtMyPropInit.h

}

void Mp7JrsProbabilityClassificationModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ProbabilityModelClass);
}

void Mp7JrsProbabilityClassificationModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ProbabilityClassificationModelTypePtr, since we
	// might need GetBase(), which isn't defined in IProbabilityClassificationModelType
	const Dc1Ptr< Mp7JrsProbabilityClassificationModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsClassificationModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsProbabilityClassificationModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ProbabilityModelClass);
		this->SetProbabilityModelClass(Dc1Factory::CloneObject(tmp->GetProbabilityModelClass()));
}

Dc1NodePtr Mp7JrsProbabilityClassificationModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsProbabilityClassificationModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsClassificationModelType > Mp7JrsProbabilityClassificationModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr Mp7JrsProbabilityClassificationModelType::GetProbabilityModelClass() const
{
		return m_ProbabilityModelClass;
}

void Mp7JrsProbabilityClassificationModelType::SetProbabilityModelClass(const Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SetProbabilityModelClass().");
	}
	if (m_ProbabilityModelClass != item)
	{
		// Dc1Factory::DeleteObject(m_ProbabilityModelClass);
		m_ProbabilityModelClass = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ProbabilityModelClass) m_ProbabilityModelClass->SetParent(m_myself.getPointer());
		if(m_ProbabilityModelClass != Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr())
		{
			m_ProbabilityModelClass->SetContentName(XMLString::transcode("ProbabilityModelClass"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsProbabilityClassificationModelType::Getcomplete() const
{
	return GetBase()->Getcomplete();
}

bool Mp7JrsProbabilityClassificationModelType::Existcomplete() const
{
	return GetBase()->Existcomplete();
}
void Mp7JrsProbabilityClassificationModelType::Setcomplete(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::Setcomplete().");
	}
	GetBase()->Setcomplete(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::Invalidatecomplete()
{
	GetBase()->Invalidatecomplete();
}
bool Mp7JrsProbabilityClassificationModelType::Getredundant() const
{
	return GetBase()->Getredundant();
}

bool Mp7JrsProbabilityClassificationModelType::Existredundant() const
{
	return GetBase()->Existredundant();
}
void Mp7JrsProbabilityClassificationModelType::Setredundant(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::Setredundant().");
	}
	GetBase()->Setredundant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::Invalidateredundant()
{
	GetBase()->Invalidateredundant();
}
Mp7JrszeroToOnePtr Mp7JrsProbabilityClassificationModelType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsProbabilityClassificationModelType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsProbabilityClassificationModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsProbabilityClassificationModelType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsProbabilityClassificationModelType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsProbabilityClassificationModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsProbabilityClassificationModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsProbabilityClassificationModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsProbabilityClassificationModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityClassificationModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsProbabilityClassificationModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsProbabilityClassificationModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsProbabilityClassificationModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsProbabilityClassificationModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsProbabilityClassificationModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityClassificationModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsProbabilityClassificationModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsProbabilityClassificationModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsProbabilityClassificationModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsProbabilityClassificationModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsProbabilityClassificationModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityClassificationModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsProbabilityClassificationModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsProbabilityClassificationModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityClassificationModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsProbabilityClassificationModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ProbabilityModelClass")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ProbabilityModelClass is item of type ProbabilityModelClassType
		// in element collection ProbabilityClassificationModelType_ProbabilityModelClass_CollectionType
		
		context->Found = true;
		Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr coll = GetProbabilityModelClass();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateProbabilityClassificationModelType_ProbabilityModelClass_CollectionType; // FTT, check this
				SetProbabilityModelClass(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityModelClassType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityModelClassPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ProbabilityClassificationModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ProbabilityClassificationModelType");
	}
	return result;
}

XMLCh * Mp7JrsProbabilityClassificationModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsProbabilityClassificationModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsProbabilityClassificationModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ProbabilityClassificationModelType"));
	// Element serialization:
	if (m_ProbabilityModelClass != Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ProbabilityModelClass->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsProbabilityClassificationModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsClassificationModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ProbabilityModelClass"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ProbabilityModelClass")) == 0))
		{
			// Deserialize factory type
			Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr tmp = CreateProbabilityClassificationModelType_ProbabilityModelClass_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetProbabilityModelClass(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsProbabilityClassificationModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ProbabilityModelClass")) == 0))
  {
	Mp7JrsProbabilityClassificationModelType_ProbabilityModelClass_CollectionPtr tmp = CreateProbabilityClassificationModelType_ProbabilityModelClass_CollectionType; // FTT, check this
	this->SetProbabilityModelClass(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsProbabilityClassificationModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetProbabilityModelClass() != Dc1NodePtr())
		result.Insert(GetProbabilityModelClass());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsProbabilityClassificationModelType_ExtMethodImpl.h


