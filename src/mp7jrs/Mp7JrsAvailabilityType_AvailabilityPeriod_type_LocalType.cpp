
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType_ExtImplInclude.h


#include "Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType.h"

// no includefile for extension defined 
// file Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType_ExtMethodImpl.h


const XMLCh * Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::ToText(Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/live:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("live");
			break;
		case /*Enumeration::*/repeat:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("repeat");
			break;
		case /*Enumeration::*/firstShowing:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("firstShowing");
			break;
		case /*Enumeration::*/lastShowing:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("lastShowing");
			break;
		case /*Enumeration::*/conditionalAccess:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("conditionalAccess");
			break;
		case /*Enumeration::*/encrypted:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("encrypted");
			break;
		case /*Enumeration::*/payPerUse:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("payPerUse");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::Enumeration result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "live") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/live;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "repeat") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/repeat;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "firstShowing") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/firstShowing;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "lastShowing") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/lastShowing;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "conditionalAccess") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/conditionalAccess;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "encrypted") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/encrypted;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "payPerUse") == 0)
	{
		result = Mp7JrsAvailabilityType_AvailabilityPeriod_type_LocalType::/*Enumeration::*/payPerUse;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
