
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtImplInclude.h


#include "Mp7JrsPlaceType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsPlaceType_Name_CollectionType.h"
#include "Mp7JrsPlaceType_NameTerm_CollectionType.h"
#include "Mp7JrsPlaceType_PlaceDescription_CollectionType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsPlaceType_GeographicPosition_LocalType.h"
#include "Mp7JrsPlaceType_AstronomicalBody_CollectionType.h"
#include "Mp7JrsPlaceType_Region_CollectionType.h"
#include "Mp7JrsPlaceType_AdministrativeUnit_CollectionType.h"
#include "Mp7JrsPlaceType_PostalAddress_LocalType.h"
#include "Mp7JrsPlaceType_StructuredPostalAddress_LocalType.h"
#include "Mp7JrsPlaceType_StructuredInternalCoordinates_LocalType.h"
#include "Mp7JrsPlaceType_ElectronicAddress_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Location_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:NameTerm
#include "Mp7JrsregionCode.h" // Element collection urn:mpeg:mpeg7:schema:2004:Region
#include "Mp7JrsPlaceType_AdministrativeUnit_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AdministrativeUnit
#include "Mp7JrsElectronicAddressType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ElectronicAddress

#include <assert.h>
IMp7JrsCreationPreferencesType_Location_LocalType::IMp7JrsCreationPreferencesType_Location_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtPropInit.h

}

IMp7JrsCreationPreferencesType_Location_LocalType::~IMp7JrsCreationPreferencesType_Location_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtPropCleanup.h

}

Mp7JrsCreationPreferencesType_Location_LocalType::Mp7JrsCreationPreferencesType_Location_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreationPreferencesType_Location_LocalType::~Mp7JrsCreationPreferencesType_Location_LocalType()
{
	Cleanup();
}

void Mp7JrsCreationPreferencesType_Location_LocalType::Init()
{
	// Init base
	m_Base = CreatePlaceType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;



// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMyPropInit.h

}

void Mp7JrsCreationPreferencesType_Location_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
}

void Mp7JrsCreationPreferencesType_Location_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreationPreferencesType_Location_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ICreationPreferencesType_Location_LocalType
	const Dc1Ptr< Mp7JrsCreationPreferencesType_Location_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsPlacePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCreationPreferencesType_Location_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
}

Dc1NodePtr Mp7JrsCreationPreferencesType_Location_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCreationPreferencesType_Location_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsPlaceType > Mp7JrsCreationPreferencesType_Location_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrspreferenceValuePtr Mp7JrsCreationPreferencesType_Location_LocalType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
XMLCh * Mp7JrsCreationPreferencesType_Location_LocalType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsPlaceType_Name_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetName() const
{
	return GetBase()->GetName();
}

Mp7JrsPlaceType_NameTerm_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetNameTerm() const
{
	return GetBase()->GetNameTerm();
}

Mp7JrsPlaceType_PlaceDescription_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetPlaceDescription() const
{
	return GetBase()->GetPlaceDescription();
}

Mp7JrsTermUsePtr Mp7JrsCreationPreferencesType_Location_LocalType::GetRole() const
{
	return GetBase()->GetRole();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidRole() const
{
	return GetBase()->IsValidRole();
}

Mp7JrsPlaceType_GeographicPosition_LocalPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetGeographicPosition() const
{
	return GetBase()->GetGeographicPosition();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidGeographicPosition() const
{
	return GetBase()->IsValidGeographicPosition();
}

Mp7JrsPlaceType_AstronomicalBody_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetAstronomicalBody() const
{
	return GetBase()->GetAstronomicalBody();
}

Mp7JrsPlaceType_Region_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetRegion() const
{
	return GetBase()->GetRegion();
}

Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetAdministrativeUnit() const
{
	return GetBase()->GetAdministrativeUnit();
}

Mp7JrsPlaceType_PostalAddress_LocalPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetPostalAddress() const
{
	return GetBase()->GetPostalAddress();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidPostalAddress() const
{
	return GetBase()->IsValidPostalAddress();
}

Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetStructuredPostalAddress() const
{
	return GetBase()->GetStructuredPostalAddress();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidStructuredPostalAddress() const
{
	return GetBase()->IsValidStructuredPostalAddress();
}

XMLCh * Mp7JrsCreationPreferencesType_Location_LocalType::GetInternalCoordinates() const
{
	return GetBase()->GetInternalCoordinates();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidInternalCoordinates() const
{
	return GetBase()->IsValidInternalCoordinates();
}

Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetStructuredInternalCoordinates() const
{
	return GetBase()->GetStructuredInternalCoordinates();
}

// Element is optional
bool Mp7JrsCreationPreferencesType_Location_LocalType::IsValidStructuredInternalCoordinates() const
{
	return GetBase()->IsValidStructuredInternalCoordinates();
}

Mp7JrsPlaceType_ElectronicAddress_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetElectronicAddress() const
{
	return GetBase()->GetElectronicAddress();
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetName(const Mp7JrsPlaceType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetName().");
	}
	GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetNameTerm(const Mp7JrsPlaceType_NameTerm_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetNameTerm().");
	}
	GetBase()->SetNameTerm(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetPlaceDescription(const Mp7JrsPlaceType_PlaceDescription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetPlaceDescription().");
	}
	GetBase()->SetPlaceDescription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetRole(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetRole().");
	}
	GetBase()->SetRole(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidateRole()
{
	GetBase()->InvalidateRole();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetGeographicPosition(const Mp7JrsPlaceType_GeographicPosition_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetGeographicPosition().");
	}
	GetBase()->SetGeographicPosition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidateGeographicPosition()
{
	GetBase()->InvalidateGeographicPosition();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetAstronomicalBody(const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetAstronomicalBody().");
	}
	GetBase()->SetAstronomicalBody(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetRegion(const Mp7JrsPlaceType_Region_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetRegion().");
	}
	GetBase()->SetRegion(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetAdministrativeUnit(const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetAdministrativeUnit().");
	}
	GetBase()->SetAdministrativeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsCreationPreferencesType_Location_LocalType::SetPostalAddress(const Mp7JrsPlaceType_PostalAddress_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetPostalAddress().");
	}
	GetBase()->SetPostalAddress(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatePostalAddress()
{
	GetBase()->InvalidatePostalAddress();
}
/* element */
void Mp7JrsCreationPreferencesType_Location_LocalType::SetStructuredPostalAddress(const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetStructuredPostalAddress().");
	}
	GetBase()->SetStructuredPostalAddress(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidateStructuredPostalAddress()
{
	GetBase()->InvalidateStructuredPostalAddress();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsCreationPreferencesType_Location_LocalType::SetInternalCoordinates(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetInternalCoordinates().");
	}
	GetBase()->SetInternalCoordinates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidateInternalCoordinates()
{
	GetBase()->InvalidateInternalCoordinates();
}
/* element */
void Mp7JrsCreationPreferencesType_Location_LocalType::SetStructuredInternalCoordinates(const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetStructuredInternalCoordinates().");
	}
	GetBase()->SetStructuredInternalCoordinates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidateStructuredInternalCoordinates()
{
	GetBase()->InvalidateStructuredInternalCoordinates();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetElectronicAddress(const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetElectronicAddress().");
	}
	GetBase()->SetElectronicAddress(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsCreationPreferencesType_Location_LocalType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsCreationPreferencesType_Location_LocalType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsCreationPreferencesType_Location_LocalType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsCreationPreferencesType_Location_LocalType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsCreationPreferencesType_Location_LocalType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Location_LocalType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsCreationPreferencesType_Location_LocalType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsCreationPreferencesType_Location_LocalType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Location_LocalType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCreationPreferencesType_Location_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreationPreferencesType_Location_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreationPreferencesType_Location_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsCreationPreferencesType_Location_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsCreationPreferencesType_Location_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsCreationPreferencesType_Location_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreationPreferencesType_Location_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsCreationPreferencesType_Location_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsPlaceType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreationPreferencesType_Location_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreationPreferencesType_Location_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetNameTerm() != Dc1NodePtr())
		result.Insert(GetNameTerm());
	if (GetPlaceDescription() != Dc1NodePtr())
		result.Insert(GetPlaceDescription());
	if (GetRole() != Dc1NodePtr())
		result.Insert(GetRole());
	if (GetGeographicPosition() != Dc1NodePtr())
		result.Insert(GetGeographicPosition());
	if (GetAstronomicalBody() != Dc1NodePtr())
		result.Insert(GetAstronomicalBody());
	if (GetRegion() != Dc1NodePtr())
		result.Insert(GetRegion());
	if (GetAdministrativeUnit() != Dc1NodePtr())
		result.Insert(GetAdministrativeUnit());
	if (GetElectronicAddress() != Dc1NodePtr())
		result.Insert(GetElectronicAddress());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetPostalAddress() != Dc1NodePtr())
		result.Insert(GetPostalAddress());
	if (GetStructuredPostalAddress() != Dc1NodePtr())
		result.Insert(GetStructuredPostalAddress());
	if (GetStructuredInternalCoordinates() != Dc1NodePtr())
		result.Insert(GetStructuredInternalCoordinates());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Location_LocalType_ExtMethodImpl.h


