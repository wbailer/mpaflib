
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtImplInclude.h


#include "Mp7JrsClassificationType_Target_Market_CollectionType.h"
#include "Mp7JrsClassificationType_Target_Age_LocalType.h"
#include "Mp7JrsClassificationType_Target_Region_CollectionType.h"
#include "Mp7JrsClassificationType_Target_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Market
#include "Mp7JrsregionCode.h" // Element collection urn:mpeg:mpeg7:schema:2004:Region

#include <assert.h>
IMp7JrsClassificationType_Target_LocalType::IMp7JrsClassificationType_Target_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtPropInit.h

}

IMp7JrsClassificationType_Target_LocalType::~IMp7JrsClassificationType_Target_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtPropCleanup.h

}

Mp7JrsClassificationType_Target_LocalType::Mp7JrsClassificationType_Target_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationType_Target_LocalType::~Mp7JrsClassificationType_Target_LocalType()
{
	Cleanup();
}

void Mp7JrsClassificationType_Target_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Market = Mp7JrsClassificationType_Target_Market_CollectionPtr(); // Collection
	m_Age = Mp7JrsClassificationType_Target_Age_LocalPtr(); // Class
	m_Age_Exist = false;
	m_Region = Mp7JrsClassificationType_Target_Region_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtMyPropInit.h

}

void Mp7JrsClassificationType_Target_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Market);
	// Dc1Factory::DeleteObject(m_Age);
	// Dc1Factory::DeleteObject(m_Region);
}

void Mp7JrsClassificationType_Target_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationType_Target_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationType_Target_LocalType
	const Dc1Ptr< Mp7JrsClassificationType_Target_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Market);
		this->SetMarket(Dc1Factory::CloneObject(tmp->GetMarket()));
	if (tmp->IsValidAge())
	{
		// Dc1Factory::DeleteObject(m_Age);
		this->SetAge(Dc1Factory::CloneObject(tmp->GetAge()));
	}
	else
	{
		InvalidateAge();
	}
		// Dc1Factory::DeleteObject(m_Region);
		this->SetRegion(Dc1Factory::CloneObject(tmp->GetRegion()));
}

Dc1NodePtr Mp7JrsClassificationType_Target_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsClassificationType_Target_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsClassificationType_Target_Market_CollectionPtr Mp7JrsClassificationType_Target_LocalType::GetMarket() const
{
		return m_Market;
}

Mp7JrsClassificationType_Target_Age_LocalPtr Mp7JrsClassificationType_Target_LocalType::GetAge() const
{
		return m_Age;
}

// Element is optional
bool Mp7JrsClassificationType_Target_LocalType::IsValidAge() const
{
	return m_Age_Exist;
}

Mp7JrsClassificationType_Target_Region_CollectionPtr Mp7JrsClassificationType_Target_LocalType::GetRegion() const
{
		return m_Region;
}

void Mp7JrsClassificationType_Target_LocalType::SetMarket(const Mp7JrsClassificationType_Target_Market_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Target_LocalType::SetMarket().");
	}
	if (m_Market != item)
	{
		// Dc1Factory::DeleteObject(m_Market);
		m_Market = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Market) m_Market->SetParent(m_myself.getPointer());
		if(m_Market != Mp7JrsClassificationType_Target_Market_CollectionPtr())
		{
			m_Market->SetContentName(XMLString::transcode("Market"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType_Target_LocalType::SetAge(const Mp7JrsClassificationType_Target_Age_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Target_LocalType::SetAge().");
	}
	if (m_Age != item || m_Age_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Age);
		m_Age = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Age) m_Age->SetParent(m_myself.getPointer());
		if (m_Age && m_Age->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_Age_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Age->UseTypeAttribute = true;
		}
		m_Age_Exist = true;
		if(m_Age != Mp7JrsClassificationType_Target_Age_LocalPtr())
		{
			m_Age->SetContentName(XMLString::transcode("Age"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType_Target_LocalType::InvalidateAge()
{
	m_Age_Exist = false;
}
void Mp7JrsClassificationType_Target_LocalType::SetRegion(const Mp7JrsClassificationType_Target_Region_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Target_LocalType::SetRegion().");
	}
	if (m_Region != item)
	{
		// Dc1Factory::DeleteObject(m_Region);
		m_Region = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Region) m_Region->SetParent(m_myself.getPointer());
		if(m_Region != Mp7JrsClassificationType_Target_Region_CollectionPtr())
		{
			m_Region->SetContentName(XMLString::transcode("Region"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationType_Target_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Market")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Market is item of type ControlledTermUseType
		// in element collection ClassificationType_Target_Market_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Target_Market_CollectionPtr coll = GetMarket();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Target_Market_CollectionType; // FTT, check this
				SetMarket(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Age")) == 0)
	{
		// Age is simple element ClassificationType_Target_Age_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAge()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_Age_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_Target_Age_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAge(p, client);
					if((p = GetAge()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Region")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Region is item of type regionCode
		// in element collection ClassificationType_Target_Region_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Target_Region_CollectionPtr coll = GetRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Target_Region_CollectionType; // FTT, check this
				SetRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:regionCode")))) != empty)
			{
				// Is type allowed
				Mp7JrsregionCodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationType_Target_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationType_Target_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationType_Target_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsClassificationType_Target_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsClassificationType_Target_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationType_Target_LocalType"));
	// Element serialization:
	if (m_Market != Mp7JrsClassificationType_Target_Market_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Market->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Age != Mp7JrsClassificationType_Target_Age_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Age->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Age"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Age->Serialize(doc, element, &elem);
	}
	if (m_Region != Mp7JrsClassificationType_Target_Region_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Region->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsClassificationType_Target_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Market"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Market")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Target_Market_CollectionPtr tmp = CreateClassificationType_Target_Market_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMarket(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Age"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Age")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateClassificationType_Target_Age_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAge(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Region"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Region")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Target_Region_CollectionPtr tmp = CreateClassificationType_Target_Region_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationType_Target_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Market")) == 0))
  {
	Mp7JrsClassificationType_Target_Market_CollectionPtr tmp = CreateClassificationType_Target_Market_CollectionType; // FTT, check this
	this->SetMarket(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Age")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateClassificationType_Target_Age_LocalType; // FTT, check this
	}
	this->SetAge(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Region")) == 0))
  {
	Mp7JrsClassificationType_Target_Region_CollectionPtr tmp = CreateClassificationType_Target_Region_CollectionType; // FTT, check this
	this->SetRegion(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationType_Target_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMarket() != Dc1NodePtr())
		result.Insert(GetMarket());
	if (GetAge() != Dc1NodePtr())
		result.Insert(GetAge());
	if (GetRegion() != Dc1NodePtr())
		result.Insert(GetRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationType_Target_LocalType_ExtMethodImpl.h


