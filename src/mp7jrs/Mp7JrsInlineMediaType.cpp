
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtImplInclude.h


#include "Mp7JrsmimeType.h"
#include "Mp7JrsInlineMediaType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsInlineMediaType::IMp7JrsInlineMediaType()
{

// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtPropInit.h

}

IMp7JrsInlineMediaType::~IMp7JrsInlineMediaType()
{
// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtPropCleanup.h

}

Mp7JrsInlineMediaType::Mp7JrsInlineMediaType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInlineMediaType::~Mp7JrsInlineMediaType()
{
	Cleanup();
}

void Mp7JrsInlineMediaType::Init()
{

	// Init attributes
	m_type = Mp7JrsmimePtr(); // Pattern

	// Init elements (element, union sequence choice all any)
	
	// TODO: Binary data for elements is not implemented the full way  (just as string)
	m_MediaData16 = NULL; // Binary
	// TODO: Binary data for elements is not implemented the full way  (just as string)
	m_MediaData64 = NULL; // Binary


// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtMyPropInit.h

}

void Mp7JrsInlineMediaType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_type); // Pattern
	// TODO Binary data for elements is not implemented (just as string)
	XMLString::release(&this->m_MediaData16);
	this->m_MediaData16 = NULL;
	// TODO Binary data for elements is not implemented (just as string)
	XMLString::release(&this->m_MediaData64);
	this->m_MediaData64 = NULL;
}

void Mp7JrsInlineMediaType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InlineMediaTypePtr, since we
	// might need GetBase(), which isn't defined in IInlineMediaType
	const Dc1Ptr< Mp7JrsInlineMediaType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_type); // Pattern
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
		// TODO Binary data for elements is not implemented  (just as string)
		this->SetMediaData16(XMLString::replicate(tmp->GetMediaData16()));
		// TODO Binary data for elements is not implemented  (just as string)
		this->SetMediaData64(XMLString::replicate(tmp->GetMediaData64()));
}

Dc1NodePtr Mp7JrsInlineMediaType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsInlineMediaType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsmimePtr Mp7JrsInlineMediaType::Gettype() const
{
	return m_type;
}

void Mp7JrsInlineMediaType::Settype(const Mp7JrsmimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineMediaType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsInlineMediaType::GetMediaData16() const
{
		return m_MediaData16;
}

XMLCh * Mp7JrsInlineMediaType::GetMediaData64() const
{
		return m_MediaData64;
}

// implementing setter for choice 
/* element */
void Mp7JrsInlineMediaType::SetMediaData16(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineMediaType::SetMediaData16().");
	}
	if (m_MediaData16 != item)
	{
		// TODO Binary data for elements is not implemented (just as string)
		XMLString::release(&m_MediaData16);
		m_MediaData16 = item;
	// TODO Binary data for elements is not implemented (just as string)
	// TODO Binary data for elements is not implemented (just as string)
  //	if(m_MediaData64 = NULL; // FTT Shouldn't it be XMLString::release()?
  if(!m_MediaData64) XMLString::release(&m_MediaData64); // Just for testing
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsInlineMediaType::SetMediaData64(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineMediaType::SetMediaData64().");
	}
	if (m_MediaData64 != item)
	{
		// TODO Binary data for elements is not implemented (just as string)
		XMLString::release(&m_MediaData64);
		m_MediaData64 = item;
	// TODO Binary data for elements is not implemented (just as string)
	// TODO Binary data for elements is not implemented (just as string)
  //	if(m_MediaData16 = NULL; // FTT Shouldn't it be XMLString::release()?
  if(!m_MediaData16) XMLString::release(&m_MediaData16); // Just for testing
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsInlineMediaType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsInlineMediaType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsInlineMediaType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsInlineMediaType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InlineMediaType"));
	// Attribute Serialization:
	// Pattern
	if (m_type != Mp7JrsmimePtr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// TODO Binary data for elements is not implemented (just as string)
	if(m_MediaData16 != (XMLCh*)NULL)Dc1Util::SerializeTextNode(element, doc, m_MediaData16, X("urn:mpeg:mpeg7:schema:2004"), X("MediaData16"), false);
	
	// TODO Binary data for elements is not implemented (just as string)
	if(m_MediaData64 != (XMLCh*)NULL)Dc1Util::SerializeTextNode(element, doc, m_MediaData64, X("urn:mpeg:mpeg7:schema:2004"), X("MediaData64"), false);
	

}

bool Mp7JrsInlineMediaType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		Mp7JrsmimePtr tmp = CreatemimeType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize binary element
		// TODO Binary data for elements is not implemented (just as string)
	if(Dc1Util::HasNodeName(parent, X("MediaData16"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName() , X("MediaData16")) == 0))
		{
			// Deserialize binary (XMLCh *)
			this->SetMediaData16(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
			found = true;
			* current = parent;
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize binary element
		// TODO Binary data for elements is not implemented (just as string)
	if(Dc1Util::HasNodeName(parent, X("MediaData64"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName() , X("MediaData64")) == 0))
		{
			// Deserialize binary (XMLCh *)
			this->SetMediaData64(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
			found = true;
			* current = parent;
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInlineMediaType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsInlineMediaType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInlineMediaType_ExtMethodImpl.h


