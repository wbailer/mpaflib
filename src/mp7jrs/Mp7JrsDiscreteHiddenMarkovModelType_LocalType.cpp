
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtImplInclude.h


#include "Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionType.h"
#include "Mp7JrsDiscreteDistributionType.h"
#include "Mp7JrsDiscreteHiddenMarkovModelType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsDiscreteHiddenMarkovModelType_LocalType::IMp7JrsDiscreteHiddenMarkovModelType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtPropInit.h

}

IMp7JrsDiscreteHiddenMarkovModelType_LocalType::~IMp7JrsDiscreteHiddenMarkovModelType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtPropCleanup.h

}

Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Mp7JrsDiscreteHiddenMarkovModelType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDiscreteHiddenMarkovModelType_LocalType::~Mp7JrsDiscreteHiddenMarkovModelType_LocalType()
{
	Cleanup();
}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_NumOfObservationsPerState = (0); // Value

	m_Observation = Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr(); // Collection
	m_ObservationDistribution = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMyPropInit.h

}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Observation);
	// Dc1Factory::DeleteObject(m_ObservationDistribution);
}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DiscreteHiddenMarkovModelType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IDiscreteHiddenMarkovModelType_LocalType
	const Dc1Ptr< Mp7JrsDiscreteHiddenMarkovModelType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetNumOfObservationsPerState(tmp->GetNumOfObservationsPerState());
		// Dc1Factory::DeleteObject(m_Observation);
		this->SetObservation(Dc1Factory::CloneObject(tmp->GetObservation()));
		// Dc1Factory::DeleteObject(m_ObservationDistribution);
		this->SetObservationDistribution(Dc1Factory::CloneObject(tmp->GetObservationDistribution()));
}

Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


unsigned Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetNumOfObservationsPerState() const
{
		return m_NumOfObservationsPerState;
}

Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetObservation() const
{
		return m_Observation;
}

Dc1Ptr< Dc1Node > Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetObservationDistribution() const
{
		return m_ObservationDistribution;
}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetNumOfObservationsPerState(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetNumOfObservationsPerState().");
	}
	if (m_NumOfObservationsPerState != item)
	{
		m_NumOfObservationsPerState = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetObservation(const Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetObservation().");
	}
	if (m_Observation != item)
	{
		// Dc1Factory::DeleteObject(m_Observation);
		m_Observation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Observation) m_Observation->SetParent(m_myself.getPointer());
		if(m_Observation != Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr())
		{
			m_Observation->SetContentName(XMLString::transcode("Observation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetObservationDistribution(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType_LocalType::SetObservationDistribution().");
	}
	if (m_ObservationDistribution != item)
	{
		// Dc1Factory::DeleteObject(m_ObservationDistribution);
		m_ObservationDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ObservationDistribution) m_ObservationDistribution->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_ObservationDistribution) m_ObservationDistribution->UseTypeAttribute = true;
		if(m_ObservationDistribution != Dc1Ptr< Dc1Node >())
		{
			m_ObservationDistribution->SetContentName(XMLString::transcode("ObservationDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: DiscreteHiddenMarkovModelType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsDiscreteHiddenMarkovModelType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DiscreteHiddenMarkovModelType_LocalType"));
	// Element serialization:
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_NumOfObservationsPerState);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("NumOfObservationsPerState"), false);
		XMLString::release(&tmp);
	}
	if (m_Observation != Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Observation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_ObservationDistribution != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ObservationDistribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ObservationDistribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_ObservationDistribution->UseTypeAttribute = true;
		}
		m_ObservationDistribution->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsDiscreteHiddenMarkovModelType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("NumOfObservationsPerState"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetNumOfObservationsPerState(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Observation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Observation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr tmp = CreateDiscreteHiddenMarkovModelType_Observation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetObservation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ObservationDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ObservationDistribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDiscreteDistributionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetObservationDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Observation")) == 0))
  {
	Mp7JrsDiscreteHiddenMarkovModelType_Observation_CollectionPtr tmp = CreateDiscreteHiddenMarkovModelType_Observation_CollectionType; // FTT, check this
	this->SetObservation(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("ObservationDistribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDiscreteDistributionType; // FTT, check this
	}
	this->SetObservationDistribution(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDiscreteHiddenMarkovModelType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetObservation() != Dc1NodePtr())
		result.Insert(GetObservation());
	if (GetObservationDistribution() != Dc1NodePtr())
		result.Insert(GetObservationDistribution());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_LocalType_ExtMethodImpl.h


