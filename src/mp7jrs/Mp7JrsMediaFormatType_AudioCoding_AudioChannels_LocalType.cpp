
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtPropInit.h

}

IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::~IMp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::~Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Init()
{
	// Init base
	m_Base = 0;

	// Init attributes
	m_front = 0; // Value
	m_front_Exist = false;
	m_side = 0; // Value
	m_side_Exist = false;
	m_rear = 0; // Value
	m_rear_Exist = false;
	m_lfe = 0; // Value
	m_lfe_Exist = false;
	m_track = 0; // Value
	m_track_Exist = false;



// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaFormatType_AudioCoding_AudioChannels_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaFormatType_AudioCoding_AudioChannels_LocalType
	const Dc1Ptr< Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	if (tmp->Existfront())
	{
		this->Setfront(tmp->Getfront());
	}
	else
	{
		Invalidatefront();
	}
	}
	{
	if (tmp->Existside())
	{
		this->Setside(tmp->Getside());
	}
	else
	{
		Invalidateside();
	}
	}
	{
	if (tmp->Existrear())
	{
		this->Setrear(tmp->Getrear());
	}
	else
	{
		Invalidaterear();
	}
	}
	{
	if (tmp->Existlfe())
	{
		this->Setlfe(tmp->Getlfe());
	}
	else
	{
		Invalidatelfe();
	}
	}
	{
	if (tmp->Existtrack())
	{
		this->Settrack(tmp->Gettrack());
	}
	else
	{
		Invalidatetrack();
	}
	}
}

Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::SetContent(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::GetContent() const
{
	return m_Base;
}
unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Getfront() const
{
	return m_front;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Existfront() const
{
	return m_front_Exist;
}
void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setfront(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setfront().");
	}
	m_front = item;
	m_front_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Invalidatefront()
{
	m_front_Exist = false;
}
unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Getside() const
{
	return m_side;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Existside() const
{
	return m_side_Exist;
}
void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setside(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setside().");
	}
	m_side = item;
	m_side_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Invalidateside()
{
	m_side_Exist = false;
}
unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Getrear() const
{
	return m_rear;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Existrear() const
{
	return m_rear_Exist;
}
void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setrear(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setrear().");
	}
	m_rear = item;
	m_rear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Invalidaterear()
{
	m_rear_Exist = false;
}
unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Getlfe() const
{
	return m_lfe;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Existlfe() const
{
	return m_lfe_Exist;
}
void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setlfe(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Setlfe().");
	}
	m_lfe = item;
	m_lfe_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Invalidatelfe()
{
	m_lfe_Exist = false;
}
unsigned Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Gettrack() const
{
	return m_track;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Existtrack() const
{
	return m_track_Exist;
}
void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Settrack(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Settrack().");
	}
	m_track = item;
	m_track_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Invalidatetrack()
{
	m_track_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("front")) == 0)
	{
		// front is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("side")) == 0)
	{
		// side is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("rear")) == 0)
	{
		// rear is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lfe")) == 0)
	{
		// lfe is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("track")) == 0)
	{
		// track is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaFormatType_AudioCoding_AudioChannels_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaFormatType_AudioCoding_AudioChannels_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::ContentToString() const
{
	// Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormatType_AudioCoding_AudioChannels_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_front_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_front);
		element->setAttributeNS(X(""), X("front"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_side_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_side);
		element->setAttributeNS(X(""), X("side"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_rear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_rear);
		element->setAttributeNS(X(""), X("rear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lfe_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_lfe);
		element->setAttributeNS(X(""), X("lfe"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_track_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_track);
		element->setAttributeNS(X(""), X("track"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("front")))
	{
		// deserialize value type
		this->Setfront(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("front"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("side")))
	{
		// deserialize value type
		this->Setside(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("side"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("rear")))
	{
		// deserialize value type
		this->Setrear(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("rear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("lfe")))
	{
		// deserialize value type
		this->Setlfe(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("lfe"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("track")))
	{
		// deserialize value type
		this->Settrack(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("track"))));
		* current = parent;
	}

  m_Base = Dc1Convert::TextToUnsignedInt(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType_ExtMethodImpl.h


