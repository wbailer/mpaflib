
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned15.h"
#include "Mp7JrsPerceptual3DShapeType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsPerceptual3DShapeType_LocalType::IMp7JrsPerceptual3DShapeType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtPropInit.h

}

IMp7JrsPerceptual3DShapeType_LocalType::~IMp7JrsPerceptual3DShapeType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtPropCleanup.h

}

Mp7JrsPerceptual3DShapeType_LocalType::Mp7JrsPerceptual3DShapeType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptual3DShapeType_LocalType::~Mp7JrsPerceptual3DShapeType_LocalType()
{
	Cleanup();
}

void Mp7JrsPerceptual3DShapeType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Volume = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Center_X = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Center_Y = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Center_Z = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_1X = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_1Y = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_1Z = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_2X = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_2Y = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_PCA_Axis_2Z = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Max_1 = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Max_2 = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Max_3 = Mp7Jrsunsigned15Ptr(); // Class with content 
	m_Convexity = Mp7Jrsunsigned15Ptr(); // Class with content 


// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMyPropInit.h

}

void Mp7JrsPerceptual3DShapeType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Volume);
	// Dc1Factory::DeleteObject(m_Center_X);
	// Dc1Factory::DeleteObject(m_Center_Y);
	// Dc1Factory::DeleteObject(m_Center_Z);
	// Dc1Factory::DeleteObject(m_PCA_Axis_1X);
	// Dc1Factory::DeleteObject(m_PCA_Axis_1Y);
	// Dc1Factory::DeleteObject(m_PCA_Axis_1Z);
	// Dc1Factory::DeleteObject(m_PCA_Axis_2X);
	// Dc1Factory::DeleteObject(m_PCA_Axis_2Y);
	// Dc1Factory::DeleteObject(m_PCA_Axis_2Z);
	// Dc1Factory::DeleteObject(m_Max_1);
	// Dc1Factory::DeleteObject(m_Max_2);
	// Dc1Factory::DeleteObject(m_Max_3);
	// Dc1Factory::DeleteObject(m_Convexity);
}

void Mp7JrsPerceptual3DShapeType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Perceptual3DShapeType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptual3DShapeType_LocalType
	const Dc1Ptr< Mp7JrsPerceptual3DShapeType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Volume);
		this->SetVolume(Dc1Factory::CloneObject(tmp->GetVolume()));
		// Dc1Factory::DeleteObject(m_Center_X);
		this->SetCenter_X(Dc1Factory::CloneObject(tmp->GetCenter_X()));
		// Dc1Factory::DeleteObject(m_Center_Y);
		this->SetCenter_Y(Dc1Factory::CloneObject(tmp->GetCenter_Y()));
		// Dc1Factory::DeleteObject(m_Center_Z);
		this->SetCenter_Z(Dc1Factory::CloneObject(tmp->GetCenter_Z()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_1X);
		this->SetPCA_Axis_1X(Dc1Factory::CloneObject(tmp->GetPCA_Axis_1X()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_1Y);
		this->SetPCA_Axis_1Y(Dc1Factory::CloneObject(tmp->GetPCA_Axis_1Y()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_1Z);
		this->SetPCA_Axis_1Z(Dc1Factory::CloneObject(tmp->GetPCA_Axis_1Z()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_2X);
		this->SetPCA_Axis_2X(Dc1Factory::CloneObject(tmp->GetPCA_Axis_2X()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_2Y);
		this->SetPCA_Axis_2Y(Dc1Factory::CloneObject(tmp->GetPCA_Axis_2Y()));
		// Dc1Factory::DeleteObject(m_PCA_Axis_2Z);
		this->SetPCA_Axis_2Z(Dc1Factory::CloneObject(tmp->GetPCA_Axis_2Z()));
		// Dc1Factory::DeleteObject(m_Max_1);
		this->SetMax_1(Dc1Factory::CloneObject(tmp->GetMax_1()));
		// Dc1Factory::DeleteObject(m_Max_2);
		this->SetMax_2(Dc1Factory::CloneObject(tmp->GetMax_2()));
		// Dc1Factory::DeleteObject(m_Max_3);
		this->SetMax_3(Dc1Factory::CloneObject(tmp->GetMax_3()));
		// Dc1Factory::DeleteObject(m_Convexity);
		this->SetConvexity(Dc1Factory::CloneObject(tmp->GetConvexity()));
}

Dc1NodePtr Mp7JrsPerceptual3DShapeType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPerceptual3DShapeType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetVolume() const
{
		return m_Volume;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetCenter_X() const
{
		return m_Center_X;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetCenter_Y() const
{
		return m_Center_Y;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetCenter_Z() const
{
		return m_Center_Z;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_1X() const
{
		return m_PCA_Axis_1X;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_1Y() const
{
		return m_PCA_Axis_1Y;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_1Z() const
{
		return m_PCA_Axis_1Z;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_2X() const
{
		return m_PCA_Axis_2X;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_2Y() const
{
		return m_PCA_Axis_2Y;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetPCA_Axis_2Z() const
{
		return m_PCA_Axis_2Z;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetMax_1() const
{
		return m_Max_1;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetMax_2() const
{
		return m_Max_2;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetMax_3() const
{
		return m_Max_3;
}

Mp7Jrsunsigned15Ptr Mp7JrsPerceptual3DShapeType_LocalType::GetConvexity() const
{
		return m_Convexity;
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetVolume(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetVolume().");
	}
	if (m_Volume != item)
	{
		// Dc1Factory::DeleteObject(m_Volume);
		m_Volume = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Volume) m_Volume->SetParent(m_myself.getPointer());
		if (m_Volume && m_Volume->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Volume->UseTypeAttribute = true;
		}
		if(m_Volume != Mp7Jrsunsigned15Ptr())
		{
			m_Volume->SetContentName(XMLString::transcode("Volume"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_X().");
	}
	if (m_Center_X != item)
	{
		// Dc1Factory::DeleteObject(m_Center_X);
		m_Center_X = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Center_X) m_Center_X->SetParent(m_myself.getPointer());
		if (m_Center_X && m_Center_X->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Center_X->UseTypeAttribute = true;
		}
		if(m_Center_X != Mp7Jrsunsigned15Ptr())
		{
			m_Center_X->SetContentName(XMLString::transcode("Center_X"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_Y().");
	}
	if (m_Center_Y != item)
	{
		// Dc1Factory::DeleteObject(m_Center_Y);
		m_Center_Y = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Center_Y) m_Center_Y->SetParent(m_myself.getPointer());
		if (m_Center_Y && m_Center_Y->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Center_Y->UseTypeAttribute = true;
		}
		if(m_Center_Y != Mp7Jrsunsigned15Ptr())
		{
			m_Center_Y->SetContentName(XMLString::transcode("Center_Y"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetCenter_Z().");
	}
	if (m_Center_Z != item)
	{
		// Dc1Factory::DeleteObject(m_Center_Z);
		m_Center_Z = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Center_Z) m_Center_Z->SetParent(m_myself.getPointer());
		if (m_Center_Z && m_Center_Z->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Center_Z->UseTypeAttribute = true;
		}
		if(m_Center_Z != Mp7Jrsunsigned15Ptr())
		{
			m_Center_Z->SetContentName(XMLString::transcode("Center_Z"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1X().");
	}
	if (m_PCA_Axis_1X != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_1X);
		m_PCA_Axis_1X = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_1X) m_PCA_Axis_1X->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_1X && m_PCA_Axis_1X->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_1X->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_1X != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_1X->SetContentName(XMLString::transcode("PCA_Axis_1X"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1Y().");
	}
	if (m_PCA_Axis_1Y != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_1Y);
		m_PCA_Axis_1Y = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_1Y) m_PCA_Axis_1Y->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_1Y && m_PCA_Axis_1Y->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_1Y->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_1Y != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_1Y->SetContentName(XMLString::transcode("PCA_Axis_1Y"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_1Z().");
	}
	if (m_PCA_Axis_1Z != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_1Z);
		m_PCA_Axis_1Z = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_1Z) m_PCA_Axis_1Z->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_1Z && m_PCA_Axis_1Z->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_1Z->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_1Z != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_1Z->SetContentName(XMLString::transcode("PCA_Axis_1Z"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2X(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2X().");
	}
	if (m_PCA_Axis_2X != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_2X);
		m_PCA_Axis_2X = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_2X) m_PCA_Axis_2X->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_2X && m_PCA_Axis_2X->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_2X->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_2X != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_2X->SetContentName(XMLString::transcode("PCA_Axis_2X"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2Y(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2Y().");
	}
	if (m_PCA_Axis_2Y != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_2Y);
		m_PCA_Axis_2Y = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_2Y) m_PCA_Axis_2Y->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_2Y && m_PCA_Axis_2Y->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_2Y->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_2Y != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_2Y->SetContentName(XMLString::transcode("PCA_Axis_2Y"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2Z(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetPCA_Axis_2Z().");
	}
	if (m_PCA_Axis_2Z != item)
	{
		// Dc1Factory::DeleteObject(m_PCA_Axis_2Z);
		m_PCA_Axis_2Z = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PCA_Axis_2Z) m_PCA_Axis_2Z->SetParent(m_myself.getPointer());
		if (m_PCA_Axis_2Z && m_PCA_Axis_2Z->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PCA_Axis_2Z->UseTypeAttribute = true;
		}
		if(m_PCA_Axis_2Z != Mp7Jrsunsigned15Ptr())
		{
			m_PCA_Axis_2Z->SetContentName(XMLString::transcode("PCA_Axis_2Z"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetMax_1(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetMax_1().");
	}
	if (m_Max_1 != item)
	{
		// Dc1Factory::DeleteObject(m_Max_1);
		m_Max_1 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max_1) m_Max_1->SetParent(m_myself.getPointer());
		if (m_Max_1 && m_Max_1->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Max_1->UseTypeAttribute = true;
		}
		if(m_Max_1 != Mp7Jrsunsigned15Ptr())
		{
			m_Max_1->SetContentName(XMLString::transcode("Max_1"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetMax_2(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetMax_2().");
	}
	if (m_Max_2 != item)
	{
		// Dc1Factory::DeleteObject(m_Max_2);
		m_Max_2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max_2) m_Max_2->SetParent(m_myself.getPointer());
		if (m_Max_2 && m_Max_2->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Max_2->UseTypeAttribute = true;
		}
		if(m_Max_2 != Mp7Jrsunsigned15Ptr())
		{
			m_Max_2->SetContentName(XMLString::transcode("Max_2"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetMax_3(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetMax_3().");
	}
	if (m_Max_3 != item)
	{
		// Dc1Factory::DeleteObject(m_Max_3);
		m_Max_3 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max_3) m_Max_3->SetParent(m_myself.getPointer());
		if (m_Max_3 && m_Max_3->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Max_3->UseTypeAttribute = true;
		}
		if(m_Max_3 != Mp7Jrsunsigned15Ptr())
		{
			m_Max_3->SetContentName(XMLString::transcode("Max_3"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType_LocalType::SetConvexity(const Mp7Jrsunsigned15Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType_LocalType::SetConvexity().");
	}
	if (m_Convexity != item)
	{
		// Dc1Factory::DeleteObject(m_Convexity);
		m_Convexity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Convexity) m_Convexity->SetParent(m_myself.getPointer());
		if (m_Convexity && m_Convexity->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Convexity->UseTypeAttribute = true;
		}
		if(m_Convexity != Mp7Jrsunsigned15Ptr())
		{
			m_Convexity->SetContentName(XMLString::transcode("Convexity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: Perceptual3DShapeType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsPerceptual3DShapeType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsPerceptual3DShapeType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPerceptual3DShapeType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPerceptual3DShapeType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Perceptual3DShapeType_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_Volume != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Volume->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Volume"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Volume->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Center_X != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Center_X->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Center_X"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Center_X->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Center_Y != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Center_Y->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Center_Y"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Center_Y->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Center_Z != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Center_Z->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Center_Z"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Center_Z->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_1X != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_1X->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_1X"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_1X->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_1Y != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_1Y->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_1Y"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_1Y->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_1Z != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_1Z->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_1Z"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_1Z->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_2X != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_2X->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_2X"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_2X->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_2Y != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_2Y->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_2Y"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_2Y->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PCA_Axis_2Z != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PCA_Axis_2Z->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PCA_Axis_2Z"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PCA_Axis_2Z->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Max_1 != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Max_1->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Max_1"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Max_1->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Max_2 != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Max_2->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Max_2"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Max_2->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Max_3 != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Max_3->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Max_3"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Max_3->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Convexity != Mp7Jrsunsigned15Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Convexity->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Convexity"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Convexity->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPerceptual3DShapeType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Volume"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Volume")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVolume(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Center_X"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Center_X")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCenter_X(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Center_Y"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Center_Y")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCenter_Y(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Center_Z"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Center_Z")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCenter_Z(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_1X"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_1X")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_1X(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_1Y"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_1Y")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_1Y(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_1Z"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_1Z")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_1Z(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_2X"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_2X")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_2X(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_2Y"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_2Y")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_2Y(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PCA_Axis_2Z"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PCA_Axis_2Z")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPCA_Axis_2Z(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Max_1"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Max_1")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMax_1(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Max_2"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Max_2")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMax_2(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Max_3"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Max_3")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMax_3(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Convexity"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Convexity")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned15; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetConvexity(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptual3DShapeType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Volume")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetVolume(child);
  }
  if (XMLString::compareString(elementname, X("Center_X")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetCenter_X(child);
  }
  if (XMLString::compareString(elementname, X("Center_Y")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetCenter_Y(child);
  }
  if (XMLString::compareString(elementname, X("Center_Z")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetCenter_Z(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_1X")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_1X(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_1Y")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_1Y(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_1Z")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_1Z(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_2X")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_2X(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_2Y")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_2Y(child);
  }
  if (XMLString::compareString(elementname, X("PCA_Axis_2Z")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetPCA_Axis_2Z(child);
  }
  if (XMLString::compareString(elementname, X("Max_1")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetMax_1(child);
  }
  if (XMLString::compareString(elementname, X("Max_2")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetMax_2(child);
  }
  if (XMLString::compareString(elementname, X("Max_3")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetMax_3(child);
  }
  if (XMLString::compareString(elementname, X("Convexity")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned15; // FTT, check this
	}
	this->SetConvexity(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptual3DShapeType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVolume() != Dc1NodePtr())
		result.Insert(GetVolume());
	if (GetCenter_X() != Dc1NodePtr())
		result.Insert(GetCenter_X());
	if (GetCenter_Y() != Dc1NodePtr())
		result.Insert(GetCenter_Y());
	if (GetCenter_Z() != Dc1NodePtr())
		result.Insert(GetCenter_Z());
	if (GetPCA_Axis_1X() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_1X());
	if (GetPCA_Axis_1Y() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_1Y());
	if (GetPCA_Axis_1Z() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_1Z());
	if (GetPCA_Axis_2X() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_2X());
	if (GetPCA_Axis_2Y() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_2Y());
	if (GetPCA_Axis_2Z() != Dc1NodePtr())
		result.Insert(GetPCA_Axis_2Z());
	if (GetMax_1() != Dc1NodePtr())
		result.Insert(GetMax_1());
	if (GetMax_2() != Dc1NodePtr())
		result.Insert(GetMax_2());
	if (GetMax_3() != Dc1NodePtr())
		result.Insert(GetMax_3());
	if (GetConvexity() != Dc1NodePtr())
		result.Insert(GetConvexity());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_LocalType_ExtMethodImpl.h


