
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtImplInclude.h


#include "Mp7JrsFragmentUpdateCommandType.h"
#include "Mp7JrsFragmentUpdateContextType.h"
#include "Mp7JrsFragmentUpdatePayloadType.h"
#include "Mp7JrsFragmentUpdateUnitType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFragmentUpdateUnitType::IMp7JrsFragmentUpdateUnitType()
{

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtPropInit.h

}

IMp7JrsFragmentUpdateUnitType::~IMp7JrsFragmentUpdateUnitType()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtPropCleanup.h

}

Mp7JrsFragmentUpdateUnitType::Mp7JrsFragmentUpdateUnitType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFragmentUpdateUnitType::~Mp7JrsFragmentUpdateUnitType()
{
	Cleanup();
}

void Mp7JrsFragmentUpdateUnitType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FUCommand = Mp7JrsFragmentUpdateCommandPtr(); // Class
	m_FUContext = Mp7JrsFragmentUpdateContextPtr(); // Class
	m_FUContext_Exist = false;
	m_FUPayload = Mp7JrsFragmentUpdatePayloadPtr(); // Class
	m_FUPayload_Exist = false;


// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtMyPropInit.h

}

void Mp7JrsFragmentUpdateUnitType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FUCommand);
	// Dc1Factory::DeleteObject(m_FUContext);
	// Dc1Factory::DeleteObject(m_FUPayload);
}

void Mp7JrsFragmentUpdateUnitType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FragmentUpdateUnitTypePtr, since we
	// might need GetBase(), which isn't defined in IFragmentUpdateUnitType
	const Dc1Ptr< Mp7JrsFragmentUpdateUnitType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_FUCommand);
		this->SetFUCommand(Dc1Factory::CloneObject(tmp->GetFUCommand()));
	if (tmp->IsValidFUContext())
	{
		// Dc1Factory::DeleteObject(m_FUContext);
		this->SetFUContext(Dc1Factory::CloneObject(tmp->GetFUContext()));
	}
	else
	{
		InvalidateFUContext();
	}
	if (tmp->IsValidFUPayload())
	{
		// Dc1Factory::DeleteObject(m_FUPayload);
		this->SetFUPayload(Dc1Factory::CloneObject(tmp->GetFUPayload()));
	}
	else
	{
		InvalidateFUPayload();
	}
}

Dc1NodePtr Mp7JrsFragmentUpdateUnitType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFragmentUpdateUnitType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsFragmentUpdateCommandPtr Mp7JrsFragmentUpdateUnitType::GetFUCommand() const
{
		return m_FUCommand;
}

Mp7JrsFragmentUpdateContextPtr Mp7JrsFragmentUpdateUnitType::GetFUContext() const
{
		return m_FUContext;
}

// Element is optional
bool Mp7JrsFragmentUpdateUnitType::IsValidFUContext() const
{
	return m_FUContext_Exist;
}

Mp7JrsFragmentUpdatePayloadPtr Mp7JrsFragmentUpdateUnitType::GetFUPayload() const
{
		return m_FUPayload;
}

// Element is optional
bool Mp7JrsFragmentUpdateUnitType::IsValidFUPayload() const
{
	return m_FUPayload_Exist;
}

void Mp7JrsFragmentUpdateUnitType::SetFUCommand(const Mp7JrsFragmentUpdateCommandPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateUnitType::SetFUCommand().");
	}
	if (m_FUCommand != item)
	{
		// Dc1Factory::DeleteObject(m_FUCommand);
		m_FUCommand = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FUCommand) m_FUCommand->SetParent(m_myself.getPointer());
		if (m_FUCommand && m_FUCommand->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FUCommand->UseTypeAttribute = true;
		}
		if(m_FUCommand != Mp7JrsFragmentUpdateCommandPtr())
		{
			m_FUCommand->SetContentName(XMLString::transcode("FUCommand"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdateUnitType::SetFUContext(const Mp7JrsFragmentUpdateContextPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateUnitType::SetFUContext().");
	}
	if (m_FUContext != item || m_FUContext_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FUContext);
		m_FUContext = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FUContext) m_FUContext->SetParent(m_myself.getPointer());
		if (m_FUContext && m_FUContext->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateContextType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FUContext->UseTypeAttribute = true;
		}
		m_FUContext_Exist = true;
		if(m_FUContext != Mp7JrsFragmentUpdateContextPtr())
		{
			m_FUContext->SetContentName(XMLString::transcode("FUContext"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdateUnitType::InvalidateFUContext()
{
	m_FUContext_Exist = false;
}
void Mp7JrsFragmentUpdateUnitType::SetFUPayload(const Mp7JrsFragmentUpdatePayloadPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateUnitType::SetFUPayload().");
	}
	if (m_FUPayload != item || m_FUPayload_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FUPayload);
		m_FUPayload = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FUPayload) m_FUPayload->SetParent(m_myself.getPointer());
		if (m_FUPayload && m_FUPayload->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdatePayloadType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FUPayload->UseTypeAttribute = true;
		}
		m_FUPayload_Exist = true;
		if(m_FUPayload != Mp7JrsFragmentUpdatePayloadPtr())
		{
			m_FUPayload->SetContentName(XMLString::transcode("FUPayload"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdateUnitType::InvalidateFUPayload()
{
	m_FUPayload_Exist = false;
}

Dc1NodeEnum Mp7JrsFragmentUpdateUnitType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("FUCommand")) == 0)
	{
		// FUCommand is simple element FragmentUpdateCommandType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFUCommand()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFragmentUpdateCommandPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFUCommand(p, client);
					if((p = GetFUCommand()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FUContext")) == 0)
	{
		// FUContext is simple element FragmentUpdateContextType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFUContext()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateContextType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFragmentUpdateContextPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFUContext(p, client);
					if((p = GetFUContext()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FUPayload")) == 0)
	{
		// FUPayload is simple element FragmentUpdatePayloadType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFUPayload()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdatePayloadType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFragmentUpdatePayloadPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFUPayload(p, client);
					if((p = GetFUPayload()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FragmentUpdateUnitType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FragmentUpdateUnitType");
	}
	return result;
}

XMLCh * Mp7JrsFragmentUpdateUnitType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFragmentUpdateUnitType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFragmentUpdateUnitType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdateUnitType"));
	// Element serialization:
	// Class
	
	if (m_FUCommand != Mp7JrsFragmentUpdateCommandPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FUCommand->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FUCommand"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FUCommand->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FUContext != Mp7JrsFragmentUpdateContextPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FUContext->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FUContext"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FUContext->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FUPayload != Mp7JrsFragmentUpdatePayloadPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FUPayload->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FUPayload"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FUPayload->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFragmentUpdateUnitType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FUCommand"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FUCommand")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFragmentUpdateCommandType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFUCommand(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FUContext"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FUContext")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFragmentUpdateContextType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFUContext(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FUPayload"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FUPayload")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFragmentUpdatePayloadType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFUPayload(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFragmentUpdateUnitType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FUCommand")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFragmentUpdateCommandType; // FTT, check this
	}
	this->SetFUCommand(child);
  }
  if (XMLString::compareString(elementname, X("FUContext")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFragmentUpdateContextType; // FTT, check this
	}
	this->SetFUContext(child);
  }
  if (XMLString::compareString(elementname, X("FUPayload")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFragmentUpdatePayloadType; // FTT, check this
	}
	this->SetFUPayload(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFragmentUpdateUnitType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFUCommand() != Dc1NodePtr())
		result.Insert(GetFUCommand());
	if (GetFUContext() != Dc1NodePtr())
		result.Insert(GetFUContext());
	if (GetFUPayload() != Dc1NodePtr())
		result.Insert(GetFUPayload());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateUnitType_ExtMethodImpl.h


