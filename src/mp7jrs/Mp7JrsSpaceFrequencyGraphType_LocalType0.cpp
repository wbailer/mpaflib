
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtImplInclude.h


#include "Mp7JrsSpaceFrequencyGraphType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsSpaceFrequencyGraphType_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpaceFrequencyGraphType_LocalType0::IMp7JrsSpaceFrequencyGraphType_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtPropInit.h

}

IMp7JrsSpaceFrequencyGraphType_LocalType0::~IMp7JrsSpaceFrequencyGraphType_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtPropCleanup.h

}

Mp7JrsSpaceFrequencyGraphType_LocalType0::Mp7JrsSpaceFrequencyGraphType_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpaceFrequencyGraphType_LocalType0::~Mp7JrsSpaceFrequencyGraphType_LocalType0()
{
	Cleanup();
}

void Mp7JrsSpaceFrequencyGraphType_LocalType0::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FrequencyChild = Mp7JrsSpaceFrequencyGraphPtr(); // Class
	m_FrequencyChildRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtMyPropInit.h

}

void Mp7JrsSpaceFrequencyGraphType_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FrequencyChild);
	// Dc1Factory::DeleteObject(m_FrequencyChildRef);
}

void Mp7JrsSpaceFrequencyGraphType_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpaceFrequencyGraphType_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in ISpaceFrequencyGraphType_LocalType0
	const Dc1Ptr< Mp7JrsSpaceFrequencyGraphType_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_FrequencyChild);
		this->SetFrequencyChild(Dc1Factory::CloneObject(tmp->GetFrequencyChild()));
		// Dc1Factory::DeleteObject(m_FrequencyChildRef);
		this->SetFrequencyChildRef(Dc1Factory::CloneObject(tmp->GetFrequencyChildRef()));
}

Dc1NodePtr Mp7JrsSpaceFrequencyGraphType_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpaceFrequencyGraphType_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSpaceFrequencyGraphPtr Mp7JrsSpaceFrequencyGraphType_LocalType0::GetFrequencyChild() const
{
		return m_FrequencyChild;
}

Mp7JrsReferencePtr Mp7JrsSpaceFrequencyGraphType_LocalType0::GetFrequencyChildRef() const
{
		return m_FrequencyChildRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsSpaceFrequencyGraphType_LocalType0::SetFrequencyChild(const Mp7JrsSpaceFrequencyGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType_LocalType0::SetFrequencyChild().");
	}
	if (m_FrequencyChild != item)
	{
		// Dc1Factory::DeleteObject(m_FrequencyChild);
		m_FrequencyChild = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrequencyChild) m_FrequencyChild->SetParent(m_myself.getPointer());
		if (m_FrequencyChild && m_FrequencyChild->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrequencyChild->UseTypeAttribute = true;
		}
		if(m_FrequencyChild != Mp7JrsSpaceFrequencyGraphPtr())
		{
			m_FrequencyChild->SetContentName(XMLString::transcode("FrequencyChild"));
		}
	// Dc1Factory::DeleteObject(m_FrequencyChildRef);
	m_FrequencyChildRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpaceFrequencyGraphType_LocalType0::SetFrequencyChildRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType_LocalType0::SetFrequencyChildRef().");
	}
	if (m_FrequencyChildRef != item)
	{
		// Dc1Factory::DeleteObject(m_FrequencyChildRef);
		m_FrequencyChildRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrequencyChildRef) m_FrequencyChildRef->SetParent(m_myself.getPointer());
		if (m_FrequencyChildRef && m_FrequencyChildRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrequencyChildRef->UseTypeAttribute = true;
		}
		if(m_FrequencyChildRef != Mp7JrsReferencePtr())
		{
			m_FrequencyChildRef->SetContentName(XMLString::transcode("FrequencyChildRef"));
		}
	// Dc1Factory::DeleteObject(m_FrequencyChild);
	m_FrequencyChild = Mp7JrsSpaceFrequencyGraphPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SpaceFrequencyGraphType_LocalType0: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpaceFrequencyGraphType_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpaceFrequencyGraphType_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpaceFrequencyGraphType_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpaceFrequencyGraphType_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_FrequencyChild != Mp7JrsSpaceFrequencyGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrequencyChild->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrequencyChild"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrequencyChild->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FrequencyChildRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrequencyChildRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrequencyChildRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrequencyChildRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpaceFrequencyGraphType_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrequencyChild"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrequencyChild")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpaceFrequencyGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrequencyChild(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrequencyChildRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrequencyChildRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpaceFrequencyGraphType_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FrequencyChild")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpaceFrequencyGraphType; // FTT, check this
	}
	this->SetFrequencyChild(child);
  }
  if (XMLString::compareString(elementname, X("FrequencyChildRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetFrequencyChildRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpaceFrequencyGraphType_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFrequencyChild() != Dc1NodePtr())
		result.Insert(GetFrequencyChild());
	if (GetFrequencyChildRef() != Dc1NodePtr())
		result.Insert(GetFrequencyChildRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_LocalType0_ExtMethodImpl.h


