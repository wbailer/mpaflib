
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsPerceptualEnergyType.h"
#include "Mp7JrsPerceptualValenceType.h"
#include "Mp7JrsPerceptualBeatType.h"
#include "Mp7JrsPerceptualTempoType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsLinearPerceptualAttributeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsLinearPerceptualAttributeType::IMp7JrsLinearPerceptualAttributeType()
{

// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtPropInit.h

}

IMp7JrsLinearPerceptualAttributeType::~IMp7JrsLinearPerceptualAttributeType()
{
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtPropCleanup.h

}

Mp7JrsLinearPerceptualAttributeType::Mp7JrsLinearPerceptualAttributeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsLinearPerceptualAttributeType::~Mp7JrsLinearPerceptualAttributeType()
{
	Cleanup();
}

void Mp7JrsLinearPerceptualAttributeType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_PerceptualEnergy = Mp7JrsPerceptualEnergyPtr(); // Class
	m_PerceptualEnergy_Exist = false;
	m_PerceptualValence = Mp7JrsPerceptualValencePtr(); // Class
	m_PerceptualValence_Exist = false;
	m_PerceptualBeat = Mp7JrsPerceptualBeatPtr(); // Class
	m_PerceptualBeat_Exist = false;
	m_PerceptualTempo = Mp7JrsPerceptualTempoPtr(); // Class
	m_PerceptualTempo_Exist = false;


// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMyPropInit.h

}

void Mp7JrsLinearPerceptualAttributeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PerceptualEnergy);
	// Dc1Factory::DeleteObject(m_PerceptualValence);
	// Dc1Factory::DeleteObject(m_PerceptualBeat);
	// Dc1Factory::DeleteObject(m_PerceptualTempo);
}

void Mp7JrsLinearPerceptualAttributeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use LinearPerceptualAttributeTypePtr, since we
	// might need GetBase(), which isn't defined in ILinearPerceptualAttributeType
	const Dc1Ptr< Mp7JrsLinearPerceptualAttributeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsLinearPerceptualAttributeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidPerceptualEnergy())
	{
		// Dc1Factory::DeleteObject(m_PerceptualEnergy);
		this->SetPerceptualEnergy(Dc1Factory::CloneObject(tmp->GetPerceptualEnergy()));
	}
	else
	{
		InvalidatePerceptualEnergy();
	}
	if (tmp->IsValidPerceptualValence())
	{
		// Dc1Factory::DeleteObject(m_PerceptualValence);
		this->SetPerceptualValence(Dc1Factory::CloneObject(tmp->GetPerceptualValence()));
	}
	else
	{
		InvalidatePerceptualValence();
	}
	if (tmp->IsValidPerceptualBeat())
	{
		// Dc1Factory::DeleteObject(m_PerceptualBeat);
		this->SetPerceptualBeat(Dc1Factory::CloneObject(tmp->GetPerceptualBeat()));
	}
	else
	{
		InvalidatePerceptualBeat();
	}
	if (tmp->IsValidPerceptualTempo())
	{
		// Dc1Factory::DeleteObject(m_PerceptualTempo);
		this->SetPerceptualTempo(Dc1Factory::CloneObject(tmp->GetPerceptualTempo()));
	}
	else
	{
		InvalidatePerceptualTempo();
	}
}

Dc1NodePtr Mp7JrsLinearPerceptualAttributeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsLinearPerceptualAttributeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsLinearPerceptualAttributeType::GetBase() const
{
	return m_Base;
}

Mp7JrsPerceptualEnergyPtr Mp7JrsLinearPerceptualAttributeType::GetPerceptualEnergy() const
{
		return m_PerceptualEnergy;
}

// Element is optional
bool Mp7JrsLinearPerceptualAttributeType::IsValidPerceptualEnergy() const
{
	return m_PerceptualEnergy_Exist;
}

Mp7JrsPerceptualValencePtr Mp7JrsLinearPerceptualAttributeType::GetPerceptualValence() const
{
		return m_PerceptualValence;
}

// Element is optional
bool Mp7JrsLinearPerceptualAttributeType::IsValidPerceptualValence() const
{
	return m_PerceptualValence_Exist;
}

Mp7JrsPerceptualBeatPtr Mp7JrsLinearPerceptualAttributeType::GetPerceptualBeat() const
{
		return m_PerceptualBeat;
}

// Element is optional
bool Mp7JrsLinearPerceptualAttributeType::IsValidPerceptualBeat() const
{
	return m_PerceptualBeat_Exist;
}

Mp7JrsPerceptualTempoPtr Mp7JrsLinearPerceptualAttributeType::GetPerceptualTempo() const
{
		return m_PerceptualTempo;
}

// Element is optional
bool Mp7JrsLinearPerceptualAttributeType::IsValidPerceptualTempo() const
{
	return m_PerceptualTempo_Exist;
}

void Mp7JrsLinearPerceptualAttributeType::SetPerceptualEnergy(const Mp7JrsPerceptualEnergyPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinearPerceptualAttributeType::SetPerceptualEnergy().");
	}
	if (m_PerceptualEnergy != item || m_PerceptualEnergy_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualEnergy);
		m_PerceptualEnergy = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualEnergy) m_PerceptualEnergy->SetParent(m_myself.getPointer());
		if (m_PerceptualEnergy && m_PerceptualEnergy->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualEnergyType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualEnergy->UseTypeAttribute = true;
		}
		m_PerceptualEnergy_Exist = true;
		if(m_PerceptualEnergy != Mp7JrsPerceptualEnergyPtr())
		{
			m_PerceptualEnergy->SetContentName(XMLString::transcode("PerceptualEnergy"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinearPerceptualAttributeType::InvalidatePerceptualEnergy()
{
	m_PerceptualEnergy_Exist = false;
}
void Mp7JrsLinearPerceptualAttributeType::SetPerceptualValence(const Mp7JrsPerceptualValencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinearPerceptualAttributeType::SetPerceptualValence().");
	}
	if (m_PerceptualValence != item || m_PerceptualValence_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualValence);
		m_PerceptualValence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualValence) m_PerceptualValence->SetParent(m_myself.getPointer());
		if (m_PerceptualValence && m_PerceptualValence->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualValenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualValence->UseTypeAttribute = true;
		}
		m_PerceptualValence_Exist = true;
		if(m_PerceptualValence != Mp7JrsPerceptualValencePtr())
		{
			m_PerceptualValence->SetContentName(XMLString::transcode("PerceptualValence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinearPerceptualAttributeType::InvalidatePerceptualValence()
{
	m_PerceptualValence_Exist = false;
}
void Mp7JrsLinearPerceptualAttributeType::SetPerceptualBeat(const Mp7JrsPerceptualBeatPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinearPerceptualAttributeType::SetPerceptualBeat().");
	}
	if (m_PerceptualBeat != item || m_PerceptualBeat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualBeat);
		m_PerceptualBeat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualBeat) m_PerceptualBeat->SetParent(m_myself.getPointer());
		if (m_PerceptualBeat && m_PerceptualBeat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualBeatType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualBeat->UseTypeAttribute = true;
		}
		m_PerceptualBeat_Exist = true;
		if(m_PerceptualBeat != Mp7JrsPerceptualBeatPtr())
		{
			m_PerceptualBeat->SetContentName(XMLString::transcode("PerceptualBeat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinearPerceptualAttributeType::InvalidatePerceptualBeat()
{
	m_PerceptualBeat_Exist = false;
}
void Mp7JrsLinearPerceptualAttributeType::SetPerceptualTempo(const Mp7JrsPerceptualTempoPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinearPerceptualAttributeType::SetPerceptualTempo().");
	}
	if (m_PerceptualTempo != item || m_PerceptualTempo_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualTempo);
		m_PerceptualTempo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualTempo) m_PerceptualTempo->SetParent(m_myself.getPointer());
		if (m_PerceptualTempo && m_PerceptualTempo->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualTempoType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualTempo->UseTypeAttribute = true;
		}
		m_PerceptualTempo_Exist = true;
		if(m_PerceptualTempo != Mp7JrsPerceptualTempoPtr())
		{
			m_PerceptualTempo->SetContentName(XMLString::transcode("PerceptualTempo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinearPerceptualAttributeType::InvalidatePerceptualTempo()
{
	m_PerceptualTempo_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsLinearPerceptualAttributeType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsLinearPerceptualAttributeType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsLinearPerceptualAttributeType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinearPerceptualAttributeType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinearPerceptualAttributeType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsLinearPerceptualAttributeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("PerceptualEnergy")) == 0)
	{
		// PerceptualEnergy is simple element PerceptualEnergyType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualEnergy()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualEnergyType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualEnergyPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualEnergy(p, client);
					if((p = GetPerceptualEnergy()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualValence")) == 0)
	{
		// PerceptualValence is simple element PerceptualValenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualValence()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualValenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualValencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualValence(p, client);
					if((p = GetPerceptualValence()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualBeat")) == 0)
	{
		// PerceptualBeat is simple element PerceptualBeatType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualBeat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualBeatType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualBeatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualBeat(p, client);
					if((p = GetPerceptualBeat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualTempo")) == 0)
	{
		// PerceptualTempo is simple element PerceptualTempoType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualTempo()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualTempoType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualTempoPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualTempo(p, client);
					if((p = GetPerceptualTempo()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for LinearPerceptualAttributeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "LinearPerceptualAttributeType");
	}
	return result;
}

XMLCh * Mp7JrsLinearPerceptualAttributeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsLinearPerceptualAttributeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsLinearPerceptualAttributeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("LinearPerceptualAttributeType"));
	// Element serialization:
	// Class
	
	if (m_PerceptualEnergy != Mp7JrsPerceptualEnergyPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualEnergy->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualEnergy"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualEnergy->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualValence != Mp7JrsPerceptualValencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualValence->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualValence"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualValence->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualBeat != Mp7JrsPerceptualBeatPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualBeat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualBeat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualBeat->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualTempo != Mp7JrsPerceptualTempoPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualTempo->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualTempo"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualTempo->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsLinearPerceptualAttributeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualEnergy"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualEnergy")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualEnergyType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualEnergy(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualValence"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualValence")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualValenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualValence(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualBeat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualBeat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualBeatType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualBeat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualTempo"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualTempo")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualTempoType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualTempo(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsLinearPerceptualAttributeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("PerceptualEnergy")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualEnergyType; // FTT, check this
	}
	this->SetPerceptualEnergy(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualValence")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualValenceType; // FTT, check this
	}
	this->SetPerceptualValence(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualBeat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualBeatType; // FTT, check this
	}
	this->SetPerceptualBeat(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualTempo")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualTempoType; // FTT, check this
	}
	this->SetPerceptualTempo(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsLinearPerceptualAttributeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPerceptualEnergy() != Dc1NodePtr())
		result.Insert(GetPerceptualEnergy());
	if (GetPerceptualValence() != Dc1NodePtr())
		result.Insert(GetPerceptualValence());
	if (GetPerceptualBeat() != Dc1NodePtr())
		result.Insert(GetPerceptualBeat());
	if (GetPerceptualTempo() != Dc1NodePtr())
		result.Insert(GetPerceptualTempo());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsLinearPerceptualAttributeType_ExtMethodImpl.h


