
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsCreationType_Title_CollectionType.h"
#include "Mp7JrsTitleMediaType.h"
#include "Mp7JrsCreationType_Abstract_CollectionType.h"
#include "Mp7JrsCreationType_Creator_CollectionType.h"
#include "Mp7JrsCreationType_CreationCoordinates_CollectionType.h"
#include "Mp7JrsCreationType_CreationTool_CollectionType.h"
#include "Mp7JrsCreationType_CopyrightString_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsCreationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTitleType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Title
#include "Mp7JrsTextAnnotationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Abstract
#include "Mp7JrsCreatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Creator
#include "Mp7JrsCreationType_CreationCoordinates_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CreationCoordinates
#include "Mp7JrsCreationToolType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CreationTool
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CopyrightString

#include <assert.h>
IMp7JrsCreationType::IMp7JrsCreationType()
{

// no includefile for extension defined 
// file Mp7JrsCreationType_ExtPropInit.h

}

IMp7JrsCreationType::~IMp7JrsCreationType()
{
// no includefile for extension defined 
// file Mp7JrsCreationType_ExtPropCleanup.h

}

Mp7JrsCreationType::Mp7JrsCreationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreationType::~Mp7JrsCreationType()
{
	Cleanup();
}

void Mp7JrsCreationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Title = Mp7JrsCreationType_Title_CollectionPtr(); // Collection
	m_TitleMedia = Mp7JrsTitleMediaPtr(); // Class
	m_TitleMedia_Exist = false;
	m_Abstract = Mp7JrsCreationType_Abstract_CollectionPtr(); // Collection
	m_Creator = Mp7JrsCreationType_Creator_CollectionPtr(); // Collection
	m_CreationCoordinates = Mp7JrsCreationType_CreationCoordinates_CollectionPtr(); // Collection
	m_CreationTool = Mp7JrsCreationType_CreationTool_CollectionPtr(); // Collection
	m_CopyrightString = Mp7JrsCreationType_CopyrightString_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCreationType_ExtMyPropInit.h

}

void Mp7JrsCreationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Title);
	// Dc1Factory::DeleteObject(m_TitleMedia);
	// Dc1Factory::DeleteObject(m_Abstract);
	// Dc1Factory::DeleteObject(m_Creator);
	// Dc1Factory::DeleteObject(m_CreationCoordinates);
	// Dc1Factory::DeleteObject(m_CreationTool);
	// Dc1Factory::DeleteObject(m_CopyrightString);
}

void Mp7JrsCreationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreationTypePtr, since we
	// might need GetBase(), which isn't defined in ICreationType
	const Dc1Ptr< Mp7JrsCreationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCreationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Title);
		this->SetTitle(Dc1Factory::CloneObject(tmp->GetTitle()));
	if (tmp->IsValidTitleMedia())
	{
		// Dc1Factory::DeleteObject(m_TitleMedia);
		this->SetTitleMedia(Dc1Factory::CloneObject(tmp->GetTitleMedia()));
	}
	else
	{
		InvalidateTitleMedia();
	}
		// Dc1Factory::DeleteObject(m_Abstract);
		this->SetAbstract(Dc1Factory::CloneObject(tmp->GetAbstract()));
		// Dc1Factory::DeleteObject(m_Creator);
		this->SetCreator(Dc1Factory::CloneObject(tmp->GetCreator()));
		// Dc1Factory::DeleteObject(m_CreationCoordinates);
		this->SetCreationCoordinates(Dc1Factory::CloneObject(tmp->GetCreationCoordinates()));
		// Dc1Factory::DeleteObject(m_CreationTool);
		this->SetCreationTool(Dc1Factory::CloneObject(tmp->GetCreationTool()));
		// Dc1Factory::DeleteObject(m_CopyrightString);
		this->SetCopyrightString(Dc1Factory::CloneObject(tmp->GetCopyrightString()));
}

Dc1NodePtr Mp7JrsCreationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCreationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsCreationType::GetBase() const
{
	return m_Base;
}

Mp7JrsCreationType_Title_CollectionPtr Mp7JrsCreationType::GetTitle() const
{
		return m_Title;
}

Mp7JrsTitleMediaPtr Mp7JrsCreationType::GetTitleMedia() const
{
		return m_TitleMedia;
}

// Element is optional
bool Mp7JrsCreationType::IsValidTitleMedia() const
{
	return m_TitleMedia_Exist;
}

Mp7JrsCreationType_Abstract_CollectionPtr Mp7JrsCreationType::GetAbstract() const
{
		return m_Abstract;
}

Mp7JrsCreationType_Creator_CollectionPtr Mp7JrsCreationType::GetCreator() const
{
		return m_Creator;
}

Mp7JrsCreationType_CreationCoordinates_CollectionPtr Mp7JrsCreationType::GetCreationCoordinates() const
{
		return m_CreationCoordinates;
}

Mp7JrsCreationType_CreationTool_CollectionPtr Mp7JrsCreationType::GetCreationTool() const
{
		return m_CreationTool;
}

Mp7JrsCreationType_CopyrightString_CollectionPtr Mp7JrsCreationType::GetCopyrightString() const
{
		return m_CopyrightString;
}

void Mp7JrsCreationType::SetTitle(const Mp7JrsCreationType_Title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetTitle().");
	}
	if (m_Title != item)
	{
		// Dc1Factory::DeleteObject(m_Title);
		m_Title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Title) m_Title->SetParent(m_myself.getPointer());
		if(m_Title != Mp7JrsCreationType_Title_CollectionPtr())
		{
			m_Title->SetContentName(XMLString::transcode("Title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::SetTitleMedia(const Mp7JrsTitleMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetTitleMedia().");
	}
	if (m_TitleMedia != item || m_TitleMedia_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TitleMedia);
		m_TitleMedia = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TitleMedia) m_TitleMedia->SetParent(m_myself.getPointer());
		if (m_TitleMedia && m_TitleMedia->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleMediaType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TitleMedia->UseTypeAttribute = true;
		}
		m_TitleMedia_Exist = true;
		if(m_TitleMedia != Mp7JrsTitleMediaPtr())
		{
			m_TitleMedia->SetContentName(XMLString::transcode("TitleMedia"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::InvalidateTitleMedia()
{
	m_TitleMedia_Exist = false;
}
void Mp7JrsCreationType::SetAbstract(const Mp7JrsCreationType_Abstract_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetAbstract().");
	}
	if (m_Abstract != item)
	{
		// Dc1Factory::DeleteObject(m_Abstract);
		m_Abstract = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Abstract) m_Abstract->SetParent(m_myself.getPointer());
		if(m_Abstract != Mp7JrsCreationType_Abstract_CollectionPtr())
		{
			m_Abstract->SetContentName(XMLString::transcode("Abstract"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::SetCreator(const Mp7JrsCreationType_Creator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetCreator().");
	}
	if (m_Creator != item)
	{
		// Dc1Factory::DeleteObject(m_Creator);
		m_Creator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Creator) m_Creator->SetParent(m_myself.getPointer());
		if(m_Creator != Mp7JrsCreationType_Creator_CollectionPtr())
		{
			m_Creator->SetContentName(XMLString::transcode("Creator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::SetCreationCoordinates(const Mp7JrsCreationType_CreationCoordinates_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetCreationCoordinates().");
	}
	if (m_CreationCoordinates != item)
	{
		// Dc1Factory::DeleteObject(m_CreationCoordinates);
		m_CreationCoordinates = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationCoordinates) m_CreationCoordinates->SetParent(m_myself.getPointer());
		if(m_CreationCoordinates != Mp7JrsCreationType_CreationCoordinates_CollectionPtr())
		{
			m_CreationCoordinates->SetContentName(XMLString::transcode("CreationCoordinates"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::SetCreationTool(const Mp7JrsCreationType_CreationTool_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetCreationTool().");
	}
	if (m_CreationTool != item)
	{
		// Dc1Factory::DeleteObject(m_CreationTool);
		m_CreationTool = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationTool) m_CreationTool->SetParent(m_myself.getPointer());
		if(m_CreationTool != Mp7JrsCreationType_CreationTool_CollectionPtr())
		{
			m_CreationTool->SetContentName(XMLString::transcode("CreationTool"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::SetCopyrightString(const Mp7JrsCreationType_CopyrightString_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetCopyrightString().");
	}
	if (m_CopyrightString != item)
	{
		// Dc1Factory::DeleteObject(m_CopyrightString);
		m_CopyrightString = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CopyrightString) m_CopyrightString->SetParent(m_myself.getPointer());
		if(m_CopyrightString != Mp7JrsCreationType_CopyrightString_CollectionPtr())
		{
			m_CopyrightString->SetContentName(XMLString::transcode("CopyrightString"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsCreationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsCreationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsCreationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsCreationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsCreationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsCreationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsCreationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsCreationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsCreationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsCreationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsCreationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsCreationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsCreationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsCreationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsCreationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsCreationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsCreationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCreationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Title")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Title is item of type TitleType
		// in element collection CreationType_Title_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_Title_CollectionPtr coll = GetTitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_Title_CollectionType; // FTT, check this
				SetTitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTitlePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TitleMedia")) == 0)
	{
		// TitleMedia is simple element TitleMediaType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTitleMedia()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleMediaType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTitleMediaPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTitleMedia(p, client);
					if((p = GetTitleMedia()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Abstract")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Abstract is item of type TextAnnotationType
		// in element collection CreationType_Abstract_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_Abstract_CollectionPtr coll = GetAbstract();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_Abstract_CollectionType; // FTT, check this
				SetAbstract(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Creator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Creator is item of type CreatorType
		// in element collection CreationType_Creator_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_Creator_CollectionPtr coll = GetCreator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_Creator_CollectionType; // FTT, check this
				SetCreator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationCoordinates")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CreationCoordinates is item of type CreationType_CreationCoordinates_LocalType
		// in element collection CreationType_CreationCoordinates_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_CreationCoordinates_CollectionPtr coll = GetCreationCoordinates();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_CreationCoordinates_CollectionType; // FTT, check this
				SetCreationCoordinates(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationType_CreationCoordinates_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationType_CreationCoordinates_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationTool")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CreationTool is item of type CreationToolType
		// in element collection CreationType_CreationTool_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_CreationTool_CollectionPtr coll = GetCreationTool();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_CreationTool_CollectionType; // FTT, check this
				SetCreationTool(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CopyrightString")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CopyrightString is item of type TextualType
		// in element collection CreationType_CopyrightString_CollectionType
		
		context->Found = true;
		Mp7JrsCreationType_CopyrightString_CollectionPtr coll = GetCopyrightString();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationType_CopyrightString_CollectionType; // FTT, check this
				SetCopyrightString(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreationType");
	}
	return result;
}

XMLCh * Mp7JrsCreationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsCreationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsCreationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCreationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreationType"));
	// Element serialization:
	if (m_Title != Mp7JrsCreationType_Title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Title->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_TitleMedia != Mp7JrsTitleMediaPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TitleMedia->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TitleMedia"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TitleMedia->Serialize(doc, element, &elem);
	}
	if (m_Abstract != Mp7JrsCreationType_Abstract_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Abstract->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Creator != Mp7JrsCreationType_Creator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Creator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_CreationCoordinates != Mp7JrsCreationType_CreationCoordinates_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CreationCoordinates->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_CreationTool != Mp7JrsCreationType_CreationTool_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CreationTool->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_CopyrightString != Mp7JrsCreationType_CopyrightString_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CopyrightString->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCreationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Title")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_Title_CollectionPtr tmp = CreateCreationType_Title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TitleMedia"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TitleMedia")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTitleMediaType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitleMedia(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Abstract"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Abstract")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_Abstract_CollectionPtr tmp = CreateCreationType_Abstract_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAbstract(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Creator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Creator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_Creator_CollectionPtr tmp = CreateCreationType_Creator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CreationCoordinates"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CreationCoordinates")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_CreationCoordinates_CollectionPtr tmp = CreateCreationType_CreationCoordinates_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreationCoordinates(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CreationTool"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CreationTool")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_CreationTool_CollectionPtr tmp = CreateCreationType_CreationTool_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreationTool(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CopyrightString"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CopyrightString")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationType_CopyrightString_CollectionPtr tmp = CreateCreationType_CopyrightString_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCopyrightString(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCreationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Title")) == 0))
  {
	Mp7JrsCreationType_Title_CollectionPtr tmp = CreateCreationType_Title_CollectionType; // FTT, check this
	this->SetTitle(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("TitleMedia")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTitleMediaType; // FTT, check this
	}
	this->SetTitleMedia(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Abstract")) == 0))
  {
	Mp7JrsCreationType_Abstract_CollectionPtr tmp = CreateCreationType_Abstract_CollectionType; // FTT, check this
	this->SetAbstract(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Creator")) == 0))
  {
	Mp7JrsCreationType_Creator_CollectionPtr tmp = CreateCreationType_Creator_CollectionType; // FTT, check this
	this->SetCreator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CreationCoordinates")) == 0))
  {
	Mp7JrsCreationType_CreationCoordinates_CollectionPtr tmp = CreateCreationType_CreationCoordinates_CollectionType; // FTT, check this
	this->SetCreationCoordinates(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CreationTool")) == 0))
  {
	Mp7JrsCreationType_CreationTool_CollectionPtr tmp = CreateCreationType_CreationTool_CollectionType; // FTT, check this
	this->SetCreationTool(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CopyrightString")) == 0))
  {
	Mp7JrsCreationType_CopyrightString_CollectionPtr tmp = CreateCreationType_CopyrightString_CollectionType; // FTT, check this
	this->SetCopyrightString(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTitle() != Dc1NodePtr())
		result.Insert(GetTitle());
	if (GetTitleMedia() != Dc1NodePtr())
		result.Insert(GetTitleMedia());
	if (GetAbstract() != Dc1NodePtr())
		result.Insert(GetAbstract());
	if (GetCreator() != Dc1NodePtr())
		result.Insert(GetCreator());
	if (GetCreationCoordinates() != Dc1NodePtr())
		result.Insert(GetCreationCoordinates());
	if (GetCreationTool() != Dc1NodePtr())
		result.Insert(GetCreationTool());
	if (GetCopyrightString() != Dc1NodePtr())
		result.Insert(GetCopyrightString());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreationType_ExtMethodImpl.h


