
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsRightsType.h"
#include "Mp7JrsFinancialType.h"
#include "Mp7JrsUsageInformationType_Availability_CollectionType.h"
#include "Mp7JrsUsageInformationType_UsageRecord_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAvailabilityType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Availability
#include "Mp7JrsUsageRecordType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UsageRecord

#include <assert.h>
IMp7JrsUsageInformationType::IMp7JrsUsageInformationType()
{

// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtPropInit.h

}

IMp7JrsUsageInformationType::~IMp7JrsUsageInformationType()
{
// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtPropCleanup.h

}

Mp7JrsUsageInformationType::Mp7JrsUsageInformationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUsageInformationType::~Mp7JrsUsageInformationType()
{
	Cleanup();
}

void Mp7JrsUsageInformationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Rights = Mp7JrsRightsPtr(); // Class
	m_Rights_Exist = false;
	m_FinancialResults = Mp7JrsFinancialPtr(); // Class
	m_FinancialResults_Exist = false;
	m_Availability = Mp7JrsUsageInformationType_Availability_CollectionPtr(); // Collection
	m_UsageRecord = Mp7JrsUsageInformationType_UsageRecord_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtMyPropInit.h

}

void Mp7JrsUsageInformationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Rights);
	// Dc1Factory::DeleteObject(m_FinancialResults);
	// Dc1Factory::DeleteObject(m_Availability);
	// Dc1Factory::DeleteObject(m_UsageRecord);
}

void Mp7JrsUsageInformationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UsageInformationTypePtr, since we
	// might need GetBase(), which isn't defined in IUsageInformationType
	const Dc1Ptr< Mp7JrsUsageInformationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUsageInformationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidRights())
	{
		// Dc1Factory::DeleteObject(m_Rights);
		this->SetRights(Dc1Factory::CloneObject(tmp->GetRights()));
	}
	else
	{
		InvalidateRights();
	}
	if (tmp->IsValidFinancialResults())
	{
		// Dc1Factory::DeleteObject(m_FinancialResults);
		this->SetFinancialResults(Dc1Factory::CloneObject(tmp->GetFinancialResults()));
	}
	else
	{
		InvalidateFinancialResults();
	}
		// Dc1Factory::DeleteObject(m_Availability);
		this->SetAvailability(Dc1Factory::CloneObject(tmp->GetAvailability()));
		// Dc1Factory::DeleteObject(m_UsageRecord);
		this->SetUsageRecord(Dc1Factory::CloneObject(tmp->GetUsageRecord()));
}

Dc1NodePtr Mp7JrsUsageInformationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUsageInformationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsUsageInformationType::GetBase() const
{
	return m_Base;
}

Mp7JrsRightsPtr Mp7JrsUsageInformationType::GetRights() const
{
		return m_Rights;
}

// Element is optional
bool Mp7JrsUsageInformationType::IsValidRights() const
{
	return m_Rights_Exist;
}

Mp7JrsFinancialPtr Mp7JrsUsageInformationType::GetFinancialResults() const
{
		return m_FinancialResults;
}

// Element is optional
bool Mp7JrsUsageInformationType::IsValidFinancialResults() const
{
	return m_FinancialResults_Exist;
}

Mp7JrsUsageInformationType_Availability_CollectionPtr Mp7JrsUsageInformationType::GetAvailability() const
{
		return m_Availability;
}

Mp7JrsUsageInformationType_UsageRecord_CollectionPtr Mp7JrsUsageInformationType::GetUsageRecord() const
{
		return m_UsageRecord;
}

void Mp7JrsUsageInformationType::SetRights(const Mp7JrsRightsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetRights().");
	}
	if (m_Rights != item || m_Rights_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Rights);
		m_Rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Rights) m_Rights->SetParent(m_myself.getPointer());
		if (m_Rights && m_Rights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Rights->UseTypeAttribute = true;
		}
		m_Rights_Exist = true;
		if(m_Rights != Mp7JrsRightsPtr())
		{
			m_Rights->SetContentName(XMLString::transcode("Rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidateRights()
{
	m_Rights_Exist = false;
}
void Mp7JrsUsageInformationType::SetFinancialResults(const Mp7JrsFinancialPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetFinancialResults().");
	}
	if (m_FinancialResults != item || m_FinancialResults_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FinancialResults);
		m_FinancialResults = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FinancialResults) m_FinancialResults->SetParent(m_myself.getPointer());
		if (m_FinancialResults && m_FinancialResults->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FinancialResults->UseTypeAttribute = true;
		}
		m_FinancialResults_Exist = true;
		if(m_FinancialResults != Mp7JrsFinancialPtr())
		{
			m_FinancialResults->SetContentName(XMLString::transcode("FinancialResults"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidateFinancialResults()
{
	m_FinancialResults_Exist = false;
}
void Mp7JrsUsageInformationType::SetAvailability(const Mp7JrsUsageInformationType_Availability_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetAvailability().");
	}
	if (m_Availability != item)
	{
		// Dc1Factory::DeleteObject(m_Availability);
		m_Availability = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Availability) m_Availability->SetParent(m_myself.getPointer());
		if(m_Availability != Mp7JrsUsageInformationType_Availability_CollectionPtr())
		{
			m_Availability->SetContentName(XMLString::transcode("Availability"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::SetUsageRecord(const Mp7JrsUsageInformationType_UsageRecord_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetUsageRecord().");
	}
	if (m_UsageRecord != item)
	{
		// Dc1Factory::DeleteObject(m_UsageRecord);
		m_UsageRecord = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsageRecord) m_UsageRecord->SetParent(m_myself.getPointer());
		if(m_UsageRecord != Mp7JrsUsageInformationType_UsageRecord_CollectionPtr())
		{
			m_UsageRecord->SetContentName(XMLString::transcode("UsageRecord"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUsageInformationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsUsageInformationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsUsageInformationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUsageInformationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsUsageInformationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsUsageInformationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUsageInformationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsUsageInformationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsUsageInformationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUsageInformationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUsageInformationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUsageInformationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUsageInformationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUsageInformationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUsageInformationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageInformationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUsageInformationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsUsageInformationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageInformationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUsageInformationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Rights")) == 0)
	{
		// Rights is simple element RightsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRights()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRightsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRights(p, client);
					if((p = GetRights()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FinancialResults")) == 0)
	{
		// FinancialResults is simple element FinancialType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFinancialResults()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFinancialPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFinancialResults(p, client);
					if((p = GetFinancialResults()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Availability")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Availability is item of type AvailabilityType
		// in element collection UsageInformationType_Availability_CollectionType
		
		context->Found = true;
		Mp7JrsUsageInformationType_Availability_CollectionPtr coll = GetAvailability();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUsageInformationType_Availability_CollectionType; // FTT, check this
				SetAvailability(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAvailabilityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsageRecord")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UsageRecord is item of type UsageRecordType
		// in element collection UsageInformationType_UsageRecord_CollectionType
		
		context->Found = true;
		Mp7JrsUsageInformationType_UsageRecord_CollectionPtr coll = GetUsageRecord();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUsageInformationType_UsageRecord_CollectionType; // FTT, check this
				SetUsageRecord(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageRecordType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUsageRecordPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UsageInformationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UsageInformationType");
	}
	return result;
}

XMLCh * Mp7JrsUsageInformationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsUsageInformationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsUsageInformationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UsageInformationType"));
	// Element serialization:
	// Class
	
	if (m_Rights != Mp7JrsRightsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Rights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Rights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Rights->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FinancialResults != Mp7JrsFinancialPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FinancialResults->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FinancialResults"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FinancialResults->Serialize(doc, element, &elem);
	}
	if (m_Availability != Mp7JrsUsageInformationType_Availability_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Availability->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_UsageRecord != Mp7JrsUsageInformationType_UsageRecord_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UsageRecord->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUsageInformationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Rights"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Rights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRightsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FinancialResults"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FinancialResults")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFinancialType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFinancialResults(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Availability"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Availability")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUsageInformationType_Availability_CollectionPtr tmp = CreateUsageInformationType_Availability_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAvailability(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UsageRecord"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UsageRecord")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUsageInformationType_UsageRecord_CollectionPtr tmp = CreateUsageInformationType_UsageRecord_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUsageRecord(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUsageInformationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Rights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRightsType; // FTT, check this
	}
	this->SetRights(child);
  }
  if (XMLString::compareString(elementname, X("FinancialResults")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFinancialType; // FTT, check this
	}
	this->SetFinancialResults(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Availability")) == 0))
  {
	Mp7JrsUsageInformationType_Availability_CollectionPtr tmp = CreateUsageInformationType_Availability_CollectionType; // FTT, check this
	this->SetAvailability(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UsageRecord")) == 0))
  {
	Mp7JrsUsageInformationType_UsageRecord_CollectionPtr tmp = CreateUsageInformationType_UsageRecord_CollectionType; // FTT, check this
	this->SetUsageRecord(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUsageInformationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRights() != Dc1NodePtr())
		result.Insert(GetRights());
	if (GetFinancialResults() != Dc1NodePtr())
		result.Insert(GetFinancialResults());
	if (GetAvailability() != Dc1NodePtr())
		result.Insert(GetAvailability());
	if (GetUsageRecord() != Dc1NodePtr())
		result.Insert(GetUsageRecord());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUsageInformationType_ExtMethodImpl.h


