
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtImplInclude.h


#include "Mp7JrsSegmentType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTemporalMaskType.h"
#include "Mp7JrsAudioSegmentType_CollectionType.h"
#include "Mp7JrsAudioSegmentType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAudioSegmentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsAudioSegmentType_LocalType.h" // Choice collection AudioDescriptor
#include "Mp7JrsAudioDType.h" // Choice collection element AudioDescriptor
#include "Mp7JrsAudioDSType.h" // Choice collection element AudioDescriptionScheme
#include "Mp7JrsAudioSegmentType_LocalType0.h" // Choice collection TemporalDecomposition
#include "Mp7JrsAudioSegmentTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsAudioSegmentMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition

#include <assert.h>
IMp7JrsAudioSegmentType::IMp7JrsAudioSegmentType()
{

// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtPropInit.h

}

IMp7JrsAudioSegmentType::~IMp7JrsAudioSegmentType()
{
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtPropCleanup.h

}

Mp7JrsAudioSegmentType::Mp7JrsAudioSegmentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioSegmentType::~Mp7JrsAudioSegmentType()
{
	Cleanup();
}

void Mp7JrsAudioSegmentType::Init()
{
	// Init base
	m_Base = CreateSegmentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class
	m_TemporalMask = Mp7JrsTemporalMaskPtr(); // Class
	m_AudioSegmentType_LocalType = Mp7JrsAudioSegmentType_CollectionPtr(); // Collection
	m_AudioSegmentType_LocalType0 = Mp7JrsAudioSegmentType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtMyPropInit.h

}

void Mp7JrsAudioSegmentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MediaTime);
	// Dc1Factory::DeleteObject(m_TemporalMask);
	// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType);
	// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType0);
}

void Mp7JrsAudioSegmentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioSegmentTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioSegmentType
	const Dc1Ptr< Mp7JrsAudioSegmentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSegmentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioSegmentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
		// Dc1Factory::DeleteObject(m_TemporalMask);
		this->SetTemporalMask(Dc1Factory::CloneObject(tmp->GetTemporalMask()));
		// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType);
		this->SetAudioSegmentType_LocalType(Dc1Factory::CloneObject(tmp->GetAudioSegmentType_LocalType()));
		// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType0);
		this->SetAudioSegmentType_LocalType0(Dc1Factory::CloneObject(tmp->GetAudioSegmentType_LocalType0()));
}

Dc1NodePtr Mp7JrsAudioSegmentType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioSegmentType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSegmentType > Mp7JrsAudioSegmentType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaTimePtr Mp7JrsAudioSegmentType::GetMediaTime() const
{
		return m_MediaTime;
}

Mp7JrsTemporalMaskPtr Mp7JrsAudioSegmentType::GetTemporalMask() const
{
		return m_TemporalMask;
}

Mp7JrsAudioSegmentType_CollectionPtr Mp7JrsAudioSegmentType::GetAudioSegmentType_LocalType() const
{
		return m_AudioSegmentType_LocalType;
}

Mp7JrsAudioSegmentType_Collection0Ptr Mp7JrsAudioSegmentType::GetAudioSegmentType_LocalType0() const
{
		return m_AudioSegmentType_LocalType0;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAudioSegmentType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetMediaTime().");
	}
	if (m_MediaTime != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	// Dc1Factory::DeleteObject(m_TemporalMask);
	m_TemporalMask = Mp7JrsTemporalMaskPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSegmentType::SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetTemporalMask().");
	}
	if (m_TemporalMask != item)
	{
		// Dc1Factory::DeleteObject(m_TemporalMask);
		m_TemporalMask = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalMask) m_TemporalMask->SetParent(m_myself.getPointer());
		if (m_TemporalMask && m_TemporalMask->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalMaskType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemporalMask->UseTypeAttribute = true;
		}
		if(m_TemporalMask != Mp7JrsTemporalMaskPtr())
		{
			m_TemporalMask->SetContentName(XMLString::transcode("TemporalMask"));
		}
	// Dc1Factory::DeleteObject(m_MediaTime);
	m_MediaTime = Mp7JrsMediaTimePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetAudioSegmentType_LocalType(const Mp7JrsAudioSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetAudioSegmentType_LocalType().");
	}
	if (m_AudioSegmentType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType);
		m_AudioSegmentType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegmentType_LocalType) m_AudioSegmentType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetAudioSegmentType_LocalType0(const Mp7JrsAudioSegmentType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetAudioSegmentType_LocalType0().");
	}
	if (m_AudioSegmentType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegmentType_LocalType0);
		m_AudioSegmentType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegmentType_LocalType0) m_AudioSegmentType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsAudioSegmentType::GetMediaInformation() const
{
	return GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsAudioSegmentType::GetMediaInformationRef() const
{
	return GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsAudioSegmentType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsAudioSegmentType::GetStructuralUnit() const
{
	return GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsAudioSegmentType::IsValidStructuralUnit() const
{
	return GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsAudioSegmentType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsAudioSegmentType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsAudioSegmentType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsAudioSegmentType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsAudioSegmentType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsAudioSegmentType::GetSegmentType_LocalType() const
{
	return GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsAudioSegmentType::GetMatchingHint() const
{
	return GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsAudioSegmentType::GetPointOfView() const
{
	return GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsAudioSegmentType::GetRelation() const
{
	return GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAudioSegmentType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetMediaInformation().");
	}
	GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSegmentType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetMediaInformationRef().");
	}
	GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSegmentType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetStructuralUnit().");
	}
	GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::InvalidateStructuralUnit()
{
	GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAudioSegmentType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSegmentType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAudioSegmentType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSegmentType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetSegmentType_LocalType().");
	}
	GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetMatchingHint().");
	}
	GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetPointOfView().");
	}
	GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsAudioSegmentType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsAudioSegmentType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsAudioSegmentType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAudioSegmentType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsAudioSegmentType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsAudioSegmentType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAudioSegmentType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsAudioSegmentType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsAudioSegmentType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAudioSegmentType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAudioSegmentType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAudioSegmentType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAudioSegmentType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAudioSegmentType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAudioSegmentType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSegmentType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAudioSegmentType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsAudioSegmentType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSegmentType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAudioSegmentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTime")) == 0)
	{
		// MediaTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTime(p, client);
					if((p = GetMediaTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TemporalMask")) == 0)
	{
		// TemporalMask is simple element TemporalMaskType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTemporalMask()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalMaskType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalMaskPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTemporalMask(p, client);
					if((p = GetTemporalMask()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioDescriptor")) == 0)
	{
		// AudioDescriptor is contained in itemtype AudioSegmentType_LocalType
		// in choice collection AudioSegmentType_CollectionType

		context->Found = true;
		Mp7JrsAudioSegmentType_CollectionPtr coll = GetAudioSegmentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioSegmentType_CollectionType; // FTT, check this
				SetAudioSegmentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->GetAudioDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAudioSegmentType_LocalPtr item = CreateAudioSegmentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAudioDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->SetAudioDescriptor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->GetAudioDescriptor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioDescriptionScheme")) == 0)
	{
		// AudioDescriptionScheme is contained in itemtype AudioSegmentType_LocalType
		// in choice collection AudioSegmentType_CollectionType

		context->Found = true;
		Mp7JrsAudioSegmentType_CollectionPtr coll = GetAudioSegmentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioSegmentType_CollectionType; // FTT, check this
				SetAudioSegmentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->GetAudioDescriptionScheme()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAudioSegmentType_LocalPtr item = CreateAudioSegmentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAudioDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->SetAudioDescriptionScheme(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAudioSegmentType_LocalPtr)coll->elementAt(i))->GetAudioDescriptionScheme()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TemporalDecomposition")) == 0)
	{
		// TemporalDecomposition is contained in itemtype AudioSegmentType_LocalType0
		// in choice collection AudioSegmentType_CollectionType0

		context->Found = true;
		Mp7JrsAudioSegmentType_Collection0Ptr coll = GetAudioSegmentType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioSegmentType_CollectionType0; // FTT, check this
				SetAudioSegmentType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->GetTemporalDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAudioSegmentType_Local0Ptr item = CreateAudioSegmentType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentTemporalDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSegmentTemporalDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->SetTemporalDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->GetTemporalDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaSourceDecomposition")) == 0)
	{
		// MediaSourceDecomposition is contained in itemtype AudioSegmentType_LocalType0
		// in choice collection AudioSegmentType_CollectionType0

		context->Found = true;
		Mp7JrsAudioSegmentType_Collection0Ptr coll = GetAudioSegmentType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioSegmentType_CollectionType0; // FTT, check this
				SetAudioSegmentType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->GetMediaSourceDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAudioSegmentType_Local0Ptr item = CreateAudioSegmentType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentMediaSourceDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSegmentMediaSourceDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->SetMediaSourceDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAudioSegmentType_Local0Ptr)coll->elementAt(i))->GetMediaSourceDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioSegmentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioSegmentType");
	}
	return result;
}

XMLCh * Mp7JrsAudioSegmentType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAudioSegmentType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAudioSegmentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioSegmentType"));
	// Element serialization:
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TemporalMask != Mp7JrsTemporalMaskPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemporalMask->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemporalMask"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemporalMask->Serialize(doc, element, &elem);
	}
	if (m_AudioSegmentType_LocalType != Mp7JrsAudioSegmentType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioSegmentType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_AudioSegmentType_LocalType0 != Mp7JrsAudioSegmentType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioSegmentType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAudioSegmentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSegmentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemporalMask"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemporalMask")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalMaskType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemporalMask(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:AudioDescriptor
			Dc1Util::HasNodeName(parent, X("AudioDescriptor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:AudioDescriptor")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme
			Dc1Util::HasNodeName(parent, X("AudioDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsAudioSegmentType_CollectionPtr tmp = CreateAudioSegmentType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioSegmentType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:TemporalDecomposition
			Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TemporalDecomposition")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:MediaSourceDecomposition
			Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MediaSourceDecomposition")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsAudioSegmentType_Collection0Ptr tmp = CreateAudioSegmentType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioSegmentType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioSegmentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  if (XMLString::compareString(elementname, X("TemporalMask")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalMaskType; // FTT, check this
	}
	this->SetTemporalMask(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("AudioDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("AudioDescriptor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("AudioDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("AudioDescriptionScheme")) == 0)
	)
  {
	Mp7JrsAudioSegmentType_CollectionPtr tmp = CreateAudioSegmentType_CollectionType; // FTT, check this
	this->SetAudioSegmentType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TemporalDecomposition")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("MediaSourceDecomposition")) == 0)
	)
  {
	Mp7JrsAudioSegmentType_Collection0Ptr tmp = CreateAudioSegmentType_CollectionType0; // FTT, check this
	this->SetAudioSegmentType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioSegmentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAudioSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetAudioSegmentType_LocalType());
	if (GetAudioSegmentType_LocalType0() != Dc1NodePtr())
		result.Insert(GetAudioSegmentType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetTemporalMask() != Dc1NodePtr())
		result.Insert(GetTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioSegmentType_ExtMethodImpl.h


