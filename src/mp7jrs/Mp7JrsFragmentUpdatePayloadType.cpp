
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtImplInclude.h


#include "Mp7JrsFragmentReferenceType.h"
#include "Mp7JrsFragmentUpdatePayloadType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFragmentUpdatePayloadType::IMp7JrsFragmentUpdatePayloadType()
{

// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtPropInit.h

}

IMp7JrsFragmentUpdatePayloadType::~IMp7JrsFragmentUpdatePayloadType()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtPropCleanup.h

}

Mp7JrsFragmentUpdatePayloadType::Mp7JrsFragmentUpdatePayloadType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFragmentUpdatePayloadType::~Mp7JrsFragmentUpdatePayloadType()
{
	Cleanup();
}

void Mp7JrsFragmentUpdatePayloadType::Init()
{

	// Init attributes
	m_hasDeferredNodes = false; // Value
	m_hasDeferredNodes_Default = false; // Default value
	m_hasDeferredNodes_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	// m_Any = ; // Optional Any (=Unknown), nothing to init
	m_Any_Exist = false;
	m_FragmentReference = Dc1Ptr< Dc1Node >(); // Class
	m_FragmentReference_Exist = false;


// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtMyPropInit.h

}

void Mp7JrsFragmentUpdatePayloadType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FragmentReference);
}

void Mp7JrsFragmentUpdatePayloadType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FragmentUpdatePayloadTypePtr, since we
	// might need GetBase(), which isn't defined in IFragmentUpdatePayloadType
	const Dc1Ptr< Mp7JrsFragmentUpdatePayloadType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExisthasDeferredNodes())
	{
		this->SethasDeferredNodes(tmp->GethasDeferredNodes());
	}
	else
	{
		InvalidatehasDeferredNodes();
	}
	}
	if (tmp->IsValidFragmentReference())
	{
		// Dc1Factory::DeleteObject(m_FragmentReference);
		this->SetFragmentReference(Dc1Factory::CloneObject(tmp->GetFragmentReference()));
	}
	else
	{
		InvalidateFragmentReference();
	}
}

Dc1NodePtr Mp7JrsFragmentUpdatePayloadType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFragmentUpdatePayloadType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsFragmentUpdatePayloadType::GethasDeferredNodes() const
{
	if (this->ExisthasDeferredNodes()) {
		return m_hasDeferredNodes;
	} else {
		return m_hasDeferredNodes_Default;
	}
}

bool Mp7JrsFragmentUpdatePayloadType::ExisthasDeferredNodes() const
{
	return m_hasDeferredNodes_Exist;
}
void Mp7JrsFragmentUpdatePayloadType::SethasDeferredNodes(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdatePayloadType::SethasDeferredNodes().");
	}
	m_hasDeferredNodes = item;
	m_hasDeferredNodes_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdatePayloadType::InvalidatehasDeferredNodes()
{
	m_hasDeferredNodes_Exist = false;
}
Dc1UnknownPtr Mp7JrsFragmentUpdatePayloadType::GetAny(Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */)
{
  return this->m_Any;
}
Dc1Ptr< Dc1Node > Mp7JrsFragmentUpdatePayloadType::GetFragmentReference() const
{
		return m_FragmentReference;
}

// Element is optional
bool Mp7JrsFragmentUpdatePayloadType::IsValidFragmentReference() const
{
	return m_FragmentReference_Exist;
}

void Mp7JrsFragmentUpdatePayloadType::SetAny(Dc1UnknownPtr item, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdatePayloadType::SetAny().");
	}
  this->m_Any = item;

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}
void Mp7JrsFragmentUpdatePayloadType::SetFragmentReference(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdatePayloadType::SetFragmentReference().");
	}
	if (m_FragmentReference != item || m_FragmentReference_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FragmentReference);
		m_FragmentReference = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FragmentReference) m_FragmentReference->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_FragmentReference) m_FragmentReference->UseTypeAttribute = true;
		m_FragmentReference_Exist = true;
		if(m_FragmentReference != Dc1Ptr< Dc1Node >())
		{
			m_FragmentReference->SetContentName(XMLString::transcode("FragmentReference"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdatePayloadType::InvalidateFragmentReference()
{
	m_FragmentReference_Exist = false;
}

Dc1NodeEnum Mp7JrsFragmentUpdatePayloadType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("hasDeferredNodes")) == 0)
	{
		// hasDeferredNodes is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("FragmentReference")) == 0)
	{
		// FragmentReference is simple abstract element FragmentReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFragmentReference()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsFragmentReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFragmentReference(p, client);
					if((p = GetFragmentReference()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FragmentUpdatePayloadType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FragmentUpdatePayloadType");
	}
	return result;
}

XMLCh * Mp7JrsFragmentUpdatePayloadType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFragmentUpdatePayloadType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFragmentUpdatePayloadType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdatePayloadType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_hasDeferredNodes_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_hasDeferredNodes);
		element->setAttributeNS(X(""), X("hasDeferredNodes"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Optional Any  
	if(m_Any != Dc1UnknownPtr()) { 
	  m_Any->Serialize(doc, parent, newElem);
	}
	// Class
	
	if (m_FragmentReference != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FragmentReference->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FragmentReference"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_FragmentReference->UseTypeAttribute = true;
		}
		m_FragmentReference->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFragmentUpdatePayloadType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("hasDeferredNodes")))
	{
		// deserialize value type
		this->SethasDeferredNodes(Dc1Convert::TextToBool(parent->getAttribute(X("hasDeferredNodes"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	// Optional Any
	if(true 
	  && !Dc1Util::HasNodeName(parent, X("FragmentReference"), X("urn:mpeg:mpeg7:schema:2004"))
		  )
	  {
		Dc1UnknownPtr any = Dc1Factory::CreateObject(1); // Unknown is always at position 1
		any->Deserialize(doc, parent, current);
		this->SetAny(any);
		found = true;
		parent = Dc1Util::GetNextAkin(parent);
	  }
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FragmentReference"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FragmentReference")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFragmentReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFragmentReference(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFragmentUpdatePayloadType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FragmentReference")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFragmentReferenceType; // FTT, check this
	}
	this->SetFragmentReference(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFragmentUpdatePayloadType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFragmentReference() != Dc1NodePtr())
		result.Insert(GetFragmentReference());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFragmentUpdatePayloadType_ExtMethodImpl.h


