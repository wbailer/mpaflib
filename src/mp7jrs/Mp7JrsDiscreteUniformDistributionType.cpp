
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtImplInclude.h


#include "Mp7JrsDiscreteDistributionType.h"
#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7JrsProbabilityDistributionType_Moment_CollectionType.h"
#include "Mp7JrsProbabilityDistributionType_Cumulant_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDiscreteUniformDistributionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsProbabilityDistributionType_Moment_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Moment
#include "Mp7JrsProbabilityDistributionType_Cumulant_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Cumulant

#include <assert.h>
IMp7JrsDiscreteUniformDistributionType::IMp7JrsDiscreteUniformDistributionType()
{

// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtPropInit.h

}

IMp7JrsDiscreteUniformDistributionType::~IMp7JrsDiscreteUniformDistributionType()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtPropCleanup.h

}

Mp7JrsDiscreteUniformDistributionType::Mp7JrsDiscreteUniformDistributionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDiscreteUniformDistributionType::~Mp7JrsDiscreteUniformDistributionType()
{
	Cleanup();
}

void Mp7JrsDiscreteUniformDistributionType::Init()
{
	// Init base
	m_Base = CreateDiscreteDistributionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Range = Mp7JrsDoubleMatrixPtr(); // Class
	m_Scale = Mp7JrsDoubleMatrixPtr(); // Class
	m_Scale_Exist = false;
	m_Shift = Mp7JrsDoubleMatrixPtr(); // Class
	m_Shift_Exist = false;


// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtMyPropInit.h

}

void Mp7JrsDiscreteUniformDistributionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Range);
	// Dc1Factory::DeleteObject(m_Scale);
	// Dc1Factory::DeleteObject(m_Shift);
}

void Mp7JrsDiscreteUniformDistributionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DiscreteUniformDistributionTypePtr, since we
	// might need GetBase(), which isn't defined in IDiscreteUniformDistributionType
	const Dc1Ptr< Mp7JrsDiscreteUniformDistributionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDiscreteDistributionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDiscreteUniformDistributionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Range);
		this->SetRange(Dc1Factory::CloneObject(tmp->GetRange()));
	if (tmp->IsValidScale())
	{
		// Dc1Factory::DeleteObject(m_Scale);
		this->SetScale(Dc1Factory::CloneObject(tmp->GetScale()));
	}
	else
	{
		InvalidateScale();
	}
	if (tmp->IsValidShift())
	{
		// Dc1Factory::DeleteObject(m_Shift);
		this->SetShift(Dc1Factory::CloneObject(tmp->GetShift()));
	}
	else
	{
		InvalidateShift();
	}
}

Dc1NodePtr Mp7JrsDiscreteUniformDistributionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDiscreteUniformDistributionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDiscreteDistributionType > Mp7JrsDiscreteUniformDistributionType::GetBase() const
{
	return m_Base;
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetRange() const
{
		return m_Range;
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetScale() const
{
		return m_Scale;
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidScale() const
{
	return m_Scale_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetShift() const
{
		return m_Shift;
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidShift() const
{
	return m_Shift_Exist;
}

void Mp7JrsDiscreteUniformDistributionType::SetRange(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetRange().");
	}
	if (m_Range != item)
	{
		// Dc1Factory::DeleteObject(m_Range);
		m_Range = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Range) m_Range->SetParent(m_myself.getPointer());
		if (m_Range && m_Range->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Range->UseTypeAttribute = true;
		}
		if(m_Range != Mp7JrsDoubleMatrixPtr())
		{
			m_Range->SetContentName(XMLString::transcode("Range"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::SetScale(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetScale().");
	}
	if (m_Scale != item || m_Scale_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Scale);
		m_Scale = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Scale) m_Scale->SetParent(m_myself.getPointer());
		if (m_Scale && m_Scale->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Scale->UseTypeAttribute = true;
		}
		m_Scale_Exist = true;
		if(m_Scale != Mp7JrsDoubleMatrixPtr())
		{
			m_Scale->SetContentName(XMLString::transcode("Scale"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateScale()
{
	m_Scale_Exist = false;
}
void Mp7JrsDiscreteUniformDistributionType::SetShift(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetShift().");
	}
	if (m_Shift != item || m_Shift_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Shift);
		m_Shift = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Shift) m_Shift->SetParent(m_myself.getPointer());
		if (m_Shift && m_Shift->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Shift->UseTypeAttribute = true;
		}
		m_Shift_Exist = true;
		if(m_Shift != Mp7JrsDoubleMatrixPtr())
		{
			m_Shift->SetContentName(XMLString::transcode("Shift"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateShift()
{
	m_Shift_Exist = false;
}
unsigned Mp7JrsDiscreteUniformDistributionType::Getdim() const
{
	return GetBase()->GetBase()->Getdim();
}

bool Mp7JrsDiscreteUniformDistributionType::Existdim() const
{
	return GetBase()->GetBase()->Existdim();
}
void Mp7JrsDiscreteUniformDistributionType::Setdim(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::Setdim().");
	}
	GetBase()->GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::Invalidatedim()
{
	GetBase()->GetBase()->Invalidatedim();
}
Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetMean() const
{
	return GetBase()->GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidMean() const
{
	return GetBase()->GetBase()->IsValidMean();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetVariance() const
{
	return GetBase()->GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidVariance() const
{
	return GetBase()->GetBase()->IsValidVariance();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetMin() const
{
	return GetBase()->GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidMin() const
{
	return GetBase()->GetBase()->IsValidMin();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetMax() const
{
	return GetBase()->GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidMax() const
{
	return GetBase()->GetBase()->IsValidMax();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetMode() const
{
	return GetBase()->GetBase()->GetMode();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidMode() const
{
	return GetBase()->GetBase()->IsValidMode();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteUniformDistributionType::GetMedian() const
{
	return GetBase()->GetBase()->GetMedian();
}

// Element is optional
bool Mp7JrsDiscreteUniformDistributionType::IsValidMedian() const
{
	return GetBase()->GetBase()->IsValidMedian();
}

Mp7JrsProbabilityDistributionType_Moment_CollectionPtr Mp7JrsDiscreteUniformDistributionType::GetMoment() const
{
	return GetBase()->GetBase()->GetMoment();
}

Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr Mp7JrsDiscreteUniformDistributionType::GetCumulant() const
{
	return GetBase()->GetBase()->GetCumulant();
}

void Mp7JrsDiscreteUniformDistributionType::SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMean().");
	}
	GetBase()->GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateMean()
{
	GetBase()->GetBase()->InvalidateMean();
}
void Mp7JrsDiscreteUniformDistributionType::SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetVariance().");
	}
	GetBase()->GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateVariance()
{
	GetBase()->GetBase()->InvalidateVariance();
}
void Mp7JrsDiscreteUniformDistributionType::SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMin().");
	}
	GetBase()->GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateMin()
{
	GetBase()->GetBase()->InvalidateMin();
}
void Mp7JrsDiscreteUniformDistributionType::SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMax().");
	}
	GetBase()->GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateMax()
{
	GetBase()->GetBase()->InvalidateMax();
}
void Mp7JrsDiscreteUniformDistributionType::SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMode().");
	}
	GetBase()->GetBase()->SetMode(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateMode()
{
	GetBase()->GetBase()->InvalidateMode();
}
void Mp7JrsDiscreteUniformDistributionType::SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMedian().");
	}
	GetBase()->GetBase()->SetMedian(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidateMedian()
{
	GetBase()->GetBase()->InvalidateMedian();
}
void Mp7JrsDiscreteUniformDistributionType::SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetMoment().");
	}
	GetBase()->GetBase()->SetMoment(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetCumulant().");
	}
	GetBase()->GetBase()->SetCumulant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsDiscreteUniformDistributionType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsDiscreteUniformDistributionType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsDiscreteUniformDistributionType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsDiscreteUniformDistributionType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsDiscreteUniformDistributionType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsDiscreteUniformDistributionType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsDiscreteUniformDistributionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsDiscreteUniformDistributionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsDiscreteUniformDistributionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteUniformDistributionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsDiscreteUniformDistributionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsDiscreteUniformDistributionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsDiscreteUniformDistributionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsDiscreteUniformDistributionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsDiscreteUniformDistributionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteUniformDistributionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsDiscreteUniformDistributionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsDiscreteUniformDistributionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsDiscreteUniformDistributionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsDiscreteUniformDistributionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsDiscreteUniformDistributionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteUniformDistributionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDiscreteUniformDistributionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsDiscreteUniformDistributionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteUniformDistributionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDiscreteUniformDistributionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Range")) == 0)
	{
		// Range is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRange()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRange(p, client);
					if((p = GetRange()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Scale")) == 0)
	{
		// Scale is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetScale()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetScale(p, client);
					if((p = GetScale()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Shift")) == 0)
	{
		// Shift is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShift()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShift(p, client);
					if((p = GetShift()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DiscreteUniformDistributionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DiscreteUniformDistributionType");
	}
	return result;
}

XMLCh * Mp7JrsDiscreteUniformDistributionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDiscreteUniformDistributionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDiscreteUniformDistributionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DiscreteUniformDistributionType"));
	// Element serialization:
	// Class
	
	if (m_Range != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Range->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Range"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Range->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Scale != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Scale->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Scale"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Scale->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Shift != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Shift->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Shift"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Shift->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsDiscreteUniformDistributionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDiscreteDistributionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Range"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Range")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRange(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Scale"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Scale")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetScale(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Shift"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Shift")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShift(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDiscreteUniformDistributionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Range")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetRange(child);
  }
  if (XMLString::compareString(elementname, X("Scale")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetScale(child);
  }
  if (XMLString::compareString(elementname, X("Shift")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetShift(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDiscreteUniformDistributionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRange() != Dc1NodePtr())
		result.Insert(GetRange());
	if (GetScale() != Dc1NodePtr())
		result.Insert(GetScale());
	if (GetShift() != Dc1NodePtr())
		result.Insert(GetShift());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMode() != Dc1NodePtr())
		result.Insert(GetMode());
	if (GetMedian() != Dc1NodePtr())
		result.Insert(GetMedian());
	if (GetMoment() != Dc1NodePtr())
		result.Insert(GetMoment());
	if (GetCumulant() != Dc1NodePtr())
		result.Insert(GetCumulant());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDiscreteUniformDistributionType_ExtMethodImpl.h


