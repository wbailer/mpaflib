
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtImplInclude.h


#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsRelTimePointType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsRelTimePointType::IMp7JrsRelTimePointType()
{

// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtPropInit.h

}

IMp7JrsRelTimePointType::~IMp7JrsRelTimePointType()
{
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtPropCleanup.h

}

Mp7JrsRelTimePointType::Mp7JrsRelTimePointType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRelTimePointType::~Mp7JrsRelTimePointType()
{
	Cleanup();
}

void Mp7JrsRelTimePointType::Init()
{
	// Init base
	m_Base = CreatetimeOffsetType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_timeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_timeBase_Exist = false;



// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMyPropInit.h

}

void Mp7JrsRelTimePointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
}

void Mp7JrsRelTimePointType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RelTimePointTypePtr, since we
	// might need GetBase(), which isn't defined in IRelTimePointType
	const Dc1Ptr< Mp7JrsRelTimePointType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrstimeOffsetPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsRelTimePointType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
	if (tmp->ExisttimeBase())
	{
		this->SettimeBase(Dc1Factory::CloneObject(tmp->GettimeBase()));
	}
	else
	{
		InvalidatetimeBase();
	}
	}
}

Dc1NodePtr Mp7JrsRelTimePointType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsRelTimePointType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrstimeOffsetType > Mp7JrsRelTimePointType::GetBase() const
{
	return m_Base;
}

Mp7JrsxPathRefPtr Mp7JrsRelTimePointType::GettimeBase() const
{
	return m_timeBase;
}

bool Mp7JrsRelTimePointType::ExisttimeBase() const
{
	return m_timeBase_Exist;
}
void Mp7JrsRelTimePointType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SettimeBase().");
	}
	m_timeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeBase) m_timeBase->SetParent(m_myself.getPointer());
	m_timeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelTimePointType::InvalidatetimeBase()
{
	m_timeBase_Exist = false;
}
int Mp7JrsRelTimePointType::GetOffsetSign() const
{
	return GetBase()->GetOffsetSign();
}

void Mp7JrsRelTimePointType::SetOffsetSign(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetOffsetSign().");
	}
	GetBase()->SetOffsetSign(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetDays() const
{
	return GetBase()->GetDays();
}

void Mp7JrsRelTimePointType::SetDays(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetDays().");
	}
	GetBase()->SetDays(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetHours() const
{
	return GetBase()->GetHours();
}

void Mp7JrsRelTimePointType::SetHours(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetHours().");
	}
	GetBase()->SetHours(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetMinutes() const
{
	return GetBase()->GetMinutes();
}

void Mp7JrsRelTimePointType::SetMinutes(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetMinutes().");
	}
	GetBase()->SetMinutes(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetSeconds() const
{
	return GetBase()->GetSeconds();
}

void Mp7JrsRelTimePointType::SetSeconds(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetSeconds().");
	}
	GetBase()->SetSeconds(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetNumberOfFractions() const
{
	return GetBase()->GetNumberOfFractions();
}

void Mp7JrsRelTimePointType::SetNumberOfFractions(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetNumberOfFractions().");
	}
	GetBase()->SetNumberOfFractions(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsRelTimePointType::GetTime_Exist() const
{
	return GetBase()->GetTime_Exist();
}

void Mp7JrsRelTimePointType::SetTime_Exist(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetTime_Exist().");
	}
	GetBase()->SetTime_Exist(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetFractionsPerSecond() const
{
	return GetBase()->GetFractionsPerSecond();
}

void Mp7JrsRelTimePointType::SetFractionsPerSecond(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetFractionsPerSecond().");
	}
	GetBase()->SetFractionsPerSecond(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetTimezoneSign() const
{
	return GetBase()->GetTimezoneSign();
}

void Mp7JrsRelTimePointType::SetTimezoneSign(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetTimezoneSign().");
	}
	GetBase()->SetTimezoneSign(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetTimezoneHours() const
{
	return GetBase()->GetTimezoneHours();
}

void Mp7JrsRelTimePointType::SetTimezoneHours(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetTimezoneHours().");
	}
	GetBase()->SetTimezoneHours(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsRelTimePointType::GetTimezoneMinutes() const
{
	return GetBase()->GetTimezoneMinutes();
}

void Mp7JrsRelTimePointType::SetTimezoneMinutes(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetTimezoneMinutes().");
	}
	GetBase()->SetTimezoneMinutes(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsRelTimePointType::GetTimezone_Exist() const
{
	return GetBase()->GetTimezone_Exist();
}

void Mp7JrsRelTimePointType::SetTimezone_Exist(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelTimePointType::SetTimezone_Exist().");
	}
	GetBase()->SetTimezone_Exist(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRelTimePointType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeBase")) == 0)
	{
		// timeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RelTimePointType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RelTimePointType");
	}
	return result;
}

XMLCh * Mp7JrsRelTimePointType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRelTimePointType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsRelTimePointType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsRelTimePointType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsRelTimePointType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	m_Base->Serialize(doc, element, newElem);

	assert(element);
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RelTimePointType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeBase_Exist)
	{
	// Pattern
	if (m_timeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_timeBase->ToText();
		element->setAttributeNS(X(""), X("timeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsRelTimePointType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeBase")));
		this->SettimeBase(tmp);
		* current = parent;
	}

  // Extensionbase is Mp7JrstimeOffsetType
  // Deserialize cce factory type
  if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
  }
// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRelTimePointType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRelTimePointType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRelTimePointType_ExtMethodImpl.h


