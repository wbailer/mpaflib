
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsAudioLLDScalarType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfScalar

#include <assert.h>
IMp7JrsAudioLLDScalarType::IMp7JrsAudioLLDScalarType()
{

// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtPropInit.h

}

IMp7JrsAudioLLDScalarType::~IMp7JrsAudioLLDScalarType()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtPropCleanup.h

}

Mp7JrsAudioLLDScalarType::Mp7JrsAudioLLDScalarType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioLLDScalarType::~Mp7JrsAudioLLDScalarType()
{
	Cleanup();
}

void Mp7JrsAudioLLDScalarType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_confidence = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_confidence->SetContent(0.0); // Use Value initialiser (xxx2)
	m_confidence_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Scalar = (0.0f); // Value

	m_SeriesOfScalar = Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtMyPropInit.h

}

void Mp7JrsAudioLLDScalarType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_confidence);
	// Dc1Factory::DeleteObject(m_SeriesOfScalar);
}

void Mp7JrsAudioLLDScalarType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioLLDScalarTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioLLDScalarType
	const Dc1Ptr< Mp7JrsAudioLLDScalarType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioLLDScalarType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_confidence);
	if (tmp->Existconfidence())
	{
		this->Setconfidence(Dc1Factory::CloneObject(tmp->Getconfidence()));
	}
	else
	{
		Invalidateconfidence();
	}
	}
		this->SetScalar(tmp->GetScalar());
		// Dc1Factory::DeleteObject(m_SeriesOfScalar);
		this->SetSeriesOfScalar(Dc1Factory::CloneObject(tmp->GetSeriesOfScalar()));
}

Dc1NodePtr Mp7JrsAudioLLDScalarType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioLLDScalarType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsAudioLLDScalarType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsAudioLLDScalarType::Getconfidence() const
{
	return m_confidence;
}

bool Mp7JrsAudioLLDScalarType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsAudioLLDScalarType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType::Setconfidence().");
	}
	m_confidence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_confidence) m_confidence->SetParent(m_myself.getPointer());
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
float Mp7JrsAudioLLDScalarType::GetScalar() const
{
		return m_Scalar;
}

Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr Mp7JrsAudioLLDScalarType::GetSeriesOfScalar() const
{
		return m_SeriesOfScalar;
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioLLDScalarType::SetScalar(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType::SetScalar().");
	}
	if (m_Scalar != item)
	{
		m_Scalar = item;
		m_Scalar_Valid = true;
	// Dc1Factory::DeleteObject(m_SeriesOfScalar);
	m_SeriesOfScalar = Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioLLDScalarType::SetSeriesOfScalar(const Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType::SetSeriesOfScalar().");
	}
	if (m_SeriesOfScalar != item)
	{
		// Dc1Factory::DeleteObject(m_SeriesOfScalar);
		m_SeriesOfScalar = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SeriesOfScalar) m_SeriesOfScalar->SetParent(m_myself.getPointer());
		if(m_SeriesOfScalar != Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr())
		{
			m_SeriesOfScalar->SetContentName(XMLString::transcode("SeriesOfScalar"));
		}
	
	m_Scalar_Valid = false;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsAudioLLDScalarType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsAudioLLDScalarType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsAudioLLDScalarType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsAudioLLDScalarType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SeriesOfScalar")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SeriesOfScalar is item of type AudioLLDScalarType_SeriesOfScalar_LocalType
		// in element collection AudioLLDScalarType_SeriesOfScalar_CollectionType
		
		context->Found = true;
		Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr coll = GetSeriesOfScalar();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioLLDScalarType_SeriesOfScalar_CollectionType; // FTT, check this
				SetSeriesOfScalar(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDScalarType_SeriesOfScalar_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioLLDScalarType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioLLDScalarType");
	}
	return result;
}

XMLCh * Mp7JrsAudioLLDScalarType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioLLDScalarType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioLLDScalarType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioLLDScalarType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Class
	if (m_confidence != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_confidence->ToText();
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_Scalar_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Scalar);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Scalar"), false);
		XMLString::release(&tmp);
	}
	if (m_SeriesOfScalar != Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SeriesOfScalar->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAudioLLDScalarType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("confidence")));
		this->Setconfidence(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("Scalar"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetScalar(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SeriesOfScalar"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SeriesOfScalar")) == 0))
		{
			// Deserialize factory type
			Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr tmp = CreateAudioLLDScalarType_SeriesOfScalar_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSeriesOfScalar(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioLLDScalarType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SeriesOfScalar")) == 0))
  {
	Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr tmp = CreateAudioLLDScalarType_SeriesOfScalar_CollectionType; // FTT, check this
	this->SetSeriesOfScalar(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioLLDScalarType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSeriesOfScalar() != Dc1NodePtr())
		result.Insert(GetSeriesOfScalar());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_ExtMethodImpl.h


