
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtImplInclude.h


#include "Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalType.h"
#include "Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionType.h"
#include "Mp7JrsImageSignatureType_LocalSignature_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FeaturePoint

#include <assert.h>
IMp7JrsImageSignatureType_LocalSignature_LocalType::IMp7JrsImageSignatureType_LocalSignature_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtPropInit.h

}

IMp7JrsImageSignatureType_LocalSignature_LocalType::~IMp7JrsImageSignatureType_LocalSignature_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtPropCleanup.h

}

Mp7JrsImageSignatureType_LocalSignature_LocalType::Mp7JrsImageSignatureType_LocalSignature_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsImageSignatureType_LocalSignature_LocalType::~Mp7JrsImageSignatureType_LocalSignature_LocalType()
{
	Cleanup();
}

void Mp7JrsImageSignatureType_LocalSignature_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FeaturePointCount = Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr(); // Class with content 
	m_FeaturePoint = Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtMyPropInit.h

}

void Mp7JrsImageSignatureType_LocalSignature_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FeaturePointCount);
	// Dc1Factory::DeleteObject(m_FeaturePoint);
}

void Mp7JrsImageSignatureType_LocalSignature_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ImageSignatureType_LocalSignature_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IImageSignatureType_LocalSignature_LocalType
	const Dc1Ptr< Mp7JrsImageSignatureType_LocalSignature_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_FeaturePointCount);
		this->SetFeaturePointCount(Dc1Factory::CloneObject(tmp->GetFeaturePointCount()));
		// Dc1Factory::DeleteObject(m_FeaturePoint);
		this->SetFeaturePoint(Dc1Factory::CloneObject(tmp->GetFeaturePoint()));
}

Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr Mp7JrsImageSignatureType_LocalSignature_LocalType::GetFeaturePointCount() const
{
		return m_FeaturePointCount;
}

Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr Mp7JrsImageSignatureType_LocalSignature_LocalType::GetFeaturePoint() const
{
		return m_FeaturePoint;
}

void Mp7JrsImageSignatureType_LocalSignature_LocalType::SetFeaturePointCount(const Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_LocalType::SetFeaturePointCount().");
	}
	if (m_FeaturePointCount != item)
	{
		// Dc1Factory::DeleteObject(m_FeaturePointCount);
		m_FeaturePointCount = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FeaturePointCount) m_FeaturePointCount->SetParent(m_myself.getPointer());
		if (m_FeaturePointCount && m_FeaturePointCount->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePointCount_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FeaturePointCount->UseTypeAttribute = true;
		}
		if(m_FeaturePointCount != Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr())
		{
			m_FeaturePointCount->SetContentName(XMLString::transcode("FeaturePointCount"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType_LocalSignature_LocalType::SetFeaturePoint(const Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_LocalType::SetFeaturePoint().");
	}
	if (m_FeaturePoint != item)
	{
		// Dc1Factory::DeleteObject(m_FeaturePoint);
		m_FeaturePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FeaturePoint) m_FeaturePoint->SetParent(m_myself.getPointer());
		if(m_FeaturePoint != Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr())
		{
			m_FeaturePoint->SetContentName(XMLString::transcode("FeaturePoint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsImageSignatureType_LocalSignature_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("FeaturePointCount")) == 0)
	{
		// FeaturePointCount is simple element ImageSignatureType_LocalSignature_FeaturePointCount_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFeaturePointCount()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePointCount_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFeaturePointCount(p, client);
					if((p = GetFeaturePointCount()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FeaturePoint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FeaturePoint is item of type ImageSignatureType_LocalSignature_FeaturePoint_LocalType
		// in element collection ImageSignatureType_LocalSignature_FeaturePoint_CollectionType
		
		context->Found = true;
		Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr coll = GetFeaturePoint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateImageSignatureType_LocalSignature_FeaturePoint_CollectionType; // FTT, check this
				SetFeaturePoint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 80))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ImageSignatureType_LocalSignature_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ImageSignatureType_LocalSignature_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsImageSignatureType_LocalSignature_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsImageSignatureType_LocalSignature_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsImageSignatureType_LocalSignature_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ImageSignatureType_LocalSignature_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_FeaturePointCount != Mp7JrsImageSignatureType_LocalSignature_FeaturePointCount_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FeaturePointCount->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FeaturePointCount"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FeaturePointCount->Serialize(doc, element, &elem);
	}
	if (m_FeaturePoint != Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FeaturePoint->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsImageSignatureType_LocalSignature_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FeaturePointCount"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FeaturePointCount")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageSignatureType_LocalSignature_FeaturePointCount_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFeaturePointCount(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FeaturePoint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FeaturePoint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr tmp = CreateImageSignatureType_LocalSignature_FeaturePoint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFeaturePoint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FeaturePointCount")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageSignatureType_LocalSignature_FeaturePointCount_LocalType; // FTT, check this
	}
	this->SetFeaturePointCount(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FeaturePoint")) == 0))
  {
	Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_CollectionPtr tmp = CreateImageSignatureType_LocalSignature_FeaturePoint_CollectionType; // FTT, check this
	this->SetFeaturePoint(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsImageSignatureType_LocalSignature_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFeaturePointCount() != Dc1NodePtr())
		result.Insert(GetFeaturePointCount());
	if (GetFeaturePoint() != Dc1NodePtr())
		result.Insert(GetFeaturePoint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_LocalType_ExtMethodImpl.h


