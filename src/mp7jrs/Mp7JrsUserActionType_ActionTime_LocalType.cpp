
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTimeType.h"
#include "Mp7JrsUserActionType_ActionTime_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsUserActionType_ActionTime_LocalType::IMp7JrsUserActionType_ActionTime_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtPropInit.h

}

IMp7JrsUserActionType_ActionTime_LocalType::~IMp7JrsUserActionType_ActionTime_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtPropCleanup.h

}

Mp7JrsUserActionType_ActionTime_LocalType::Mp7JrsUserActionType_ActionTime_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserActionType_ActionTime_LocalType::~Mp7JrsUserActionType_ActionTime_LocalType()
{
	Cleanup();
}

void Mp7JrsUserActionType_ActionTime_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class
	m_MediaTime_Exist = false;
	m_GeneralTime = Mp7JrsTimePtr(); // Class
	m_GeneralTime_Exist = false;


// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtMyPropInit.h

}

void Mp7JrsUserActionType_ActionTime_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MediaTime);
	// Dc1Factory::DeleteObject(m_GeneralTime);
}

void Mp7JrsUserActionType_ActionTime_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserActionType_ActionTime_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IUserActionType_ActionTime_LocalType
	const Dc1Ptr< Mp7JrsUserActionType_ActionTime_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidMediaTime())
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
	}
	else
	{
		InvalidateMediaTime();
	}
	if (tmp->IsValidGeneralTime())
	{
		// Dc1Factory::DeleteObject(m_GeneralTime);
		this->SetGeneralTime(Dc1Factory::CloneObject(tmp->GetGeneralTime()));
	}
	else
	{
		InvalidateGeneralTime();
	}
}

Dc1NodePtr Mp7JrsUserActionType_ActionTime_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsUserActionType_ActionTime_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsMediaTimePtr Mp7JrsUserActionType_ActionTime_LocalType::GetMediaTime() const
{
		return m_MediaTime;
}

// Element is optional
bool Mp7JrsUserActionType_ActionTime_LocalType::IsValidMediaTime() const
{
	return m_MediaTime_Exist;
}

Mp7JrsTimePtr Mp7JrsUserActionType_ActionTime_LocalType::GetGeneralTime() const
{
		return m_GeneralTime;
}

// Element is optional
bool Mp7JrsUserActionType_ActionTime_LocalType::IsValidGeneralTime() const
{
	return m_GeneralTime_Exist;
}

void Mp7JrsUserActionType_ActionTime_LocalType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionType_ActionTime_LocalType::SetMediaTime().");
	}
	if (m_MediaTime != item || m_MediaTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		m_MediaTime_Exist = true;
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionType_ActionTime_LocalType::InvalidateMediaTime()
{
	m_MediaTime_Exist = false;
}
void Mp7JrsUserActionType_ActionTime_LocalType::SetGeneralTime(const Mp7JrsTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionType_ActionTime_LocalType::SetGeneralTime().");
	}
	if (m_GeneralTime != item || m_GeneralTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_GeneralTime);
		m_GeneralTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GeneralTime) m_GeneralTime->SetParent(m_myself.getPointer());
		if (m_GeneralTime && m_GeneralTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GeneralTime->UseTypeAttribute = true;
		}
		m_GeneralTime_Exist = true;
		if(m_GeneralTime != Mp7JrsTimePtr())
		{
			m_GeneralTime->SetContentName(XMLString::transcode("GeneralTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionType_ActionTime_LocalType::InvalidateGeneralTime()
{
	m_GeneralTime_Exist = false;
}

Dc1NodeEnum Mp7JrsUserActionType_ActionTime_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTime")) == 0)
	{
		// MediaTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTime(p, client);
					if((p = GetMediaTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GeneralTime")) == 0)
	{
		// GeneralTime is simple element TimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGeneralTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGeneralTime(p, client);
					if((p = GetGeneralTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserActionType_ActionTime_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserActionType_ActionTime_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsUserActionType_ActionTime_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUserActionType_ActionTime_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUserActionType_ActionTime_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserActionType_ActionTime_LocalType"));
	// Element serialization:
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GeneralTime != Mp7JrsTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GeneralTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GeneralTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GeneralTime->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsUserActionType_ActionTime_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GeneralTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GeneralTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGeneralTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserActionType_ActionTime_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  if (XMLString::compareString(elementname, X("GeneralTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTimeType; // FTT, check this
	}
	this->SetGeneralTime(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserActionType_ActionTime_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetGeneralTime() != Dc1NodePtr())
		result.Insert(GetGeneralTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserActionType_ActionTime_LocalType_ExtMethodImpl.h


