
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsSilenceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSilenceType::IMp7JrsSilenceType()
{

// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtPropInit.h

}

IMp7JrsSilenceType::~IMp7JrsSilenceType()
{
// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtPropCleanup.h

}

Mp7JrsSilenceType::Mp7JrsSilenceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSilenceType::~Mp7JrsSilenceType()
{
	Cleanup();
}

void Mp7JrsSilenceType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_confidence = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_confidence->SetContent(0.0); // Use Value initialiser (xxx2)
	m_confidence_Exist = false;
	m_minDurationRef = NULL; // String
	m_minDurationRef_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtMyPropInit.h

}

void Mp7JrsSilenceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_confidence);
	XMLString::release(&m_minDurationRef); // String
}

void Mp7JrsSilenceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SilenceTypePtr, since we
	// might need GetBase(), which isn't defined in ISilenceType
	const Dc1Ptr< Mp7JrsSilenceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSilenceType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_confidence);
	if (tmp->Existconfidence())
	{
		this->Setconfidence(Dc1Factory::CloneObject(tmp->Getconfidence()));
	}
	else
	{
		Invalidateconfidence();
	}
	}
	{
	XMLString::release(&m_minDurationRef); // String
	if (tmp->ExistminDurationRef())
	{
		this->SetminDurationRef(XMLString::replicate(tmp->GetminDurationRef()));
	}
	else
	{
		InvalidateminDurationRef();
	}
	}
}

Dc1NodePtr Mp7JrsSilenceType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSilenceType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsSilenceType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsSilenceType::Getconfidence() const
{
	if (this->Existconfidence()) {
		return m_confidence;
	} else {
		return m_confidence_Default;
	}
}

bool Mp7JrsSilenceType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsSilenceType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSilenceType::Setconfidence().");
	}
	m_confidence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_confidence) m_confidence->SetParent(m_myself.getPointer());
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSilenceType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
XMLCh * Mp7JrsSilenceType::GetminDurationRef() const
{
	return m_minDurationRef;
}

bool Mp7JrsSilenceType::ExistminDurationRef() const
{
	return m_minDurationRef_Exist;
}
void Mp7JrsSilenceType::SetminDurationRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSilenceType::SetminDurationRef().");
	}
	m_minDurationRef = item;
	m_minDurationRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSilenceType::InvalidateminDurationRef()
{
	m_minDurationRef_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsSilenceType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsSilenceType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsSilenceType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSilenceType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSilenceType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsSilenceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("minDurationRef")) == 0)
	{
		// minDurationRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SilenceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SilenceType");
	}
	return result;
}

XMLCh * Mp7JrsSilenceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSilenceType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSilenceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SilenceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Class
	if (m_confidence != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_confidence->ToText();
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_minDurationRef_Exist)
	{
	// String
	if(m_minDurationRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("minDurationRef"), m_minDurationRef);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSilenceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("confidence")));
		this->Setconfidence(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("minDurationRef")))
	{
		// Deserialize string type
		this->SetminDurationRef(Dc1Convert::TextToString(parent->getAttribute(X("minDurationRef"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSilenceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSilenceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSilenceType_ExtMethodImpl.h


