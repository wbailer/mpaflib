
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtImplInclude.h


#include "Mp7JrsTextualType.h"
#include "Mp7JrsStructuredAnnotationType.h"
#include "Mp7JrsDependencyStructureType.h"
#include "Mp7JrsKeywordAnnotationType.h"
#include "Mp7JrsTextAnnotationType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsTextAnnotationType_LocalType::IMp7JrsTextAnnotationType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtPropInit.h

}

IMp7JrsTextAnnotationType_LocalType::~IMp7JrsTextAnnotationType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtPropCleanup.h

}

Mp7JrsTextAnnotationType_LocalType::Mp7JrsTextAnnotationType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextAnnotationType_LocalType::~Mp7JrsTextAnnotationType_LocalType()
{
	Cleanup();
}

void Mp7JrsTextAnnotationType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FreeTextAnnotation = Mp7JrsTextualPtr(); // Class
	m_StructuredAnnotation = Mp7JrsStructuredAnnotationPtr(); // Class
	m_DependencyStructure = Mp7JrsDependencyStructurePtr(); // Class
	m_KeywordAnnotation = Mp7JrsKeywordAnnotationPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtMyPropInit.h

}

void Mp7JrsTextAnnotationType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
	// Dc1Factory::DeleteObject(m_StructuredAnnotation);
	// Dc1Factory::DeleteObject(m_DependencyStructure);
	// Dc1Factory::DeleteObject(m_KeywordAnnotation);
}

void Mp7JrsTextAnnotationType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextAnnotationType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ITextAnnotationType_LocalType
	const Dc1Ptr< Mp7JrsTextAnnotationType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
		this->SetFreeTextAnnotation(Dc1Factory::CloneObject(tmp->GetFreeTextAnnotation()));
		// Dc1Factory::DeleteObject(m_StructuredAnnotation);
		this->SetStructuredAnnotation(Dc1Factory::CloneObject(tmp->GetStructuredAnnotation()));
		// Dc1Factory::DeleteObject(m_DependencyStructure);
		this->SetDependencyStructure(Dc1Factory::CloneObject(tmp->GetDependencyStructure()));
		// Dc1Factory::DeleteObject(m_KeywordAnnotation);
		this->SetKeywordAnnotation(Dc1Factory::CloneObject(tmp->GetKeywordAnnotation()));
}

Dc1NodePtr Mp7JrsTextAnnotationType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTextAnnotationType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTextualPtr Mp7JrsTextAnnotationType_LocalType::GetFreeTextAnnotation() const
{
		return m_FreeTextAnnotation;
}

Mp7JrsStructuredAnnotationPtr Mp7JrsTextAnnotationType_LocalType::GetStructuredAnnotation() const
{
		return m_StructuredAnnotation;
}

Mp7JrsDependencyStructurePtr Mp7JrsTextAnnotationType_LocalType::GetDependencyStructure() const
{
		return m_DependencyStructure;
}

Mp7JrsKeywordAnnotationPtr Mp7JrsTextAnnotationType_LocalType::GetKeywordAnnotation() const
{
		return m_KeywordAnnotation;
}

// implementing setter for choice 
/* element */
void Mp7JrsTextAnnotationType_LocalType::SetFreeTextAnnotation(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType_LocalType::SetFreeTextAnnotation().");
	}
	if (m_FreeTextAnnotation != item)
	{
		// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
		m_FreeTextAnnotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FreeTextAnnotation) m_FreeTextAnnotation->SetParent(m_myself.getPointer());
		if (m_FreeTextAnnotation && m_FreeTextAnnotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FreeTextAnnotation->UseTypeAttribute = true;
		}
		if(m_FreeTextAnnotation != Mp7JrsTextualPtr())
		{
			m_FreeTextAnnotation->SetContentName(XMLString::transcode("FreeTextAnnotation"));
		}
	// Dc1Factory::DeleteObject(m_StructuredAnnotation);
	m_StructuredAnnotation = Mp7JrsStructuredAnnotationPtr();
	// Dc1Factory::DeleteObject(m_DependencyStructure);
	m_DependencyStructure = Mp7JrsDependencyStructurePtr();
	// Dc1Factory::DeleteObject(m_KeywordAnnotation);
	m_KeywordAnnotation = Mp7JrsKeywordAnnotationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTextAnnotationType_LocalType::SetStructuredAnnotation(const Mp7JrsStructuredAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType_LocalType::SetStructuredAnnotation().");
	}
	if (m_StructuredAnnotation != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredAnnotation);
		m_StructuredAnnotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredAnnotation) m_StructuredAnnotation->SetParent(m_myself.getPointer());
		if (m_StructuredAnnotation && m_StructuredAnnotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StructuredAnnotation->UseTypeAttribute = true;
		}
		if(m_StructuredAnnotation != Mp7JrsStructuredAnnotationPtr())
		{
			m_StructuredAnnotation->SetContentName(XMLString::transcode("StructuredAnnotation"));
		}
	// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
	m_FreeTextAnnotation = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_DependencyStructure);
	m_DependencyStructure = Mp7JrsDependencyStructurePtr();
	// Dc1Factory::DeleteObject(m_KeywordAnnotation);
	m_KeywordAnnotation = Mp7JrsKeywordAnnotationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTextAnnotationType_LocalType::SetDependencyStructure(const Mp7JrsDependencyStructurePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType_LocalType::SetDependencyStructure().");
	}
	if (m_DependencyStructure != item)
	{
		// Dc1Factory::DeleteObject(m_DependencyStructure);
		m_DependencyStructure = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DependencyStructure) m_DependencyStructure->SetParent(m_myself.getPointer());
		if (m_DependencyStructure && m_DependencyStructure->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DependencyStructure->UseTypeAttribute = true;
		}
		if(m_DependencyStructure != Mp7JrsDependencyStructurePtr())
		{
			m_DependencyStructure->SetContentName(XMLString::transcode("DependencyStructure"));
		}
	// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
	m_FreeTextAnnotation = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_StructuredAnnotation);
	m_StructuredAnnotation = Mp7JrsStructuredAnnotationPtr();
	// Dc1Factory::DeleteObject(m_KeywordAnnotation);
	m_KeywordAnnotation = Mp7JrsKeywordAnnotationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTextAnnotationType_LocalType::SetKeywordAnnotation(const Mp7JrsKeywordAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType_LocalType::SetKeywordAnnotation().");
	}
	if (m_KeywordAnnotation != item)
	{
		// Dc1Factory::DeleteObject(m_KeywordAnnotation);
		m_KeywordAnnotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeywordAnnotation) m_KeywordAnnotation->SetParent(m_myself.getPointer());
		if (m_KeywordAnnotation && m_KeywordAnnotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_KeywordAnnotation->UseTypeAttribute = true;
		}
		if(m_KeywordAnnotation != Mp7JrsKeywordAnnotationPtr())
		{
			m_KeywordAnnotation->SetContentName(XMLString::transcode("KeywordAnnotation"));
		}
	// Dc1Factory::DeleteObject(m_FreeTextAnnotation);
	m_FreeTextAnnotation = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_StructuredAnnotation);
	m_StructuredAnnotation = Mp7JrsStructuredAnnotationPtr();
	// Dc1Factory::DeleteObject(m_DependencyStructure);
	m_DependencyStructure = Mp7JrsDependencyStructurePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: TextAnnotationType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsTextAnnotationType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsTextAnnotationType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTextAnnotationType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTextAnnotationType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_FreeTextAnnotation != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FreeTextAnnotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FreeTextAnnotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FreeTextAnnotation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StructuredAnnotation != Mp7JrsStructuredAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StructuredAnnotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StructuredAnnotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StructuredAnnotation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DependencyStructure != Mp7JrsDependencyStructurePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DependencyStructure->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DependencyStructure"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DependencyStructure->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_KeywordAnnotation != Mp7JrsKeywordAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_KeywordAnnotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("KeywordAnnotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_KeywordAnnotation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTextAnnotationType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FreeTextAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FreeTextAnnotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFreeTextAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StructuredAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StructuredAnnotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateStructuredAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStructuredAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DependencyStructure"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DependencyStructure")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDependencyStructureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDependencyStructure(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("KeywordAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("KeywordAnnotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateKeywordAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKeywordAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextAnnotationType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FreeTextAnnotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetFreeTextAnnotation(child);
  }
  if (XMLString::compareString(elementname, X("StructuredAnnotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateStructuredAnnotationType; // FTT, check this
	}
	this->SetStructuredAnnotation(child);
  }
  if (XMLString::compareString(elementname, X("DependencyStructure")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDependencyStructureType; // FTT, check this
	}
	this->SetDependencyStructure(child);
  }
  if (XMLString::compareString(elementname, X("KeywordAnnotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateKeywordAnnotationType; // FTT, check this
	}
	this->SetKeywordAnnotation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextAnnotationType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFreeTextAnnotation() != Dc1NodePtr())
		result.Insert(GetFreeTextAnnotation());
	if (GetStructuredAnnotation() != Dc1NodePtr())
		result.Insert(GetStructuredAnnotation());
	if (GetDependencyStructure() != Dc1NodePtr())
		result.Insert(GetDependencyStructure());
	if (GetKeywordAnnotation() != Dc1NodePtr())
		result.Insert(GetKeywordAnnotation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_LocalType_ExtMethodImpl.h


