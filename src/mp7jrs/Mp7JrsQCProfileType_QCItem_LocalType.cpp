
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtImplInclude.h


#include "Mp7JrsQCProfileType_QCItem_Name_CollectionType.h"
#include "Mp7JrsQCProfileType_QCItem_Relevance_LocalType.h"
#include "Mp7JrsQCProfileType_QCItem_EssenceType_CollectionType.h"
#include "Mp7JrsQCProfileType_QCItem_InputParameter_CollectionType.h"
#include "Mp7JrsQCProfileType_QCItem_ItemScope_CollectionType.h"
#include "Mp7JrsQCProfileType_QCItem_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsQCValueType.h" // Element collection urn:mpeg:mpeg7:schema:2004:InputParameter
#include "Mp7JrsSpatioTemporalLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ItemScope

#include <assert.h>
IMp7JrsQCProfileType_QCItem_LocalType::IMp7JrsQCProfileType_QCItem_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtPropInit.h

}

IMp7JrsQCProfileType_QCItem_LocalType::~IMp7JrsQCProfileType_QCItem_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtPropCleanup.h

}

Mp7JrsQCProfileType_QCItem_LocalType::Mp7JrsQCProfileType_QCItem_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsQCProfileType_QCItem_LocalType::~Mp7JrsQCProfileType_QCItem_LocalType()
{
	Cleanup();
}

void Mp7JrsQCProfileType_QCItem_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsQCProfileType_QCItem_Name_CollectionPtr(); // Collection
	m_Layer = Mp7JrsQCProfileType_QCItem_Layer_LocalType::UninitializedEnumeration; // Enumeration
	m_Layer_Exist = false;
	m_Category = Mp7JrsQCProfileType_QCItem_Category_LocalType::UninitializedEnumeration; // Enumeration
	m_Category_Exist = false;
	m_Relevance = Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr(); // Class with content 
	m_Relevance_Exist = false;
	m_EssenceType = Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr(); // Collection
	m_InputParameter = Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr(); // Collection
	m_ItemScope = Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtMyPropInit.h

}

void Mp7JrsQCProfileType_QCItem_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Relevance);
	// Dc1Factory::DeleteObject(m_EssenceType);
	// Dc1Factory::DeleteObject(m_InputParameter);
	// Dc1Factory::DeleteObject(m_ItemScope);
}

void Mp7JrsQCProfileType_QCItem_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QCProfileType_QCItem_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IQCProfileType_QCItem_LocalType
	const Dc1Ptr< Mp7JrsQCProfileType_QCItem_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	if (tmp->IsValidLayer())
	{
		this->SetLayer(tmp->GetLayer());
	}
	else
	{
		InvalidateLayer();
	}
	if (tmp->IsValidCategory())
	{
		this->SetCategory(tmp->GetCategory());
	}
	else
	{
		InvalidateCategory();
	}
	if (tmp->IsValidRelevance())
	{
		// Dc1Factory::DeleteObject(m_Relevance);
		this->SetRelevance(Dc1Factory::CloneObject(tmp->GetRelevance()));
	}
	else
	{
		InvalidateRelevance();
	}
		// Dc1Factory::DeleteObject(m_EssenceType);
		this->SetEssenceType(Dc1Factory::CloneObject(tmp->GetEssenceType()));
		// Dc1Factory::DeleteObject(m_InputParameter);
		this->SetInputParameter(Dc1Factory::CloneObject(tmp->GetInputParameter()));
		// Dc1Factory::DeleteObject(m_ItemScope);
		this->SetItemScope(Dc1Factory::CloneObject(tmp->GetItemScope()));
}

Dc1NodePtr Mp7JrsQCProfileType_QCItem_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsQCProfileType_QCItem_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsQCProfileType_QCItem_Name_CollectionPtr Mp7JrsQCProfileType_QCItem_LocalType::GetName() const
{
		return m_Name;
}

Mp7JrsQCProfileType_QCItem_Layer_LocalType::Enumeration Mp7JrsQCProfileType_QCItem_LocalType::GetLayer() const
{
		return m_Layer;
}

// Element is optional
bool Mp7JrsQCProfileType_QCItem_LocalType::IsValidLayer() const
{
	return m_Layer_Exist;
}

Mp7JrsQCProfileType_QCItem_Category_LocalType::Enumeration Mp7JrsQCProfileType_QCItem_LocalType::GetCategory() const
{
		return m_Category;
}

// Element is optional
bool Mp7JrsQCProfileType_QCItem_LocalType::IsValidCategory() const
{
	return m_Category_Exist;
}

Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr Mp7JrsQCProfileType_QCItem_LocalType::GetRelevance() const
{
		return m_Relevance;
}

// Element is optional
bool Mp7JrsQCProfileType_QCItem_LocalType::IsValidRelevance() const
{
	return m_Relevance_Exist;
}

Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr Mp7JrsQCProfileType_QCItem_LocalType::GetEssenceType() const
{
		return m_EssenceType;
}

Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr Mp7JrsQCProfileType_QCItem_LocalType::GetInputParameter() const
{
		return m_InputParameter;
}

Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr Mp7JrsQCProfileType_QCItem_LocalType::GetItemScope() const
{
		return m_ItemScope;
}

void Mp7JrsQCProfileType_QCItem_LocalType::SetName(const Mp7JrsQCProfileType_QCItem_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != Mp7JrsQCProfileType_QCItem_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::SetLayer(Mp7JrsQCProfileType_QCItem_Layer_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetLayer().");
	}
	if (m_Layer != item || m_Layer_Exist == false)
	{
		// nothing to free here, hopefully
		m_Layer = item;
		m_Layer_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::InvalidateLayer()
{
	m_Layer_Exist = false;
}
void Mp7JrsQCProfileType_QCItem_LocalType::SetCategory(Mp7JrsQCProfileType_QCItem_Category_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetCategory().");
	}
	if (m_Category != item || m_Category_Exist == false)
	{
		// nothing to free here, hopefully
		m_Category = item;
		m_Category_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::InvalidateCategory()
{
	m_Category_Exist = false;
}
void Mp7JrsQCProfileType_QCItem_LocalType::SetRelevance(const Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetRelevance().");
	}
	if (m_Relevance != item || m_Relevance_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Relevance);
		m_Relevance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relevance) m_Relevance->SetParent(m_myself.getPointer());
		if (m_Relevance && m_Relevance->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Relevance_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Relevance->UseTypeAttribute = true;
		}
		m_Relevance_Exist = true;
		if(m_Relevance != Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr())
		{
			m_Relevance->SetContentName(XMLString::transcode("Relevance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::InvalidateRelevance()
{
	m_Relevance_Exist = false;
}
void Mp7JrsQCProfileType_QCItem_LocalType::SetEssenceType(const Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetEssenceType().");
	}
	if (m_EssenceType != item)
	{
		// Dc1Factory::DeleteObject(m_EssenceType);
		m_EssenceType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EssenceType) m_EssenceType->SetParent(m_myself.getPointer());
		if(m_EssenceType != Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr())
		{
			m_EssenceType->SetContentName(XMLString::transcode("EssenceType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::SetInputParameter(const Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetInputParameter().");
	}
	if (m_InputParameter != item)
	{
		// Dc1Factory::DeleteObject(m_InputParameter);
		m_InputParameter = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InputParameter) m_InputParameter->SetParent(m_myself.getPointer());
		if(m_InputParameter != Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr())
		{
			m_InputParameter->SetContentName(XMLString::transcode("InputParameter"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType_QCItem_LocalType::SetItemScope(const Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType_QCItem_LocalType::SetItemScope().");
	}
	if (m_ItemScope != item)
	{
		// Dc1Factory::DeleteObject(m_ItemScope);
		m_ItemScope = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ItemScope) m_ItemScope->SetParent(m_myself.getPointer());
		if(m_ItemScope != Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr())
		{
			m_ItemScope->SetContentName(XMLString::transcode("ItemScope"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsQCProfileType_QCItem_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Name is item of type ControlledTermUseType
		// in element collection QCProfileType_QCItem_Name_CollectionType
		
		context->Found = true;
		Mp7JrsQCProfileType_QCItem_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCProfileType_QCItem_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relevance")) == 0)
	{
		// Relevance is simple element QCProfileType_QCItem_Relevance_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRelevance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_Relevance_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRelevance(p, client);
					if((p = GetRelevance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EssenceType")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "Mp7JrsQCProfileType_QCItem_EssenceType_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("InputParameter")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:InputParameter is item of type QCValueType
		// in element collection QCProfileType_QCItem_InputParameter_CollectionType
		
		context->Found = true;
		Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr coll = GetInputParameter();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCProfileType_QCItem_InputParameter_CollectionType; // FTT, check this
				SetInputParameter(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCValueType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCValuePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ItemScope")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ItemScope is item of type SpatioTemporalLocatorType
		// in element collection QCProfileType_QCItem_ItemScope_CollectionType
		
		context->Found = true;
		Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr coll = GetItemScope();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCProfileType_QCItem_ItemScope_CollectionType; // FTT, check this
				SetItemScope(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatioTemporalLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for QCProfileType_QCItem_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "QCProfileType_QCItem_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsQCProfileType_QCItem_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsQCProfileType_QCItem_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsQCProfileType_QCItem_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("QCProfileType_QCItem_LocalType"));
	// Element serialization:
	if (m_Name != Mp7JrsQCProfileType_QCItem_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_Layer != Mp7JrsQCProfileType_QCItem_Layer_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsQCProfileType_QCItem_Layer_LocalType::ToText(m_Layer);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Layer"), true);
		XMLString::release(&tmp);
	}
	if(m_Category != Mp7JrsQCProfileType_QCItem_Category_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsQCProfileType_QCItem_Category_LocalType::ToText(m_Category);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Category"), true);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_Relevance != Mp7JrsQCProfileType_QCItem_Relevance_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Relevance->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Relevance"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Relevance->Serialize(doc, element, &elem);
	}
	if (m_EssenceType != Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_EssenceType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_InputParameter != Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_InputParameter->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ItemScope != Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ItemScope->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsQCProfileType_QCItem_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCProfileType_QCItem_Name_CollectionPtr tmp = CreateQCProfileType_QCItem_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Layer"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Layer")) == 0))
		{
			Mp7JrsQCProfileType_QCItem_Layer_LocalType::Enumeration tmp = Mp7JrsQCProfileType_QCItem_Layer_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsQCProfileType_QCItem_Layer_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetLayer(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Category"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Category")) == 0))
		{
			Mp7JrsQCProfileType_QCItem_Category_LocalType::Enumeration tmp = Mp7JrsQCProfileType_QCItem_Category_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsQCProfileType_QCItem_Category_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetCategory(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Relevance"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Relevance")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateQCProfileType_QCItem_Relevance_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelevance(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("EssenceType"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("EssenceType")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr tmp = CreateQCProfileType_QCItem_EssenceType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEssenceType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("InputParameter"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("InputParameter")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr tmp = CreateQCProfileType_QCItem_InputParameter_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInputParameter(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ItemScope"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ItemScope")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr tmp = CreateQCProfileType_QCItem_ItemScope_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetItemScope(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsQCProfileType_QCItem_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	Mp7JrsQCProfileType_QCItem_Name_CollectionPtr tmp = CreateQCProfileType_QCItem_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Relevance")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateQCProfileType_QCItem_Relevance_LocalType; // FTT, check this
	}
	this->SetRelevance(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("EssenceType")) == 0))
  {
	Mp7JrsQCProfileType_QCItem_EssenceType_CollectionPtr tmp = CreateQCProfileType_QCItem_EssenceType_CollectionType; // FTT, check this
	this->SetEssenceType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("InputParameter")) == 0))
  {
	Mp7JrsQCProfileType_QCItem_InputParameter_CollectionPtr tmp = CreateQCProfileType_QCItem_InputParameter_CollectionType; // FTT, check this
	this->SetInputParameter(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ItemScope")) == 0))
  {
	Mp7JrsQCProfileType_QCItem_ItemScope_CollectionPtr tmp = CreateQCProfileType_QCItem_ItemScope_CollectionType; // FTT, check this
	this->SetItemScope(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsQCProfileType_QCItem_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetRelevance() != Dc1NodePtr())
		result.Insert(GetRelevance());
	if (GetEssenceType() != Dc1NodePtr())
		result.Insert(GetEssenceType());
	if (GetInputParameter() != Dc1NodePtr())
		result.Insert(GetInputParameter());
	if (GetItemScope() != Dc1NodePtr())
		result.Insert(GetItemScope());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsQCProfileType_QCItem_LocalType_ExtMethodImpl.h


