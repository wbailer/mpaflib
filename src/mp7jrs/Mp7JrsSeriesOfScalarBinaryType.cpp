
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtImplInclude.h


#include "Mp7JrsSeriesOfScalarType.h"
#include "Mp7JrsFloatMatrixType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsScalableSeriesType_Scaling_CollectionType.h"
#include "Mp7JrsSeriesOfScalarBinaryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsScalableSeriesType_Scaling_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Scaling

#include <assert.h>
IMp7JrsSeriesOfScalarBinaryType::IMp7JrsSeriesOfScalarBinaryType()
{

// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtPropInit.h

}

IMp7JrsSeriesOfScalarBinaryType::~IMp7JrsSeriesOfScalarBinaryType()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtPropCleanup.h

}

Mp7JrsSeriesOfScalarBinaryType::Mp7JrsSeriesOfScalarBinaryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSeriesOfScalarBinaryType::~Mp7JrsSeriesOfScalarBinaryType()
{
	Cleanup();
}

void Mp7JrsSeriesOfScalarBinaryType::Init()
{
	// Init base
	m_Base = CreateSeriesOfScalarType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_VarianceScalewise = Mp7JrsFloatMatrixPtr(); // Class
	m_VarianceScalewise_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtMyPropInit.h

}

void Mp7JrsSeriesOfScalarBinaryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_VarianceScalewise);
}

void Mp7JrsSeriesOfScalarBinaryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SeriesOfScalarBinaryTypePtr, since we
	// might need GetBase(), which isn't defined in ISeriesOfScalarBinaryType
	const Dc1Ptr< Mp7JrsSeriesOfScalarBinaryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSeriesOfScalarPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSeriesOfScalarBinaryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidVarianceScalewise())
	{
		// Dc1Factory::DeleteObject(m_VarianceScalewise);
		this->SetVarianceScalewise(Dc1Factory::CloneObject(tmp->GetVarianceScalewise()));
	}
	else
	{
		InvalidateVarianceScalewise();
	}
}

Dc1NodePtr Mp7JrsSeriesOfScalarBinaryType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSeriesOfScalarBinaryType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSeriesOfScalarType > Mp7JrsSeriesOfScalarBinaryType::GetBase() const
{
	return m_Base;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfScalarBinaryType::GetVarianceScalewise() const
{
		return m_VarianceScalewise;
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidVarianceScalewise() const
{
	return m_VarianceScalewise_Exist;
}

void Mp7JrsSeriesOfScalarBinaryType::SetVarianceScalewise(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetVarianceScalewise().");
	}
	if (m_VarianceScalewise != item || m_VarianceScalewise_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VarianceScalewise);
		m_VarianceScalewise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VarianceScalewise) m_VarianceScalewise->SetParent(m_myself.getPointer());
		if (m_VarianceScalewise && m_VarianceScalewise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VarianceScalewise->UseTypeAttribute = true;
		}
		m_VarianceScalewise_Exist = true;
		if(m_VarianceScalewise != Mp7JrsFloatMatrixPtr())
		{
			m_VarianceScalewise->SetContentName(XMLString::transcode("VarianceScalewise"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateVarianceScalewise()
{
	m_VarianceScalewise_Exist = false;
}
Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetRaw() const
{
	return GetBase()->GetRaw();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidRaw() const
{
	return GetBase()->IsValidRaw();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetMin() const
{
	return GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidMin() const
{
	return GetBase()->IsValidMin();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetMax() const
{
	return GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidMax() const
{
	return GetBase()->IsValidMax();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetMean() const
{
	return GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidMean() const
{
	return GetBase()->IsValidMean();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetRandom() const
{
	return GetBase()->GetRandom();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidRandom() const
{
	return GetBase()->IsValidRandom();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetFirst() const
{
	return GetBase()->GetFirst();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidFirst() const
{
	return GetBase()->IsValidFirst();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetLast() const
{
	return GetBase()->GetLast();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidLast() const
{
	return GetBase()->IsValidLast();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetVariance() const
{
	return GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidVariance() const
{
	return GetBase()->IsValidVariance();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarBinaryType::GetWeight() const
{
	return GetBase()->GetWeight();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidWeight() const
{
	return GetBase()->IsValidWeight();
}

float Mp7JrsSeriesOfScalarBinaryType::GetLogBase() const
{
	return GetBase()->GetLogBase();
}

// Element is optional
bool Mp7JrsSeriesOfScalarBinaryType::IsValidLogBase() const
{
	return GetBase()->IsValidLogBase();
}

void Mp7JrsSeriesOfScalarBinaryType::SetRaw(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetRaw().");
	}
	GetBase()->SetRaw(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateRaw()
{
	GetBase()->InvalidateRaw();
}
void Mp7JrsSeriesOfScalarBinaryType::SetMin(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetMin().");
	}
	GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateMin()
{
	GetBase()->InvalidateMin();
}
void Mp7JrsSeriesOfScalarBinaryType::SetMax(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetMax().");
	}
	GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateMax()
{
	GetBase()->InvalidateMax();
}
void Mp7JrsSeriesOfScalarBinaryType::SetMean(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetMean().");
	}
	GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateMean()
{
	GetBase()->InvalidateMean();
}
void Mp7JrsSeriesOfScalarBinaryType::SetRandom(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetRandom().");
	}
	GetBase()->SetRandom(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateRandom()
{
	GetBase()->InvalidateRandom();
}
void Mp7JrsSeriesOfScalarBinaryType::SetFirst(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetFirst().");
	}
	GetBase()->SetFirst(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateFirst()
{
	GetBase()->InvalidateFirst();
}
void Mp7JrsSeriesOfScalarBinaryType::SetLast(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetLast().");
	}
	GetBase()->SetLast(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateLast()
{
	GetBase()->InvalidateLast();
}
void Mp7JrsSeriesOfScalarBinaryType::SetVariance(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetVariance().");
	}
	GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateVariance()
{
	GetBase()->InvalidateVariance();
}
void Mp7JrsSeriesOfScalarBinaryType::SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetWeight().");
	}
	GetBase()->SetWeight(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateWeight()
{
	GetBase()->InvalidateWeight();
}
void Mp7JrsSeriesOfScalarBinaryType::SetLogBase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetLogBase().");
	}
	GetBase()->SetLogBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarBinaryType::InvalidateLogBase()
{
	GetBase()->InvalidateLogBase();
}
unsigned Mp7JrsSeriesOfScalarBinaryType::GettotalNumOfSamples() const
{
	return GetBase()->GetBase()->GettotalNumOfSamples();
}

void Mp7JrsSeriesOfScalarBinaryType::SettotalNumOfSamples(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SettotalNumOfSamples().");
	}
	GetBase()->GetBase()->SettotalNumOfSamples(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableSeriesType_Scaling_CollectionPtr Mp7JrsSeriesOfScalarBinaryType::GetScaling() const
{
	return GetBase()->GetBase()->GetScaling();
}

void Mp7JrsSeriesOfScalarBinaryType::SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarBinaryType::SetScaling().");
	}
	GetBase()->GetBase()->SetScaling(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSeriesOfScalarBinaryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VarianceScalewise")) == 0)
	{
		// VarianceScalewise is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVarianceScalewise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVarianceScalewise(p, client);
					if((p = GetVarianceScalewise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SeriesOfScalarBinaryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SeriesOfScalarBinaryType");
	}
	return result;
}

XMLCh * Mp7JrsSeriesOfScalarBinaryType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSeriesOfScalarBinaryType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSeriesOfScalarBinaryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SeriesOfScalarBinaryType"));
	// Element serialization:
	// Class
	
	if (m_VarianceScalewise != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VarianceScalewise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VarianceScalewise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VarianceScalewise->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSeriesOfScalarBinaryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSeriesOfScalarType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VarianceScalewise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VarianceScalewise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVarianceScalewise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSeriesOfScalarBinaryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("VarianceScalewise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetVarianceScalewise(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSeriesOfScalarBinaryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVarianceScalewise() != Dc1NodePtr())
		result.Insert(GetVarianceScalewise());
	if (GetRaw() != Dc1NodePtr())
		result.Insert(GetRaw());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetRandom() != Dc1NodePtr())
		result.Insert(GetRandom());
	if (GetFirst() != Dc1NodePtr())
		result.Insert(GetFirst());
	if (GetLast() != Dc1NodePtr())
		result.Insert(GetLast());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetWeight() != Dc1NodePtr())
		result.Insert(GetWeight());
	if (GetScaling() != Dc1NodePtr())
		result.Insert(GetScaling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarBinaryType_ExtMethodImpl.h


