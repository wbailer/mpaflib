
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtImplInclude.h


#include "Mp7JrsMediaSourceSegmentDecompositionType.h"
#include "Mp7JrsTextDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsTextDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextDecompositionType_LocalType.h" // Choice collection TextSegment
#include "Mp7JrsTextSegmentType.h" // Choice collection element TextSegment
#include "Mp7JrsReferenceType.h" // Choice collection element TextSegmentRef

#include <assert.h>
IMp7JrsTextDecompositionType::IMp7JrsTextDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtPropInit.h

}

IMp7JrsTextDecompositionType::~IMp7JrsTextDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtPropCleanup.h

}

Mp7JrsTextDecompositionType::Mp7JrsTextDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextDecompositionType::~Mp7JrsTextDecompositionType()
{
	Cleanup();
}

void Mp7JrsTextDecompositionType::Init()
{
	// Init base
	m_Base = CreateMediaSourceSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_TextDecompositionType_LocalType = Mp7JrsTextDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtMyPropInit.h

}

void Mp7JrsTextDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_TextDecompositionType_LocalType);
}

void Mp7JrsTextDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in ITextDecompositionType
	const Dc1Ptr< Mp7JrsTextDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaSourceSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsTextDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_TextDecompositionType_LocalType);
		this->SetTextDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetTextDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsTextDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsTextDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMediaSourceSegmentDecompositionType > Mp7JrsTextDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsTextDecompositionType_CollectionPtr Mp7JrsTextDecompositionType::GetTextDecompositionType_LocalType() const
{
		return m_TextDecompositionType_LocalType;
}

void Mp7JrsTextDecompositionType::SetTextDecompositionType_LocalType(const Mp7JrsTextDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SetTextDecompositionType_LocalType().");
	}
	if (m_TextDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_TextDecompositionType_LocalType);
		m_TextDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextDecompositionType_LocalType) m_TextDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsTextDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsTextDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsTextDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsTextDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsTextDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsTextDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsTextDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsTextDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsTextDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsTextDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsTextDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsTextDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsTextDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsTextDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsTextDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsTextDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsTextDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsTextDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsTextDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsTextDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsTextDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsTextDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsTextDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsTextDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsTextDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsTextDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTextDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TextSegment")) == 0)
	{
		// TextSegment is contained in itemtype TextDecompositionType_LocalType
		// in choice collection TextDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsTextDecompositionType_CollectionPtr coll = GetTextDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextDecompositionType_CollectionType; // FTT, check this
				SetTextDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->GetTextSegment()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextDecompositionType_LocalPtr item = CreateTextDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextSegmentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->SetTextSegment(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->GetTextSegment()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextSegmentRef")) == 0)
	{
		// TextSegmentRef is contained in itemtype TextDecompositionType_LocalType
		// in choice collection TextDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsTextDecompositionType_CollectionPtr coll = GetTextDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextDecompositionType_CollectionType; // FTT, check this
				SetTextDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->GetTextSegmentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextDecompositionType_LocalPtr item = CreateTextDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->SetTextSegmentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextDecompositionType_LocalPtr)coll->elementAt(i))->GetTextSegmentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TextDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TextDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsTextDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsTextDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsTextDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextDecompositionType"));
	// Element serialization:
	if (m_TextDecompositionType_LocalType != Mp7JrsTextDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TextDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsTextDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaSourceSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:TextSegment
			Dc1Util::HasNodeName(parent, X("TextSegment"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TextSegment")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:TextSegmentRef
			Dc1Util::HasNodeName(parent, X("TextSegmentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TextSegmentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsTextDecompositionType_CollectionPtr tmp = CreateTextDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTextDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TextSegment"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TextSegment")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TextSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TextSegmentRef")) == 0)
	)
  {
	Mp7JrsTextDecompositionType_CollectionPtr tmp = CreateTextDecompositionType_CollectionType; // FTT, check this
	this->SetTextDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTextDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetTextDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_ExtMethodImpl.h


