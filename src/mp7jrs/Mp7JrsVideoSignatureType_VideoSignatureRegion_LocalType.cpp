
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtImplInclude.h


#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType.h"
#include "Mp7Jrsunsigned32.h"
#include "Mp7Jrsunsigned16.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VSVideoSegment
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VideoFrame

#include <assert.h>
IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtPropInit.h

}

IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::~IMp7JrsVideoSignatureType_VideoSignatureRegion_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtPropCleanup.h

}

Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::~Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType()
{
	Cleanup();
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_VideoSignatureSpatialRegion = Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr(); // Class
	m_VideoSignatureSpatialRegion_Exist = false;
	m_StartFrameOfSpatialRegion = Mp7Jrsunsigned32Ptr(); // Class with content 
	m_MediaTimeUnit = Mp7Jrsunsigned16Ptr(); // Class with content 
	m_MediaTimeOfSpatialRegion = Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr(); // Class
	m_MediaTimeOfSpatialRegion_Exist = false;
	m_VSVideoSegment = Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr(); // Collection
	m_VideoFrame = Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtMyPropInit.h

}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_VideoSignatureSpatialRegion);
	// Dc1Factory::DeleteObject(m_StartFrameOfSpatialRegion);
	// Dc1Factory::DeleteObject(m_MediaTimeUnit);
	// Dc1Factory::DeleteObject(m_MediaTimeOfSpatialRegion);
	// Dc1Factory::DeleteObject(m_VSVideoSegment);
	// Dc1Factory::DeleteObject(m_VideoFrame);
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSignatureType_VideoSignatureRegion_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSignatureType_VideoSignatureRegion_LocalType
	const Dc1Ptr< Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidVideoSignatureSpatialRegion())
	{
		// Dc1Factory::DeleteObject(m_VideoSignatureSpatialRegion);
		this->SetVideoSignatureSpatialRegion(Dc1Factory::CloneObject(tmp->GetVideoSignatureSpatialRegion()));
	}
	else
	{
		InvalidateVideoSignatureSpatialRegion();
	}
		// Dc1Factory::DeleteObject(m_StartFrameOfSpatialRegion);
		this->SetStartFrameOfSpatialRegion(Dc1Factory::CloneObject(tmp->GetStartFrameOfSpatialRegion()));
		// Dc1Factory::DeleteObject(m_MediaTimeUnit);
		this->SetMediaTimeUnit(Dc1Factory::CloneObject(tmp->GetMediaTimeUnit()));
	if (tmp->IsValidMediaTimeOfSpatialRegion())
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfSpatialRegion);
		this->SetMediaTimeOfSpatialRegion(Dc1Factory::CloneObject(tmp->GetMediaTimeOfSpatialRegion()));
	}
	else
	{
		InvalidateMediaTimeOfSpatialRegion();
	}
		// Dc1Factory::DeleteObject(m_VSVideoSegment);
		this->SetVSVideoSegment(Dc1Factory::CloneObject(tmp->GetVSVideoSegment()));
		// Dc1Factory::DeleteObject(m_VideoFrame);
		this->SetVideoFrame(Dc1Factory::CloneObject(tmp->GetVideoFrame()));
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetVideoSignatureSpatialRegion() const
{
		return m_VideoSignatureSpatialRegion;
}

// Element is optional
bool Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::IsValidVideoSignatureSpatialRegion() const
{
	return m_VideoSignatureSpatialRegion_Exist;
}

Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetStartFrameOfSpatialRegion() const
{
		return m_StartFrameOfSpatialRegion;
}

Mp7Jrsunsigned16Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetMediaTimeUnit() const
{
		return m_MediaTimeUnit;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetMediaTimeOfSpatialRegion() const
{
		return m_MediaTimeOfSpatialRegion;
}

// Element is optional
bool Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::IsValidMediaTimeOfSpatialRegion() const
{
	return m_MediaTimeOfSpatialRegion_Exist;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetVSVideoSegment() const
{
		return m_VSVideoSegment;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetVideoFrame() const
{
		return m_VideoFrame;
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVideoSignatureSpatialRegion(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVideoSignatureSpatialRegion().");
	}
	if (m_VideoSignatureSpatialRegion != item || m_VideoSignatureSpatialRegion_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VideoSignatureSpatialRegion);
		m_VideoSignatureSpatialRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSignatureSpatialRegion) m_VideoSignatureSpatialRegion->SetParent(m_myself.getPointer());
		if (m_VideoSignatureSpatialRegion && m_VideoSignatureSpatialRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSignatureSpatialRegion->UseTypeAttribute = true;
		}
		m_VideoSignatureSpatialRegion_Exist = true;
		if(m_VideoSignatureSpatialRegion != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr())
		{
			m_VideoSignatureSpatialRegion->SetContentName(XMLString::transcode("VideoSignatureSpatialRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::InvalidateVideoSignatureSpatialRegion()
{
	m_VideoSignatureSpatialRegion_Exist = false;
}
void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetStartFrameOfSpatialRegion(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetStartFrameOfSpatialRegion().");
	}
	if (m_StartFrameOfSpatialRegion != item)
	{
		// Dc1Factory::DeleteObject(m_StartFrameOfSpatialRegion);
		m_StartFrameOfSpatialRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StartFrameOfSpatialRegion) m_StartFrameOfSpatialRegion->SetParent(m_myself.getPointer());
		if (m_StartFrameOfSpatialRegion && m_StartFrameOfSpatialRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StartFrameOfSpatialRegion->UseTypeAttribute = true;
		}
		if(m_StartFrameOfSpatialRegion != Mp7Jrsunsigned32Ptr())
		{
			m_StartFrameOfSpatialRegion->SetContentName(XMLString::transcode("StartFrameOfSpatialRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetMediaTimeUnit(const Mp7Jrsunsigned16Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetMediaTimeUnit().");
	}
	if (m_MediaTimeUnit != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimeUnit);
		m_MediaTimeUnit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimeUnit) m_MediaTimeUnit->SetParent(m_myself.getPointer());
		if (m_MediaTimeUnit && m_MediaTimeUnit->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned16"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimeUnit->UseTypeAttribute = true;
		}
		if(m_MediaTimeUnit != Mp7Jrsunsigned16Ptr())
		{
			m_MediaTimeUnit->SetContentName(XMLString::transcode("MediaTimeUnit"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetMediaTimeOfSpatialRegion(const Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetMediaTimeOfSpatialRegion().");
	}
	if (m_MediaTimeOfSpatialRegion != item || m_MediaTimeOfSpatialRegion_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfSpatialRegion);
		m_MediaTimeOfSpatialRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimeOfSpatialRegion) m_MediaTimeOfSpatialRegion->SetParent(m_myself.getPointer());
		if (m_MediaTimeOfSpatialRegion && m_MediaTimeOfSpatialRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimeOfSpatialRegion->UseTypeAttribute = true;
		}
		m_MediaTimeOfSpatialRegion_Exist = true;
		if(m_MediaTimeOfSpatialRegion != Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr())
		{
			m_MediaTimeOfSpatialRegion->SetContentName(XMLString::transcode("MediaTimeOfSpatialRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::InvalidateMediaTimeOfSpatialRegion()
{
	m_MediaTimeOfSpatialRegion_Exist = false;
}
void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVSVideoSegment(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVSVideoSegment().");
	}
	if (m_VSVideoSegment != item)
	{
		// Dc1Factory::DeleteObject(m_VSVideoSegment);
		m_VSVideoSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VSVideoSegment) m_VSVideoSegment->SetParent(m_myself.getPointer());
		if(m_VSVideoSegment != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr())
		{
			m_VSVideoSegment->SetContentName(XMLString::transcode("VSVideoSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVideoFrame(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::SetVideoFrame().");
	}
	if (m_VideoFrame != item)
	{
		// Dc1Factory::DeleteObject(m_VideoFrame);
		m_VideoFrame = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoFrame) m_VideoFrame->SetParent(m_myself.getPointer());
		if(m_VideoFrame != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr())
		{
			m_VideoFrame->SetContentName(XMLString::transcode("VideoFrame"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VideoSignatureSpatialRegion")) == 0)
	{
		// VideoSignatureSpatialRegion is simple element VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVideoSignatureSpatialRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVideoSignatureSpatialRegion(p, client);
					if((p = GetVideoSignatureSpatialRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StartFrameOfSpatialRegion")) == 0)
	{
		// StartFrameOfSpatialRegion is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStartFrameOfSpatialRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStartFrameOfSpatialRegion(p, client);
					if((p = GetStartFrameOfSpatialRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTimeUnit")) == 0)
	{
		// MediaTimeUnit is simple element unsigned16
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimeUnit()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned16")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned16Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimeUnit(p, client);
					if((p = GetMediaTimeUnit()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTimeOfSpatialRegion")) == 0)
	{
		// MediaTimeOfSpatialRegion is simple element VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimeOfSpatialRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimeOfSpatialRegion(p, client);
					if((p = GetMediaTimeOfSpatialRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VSVideoSegment")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VSVideoSegment is item of type VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType
		// in element collection VideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType
		
		context->Found = true;
		Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr coll = GetVSVideoSegment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType; // FTT, check this
				SetVSVideoSegment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VideoFrame")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VideoFrame is item of type VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType
		// in element collection VideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType
		
		context->Found = true;
		Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr coll = GetVideoFrame();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType; // FTT, check this
				SetVideoFrame(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSignatureType_VideoSignatureRegion_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSignatureType_VideoSignatureRegion_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSignatureType_VideoSignatureRegion_LocalType"));
	// Element serialization:
	// Class
	
	if (m_VideoSignatureSpatialRegion != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSignatureSpatialRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSignatureSpatialRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSignatureSpatialRegion->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_StartFrameOfSpatialRegion != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StartFrameOfSpatialRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StartFrameOfSpatialRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StartFrameOfSpatialRegion->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaTimeUnit != Mp7Jrsunsigned16Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTimeUnit->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTimeUnit"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTimeUnit->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaTimeOfSpatialRegion != Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTimeOfSpatialRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTimeOfSpatialRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTimeOfSpatialRegion->Serialize(doc, element, &elem);
	}
	if (m_VSVideoSegment != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VSVideoSegment->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_VideoFrame != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VideoFrame->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSignatureSpatialRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSignatureSpatialRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSignatureSpatialRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StartFrameOfSpatialRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StartFrameOfSpatialRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStartFrameOfSpatialRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTimeUnit"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimeUnit")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned16; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTimeUnit(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTimeOfSpatialRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimeOfSpatialRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTimeOfSpatialRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VSVideoSegment"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VSVideoSegment")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr tmp = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVSVideoSegment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VideoFrame"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VideoFrame")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr tmp = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVideoFrame(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("VideoSignatureSpatialRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSignatureType_VideoSignatureRegion_VideoSignatureSpatialRegion_LocalType; // FTT, check this
	}
	this->SetVideoSignatureSpatialRegion(child);
  }
  if (XMLString::compareString(elementname, X("StartFrameOfSpatialRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetStartFrameOfSpatialRegion(child);
  }
  if (XMLString::compareString(elementname, X("MediaTimeUnit")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned16; // FTT, check this
	}
	this->SetMediaTimeUnit(child);
  }
  if (XMLString::compareString(elementname, X("MediaTimeOfSpatialRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType; // FTT, check this
	}
	this->SetMediaTimeOfSpatialRegion(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VSVideoSegment")) == 0))
  {
	Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionPtr tmp = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_CollectionType; // FTT, check this
	this->SetVSVideoSegment(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VideoFrame")) == 0))
  {
	Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionPtr tmp = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_CollectionType; // FTT, check this
	this->SetVideoFrame(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVideoSignatureSpatialRegion() != Dc1NodePtr())
		result.Insert(GetVideoSignatureSpatialRegion());
	if (GetStartFrameOfSpatialRegion() != Dc1NodePtr())
		result.Insert(GetStartFrameOfSpatialRegion());
	if (GetMediaTimeUnit() != Dc1NodePtr())
		result.Insert(GetMediaTimeUnit());
	if (GetMediaTimeOfSpatialRegion() != Dc1NodePtr())
		result.Insert(GetMediaTimeOfSpatialRegion());
	if (GetVSVideoSegment() != Dc1NodePtr())
		result.Insert(GetVSVideoSegment());
	if (GetVideoFrame() != Dc1NodePtr())
		result.Insert(GetVideoFrame());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_LocalType_ExtMethodImpl.h


