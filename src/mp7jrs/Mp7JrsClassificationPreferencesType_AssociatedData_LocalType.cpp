
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtImplInclude.h


#include "Mp7JrsTextualType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsTextualBaseType_phoneticTranscription_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_AssociatedData_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsClassificationPreferencesType_AssociatedData_LocalType::IMp7JrsClassificationPreferencesType_AssociatedData_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtPropInit.h

}

IMp7JrsClassificationPreferencesType_AssociatedData_LocalType::~IMp7JrsClassificationPreferencesType_AssociatedData_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtPropCleanup.h

}

Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Mp7JrsClassificationPreferencesType_AssociatedData_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::~Mp7JrsClassificationPreferencesType_AssociatedData_LocalType()
{
	Cleanup();
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Init()
{
	// Init base
	m_Base = CreateTextualType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;



// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtMyPropInit.h

}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationPreferencesType_AssociatedData_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationPreferencesType_AssociatedData_LocalType
	const Dc1Ptr< Mp7JrsClassificationPreferencesType_AssociatedData_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTextualPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationPreferencesType_AssociatedData_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
}

Dc1NodePtr Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTextualType > Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetBase() const
{
	return m_Base;
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetContent().");
	}
	GetBase()->GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetContent() const
{
	return GetBase()->GetBase()->GetContent();
}
Mp7JrspreferenceValuePtr Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Getlang() const
{
	return GetBase()->GetBase()->Getlang();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Existlang() const
{
	return GetBase()->GetBase()->Existlang();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setlang().");
	}
	GetBase()->GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Invalidatelang()
{
	GetBase()->GetBase()->Invalidatelang();
}
Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetphoneticTranscription() const
{
	return GetBase()->GetBase()->GetphoneticTranscription();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ExistphoneticTranscription() const
{
	return GetBase()->GetBase()->ExistphoneticTranscription();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetphoneticTranscription(const Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetphoneticTranscription().");
	}
	GetBase()->GetBase()->SetphoneticTranscription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::InvalidatephoneticTranscription()
{
	GetBase()->GetBase()->InvalidatephoneticTranscription();
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetphoneticAlphabet() const
{
	return GetBase()->GetBase()->GetphoneticAlphabet();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ExistphoneticAlphabet() const
{
	return GetBase()->GetBase()->ExistphoneticAlphabet();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::SetphoneticAlphabet().");
	}
	GetBase()->GetBase()->SetphoneticAlphabet(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::InvalidatephoneticAlphabet()
{
	GetBase()->GetBase()->InvalidatephoneticAlphabet();
}
XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Getcharset() const
{
	return GetBase()->GetBase()->Getcharset();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Existcharset() const
{
	return GetBase()->GetBase()->Existcharset();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setcharset().");
	}
	GetBase()->GetBase()->Setcharset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Invalidatecharset()
{
	GetBase()->GetBase()->Invalidatecharset();
}
XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Getencoding() const
{
	return GetBase()->GetBase()->Getencoding();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Existencoding() const
{
	return GetBase()->GetBase()->Existencoding();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setencoding().");
	}
	GetBase()->GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Invalidateencoding()
{
	GetBase()->GetBase()->Invalidateencoding();
}
XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Getscript() const
{
	return GetBase()->GetBase()->Getscript();
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Existscript() const
{
	return GetBase()->GetBase()->Existscript();
}
void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setscript(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Setscript().");
	}
	GetBase()->GetBase()->Setscript(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Invalidatescript()
{
	GetBase()->GetBase()->Invalidatescript();
}

Dc1NodeEnum Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationPreferencesType_AssociatedData_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationPreferencesType_AssociatedData_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationPreferencesType_AssociatedData_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsTextualType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationPreferencesType_AssociatedData_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_AssociatedData_LocalType_ExtMethodImpl.h


