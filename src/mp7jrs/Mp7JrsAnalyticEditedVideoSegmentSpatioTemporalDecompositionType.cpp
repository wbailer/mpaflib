
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtImplInclude.h


#include "Mp7JrsSpatioTemporalSegmentDecompositionType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType.h" // Choice collection EditedMovingRegion
#include "Mp7JrsEditedMovingRegionType.h" // Choice collection element EditedMovingRegion
#include "Mp7JrsReferenceType.h" // Choice collection element EditedMovingRegionRef

#include <assert.h>
IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtPropInit.h

}

IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::~IMp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtPropCleanup.h

}

Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::~Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType()
{
	Cleanup();
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Init()
{
	// Init base
	m_Base = CreateSpatioTemporalSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType = Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtMyPropInit.h

}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType);
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AnalyticEditedVideoSegmentSpatioTemporalDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in IAnalyticEditedVideoSegmentSpatioTemporalDecompositionType
	const Dc1Ptr< Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSpatioTemporalSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType);
		this->SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSpatioTemporalSegmentDecompositionType > Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType() const
{
		return m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType;
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(const Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType().");
	}
	if (m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType);
		m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType) m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("EditedMovingRegion")) == 0)
	{
		// EditedMovingRegion is contained in itemtype AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType
		// in choice collection AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr coll = GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType; // FTT, check this
				SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetEditedMovingRegion()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr item = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EditedMovingRegionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsEditedMovingRegionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetEditedMovingRegion(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetEditedMovingRegion()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EditedMovingRegionRef")) == 0)
	{
		// EditedMovingRegionRef is contained in itemtype AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType
		// in choice collection AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr coll = GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType; // FTT, check this
				SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetEditedMovingRegionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr item = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetEditedMovingRegionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetEditedMovingRegionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AnalyticEditedVideoSegmentSpatioTemporalDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AnalyticEditedVideoSegmentSpatioTemporalDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AnalyticEditedVideoSegmentSpatioTemporalDecompositionType"));
	// Element serialization:
	if (m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType != Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSpatioTemporalSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:EditedMovingRegion
			Dc1Util::HasNodeName(parent, X("EditedMovingRegion"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:EditedMovingRegion")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:EditedMovingRegionRef
			Dc1Util::HasNodeName(parent, X("EditedMovingRegionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:EditedMovingRegionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr tmp = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("EditedMovingRegion"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("EditedMovingRegion")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("EditedMovingRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("EditedMovingRegionRef")) == 0)
	)
  {
	Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionPtr tmp = CreateAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_CollectionType; // FTT, check this
	this->SetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType_ExtMethodImpl.h


