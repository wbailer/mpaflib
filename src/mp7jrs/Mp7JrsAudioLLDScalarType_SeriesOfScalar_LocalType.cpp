
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtImplInclude.h


#include "Mp7JrsSeriesOfScalarType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsScalableSeriesType_Scaling_CollectionType.h"
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsScalableSeriesType_Scaling_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Scaling

#include <assert.h>
IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtPropInit.h

}

IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::~IMp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::~Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Init()
{
	// Init base
	m_Base = CreateSeriesOfScalarType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_hopSize = Mp7JrsmediaDurationPtr(); // Pattern
	m_hopSize_Default = CreatemediaDurationType; // Default pattern FTT, check this
	m_hopSize_Default->Parse(X("PT10N1000F"));
	m_hopSize_Exist = false;



// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_hopSize); // Pattern
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioLLDScalarType_SeriesOfScalar_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioLLDScalarType_SeriesOfScalar_LocalType
	const Dc1Ptr< Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSeriesOfScalarPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_hopSize); // Pattern
	if (tmp->ExisthopSize())
	{
		this->SethopSize(Dc1Factory::CloneObject(tmp->GethopSize()));
	}
	else
	{
		InvalidatehopSize();
	}
	}
}

Dc1NodePtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSeriesOfScalarType > Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsmediaDurationPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GethopSize() const
{
	if (this->ExisthopSize()) {
		return m_hopSize;
	} else {
		return m_hopSize_Default;
	}
}

bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::ExisthopSize() const
{
	return m_hopSize_Exist;
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SethopSize(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SethopSize().");
	}
	m_hopSize = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_hopSize) m_hopSize->SetParent(m_myself.getPointer());
	m_hopSize_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidatehopSize()
{
	m_hopSize_Exist = false;
}
Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetRaw() const
{
	return GetBase()->GetRaw();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidRaw() const
{
	return GetBase()->IsValidRaw();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetMin() const
{
	return GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidMin() const
{
	return GetBase()->IsValidMin();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetMax() const
{
	return GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidMax() const
{
	return GetBase()->IsValidMax();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetMean() const
{
	return GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidMean() const
{
	return GetBase()->IsValidMean();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetRandom() const
{
	return GetBase()->GetRandom();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidRandom() const
{
	return GetBase()->IsValidRandom();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetFirst() const
{
	return GetBase()->GetFirst();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidFirst() const
{
	return GetBase()->IsValidFirst();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetLast() const
{
	return GetBase()->GetLast();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidLast() const
{
	return GetBase()->IsValidLast();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetVariance() const
{
	return GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidVariance() const
{
	return GetBase()->IsValidVariance();
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetWeight() const
{
	return GetBase()->GetWeight();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidWeight() const
{
	return GetBase()->IsValidWeight();
}

float Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetLogBase() const
{
	return GetBase()->GetLogBase();
}

// Element is optional
bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::IsValidLogBase() const
{
	return GetBase()->IsValidLogBase();
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetRaw(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetRaw().");
	}
	GetBase()->SetRaw(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateRaw()
{
	GetBase()->InvalidateRaw();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMin(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMin().");
	}
	GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateMin()
{
	GetBase()->InvalidateMin();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMax(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMax().");
	}
	GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateMax()
{
	GetBase()->InvalidateMax();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMean(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetMean().");
	}
	GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateMean()
{
	GetBase()->InvalidateMean();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetRandom(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetRandom().");
	}
	GetBase()->SetRandom(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateRandom()
{
	GetBase()->InvalidateRandom();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetFirst(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetFirst().");
	}
	GetBase()->SetFirst(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateFirst()
{
	GetBase()->InvalidateFirst();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetLast(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetLast().");
	}
	GetBase()->SetLast(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateLast()
{
	GetBase()->InvalidateLast();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetVariance(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetVariance().");
	}
	GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateVariance()
{
	GetBase()->InvalidateVariance();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetWeight().");
	}
	GetBase()->SetWeight(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateWeight()
{
	GetBase()->InvalidateWeight();
}
void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetLogBase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetLogBase().");
	}
	GetBase()->SetLogBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::InvalidateLogBase()
{
	GetBase()->InvalidateLogBase();
}
unsigned Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GettotalNumOfSamples() const
{
	return GetBase()->GetBase()->GettotalNumOfSamples();
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SettotalNumOfSamples(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SettotalNumOfSamples().");
	}
	GetBase()->GetBase()->SettotalNumOfSamples(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableSeriesType_Scaling_CollectionPtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetScaling() const
{
	return GetBase()->GetBase()->GetScaling();
}

void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::SetScaling().");
	}
	GetBase()->GetBase()->SetScaling(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("hopSize")) == 0)
	{
		// hopSize is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioLLDScalarType_SeriesOfScalar_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioLLDScalarType_SeriesOfScalar_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioLLDScalarType_SeriesOfScalar_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_hopSize_Exist)
	{
	// Pattern
	if (m_hopSize != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_hopSize->ToText();
		element->setAttributeNS(X(""), X("hopSize"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("hopSize")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("hopSize")));
		this->SethopSize(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsSeriesOfScalarType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRaw() != Dc1NodePtr())
		result.Insert(GetRaw());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetRandom() != Dc1NodePtr())
		result.Insert(GetRandom());
	if (GetFirst() != Dc1NodePtr())
		result.Insert(GetFirst());
	if (GetLast() != Dc1NodePtr())
		result.Insert(GetLast());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetWeight() != Dc1NodePtr())
		result.Insert(GetWeight());
	if (GetScaling() != Dc1NodePtr())
		result.Insert(GetScaling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType_ExtMethodImpl.h


