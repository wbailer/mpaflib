
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtImplInclude.h


#include "Mp7JrsPersonalInterestsType_Activity_CollectionType.h"
#include "Mp7JrsPersonalInterestsType_Interest_CollectionType.h"
#include "Mp7JrsPersonalInterestsType_Favorite_CollectionType.h"
#include "Mp7JrsPersonalInterestsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSemanticBaseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Activity

#include <assert.h>
IMp7JrsPersonalInterestsType::IMp7JrsPersonalInterestsType()
{

// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtPropInit.h

}

IMp7JrsPersonalInterestsType::~IMp7JrsPersonalInterestsType()
{
// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtPropCleanup.h

}

Mp7JrsPersonalInterestsType::Mp7JrsPersonalInterestsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPersonalInterestsType::~Mp7JrsPersonalInterestsType()
{
	Cleanup();
}

void Mp7JrsPersonalInterestsType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Activity = Mp7JrsPersonalInterestsType_Activity_CollectionPtr(); // Collection
	m_Interest = Mp7JrsPersonalInterestsType_Interest_CollectionPtr(); // Collection
	m_Favorite = Mp7JrsPersonalInterestsType_Favorite_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtMyPropInit.h

}

void Mp7JrsPersonalInterestsType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Activity);
	// Dc1Factory::DeleteObject(m_Interest);
	// Dc1Factory::DeleteObject(m_Favorite);
}

void Mp7JrsPersonalInterestsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PersonalInterestsTypePtr, since we
	// might need GetBase(), which isn't defined in IPersonalInterestsType
	const Dc1Ptr< Mp7JrsPersonalInterestsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Activity);
		this->SetActivity(Dc1Factory::CloneObject(tmp->GetActivity()));
		// Dc1Factory::DeleteObject(m_Interest);
		this->SetInterest(Dc1Factory::CloneObject(tmp->GetInterest()));
		// Dc1Factory::DeleteObject(m_Favorite);
		this->SetFavorite(Dc1Factory::CloneObject(tmp->GetFavorite()));
}

Dc1NodePtr Mp7JrsPersonalInterestsType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPersonalInterestsType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsPersonalInterestsType_Activity_CollectionPtr Mp7JrsPersonalInterestsType::GetActivity() const
{
		return m_Activity;
}

Mp7JrsPersonalInterestsType_Interest_CollectionPtr Mp7JrsPersonalInterestsType::GetInterest() const
{
		return m_Interest;
}

Mp7JrsPersonalInterestsType_Favorite_CollectionPtr Mp7JrsPersonalInterestsType::GetFavorite() const
{
		return m_Favorite;
}

void Mp7JrsPersonalInterestsType::SetActivity(const Mp7JrsPersonalInterestsType_Activity_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonalInterestsType::SetActivity().");
	}
	if (m_Activity != item)
	{
		// Dc1Factory::DeleteObject(m_Activity);
		m_Activity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Activity) m_Activity->SetParent(m_myself.getPointer());
		if(m_Activity != Mp7JrsPersonalInterestsType_Activity_CollectionPtr())
		{
			m_Activity->SetContentName(XMLString::transcode("Activity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonalInterestsType::SetInterest(const Mp7JrsPersonalInterestsType_Interest_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonalInterestsType::SetInterest().");
	}
	if (m_Interest != item)
	{
		// Dc1Factory::DeleteObject(m_Interest);
		m_Interest = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Interest) m_Interest->SetParent(m_myself.getPointer());
		if(m_Interest != Mp7JrsPersonalInterestsType_Interest_CollectionPtr())
		{
			m_Interest->SetContentName(XMLString::transcode("Interest"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonalInterestsType::SetFavorite(const Mp7JrsPersonalInterestsType_Favorite_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonalInterestsType::SetFavorite().");
	}
	if (m_Favorite != item)
	{
		// Dc1Factory::DeleteObject(m_Favorite);
		m_Favorite = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Favorite) m_Favorite->SetParent(m_myself.getPointer());
		if(m_Favorite != Mp7JrsPersonalInterestsType_Favorite_CollectionPtr())
		{
			m_Favorite->SetContentName(XMLString::transcode("Favorite"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPersonalInterestsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Activity")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Activity is item of abstract type SemanticBaseType
		// in element collection PersonalInterestsType_Activity_CollectionType
		
		context->Found = true;
		Mp7JrsPersonalInterestsType_Activity_CollectionPtr coll = GetActivity();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonalInterestsType_Activity_CollectionType; // FTT, check this
				SetActivity(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Interest")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Interest is item of abstract type SemanticBaseType
		// in element collection PersonalInterestsType_Interest_CollectionType
		
		context->Found = true;
		Mp7JrsPersonalInterestsType_Interest_CollectionPtr coll = GetInterest();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonalInterestsType_Interest_CollectionType; // FTT, check this
				SetInterest(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Favorite")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Favorite is item of abstract type SemanticBaseType
		// in element collection PersonalInterestsType_Favorite_CollectionType
		
		context->Found = true;
		Mp7JrsPersonalInterestsType_Favorite_CollectionPtr coll = GetFavorite();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonalInterestsType_Favorite_CollectionType; // FTT, check this
				SetFavorite(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PersonalInterestsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PersonalInterestsType");
	}
	return result;
}

XMLCh * Mp7JrsPersonalInterestsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPersonalInterestsType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPersonalInterestsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PersonalInterestsType"));
	// Element serialization:
	if (m_Activity != Mp7JrsPersonalInterestsType_Activity_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Activity->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Interest != Mp7JrsPersonalInterestsType_Interest_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Interest->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Favorite != Mp7JrsPersonalInterestsType_Favorite_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Favorite->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPersonalInterestsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Activity"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Activity")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonalInterestsType_Activity_CollectionPtr tmp = CreatePersonalInterestsType_Activity_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetActivity(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Interest"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Interest")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonalInterestsType_Interest_CollectionPtr tmp = CreatePersonalInterestsType_Interest_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInterest(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Favorite"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Favorite")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonalInterestsType_Favorite_CollectionPtr tmp = CreatePersonalInterestsType_Favorite_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFavorite(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPersonalInterestsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Activity")) == 0))
  {
	Mp7JrsPersonalInterestsType_Activity_CollectionPtr tmp = CreatePersonalInterestsType_Activity_CollectionType; // FTT, check this
	this->SetActivity(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Interest")) == 0))
  {
	Mp7JrsPersonalInterestsType_Interest_CollectionPtr tmp = CreatePersonalInterestsType_Interest_CollectionType; // FTT, check this
	this->SetInterest(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Favorite")) == 0))
  {
	Mp7JrsPersonalInterestsType_Favorite_CollectionPtr tmp = CreatePersonalInterestsType_Favorite_CollectionType; // FTT, check this
	this->SetFavorite(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPersonalInterestsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetActivity() != Dc1NodePtr())
		result.Insert(GetActivity());
	if (GetInterest() != Dc1NodePtr())
		result.Insert(GetInterest());
	if (GetFavorite() != Dc1NodePtr())
		result.Insert(GetFavorite());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPersonalInterestsType_ExtMethodImpl.h


