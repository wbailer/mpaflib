
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtImplInclude.h


#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey

#include <assert.h>
IMp7JrsCompleteDescriptionType::IMp7JrsCompleteDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtPropInit.h

}

IMp7JrsCompleteDescriptionType::~IMp7JrsCompleteDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtPropCleanup.h

}

Mp7JrsCompleteDescriptionType::Mp7JrsCompleteDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCompleteDescriptionType::~Mp7JrsCompleteDescriptionType()
{
	Cleanup();
}

void Mp7JrsCompleteDescriptionType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_DescriptionMetadata = Mp7JrsDescriptionMetadataPtr(); // Class
	m_DescriptionMetadata_Exist = false;
	m_Relationships = Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr(); // Collection
	m_OrderingKey = Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtMyPropInit.h

}

void Mp7JrsCompleteDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_DescriptionMetadata);
	// Dc1Factory::DeleteObject(m_Relationships);
	// Dc1Factory::DeleteObject(m_OrderingKey);
}

void Mp7JrsCompleteDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CompleteDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in ICompleteDescriptionType
	const Dc1Ptr< Mp7JrsCompleteDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidDescriptionMetadata())
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		this->SetDescriptionMetadata(Dc1Factory::CloneObject(tmp->GetDescriptionMetadata()));
	}
	else
	{
		InvalidateDescriptionMetadata();
	}
		// Dc1Factory::DeleteObject(m_Relationships);
		this->SetRelationships(Dc1Factory::CloneObject(tmp->GetRelationships()));
		// Dc1Factory::DeleteObject(m_OrderingKey);
		this->SetOrderingKey(Dc1Factory::CloneObject(tmp->GetOrderingKey()));
}

Dc1NodePtr Mp7JrsCompleteDescriptionType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsCompleteDescriptionType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsDescriptionMetadataPtr Mp7JrsCompleteDescriptionType::GetDescriptionMetadata() const
{
		return m_DescriptionMetadata;
}

// Element is optional
bool Mp7JrsCompleteDescriptionType::IsValidDescriptionMetadata() const
{
	return m_DescriptionMetadata_Exist;
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsCompleteDescriptionType::GetRelationships() const
{
		return m_Relationships;
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsCompleteDescriptionType::GetOrderingKey() const
{
		return m_OrderingKey;
}

void Mp7JrsCompleteDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompleteDescriptionType::SetDescriptionMetadata().");
	}
	if (m_DescriptionMetadata != item || m_DescriptionMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		m_DescriptionMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionMetadata) m_DescriptionMetadata->SetParent(m_myself.getPointer());
		if (m_DescriptionMetadata && m_DescriptionMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DescriptionMetadata->UseTypeAttribute = true;
		}
		m_DescriptionMetadata_Exist = true;
		if(m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
		{
			m_DescriptionMetadata->SetContentName(XMLString::transcode("DescriptionMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompleteDescriptionType::InvalidateDescriptionMetadata()
{
	m_DescriptionMetadata_Exist = false;
}
void Mp7JrsCompleteDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompleteDescriptionType::SetRelationships().");
	}
	if (m_Relationships != item)
	{
		// Dc1Factory::DeleteObject(m_Relationships);
		m_Relationships = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relationships) m_Relationships->SetParent(m_myself.getPointer());
		if(m_Relationships != Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr())
		{
			m_Relationships->SetContentName(XMLString::transcode("Relationships"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompleteDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompleteDescriptionType::SetOrderingKey().");
	}
	if (m_OrderingKey != item)
	{
		// Dc1Factory::DeleteObject(m_OrderingKey);
		m_OrderingKey = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_OrderingKey) m_OrderingKey->SetParent(m_myself.getPointer());
		if(m_OrderingKey != Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr())
		{
			m_OrderingKey->SetContentName(XMLString::transcode("OrderingKey"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCompleteDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DescriptionMetadata")) == 0)
	{
		// DescriptionMetadata is simple element DescriptionMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptionMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptionMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptionMetadata(p, client);
					if((p = GetDescriptionMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relationships")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Relationships is item of type GraphType
		// in element collection CompleteDescriptionType_Relationships_CollectionType
		
		context->Found = true;
		Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr coll = GetRelationships();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompleteDescriptionType_Relationships_CollectionType; // FTT, check this
				SetRelationships(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("OrderingKey")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:OrderingKey is item of type OrderingKeyType
		// in element collection CompleteDescriptionType_OrderingKey_CollectionType
		
		context->Found = true;
		Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr coll = GetOrderingKey();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompleteDescriptionType_OrderingKey_CollectionType; // FTT, check this
				SetOrderingKey(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType")))) != empty)
			{
				// Is type allowed
				Mp7JrsOrderingKeyPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CompleteDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CompleteDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsCompleteDescriptionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsCompleteDescriptionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsCompleteDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CompleteDescriptionType"));
	// Element serialization:
	// Class
	
	if (m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DescriptionMetadata->Serialize(doc, element, &elem);
	}
	if (m_Relationships != Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relationships->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_OrderingKey != Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_OrderingKey->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCompleteDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionMetadata"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDescriptionMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relationships"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relationships")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr tmp = CreateCompleteDescriptionType_Relationships_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelationships(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("OrderingKey"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("OrderingKey")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr tmp = CreateCompleteDescriptionType_OrderingKey_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOrderingKey(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCompleteDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("DescriptionMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDescriptionMetadataType; // FTT, check this
	}
	this->SetDescriptionMetadata(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relationships")) == 0))
  {
	Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr tmp = CreateCompleteDescriptionType_Relationships_CollectionType; // FTT, check this
	this->SetRelationships(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("OrderingKey")) == 0))
  {
	Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr tmp = CreateCompleteDescriptionType_OrderingKey_CollectionType; // FTT, check this
	this->SetOrderingKey(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCompleteDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCompleteDescriptionType_ExtMethodImpl.h


