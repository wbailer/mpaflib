
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned32.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtPropInit.h

}

IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::~IMp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtPropCleanup.h

}

Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::~Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType()
{
	Cleanup();
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_StartMediaTimeOfSpatialRegion = Mp7Jrsunsigned32Ptr(); // Class with content 
	m_EndMediaTimeOfSpatialRegion = Mp7Jrsunsigned32Ptr(); // Class with content 


// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtMyPropInit.h

}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_StartMediaTimeOfSpatialRegion);
	// Dc1Factory::DeleteObject(m_EndMediaTimeOfSpatialRegion);
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType
	const Dc1Ptr< Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_StartMediaTimeOfSpatialRegion);
		this->SetStartMediaTimeOfSpatialRegion(Dc1Factory::CloneObject(tmp->GetStartMediaTimeOfSpatialRegion()));
		// Dc1Factory::DeleteObject(m_EndMediaTimeOfSpatialRegion);
		this->SetEndMediaTimeOfSpatialRegion(Dc1Factory::CloneObject(tmp->GetEndMediaTimeOfSpatialRegion()));
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetStartMediaTimeOfSpatialRegion() const
{
		return m_StartMediaTimeOfSpatialRegion;
}

Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetEndMediaTimeOfSpatialRegion() const
{
		return m_EndMediaTimeOfSpatialRegion;
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::SetStartMediaTimeOfSpatialRegion(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::SetStartMediaTimeOfSpatialRegion().");
	}
	if (m_StartMediaTimeOfSpatialRegion != item)
	{
		// Dc1Factory::DeleteObject(m_StartMediaTimeOfSpatialRegion);
		m_StartMediaTimeOfSpatialRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StartMediaTimeOfSpatialRegion) m_StartMediaTimeOfSpatialRegion->SetParent(m_myself.getPointer());
		if (m_StartMediaTimeOfSpatialRegion && m_StartMediaTimeOfSpatialRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StartMediaTimeOfSpatialRegion->UseTypeAttribute = true;
		}
		if(m_StartMediaTimeOfSpatialRegion != Mp7Jrsunsigned32Ptr())
		{
			m_StartMediaTimeOfSpatialRegion->SetContentName(XMLString::transcode("StartMediaTimeOfSpatialRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::SetEndMediaTimeOfSpatialRegion(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::SetEndMediaTimeOfSpatialRegion().");
	}
	if (m_EndMediaTimeOfSpatialRegion != item)
	{
		// Dc1Factory::DeleteObject(m_EndMediaTimeOfSpatialRegion);
		m_EndMediaTimeOfSpatialRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EndMediaTimeOfSpatialRegion) m_EndMediaTimeOfSpatialRegion->SetParent(m_myself.getPointer());
		if (m_EndMediaTimeOfSpatialRegion && m_EndMediaTimeOfSpatialRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_EndMediaTimeOfSpatialRegion->UseTypeAttribute = true;
		}
		if(m_EndMediaTimeOfSpatialRegion != Mp7Jrsunsigned32Ptr())
		{
			m_EndMediaTimeOfSpatialRegion->SetContentName(XMLString::transcode("EndMediaTimeOfSpatialRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("StartMediaTimeOfSpatialRegion")) == 0)
	{
		// StartMediaTimeOfSpatialRegion is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStartMediaTimeOfSpatialRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStartMediaTimeOfSpatialRegion(p, client);
					if((p = GetStartMediaTimeOfSpatialRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EndMediaTimeOfSpatialRegion")) == 0)
	{
		// EndMediaTimeOfSpatialRegion is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEndMediaTimeOfSpatialRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEndMediaTimeOfSpatialRegion(p, client);
					if((p = GetEndMediaTimeOfSpatialRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_StartMediaTimeOfSpatialRegion != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StartMediaTimeOfSpatialRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StartMediaTimeOfSpatialRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StartMediaTimeOfSpatialRegion->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_EndMediaTimeOfSpatialRegion != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_EndMediaTimeOfSpatialRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("EndMediaTimeOfSpatialRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_EndMediaTimeOfSpatialRegion->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StartMediaTimeOfSpatialRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StartMediaTimeOfSpatialRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStartMediaTimeOfSpatialRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("EndMediaTimeOfSpatialRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("EndMediaTimeOfSpatialRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetEndMediaTimeOfSpatialRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("StartMediaTimeOfSpatialRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetStartMediaTimeOfSpatialRegion(child);
  }
  if (XMLString::compareString(elementname, X("EndMediaTimeOfSpatialRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetEndMediaTimeOfSpatialRegion(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStartMediaTimeOfSpatialRegion() != Dc1NodePtr())
		result.Insert(GetStartMediaTimeOfSpatialRegion());
	if (GetEndMediaTimeOfSpatialRegion() != Dc1NodePtr())
		result.Insert(GetEndMediaTimeOfSpatialRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_MediaTimeOfSpatialRegion_LocalType_ExtMethodImpl.h


