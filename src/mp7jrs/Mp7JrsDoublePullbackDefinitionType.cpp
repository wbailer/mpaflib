
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtImplInclude.h


#include "Mp7JrsGraphicalRuleDefinitionType.h"
#include "Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionType.h"
#include "Mp7JrsGraphType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDoublePullbackDefinitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMorphismGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MorphismGraph

#include <assert.h>
IMp7JrsDoublePullbackDefinitionType::IMp7JrsDoublePullbackDefinitionType()
{

// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtPropInit.h

}

IMp7JrsDoublePullbackDefinitionType::~IMp7JrsDoublePullbackDefinitionType()
{
// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtPropCleanup.h

}

Mp7JrsDoublePullbackDefinitionType::Mp7JrsDoublePullbackDefinitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDoublePullbackDefinitionType::~Mp7JrsDoublePullbackDefinitionType()
{
	Cleanup();
}

void Mp7JrsDoublePullbackDefinitionType::Init()
{
	// Init base
	m_Base = CreateGraphicalRuleDefinitionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_RuleGraph = Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr(); // Collection
	m_AlphabetGraph = Mp7JrsGraphPtr(); // Class
	m_AlphabetGraphRef = Mp7JrsControlledTermUsePtr(); // Class
	m_MorphismGraph = Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtMyPropInit.h

}

void Mp7JrsDoublePullbackDefinitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_RuleGraph);
	// Dc1Factory::DeleteObject(m_AlphabetGraph);
	// Dc1Factory::DeleteObject(m_AlphabetGraphRef);
	// Dc1Factory::DeleteObject(m_MorphismGraph);
}

void Mp7JrsDoublePullbackDefinitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DoublePullbackDefinitionTypePtr, since we
	// might need GetBase(), which isn't defined in IDoublePullbackDefinitionType
	const Dc1Ptr< Mp7JrsDoublePullbackDefinitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsGraphicalRuleDefinitionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDoublePullbackDefinitionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_RuleGraph);
		this->SetRuleGraph(Dc1Factory::CloneObject(tmp->GetRuleGraph()));
		// Dc1Factory::DeleteObject(m_AlphabetGraph);
		this->SetAlphabetGraph(Dc1Factory::CloneObject(tmp->GetAlphabetGraph()));
		// Dc1Factory::DeleteObject(m_AlphabetGraphRef);
		this->SetAlphabetGraphRef(Dc1Factory::CloneObject(tmp->GetAlphabetGraphRef()));
		// Dc1Factory::DeleteObject(m_MorphismGraph);
		this->SetMorphismGraph(Dc1Factory::CloneObject(tmp->GetMorphismGraph()));
}

Dc1NodePtr Mp7JrsDoublePullbackDefinitionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDoublePullbackDefinitionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsGraphicalRuleDefinitionType > Mp7JrsDoublePullbackDefinitionType::GetBase() const
{
	return m_Base;
}

Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr Mp7JrsDoublePullbackDefinitionType::GetRuleGraph() const
{
		return m_RuleGraph;
}

Mp7JrsGraphPtr Mp7JrsDoublePullbackDefinitionType::GetAlphabetGraph() const
{
		return m_AlphabetGraph;
}

Mp7JrsControlledTermUsePtr Mp7JrsDoublePullbackDefinitionType::GetAlphabetGraphRef() const
{
		return m_AlphabetGraphRef;
}

Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr Mp7JrsDoublePullbackDefinitionType::GetMorphismGraph() const
{
		return m_MorphismGraph;
}

void Mp7JrsDoublePullbackDefinitionType::SetRuleGraph(const Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetRuleGraph().");
	}
	if (m_RuleGraph != item)
	{
		// Dc1Factory::DeleteObject(m_RuleGraph);
		m_RuleGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RuleGraph) m_RuleGraph->SetParent(m_myself.getPointer());
		if(m_RuleGraph != Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr())
		{
			m_RuleGraph->SetContentName(XMLString::transcode("RuleGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsDoublePullbackDefinitionType::SetAlphabetGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetAlphabetGraph().");
	}
	if (m_AlphabetGraph != item)
	{
		// Dc1Factory::DeleteObject(m_AlphabetGraph);
		m_AlphabetGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AlphabetGraph) m_AlphabetGraph->SetParent(m_myself.getPointer());
		if (m_AlphabetGraph && m_AlphabetGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AlphabetGraph->UseTypeAttribute = true;
		}
		if(m_AlphabetGraph != Mp7JrsGraphPtr())
		{
			m_AlphabetGraph->SetContentName(XMLString::transcode("AlphabetGraph"));
		}
	// Dc1Factory::DeleteObject(m_AlphabetGraphRef);
	m_AlphabetGraphRef = Mp7JrsControlledTermUsePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsDoublePullbackDefinitionType::SetAlphabetGraphRef(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetAlphabetGraphRef().");
	}
	if (m_AlphabetGraphRef != item)
	{
		// Dc1Factory::DeleteObject(m_AlphabetGraphRef);
		m_AlphabetGraphRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AlphabetGraphRef) m_AlphabetGraphRef->SetParent(m_myself.getPointer());
		if (m_AlphabetGraphRef && m_AlphabetGraphRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AlphabetGraphRef->UseTypeAttribute = true;
		}
		if(m_AlphabetGraphRef != Mp7JrsControlledTermUsePtr())
		{
			m_AlphabetGraphRef->SetContentName(XMLString::transcode("AlphabetGraphRef"));
		}
	// Dc1Factory::DeleteObject(m_AlphabetGraph);
	m_AlphabetGraph = Mp7JrsGraphPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::SetMorphismGraph(const Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetMorphismGraph().");
	}
	if (m_MorphismGraph != item)
	{
		// Dc1Factory::DeleteObject(m_MorphismGraph);
		m_MorphismGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MorphismGraph) m_MorphismGraph->SetParent(m_myself.getPointer());
		if(m_MorphismGraph != Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr())
		{
			m_MorphismGraph->SetContentName(XMLString::transcode("MorphismGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsGraphPtr Mp7JrsDoublePullbackDefinitionType::GetTemplateGraph() const
{
	return GetBase()->GetTemplateGraph();
}

// Element is optional
bool Mp7JrsDoublePullbackDefinitionType::IsValidTemplateGraph() const
{
	return GetBase()->IsValidTemplateGraph();
}

void Mp7JrsDoublePullbackDefinitionType::SetTemplateGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetTemplateGraph().");
	}
	GetBase()->SetTemplateGraph(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::InvalidateTemplateGraph()
{
	GetBase()->InvalidateTemplateGraph();
}
XMLCh * Mp7JrsDoublePullbackDefinitionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsDoublePullbackDefinitionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsDoublePullbackDefinitionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsDoublePullbackDefinitionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsDoublePullbackDefinitionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsDoublePullbackDefinitionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsDoublePullbackDefinitionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsDoublePullbackDefinitionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsDoublePullbackDefinitionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsDoublePullbackDefinitionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsDoublePullbackDefinitionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsDoublePullbackDefinitionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsDoublePullbackDefinitionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsDoublePullbackDefinitionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsDoublePullbackDefinitionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePullbackDefinitionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDoublePullbackDefinitionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsDoublePullbackDefinitionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePullbackDefinitionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDoublePullbackDefinitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("RuleGraph")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:RuleGraph is item of type GraphType
		// in element collection DoublePullbackDefinitionType_RuleGraph_CollectionType
		
		context->Found = true;
		Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr coll = GetRuleGraph();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDoublePullbackDefinitionType_RuleGraph_CollectionType; // FTT, check this
				SetRuleGraph(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 2))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AlphabetGraph")) == 0)
	{
		// AlphabetGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAlphabetGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAlphabetGraph(p, client);
					if((p = GetAlphabetGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AlphabetGraphRef")) == 0)
	{
		// AlphabetGraphRef is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAlphabetGraphRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAlphabetGraphRef(p, client);
					if((p = GetAlphabetGraphRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MorphismGraph")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MorphismGraph is item of type MorphismGraphType
		// in element collection DoublePullbackDefinitionType_MorphismGraph_CollectionType
		
		context->Found = true;
		Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr coll = GetMorphismGraph();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDoublePullbackDefinitionType_MorphismGraph_CollectionType; // FTT, check this
				SetMorphismGraph(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 2))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MorphismGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMorphismGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DoublePullbackDefinitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DoublePullbackDefinitionType");
	}
	return result;
}

XMLCh * Mp7JrsDoublePullbackDefinitionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDoublePullbackDefinitionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDoublePullbackDefinitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DoublePullbackDefinitionType"));
	// Element serialization:
	if (m_RuleGraph != Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RuleGraph->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_AlphabetGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AlphabetGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AlphabetGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AlphabetGraph->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AlphabetGraphRef != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AlphabetGraphRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AlphabetGraphRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AlphabetGraphRef->Serialize(doc, element, &elem);
	}
	if (m_MorphismGraph != Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MorphismGraph->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDoublePullbackDefinitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsGraphicalRuleDefinitionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RuleGraph"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RuleGraph")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr tmp = CreateDoublePullbackDefinitionType_RuleGraph_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRuleGraph(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AlphabetGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AlphabetGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAlphabetGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AlphabetGraphRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AlphabetGraphRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAlphabetGraphRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MorphismGraph"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MorphismGraph")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr tmp = CreateDoublePullbackDefinitionType_MorphismGraph_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMorphismGraph(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDoublePullbackDefinitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RuleGraph")) == 0))
  {
	Mp7JrsDoublePullbackDefinitionType_RuleGraph_CollectionPtr tmp = CreateDoublePullbackDefinitionType_RuleGraph_CollectionType; // FTT, check this
	this->SetRuleGraph(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("AlphabetGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetAlphabetGraph(child);
  }
  if (XMLString::compareString(elementname, X("AlphabetGraphRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetAlphabetGraphRef(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MorphismGraph")) == 0))
  {
	Mp7JrsDoublePullbackDefinitionType_MorphismGraph_CollectionPtr tmp = CreateDoublePullbackDefinitionType_MorphismGraph_CollectionType; // FTT, check this
	this->SetMorphismGraph(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDoublePullbackDefinitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRuleGraph() != Dc1NodePtr())
		result.Insert(GetRuleGraph());
	if (GetMorphismGraph() != Dc1NodePtr())
		result.Insert(GetMorphismGraph());
	if (GetTemplateGraph() != Dc1NodePtr())
		result.Insert(GetTemplateGraph());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetAlphabetGraph() != Dc1NodePtr())
		result.Insert(GetAlphabetGraph());
	if (GetAlphabetGraphRef() != Dc1NodePtr())
		result.Insert(GetAlphabetGraphRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDoublePullbackDefinitionType_ExtMethodImpl.h


