
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionType.h"
#include "Mp7JrsSourcePreferencesType_DisseminationSource_CollectionType.h"
#include "Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionType.h"
#include "Mp7JrsSourcePreferencesType_DisseminationDate_CollectionType.h"
#include "Mp7JrsSourcePreferencesType_Disseminator_CollectionType.h"
#include "Mp7JrsSourcePreferencesType_MediaFormat_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSourcePreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSourcePreferencesType_DisseminationFormat_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DisseminationFormat
#include "Mp7JrsSourcePreferencesType_DisseminationSource_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DisseminationSource
#include "Mp7JrsSourcePreferencesType_DisseminationLocation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DisseminationLocation
#include "Mp7JrsSourcePreferencesType_DisseminationDate_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DisseminationDate
#include "Mp7JrsSourcePreferencesType_Disseminator_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Disseminator
#include "Mp7JrsSourcePreferencesType_MediaFormat_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaFormat

#include <assert.h>
IMp7JrsSourcePreferencesType::IMp7JrsSourcePreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtPropInit.h

}

IMp7JrsSourcePreferencesType::~IMp7JrsSourcePreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtPropCleanup.h

}

Mp7JrsSourcePreferencesType::Mp7JrsSourcePreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSourcePreferencesType::~Mp7JrsSourcePreferencesType()
{
	Cleanup();
}

void Mp7JrsSourcePreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_noRepeat = false; // Value
	m_noRepeat_Exist = false;
	m_noEncryption = false; // Value
	m_noEncryption_Exist = false;
	m_noPayPerUse = false; // Value
	m_noPayPerUse_Exist = false;
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_DisseminationFormat = Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr(); // Collection
	m_DisseminationSource = Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr(); // Collection
	m_DisseminationLocation = Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr(); // Collection
	m_DisseminationDate = Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr(); // Collection
	m_Disseminator = Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr(); // Collection
	m_MediaFormat = Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMyPropInit.h

}

void Mp7JrsSourcePreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_DisseminationFormat);
	// Dc1Factory::DeleteObject(m_DisseminationSource);
	// Dc1Factory::DeleteObject(m_DisseminationLocation);
	// Dc1Factory::DeleteObject(m_DisseminationDate);
	// Dc1Factory::DeleteObject(m_Disseminator);
	// Dc1Factory::DeleteObject(m_MediaFormat);
}

void Mp7JrsSourcePreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SourcePreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in ISourcePreferencesType
	const Dc1Ptr< Mp7JrsSourcePreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSourcePreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistnoRepeat())
	{
		this->SetnoRepeat(tmp->GetnoRepeat());
	}
	else
	{
		InvalidatenoRepeat();
	}
	}
	{
	if (tmp->ExistnoEncryption())
	{
		this->SetnoEncryption(tmp->GetnoEncryption());
	}
	else
	{
		InvalidatenoEncryption();
	}
	}
	{
	if (tmp->ExistnoPayPerUse())
	{
		this->SetnoPayPerUse(tmp->GetnoPayPerUse());
	}
	else
	{
		InvalidatenoPayPerUse();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_DisseminationFormat);
		this->SetDisseminationFormat(Dc1Factory::CloneObject(tmp->GetDisseminationFormat()));
		// Dc1Factory::DeleteObject(m_DisseminationSource);
		this->SetDisseminationSource(Dc1Factory::CloneObject(tmp->GetDisseminationSource()));
		// Dc1Factory::DeleteObject(m_DisseminationLocation);
		this->SetDisseminationLocation(Dc1Factory::CloneObject(tmp->GetDisseminationLocation()));
		// Dc1Factory::DeleteObject(m_DisseminationDate);
		this->SetDisseminationDate(Dc1Factory::CloneObject(tmp->GetDisseminationDate()));
		// Dc1Factory::DeleteObject(m_Disseminator);
		this->SetDisseminator(Dc1Factory::CloneObject(tmp->GetDisseminator()));
		// Dc1Factory::DeleteObject(m_MediaFormat);
		this->SetMediaFormat(Dc1Factory::CloneObject(tmp->GetMediaFormat()));
}

Dc1NodePtr Mp7JrsSourcePreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSourcePreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSourcePreferencesType::GetBase() const
{
	return m_Base;
}

bool Mp7JrsSourcePreferencesType::GetnoRepeat() const
{
	return m_noRepeat;
}

bool Mp7JrsSourcePreferencesType::ExistnoRepeat() const
{
	return m_noRepeat_Exist;
}
void Mp7JrsSourcePreferencesType::SetnoRepeat(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetnoRepeat().");
	}
	m_noRepeat = item;
	m_noRepeat_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatenoRepeat()
{
	m_noRepeat_Exist = false;
}
bool Mp7JrsSourcePreferencesType::GetnoEncryption() const
{
	return m_noEncryption;
}

bool Mp7JrsSourcePreferencesType::ExistnoEncryption() const
{
	return m_noEncryption_Exist;
}
void Mp7JrsSourcePreferencesType::SetnoEncryption(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetnoEncryption().");
	}
	m_noEncryption = item;
	m_noEncryption_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatenoEncryption()
{
	m_noEncryption_Exist = false;
}
bool Mp7JrsSourcePreferencesType::GetnoPayPerUse() const
{
	return m_noPayPerUse;
}

bool Mp7JrsSourcePreferencesType::ExistnoPayPerUse() const
{
	return m_noPayPerUse_Exist;
}
void Mp7JrsSourcePreferencesType::SetnoPayPerUse(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetnoPayPerUse().");
	}
	m_noPayPerUse = item;
	m_noPayPerUse_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatenoPayPerUse()
{
	m_noPayPerUse_Exist = false;
}
Mp7JrspreferenceValuePtr Mp7JrsSourcePreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsSourcePreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsSourcePreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr Mp7JrsSourcePreferencesType::GetDisseminationFormat() const
{
		return m_DisseminationFormat;
}

Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr Mp7JrsSourcePreferencesType::GetDisseminationSource() const
{
		return m_DisseminationSource;
}

Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr Mp7JrsSourcePreferencesType::GetDisseminationLocation() const
{
		return m_DisseminationLocation;
}

Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr Mp7JrsSourcePreferencesType::GetDisseminationDate() const
{
		return m_DisseminationDate;
}

Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr Mp7JrsSourcePreferencesType::GetDisseminator() const
{
		return m_Disseminator;
}

Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr Mp7JrsSourcePreferencesType::GetMediaFormat() const
{
		return m_MediaFormat;
}

void Mp7JrsSourcePreferencesType::SetDisseminationFormat(const Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetDisseminationFormat().");
	}
	if (m_DisseminationFormat != item)
	{
		// Dc1Factory::DeleteObject(m_DisseminationFormat);
		m_DisseminationFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DisseminationFormat) m_DisseminationFormat->SetParent(m_myself.getPointer());
		if(m_DisseminationFormat != Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr())
		{
			m_DisseminationFormat->SetContentName(XMLString::transcode("DisseminationFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::SetDisseminationSource(const Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetDisseminationSource().");
	}
	if (m_DisseminationSource != item)
	{
		// Dc1Factory::DeleteObject(m_DisseminationSource);
		m_DisseminationSource = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DisseminationSource) m_DisseminationSource->SetParent(m_myself.getPointer());
		if(m_DisseminationSource != Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr())
		{
			m_DisseminationSource->SetContentName(XMLString::transcode("DisseminationSource"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::SetDisseminationLocation(const Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetDisseminationLocation().");
	}
	if (m_DisseminationLocation != item)
	{
		// Dc1Factory::DeleteObject(m_DisseminationLocation);
		m_DisseminationLocation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DisseminationLocation) m_DisseminationLocation->SetParent(m_myself.getPointer());
		if(m_DisseminationLocation != Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr())
		{
			m_DisseminationLocation->SetContentName(XMLString::transcode("DisseminationLocation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::SetDisseminationDate(const Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetDisseminationDate().");
	}
	if (m_DisseminationDate != item)
	{
		// Dc1Factory::DeleteObject(m_DisseminationDate);
		m_DisseminationDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DisseminationDate) m_DisseminationDate->SetParent(m_myself.getPointer());
		if(m_DisseminationDate != Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr())
		{
			m_DisseminationDate->SetContentName(XMLString::transcode("DisseminationDate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::SetDisseminator(const Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetDisseminator().");
	}
	if (m_Disseminator != item)
	{
		// Dc1Factory::DeleteObject(m_Disseminator);
		m_Disseminator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Disseminator) m_Disseminator->SetParent(m_myself.getPointer());
		if(m_Disseminator != Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr())
		{
			m_Disseminator->SetContentName(XMLString::transcode("Disseminator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::SetMediaFormat(const Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetMediaFormat().");
	}
	if (m_MediaFormat != item)
	{
		// Dc1Factory::DeleteObject(m_MediaFormat);
		m_MediaFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaFormat) m_MediaFormat->SetParent(m_myself.getPointer());
		if(m_MediaFormat != Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr())
		{
			m_MediaFormat->SetContentName(XMLString::transcode("MediaFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSourcePreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSourcePreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSourcePreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSourcePreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSourcePreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSourcePreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSourcePreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSourcePreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSourcePreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSourcePreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSourcePreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSourcePreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSourcePreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSourcePreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSourcePreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSourcePreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSourcePreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSourcePreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("noRepeat")) == 0)
	{
		// noRepeat is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("noEncryption")) == 0)
	{
		// noEncryption is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("noPayPerUse")) == 0)
	{
		// noPayPerUse is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DisseminationFormat")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DisseminationFormat is item of type SourcePreferencesType_DisseminationFormat_LocalType
		// in element collection SourcePreferencesType_DisseminationFormat_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr coll = GetDisseminationFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_DisseminationFormat_CollectionType; // FTT, check this
				SetDisseminationFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationFormat_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_DisseminationFormat_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DisseminationSource")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DisseminationSource is item of type SourcePreferencesType_DisseminationSource_LocalType
		// in element collection SourcePreferencesType_DisseminationSource_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr coll = GetDisseminationSource();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_DisseminationSource_CollectionType; // FTT, check this
				SetDisseminationSource(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationSource_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_DisseminationSource_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DisseminationLocation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DisseminationLocation is item of type SourcePreferencesType_DisseminationLocation_LocalType
		// in element collection SourcePreferencesType_DisseminationLocation_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr coll = GetDisseminationLocation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_DisseminationLocation_CollectionType; // FTT, check this
				SetDisseminationLocation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationLocation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_DisseminationLocation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DisseminationDate")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DisseminationDate is item of type SourcePreferencesType_DisseminationDate_LocalType
		// in element collection SourcePreferencesType_DisseminationDate_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr coll = GetDisseminationDate();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_DisseminationDate_CollectionType; // FTT, check this
				SetDisseminationDate(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_DisseminationDate_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_DisseminationDate_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Disseminator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Disseminator is item of type SourcePreferencesType_Disseminator_LocalType
		// in element collection SourcePreferencesType_Disseminator_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr coll = GetDisseminator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_Disseminator_CollectionType; // FTT, check this
				SetDisseminator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_Disseminator_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_Disseminator_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaFormat")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaFormat is item of type SourcePreferencesType_MediaFormat_LocalType
		// in element collection SourcePreferencesType_MediaFormat_CollectionType
		
		context->Found = true;
		Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr coll = GetMediaFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSourcePreferencesType_MediaFormat_CollectionType; // FTT, check this
				SetMediaFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType_MediaFormat_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesType_MediaFormat_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SourcePreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SourcePreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsSourcePreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSourcePreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSourcePreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SourcePreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_noRepeat_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_noRepeat);
		element->setAttributeNS(X(""), X("noRepeat"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_noEncryption_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_noEncryption);
		element->setAttributeNS(X(""), X("noEncryption"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_noPayPerUse_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_noPayPerUse);
		element->setAttributeNS(X(""), X("noPayPerUse"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_DisseminationFormat != Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DisseminationFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DisseminationSource != Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DisseminationSource->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DisseminationLocation != Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DisseminationLocation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DisseminationDate != Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DisseminationDate->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Disseminator != Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Disseminator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaFormat != Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSourcePreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("noRepeat")))
	{
		// deserialize value type
		this->SetnoRepeat(Dc1Convert::TextToBool(parent->getAttribute(X("noRepeat"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("noEncryption")))
	{
		// deserialize value type
		this->SetnoEncryption(Dc1Convert::TextToBool(parent->getAttribute(X("noEncryption"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("noPayPerUse")))
	{
		// deserialize value type
		this->SetnoPayPerUse(Dc1Convert::TextToBool(parent->getAttribute(X("noPayPerUse"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DisseminationFormat"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DisseminationFormat")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDisseminationFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DisseminationSource"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DisseminationSource")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationSource_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDisseminationSource(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DisseminationLocation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DisseminationLocation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationLocation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDisseminationLocation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DisseminationDate"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DisseminationDate")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationDate_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDisseminationDate(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Disseminator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Disseminator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr tmp = CreateSourcePreferencesType_Disseminator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDisseminator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaFormat"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaFormat")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr tmp = CreateSourcePreferencesType_MediaFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSourcePreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DisseminationFormat")) == 0))
  {
	Mp7JrsSourcePreferencesType_DisseminationFormat_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationFormat_CollectionType; // FTT, check this
	this->SetDisseminationFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DisseminationSource")) == 0))
  {
	Mp7JrsSourcePreferencesType_DisseminationSource_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationSource_CollectionType; // FTT, check this
	this->SetDisseminationSource(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DisseminationLocation")) == 0))
  {
	Mp7JrsSourcePreferencesType_DisseminationLocation_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationLocation_CollectionType; // FTT, check this
	this->SetDisseminationLocation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DisseminationDate")) == 0))
  {
	Mp7JrsSourcePreferencesType_DisseminationDate_CollectionPtr tmp = CreateSourcePreferencesType_DisseminationDate_CollectionType; // FTT, check this
	this->SetDisseminationDate(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Disseminator")) == 0))
  {
	Mp7JrsSourcePreferencesType_Disseminator_CollectionPtr tmp = CreateSourcePreferencesType_Disseminator_CollectionType; // FTT, check this
	this->SetDisseminator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaFormat")) == 0))
  {
	Mp7JrsSourcePreferencesType_MediaFormat_CollectionPtr tmp = CreateSourcePreferencesType_MediaFormat_CollectionType; // FTT, check this
	this->SetMediaFormat(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSourcePreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDisseminationFormat() != Dc1NodePtr())
		result.Insert(GetDisseminationFormat());
	if (GetDisseminationSource() != Dc1NodePtr())
		result.Insert(GetDisseminationSource());
	if (GetDisseminationLocation() != Dc1NodePtr())
		result.Insert(GetDisseminationLocation());
	if (GetDisseminationDate() != Dc1NodePtr())
		result.Insert(GetDisseminationDate());
	if (GetDisseminator() != Dc1NodePtr())
		result.Insert(GetDisseminator());
	if (GetMediaFormat() != Dc1NodePtr())
		result.Insert(GetMediaFormat());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_ExtMethodImpl.h


