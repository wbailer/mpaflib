
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtImplInclude.h


#include "Mp7JrsNonDependencyStructurePhraseType_CollectionType.h"
#include "Mp7JrsNonDependencyStructurePhraseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsNonDependencyStructurePhraseType_LocalType.h" // Choice collection Quotation
#include "Mp7JrsDependencyStructurePhraseType.h" // Choice collection element Quotation
#include "Mp7JrsWordFormType.h" // Choice collection element Word
#include "Mp7JrsNonDependencyStructurePhraseType.h" // Choice collection element NonDependencyPhrase

#include <assert.h>
IMp7JrsNonDependencyStructurePhraseType::IMp7JrsNonDependencyStructurePhraseType()
{

// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtPropInit.h

}

IMp7JrsNonDependencyStructurePhraseType::~IMp7JrsNonDependencyStructurePhraseType()
{
// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtPropCleanup.h

}

Mp7JrsNonDependencyStructurePhraseType::Mp7JrsNonDependencyStructurePhraseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsNonDependencyStructurePhraseType::~Mp7JrsNonDependencyStructurePhraseType()
{
	Cleanup();
}

void Mp7JrsNonDependencyStructurePhraseType::Init()
{

	// Init attributes
	m_id = NULL; // String
	m_id_Exist = false;
	m_equal = NULL; // String
	m_equal_Exist = false;
	m_synthesis = Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::UninitializedEnumeration;
	m_synthesis_Default = Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::unspecified; // Default enumeration
	m_synthesis_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_NonDependencyStructurePhraseType_LocalType = Mp7JrsNonDependencyStructurePhraseType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtMyPropInit.h

}

void Mp7JrsNonDependencyStructurePhraseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtMyPropCleanup.h


	XMLString::release(&m_id); // String
	XMLString::release(&m_equal); // String
	// Dc1Factory::DeleteObject(m_NonDependencyStructurePhraseType_LocalType);
}

void Mp7JrsNonDependencyStructurePhraseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use NonDependencyStructurePhraseTypePtr, since we
	// might need GetBase(), which isn't defined in INonDependencyStructurePhraseType
	const Dc1Ptr< Mp7JrsNonDependencyStructurePhraseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_id); // String
	if (tmp->Existid())
	{
		this->Setid(XMLString::replicate(tmp->Getid()));
	}
	else
	{
		Invalidateid();
	}
	}
	{
	XMLString::release(&m_equal); // String
	if (tmp->Existequal())
	{
		this->Setequal(XMLString::replicate(tmp->Getequal()));
	}
	else
	{
		Invalidateequal();
	}
	}
	{
	if (tmp->Existsynthesis())
	{
		this->Setsynthesis(tmp->Getsynthesis());
	}
	else
	{
		Invalidatesynthesis();
	}
	}
		// Dc1Factory::DeleteObject(m_NonDependencyStructurePhraseType_LocalType);
		this->SetNonDependencyStructurePhraseType_LocalType(Dc1Factory::CloneObject(tmp->GetNonDependencyStructurePhraseType_LocalType()));
}

Dc1NodePtr Mp7JrsNonDependencyStructurePhraseType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsNonDependencyStructurePhraseType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsNonDependencyStructurePhraseType::Getid() const
{
	return m_id;
}

bool Mp7JrsNonDependencyStructurePhraseType::Existid() const
{
	return m_id_Exist;
}
void Mp7JrsNonDependencyStructurePhraseType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonDependencyStructurePhraseType::Setid().");
	}
	m_id = item;
	m_id_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNonDependencyStructurePhraseType::Invalidateid()
{
	m_id_Exist = false;
}
XMLCh * Mp7JrsNonDependencyStructurePhraseType::Getequal() const
{
	return m_equal;
}

bool Mp7JrsNonDependencyStructurePhraseType::Existequal() const
{
	return m_equal_Exist;
}
void Mp7JrsNonDependencyStructurePhraseType::Setequal(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonDependencyStructurePhraseType::Setequal().");
	}
	m_equal = item;
	m_equal_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNonDependencyStructurePhraseType::Invalidateequal()
{
	m_equal_Exist = false;
}
Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::Enumeration Mp7JrsNonDependencyStructurePhraseType::Getsynthesis() const
{
	if (this->Existsynthesis()) {
		return m_synthesis;
	} else {
		return m_synthesis_Default;
	}
}

bool Mp7JrsNonDependencyStructurePhraseType::Existsynthesis() const
{
	return m_synthesis_Exist;
}
void Mp7JrsNonDependencyStructurePhraseType::Setsynthesis(Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonDependencyStructurePhraseType::Setsynthesis().");
	}
	m_synthesis = item;
	m_synthesis_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNonDependencyStructurePhraseType::Invalidatesynthesis()
{
	m_synthesis_Exist = false;
}
Mp7JrsNonDependencyStructurePhraseType_CollectionPtr Mp7JrsNonDependencyStructurePhraseType::GetNonDependencyStructurePhraseType_LocalType() const
{
		return m_NonDependencyStructurePhraseType_LocalType;
}

void Mp7JrsNonDependencyStructurePhraseType::SetNonDependencyStructurePhraseType_LocalType(const Mp7JrsNonDependencyStructurePhraseType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonDependencyStructurePhraseType::SetNonDependencyStructurePhraseType_LocalType().");
	}
	if (m_NonDependencyStructurePhraseType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_NonDependencyStructurePhraseType_LocalType);
		m_NonDependencyStructurePhraseType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NonDependencyStructurePhraseType_LocalType) m_NonDependencyStructurePhraseType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsNonDependencyStructurePhraseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("id")) == 0)
	{
		// id is simple attribute ID
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("equal")) == 0)
	{
		// equal is simple attribute IDREF
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("synthesis")) == 0)
	{
		// synthesis is simple attribute NonDependencyStructurePhraseType_synthesis_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quotation")) == 0)
	{
		// Quotation is contained in itemtype NonDependencyStructurePhraseType_LocalType
		// in choice collection NonDependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsNonDependencyStructurePhraseType_CollectionPtr coll = GetNonDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetNonDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsNonDependencyStructurePhraseType_LocalPtr item = CreateNonDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetQuotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Phrase")) == 0)
	{
		// Phrase is contained in itemtype NonDependencyStructurePhraseType_LocalType
		// in choice collection NonDependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsNonDependencyStructurePhraseType_CollectionPtr coll = GetNonDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetNonDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsNonDependencyStructurePhraseType_LocalPtr item = CreateNonDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetPhrase(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Word")) == 0)
	{
		// Word is contained in itemtype NonDependencyStructurePhraseType_LocalType
		// in choice collection NonDependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsNonDependencyStructurePhraseType_CollectionPtr coll = GetNonDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetNonDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetWord()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsNonDependencyStructurePhraseType_LocalPtr item = CreateNonDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType")))) != empty)
			{
				// Is type allowed
				Mp7JrsWordFormPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetWord(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetWord()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NonDependencyPhrase")) == 0)
	{
		// NonDependencyPhrase is contained in itemtype NonDependencyStructurePhraseType_LocalType
		// in choice collection NonDependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsNonDependencyStructurePhraseType_CollectionPtr coll = GetNonDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetNonDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetNonDependencyPhrase()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsNonDependencyStructurePhraseType_LocalPtr item = CreateNonDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNonDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetNonDependencyPhrase(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsNonDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetNonDependencyPhrase()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for NonDependencyStructurePhraseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "NonDependencyStructurePhraseType");
	}
	return result;
}

XMLCh * Mp7JrsNonDependencyStructurePhraseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsNonDependencyStructurePhraseType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsNonDependencyStructurePhraseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("NonDependencyStructurePhraseType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_id_Exist)
	{
	// String
	if(m_id != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("id"), m_id);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_equal_Exist)
	{
	// String
	if(m_equal != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("equal"), m_equal);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_synthesis_Exist)
	{
	// Enumeration
	if(m_synthesis != Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::ToText(m_synthesis);
		element->setAttributeNS(X(""), X("synthesis"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_NonDependencyStructurePhraseType_LocalType != Mp7JrsNonDependencyStructurePhraseType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_NonDependencyStructurePhraseType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsNonDependencyStructurePhraseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("id")))
	{
		// Deserialize string type
		this->Setid(Dc1Convert::TextToString(parent->getAttribute(X("id"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("equal")))
	{
		// Deserialize string type
		this->Setequal(Dc1Convert::TextToString(parent->getAttribute(X("equal"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("synthesis")))
	{
		this->Setsynthesis(Mp7JrsNonDependencyStructurePhraseType_synthesis_LocalType::Parse(parent->getAttribute(X("synthesis"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Quotation
			Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Quotation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Phrase
			Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Phrase")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Word
			Dc1Util::HasNodeName(parent, X("Word"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Word")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:NonDependencyPhrase
			Dc1Util::HasNodeName(parent, X("NonDependencyPhrase"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:NonDependencyPhrase")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsNonDependencyStructurePhraseType_CollectionPtr tmp = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNonDependencyStructurePhraseType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsNonDependencyStructurePhraseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Quotation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Phrase")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Word"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Word")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("NonDependencyPhrase"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("NonDependencyPhrase")) == 0)
	)
  {
	Mp7JrsNonDependencyStructurePhraseType_CollectionPtr tmp = CreateNonDependencyStructurePhraseType_CollectionType; // FTT, check this
	this->SetNonDependencyStructurePhraseType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsNonDependencyStructurePhraseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetNonDependencyStructurePhraseType_LocalType() != Dc1NodePtr())
		result.Insert(GetNonDependencyStructurePhraseType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsNonDependencyStructurePhraseType_ExtMethodImpl.h


