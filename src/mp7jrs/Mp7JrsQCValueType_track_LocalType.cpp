
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtImplInclude.h


#include "Mp7JrsQCValueType_track_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsQCValueType_track_LocalType::IMp7JrsQCValueType_track_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtPropInit.h

}

IMp7JrsQCValueType_track_LocalType::~IMp7JrsQCValueType_track_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtPropCleanup.h

}

Mp7JrsQCValueType_track_LocalType::Mp7JrsQCValueType_track_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsQCValueType_track_LocalType::~Mp7JrsQCValueType_track_LocalType()
{
	Cleanup();
}

void Mp7JrsQCValueType_track_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_string = XMLString::transcode(""); //  Mandatory String
	m_nonNegativeInteger = (0); // Value
	m_nonNegativeInteger_Valid = false; // Union flag for value



// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtMyPropInit.h

}

void Mp7JrsQCValueType_track_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtMyPropCleanup.h


	XMLString::release(&this->m_string);
	this->m_string = NULL;
	// m_nonNegativeInteger_Valid = false; // Flag for union values		
}

void Mp7JrsQCValueType_track_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QCValueType_track_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IQCValueType_track_LocalType
	const Dc1Ptr< Mp7JrsQCValueType_track_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if(tmp->Getstring() != NULL)
	{
		this->Setstring(XMLString::replicate(tmp->Getstring()));
		return; 
	}
	{
		if(tmp->m_nonNegativeInteger_Valid)
		{
			this->SetnonNegativeInteger(tmp->GetnonNegativeInteger());
			return; // Got everything we need
		}
		else
		{
			this->m_nonNegativeInteger_Valid = false;
		}
 	}
	}
}

Dc1NodePtr Mp7JrsQCValueType_track_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsQCValueType_track_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsQCValueType_track_LocalType::Getstring() const
{
		return m_string;
}

unsigned Mp7JrsQCValueType_track_LocalType::GetnonNegativeInteger() const
{
		return m_nonNegativeInteger;
}

// Value is part of a union
bool Mp7JrsQCValueType_track_LocalType::IsValidnonNegativeInteger() const
{
	return m_nonNegativeInteger_Valid;
}

void Mp7JrsQCValueType_track_LocalType::Setstring(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCValueType_track_LocalType::Setstring().");
	}
	if (m_string != item)
	{
		XMLString::release(&m_string);
		m_string = item;
	
	m_nonNegativeInteger_Valid = false;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCValueType_track_LocalType::SetnonNegativeInteger(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCValueType_track_LocalType::SetnonNegativeInteger().");
	}
	if (m_nonNegativeInteger != item)
	{
		m_nonNegativeInteger = item;
		m_nonNegativeInteger_Valid = true;
	XMLString::release(&this->m_string);
	m_string = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsQCValueType_track_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsQCValueType_track_LocalType::ToText() const
{
	if(m_string != NULL)
	{
		return m_string;
	}
	if(m_nonNegativeInteger != 0)
	{
		return Dc1Convert::BinToText(m_nonNegativeInteger);
	}
	return XMLString::transcode("");
}


bool Mp7JrsQCValueType_track_LocalType::Parse(const XMLCh * const txt)
{

	// Parse stu type "string"
	{
		XMLCh * tmp = XMLString::replicate(txt);
		this->Setstring(tmp);
		return true;
	}
										
	// Parse stu value type "nonNegativeInteger"
	{
		
		unsigned tmp = Dc1Convert::TextToUnsignedInt(txt);
		if(tmp > 0)
		{
			this->SetnonNegativeInteger(tmp);
			return true;
		}
	}
	return false;
}

bool Mp7JrsQCValueType_track_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsQCValueType_track_LocalType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsQCValueType_track_LocalType::ContentToString() const
{
	// Mp7JrsQCValueType_track_LocalType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsQCValueType_track_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("QCValueType_track_LocalType"));
	// Element serialization:
	 // String
	if(m_string != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_string, X("urn:mpeg:mpeg7:schema:2004"), X("string"), false);
	if(m_nonNegativeInteger_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_nonNegativeInteger);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("nonNegativeInteger"), false);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsQCValueType_track_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("string"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->Setstring(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setstring(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("nonNegativeInteger"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetnonNegativeInteger(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsQCValueType_track_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsQCValueType_track_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsQCValueType_track_LocalType_ExtMethodImpl.h


