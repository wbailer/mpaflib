
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtImplInclude.h


#include "Mp7JrsAnalyticEditedVideoSegmentType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTemporalMaskType.h"
#include "Mp7JrsVideoSegmentType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsVideoSegmentType_Mosaic_CollectionType.h"
#include "Mp7JrsVideoSegmentType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAnalyticClipType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsVideoSegmentType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsVisualTimeSeriesType.h" // Choice collection element VisualTimeSeriesDescriptor
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsMosaicType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Mosaic
#include "Mp7JrsVideoSegmentType_LocalType0.h" // Choice collection SpatialDecomposition
#include "Mp7JrsVideoSegmentSpatialDecompositionType.h" // Choice collection element SpatialDecomposition
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsVideoSegmentSpatioTemporalDecompositionType.h" // Choice collection element SpatioTemporalDecomposition
#include "Mp7JrsVideoSegmentMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AnalyticEditionAreaDecomposition

#include <assert.h>
IMp7JrsAnalyticClipType::IMp7JrsAnalyticClipType()
{

// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtPropInit.h

}

IMp7JrsAnalyticClipType::~IMp7JrsAnalyticClipType()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtPropCleanup.h

}

Mp7JrsAnalyticClipType::Mp7JrsAnalyticClipType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAnalyticClipType::~Mp7JrsAnalyticClipType()
{
	Cleanup();
}

void Mp7JrsAnalyticClipType::Init()
{
	// Init base
	m_Base = CreateAnalyticEditedVideoSegmentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtMyPropInit.h

}

void Mp7JrsAnalyticClipType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsAnalyticClipType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AnalyticClipTypePtr, since we
	// might need GetBase(), which isn't defined in IAnalyticClipType
	const Dc1Ptr< Mp7JrsAnalyticClipType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAnalyticEditedVideoSegmentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAnalyticClipType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrsAnalyticClipType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAnalyticClipType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAnalyticEditedVideoSegmentType > Mp7JrsAnalyticClipType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsAnalyticClipType::GetlocationReliability() const
{
	return GetBase()->GetlocationReliability();
}

bool Mp7JrsAnalyticClipType::ExistlocationReliability() const
{
	return GetBase()->ExistlocationReliability();
}
void Mp7JrsAnalyticClipType::SetlocationReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetlocationReliability().");
	}
	GetBase()->SetlocationReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidatelocationReliability()
{
	GetBase()->InvalidatelocationReliability();
}
Mp7JrszeroToOnePtr Mp7JrsAnalyticClipType::GeteditingLevelReliability() const
{
	return GetBase()->GeteditingLevelReliability();
}

bool Mp7JrsAnalyticClipType::ExisteditingLevelReliability() const
{
	return GetBase()->ExisteditingLevelReliability();
}
void Mp7JrsAnalyticClipType::SeteditingLevelReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SeteditingLevelReliability().");
	}
	GetBase()->SeteditingLevelReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidateeditingLevelReliability()
{
	GetBase()->InvalidateeditingLevelReliability();
}
Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration Mp7JrsAnalyticClipType::Gettype() const
{
	return GetBase()->Gettype();
}

bool Mp7JrsAnalyticClipType::Existtype() const
{
	return GetBase()->Existtype();
}
void Mp7JrsAnalyticClipType::Settype(Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::Invalidatetype()
{
	GetBase()->Invalidatetype();
}
Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr Mp7JrsAnalyticClipType::GetAnalyticEditionAreaDecomposition() const
{
	return GetBase()->GetAnalyticEditionAreaDecomposition();
}

void Mp7JrsAnalyticClipType::SetAnalyticEditionAreaDecomposition(const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetAnalyticEditionAreaDecomposition().");
	}
	GetBase()->SetAnalyticEditionAreaDecomposition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaTimePtr Mp7JrsAnalyticClipType::GetMediaTime() const
{
	return GetBase()->GetBase()->GetMediaTime();
}

Mp7JrsTemporalMaskPtr Mp7JrsAnalyticClipType::GetTemporalMask() const
{
	return GetBase()->GetBase()->GetTemporalMask();
}

Mp7JrsVideoSegmentType_CollectionPtr Mp7JrsAnalyticClipType::GetVideoSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetVideoSegmentType_LocalType();
}

Mp7JrsMultipleViewPtr Mp7JrsAnalyticClipType::GetMultipleView() const
{
	return GetBase()->GetBase()->GetMultipleView();
}

// Element is optional
bool Mp7JrsAnalyticClipType::IsValidMultipleView() const
{
	return GetBase()->GetBase()->IsValidMultipleView();
}

Mp7JrsVideoSegmentType_Mosaic_CollectionPtr Mp7JrsAnalyticClipType::GetMosaic() const
{
	return GetBase()->GetBase()->GetMosaic();
}

Mp7JrsVideoSegmentType_Collection0Ptr Mp7JrsAnalyticClipType::GetVideoSegmentType_LocalType0() const
{
	return GetBase()->GetBase()->GetVideoSegmentType_LocalType0();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAnalyticClipType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMediaTime().");
	}
	GetBase()->GetBase()->SetMediaTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticClipType::SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetTemporalMask().");
	}
	GetBase()->GetBase()->SetTemporalMask(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetVideoSegmentType_LocalType(const Mp7JrsVideoSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetVideoSegmentType_LocalType().");
	}
	GetBase()->GetBase()->SetVideoSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMultipleView().");
	}
	GetBase()->GetBase()->SetMultipleView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidateMultipleView()
{
	GetBase()->GetBase()->InvalidateMultipleView();
}
void Mp7JrsAnalyticClipType::SetMosaic(const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMosaic().");
	}
	GetBase()->GetBase()->SetMosaic(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetVideoSegmentType_LocalType0(const Mp7JrsVideoSegmentType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetVideoSegmentType_LocalType0().");
	}
	GetBase()->GetBase()->SetVideoSegmentType_LocalType0(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsAnalyticClipType::GetMediaInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsAnalyticClipType::GetMediaInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsAnalyticClipType::GetMediaLocator() const
{
	return GetBase()->GetBase()->GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsAnalyticClipType::GetStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsAnalyticClipType::IsValidStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsAnalyticClipType::GetCreationInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsAnalyticClipType::GetCreationInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsAnalyticClipType::GetUsageInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsAnalyticClipType::GetUsageInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsAnalyticClipType::GetTextAnnotation() const
{
	return GetBase()->GetBase()->GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsAnalyticClipType::GetSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsAnalyticClipType::GetMatchingHint() const
{
	return GetBase()->GetBase()->GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsAnalyticClipType::GetPointOfView() const
{
	return GetBase()->GetBase()->GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsAnalyticClipType::GetRelation() const
{
	return GetBase()->GetBase()->GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAnalyticClipType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMediaInformation().");
	}
	GetBase()->GetBase()->GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticClipType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMediaInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticClipType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMediaLocator().");
	}
	GetBase()->GetBase()->GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetStructuralUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidateStructuralUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAnalyticClipType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetCreationInformation().");
	}
	GetBase()->GetBase()->GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticClipType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetCreationInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsAnalyticClipType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetUsageInformation().");
	}
	GetBase()->GetBase()->GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticClipType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetUsageInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetTextAnnotation().");
	}
	GetBase()->GetBase()->GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetSegmentType_LocalType().");
	}
	GetBase()->GetBase()->GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetMatchingHint().");
	}
	GetBase()->GetBase()->GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetPointOfView().");
	}
	GetBase()->GetBase()->GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetRelation().");
	}
	GetBase()->GetBase()->GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsAnalyticClipType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsAnalyticClipType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsAnalyticClipType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticClipType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsAnalyticClipType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsAnalyticClipType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAnalyticClipType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsAnalyticClipType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsAnalyticClipType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticClipType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAnalyticClipType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAnalyticClipType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAnalyticClipType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAnalyticClipType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAnalyticClipType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticClipType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAnalyticClipType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsAnalyticClipType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticClipType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAnalyticClipType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AnalyticClipType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AnalyticClipType");
	}
	return result;
}

XMLCh * Mp7JrsAnalyticClipType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAnalyticClipType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAnalyticClipType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AnalyticClipType"));
	// Element serialization:

}

bool Mp7JrsAnalyticClipType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAnalyticEditedVideoSegmentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAnalyticClipType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAnalyticClipType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAnalyticEditionAreaDecomposition() != Dc1NodePtr())
		result.Insert(GetAnalyticEditionAreaDecomposition());
	if (GetVideoSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetMosaic() != Dc1NodePtr())
		result.Insert(GetMosaic());
	if (GetVideoSegmentType_LocalType0() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetTemporalMask() != Dc1NodePtr())
		result.Insert(GetTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAnalyticClipType_ExtMethodImpl.h


