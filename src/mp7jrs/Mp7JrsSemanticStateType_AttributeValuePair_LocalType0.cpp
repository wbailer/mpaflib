
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrsSemanticStateType_AttributeValuePair_CollectionType.h"
#include "Mp7JrsSemanticStateType_AttributeValuePair_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSemanticStateType_AttributeValuePair_LocalType.h" // Choice collection IntegerMatrixValue
#include "Mp7JrsIntegerMatrixType.h" // Choice collection element IntegerMatrixValue
#include "Mp7JrsFloatMatrixType.h" // Choice collection element FloatMatrixValue
#include "Mp7JrsTextualType.h" // Choice collection element TextValue
#include "Mp7JrsTextAnnotationType.h" // Choice collection element TextAnnotationValue
#include "Mp7JrsControlledTermUseType.h" // Choice collection element ControlledTermUseValue
#include "Mp7JrsDType.h" // Choice collection element DescriptorValue

#include <assert.h>
IMp7JrsSemanticStateType_AttributeValuePair_LocalType0::IMp7JrsSemanticStateType_AttributeValuePair_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtPropInit.h

}

IMp7JrsSemanticStateType_AttributeValuePair_LocalType0::~IMp7JrsSemanticStateType_AttributeValuePair_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtPropCleanup.h

}

Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Mp7JrsSemanticStateType_AttributeValuePair_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::~Mp7JrsSemanticStateType_AttributeValuePair_LocalType0()
{
	Cleanup();
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Attribute = Mp7JrsTermUsePtr(); // Class
	m_Unit = Mp7JrsTermUsePtr(); // Class
	m_Unit_Exist = false;
	m_SemanticStateType_AttributeValuePair_LocalType = Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtMyPropInit.h

}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Attribute);
	// Dc1Factory::DeleteObject(m_Unit);
	// Dc1Factory::DeleteObject(m_SemanticStateType_AttributeValuePair_LocalType);
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticStateType_AttributeValuePair_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in ISemanticStateType_AttributeValuePair_LocalType0
	const Dc1Ptr< Mp7JrsSemanticStateType_AttributeValuePair_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Attribute);
		this->SetAttribute(Dc1Factory::CloneObject(tmp->GetAttribute()));
	if (tmp->IsValidUnit())
	{
		// Dc1Factory::DeleteObject(m_Unit);
		this->SetUnit(Dc1Factory::CloneObject(tmp->GetUnit()));
	}
	else
	{
		InvalidateUnit();
	}
		// Dc1Factory::DeleteObject(m_SemanticStateType_AttributeValuePair_LocalType);
		this->SetSemanticStateType_AttributeValuePair_LocalType(Dc1Factory::CloneObject(tmp->GetSemanticStateType_AttributeValuePair_LocalType()));
}

Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTermUsePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetAttribute() const
{
		return m_Attribute;
}

Mp7JrsTermUsePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetUnit() const
{
		return m_Unit;
}

// Element is optional
bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::IsValidUnit() const
{
	return m_Unit_Exist;
}

Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetSemanticStateType_AttributeValuePair_LocalType() const
{
		return m_SemanticStateType_AttributeValuePair_LocalType;
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetAttribute(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetAttribute().");
	}
	if (m_Attribute != item)
	{
		// Dc1Factory::DeleteObject(m_Attribute);
		m_Attribute = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Attribute) m_Attribute->SetParent(m_myself.getPointer());
		if (m_Attribute && m_Attribute->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Attribute->UseTypeAttribute = true;
		}
		if(m_Attribute != Mp7JrsTermUsePtr())
		{
			m_Attribute->SetContentName(XMLString::transcode("Attribute"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetUnit(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetUnit().");
	}
	if (m_Unit != item || m_Unit_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Unit);
		m_Unit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Unit) m_Unit->SetParent(m_myself.getPointer());
		if (m_Unit && m_Unit->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Unit->UseTypeAttribute = true;
		}
		m_Unit_Exist = true;
		if(m_Unit != Mp7JrsTermUsePtr())
		{
			m_Unit->SetContentName(XMLString::transcode("Unit"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::InvalidateUnit()
{
	m_Unit_Exist = false;
}
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetSemanticStateType_AttributeValuePair_LocalType(const Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::SetSemanticStateType_AttributeValuePair_LocalType().");
	}
	if (m_SemanticStateType_AttributeValuePair_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SemanticStateType_AttributeValuePair_LocalType);
		m_SemanticStateType_AttributeValuePair_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SemanticStateType_AttributeValuePair_LocalType) m_SemanticStateType_AttributeValuePair_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Attribute")) == 0)
	{
		// Attribute is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAttribute()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAttribute(p, client);
					if((p = GetAttribute()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Unit")) == 0)
	{
		// Unit is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUnit()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUnit(p, client);
					if((p = GetUnit()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IntegerMatrixValue")) == 0)
	{
		// IntegerMatrixValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetIntegerMatrixValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIntegerMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetIntegerMatrixValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetIntegerMatrixValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FloatMatrixValue")) == 0)
	{
		// FloatMatrixValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetFloatMatrixValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetFloatMatrixValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetFloatMatrixValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextValue")) == 0)
	{
		// TextValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetTextValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetTextValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetTextValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextAnnotationValue")) == 0)
	{
		// TextAnnotationValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetTextAnnotationValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetTextAnnotationValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetTextAnnotationValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ControlledTermUseValue")) == 0)
	{
		// ControlledTermUseValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetControlledTermUseValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetControlledTermUseValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetControlledTermUseValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DescriptorValue")) == 0)
	{
		// DescriptorValue is contained in itemtype SemanticStateType_AttributeValuePair_LocalType
		// in choice collection SemanticStateType_AttributeValuePair_CollectionType

		context->Found = true;
		Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr coll = GetSemanticStateType_AttributeValuePair_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
				SetSemanticStateType_AttributeValuePair_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetDescriptorValue()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr item = CreateSemanticStateType_AttributeValuePair_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->SetDescriptorValue(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSemanticStateType_AttributeValuePair_LocalPtr)coll->elementAt(i))->GetDescriptorValue()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticStateType_AttributeValuePair_LocalType0
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticStateType_AttributeValuePair_LocalType0");
	}
	return result;
}

XMLCh * Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticStateType_AttributeValuePair_LocalType0"));
	// Element serialization:
	// Class
	
	if (m_Attribute != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Attribute->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Attribute"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Attribute->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Unit != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Unit->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Unit"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Unit->Serialize(doc, element, &elem);
	}
	if (m_SemanticStateType_AttributeValuePair_LocalType != Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SemanticStateType_AttributeValuePair_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Attribute"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Attribute")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAttribute(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Unit"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Unit")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUnit(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:BooleanValue
			Dc1Util::HasNodeName(parent, X("BooleanValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:BooleanValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:IntegerValue
			Dc1Util::HasNodeName(parent, X("IntegerValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IntegerValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:FloatValue
			Dc1Util::HasNodeName(parent, X("FloatValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FloatValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:IntegerMatrixValue
			Dc1Util::HasNodeName(parent, X("IntegerMatrixValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:FloatMatrixValue
			Dc1Util::HasNodeName(parent, X("FloatMatrixValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FloatMatrixValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:TextValue
			Dc1Util::HasNodeName(parent, X("TextValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TextValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:TextAnnotationValue
			Dc1Util::HasNodeName(parent, X("TextAnnotationValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TextAnnotationValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ControlledTermUseValue
			Dc1Util::HasNodeName(parent, X("ControlledTermUseValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseValue")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:DescriptorValue
			Dc1Util::HasNodeName(parent, X("DescriptorValue"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:DescriptorValue")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr tmp = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSemanticStateType_AttributeValuePair_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Attribute")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetAttribute(child);
  }
  if (XMLString::compareString(elementname, X("Unit")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetUnit(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("BooleanValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("BooleanValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("IntegerValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("IntegerValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FloatValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FloatValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("IntegerMatrixValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("IntegerMatrixValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FloatMatrixValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FloatMatrixValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TextValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TextValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TextAnnotationValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TextAnnotationValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ControlledTermUseValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ControlledTermUseValue")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("DescriptorValue"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("DescriptorValue")) == 0)
	)
  {
	Mp7JrsSemanticStateType_AttributeValuePair_CollectionPtr tmp = CreateSemanticStateType_AttributeValuePair_CollectionType; // FTT, check this
	this->SetSemanticStateType_AttributeValuePair_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticStateType_AttributeValuePair_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAttribute() != Dc1NodePtr())
		result.Insert(GetAttribute());
	if (GetUnit() != Dc1NodePtr())
		result.Insert(GetUnit());
	if (GetSemanticStateType_AttributeValuePair_LocalType() != Dc1NodePtr())
		result.Insert(GetSemanticStateType_AttributeValuePair_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType0_ExtMethodImpl.h


