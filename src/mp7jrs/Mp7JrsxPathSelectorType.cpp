
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtImplInclude.h


#include "Mp7JrsxPathSelectorType.h"

Mp7JrsxPathSelectorType::Mp7JrsxPathSelectorType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrsxPathSelectorType::~Mp7JrsxPathSelectorType()
{
		Cleanup();
}

void Mp7JrsxPathSelectorType::Init()
{	   
		m_Content = NULL;

// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtPropInit.h

}

void Mp7JrsxPathSelectorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtPropCleanup.h


		XMLString::release(&m_Content);
}

void Mp7JrsxPathSelectorType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrsxPathSelectorType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		SetContent(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(tmp->GetContent()));
}

Dc1NodePtr Mp7JrsxPathSelectorType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsxPathSelectorType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

XMLCh * Mp7JrsxPathSelectorType::GetContent() const
{
		return m_Content;
}

void Mp7JrsxPathSelectorType::SetContent(XMLCh * val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsxPathSelectorType::SetContent().");
		}
		if(m_Content != val)
		{
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&m_Content);
				m_Content = val;
		}
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrsxPathSelectorType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");

		// For symmetry, create an entry, which must be released
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(m_Content);
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrsxPathSelectorType::Parse(const XMLCh * const txt)
{
		Cleanup();
		// mio 07 10 2004
		// FIXME: Regular expression validation is not implemented for this general pattern type.
		// (developers: see PatternType_CPP.template comments for more details.) 
		
		m_Content =  XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(txt);
		return true;
}

bool Mp7JrsxPathSelectorType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsxPathSelectorType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsxPathSelectorType::ContentToString() const
{
	// Mp7JrsxPathSelectorType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsxPathSelectorType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:xPathSelectorType")))));
		}

// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtPreImplementCleanup.h


		if(this->GetContentName() != NULL)
		{
				element->appendChild(doc->createTextNode(this->GetContentName())); 
		}
}

bool Mp7JrsxPathSelectorType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtPostDeserialize.h



		if(parent == NULL)
		{
				return false;
		}

		bool found = false;

		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::replicate(Dc1Util::GetElementText(parent));
		if(buf != NULL)
		{
				found = true;
				* current = parent;
				SetContent(buf);
		}
		return found;
}

// no includefile for extension defined 
// file Mp7JrsxPathSelectorType_ExtMethodImpl.h








