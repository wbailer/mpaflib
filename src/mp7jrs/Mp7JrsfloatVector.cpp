
// Extension library class
// based on ValCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <stdio.h>
#include "Mp7JrsfloatVector.h"
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtImplInclude.h


Mp7JrsfloatVector::Mp7JrsfloatVector()
{
	m_Item = new Dc1ValueVectorOf <float>(0);
	
// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtPropInit.h


}

Mp7JrsfloatVector::~Mp7JrsfloatVector()
{
// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtPropCleanup.h


	delete m_Item;
}

void Mp7JrsfloatVector::DeepCopy(const Dc1NodePtr & original)
{
	const Mp7JrsfloatVectorPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: wrong argument type

	m_Item->removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		m_Item->addElement(tmp->elementAt(i));
	}
}

// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtMethodImpl.h


void Mp7JrsfloatVector::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void Mp7JrsfloatVector::addElement(const float & toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsfloatVector::addElement().");
	}
	m_Item->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsfloatVector::setElementAt(const float & toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsfloatVector::setElementAt().");
	}
	m_Item->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsfloatVector::insertElementAt(const float & toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsfloatVector::insertElementAt().");
	}
	m_Item->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsfloatVector::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsfloatVector::removeAllElements().");
	}
	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsfloatVector::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsfloatVector::removeElementAt().");
	}
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsfloatVector::containsElement(const float &toCheck)
{
	return m_Item->containsElement(toCheck);
}

unsigned int Mp7JrsfloatVector::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const float Mp7JrsfloatVector::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

float Mp7JrsfloatVector::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

unsigned int Mp7JrsfloatVector::size() const
{
	return (unsigned int)m_Item->size();
}

void Mp7JrsfloatVector::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

const float * Mp7JrsfloatVector::rawData() const
{
	return m_Item->rawData();
}

int Mp7JrsfloatVector::elementIndexOf(const float &toCheck) const
{
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		if(m_Item->elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1; // Not found
}


XMLCh * Mp7JrsfloatVector::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Item->elementAt(i));
		Dc1Util::CatString(&buf, tmp);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		if(i < m_Item->size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer
	return buf;
}

bool Mp7JrsfloatVector::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(txt);
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			m_Item->addElement(Dc1Convert::TextToFloat(tmp->elementAt(i)));
		}
	}
	delete tmp; // FTT release?
	return m_Item->size() > 0;
}

bool Mp7JrsfloatVector::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsfloatVector has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsfloatVector::ContentToString() const
{
	// Mp7JrsfloatVector has no attributes so forward it to ToText()
	return ToText();
}

/**

 * Item type is Empty.
 */
void Mp7JrsfloatVector::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
		if(this->GetContentName() != (XMLCh*)NULL)
		{
// OLD 			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			element->appendChild(elem);
			*newElem = elem;
			element = elem;
		}
	} else {
		element = doc->getDocumentElement();
	}
	*newElem = element;

	if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
	{
		// FTT This has to be solved in all classes using  setAttribute(X("xsi:type")!
		element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))));
	}

// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtPreSerialize.h


	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * txt = Dc1Convert::BinToText(m_Item->elementAt(i));
		// sat 2004-04-14: removed creation of elements for each item in value collection
		// This could be wrong as well, but at least there are no duplicated tags in Mp7JrsintegerVector any more.
//		if(this->GetContentName() != NULL)
//		{
//			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
//			elem->appendChild(doc->createTextNode(txt));
//			element->appendChild(elem);
//		}
//		else
//		{
			element->appendChild(doc->createTextNode(txt));
//		}
		if(i < m_Item->size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&txt);
	}
}





bool Mp7JrsfloatVector::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
	if(parent == NULL)
	{
		return false;
	}
	* current = parent; // Init with first element - check this!

	// Deserialize value type collection
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			m_Item->addElement(Dc1Convert::TextToFloat(tmp->elementAt(i)));
		}
		// TODO Check do we need elementnames?
	}
	delete tmp; // FTT release?

// no includefile for extension defined 
// file Mp7JrsfloatVector_ExtPostDeserialize.h


	return true;
}
