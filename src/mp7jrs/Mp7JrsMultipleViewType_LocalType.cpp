
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsMultipleViewType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsMultipleViewType_LocalType::IMp7JrsMultipleViewType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtPropInit.h

}

IMp7JrsMultipleViewType_LocalType::~IMp7JrsMultipleViewType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtPropCleanup.h

}

Mp7JrsMultipleViewType_LocalType::Mp7JrsMultipleViewType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMultipleViewType_LocalType::~Mp7JrsMultipleViewType_LocalType()
{
	Cleanup();
}

void Mp7JrsMultipleViewType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_IsVisible = (false); // Value

	m_Descriptor = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtMyPropInit.h

}

void Mp7JrsMultipleViewType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Descriptor);
}

void Mp7JrsMultipleViewType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MultipleViewType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMultipleViewType_LocalType
	const Dc1Ptr< Mp7JrsMultipleViewType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetIsVisible(tmp->GetIsVisible());
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
}

Dc1NodePtr Mp7JrsMultipleViewType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMultipleViewType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsMultipleViewType_LocalType::GetIsVisible() const
{
		return m_IsVisible;
}

Dc1Ptr< Dc1Node > Mp7JrsMultipleViewType_LocalType::GetDescriptor() const
{
		return m_Descriptor;
}

void Mp7JrsMultipleViewType_LocalType::SetIsVisible(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultipleViewType_LocalType::SetIsVisible().");
	}
	if (m_IsVisible != item)
	{
		m_IsVisible = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultipleViewType_LocalType::SetDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultipleViewType_LocalType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Descriptor) m_Descriptor->UseTypeAttribute = true;
		if(m_Descriptor != Dc1Ptr< Dc1Node >())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: MultipleViewType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsMultipleViewType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsMultipleViewType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMultipleViewType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMultipleViewType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MultipleViewType_LocalType"));
	// Element serialization:
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_IsVisible);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("IsVisible"), false);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_Descriptor != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Descriptor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Descriptor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Descriptor->UseTypeAttribute = true;
		}
		m_Descriptor->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMultipleViewType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("IsVisible"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetIsVisible(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMultipleViewType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Descriptor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDType; // FTT, check this
	}
	this->SetDescriptor(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMultipleViewType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMultipleViewType_LocalType_ExtMethodImpl.h


