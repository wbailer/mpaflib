
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsClassificationSchemeBaseType_domain_CollectionType.h"
#include "Mp7JrsClassificationSchemeBaseType_Import_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsClassificationSchemeBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsReferenceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Import

#include <assert.h>
IMp7JrsClassificationSchemeBaseType::IMp7JrsClassificationSchemeBaseType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtPropInit.h

}

IMp7JrsClassificationSchemeBaseType::~IMp7JrsClassificationSchemeBaseType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtPropCleanup.h

}

Mp7JrsClassificationSchemeBaseType::Mp7JrsClassificationSchemeBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationSchemeBaseType::~Mp7JrsClassificationSchemeBaseType()
{
	Cleanup();
}

void Mp7JrsClassificationSchemeBaseType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_uri = NULL; // String
	m_domain = Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr(); // Collection
	m_domain_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Import = Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtMyPropInit.h

}

void Mp7JrsClassificationSchemeBaseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_uri); // String
	// Dc1Factory::DeleteObject(m_domain);
	// Dc1Factory::DeleteObject(m_Import);
}

void Mp7JrsClassificationSchemeBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationSchemeBaseTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationSchemeBaseType
	const Dc1Ptr< Mp7JrsClassificationSchemeBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationSchemeBaseType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_uri); // String
		this->Seturi(XMLString::replicate(tmp->Geturi()));
	}
	{
	// Dc1Factory::DeleteObject(m_domain);
	if (tmp->Existdomain())
	{
		this->Setdomain(Dc1Factory::CloneObject(tmp->Getdomain()));
	}
	else
	{
		Invalidatedomain();
	}
	}
		// Dc1Factory::DeleteObject(m_Import);
		this->SetImport(Dc1Factory::CloneObject(tmp->GetImport()));
}

Dc1NodePtr Mp7JrsClassificationSchemeBaseType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationSchemeBaseType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsClassificationSchemeBaseType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsClassificationSchemeBaseType::Geturi() const
{
	return m_uri;
}

void Mp7JrsClassificationSchemeBaseType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::Seturi().");
	}
	m_uri = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr Mp7JrsClassificationSchemeBaseType::Getdomain() const
{
	return m_domain;
}

bool Mp7JrsClassificationSchemeBaseType::Existdomain() const
{
	return m_domain_Exist;
}
void Mp7JrsClassificationSchemeBaseType::Setdomain(const Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::Setdomain().");
	}
	m_domain = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_domain) m_domain->SetParent(m_myself.getPointer());
	m_domain_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::Invalidatedomain()
{
	m_domain_Exist = false;
}
Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr Mp7JrsClassificationSchemeBaseType::GetImport() const
{
		return m_Import;
}

void Mp7JrsClassificationSchemeBaseType::SetImport(const Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SetImport().");
	}
	if (m_Import != item)
	{
		// Dc1Factory::DeleteObject(m_Import);
		m_Import = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Import) m_Import->SetParent(m_myself.getPointer());
		if(m_Import != Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr())
		{
			m_Import->SetContentName(XMLString::transcode("Import"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsClassificationSchemeBaseType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsClassificationSchemeBaseType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsClassificationSchemeBaseType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationSchemeBaseType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsClassificationSchemeBaseType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsClassificationSchemeBaseType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsClassificationSchemeBaseType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsClassificationSchemeBaseType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsClassificationSchemeBaseType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationSchemeBaseType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsClassificationSchemeBaseType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsClassificationSchemeBaseType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsClassificationSchemeBaseType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsClassificationSchemeBaseType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsClassificationSchemeBaseType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeBaseType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsClassificationSchemeBaseType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsClassificationSchemeBaseType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeBaseType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationSchemeBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("uri")) == 0)
	{
		// uri is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("domain")) == 0)
	{
		// domain is simple attribute ClassificationSchemeBaseType_domain_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Import")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Import is item of type ReferenceType
		// in element collection ClassificationSchemeBaseType_Import_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr coll = GetImport();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationSchemeBaseType_Import_CollectionType; // FTT, check this
				SetImport(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationSchemeBaseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationSchemeBaseType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationSchemeBaseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsClassificationSchemeBaseType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsClassificationSchemeBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationSchemeBaseType"));
	// Attribute Serialization:
	// String
	if(m_uri != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("uri"), m_uri);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_domain_Exist)
	{
	// Collection
	if(m_domain != Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr())
	{
		XMLCh * tmp = m_domain->ToText();
		element->setAttributeNS(X(""), X("domain"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Import != Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Import->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsClassificationSchemeBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("uri")))
	{
		// Deserialize string type
		this->Seturi(Dc1Convert::TextToString(parent->getAttribute(X("uri"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("domain")))
	{
		// Deserialize collection type
		Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr tmp = CreateClassificationSchemeBaseType_domain_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("domain")));
		this->Setdomain(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Import"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Import")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr tmp = CreateClassificationSchemeBaseType_Import_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetImport(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationSchemeBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Import")) == 0))
  {
	Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr tmp = CreateClassificationSchemeBaseType_Import_CollectionType; // FTT, check this
	this->SetImport(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationSchemeBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetImport() != Dc1NodePtr())
		result.Insert(GetImport());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationSchemeBaseType_ExtMethodImpl.h


