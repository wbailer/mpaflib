
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtImplInclude.h


#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsRecordingRequestType_RecordingPeriod_LocalType.h"
#include "Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType.h"
#include "Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionType.h"
#include "Mp7JrsRecordingRequestType_RecordAssociatedData_LocalType.h"
#include "Mp7JrsRecordingRequestType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RecordingSourceTypePreferences

#include <assert.h>
IMp7JrsRecordingRequestType::IMp7JrsRecordingRequestType()
{

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtPropInit.h

}

IMp7JrsRecordingRequestType::~IMp7JrsRecordingRequestType()
{
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtPropCleanup.h

}

Mp7JrsRecordingRequestType::Mp7JrsRecordingRequestType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRecordingRequestType::~Mp7JrsRecordingRequestType()
{
	Cleanup();
}

void Mp7JrsRecordingRequestType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_ContentIdentifier = Mp7JrsUniqueIDPtr(); // Class
	m_DisseminationSource = Mp7JrsTextualPtr(); // Class
	m_DisseminationSource_Exist = false;
	m_Title = Mp7JrsTextualPtr(); // Class
	m_Title_Exist = false;
	m_RecordingPeriod = Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr(); // Class
	m_RecordingLocationPreferences = Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr(); // Collection
	m_RecordingSourceTypePreferences = Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr(); // Collection
	m_RecordAssociatedData = Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr(); // Class
	m_RecordAssociatedData_Exist = false;


// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMyPropInit.h

}

void Mp7JrsRecordingRequestType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_ContentIdentifier);
	// Dc1Factory::DeleteObject(m_DisseminationSource);
	// Dc1Factory::DeleteObject(m_Title);
	// Dc1Factory::DeleteObject(m_RecordingPeriod);
	// Dc1Factory::DeleteObject(m_RecordingLocationPreferences);
	// Dc1Factory::DeleteObject(m_RecordingSourceTypePreferences);
	// Dc1Factory::DeleteObject(m_RecordAssociatedData);
}

void Mp7JrsRecordingRequestType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RecordingRequestTypePtr, since we
	// might need GetBase(), which isn't defined in IRecordingRequestType
	const Dc1Ptr< Mp7JrsRecordingRequestType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_ContentIdentifier);
		this->SetContentIdentifier(Dc1Factory::CloneObject(tmp->GetContentIdentifier()));
	if (tmp->IsValidDisseminationSource())
	{
		// Dc1Factory::DeleteObject(m_DisseminationSource);
		this->SetDisseminationSource(Dc1Factory::CloneObject(tmp->GetDisseminationSource()));
	}
	else
	{
		InvalidateDisseminationSource();
	}
	if (tmp->IsValidTitle())
	{
		// Dc1Factory::DeleteObject(m_Title);
		this->SetTitle(Dc1Factory::CloneObject(tmp->GetTitle()));
	}
	else
	{
		InvalidateTitle();
	}
		// Dc1Factory::DeleteObject(m_RecordingPeriod);
		this->SetRecordingPeriod(Dc1Factory::CloneObject(tmp->GetRecordingPeriod()));
		// Dc1Factory::DeleteObject(m_RecordingLocationPreferences);
		this->SetRecordingLocationPreferences(Dc1Factory::CloneObject(tmp->GetRecordingLocationPreferences()));
		// Dc1Factory::DeleteObject(m_RecordingSourceTypePreferences);
		this->SetRecordingSourceTypePreferences(Dc1Factory::CloneObject(tmp->GetRecordingSourceTypePreferences()));
	if (tmp->IsValidRecordAssociatedData())
	{
		// Dc1Factory::DeleteObject(m_RecordAssociatedData);
		this->SetRecordAssociatedData(Dc1Factory::CloneObject(tmp->GetRecordAssociatedData()));
	}
	else
	{
		InvalidateRecordAssociatedData();
	}
}

Dc1NodePtr Mp7JrsRecordingRequestType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsRecordingRequestType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsUniqueIDPtr Mp7JrsRecordingRequestType::GetContentIdentifier() const
{
		return m_ContentIdentifier;
}

Mp7JrsTextualPtr Mp7JrsRecordingRequestType::GetDisseminationSource() const
{
		return m_DisseminationSource;
}

// Element is optional
bool Mp7JrsRecordingRequestType::IsValidDisseminationSource() const
{
	return m_DisseminationSource_Exist;
}

Mp7JrsTextualPtr Mp7JrsRecordingRequestType::GetTitle() const
{
		return m_Title;
}

// Element is optional
bool Mp7JrsRecordingRequestType::IsValidTitle() const
{
	return m_Title_Exist;
}

Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr Mp7JrsRecordingRequestType::GetRecordingPeriod() const
{
		return m_RecordingPeriod;
}

Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr Mp7JrsRecordingRequestType::GetRecordingLocationPreferences() const
{
		return m_RecordingLocationPreferences;
}

Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr Mp7JrsRecordingRequestType::GetRecordingSourceTypePreferences() const
{
		return m_RecordingSourceTypePreferences;
}

Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr Mp7JrsRecordingRequestType::GetRecordAssociatedData() const
{
		return m_RecordAssociatedData;
}

// Element is optional
bool Mp7JrsRecordingRequestType::IsValidRecordAssociatedData() const
{
	return m_RecordAssociatedData_Exist;
}

void Mp7JrsRecordingRequestType::SetContentIdentifier(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetContentIdentifier().");
	}
	if (m_ContentIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_ContentIdentifier);
		m_ContentIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContentIdentifier) m_ContentIdentifier->SetParent(m_myself.getPointer());
		if (m_ContentIdentifier && m_ContentIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ContentIdentifier->UseTypeAttribute = true;
		}
		if(m_ContentIdentifier != Mp7JrsUniqueIDPtr())
		{
			m_ContentIdentifier->SetContentName(XMLString::transcode("ContentIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::SetDisseminationSource(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetDisseminationSource().");
	}
	if (m_DisseminationSource != item || m_DisseminationSource_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DisseminationSource);
		m_DisseminationSource = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DisseminationSource) m_DisseminationSource->SetParent(m_myself.getPointer());
		if (m_DisseminationSource && m_DisseminationSource->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DisseminationSource->UseTypeAttribute = true;
		}
		m_DisseminationSource_Exist = true;
		if(m_DisseminationSource != Mp7JrsTextualPtr())
		{
			m_DisseminationSource->SetContentName(XMLString::transcode("DisseminationSource"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::InvalidateDisseminationSource()
{
	m_DisseminationSource_Exist = false;
}
void Mp7JrsRecordingRequestType::SetTitle(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetTitle().");
	}
	if (m_Title != item || m_Title_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Title);
		m_Title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Title) m_Title->SetParent(m_myself.getPointer());
		if (m_Title && m_Title->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Title->UseTypeAttribute = true;
		}
		m_Title_Exist = true;
		if(m_Title != Mp7JrsTextualPtr())
		{
			m_Title->SetContentName(XMLString::transcode("Title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::InvalidateTitle()
{
	m_Title_Exist = false;
}
void Mp7JrsRecordingRequestType::SetRecordingPeriod(const Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetRecordingPeriod().");
	}
	if (m_RecordingPeriod != item)
	{
		// Dc1Factory::DeleteObject(m_RecordingPeriod);
		m_RecordingPeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RecordingPeriod) m_RecordingPeriod->SetParent(m_myself.getPointer());
		if (m_RecordingPeriod && m_RecordingPeriod->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingPeriod_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RecordingPeriod->UseTypeAttribute = true;
		}
		if(m_RecordingPeriod != Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr())
		{
			m_RecordingPeriod->SetContentName(XMLString::transcode("RecordingPeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::SetRecordingLocationPreferences(const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetRecordingLocationPreferences().");
	}
	if (m_RecordingLocationPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_RecordingLocationPreferences);
		m_RecordingLocationPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RecordingLocationPreferences) m_RecordingLocationPreferences->SetParent(m_myself.getPointer());
		if(m_RecordingLocationPreferences != Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr())
		{
			m_RecordingLocationPreferences->SetContentName(XMLString::transcode("RecordingLocationPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::SetRecordingSourceTypePreferences(const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetRecordingSourceTypePreferences().");
	}
	if (m_RecordingSourceTypePreferences != item)
	{
		// Dc1Factory::DeleteObject(m_RecordingSourceTypePreferences);
		m_RecordingSourceTypePreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RecordingSourceTypePreferences) m_RecordingSourceTypePreferences->SetParent(m_myself.getPointer());
		if(m_RecordingSourceTypePreferences != Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr())
		{
			m_RecordingSourceTypePreferences->SetContentName(XMLString::transcode("RecordingSourceTypePreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::SetRecordAssociatedData(const Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType::SetRecordAssociatedData().");
	}
	if (m_RecordAssociatedData != item || m_RecordAssociatedData_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RecordAssociatedData);
		m_RecordAssociatedData = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RecordAssociatedData) m_RecordAssociatedData->SetParent(m_myself.getPointer());
		if (m_RecordAssociatedData && m_RecordAssociatedData->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RecordAssociatedData->UseTypeAttribute = true;
		}
		m_RecordAssociatedData_Exist = true;
		if(m_RecordAssociatedData != Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr())
		{
			m_RecordAssociatedData->SetContentName(XMLString::transcode("RecordAssociatedData"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType::InvalidateRecordAssociatedData()
{
	m_RecordAssociatedData_Exist = false;
}

Dc1NodeEnum Mp7JrsRecordingRequestType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ContentIdentifier")) == 0)
	{
		// ContentIdentifier is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContentIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContentIdentifier(p, client);
					if((p = GetContentIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DisseminationSource")) == 0)
	{
		// DisseminationSource is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDisseminationSource()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDisseminationSource(p, client);
					if((p = GetDisseminationSource()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Title")) == 0)
	{
		// Title is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTitle()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTitle(p, client);
					if((p = GetTitle()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RecordingPeriod")) == 0)
	{
		// RecordingPeriod is simple element RecordingRequestType_RecordingPeriod_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRecordingPeriod()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingPeriod_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRecordingPeriod(p, client);
					if((p = GetRecordingPeriod()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RecordingLocationPreferences")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RecordingSourceTypePreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:RecordingSourceTypePreferences is item of type RecordingRequestType_RecordingSourceTypePreferences_LocalType
		// in element collection RecordingRequestType_RecordingSourceTypePreferences_CollectionType
		
		context->Found = true;
		Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr coll = GetRecordingSourceTypePreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRecordingRequestType_RecordingSourceTypePreferences_CollectionType; // FTT, check this
				SetRecordingSourceTypePreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 2))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordingSourceTypePreferences_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RecordAssociatedData")) == 0)
	{
		// RecordAssociatedData is simple element RecordingRequestType_RecordAssociatedData_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRecordAssociatedData()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingRequestType_RecordAssociatedData_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRecordAssociatedData(p, client);
					if((p = GetRecordAssociatedData()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RecordingRequestType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RecordingRequestType");
	}
	return result;
}

XMLCh * Mp7JrsRecordingRequestType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRecordingRequestType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsRecordingRequestType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RecordingRequestType"));
	// Element serialization:
	// Class
	
	if (m_ContentIdentifier != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ContentIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ContentIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ContentIdentifier->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DisseminationSource != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DisseminationSource->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DisseminationSource"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DisseminationSource->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Title != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Title->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Title"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Title->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_RecordingPeriod != Mp7JrsRecordingRequestType_RecordingPeriod_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RecordingPeriod->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RecordingPeriod"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RecordingPeriod->Serialize(doc, element, &elem);
	}
	if (m_RecordingLocationPreferences != Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RecordingLocationPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_RecordingSourceTypePreferences != Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RecordingSourceTypePreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_RecordAssociatedData != Mp7JrsRecordingRequestType_RecordAssociatedData_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RecordAssociatedData->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RecordAssociatedData"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RecordAssociatedData->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsRecordingRequestType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ContentIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ContentIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContentIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DisseminationSource"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DisseminationSource")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDisseminationSource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Title")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitle(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RecordingPeriod"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RecordingPeriod")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRecordingRequestType_RecordingPeriod_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRecordingPeriod(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RecordingLocationPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RecordingLocationPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr tmp = CreateRecordingRequestType_RecordingLocationPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRecordingLocationPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RecordingSourceTypePreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RecordingSourceTypePreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr tmp = CreateRecordingRequestType_RecordingSourceTypePreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRecordingSourceTypePreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RecordAssociatedData"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RecordAssociatedData")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRecordingRequestType_RecordAssociatedData_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRecordAssociatedData(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRecordingRequestType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("ContentIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetContentIdentifier(child);
  }
  if (XMLString::compareString(elementname, X("DisseminationSource")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetDisseminationSource(child);
  }
  if (XMLString::compareString(elementname, X("Title")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetTitle(child);
  }
  if (XMLString::compareString(elementname, X("RecordingPeriod")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRecordingRequestType_RecordingPeriod_LocalType; // FTT, check this
	}
	this->SetRecordingPeriod(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RecordingLocationPreferences")) == 0))
  {
	Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr tmp = CreateRecordingRequestType_RecordingLocationPreferences_CollectionType; // FTT, check this
	this->SetRecordingLocationPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RecordingSourceTypePreferences")) == 0))
  {
	Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_CollectionPtr tmp = CreateRecordingRequestType_RecordingSourceTypePreferences_CollectionType; // FTT, check this
	this->SetRecordingSourceTypePreferences(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("RecordAssociatedData")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRecordingRequestType_RecordAssociatedData_LocalType; // FTT, check this
	}
	this->SetRecordAssociatedData(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRecordingRequestType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContentIdentifier() != Dc1NodePtr())
		result.Insert(GetContentIdentifier());
	if (GetDisseminationSource() != Dc1NodePtr())
		result.Insert(GetDisseminationSource());
	if (GetTitle() != Dc1NodePtr())
		result.Insert(GetTitle());
	if (GetRecordingPeriod() != Dc1NodePtr())
		result.Insert(GetRecordingPeriod());
	if (GetRecordingLocationPreferences() != Dc1NodePtr())
		result.Insert(GetRecordingLocationPreferences());
	if (GetRecordingSourceTypePreferences() != Dc1NodePtr())
		result.Insert(GetRecordingSourceTypePreferences());
	if (GetRecordAssociatedData() != Dc1NodePtr())
		result.Insert(GetRecordAssociatedData());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_ExtMethodImpl.h


