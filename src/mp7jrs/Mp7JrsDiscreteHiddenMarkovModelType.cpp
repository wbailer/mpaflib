
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtImplInclude.h


#include "Mp7JrsStateTransitionModelType.h"
#include "Mp7JrsDiscreteHiddenMarkovModelType_CollectionType.h"
#include "Mp7JrsProbabilityMatrixType.h"
#include "Mp7JrsStateTransitionModelType_State_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDiscreteHiddenMarkovModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsModelStateType.h" // Element collection urn:mpeg:mpeg7:schema:2004:State
#include "Mp7JrsDiscreteHiddenMarkovModelType_LocalType.h" // Sequence collection ObservationDistribution
#include "Mp7JrsDiscreteDistributionType.h" // Sequence collection element ObservationDistribution

#include <assert.h>
IMp7JrsDiscreteHiddenMarkovModelType::IMp7JrsDiscreteHiddenMarkovModelType()
{

// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtPropInit.h

}

IMp7JrsDiscreteHiddenMarkovModelType::~IMp7JrsDiscreteHiddenMarkovModelType()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtPropCleanup.h

}

Mp7JrsDiscreteHiddenMarkovModelType::Mp7JrsDiscreteHiddenMarkovModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDiscreteHiddenMarkovModelType::~Mp7JrsDiscreteHiddenMarkovModelType()
{
	Cleanup();
}

void Mp7JrsDiscreteHiddenMarkovModelType::Init()
{
	// Init base
	m_Base = CreateStateTransitionModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_DiscreteHiddenMarkovModelType_LocalType = Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtMyPropInit.h

}

void Mp7JrsDiscreteHiddenMarkovModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_DiscreteHiddenMarkovModelType_LocalType);
}

void Mp7JrsDiscreteHiddenMarkovModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DiscreteHiddenMarkovModelTypePtr, since we
	// might need GetBase(), which isn't defined in IDiscreteHiddenMarkovModelType
	const Dc1Ptr< Mp7JrsDiscreteHiddenMarkovModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsStateTransitionModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDiscreteHiddenMarkovModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_DiscreteHiddenMarkovModelType_LocalType);
		this->SetDiscreteHiddenMarkovModelType_LocalType(Dc1Factory::CloneObject(tmp->GetDiscreteHiddenMarkovModelType_LocalType()));
}

Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsStateTransitionModelType > Mp7JrsDiscreteHiddenMarkovModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr Mp7JrsDiscreteHiddenMarkovModelType::GetDiscreteHiddenMarkovModelType_LocalType() const
{
		return m_DiscreteHiddenMarkovModelType_LocalType;
}

void Mp7JrsDiscreteHiddenMarkovModelType::SetDiscreteHiddenMarkovModelType_LocalType(const Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetDiscreteHiddenMarkovModelType_LocalType().");
	}
	if (m_DiscreteHiddenMarkovModelType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_DiscreteHiddenMarkovModelType_LocalType);
		m_DiscreteHiddenMarkovModelType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DiscreteHiddenMarkovModelType_LocalType) m_DiscreteHiddenMarkovModelType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsProbabilityMatrixPtr Mp7JrsDiscreteHiddenMarkovModelType::GetInitial() const
{
	return GetBase()->GetInitial();
}

// Element is optional
bool Mp7JrsDiscreteHiddenMarkovModelType::IsValidInitial() const
{
	return GetBase()->IsValidInitial();
}

Mp7JrsProbabilityMatrixPtr Mp7JrsDiscreteHiddenMarkovModelType::GetTransitions() const
{
	return GetBase()->GetTransitions();
}

Mp7JrsStateTransitionModelType_State_CollectionPtr Mp7JrsDiscreteHiddenMarkovModelType::GetState() const
{
	return GetBase()->GetState();
}

void Mp7JrsDiscreteHiddenMarkovModelType::SetInitial(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetInitial().");
	}
	GetBase()->SetInitial(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::InvalidateInitial()
{
	GetBase()->InvalidateInitial();
}
void Mp7JrsDiscreteHiddenMarkovModelType::SetTransitions(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetTransitions().");
	}
	GetBase()->SetTransitions(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::SetState(const Mp7JrsStateTransitionModelType_State_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetState().");
	}
	GetBase()->SetState(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsDiscreteHiddenMarkovModelType::GetnumOfStates() const
{
	return GetBase()->GetBase()->GetnumOfStates();
}

void Mp7JrsDiscreteHiddenMarkovModelType::SetnumOfStates(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetnumOfStates().");
	}
	GetBase()->GetBase()->SetnumOfStates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsDiscreteHiddenMarkovModelType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsDiscreteHiddenMarkovModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsDiscreteHiddenMarkovModelType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsDiscreteHiddenMarkovModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsDiscreteHiddenMarkovModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsDiscreteHiddenMarkovModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteHiddenMarkovModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsDiscreteHiddenMarkovModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsDiscreteHiddenMarkovModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsDiscreteHiddenMarkovModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteHiddenMarkovModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsDiscreteHiddenMarkovModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsDiscreteHiddenMarkovModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsDiscreteHiddenMarkovModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsDiscreteHiddenMarkovModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteHiddenMarkovModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDiscreteHiddenMarkovModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsDiscreteHiddenMarkovModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteHiddenMarkovModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDiscreteHiddenMarkovModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Observation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Observation is itemtype TextualType in element collection DiscreteHiddenMarkovModelType_Observation_CollectionType
		// nested in sequence collection DiscreteHiddenMarkovModelType_CollectionType (itemtype: DiscreteHiddenMarkovModelType_LocalType)
		throw Dc1Exception(DC1_ERROR, "Sorry, sequence collections of element collections will be supported soon.");
		return result;
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ObservationDistribution")) == 0)
	{
		// ObservationDistribution is contained in itemtype DiscreteHiddenMarkovModelType_LocalType
		// in sequence collection DiscreteHiddenMarkovModelType_CollectionType

		context->Found = true;
		Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr coll = GetDiscreteHiddenMarkovModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDiscreteHiddenMarkovModelType_CollectionType; // FTT, check this
				SetDiscreteHiddenMarkovModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(i))->GetObservationDistribution()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr item = CreateDiscreteHiddenMarkovModelType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(j))->SetObservationDistribution(
						((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(j+1))->GetObservationDistribution());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(j))->SetObservationDistribution(
						((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(j-1))->GetObservationDistribution());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsDiscreteDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(i))->SetObservationDistribution(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsDiscreteHiddenMarkovModelType_LocalPtr)coll->elementAt(i))->GetObservationDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DiscreteHiddenMarkovModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DiscreteHiddenMarkovModelType");
	}
	return result;
}

XMLCh * Mp7JrsDiscreteHiddenMarkovModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDiscreteHiddenMarkovModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDiscreteHiddenMarkovModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DiscreteHiddenMarkovModelType"));
	// Element serialization:
	if (m_DiscreteHiddenMarkovModelType_LocalType != Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DiscreteHiddenMarkovModelType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDiscreteHiddenMarkovModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsStateTransitionModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:NumOfObservationsPerState")) == 0)
			(Dc1Util::HasNodeName(parent, X("NumOfObservationsPerState"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Observation")) == 0)
			(Dc1Util::HasNodeName(parent, X("Observation"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ObservationDistribution")) == 0)
			(Dc1Util::HasNodeName(parent, X("ObservationDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr tmp = CreateDiscreteHiddenMarkovModelType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDiscreteHiddenMarkovModelType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDiscreteHiddenMarkovModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsDiscreteHiddenMarkovModelType_CollectionPtr tmp = CreateDiscreteHiddenMarkovModelType_CollectionType; // FTT, check this
	this->SetDiscreteHiddenMarkovModelType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDiscreteHiddenMarkovModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDiscreteHiddenMarkovModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetDiscreteHiddenMarkovModelType_LocalType());
	if (GetInitial() != Dc1NodePtr())
		result.Insert(GetInitial());
	if (GetTransitions() != Dc1NodePtr())
		result.Insert(GetTransitions());
	if (GetState() != Dc1NodePtr())
		result.Insert(GetState());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDiscreteHiddenMarkovModelType_ExtMethodImpl.h


