
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsUserIdentifierType.h"
#include "Mp7JrsUsageHistoryType_UserActionHistory_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUsageHistoryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsUserActionHistoryType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UserActionHistory

#include <assert.h>
IMp7JrsUsageHistoryType::IMp7JrsUsageHistoryType()
{

// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtPropInit.h

}

IMp7JrsUsageHistoryType::~IMp7JrsUsageHistoryType()
{
// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtPropCleanup.h

}

Mp7JrsUsageHistoryType::Mp7JrsUsageHistoryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUsageHistoryType::~Mp7JrsUsageHistoryType()
{
	Cleanup();
}

void Mp7JrsUsageHistoryType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_allowCollection = Mp7JrsuserChoiceType::UninitializedEnumeration;
	m_allowCollection_Default = Mp7JrsuserChoiceType::_false; // Default enumeration
	m_allowCollection_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_UserIdentifier = Mp7JrsUserIdentifierPtr(); // Class
	m_UserIdentifier_Exist = false;
	m_UserActionHistory = Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtMyPropInit.h

}

void Mp7JrsUsageHistoryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_UserIdentifier);
	// Dc1Factory::DeleteObject(m_UserActionHistory);
}

void Mp7JrsUsageHistoryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UsageHistoryTypePtr, since we
	// might need GetBase(), which isn't defined in IUsageHistoryType
	const Dc1Ptr< Mp7JrsUsageHistoryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUsageHistoryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistallowCollection())
	{
		this->SetallowCollection(tmp->GetallowCollection());
	}
	else
	{
		InvalidateallowCollection();
	}
	}
	if (tmp->IsValidUserIdentifier())
	{
		// Dc1Factory::DeleteObject(m_UserIdentifier);
		this->SetUserIdentifier(Dc1Factory::CloneObject(tmp->GetUserIdentifier()));
	}
	else
	{
		InvalidateUserIdentifier();
	}
		// Dc1Factory::DeleteObject(m_UserActionHistory);
		this->SetUserActionHistory(Dc1Factory::CloneObject(tmp->GetUserActionHistory()));
}

Dc1NodePtr Mp7JrsUsageHistoryType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUsageHistoryType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsUsageHistoryType::GetBase() const
{
	return m_Base;
}

Mp7JrsuserChoiceType::Enumeration Mp7JrsUsageHistoryType::GetallowCollection() const
{
	if (this->ExistallowCollection()) {
		return m_allowCollection;
	} else {
		return m_allowCollection_Default;
	}
}

bool Mp7JrsUsageHistoryType::ExistallowCollection() const
{
	return m_allowCollection_Exist;
}
void Mp7JrsUsageHistoryType::SetallowCollection(Mp7JrsuserChoiceType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetallowCollection().");
	}
	m_allowCollection = item;
	m_allowCollection_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidateallowCollection()
{
	m_allowCollection_Exist = false;
}
Mp7JrsUserIdentifierPtr Mp7JrsUsageHistoryType::GetUserIdentifier() const
{
		return m_UserIdentifier;
}

// Element is optional
bool Mp7JrsUsageHistoryType::IsValidUserIdentifier() const
{
	return m_UserIdentifier_Exist;
}

Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr Mp7JrsUsageHistoryType::GetUserActionHistory() const
{
		return m_UserActionHistory;
}

void Mp7JrsUsageHistoryType::SetUserIdentifier(const Mp7JrsUserIdentifierPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetUserIdentifier().");
	}
	if (m_UserIdentifier != item || m_UserIdentifier_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_UserIdentifier);
		m_UserIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserIdentifier) m_UserIdentifier->SetParent(m_myself.getPointer());
		if (m_UserIdentifier && m_UserIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserIdentifierType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UserIdentifier->UseTypeAttribute = true;
		}
		m_UserIdentifier_Exist = true;
		if(m_UserIdentifier != Mp7JrsUserIdentifierPtr())
		{
			m_UserIdentifier->SetContentName(XMLString::transcode("UserIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidateUserIdentifier()
{
	m_UserIdentifier_Exist = false;
}
void Mp7JrsUsageHistoryType::SetUserActionHistory(const Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetUserActionHistory().");
	}
	if (m_UserActionHistory != item)
	{
		// Dc1Factory::DeleteObject(m_UserActionHistory);
		m_UserActionHistory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserActionHistory) m_UserActionHistory->SetParent(m_myself.getPointer());
		if(m_UserActionHistory != Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr())
		{
			m_UserActionHistory->SetContentName(XMLString::transcode("UserActionHistory"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUsageHistoryType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsUsageHistoryType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsUsageHistoryType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUsageHistoryType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsUsageHistoryType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsUsageHistoryType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUsageHistoryType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsUsageHistoryType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsUsageHistoryType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUsageHistoryType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUsageHistoryType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUsageHistoryType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUsageHistoryType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUsageHistoryType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUsageHistoryType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUsageHistoryType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUsageHistoryType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsUsageHistoryType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUsageHistoryType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUsageHistoryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("allowCollection")) == 0)
	{
		// allowCollection is simple attribute userChoiceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsuserChoiceType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserIdentifier")) == 0)
	{
		// UserIdentifier is simple element UserIdentifierType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUserIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserIdentifierType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserIdentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUserIdentifier(p, client);
					if((p = GetUserIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserActionHistory")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UserActionHistory is item of type UserActionHistoryType
		// in element collection UsageHistoryType_UserActionHistory_CollectionType
		
		context->Found = true;
		Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr coll = GetUserActionHistory();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUsageHistoryType_UserActionHistory_CollectionType; // FTT, check this
				SetUserActionHistory(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionHistoryType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserActionHistoryPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UsageHistoryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UsageHistoryType");
	}
	return result;
}

XMLCh * Mp7JrsUsageHistoryType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUsageHistoryType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUsageHistoryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UsageHistoryType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_allowCollection_Exist)
	{
	// Enumeration
	if(m_allowCollection != Mp7JrsuserChoiceType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsuserChoiceType::ToText(m_allowCollection);
		element->setAttributeNS(X(""), X("allowCollection"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_UserIdentifier != Mp7JrsUserIdentifierPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UserIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UserIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UserIdentifier->Serialize(doc, element, &elem);
	}
	if (m_UserActionHistory != Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UserActionHistory->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUsageHistoryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("allowCollection")))
	{
		this->SetallowCollection(Mp7JrsuserChoiceType::Parse(parent->getAttribute(X("allowCollection"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UserIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UserIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUserIdentifierType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUserIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UserActionHistory"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UserActionHistory")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr tmp = CreateUsageHistoryType_UserActionHistory_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUserActionHistory(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUsageHistoryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("UserIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUserIdentifierType; // FTT, check this
	}
	this->SetUserIdentifier(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UserActionHistory")) == 0))
  {
	Mp7JrsUsageHistoryType_UserActionHistory_CollectionPtr tmp = CreateUsageHistoryType_UserActionHistory_CollectionType; // FTT, check this
	this->SetUserActionHistory(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUsageHistoryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetUserIdentifier() != Dc1NodePtr())
		result.Insert(GetUserIdentifier());
	if (GetUserActionHistory() != Dc1NodePtr())
		result.Insert(GetUserActionHistory());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUsageHistoryType_ExtMethodImpl.h


