
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtImplInclude.h


#include "Mp7JrsbasicTimePointType.h"

Mp7JrsbasicTimePointType::Mp7JrsbasicTimePointType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrsbasicTimePointType::~Mp7JrsbasicTimePointType()
{
		Cleanup();
}

void Mp7JrsbasicTimePointType::Init()
{	   
		m_TimepointSign = 1;
		m_Year = -1;
		m_Month = -1;
		m_Day = -1;
		m_Date_Exist = false;
		m_Hours = -1;
		m_Minutes = -1;
		m_Seconds = -1;
		m_NumberOfFractions = -1;
		m_DecimalFractions = -1;
		m_Time_Exist = false;
		m_FractionsPerSecond = -1;
		m_TimezoneSign = 1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_Timezone_Exist = false;

// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtPropInit.h

}

void Mp7JrsbasicTimePointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtPropCleanup.h


}

void Mp7JrsbasicTimePointType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrsbasicTimePointType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrsbasicTimePointType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsbasicTimePointType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrsbasicTimePointType::GetTimepointSign() const
{
		return m_TimepointSign;
}

void Mp7JrsbasicTimePointType::SetTimepointSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTimepointSign().");
		}
		m_TimepointSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetYear() const
{
		return m_Year;
}

void Mp7JrsbasicTimePointType::SetYear(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetYear().");
		}
		m_Year = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetMonth() const
{
		return m_Month;
}

void Mp7JrsbasicTimePointType::SetMonth(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetMonth().");
		}
		m_Month = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetDay() const
{
		return m_Day;
}

void Mp7JrsbasicTimePointType::SetDay(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetDay().");
		}
		m_Day = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsbasicTimePointType::GetDate_Exist() const
{
		return m_Date_Exist;
}

void Mp7JrsbasicTimePointType::SetDate_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetDate_Exist().");
		}
		m_Date_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetHours() const
{
		return m_Hours;
}

void Mp7JrsbasicTimePointType::SetHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetHours().");
		}
		m_Hours = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetMinutes() const
{
		return m_Minutes;
}

void Mp7JrsbasicTimePointType::SetMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetMinutes().");
		}
		m_Minutes = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetSeconds() const
{
		return m_Seconds;
}

void Mp7JrsbasicTimePointType::SetSeconds(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetSeconds().");
		}
		m_Seconds = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetNumberOfFractions() const
{
		return m_NumberOfFractions;
}

void Mp7JrsbasicTimePointType::SetNumberOfFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetNumberOfFractions().");
		}
		m_NumberOfFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetDecimalFractions() const
{
		return m_DecimalFractions;
}

void Mp7JrsbasicTimePointType::SetDecimalFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetDecimalFractions().");
		}
		m_DecimalFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsbasicTimePointType::GetTime_Exist() const
{
		return m_Time_Exist;
}

void Mp7JrsbasicTimePointType::SetTime_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTime_Exist().");
		}
		m_Time_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetFractionsPerSecond() const
{
		return m_FractionsPerSecond;
}

void Mp7JrsbasicTimePointType::SetFractionsPerSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetFractionsPerSecond().");
		}
		m_FractionsPerSecond = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void Mp7JrsbasicTimePointType::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void Mp7JrsbasicTimePointType::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsbasicTimePointType::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void Mp7JrsbasicTimePointType::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsbasicTimePointType::GetTimezone_Exist() const
{
		return m_Timezone_Exist;
}

void Mp7JrsbasicTimePointType::SetTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsbasicTimePointType::SetTimezone_Exist().");
		}
		m_Timezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrsbasicTimePointType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_TimepointSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}

		if(m_Date_Exist)
		{
			if(m_Year >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Year);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Month >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Month);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Day >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Day);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_Time_Exist)
		{
			if(m_Hours >= 0)
			{
				Dc1Util::CatString(&buf, X("T"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Hours);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Minutes >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Minutes);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Seconds >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Seconds);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_NumberOfFractions >= 0)
			{
				tmp = Dc1Convert::BinToText(m_NumberOfFractions);
				Dc1Util::CatString(&buf, X(":"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_DecimalFractions >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_DecimalFractions);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("."));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_FractionsPerSecond >= 0)
		{
			tmp = Dc1Convert::BinToText(m_FractionsPerSecond);
			Dc1Util::CatString(&buf, X("F"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Timezone_Exist)
		{
			if(m_TimezoneSign > 0)
			{
				Dc1Util::CatString(&buf, X("+"));
			}
			else if(m_TimezoneSign < 0)
			{
				Dc1Util::CatString(&buf, X("-"));
			}
			XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X(":"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrsbasicTimePointType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^\\-?(\\d+(\\-\\d{2}(\\-\\d{2})?)?)?(T\\d{2}(:\\d{2}(:\\d{2}(:\\d+(\\.\\d{2})?)?)?)?)?(F\\d+)?((\\-|\\+)\\d{2}:\\d{2})?$");
		// This is for Xerces 3.1.1+
		if(r.matches(txt, &m))
		{
			for(int i = 0; i < m.getNoGroups(); i++)
			{
				int i1 = m.getStartPos(i);
				int i2 = Dc1Util::GetNextEndPos(m, i);
				if(i1 == i2) continue; // ignore missing optional or wrong group
				switch(i)
				{
					case 0:
							if(i1 >= 0 && i2 - i1 == 1)
							{
									// \\-?
									m_TimepointSign = -1;
							}
					break;

					case 1:
							if(i1 >= 0)
							{
									// (\\d+...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2);
									m_Year = Dc1Convert::TextToUnsignedInt(substring);
									m_Date_Exist = true;
							}
					break;

					case 2:
							if(i1 >= 0)
							{
									// (\\-\\d{2}...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_Month = Dc1Convert::TextToUnsignedInt(substring);
									m_Date_Exist = true;
							}
					break;

					case 3:
							if(i1 >= 0)
							{
									// (\\-\\d{2})?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_Day = Dc1Convert::TextToUnsignedInt(substring);
									m_Date_Exist = true;
							}
					break;

					case 4:
							if(i1 >= 0)
							{
									// (T\\d{2}...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_Hours = Dc1Convert::TextToUnsignedInt(substring);
									m_Time_Exist = true;
							}
					break;


					case 5:
							if(i1 >= 0)
							{
									// (:\\d{2}...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_Minutes = Dc1Convert::TextToUnsignedInt(substring);
									m_Time_Exist = true;
							}
					break;

					case 6:
							if(i1 >= 0)
							{
									// (:\\d{2}...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_Seconds = Dc1Convert::TextToUnsignedInt(substring);
									m_Time_Exist = true;
							}
					break;

					case 7:
							if(i1 >= 0)
							{
									// (:\\d+...)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_NumberOfFractions = Dc1Convert::TextToUnsignedInt(substring);
									m_Time_Exist = true;
							}
					break;

					case 8:
							if(i1 >= 0)
							{
									// (.\\d{2})?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_DecimalFractions = Dc1Convert::TextToUnsignedInt(substring);
									m_Time_Exist = true;
							}
					break;

					case 9:
							if(i1 >= 0)
							{
									// (F\\d+)?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
									m_FractionsPerSecond = Dc1Convert::TextToUnsignedInt(substring);
							}
					break;

					case 10:
							if(i1 >= 0)
							{
									// ((\\-|\\+)\\d{2}:\\d{2})?
									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 1);
									m_TimezoneSign = Dc1Convert::TextToSign(substring);

									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i1 + 3);
									m_TimezoneHours = Dc1Convert::TextToUnsignedInt(substring);

									XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 4, i2);
									m_TimezoneMinutes = Dc1Convert::TextToUnsignedInt(substring);

									m_Timezone_Exist = true;
							}
					break;

					default: return true;
					break;
				}
			}
		}
		else return false;
		return true;
}

bool Mp7JrsbasicTimePointType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsbasicTimePointType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsbasicTimePointType::ContentToString() const
{
	// Mp7JrsbasicTimePointType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsbasicTimePointType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:basicTimePointType")))));
		}

// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool Mp7JrsbasicTimePointType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file Mp7JrsbasicTimePointType_ExtMethodImpl.h








