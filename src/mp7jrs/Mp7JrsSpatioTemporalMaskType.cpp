
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtImplInclude.h


#include "Mp7JrsMaskType.h"
#include "Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionType.h"
#include "Mp7JrsSpatioTemporalMaskType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSpatioTemporalLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SubRegion

#include <assert.h>
IMp7JrsSpatioTemporalMaskType::IMp7JrsSpatioTemporalMaskType()
{

// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtPropInit.h

}

IMp7JrsSpatioTemporalMaskType::~IMp7JrsSpatioTemporalMaskType()
{
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtPropCleanup.h

}

Mp7JrsSpatioTemporalMaskType::Mp7JrsSpatioTemporalMaskType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpatioTemporalMaskType::~Mp7JrsSpatioTemporalMaskType()
{
	Cleanup();
}

void Mp7JrsSpatioTemporalMaskType::Init()
{
	// Init base
	m_Base = CreateMaskType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SubRegion = Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtMyPropInit.h

}

void Mp7JrsSpatioTemporalMaskType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SubRegion);
}

void Mp7JrsSpatioTemporalMaskType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpatioTemporalMaskTypePtr, since we
	// might need GetBase(), which isn't defined in ISpatioTemporalMaskType
	const Dc1Ptr< Mp7JrsSpatioTemporalMaskType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMaskPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSpatioTemporalMaskType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SubRegion);
		this->SetSubRegion(Dc1Factory::CloneObject(tmp->GetSubRegion()));
}

Dc1NodePtr Mp7JrsSpatioTemporalMaskType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSpatioTemporalMaskType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMaskType > Mp7JrsSpatioTemporalMaskType::GetBase() const
{
	return m_Base;
}

Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr Mp7JrsSpatioTemporalMaskType::GetSubRegion() const
{
		return m_SubRegion;
}

void Mp7JrsSpatioTemporalMaskType::SetSubRegion(const Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatioTemporalMaskType::SetSubRegion().");
	}
	if (m_SubRegion != item)
	{
		// Dc1Factory::DeleteObject(m_SubRegion);
		m_SubRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SubRegion) m_SubRegion->SetParent(m_myself.getPointer());
		if(m_SubRegion != Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr())
		{
			m_SubRegion->SetContentName(XMLString::transcode("SubRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpatioTemporalMaskType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SubRegion")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SubRegion is item of type SpatioTemporalLocatorType
		// in element collection SpatioTemporalMaskType_SubRegion_CollectionType
		
		context->Found = true;
		Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr coll = GetSubRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpatioTemporalMaskType_SubRegion_CollectionType; // FTT, check this
				SetSubRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatioTemporalLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpatioTemporalMaskType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpatioTemporalMaskType");
	}
	return result;
}

XMLCh * Mp7JrsSpatioTemporalMaskType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSpatioTemporalMaskType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSpatioTemporalMaskType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpatioTemporalMaskType"));
	// Element serialization:
	if (m_SubRegion != Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SubRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpatioTemporalMaskType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMaskType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SubRegion"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SubRegion")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr tmp = CreateSpatioTemporalMaskType_SubRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSubRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpatioTemporalMaskType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SubRegion")) == 0))
  {
	Mp7JrsSpatioTemporalMaskType_SubRegion_CollectionPtr tmp = CreateSpatioTemporalMaskType_SubRegion_CollectionType; // FTT, check this
	this->SetSubRegion(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpatioTemporalMaskType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSubRegion() != Dc1NodePtr())
		result.Insert(GetSubRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpatioTemporalMaskType_ExtMethodImpl.h


