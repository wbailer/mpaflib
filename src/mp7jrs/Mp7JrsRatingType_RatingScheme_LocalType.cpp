
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrsInlineTermDefinitionType_Name_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Definition_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsRatingType_RatingScheme_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsInlineTermDefinitionType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition
#include "Mp7JrsInlineTermDefinitionType_Term_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Term

#include <assert.h>
IMp7JrsRatingType_RatingScheme_LocalType::IMp7JrsRatingType_RatingScheme_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtPropInit.h

}

IMp7JrsRatingType_RatingScheme_LocalType::~IMp7JrsRatingType_RatingScheme_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtPropCleanup.h

}

Mp7JrsRatingType_RatingScheme_LocalType::Mp7JrsRatingType_RatingScheme_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRatingType_RatingScheme_LocalType::~Mp7JrsRatingType_RatingScheme_LocalType()
{
	Cleanup();
}

void Mp7JrsRatingType_RatingScheme_LocalType::Init()
{
	// Init base
	m_Base = CreateTermUseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_best = 0.0f; // Value
	m_best_Exist = false;
	m_worst = 0.0f; // Value
	m_worst_Exist = false;
	m_style = Mp7JrsRatingType_RatingScheme_style_LocalType::UninitializedEnumeration;



// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtMyPropInit.h

}

void Mp7JrsRatingType_RatingScheme_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsRatingType_RatingScheme_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RatingType_RatingScheme_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IRatingType_RatingScheme_LocalType
	const Dc1Ptr< Mp7JrsRatingType_RatingScheme_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTermUsePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsRatingType_RatingScheme_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existbest())
	{
		this->Setbest(tmp->Getbest());
	}
	else
	{
		Invalidatebest();
	}
	}
	{
	if (tmp->Existworst())
	{
		this->Setworst(tmp->Getworst());
	}
	else
	{
		Invalidateworst();
	}
	}
	{
		this->Setstyle(tmp->Getstyle());
	}
}

Dc1NodePtr Mp7JrsRatingType_RatingScheme_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsRatingType_RatingScheme_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTermUseType > Mp7JrsRatingType_RatingScheme_LocalType::GetBase() const
{
	return m_Base;
}

float Mp7JrsRatingType_RatingScheme_LocalType::Getbest() const
{
	return m_best;
}

bool Mp7JrsRatingType_RatingScheme_LocalType::Existbest() const
{
	return m_best_Exist;
}
void Mp7JrsRatingType_RatingScheme_LocalType::Setbest(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::Setbest().");
	}
	m_best = item;
	m_best_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRatingType_RatingScheme_LocalType::Invalidatebest()
{
	m_best_Exist = false;
}
float Mp7JrsRatingType_RatingScheme_LocalType::Getworst() const
{
	return m_worst;
}

bool Mp7JrsRatingType_RatingScheme_LocalType::Existworst() const
{
	return m_worst_Exist;
}
void Mp7JrsRatingType_RatingScheme_LocalType::Setworst(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::Setworst().");
	}
	m_worst = item;
	m_worst_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRatingType_RatingScheme_LocalType::Invalidateworst()
{
	m_worst_Exist = false;
}
Mp7JrsRatingType_RatingScheme_style_LocalType::Enumeration Mp7JrsRatingType_RatingScheme_LocalType::Getstyle() const
{
	return m_style;
}

void Mp7JrsRatingType_RatingScheme_LocalType::Setstyle(Mp7JrsRatingType_RatingScheme_style_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::Setstyle().");
	}
	m_style = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrstermReferencePtr Mp7JrsRatingType_RatingScheme_LocalType::Gethref() const
{
	return GetBase()->Gethref();
}

bool Mp7JrsRatingType_RatingScheme_LocalType::Existhref() const
{
	return GetBase()->Existhref();
}
void Mp7JrsRatingType_RatingScheme_LocalType::Sethref(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::Sethref().");
	}
	GetBase()->Sethref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRatingType_RatingScheme_LocalType::Invalidatehref()
{
	GetBase()->Invalidatehref();
}
Mp7JrsInlineTermDefinitionType_Name_CollectionPtr Mp7JrsRatingType_RatingScheme_LocalType::GetName() const
{
	return GetBase()->GetBase()->GetName();
}

Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr Mp7JrsRatingType_RatingScheme_LocalType::GetDefinition() const
{
	return GetBase()->GetBase()->GetDefinition();
}

Mp7JrsInlineTermDefinitionType_Term_CollectionPtr Mp7JrsRatingType_RatingScheme_LocalType::GetTerm() const
{
	return GetBase()->GetBase()->GetTerm();
}

void Mp7JrsRatingType_RatingScheme_LocalType::SetName(const Mp7JrsInlineTermDefinitionType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::SetName().");
	}
	GetBase()->GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRatingType_RatingScheme_LocalType::SetDefinition(const Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::SetDefinition().");
	}
	GetBase()->GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRatingType_RatingScheme_LocalType::SetTerm(const Mp7JrsInlineTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRatingType_RatingScheme_LocalType::SetTerm().");
	}
	GetBase()->GetBase()->SetTerm(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRatingType_RatingScheme_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("best")) == 0)
	{
		// best is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("worst")) == 0)
	{
		// worst is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("style")) == 0)
	{
		// style is simple attribute RatingType_RatingScheme_style_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsRatingType_RatingScheme_style_LocalType::Enumeration");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RatingType_RatingScheme_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RatingType_RatingScheme_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsRatingType_RatingScheme_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRatingType_RatingScheme_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsRatingType_RatingScheme_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RatingType_RatingScheme_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_best_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_best);
		element->setAttributeNS(X(""), X("best"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_worst_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_worst);
		element->setAttributeNS(X(""), X("worst"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
	// Enumeration
	if(m_style != Mp7JrsRatingType_RatingScheme_style_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsRatingType_RatingScheme_style_LocalType::ToText(m_style);
		element->setAttributeNS(X(""), X("style"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsRatingType_RatingScheme_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("best")))
	{
		// deserialize value type
		this->Setbest(Dc1Convert::TextToFloat(parent->getAttribute(X("best"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("worst")))
	{
		// deserialize value type
		this->Setworst(Dc1Convert::TextToFloat(parent->getAttribute(X("worst"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("style")))
	{
		this->Setstyle(Mp7JrsRatingType_RatingScheme_style_LocalType::Parse(parent->getAttribute(X("style"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsTermUseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRatingType_RatingScheme_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRatingType_RatingScheme_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRatingType_RatingScheme_LocalType_ExtMethodImpl.h


