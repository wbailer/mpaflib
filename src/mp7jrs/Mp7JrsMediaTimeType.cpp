
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaTimeType_ExtImplInclude.h


#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsMediaRelTimePointType.h"
#include "Mp7JrsMediaRelIncrTimePointType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsMediaIncrDurationType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include "Dc1Settings.h" // For TreatRelIncrTimeAsAbsolute
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsDSType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsImageLocatorType.h"
#include "Mp7JrsStreamLocatorType.h"
#include "Mp7JrsLogicalUnitLocatorType.h"

#include <assert.h>
IMp7JrsMediaTimeType::IMp7JrsMediaTimeType()
{

// begin extension included
// file Mp7JrsMediaTimeType_ExtPropInit.h

	_endTime = CreatemediaTimePointType;
// end extension included

}

IMp7JrsMediaTimeType::~IMp7JrsMediaTimeType()
{
// no includefile for extension defined 
// file Mp7JrsMediaTimeType_ExtPropCleanup.h

}

Mp7JrsMediaTimeType::Mp7JrsMediaTimeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaTimeType::~Mp7JrsMediaTimeType()
{
	Cleanup();
}

void Mp7JrsMediaTimeType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr(); // Pattern
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr(); // Class
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr(); // Class with content 
	m_MediaDuration = Mp7JrsmediaDurationPtr(); // Pattern
	m_MediaIncrDuration = Mp7JrsMediaIncrDurationPtr(); // Class with content 


// begin extension included
// file Mp7JrsMediaTimeType_ExtMyPropInit.h


// end extension included

}

void Mp7JrsMediaTimeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaTimeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	// Dc1Factory::DeleteObject(m_MediaDuration);
	// Dc1Factory::DeleteObject(m_MediaIncrDuration);
}

void Mp7JrsMediaTimeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaTimeTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaTimeType
	const Dc1Ptr< Mp7JrsMediaTimeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		this->SetMediaTimePoint(Dc1Factory::CloneObject(tmp->GetMediaTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		this->SetMediaRelTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		this->SetMediaRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelIncrTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaDuration);
		this->SetMediaDuration(Dc1Factory::CloneObject(tmp->GetMediaDuration()));
		// Dc1Factory::DeleteObject(m_MediaIncrDuration);
		this->SetMediaIncrDuration(Dc1Factory::CloneObject(tmp->GetMediaIncrDuration()));
}

Dc1NodePtr Mp7JrsMediaTimeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaTimeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsmediaTimePointPtr Mp7JrsMediaTimeType::GetMediaTimePoint() const
{

	// GetMediaTimePoint() behaves special
	if (Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute) {

		// FTT
		// This is AVDP code
		// We do a special treatment for MediaTime in AVDP
		// In MPEG-7 V2 MediaTime offers a choice of MediaTimePoint, MediaRelTimePoint and MediaRelIncrTimePoint
		// In AVDP we only have MediaRelIncrTimePoint (+ and optional MediaIncrDuration)

		// Since we don't want an extra lib for AVDP
		// the MediaTimePoint is used to retrieve the absolute timepoint
		// calculated from the MediaRelIncrTimePoint and the referenced mediaTimeUnit and mediaTimeBase.
		// mediaTimeBase points to a MediaLocator, that now contains an absolute TimePointType
		// so that MediaTimePoint (absolute) = MediaRelIncrTimePoint * mediaTimeUnit + mediaTimeBase (from MediaLocator)
		// This behaviour is only available if 
		// Mp7JrsSettings::GetInstance().TreatRelIncrTimeAsAbsolute is set to true.


		// Find mediaTimeUnit and mediaTimeBase - which is a MediaLocator -
		// get the final media time base from the medialocator
		// and map the MediaRelIncrTime to absolute time point.

		// Is there an AVDP-relative time value to be converted to absolute media time?
		if(GetMediaRelIncrTimePoint() != NULL) {
			return GetMediaRelIncrTimePoint()->GetMediaTimePoint();
		}
	}
		return m_MediaTimePoint;
}

Mp7JrsMediaRelTimePointPtr Mp7JrsMediaTimeType::GetMediaRelTimePoint() const
{
		return m_MediaRelTimePoint;
}

Mp7JrsMediaRelIncrTimePointPtr Mp7JrsMediaTimeType::GetMediaRelIncrTimePoint() const
{
		return m_MediaRelIncrTimePoint;
}

Mp7JrsmediaDurationPtr Mp7JrsMediaTimeType::GetMediaDuration() const
{
	// GetMediaDuration() behaves special
	if (Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute) {
		if (GetMediaIncrDuration() != 0) {
			return GetMediaIncrDuration()->GetMediaDuration();
		}
	}
		return m_MediaDuration;
}

Mp7JrsMediaIncrDurationPtr Mp7JrsMediaTimeType::GetMediaIncrDuration() const
{
		return m_MediaIncrDuration;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMediaTimeType::SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTimeType::SetMediaTimePoint().");
	}

	if (Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute) {

		// FTT
		// This is AVDP code
		// We do a special treatment for MediaTime in AVDP
		// In MPEG-7 V2 MediaTime offers a choice of MediaTimePoint, MediaRelTimePoint and MediaRelIncrTimePoint
		// In AVDP we only have MediaRelIncrTimePoint (+ and optional MediaIncrDuration)

		// Since we don't want an extra lib for AVDP
		// the MediaTimePoint is used to set the absolute timepoint
		// but converted to AVDP expected RelIncrTimePointType
		// For conversion we need the mediaTimeUnit and mediaTimeBase
		// that may come from a MediaLocator in the DOM above

		// so MediaRelIncrTimePoint and the referenced mediaTimeUnit and mediaTimeBase.
		// mediaTimeBase points to a MediaLocator, that now contains an absolute TimePointType
		// so that MediaTimePoint (absolute) = MediaRelIncrTimePoint * mediaTimeUnit + mediaTimeBase (from MediaLocator)
		// This behaviour is only available if 
		// Mp7JrsSettings::GetInstance().TreatRelIncrTimeAsAbsolute is set to true.


		// Find mediaTimeUnit and mediaTimeBase - which is a MediaLocator -
		// get the final media time base from the medialocator
		// and map the MediaRelIncrTime to absolute time point.
		if (item == 0) // setting an empty item means to clear the node.
			SetMediaRelIncrTimePoint(Dc1NodePtr());
		else {
			if (GetMediaRelIncrTimePoint() == 0)
				SetMediaRelIncrTimePoint(CreateMediaRelIncrTimePointType); // FTT, check this
			GetMediaRelIncrTimePoint()->SetMediaTimePoint(item);
		}
		return;
	}
	if (m_MediaTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		m_MediaTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimePoint) m_MediaTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaTimePoint && m_MediaTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
		{
			m_MediaTimePoint->SetContentName(XMLString::transcode("MediaTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMediaTimeType::SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTimeType::SetMediaRelTimePoint().");
	}
	if (m_MediaRelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		m_MediaRelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelTimePoint) m_MediaRelTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelTimePoint && m_MediaRelTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
		{
			m_MediaRelTimePoint->SetContentName(XMLString::transcode("MediaRelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMediaTimeType::SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTimeType::SetMediaRelIncrTimePoint().");
	}
	if (m_MediaRelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		m_MediaRelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelIncrTimePoint) m_MediaRelIncrTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelIncrTimePoint && m_MediaRelIncrTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelIncrTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
		{
			m_MediaRelIncrTimePoint->SetContentName(XMLString::transcode("MediaRelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMediaTimeType::SetMediaDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTimeType::SetMediaDuration().");
	}

	if (Dc1Settings::GetInstance().TreatRelIncrTimeAsAbsolute) {
		if (item == 0) // setting an empty item means to clear the node.
			SetMediaIncrDuration(Dc1NodePtr());
		else {
			if (GetMediaIncrDuration() == 0)
				SetMediaIncrDuration(CreateMediaIncrDurationType); // FTT, check this
			GetMediaIncrDuration()->SetMediaDuration(item);
		}
		return;
	}
	if (m_MediaDuration != item)
	{
		// Dc1Factory::DeleteObject(m_MediaDuration);
		m_MediaDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaDuration) m_MediaDuration->SetParent(m_myself.getPointer());
		if (m_MediaDuration && m_MediaDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaDuration->UseTypeAttribute = true;
		}
		if(m_MediaDuration != Mp7JrsmediaDurationPtr())
		{
			m_MediaDuration->SetContentName(XMLString::transcode("MediaDuration"));
		}
	// Dc1Factory::DeleteObject(m_MediaIncrDuration);
	m_MediaIncrDuration = Mp7JrsMediaIncrDurationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMediaTimeType::SetMediaIncrDuration(const Mp7JrsMediaIncrDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTimeType::SetMediaIncrDuration().");
	}
	if (m_MediaIncrDuration != item)
	{
		// Dc1Factory::DeleteObject(m_MediaIncrDuration);
		m_MediaIncrDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaIncrDuration) m_MediaIncrDuration->SetParent(m_myself.getPointer());
		if (m_MediaIncrDuration && m_MediaIncrDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaIncrDuration->UseTypeAttribute = true;
		}
		if(m_MediaIncrDuration != Mp7JrsMediaIncrDurationPtr())
		{
			m_MediaIncrDuration->SetContentName(XMLString::transcode("MediaIncrDuration"));
		}
	// Dc1Factory::DeleteObject(m_MediaDuration);
	m_MediaDuration = Mp7JrsmediaDurationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaTimeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTimePoint")) == 0)
	{
		// MediaTimePoint is simple element mediaTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimePoint(p, client);
					if((p = GetMediaTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelTimePoint")) == 0)
	{
		// MediaRelTimePoint is simple element MediaRelTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelTimePoint(p, client);
					if((p = GetMediaRelTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelIncrTimePoint")) == 0)
	{
		// MediaRelIncrTimePoint is simple element MediaRelIncrTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelIncrTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelIncrTimePoint(p, client);
					if((p = GetMediaRelIncrTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaDuration")) == 0)
	{
		// MediaDuration is simple element mediaDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaDuration(p, client);
					if((p = GetMediaDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaIncrDuration")) == 0)
	{
		// MediaIncrDuration is simple element MediaIncrDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaIncrDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaIncrDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaIncrDuration(p, client);
					if((p = GetMediaIncrDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaTimeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaTimeType");
	}
	return result;
}

XMLCh * Mp7JrsMediaTimeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaTimeType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaTimeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaTimeType"));
	// Element serialization:
	if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MediaTimePoint");
//		m_MediaTimePoint->SetContentName(contentname);
		m_MediaTimePoint->UseTypeAttribute = this->UseTypeAttribute;
		m_MediaTimePoint->Serialize(doc, element);
	}
	// Class
	
	if (m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelTimePoint->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelIncrTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelIncrTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelIncrTimePoint->Serialize(doc, element, &elem);
	}
	if(m_MediaDuration != Mp7JrsmediaDurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MediaDuration");
//		m_MediaDuration->SetContentName(contentname);
		m_MediaDuration->UseTypeAttribute = this->UseTypeAttribute;
		m_MediaDuration->Serialize(doc, element);
	}
	// Class with content		
	
	if (m_MediaIncrDuration != Mp7JrsMediaIncrDurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaIncrDuration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaIncrDuration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaIncrDuration->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaTimeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimePoint")) == 0))
		{
			Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelIncrTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelIncrTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelIncrTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MediaDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaDuration")) == 0))
		{
			Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaIncrDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaIncrDuration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaIncrDurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaIncrDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMediaTimeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaTimeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaTimePointType; // FTT, check this
	}
	this->SetMediaTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelTimePointType; // FTT, check this
	}
	this->SetMediaRelTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelIncrTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelIncrTimePointType; // FTT, check this
	}
	this->SetMediaRelIncrTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaDurationType; // FTT, check this
	}
	this->SetMediaDuration(child);
  }
  if (XMLString::compareString(elementname, X("MediaIncrDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaIncrDurationType; // FTT, check this
	}
	this->SetMediaIncrDuration(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaTimeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaTimePoint());
	if (GetMediaRelTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelTimePoint());
	if (GetMediaRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelIncrTimePoint());
	if (GetMediaDuration() != Dc1NodePtr())
		result.Insert(GetMediaDuration());
	if (GetMediaIncrDuration() != Dc1NodePtr())
		result.Insert(GetMediaIncrDuration());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsMediaTimeType_ExtMethodImpl.h

Mp7JrsmediaTimePointPtr IMp7JrsMediaTimeType::GetBeginTime() {
	return GetMediaTimePoint();
}

void IMp7JrsMediaTimeType::SetBeginTime(const Mp7JrsmediaTimePointPtr &beginTime, Dc1ClientID client)
{
	// FIXME: generated Setter methods store the passed object, not a
	// clone of it.  Should we do the same here?
	SetMediaTimePoint(Mp7JrsmediaTimePointPtr(Dc1Factory::CloneObject(beginTime)),client);
}

Mp7JrsmediaTimePointPtr IMp7JrsMediaTimeType::GetEndTime() {

	Dc1Factory::DeleteObject(_endTime);

	_endTime = (Mp7JrsmediaTimePointPtr) Dc1Factory::CloneObject(GetBeginTime());
	
	if (GetMediaDuration()) {
		_endTime->Add(GetMediaDuration());
	}
	return _endTime;
}

void IMp7JrsMediaTimeType::SetEndTime(const Mp7JrsmediaTimePointPtr &endTime, Dc1ClientID client)
{
	Dc1Factory::DeleteObject(_endTime);
	_endTime = Mp7JrsmediaTimePointPtr(Dc1Factory::CloneObject(endTime));
	SetMediaDuration(_endTime->Subtract(GetMediaTimePoint()),client);
}

void IMp7JrsMediaTimeType::GetTime(Mp7JrsmediaTimePointPtr& beginTime, Mp7JrsmediaTimePointPtr& endTime) {
	beginTime = GetBeginTime();
	endTime = GetEndTime();
}

void IMp7JrsMediaTimeType::SetTime(const Mp7JrsmediaTimePointPtr &beginTime, const Mp7JrsmediaTimePointPtr &endTime, Dc1ClientID client) {
	SetBeginTime(beginTime,client);
	SetEndTime(endTime,client);
}

bool IMp7JrsMediaTimeType::IsInside(const Mp7JrsmediaTimePointPtr &timePoint){

	Mp7JrsmediaTimePointPtr endTime = GetEndTime();
	Mp7JrsmediaTimePointPtr beginTime = GetBeginTime();

	if ((timePoint->GetTotalNrFractions() >= beginTime->GetTotalNrFractions())
		&& (timePoint->GetTotalNrFractions() <= endTime->GetTotalNrFractions())) {
		return true;
	}
	else {
		return false;
	}

}

void Mp7JrsMediaTimeType::resolveBaseAndUnit(Mp7JrsmediaTimePointPtr &base, Mp7JrsmediaDurationPtr &unit) {
	// Is there a related mediaTimeBase out there?
	Mp7JrsxPathRefPtr pathToMediaLocator;
	Dc1NodePtr parent;
	Dc1NodePtr targetForBase; // Target has to be a MediaLocator (customized to contain a MediaTimePoint)

	bool resolvedBase = false;
	bool resolvedUnit = false;
	// Is there a related mediaTimeUnit out there?
	Mp7JrsmediaDurationPtr mtu; // NULL 
	if (GetMediaRelIncrTimePoint() != 0 && GetMediaRelIncrTimePoint()->ExistmediaTimeUnit()) {
		mtu = GetMediaRelIncrTimePoint()->GetmediaTimeUnit();
		resolvedUnit = true;
	}
	if (GetMediaRelIncrTimePoint() != 0 && GetMediaRelIncrTimePoint()->ExistmediaTimeBase()) {
		pathToMediaLocator = GetMediaRelIncrTimePoint()->GetmediaTimeBase();
		XMLCh* path = pathToMediaLocator->GetContent();

		Dc1NodeEnum e1 = GetMediaRelIncrTimePoint()->GetFromXPath(path);
		if(e1.HasNext()) {
			targetForBase = e1.GetNext();
			resolvedBase = true;
		}
	}
	if (!resolvedBase || !resolvedUnit) { // Search in DOM
		parent = GetParent();
		while (parent && (!resolvedBase || !resolvedUnit)) {
			Mp7JrsDSPtr dstype = parent;
			if (dstype != 0) {
				if (!resolvedBase && dstype->ExistmediaTimeBase()) {
					pathToMediaLocator = dstype->GetmediaTimeBase();
					XMLCh* path = pathToMediaLocator->GetContent();
					Dc1NodeEnum e1 = dstype->GetFromXPath(path);
					if(e1.HasNext()) {
						targetForBase = e1.GetNext();
						resolvedBase = true;
					}
				}
				if (!resolvedUnit && dstype->ExistmediaTimeUnit()) {
					mtu = dstype->GetmediaTimeUnit();
					resolvedUnit = true;
				}
			}
			parent = parent->GetParent();
		}
	}

	// Check existence of mediaTimeUnit
	if(mtu != 0) {
		unit = mtu;
		base = CreatemediaTimePointType;
		base->SetFractionsPerSecond(unit->GetFractionsPerSecond());
		base->SetNumberOfFractions(0);
	}

	// Resolve MediaLocator which contains the mediatimepoint - the absolute timebase
	// If not found, assume timebase starts with 0.
	if (targetForBase != 0) {
		// Check if target has a TemporalSegmentLocator with deserialized offset
		{
			Mp7JrsTemporalSegmentLocatorPtr mediaLocator = targetForBase;
			// AVDP access to either public MediaTime.MediaRelIncrTimePoint.mediaTimeBase or private MediaTimeBase
			if(mediaLocator != 0) {
				Mp7JrsMediaTimePtr mt = mediaLocator->GetMediaTime();
				if(mt != 0) {
					Mp7JrsMediaRelIncrTimePointPtr rit = mt->GetMediaRelIncrTimePoint();
					if(rit != 0) {
						int initialoffset = rit->GetContent();
						base->SetTotalNrFractions(initialoffset);
						return;
					}
				}
			}
		}
		//{
		//	Mp7JrsImageLocatorPtr mediaLocator = targetForBase;
		//	if(mediaLocator != 0 && mediaLocator->GetMediaTimeBase() != 0) 
		//		base = mediaLocator->GetMediaTimeBase();
		//}
		//{
		//	Mp7JrsStreamLocatorPtr mediaLocator = targetForBase;
		//	if(mediaLocator != 0 && mediaLocator->GetMediaTimeBase() != 0) 
		//		base = mediaLocator->GetMediaTimeBase();
		//}
		//{
		//	Mp7JrsLogicalUnitLocatorPtr mediaLocator = targetForBase;
		//	if(mediaLocator != 0 && mediaLocator->GetMediaTimeBase() != 0) 
		//		base = mediaLocator->GetMediaTimeBase();
		//}

		// Check if target has a MediaTimeType set programmatically
		{
			Mp7JrsMediaLocatorPtr mediaLocator = targetForBase;
			// AVDP access to private MediaTimeBase
			if(mediaLocator != 0 && mediaLocator->GetMediaTimeBase() != 0) 
				base = mediaLocator->GetMediaTimeBase();
		}
	}
}
// end extension included


