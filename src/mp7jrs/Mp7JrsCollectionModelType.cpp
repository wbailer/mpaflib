
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtImplInclude.h


#include "Mp7JrsAnalyticModelType.h"
#include "Mp7JrsCollectionModelType_CollectionType.h"
#include "Mp7JrsAnalyticModelType_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsCollectionModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAnalyticModelType_LocalType.h" // Choice collection Label
#include "Mp7JrsAnalyticModelType_Label_LocalType.h" // Choice collection element Label
#include "Mp7JrsAnalyticModelType_Semantics_LocalType.h" // Choice collection element Semantics
#include "Mp7JrsCollectionModelType_LocalType.h" // Choice collection Collection
#include "Mp7JrsCollectionType.h" // Choice collection element Collection
#include "Mp7JrsReferenceType.h" // Choice collection element CollectionRef

#include <assert.h>
IMp7JrsCollectionModelType::IMp7JrsCollectionModelType()
{

// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtPropInit.h

}

IMp7JrsCollectionModelType::~IMp7JrsCollectionModelType()
{
// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtPropCleanup.h

}

Mp7JrsCollectionModelType::Mp7JrsCollectionModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCollectionModelType::~Mp7JrsCollectionModelType()
{
	Cleanup();
}

void Mp7JrsCollectionModelType::Init()
{
	// Init base
	m_Base = CreateAnalyticModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_CollectionModelType_LocalType = Mp7JrsCollectionModelType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtMyPropInit.h

}

void Mp7JrsCollectionModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_CollectionModelType_LocalType);
}

void Mp7JrsCollectionModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CollectionModelTypePtr, since we
	// might need GetBase(), which isn't defined in ICollectionModelType
	const Dc1Ptr< Mp7JrsCollectionModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAnalyticModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCollectionModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_CollectionModelType_LocalType);
		this->SetCollectionModelType_LocalType(Dc1Factory::CloneObject(tmp->GetCollectionModelType_LocalType()));
}

Dc1NodePtr Mp7JrsCollectionModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCollectionModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAnalyticModelType > Mp7JrsCollectionModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsCollectionModelType_CollectionPtr Mp7JrsCollectionModelType::GetCollectionModelType_LocalType() const
{
		return m_CollectionModelType_LocalType;
}

void Mp7JrsCollectionModelType::SetCollectionModelType_LocalType(const Mp7JrsCollectionModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SetCollectionModelType_LocalType().");
	}
	if (m_CollectionModelType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_CollectionModelType_LocalType);
		m_CollectionModelType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CollectionModelType_LocalType) m_CollectionModelType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsAnalyticModelType_function_LocalType::Enumeration Mp7JrsCollectionModelType::Getfunction() const
{
	return GetBase()->Getfunction();
}

bool Mp7JrsCollectionModelType::Existfunction() const
{
	return GetBase()->Existfunction();
}
void Mp7JrsCollectionModelType::Setfunction(Mp7JrsAnalyticModelType_function_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::Setfunction().");
	}
	GetBase()->Setfunction(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::Invalidatefunction()
{
	GetBase()->Invalidatefunction();
}
Mp7JrsAnalyticModelType_CollectionPtr Mp7JrsCollectionModelType::GetAnalyticModelType_LocalType() const
{
	return GetBase()->GetAnalyticModelType_LocalType();
}

void Mp7JrsCollectionModelType::SetAnalyticModelType_LocalType(const Mp7JrsAnalyticModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SetAnalyticModelType_LocalType().");
	}
	GetBase()->SetAnalyticModelType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsCollectionModelType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsCollectionModelType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsCollectionModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsCollectionModelType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsCollectionModelType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsCollectionModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsCollectionModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsCollectionModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsCollectionModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsCollectionModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsCollectionModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsCollectionModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsCollectionModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsCollectionModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsCollectionModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsCollectionModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsCollectionModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsCollectionModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsCollectionModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsCollectionModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsCollectionModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCollectionModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsCollectionModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsCollectionModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCollectionModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCollectionModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Collection")) == 0)
	{
		// Collection is contained in itemtype CollectionModelType_LocalType
		// in choice collection CollectionModelType_CollectionType

		context->Found = true;
		Mp7JrsCollectionModelType_CollectionPtr coll = GetCollectionModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCollectionModelType_CollectionType; // FTT, check this
				SetCollectionModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsCollectionModelType_LocalPtr item = CreateCollectionModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->SetCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CollectionRef")) == 0)
	{
		// CollectionRef is contained in itemtype CollectionModelType_LocalType
		// in choice collection CollectionModelType_CollectionType

		context->Found = true;
		Mp7JrsCollectionModelType_CollectionPtr coll = GetCollectionModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCollectionModelType_CollectionType; // FTT, check this
				SetCollectionModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsCollectionModelType_LocalPtr item = CreateCollectionModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->SetCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsCollectionModelType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CollectionModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CollectionModelType");
	}
	return result;
}

XMLCh * Mp7JrsCollectionModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsCollectionModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsCollectionModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CollectionModelType"));
	// Element serialization:
	if (m_CollectionModelType_LocalType != Mp7JrsCollectionModelType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CollectionModelType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCollectionModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAnalyticModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Collection
			Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Collection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:CollectionRef
			Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsCollectionModelType_CollectionPtr tmp = CreateCollectionModelType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCollectionModelType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCollectionModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Collection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("CollectionRef")) == 0)
	)
  {
	Mp7JrsCollectionModelType_CollectionPtr tmp = CreateCollectionModelType_CollectionType; // FTT, check this
	this->SetCollectionModelType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCollectionModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCollectionModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetCollectionModelType_LocalType());
	if (GetAnalyticModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetAnalyticModelType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCollectionModelType_ExtMethodImpl.h


