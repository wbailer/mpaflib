
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsClassificationType_Form_CollectionType.h"
#include "Mp7JrsClassificationType_Genre_CollectionType.h"
#include "Mp7JrsClassificationType_Subject_CollectionType.h"
#include "Mp7JrsClassificationType_Purpose_CollectionType.h"
#include "Mp7JrsClassificationType_Language_CollectionType.h"
#include "Mp7JrsClassificationType_CaptionLanguage_CollectionType.h"
#include "Mp7JrsClassificationType_SignLanguage_CollectionType.h"
#include "Mp7JrsClassificationType_Release_CollectionType.h"
#include "Mp7JrsClassificationType_Target_LocalType.h"
#include "Mp7JrsClassificationType_ParentalGuidance_CollectionType.h"
#include "Mp7JrsClassificationType_MediaReview_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsClassificationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Form
#include "Mp7JrsClassificationType_Genre_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Genre
#include "Mp7JrsExtendedLanguageType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Language
#include "Mp7JrsClassificationType_CaptionLanguage_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CaptionLanguage
#include "Mp7JrsClassificationType_SignLanguage_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SignLanguage
#include "Mp7JrsClassificationType_Release_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Release
#include "Mp7JrsParentalGuidanceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ParentalGuidance
#include "Mp7JrsMediaReviewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaReview

#include <assert.h>
IMp7JrsClassificationType::IMp7JrsClassificationType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtPropInit.h

}

IMp7JrsClassificationType::~IMp7JrsClassificationType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtPropCleanup.h

}

Mp7JrsClassificationType::Mp7JrsClassificationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationType::~Mp7JrsClassificationType()
{
	Cleanup();
}

void Mp7JrsClassificationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Form = Mp7JrsClassificationType_Form_CollectionPtr(); // Collection
	m_Genre = Mp7JrsClassificationType_Genre_CollectionPtr(); // Collection
	m_Subject = Mp7JrsClassificationType_Subject_CollectionPtr(); // Collection
	m_Purpose = Mp7JrsClassificationType_Purpose_CollectionPtr(); // Collection
	m_Language = Mp7JrsClassificationType_Language_CollectionPtr(); // Collection
	m_CaptionLanguage = Mp7JrsClassificationType_CaptionLanguage_CollectionPtr(); // Collection
	m_SignLanguage = Mp7JrsClassificationType_SignLanguage_CollectionPtr(); // Collection
	m_Release = Mp7JrsClassificationType_Release_CollectionPtr(); // Collection
	m_Target = Mp7JrsClassificationType_Target_LocalPtr(); // Class
	m_Target_Exist = false;
	m_ParentalGuidance = Mp7JrsClassificationType_ParentalGuidance_CollectionPtr(); // Collection
	m_MediaReview = Mp7JrsClassificationType_MediaReview_CollectionPtr(); // Collection
	m_Version = Mp7JrsTextAnnotationPtr(); // Class
	m_Version_Exist = false;


// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtMyPropInit.h

}

void Mp7JrsClassificationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Form);
	// Dc1Factory::DeleteObject(m_Genre);
	// Dc1Factory::DeleteObject(m_Subject);
	// Dc1Factory::DeleteObject(m_Purpose);
	// Dc1Factory::DeleteObject(m_Language);
	// Dc1Factory::DeleteObject(m_CaptionLanguage);
	// Dc1Factory::DeleteObject(m_SignLanguage);
	// Dc1Factory::DeleteObject(m_Release);
	// Dc1Factory::DeleteObject(m_Target);
	// Dc1Factory::DeleteObject(m_ParentalGuidance);
	// Dc1Factory::DeleteObject(m_MediaReview);
	// Dc1Factory::DeleteObject(m_Version);
}

void Mp7JrsClassificationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationType
	const Dc1Ptr< Mp7JrsClassificationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Form);
		this->SetForm(Dc1Factory::CloneObject(tmp->GetForm()));
		// Dc1Factory::DeleteObject(m_Genre);
		this->SetGenre(Dc1Factory::CloneObject(tmp->GetGenre()));
		// Dc1Factory::DeleteObject(m_Subject);
		this->SetSubject(Dc1Factory::CloneObject(tmp->GetSubject()));
		// Dc1Factory::DeleteObject(m_Purpose);
		this->SetPurpose(Dc1Factory::CloneObject(tmp->GetPurpose()));
		// Dc1Factory::DeleteObject(m_Language);
		this->SetLanguage(Dc1Factory::CloneObject(tmp->GetLanguage()));
		// Dc1Factory::DeleteObject(m_CaptionLanguage);
		this->SetCaptionLanguage(Dc1Factory::CloneObject(tmp->GetCaptionLanguage()));
		// Dc1Factory::DeleteObject(m_SignLanguage);
		this->SetSignLanguage(Dc1Factory::CloneObject(tmp->GetSignLanguage()));
		// Dc1Factory::DeleteObject(m_Release);
		this->SetRelease(Dc1Factory::CloneObject(tmp->GetRelease()));
	if (tmp->IsValidTarget())
	{
		// Dc1Factory::DeleteObject(m_Target);
		this->SetTarget(Dc1Factory::CloneObject(tmp->GetTarget()));
	}
	else
	{
		InvalidateTarget();
	}
		// Dc1Factory::DeleteObject(m_ParentalGuidance);
		this->SetParentalGuidance(Dc1Factory::CloneObject(tmp->GetParentalGuidance()));
		// Dc1Factory::DeleteObject(m_MediaReview);
		this->SetMediaReview(Dc1Factory::CloneObject(tmp->GetMediaReview()));
	if (tmp->IsValidVersion())
	{
		// Dc1Factory::DeleteObject(m_Version);
		this->SetVersion(Dc1Factory::CloneObject(tmp->GetVersion()));
	}
	else
	{
		InvalidateVersion();
	}
}

Dc1NodePtr Mp7JrsClassificationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsClassificationType::GetBase() const
{
	return m_Base;
}

Mp7JrsClassificationType_Form_CollectionPtr Mp7JrsClassificationType::GetForm() const
{
		return m_Form;
}

Mp7JrsClassificationType_Genre_CollectionPtr Mp7JrsClassificationType::GetGenre() const
{
		return m_Genre;
}

Mp7JrsClassificationType_Subject_CollectionPtr Mp7JrsClassificationType::GetSubject() const
{
		return m_Subject;
}

Mp7JrsClassificationType_Purpose_CollectionPtr Mp7JrsClassificationType::GetPurpose() const
{
		return m_Purpose;
}

Mp7JrsClassificationType_Language_CollectionPtr Mp7JrsClassificationType::GetLanguage() const
{
		return m_Language;
}

Mp7JrsClassificationType_CaptionLanguage_CollectionPtr Mp7JrsClassificationType::GetCaptionLanguage() const
{
		return m_CaptionLanguage;
}

Mp7JrsClassificationType_SignLanguage_CollectionPtr Mp7JrsClassificationType::GetSignLanguage() const
{
		return m_SignLanguage;
}

Mp7JrsClassificationType_Release_CollectionPtr Mp7JrsClassificationType::GetRelease() const
{
		return m_Release;
}

Mp7JrsClassificationType_Target_LocalPtr Mp7JrsClassificationType::GetTarget() const
{
		return m_Target;
}

// Element is optional
bool Mp7JrsClassificationType::IsValidTarget() const
{
	return m_Target_Exist;
}

Mp7JrsClassificationType_ParentalGuidance_CollectionPtr Mp7JrsClassificationType::GetParentalGuidance() const
{
		return m_ParentalGuidance;
}

Mp7JrsClassificationType_MediaReview_CollectionPtr Mp7JrsClassificationType::GetMediaReview() const
{
		return m_MediaReview;
}

Mp7JrsTextAnnotationPtr Mp7JrsClassificationType::GetVersion() const
{
		return m_Version;
}

// Element is optional
bool Mp7JrsClassificationType::IsValidVersion() const
{
	return m_Version_Exist;
}

void Mp7JrsClassificationType::SetForm(const Mp7JrsClassificationType_Form_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetForm().");
	}
	if (m_Form != item)
	{
		// Dc1Factory::DeleteObject(m_Form);
		m_Form = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Form) m_Form->SetParent(m_myself.getPointer());
		if(m_Form != Mp7JrsClassificationType_Form_CollectionPtr())
		{
			m_Form->SetContentName(XMLString::transcode("Form"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetGenre(const Mp7JrsClassificationType_Genre_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetGenre().");
	}
	if (m_Genre != item)
	{
		// Dc1Factory::DeleteObject(m_Genre);
		m_Genre = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Genre) m_Genre->SetParent(m_myself.getPointer());
		if(m_Genre != Mp7JrsClassificationType_Genre_CollectionPtr())
		{
			m_Genre->SetContentName(XMLString::transcode("Genre"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetSubject(const Mp7JrsClassificationType_Subject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetSubject().");
	}
	if (m_Subject != item)
	{
		// Dc1Factory::DeleteObject(m_Subject);
		m_Subject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Subject) m_Subject->SetParent(m_myself.getPointer());
		if(m_Subject != Mp7JrsClassificationType_Subject_CollectionPtr())
		{
			m_Subject->SetContentName(XMLString::transcode("Subject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetPurpose(const Mp7JrsClassificationType_Purpose_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetPurpose().");
	}
	if (m_Purpose != item)
	{
		// Dc1Factory::DeleteObject(m_Purpose);
		m_Purpose = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Purpose) m_Purpose->SetParent(m_myself.getPointer());
		if(m_Purpose != Mp7JrsClassificationType_Purpose_CollectionPtr())
		{
			m_Purpose->SetContentName(XMLString::transcode("Purpose"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetLanguage(const Mp7JrsClassificationType_Language_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetLanguage().");
	}
	if (m_Language != item)
	{
		// Dc1Factory::DeleteObject(m_Language);
		m_Language = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Language) m_Language->SetParent(m_myself.getPointer());
		if(m_Language != Mp7JrsClassificationType_Language_CollectionPtr())
		{
			m_Language->SetContentName(XMLString::transcode("Language"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetCaptionLanguage(const Mp7JrsClassificationType_CaptionLanguage_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetCaptionLanguage().");
	}
	if (m_CaptionLanguage != item)
	{
		// Dc1Factory::DeleteObject(m_CaptionLanguage);
		m_CaptionLanguage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CaptionLanguage) m_CaptionLanguage->SetParent(m_myself.getPointer());
		if(m_CaptionLanguage != Mp7JrsClassificationType_CaptionLanguage_CollectionPtr())
		{
			m_CaptionLanguage->SetContentName(XMLString::transcode("CaptionLanguage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetSignLanguage(const Mp7JrsClassificationType_SignLanguage_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetSignLanguage().");
	}
	if (m_SignLanguage != item)
	{
		// Dc1Factory::DeleteObject(m_SignLanguage);
		m_SignLanguage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SignLanguage) m_SignLanguage->SetParent(m_myself.getPointer());
		if(m_SignLanguage != Mp7JrsClassificationType_SignLanguage_CollectionPtr())
		{
			m_SignLanguage->SetContentName(XMLString::transcode("SignLanguage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetRelease(const Mp7JrsClassificationType_Release_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetRelease().");
	}
	if (m_Release != item)
	{
		// Dc1Factory::DeleteObject(m_Release);
		m_Release = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Release) m_Release->SetParent(m_myself.getPointer());
		if(m_Release != Mp7JrsClassificationType_Release_CollectionPtr())
		{
			m_Release->SetContentName(XMLString::transcode("Release"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetTarget(const Mp7JrsClassificationType_Target_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetTarget().");
	}
	if (m_Target != item || m_Target_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Target);
		m_Target = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Target) m_Target->SetParent(m_myself.getPointer());
		if (m_Target && m_Target->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Target->UseTypeAttribute = true;
		}
		m_Target_Exist = true;
		if(m_Target != Mp7JrsClassificationType_Target_LocalPtr())
		{
			m_Target->SetContentName(XMLString::transcode("Target"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidateTarget()
{
	m_Target_Exist = false;
}
void Mp7JrsClassificationType::SetParentalGuidance(const Mp7JrsClassificationType_ParentalGuidance_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetParentalGuidance().");
	}
	if (m_ParentalGuidance != item)
	{
		// Dc1Factory::DeleteObject(m_ParentalGuidance);
		m_ParentalGuidance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ParentalGuidance) m_ParentalGuidance->SetParent(m_myself.getPointer());
		if(m_ParentalGuidance != Mp7JrsClassificationType_ParentalGuidance_CollectionPtr())
		{
			m_ParentalGuidance->SetContentName(XMLString::transcode("ParentalGuidance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetMediaReview(const Mp7JrsClassificationType_MediaReview_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetMediaReview().");
	}
	if (m_MediaReview != item)
	{
		// Dc1Factory::DeleteObject(m_MediaReview);
		m_MediaReview = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaReview) m_MediaReview->SetParent(m_myself.getPointer());
		if(m_MediaReview != Mp7JrsClassificationType_MediaReview_CollectionPtr())
		{
			m_MediaReview->SetContentName(XMLString::transcode("MediaReview"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::SetVersion(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetVersion().");
	}
	if (m_Version != item || m_Version_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Version);
		m_Version = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Version) m_Version->SetParent(m_myself.getPointer());
		if (m_Version && m_Version->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Version->UseTypeAttribute = true;
		}
		m_Version_Exist = true;
		if(m_Version != Mp7JrsTextAnnotationPtr())
		{
			m_Version->SetContentName(XMLString::transcode("Version"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidateVersion()
{
	m_Version_Exist = false;
}
XMLCh * Mp7JrsClassificationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsClassificationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsClassificationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsClassificationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsClassificationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsClassificationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsClassificationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsClassificationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsClassificationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsClassificationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsClassificationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsClassificationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsClassificationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsClassificationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsClassificationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Form")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Form is item of type ControlledTermUseType
		// in element collection ClassificationType_Form_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Form_CollectionPtr coll = GetForm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Form_CollectionType; // FTT, check this
				SetForm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Genre")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Genre is item of type ClassificationType_Genre_LocalType
		// in element collection ClassificationType_Genre_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Genre_CollectionPtr coll = GetGenre();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Genre_CollectionType; // FTT, check this
				SetGenre(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Genre_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_Genre_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Subject")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Subject is item of type TextAnnotationType
		// in element collection ClassificationType_Subject_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Subject_CollectionPtr coll = GetSubject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Subject_CollectionType; // FTT, check this
				SetSubject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Purpose")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Purpose is item of type ControlledTermUseType
		// in element collection ClassificationType_Purpose_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Purpose_CollectionPtr coll = GetPurpose();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Purpose_CollectionType; // FTT, check this
				SetPurpose(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Language")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Language is item of type ExtendedLanguageType
		// in element collection ClassificationType_Language_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Language_CollectionPtr coll = GetLanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Language_CollectionType; // FTT, check this
				SetLanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtendedLanguageType")))) != empty)
			{
				// Is type allowed
				Mp7JrsExtendedLanguagePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CaptionLanguage")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CaptionLanguage is item of type ClassificationType_CaptionLanguage_LocalType
		// in element collection ClassificationType_CaptionLanguage_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_CaptionLanguage_CollectionPtr coll = GetCaptionLanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_CaptionLanguage_CollectionType; // FTT, check this
				SetCaptionLanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_CaptionLanguage_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_CaptionLanguage_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SignLanguage")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SignLanguage is item of type ClassificationType_SignLanguage_LocalType
		// in element collection ClassificationType_SignLanguage_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_SignLanguage_CollectionPtr coll = GetSignLanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_SignLanguage_CollectionType; // FTT, check this
				SetSignLanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_SignLanguage_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_SignLanguage_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Release")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Release is item of type ClassificationType_Release_LocalType
		// in element collection ClassificationType_Release_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_Release_CollectionPtr coll = GetRelease();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_Release_CollectionType; // FTT, check this
				SetRelease(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Release_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_Release_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Target")) == 0)
	{
		// Target is simple element ClassificationType_Target_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTarget()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationType_Target_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationType_Target_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTarget(p, client);
					if((p = GetTarget()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ParentalGuidance")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ParentalGuidance is item of type ParentalGuidanceType
		// in element collection ClassificationType_ParentalGuidance_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_ParentalGuidance_CollectionPtr coll = GetParentalGuidance();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_ParentalGuidance_CollectionType; // FTT, check this
				SetParentalGuidance(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParentalGuidanceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsParentalGuidancePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaReview")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaReview is item of type MediaReviewType
		// in element collection ClassificationType_MediaReview_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationType_MediaReview_CollectionPtr coll = GetMediaReview();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationType_MediaReview_CollectionType; // FTT, check this
				SetMediaReview(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaReviewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaReviewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Version")) == 0)
	{
		// Version is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVersion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVersion(p, client);
					if((p = GetVersion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsClassificationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsClassificationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationType"));
	// Element serialization:
	if (m_Form != Mp7JrsClassificationType_Form_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Form->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Genre != Mp7JrsClassificationType_Genre_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Genre->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Subject != Mp7JrsClassificationType_Subject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Subject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Purpose != Mp7JrsClassificationType_Purpose_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Purpose->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Language != Mp7JrsClassificationType_Language_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Language->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_CaptionLanguage != Mp7JrsClassificationType_CaptionLanguage_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CaptionLanguage->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SignLanguage != Mp7JrsClassificationType_SignLanguage_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SignLanguage->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Release != Mp7JrsClassificationType_Release_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Release->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Target != Mp7JrsClassificationType_Target_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Target->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Target"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Target->Serialize(doc, element, &elem);
	}
	if (m_ParentalGuidance != Mp7JrsClassificationType_ParentalGuidance_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ParentalGuidance->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaReview != Mp7JrsClassificationType_MediaReview_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaReview->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Version != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Version->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Version"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Version->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsClassificationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Form"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Form")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Form_CollectionPtr tmp = CreateClassificationType_Form_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetForm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Genre"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Genre")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Genre_CollectionPtr tmp = CreateClassificationType_Genre_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetGenre(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Subject"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Subject")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Subject_CollectionPtr tmp = CreateClassificationType_Subject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSubject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Purpose"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Purpose")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Purpose_CollectionPtr tmp = CreateClassificationType_Purpose_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPurpose(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Language"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Language")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Language_CollectionPtr tmp = CreateClassificationType_Language_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CaptionLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CaptionLanguage")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_CaptionLanguage_CollectionPtr tmp = CreateClassificationType_CaptionLanguage_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCaptionLanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SignLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SignLanguage")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_SignLanguage_CollectionPtr tmp = CreateClassificationType_SignLanguage_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSignLanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Release"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Release")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_Release_CollectionPtr tmp = CreateClassificationType_Release_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelease(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Target"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Target")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateClassificationType_Target_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTarget(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ParentalGuidance"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ParentalGuidance")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_ParentalGuidance_CollectionPtr tmp = CreateClassificationType_ParentalGuidance_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetParentalGuidance(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaReview"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaReview")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationType_MediaReview_CollectionPtr tmp = CreateClassificationType_MediaReview_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaReview(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Version"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Version")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVersion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Form")) == 0))
  {
	Mp7JrsClassificationType_Form_CollectionPtr tmp = CreateClassificationType_Form_CollectionType; // FTT, check this
	this->SetForm(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Genre")) == 0))
  {
	Mp7JrsClassificationType_Genre_CollectionPtr tmp = CreateClassificationType_Genre_CollectionType; // FTT, check this
	this->SetGenre(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Subject")) == 0))
  {
	Mp7JrsClassificationType_Subject_CollectionPtr tmp = CreateClassificationType_Subject_CollectionType; // FTT, check this
	this->SetSubject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Purpose")) == 0))
  {
	Mp7JrsClassificationType_Purpose_CollectionPtr tmp = CreateClassificationType_Purpose_CollectionType; // FTT, check this
	this->SetPurpose(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Language")) == 0))
  {
	Mp7JrsClassificationType_Language_CollectionPtr tmp = CreateClassificationType_Language_CollectionType; // FTT, check this
	this->SetLanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CaptionLanguage")) == 0))
  {
	Mp7JrsClassificationType_CaptionLanguage_CollectionPtr tmp = CreateClassificationType_CaptionLanguage_CollectionType; // FTT, check this
	this->SetCaptionLanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SignLanguage")) == 0))
  {
	Mp7JrsClassificationType_SignLanguage_CollectionPtr tmp = CreateClassificationType_SignLanguage_CollectionType; // FTT, check this
	this->SetSignLanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Release")) == 0))
  {
	Mp7JrsClassificationType_Release_CollectionPtr tmp = CreateClassificationType_Release_CollectionType; // FTT, check this
	this->SetRelease(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Target")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateClassificationType_Target_LocalType; // FTT, check this
	}
	this->SetTarget(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ParentalGuidance")) == 0))
  {
	Mp7JrsClassificationType_ParentalGuidance_CollectionPtr tmp = CreateClassificationType_ParentalGuidance_CollectionType; // FTT, check this
	this->SetParentalGuidance(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaReview")) == 0))
  {
	Mp7JrsClassificationType_MediaReview_CollectionPtr tmp = CreateClassificationType_MediaReview_CollectionType; // FTT, check this
	this->SetMediaReview(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Version")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetVersion(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetForm() != Dc1NodePtr())
		result.Insert(GetForm());
	if (GetGenre() != Dc1NodePtr())
		result.Insert(GetGenre());
	if (GetSubject() != Dc1NodePtr())
		result.Insert(GetSubject());
	if (GetPurpose() != Dc1NodePtr())
		result.Insert(GetPurpose());
	if (GetLanguage() != Dc1NodePtr())
		result.Insert(GetLanguage());
	if (GetCaptionLanguage() != Dc1NodePtr())
		result.Insert(GetCaptionLanguage());
	if (GetSignLanguage() != Dc1NodePtr())
		result.Insert(GetSignLanguage());
	if (GetRelease() != Dc1NodePtr())
		result.Insert(GetRelease());
	if (GetTarget() != Dc1NodePtr())
		result.Insert(GetTarget());
	if (GetParentalGuidance() != Dc1NodePtr())
		result.Insert(GetParentalGuidance());
	if (GetMediaReview() != Dc1NodePtr())
		result.Insert(GetMediaReview());
	if (GetVersion() != Dc1NodePtr())
		result.Insert(GetVersion());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationType_ExtMethodImpl.h


