
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsMotionTrajectoryType_CoordRef_LocalType.h"
#include "Mp7JrsMotionTrajectoryType_CoordDef_LocalType.h"
#include "Mp7JrsTemporalInterpolationType.h"
#include "Mp7JrsMotionTrajectoryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMotionTrajectoryType::IMp7JrsMotionTrajectoryType()
{

// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtPropInit.h

}

IMp7JrsMotionTrajectoryType::~IMp7JrsMotionTrajectoryType()
{
// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtPropCleanup.h

}

Mp7JrsMotionTrajectoryType::Mp7JrsMotionTrajectoryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMotionTrajectoryType::~Mp7JrsMotionTrajectoryType()
{
	Cleanup();
}

void Mp7JrsMotionTrajectoryType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_cameraFollows = false; // Value
	m_cameraFollows_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_CoordRef = Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr(); // Class
	m_CoordDef = Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr(); // Class
	m_Params = Mp7JrsTemporalInterpolationPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtMyPropInit.h

}

void Mp7JrsMotionTrajectoryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_CoordRef);
	// Dc1Factory::DeleteObject(m_CoordDef);
	// Dc1Factory::DeleteObject(m_Params);
}

void Mp7JrsMotionTrajectoryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MotionTrajectoryTypePtr, since we
	// might need GetBase(), which isn't defined in IMotionTrajectoryType
	const Dc1Ptr< Mp7JrsMotionTrajectoryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMotionTrajectoryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistcameraFollows())
	{
		this->SetcameraFollows(tmp->GetcameraFollows());
	}
	else
	{
		InvalidatecameraFollows();
	}
	}
		// Dc1Factory::DeleteObject(m_CoordRef);
		this->SetCoordRef(Dc1Factory::CloneObject(tmp->GetCoordRef()));
		// Dc1Factory::DeleteObject(m_CoordDef);
		this->SetCoordDef(Dc1Factory::CloneObject(tmp->GetCoordDef()));
		// Dc1Factory::DeleteObject(m_Params);
		this->SetParams(Dc1Factory::CloneObject(tmp->GetParams()));
}

Dc1NodePtr Mp7JrsMotionTrajectoryType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMotionTrajectoryType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsMotionTrajectoryType::GetBase() const
{
	return m_Base;
}

bool Mp7JrsMotionTrajectoryType::GetcameraFollows() const
{
	return m_cameraFollows;
}

bool Mp7JrsMotionTrajectoryType::ExistcameraFollows() const
{
	return m_cameraFollows_Exist;
}
void Mp7JrsMotionTrajectoryType::SetcameraFollows(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionTrajectoryType::SetcameraFollows().");
	}
	m_cameraFollows = item;
	m_cameraFollows_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionTrajectoryType::InvalidatecameraFollows()
{
	m_cameraFollows_Exist = false;
}
Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr Mp7JrsMotionTrajectoryType::GetCoordRef() const
{
		return m_CoordRef;
}

Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr Mp7JrsMotionTrajectoryType::GetCoordDef() const
{
		return m_CoordDef;
}

Mp7JrsTemporalInterpolationPtr Mp7JrsMotionTrajectoryType::GetParams() const
{
		return m_Params;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMotionTrajectoryType::SetCoordRef(const Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionTrajectoryType::SetCoordRef().");
	}
	if (m_CoordRef != item)
	{
		// Dc1Factory::DeleteObject(m_CoordRef);
		m_CoordRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CoordRef) m_CoordRef->SetParent(m_myself.getPointer());
		if (m_CoordRef && m_CoordRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordRef_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CoordRef->UseTypeAttribute = true;
		}
		if(m_CoordRef != Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr())
		{
			m_CoordRef->SetContentName(XMLString::transcode("CoordRef"));
		}
	// Dc1Factory::DeleteObject(m_CoordDef);
	m_CoordDef = Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMotionTrajectoryType::SetCoordDef(const Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionTrajectoryType::SetCoordDef().");
	}
	if (m_CoordDef != item)
	{
		// Dc1Factory::DeleteObject(m_CoordDef);
		m_CoordDef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CoordDef) m_CoordDef->SetParent(m_myself.getPointer());
		if (m_CoordDef && m_CoordDef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordDef_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CoordDef->UseTypeAttribute = true;
		}
		if(m_CoordDef != Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr())
		{
			m_CoordDef->SetContentName(XMLString::transcode("CoordDef"));
		}
	// Dc1Factory::DeleteObject(m_CoordRef);
	m_CoordRef = Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionTrajectoryType::SetParams(const Mp7JrsTemporalInterpolationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionTrajectoryType::SetParams().");
	}
	if (m_Params != item)
	{
		// Dc1Factory::DeleteObject(m_Params);
		m_Params = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Params) m_Params->SetParent(m_myself.getPointer());
		if (m_Params && m_Params->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Params->UseTypeAttribute = true;
		}
		if(m_Params != Mp7JrsTemporalInterpolationPtr())
		{
			m_Params->SetContentName(XMLString::transcode("Params"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMotionTrajectoryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("cameraFollows")) == 0)
	{
		// cameraFollows is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CoordRef")) == 0)
	{
		// CoordRef is simple element MotionTrajectoryType_CoordRef_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoordRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordRef_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoordRef(p, client);
					if((p = GetCoordRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CoordDef")) == 0)
	{
		// CoordDef is simple element MotionTrajectoryType_CoordDef_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoordDef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType_CoordDef_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoordDef(p, client);
					if((p = GetCoordDef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Params")) == 0)
	{
		// Params is simple element TemporalInterpolationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetParams()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalInterpolationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetParams(p, client);
					if((p = GetParams()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MotionTrajectoryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MotionTrajectoryType");
	}
	return result;
}

XMLCh * Mp7JrsMotionTrajectoryType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMotionTrajectoryType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMotionTrajectoryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MotionTrajectoryType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_cameraFollows_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_cameraFollows);
		element->setAttributeNS(X(""), X("cameraFollows"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_CoordRef != Mp7JrsMotionTrajectoryType_CoordRef_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CoordRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CoordRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CoordRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CoordDef != Mp7JrsMotionTrajectoryType_CoordDef_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CoordDef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CoordDef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CoordDef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Params != Mp7JrsTemporalInterpolationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Params->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Params"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Params->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMotionTrajectoryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("cameraFollows")))
	{
		// deserialize value type
		this->SetcameraFollows(Dc1Convert::TextToBool(parent->getAttribute(X("cameraFollows"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CoordRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CoordRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionTrajectoryType_CoordRef_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoordRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CoordDef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CoordDef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionTrajectoryType_CoordDef_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoordDef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Params"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Params")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalInterpolationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParams(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMotionTrajectoryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("CoordRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionTrajectoryType_CoordRef_LocalType; // FTT, check this
	}
	this->SetCoordRef(child);
  }
  if (XMLString::compareString(elementname, X("CoordDef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionTrajectoryType_CoordDef_LocalType; // FTT, check this
	}
	this->SetCoordDef(child);
  }
  if (XMLString::compareString(elementname, X("Params")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalInterpolationType; // FTT, check this
	}
	this->SetParams(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMotionTrajectoryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetParams() != Dc1NodePtr())
		result.Insert(GetParams());
	if (GetCoordRef() != Dc1NodePtr())
		result.Insert(GetCoordRef());
	if (GetCoordDef() != Dc1NodePtr())
		result.Insert(GetCoordDef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMotionTrajectoryType_ExtMethodImpl.h


