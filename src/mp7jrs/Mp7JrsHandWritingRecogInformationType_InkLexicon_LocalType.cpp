
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionType.h"
#include "Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Entry

#include <assert.h>
IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtPropInit.h

}

IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::~IMp7JrsHandWritingRecogInformationType_InkLexicon_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtPropCleanup.h

}

Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::~Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType()
{
	Cleanup();
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsTermUsePtr(); // Class
	m_Name_Exist = false;
	m_Owner = Dc1Ptr< Dc1Node >(); // Class
	m_Owner_Exist = false;
	m_Resource = Mp7JrsMediaLocatorPtr(); // Class
	m_Entry = Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtMyPropInit.h

}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Owner);
	// Dc1Factory::DeleteObject(m_Resource);
	// Dc1Factory::DeleteObject(m_Entry);
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HandWritingRecogInformationType_InkLexicon_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IHandWritingRecogInformationType_InkLexicon_LocalType
	const Dc1Ptr< Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidName())
	{
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	}
	else
	{
		InvalidateName();
	}
	if (tmp->IsValidOwner())
	{
		// Dc1Factory::DeleteObject(m_Owner);
		this->SetOwner(Dc1Factory::CloneObject(tmp->GetOwner()));
	}
	else
	{
		InvalidateOwner();
	}
		// Dc1Factory::DeleteObject(m_Resource);
		this->SetResource(Dc1Factory::CloneObject(tmp->GetResource()));
		// Dc1Factory::DeleteObject(m_Entry);
		this->SetEntry(Dc1Factory::CloneObject(tmp->GetEntry()));
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTermUsePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetName() const
{
		return m_Name;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::IsValidName() const
{
	return m_Name_Exist;
}

Dc1Ptr< Dc1Node > Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetOwner() const
{
		return m_Owner;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::IsValidOwner() const
{
	return m_Owner_Exist;
}

Mp7JrsMediaLocatorPtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetResource() const
{
		return m_Resource;
}

Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetEntry() const
{
		return m_Entry;
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetName(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetName().");
	}
	if (m_Name != item || m_Name_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if (m_Name && m_Name->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Name->UseTypeAttribute = true;
		}
		m_Name_Exist = true;
		if(m_Name != Mp7JrsTermUsePtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::InvalidateName()
{
	m_Name_Exist = false;
}
void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetOwner(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetOwner().");
	}
	if (m_Owner != item || m_Owner_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Owner);
		m_Owner = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Owner) m_Owner->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Owner) m_Owner->UseTypeAttribute = true;
		m_Owner_Exist = true;
		if(m_Owner != Dc1Ptr< Dc1Node >())
		{
			m_Owner->SetContentName(XMLString::transcode("Owner"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::InvalidateOwner()
{
	m_Owner_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetResource(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetResource().");
	}
	if (m_Resource != item)
	{
		// Dc1Factory::DeleteObject(m_Resource);
		m_Resource = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Resource) m_Resource->SetParent(m_myself.getPointer());
		if (m_Resource && m_Resource->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Resource->UseTypeAttribute = true;
		}
		if(m_Resource != Mp7JrsMediaLocatorPtr())
		{
			m_Resource->SetContentName(XMLString::transcode("Resource"));
		}
	// Dc1Factory::DeleteObject(m_Entry);
	m_Entry = Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetEntry(const Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::SetEntry().");
	}
	if (m_Entry != item)
	{
		// Dc1Factory::DeleteObject(m_Entry);
		m_Entry = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Entry) m_Entry->SetParent(m_myself.getPointer());
		if(m_Entry != Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr())
		{
			m_Entry->SetContentName(XMLString::transcode("Entry"));
		}
	// Dc1Factory::DeleteObject(m_Resource);
	m_Resource = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// Name is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetName(p, client);
					if((p = GetName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Owner")) == 0)
	{
		// Owner is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOwner()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOwner(p, client);
					if((p = GetOwner()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Resource")) == 0)
	{
		// Resource is simple element MediaLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetResource()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetResource(p, client);
					if((p = GetResource()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Entry")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Entry is item of type HandWritingRecogInformationType_InkLexicon_Entry_LocalType
		// in element collection HandWritingRecogInformationType_InkLexicon_Entry_CollectionType
		
		context->Found = true;
		Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr coll = GetEntry();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateHandWritingRecogInformationType_InkLexicon_Entry_CollectionType; // FTT, check this
				SetEntry(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogInformationType_InkLexicon_Entry_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HandWritingRecogInformationType_InkLexicon_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HandWritingRecogInformationType_InkLexicon_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogInformationType_InkLexicon_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Name != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Name->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Name"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Name->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Owner != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Owner->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Owner"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Owner->UseTypeAttribute = true;
		}
		m_Owner->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Resource != Mp7JrsMediaLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Resource->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Resource"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Resource->Serialize(doc, element, &elem);
	}
	if (m_Entry != Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Entry->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Owner"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Owner")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOwner(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Resource"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Resource")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetResource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Entry"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Entry")) == 0))
		{
			// Deserialize factory type
			Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr tmp = CreateHandWritingRecogInformationType_InkLexicon_Entry_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEntry(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Name")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetName(child);
  }
  if (XMLString::compareString(elementname, X("Owner")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetOwner(child);
  }
  if (XMLString::compareString(elementname, X("Resource")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaLocatorType; // FTT, check this
	}
	this->SetResource(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Entry")) == 0))
  {
	Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_CollectionPtr tmp = CreateHandWritingRecogInformationType_InkLexicon_Entry_CollectionType; // FTT, check this
	this->SetEntry(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetOwner() != Dc1NodePtr())
		result.Insert(GetOwner());
	if (GetResource() != Dc1NodePtr())
		result.Insert(GetResource());
	if (GetEntry() != Dc1NodePtr())
		result.Insert(GetEntry());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_LocalType_ExtMethodImpl.h


