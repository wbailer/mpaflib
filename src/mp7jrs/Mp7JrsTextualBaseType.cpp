
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtImplInclude.h


#include "Mp7JrsTextualBaseType_phoneticTranscription_CollectionType.h"
#include "Mp7JrsTextualBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTextualBaseType::IMp7JrsTextualBaseType()
{

// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtPropInit.h

}

IMp7JrsTextualBaseType::~IMp7JrsTextualBaseType()
{
// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtPropCleanup.h

}

Mp7JrsTextualBaseType::Mp7JrsTextualBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextualBaseType::~Mp7JrsTextualBaseType()
{
	Cleanup();
}

void Mp7JrsTextualBaseType::Init()
{
	// Init base
	m_Base = XMLString::transcode("");

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;
	m_phoneticTranscription = Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr(); // Collection
	m_phoneticTranscription_Exist = false;
	m_phoneticAlphabet = Mp7JrsphoneticAlphabetType::UninitializedEnumeration;
	m_phoneticAlphabet_Default = Mp7JrsphoneticAlphabetType::sampa; // Default enumeration
	m_phoneticAlphabet_Exist = false;
	m_charset = NULL; // String
	m_charset_Exist = false;
	m_encoding = NULL; // String
	m_encoding_Exist = false;
	m_script = NULL; // String
	m_script_Exist = false;



// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtMyPropInit.h

}

void Mp7JrsTextualBaseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtMyPropCleanup.h


	XMLString::release(&m_Base);
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_phoneticTranscription);
	XMLString::release(&m_charset); // String
	XMLString::release(&m_encoding); // String
	XMLString::release(&m_script); // String
}

void Mp7JrsTextualBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextualBaseTypePtr, since we
	// might need GetBase(), which isn't defined in ITextualBaseType
	const Dc1Ptr< Mp7JrsTextualBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	XMLString::release(&m_Base);
	m_Base = XMLString::replicate(tmp->GetContent());
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_phoneticTranscription);
	if (tmp->ExistphoneticTranscription())
	{
		this->SetphoneticTranscription(Dc1Factory::CloneObject(tmp->GetphoneticTranscription()));
	}
	else
	{
		InvalidatephoneticTranscription();
	}
	}
	{
	if (tmp->ExistphoneticAlphabet())
	{
		this->SetphoneticAlphabet(tmp->GetphoneticAlphabet());
	}
	else
	{
		InvalidatephoneticAlphabet();
	}
	}
	{
	XMLString::release(&m_charset); // String
	if (tmp->Existcharset())
	{
		this->Setcharset(XMLString::replicate(tmp->Getcharset()));
	}
	else
	{
		Invalidatecharset();
	}
	}
	{
	XMLString::release(&m_encoding); // String
	if (tmp->Existencoding())
	{
		this->Setencoding(XMLString::replicate(tmp->Getencoding()));
	}
	else
	{
		Invalidateencoding();
	}
	}
	{
	XMLString::release(&m_script); // String
	if (tmp->Existscript())
	{
		this->Setscript(XMLString::replicate(tmp->Getscript()));
	}
	else
	{
		Invalidatescript();
	}
	}
}

Dc1NodePtr Mp7JrsTextualBaseType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTextualBaseType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsTextualBaseType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::SetContent().");
	}
	if (m_Base != item)
	{
		XMLString::release(&m_Base);
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsTextualBaseType::GetContent() const
{
	return m_Base;
}
XMLCh * Mp7JrsTextualBaseType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsTextualBaseType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsTextualBaseType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr Mp7JrsTextualBaseType::GetphoneticTranscription() const
{
	return m_phoneticTranscription;
}

bool Mp7JrsTextualBaseType::ExistphoneticTranscription() const
{
	return m_phoneticTranscription_Exist;
}
void Mp7JrsTextualBaseType::SetphoneticTranscription(const Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::SetphoneticTranscription().");
	}
	m_phoneticTranscription = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_phoneticTranscription) m_phoneticTranscription->SetParent(m_myself.getPointer());
	m_phoneticTranscription_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::InvalidatephoneticTranscription()
{
	m_phoneticTranscription_Exist = false;
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsTextualBaseType::GetphoneticAlphabet() const
{
	if (this->ExistphoneticAlphabet()) {
		return m_phoneticAlphabet;
	} else {
		return m_phoneticAlphabet_Default;
	}
}

bool Mp7JrsTextualBaseType::ExistphoneticAlphabet() const
{
	return m_phoneticAlphabet_Exist;
}
void Mp7JrsTextualBaseType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::SetphoneticAlphabet().");
	}
	m_phoneticAlphabet = item;
	m_phoneticAlphabet_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::InvalidatephoneticAlphabet()
{
	m_phoneticAlphabet_Exist = false;
}
XMLCh * Mp7JrsTextualBaseType::Getcharset() const
{
	return m_charset;
}

bool Mp7JrsTextualBaseType::Existcharset() const
{
	return m_charset_Exist;
}
void Mp7JrsTextualBaseType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::Setcharset().");
	}
	m_charset = item;
	m_charset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::Invalidatecharset()
{
	m_charset_Exist = false;
}
XMLCh * Mp7JrsTextualBaseType::Getencoding() const
{
	return m_encoding;
}

bool Mp7JrsTextualBaseType::Existencoding() const
{
	return m_encoding_Exist;
}
void Mp7JrsTextualBaseType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::Setencoding().");
	}
	m_encoding = item;
	m_encoding_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::Invalidateencoding()
{
	m_encoding_Exist = false;
}
XMLCh * Mp7JrsTextualBaseType::Getscript() const
{
	return m_script;
}

bool Mp7JrsTextualBaseType::Existscript() const
{
	return m_script_Exist;
}
void Mp7JrsTextualBaseType::Setscript(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualBaseType::Setscript().");
	}
	m_script = item;
	m_script_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualBaseType::Invalidatescript()
{
	m_script_Exist = false;
}

Dc1NodeEnum Mp7JrsTextualBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  6, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticTranscription")) == 0)
	{
		// phoneticTranscription is simple attribute TextualBaseType_phoneticTranscription_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticAlphabet")) == 0)
	{
		// phoneticAlphabet is simple attribute phoneticAlphabetType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsphoneticAlphabetType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("charset")) == 0)
	{
		// charset is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("encoding")) == 0)
	{
		// encoding is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("script")) == 0)
	{
		// script is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TextualBaseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TextualBaseType");
	}
	return result;
}

XMLCh * Mp7JrsTextualBaseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTextualBaseType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsTextualBaseType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsTextualBaseType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsTextualBaseType::ContentToString() const
{
	// Mp7JrsTextualBaseType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsTextualBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	element->appendChild(doc->createTextNode(m_Base));

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextualBaseType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneticTranscription_Exist)
	{
	// Collection
	if(m_phoneticTranscription != Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr())
	{
		XMLCh * tmp = m_phoneticTranscription->ToText();
		element->setAttributeNS(X(""), X("phoneticTranscription"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneticAlphabet_Exist)
	{
	// Enumeration
	if(m_phoneticAlphabet != Mp7JrsphoneticAlphabetType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsphoneticAlphabetType::ToText(m_phoneticAlphabet);
		element->setAttributeNS(X(""), X("phoneticAlphabet"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_charset_Exist)
	{
	// String
	if(m_charset != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("charset"), m_charset);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_encoding_Exist)
	{
	// String
	if(m_encoding != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("encoding"), m_encoding);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_script_Exist)
	{
	// String
	if(m_script != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("script"), m_script);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsTextualBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticTranscription")))
	{
		// Deserialize collection type
		Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr tmp = CreateTextualBaseType_phoneticTranscription_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("phoneticTranscription")));
		this->SetphoneticTranscription(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticAlphabet")))
	{
		this->SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Parse(parent->getAttribute(X("phoneticAlphabet"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("charset")))
	{
		// Deserialize string type
		this->Setcharset(Dc1Convert::TextToString(parent->getAttribute(X("charset"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("encoding")))
	{
		// Deserialize string type
		this->Setencoding(Dc1Convert::TextToString(parent->getAttribute(X("encoding"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("script")))
	{
		// Deserialize string type
		this->Setscript(Dc1Convert::TextToString(parent->getAttribute(X("script"))));
		* current = parent;
	}

	XMLCh * tmp = Dc1Convert::TextToString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		XMLString::release(&m_Base);
		m_Base = tmp;
		found = true;
	}
// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextualBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsTextualBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextualBaseType_ExtMethodImpl.h


