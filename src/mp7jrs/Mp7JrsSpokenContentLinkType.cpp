
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtImplInclude.h


#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsnonNegativeReal.h"
#include "Mp7Jrsunsigned16.h"
#include "Mp7JrsSpokenContentLinkType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSpokenContentLinkType::IMp7JrsSpokenContentLinkType()
{

// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtPropInit.h

}

IMp7JrsSpokenContentLinkType::~IMp7JrsSpokenContentLinkType()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtPropCleanup.h

}

Mp7JrsSpokenContentLinkType::Mp7JrsSpokenContentLinkType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpokenContentLinkType::~Mp7JrsSpokenContentLinkType()
{
	Cleanup();
}

void Mp7JrsSpokenContentLinkType::Init()
{

	// Init attributes
	m_probability = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_probability->SetContent(0.0); // Use Value initialiser (xxx2)
	m_probability_Exist = false;
	m_acousticScore = CreatenonNegativeReal; // Create a valid class, FTT, check this
	m_acousticScore->SetContent(0.0); // Use Value initialiser (xxx2)
	m_acousticScore_Exist = false;
	m_nodeOffset = Createunsigned16; // Create a valid class, FTT, check this
	m_nodeOffset->SetContent(0); // Use Value initialiser (xxx2)
	m_nodeOffset_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtMyPropInit.h

}

void Mp7JrsSpokenContentLinkType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_probability);
	// Dc1Factory::DeleteObject(m_acousticScore);
	// Dc1Factory::DeleteObject(m_nodeOffset);
}

void Mp7JrsSpokenContentLinkType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpokenContentLinkTypePtr, since we
	// might need GetBase(), which isn't defined in ISpokenContentLinkType
	const Dc1Ptr< Mp7JrsSpokenContentLinkType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_probability);
	if (tmp->Existprobability())
	{
		this->Setprobability(Dc1Factory::CloneObject(tmp->Getprobability()));
	}
	else
	{
		Invalidateprobability();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_acousticScore);
	if (tmp->ExistacousticScore())
	{
		this->SetacousticScore(Dc1Factory::CloneObject(tmp->GetacousticScore()));
	}
	else
	{
		InvalidateacousticScore();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_nodeOffset);
	if (tmp->ExistnodeOffset())
	{
		this->SetnodeOffset(Dc1Factory::CloneObject(tmp->GetnodeOffset()));
	}
	else
	{
		InvalidatenodeOffset();
	}
	}
}

Dc1NodePtr Mp7JrsSpokenContentLinkType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpokenContentLinkType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrszeroToOnePtr Mp7JrsSpokenContentLinkType::Getprobability() const
{
	if (this->Existprobability()) {
		return m_probability;
	} else {
		return m_probability_Default;
	}
}

bool Mp7JrsSpokenContentLinkType::Existprobability() const
{
	return m_probability_Exist;
}
void Mp7JrsSpokenContentLinkType::Setprobability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLinkType::Setprobability().");
	}
	m_probability = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_probability) m_probability->SetParent(m_myself.getPointer());
	m_probability_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLinkType::Invalidateprobability()
{
	m_probability_Exist = false;
}
Mp7JrsnonNegativeRealPtr Mp7JrsSpokenContentLinkType::GetacousticScore() const
{
	return m_acousticScore;
}

bool Mp7JrsSpokenContentLinkType::ExistacousticScore() const
{
	return m_acousticScore_Exist;
}
void Mp7JrsSpokenContentLinkType::SetacousticScore(const Mp7JrsnonNegativeRealPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLinkType::SetacousticScore().");
	}
	m_acousticScore = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_acousticScore) m_acousticScore->SetParent(m_myself.getPointer());
	m_acousticScore_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLinkType::InvalidateacousticScore()
{
	m_acousticScore_Exist = false;
}
Mp7Jrsunsigned16Ptr Mp7JrsSpokenContentLinkType::GetnodeOffset() const
{
	if (this->ExistnodeOffset()) {
		return m_nodeOffset;
	} else {
		return m_nodeOffset_Default;
	}
}

bool Mp7JrsSpokenContentLinkType::ExistnodeOffset() const
{
	return m_nodeOffset_Exist;
}
void Mp7JrsSpokenContentLinkType::SetnodeOffset(const Mp7Jrsunsigned16Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLinkType::SetnodeOffset().");
	}
	m_nodeOffset = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_nodeOffset) m_nodeOffset->SetParent(m_myself.getPointer());
	m_nodeOffset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLinkType::InvalidatenodeOffset()
{
	m_nodeOffset_Exist = false;
}

Dc1NodeEnum Mp7JrsSpokenContentLinkType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsSpokenContentLinkType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpokenContentLinkType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSpokenContentLinkType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpokenContentLinkType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_probability_Exist)
	{
	// Class
	if (m_probability != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_probability->ToText();
		element->setAttributeNS(X(""), X("probability"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_acousticScore_Exist)
	{
	// Class
	if (m_acousticScore != Mp7JrsnonNegativeRealPtr())
	{
		XMLCh * tmp = m_acousticScore->ToText();
		element->setAttributeNS(X(""), X("acousticScore"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_nodeOffset_Exist)
	{
	// Class
	if (m_nodeOffset != Mp7Jrsunsigned16Ptr())
	{
		XMLCh * tmp = m_nodeOffset->ToText();
		element->setAttributeNS(X(""), X("nodeOffset"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSpokenContentLinkType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("probability")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("probability")));
		this->Setprobability(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("acousticScore")))
	{
		// Deserialize class type
		Mp7JrsnonNegativeRealPtr tmp = CreatenonNegativeReal; // FTT, check this
		tmp->Parse(parent->getAttribute(X("acousticScore")));
		this->SetacousticScore(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("nodeOffset")))
	{
		// Deserialize class type
		Mp7Jrsunsigned16Ptr tmp = Createunsigned16; // FTT, check this
		tmp->Parse(parent->getAttribute(X("nodeOffset")));
		this->SetnodeOffset(tmp);
		* current = parent;
	}

// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpokenContentLinkType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsSpokenContentLinkType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpokenContentLinkType_ExtMethodImpl.h


