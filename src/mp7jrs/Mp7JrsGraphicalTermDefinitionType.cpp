
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtImplInclude.h


#include "Mp7JrsTermDefinitionBaseType.h"
#include "Mp7JrsGraphType.h"
#include "Mp7JrsGraphicalRuleDefinitionType.h"
#include "Mp7JrsTermDefinitionBaseType_Name_CollectionType.h"
#include "Mp7JrsTermDefinitionBaseType_Definition_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsGraphicalTermDefinitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTermDefinitionBaseType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition

#include <assert.h>
IMp7JrsGraphicalTermDefinitionType::IMp7JrsGraphicalTermDefinitionType()
{

// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtPropInit.h

}

IMp7JrsGraphicalTermDefinitionType::~IMp7JrsGraphicalTermDefinitionType()
{
// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtPropCleanup.h

}

Mp7JrsGraphicalTermDefinitionType::Mp7JrsGraphicalTermDefinitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGraphicalTermDefinitionType::~Mp7JrsGraphicalTermDefinitionType()
{
	Cleanup();
}

void Mp7JrsGraphicalTermDefinitionType::Init()
{
	// Init base
	m_Base = CreateTermDefinitionBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_TemplateGraph = Mp7JrsGraphPtr(); // Class
	m_AlphabetGraph = Mp7JrsGraphPtr(); // Class
	m_ProductionRule = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtMyPropInit.h

}

void Mp7JrsGraphicalTermDefinitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_TemplateGraph);
	// Dc1Factory::DeleteObject(m_AlphabetGraph);
	// Dc1Factory::DeleteObject(m_ProductionRule);
}

void Mp7JrsGraphicalTermDefinitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GraphicalTermDefinitionTypePtr, since we
	// might need GetBase(), which isn't defined in IGraphicalTermDefinitionType
	const Dc1Ptr< Mp7JrsGraphicalTermDefinitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTermDefinitionBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsGraphicalTermDefinitionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_TemplateGraph);
		this->SetTemplateGraph(Dc1Factory::CloneObject(tmp->GetTemplateGraph()));
		// Dc1Factory::DeleteObject(m_AlphabetGraph);
		this->SetAlphabetGraph(Dc1Factory::CloneObject(tmp->GetAlphabetGraph()));
		// Dc1Factory::DeleteObject(m_ProductionRule);
		this->SetProductionRule(Dc1Factory::CloneObject(tmp->GetProductionRule()));
}

Dc1NodePtr Mp7JrsGraphicalTermDefinitionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsGraphicalTermDefinitionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTermDefinitionBaseType > Mp7JrsGraphicalTermDefinitionType::GetBase() const
{
	return m_Base;
}

Mp7JrsGraphPtr Mp7JrsGraphicalTermDefinitionType::GetTemplateGraph() const
{
		return m_TemplateGraph;
}

Mp7JrsGraphPtr Mp7JrsGraphicalTermDefinitionType::GetAlphabetGraph() const
{
		return m_AlphabetGraph;
}

Dc1Ptr< Dc1Node > Mp7JrsGraphicalTermDefinitionType::GetProductionRule() const
{
		return m_ProductionRule;
}

// implementing setter for choice 
/* element */
void Mp7JrsGraphicalTermDefinitionType::SetTemplateGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetTemplateGraph().");
	}
	if (m_TemplateGraph != item)
	{
		// Dc1Factory::DeleteObject(m_TemplateGraph);
		m_TemplateGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemplateGraph) m_TemplateGraph->SetParent(m_myself.getPointer());
		if (m_TemplateGraph && m_TemplateGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemplateGraph->UseTypeAttribute = true;
		}
		if(m_TemplateGraph != Mp7JrsGraphPtr())
		{
			m_TemplateGraph->SetContentName(XMLString::transcode("TemplateGraph"));
		}
	// Dc1Factory::DeleteObject(m_AlphabetGraph);
	m_AlphabetGraph = Mp7JrsGraphPtr();
	// Dc1Factory::DeleteObject(m_ProductionRule);
	m_ProductionRule = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGraphicalTermDefinitionType::SetAlphabetGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetAlphabetGraph().");
	}
	if (m_AlphabetGraph != item)
	{
		// Dc1Factory::DeleteObject(m_AlphabetGraph);
		m_AlphabetGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AlphabetGraph) m_AlphabetGraph->SetParent(m_myself.getPointer());
		if (m_AlphabetGraph && m_AlphabetGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AlphabetGraph->UseTypeAttribute = true;
		}
		if(m_AlphabetGraph != Mp7JrsGraphPtr())
		{
			m_AlphabetGraph->SetContentName(XMLString::transcode("AlphabetGraph"));
		}
	// Dc1Factory::DeleteObject(m_TemplateGraph);
	m_TemplateGraph = Mp7JrsGraphPtr();
	// Dc1Factory::DeleteObject(m_ProductionRule);
	m_ProductionRule = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGraphicalTermDefinitionType::SetProductionRule(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetProductionRule().");
	}
	if (m_ProductionRule != item)
	{
		// Dc1Factory::DeleteObject(m_ProductionRule);
		m_ProductionRule = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ProductionRule) m_ProductionRule->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_ProductionRule) m_ProductionRule->UseTypeAttribute = true;
		if(m_ProductionRule != Dc1Ptr< Dc1Node >())
		{
			m_ProductionRule->SetContentName(XMLString::transcode("ProductionRule"));
		}
	// Dc1Factory::DeleteObject(m_TemplateGraph);
	m_TemplateGraph = Mp7JrsGraphPtr();
	// Dc1Factory::DeleteObject(m_AlphabetGraph);
	m_AlphabetGraph = Mp7JrsGraphPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsGraphicalTermDefinitionType::GettermID() const
{
	return GetBase()->GettermID();
}

bool Mp7JrsGraphicalTermDefinitionType::ExisttermID() const
{
	return GetBase()->ExisttermID();
}
void Mp7JrsGraphicalTermDefinitionType::SettermID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SettermID().");
	}
	GetBase()->SettermID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::InvalidatetermID()
{
	GetBase()->InvalidatetermID();
}
Mp7JrsTermDefinitionBaseType_Name_CollectionPtr Mp7JrsGraphicalTermDefinitionType::GetName() const
{
	return GetBase()->GetName();
}

Mp7JrsTermDefinitionBaseType_Definition_CollectionPtr Mp7JrsGraphicalTermDefinitionType::GetDefinition() const
{
	return GetBase()->GetDefinition();
}

void Mp7JrsGraphicalTermDefinitionType::SetName(const Mp7JrsTermDefinitionBaseType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetName().");
	}
	GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::SetDefinition(const Mp7JrsTermDefinitionBaseType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetDefinition().");
	}
	GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsGraphicalTermDefinitionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsGraphicalTermDefinitionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsGraphicalTermDefinitionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsGraphicalTermDefinitionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsGraphicalTermDefinitionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsGraphicalTermDefinitionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsGraphicalTermDefinitionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsGraphicalTermDefinitionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsGraphicalTermDefinitionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsGraphicalTermDefinitionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsGraphicalTermDefinitionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsGraphicalTermDefinitionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsGraphicalTermDefinitionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsGraphicalTermDefinitionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsGraphicalTermDefinitionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalTermDefinitionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsGraphicalTermDefinitionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsGraphicalTermDefinitionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalTermDefinitionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGraphicalTermDefinitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TemplateGraph")) == 0)
	{
		// TemplateGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTemplateGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTemplateGraph(p, client);
					if((p = GetTemplateGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AlphabetGraph")) == 0)
	{
		// AlphabetGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAlphabetGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAlphabetGraph(p, client);
					if((p = GetAlphabetGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ProductionRule")) == 0)
	{
		// ProductionRule is simple abstract element GraphicalRuleDefinitionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetProductionRule()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsGraphicalRuleDefinitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetProductionRule(p, client);
					if((p = GetProductionRule()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GraphicalTermDefinitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GraphicalTermDefinitionType");
	}
	return result;
}

XMLCh * Mp7JrsGraphicalTermDefinitionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsGraphicalTermDefinitionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsGraphicalTermDefinitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GraphicalTermDefinitionType"));
	// Element serialization:
	// Class
	
	if (m_TemplateGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemplateGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemplateGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemplateGraph->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AlphabetGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AlphabetGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AlphabetGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AlphabetGraph->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ProductionRule != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ProductionRule->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ProductionRule"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_ProductionRule->UseTypeAttribute = true;
		}
		m_ProductionRule->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsGraphicalTermDefinitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTermDefinitionBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemplateGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemplateGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemplateGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AlphabetGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AlphabetGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAlphabetGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ProductionRule"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ProductionRule")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphicalRuleDefinitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetProductionRule(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGraphicalTermDefinitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("TemplateGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetTemplateGraph(child);
  }
  if (XMLString::compareString(elementname, X("AlphabetGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetAlphabetGraph(child);
  }
  if (XMLString::compareString(elementname, X("ProductionRule")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphicalRuleDefinitionType; // FTT, check this
	}
	this->SetProductionRule(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGraphicalTermDefinitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTemplateGraph() != Dc1NodePtr())
		result.Insert(GetTemplateGraph());
	if (GetAlphabetGraph() != Dc1NodePtr())
		result.Insert(GetAlphabetGraph());
	if (GetProductionRule() != Dc1NodePtr())
		result.Insert(GetProductionRule());
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGraphicalTermDefinitionType_ExtMethodImpl.h


