
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtImplInclude.h


#include "Mp7JrsWordLexiconType.h"
#include "Mp7JrsPhoneLexiconType.h"
#include "Mp7JrsSpokenContentHeaderType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpokenContentHeaderType_LocalType::IMp7JrsSpokenContentHeaderType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtPropInit.h

}

IMp7JrsSpokenContentHeaderType_LocalType::~IMp7JrsSpokenContentHeaderType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtPropCleanup.h

}

Mp7JrsSpokenContentHeaderType_LocalType::Mp7JrsSpokenContentHeaderType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpokenContentHeaderType_LocalType::~Mp7JrsSpokenContentHeaderType_LocalType()
{
	Cleanup();
}

void Mp7JrsSpokenContentHeaderType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_WordLexicon = Mp7JrsWordLexiconPtr(); // Class
	m_PhoneLexicon = Mp7JrsPhoneLexiconPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpokenContentHeaderType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_WordLexicon);
	// Dc1Factory::DeleteObject(m_PhoneLexicon);
}

void Mp7JrsSpokenContentHeaderType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpokenContentHeaderType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpokenContentHeaderType_LocalType
	const Dc1Ptr< Mp7JrsSpokenContentHeaderType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_WordLexicon);
		this->SetWordLexicon(Dc1Factory::CloneObject(tmp->GetWordLexicon()));
		// Dc1Factory::DeleteObject(m_PhoneLexicon);
		this->SetPhoneLexicon(Dc1Factory::CloneObject(tmp->GetPhoneLexicon()));
}

Dc1NodePtr Mp7JrsSpokenContentHeaderType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpokenContentHeaderType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsWordLexiconPtr Mp7JrsSpokenContentHeaderType_LocalType::GetWordLexicon() const
{
		return m_WordLexicon;
}

Mp7JrsPhoneLexiconPtr Mp7JrsSpokenContentHeaderType_LocalType::GetPhoneLexicon() const
{
		return m_PhoneLexicon;
}

// implementing setter for choice 
/* element */
void Mp7JrsSpokenContentHeaderType_LocalType::SetWordLexicon(const Mp7JrsWordLexiconPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType_LocalType::SetWordLexicon().");
	}
	if (m_WordLexicon != item)
	{
		// Dc1Factory::DeleteObject(m_WordLexicon);
		m_WordLexicon = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WordLexicon) m_WordLexicon->SetParent(m_myself.getPointer());
		if (m_WordLexicon && m_WordLexicon->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_WordLexicon->UseTypeAttribute = true;
		}
		if(m_WordLexicon != Mp7JrsWordLexiconPtr())
		{
			m_WordLexicon->SetContentName(XMLString::transcode("WordLexicon"));
		}
	// Dc1Factory::DeleteObject(m_PhoneLexicon);
	m_PhoneLexicon = Mp7JrsPhoneLexiconPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpokenContentHeaderType_LocalType::SetPhoneLexicon(const Mp7JrsPhoneLexiconPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType_LocalType::SetPhoneLexicon().");
	}
	if (m_PhoneLexicon != item)
	{
		// Dc1Factory::DeleteObject(m_PhoneLexicon);
		m_PhoneLexicon = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PhoneLexicon) m_PhoneLexicon->SetParent(m_myself.getPointer());
		if (m_PhoneLexicon && m_PhoneLexicon->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneLexiconType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PhoneLexicon->UseTypeAttribute = true;
		}
		if(m_PhoneLexicon != Mp7JrsPhoneLexiconPtr())
		{
			m_PhoneLexicon->SetContentName(XMLString::transcode("PhoneLexicon"));
		}
	// Dc1Factory::DeleteObject(m_WordLexicon);
	m_WordLexicon = Mp7JrsWordLexiconPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SpokenContentHeaderType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpokenContentHeaderType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpokenContentHeaderType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpokenContentHeaderType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpokenContentHeaderType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_WordLexicon != Mp7JrsWordLexiconPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_WordLexicon->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("WordLexicon"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_WordLexicon->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PhoneLexicon != Mp7JrsPhoneLexiconPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PhoneLexicon->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PhoneLexicon"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PhoneLexicon->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpokenContentHeaderType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("WordLexicon"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("WordLexicon")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateWordLexiconType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetWordLexicon(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PhoneLexicon"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PhoneLexicon")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePhoneLexiconType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPhoneLexicon(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpokenContentHeaderType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("WordLexicon")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateWordLexiconType; // FTT, check this
	}
	this->SetWordLexicon(child);
  }
  if (XMLString::compareString(elementname, X("PhoneLexicon")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePhoneLexiconType; // FTT, check this
	}
	this->SetPhoneLexicon(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpokenContentHeaderType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetWordLexicon() != Dc1NodePtr())
		result.Insert(GetWordLexicon());
	if (GetPhoneLexicon() != Dc1NodePtr())
		result.Insert(GetPhoneLexicon());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_LocalType_ExtMethodImpl.h


