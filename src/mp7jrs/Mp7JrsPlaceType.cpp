
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsPlaceType_Name_CollectionType.h"
#include "Mp7JrsPlaceType_NameTerm_CollectionType.h"
#include "Mp7JrsPlaceType_PlaceDescription_CollectionType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsPlaceType_GeographicPosition_LocalType.h"
#include "Mp7JrsPlaceType_AstronomicalBody_CollectionType.h"
#include "Mp7JrsPlaceType_Region_CollectionType.h"
#include "Mp7JrsPlaceType_AdministrativeUnit_CollectionType.h"
#include "Mp7JrsPlaceType_PostalAddress_LocalType.h"
#include "Mp7JrsPlaceType_StructuredPostalAddress_LocalType.h"
#include "Mp7JrsPlaceType_StructuredInternalCoordinates_LocalType.h"
#include "Mp7JrsPlaceType_ElectronicAddress_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsPlaceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:NameTerm
#include "Mp7JrsregionCode.h" // Element collection urn:mpeg:mpeg7:schema:2004:Region
#include "Mp7JrsPlaceType_AdministrativeUnit_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AdministrativeUnit
#include "Mp7JrsElectronicAddressType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ElectronicAddress

#include <assert.h>
IMp7JrsPlaceType::IMp7JrsPlaceType()
{

// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtPropInit.h

}

IMp7JrsPlaceType::~IMp7JrsPlaceType()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtPropCleanup.h

}

Mp7JrsPlaceType::Mp7JrsPlaceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPlaceType::~Mp7JrsPlaceType()
{
	Cleanup();
}

void Mp7JrsPlaceType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsPlaceType_Name_CollectionPtr(); // Collection
	m_NameTerm = Mp7JrsPlaceType_NameTerm_CollectionPtr(); // Collection
	m_PlaceDescription = Mp7JrsPlaceType_PlaceDescription_CollectionPtr(); // Collection
	m_Role = Mp7JrsTermUsePtr(); // Class
	m_Role_Exist = false;
	m_GeographicPosition = Mp7JrsPlaceType_GeographicPosition_LocalPtr(); // Class
	m_GeographicPosition_Exist = false;
	m_AstronomicalBody = Mp7JrsPlaceType_AstronomicalBody_CollectionPtr(); // Collection
	m_Region = Mp7JrsPlaceType_Region_CollectionPtr(); // Collection
	m_AdministrativeUnit = Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr(); // Collection
	m_PostalAddress = Mp7JrsPlaceType_PostalAddress_LocalPtr(); // Class
	m_PostalAddress_Exist = false;
	m_StructuredPostalAddress = Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr(); // Class
	m_StructuredPostalAddress_Exist = false;
	m_InternalCoordinates = NULL; // Optional String
	m_InternalCoordinates_Exist = false;
	m_StructuredInternalCoordinates = Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr(); // Class
	m_StructuredInternalCoordinates_Exist = false;
	m_ElectronicAddress = Mp7JrsPlaceType_ElectronicAddress_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtMyPropInit.h

}

void Mp7JrsPlaceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_NameTerm);
	// Dc1Factory::DeleteObject(m_PlaceDescription);
	// Dc1Factory::DeleteObject(m_Role);
	// Dc1Factory::DeleteObject(m_GeographicPosition);
	// Dc1Factory::DeleteObject(m_AstronomicalBody);
	// Dc1Factory::DeleteObject(m_Region);
	// Dc1Factory::DeleteObject(m_AdministrativeUnit);
	// Dc1Factory::DeleteObject(m_PostalAddress);
	// Dc1Factory::DeleteObject(m_StructuredPostalAddress);
	XMLString::release(&this->m_InternalCoordinates);
	this->m_InternalCoordinates = NULL;
	// Dc1Factory::DeleteObject(m_StructuredInternalCoordinates);
	// Dc1Factory::DeleteObject(m_ElectronicAddress);
}

void Mp7JrsPlaceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PlaceTypePtr, since we
	// might need GetBase(), which isn't defined in IPlaceType
	const Dc1Ptr< Mp7JrsPlaceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPlaceType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
		// Dc1Factory::DeleteObject(m_NameTerm);
		this->SetNameTerm(Dc1Factory::CloneObject(tmp->GetNameTerm()));
		// Dc1Factory::DeleteObject(m_PlaceDescription);
		this->SetPlaceDescription(Dc1Factory::CloneObject(tmp->GetPlaceDescription()));
	if (tmp->IsValidRole())
	{
		// Dc1Factory::DeleteObject(m_Role);
		this->SetRole(Dc1Factory::CloneObject(tmp->GetRole()));
	}
	else
	{
		InvalidateRole();
	}
	if (tmp->IsValidGeographicPosition())
	{
		// Dc1Factory::DeleteObject(m_GeographicPosition);
		this->SetGeographicPosition(Dc1Factory::CloneObject(tmp->GetGeographicPosition()));
	}
	else
	{
		InvalidateGeographicPosition();
	}
		// Dc1Factory::DeleteObject(m_AstronomicalBody);
		this->SetAstronomicalBody(Dc1Factory::CloneObject(tmp->GetAstronomicalBody()));
		// Dc1Factory::DeleteObject(m_Region);
		this->SetRegion(Dc1Factory::CloneObject(tmp->GetRegion()));
		// Dc1Factory::DeleteObject(m_AdministrativeUnit);
		this->SetAdministrativeUnit(Dc1Factory::CloneObject(tmp->GetAdministrativeUnit()));
	if (tmp->IsValidPostalAddress())
	{
		// Dc1Factory::DeleteObject(m_PostalAddress);
		this->SetPostalAddress(Dc1Factory::CloneObject(tmp->GetPostalAddress()));
	}
	else
	{
		InvalidatePostalAddress();
	}
	if (tmp->IsValidStructuredPostalAddress())
	{
		// Dc1Factory::DeleteObject(m_StructuredPostalAddress);
		this->SetStructuredPostalAddress(Dc1Factory::CloneObject(tmp->GetStructuredPostalAddress()));
	}
	else
	{
		InvalidateStructuredPostalAddress();
	}
	if (tmp->IsValidInternalCoordinates())
	{
		this->SetInternalCoordinates(XMLString::replicate(tmp->GetInternalCoordinates()));
	}
	else
	{
		InvalidateInternalCoordinates();
	}
	if (tmp->IsValidStructuredInternalCoordinates())
	{
		// Dc1Factory::DeleteObject(m_StructuredInternalCoordinates);
		this->SetStructuredInternalCoordinates(Dc1Factory::CloneObject(tmp->GetStructuredInternalCoordinates()));
	}
	else
	{
		InvalidateStructuredInternalCoordinates();
	}
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		this->SetElectronicAddress(Dc1Factory::CloneObject(tmp->GetElectronicAddress()));
}

Dc1NodePtr Mp7JrsPlaceType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPlaceType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsPlaceType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsPlaceType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsPlaceType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsPlaceType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsPlaceType_Name_CollectionPtr Mp7JrsPlaceType::GetName() const
{
		return m_Name;
}

Mp7JrsPlaceType_NameTerm_CollectionPtr Mp7JrsPlaceType::GetNameTerm() const
{
		return m_NameTerm;
}

Mp7JrsPlaceType_PlaceDescription_CollectionPtr Mp7JrsPlaceType::GetPlaceDescription() const
{
		return m_PlaceDescription;
}

Mp7JrsTermUsePtr Mp7JrsPlaceType::GetRole() const
{
		return m_Role;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidRole() const
{
	return m_Role_Exist;
}

Mp7JrsPlaceType_GeographicPosition_LocalPtr Mp7JrsPlaceType::GetGeographicPosition() const
{
		return m_GeographicPosition;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidGeographicPosition() const
{
	return m_GeographicPosition_Exist;
}

Mp7JrsPlaceType_AstronomicalBody_CollectionPtr Mp7JrsPlaceType::GetAstronomicalBody() const
{
		return m_AstronomicalBody;
}

Mp7JrsPlaceType_Region_CollectionPtr Mp7JrsPlaceType::GetRegion() const
{
		return m_Region;
}

Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr Mp7JrsPlaceType::GetAdministrativeUnit() const
{
		return m_AdministrativeUnit;
}

Mp7JrsPlaceType_PostalAddress_LocalPtr Mp7JrsPlaceType::GetPostalAddress() const
{
		return m_PostalAddress;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidPostalAddress() const
{
	return m_PostalAddress_Exist;
}

Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr Mp7JrsPlaceType::GetStructuredPostalAddress() const
{
		return m_StructuredPostalAddress;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidStructuredPostalAddress() const
{
	return m_StructuredPostalAddress_Exist;
}

XMLCh * Mp7JrsPlaceType::GetInternalCoordinates() const
{
		return m_InternalCoordinates;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidInternalCoordinates() const
{
	return m_InternalCoordinates_Exist;
}

Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr Mp7JrsPlaceType::GetStructuredInternalCoordinates() const
{
		return m_StructuredInternalCoordinates;
}

// Element is optional
bool Mp7JrsPlaceType::IsValidStructuredInternalCoordinates() const
{
	return m_StructuredInternalCoordinates_Exist;
}

Mp7JrsPlaceType_ElectronicAddress_CollectionPtr Mp7JrsPlaceType::GetElectronicAddress() const
{
		return m_ElectronicAddress;
}

void Mp7JrsPlaceType::SetName(const Mp7JrsPlaceType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != Mp7JrsPlaceType_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::SetNameTerm(const Mp7JrsPlaceType_NameTerm_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetNameTerm().");
	}
	if (m_NameTerm != item)
	{
		// Dc1Factory::DeleteObject(m_NameTerm);
		m_NameTerm = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NameTerm) m_NameTerm->SetParent(m_myself.getPointer());
		if(m_NameTerm != Mp7JrsPlaceType_NameTerm_CollectionPtr())
		{
			m_NameTerm->SetContentName(XMLString::transcode("NameTerm"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::SetPlaceDescription(const Mp7JrsPlaceType_PlaceDescription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetPlaceDescription().");
	}
	if (m_PlaceDescription != item)
	{
		// Dc1Factory::DeleteObject(m_PlaceDescription);
		m_PlaceDescription = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PlaceDescription) m_PlaceDescription->SetParent(m_myself.getPointer());
		if(m_PlaceDescription != Mp7JrsPlaceType_PlaceDescription_CollectionPtr())
		{
			m_PlaceDescription->SetContentName(XMLString::transcode("PlaceDescription"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::SetRole(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetRole().");
	}
	if (m_Role != item || m_Role_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Role);
		m_Role = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Role) m_Role->SetParent(m_myself.getPointer());
		if (m_Role && m_Role->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Role->UseTypeAttribute = true;
		}
		m_Role_Exist = true;
		if(m_Role != Mp7JrsTermUsePtr())
		{
			m_Role->SetContentName(XMLString::transcode("Role"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidateRole()
{
	m_Role_Exist = false;
}
void Mp7JrsPlaceType::SetGeographicPosition(const Mp7JrsPlaceType_GeographicPosition_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetGeographicPosition().");
	}
	if (m_GeographicPosition != item || m_GeographicPosition_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_GeographicPosition);
		m_GeographicPosition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GeographicPosition) m_GeographicPosition->SetParent(m_myself.getPointer());
		if (m_GeographicPosition && m_GeographicPosition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_GeographicPosition_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GeographicPosition->UseTypeAttribute = true;
		}
		m_GeographicPosition_Exist = true;
		if(m_GeographicPosition != Mp7JrsPlaceType_GeographicPosition_LocalPtr())
		{
			m_GeographicPosition->SetContentName(XMLString::transcode("GeographicPosition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidateGeographicPosition()
{
	m_GeographicPosition_Exist = false;
}
void Mp7JrsPlaceType::SetAstronomicalBody(const Mp7JrsPlaceType_AstronomicalBody_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetAstronomicalBody().");
	}
	if (m_AstronomicalBody != item)
	{
		// Dc1Factory::DeleteObject(m_AstronomicalBody);
		m_AstronomicalBody = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AstronomicalBody) m_AstronomicalBody->SetParent(m_myself.getPointer());
		if(m_AstronomicalBody != Mp7JrsPlaceType_AstronomicalBody_CollectionPtr())
		{
			m_AstronomicalBody->SetContentName(XMLString::transcode("AstronomicalBody"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::SetRegion(const Mp7JrsPlaceType_Region_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetRegion().");
	}
	if (m_Region != item)
	{
		// Dc1Factory::DeleteObject(m_Region);
		m_Region = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Region) m_Region->SetParent(m_myself.getPointer());
		if(m_Region != Mp7JrsPlaceType_Region_CollectionPtr())
		{
			m_Region->SetContentName(XMLString::transcode("Region"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::SetAdministrativeUnit(const Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetAdministrativeUnit().");
	}
	if (m_AdministrativeUnit != item)
	{
		// Dc1Factory::DeleteObject(m_AdministrativeUnit);
		m_AdministrativeUnit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AdministrativeUnit) m_AdministrativeUnit->SetParent(m_myself.getPointer());
		if(m_AdministrativeUnit != Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr())
		{
			m_AdministrativeUnit->SetContentName(XMLString::transcode("AdministrativeUnit"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPlaceType::SetPostalAddress(const Mp7JrsPlaceType_PostalAddress_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetPostalAddress().");
	}
	if (m_PostalAddress != item || m_PostalAddress_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PostalAddress);
		m_PostalAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PostalAddress) m_PostalAddress->SetParent(m_myself.getPointer());
		if (m_PostalAddress && m_PostalAddress->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_PostalAddress_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PostalAddress->UseTypeAttribute = true;
		}
		m_PostalAddress_Exist = true;
		if(m_PostalAddress != Mp7JrsPlaceType_PostalAddress_LocalPtr())
		{
			m_PostalAddress->SetContentName(XMLString::transcode("PostalAddress"));
		}
	// Dc1Factory::DeleteObject(m_StructuredPostalAddress);
	m_StructuredPostalAddress = Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidatePostalAddress()
{
	m_PostalAddress_Exist = false;
}
/* element */
void Mp7JrsPlaceType::SetStructuredPostalAddress(const Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetStructuredPostalAddress().");
	}
	if (m_StructuredPostalAddress != item || m_StructuredPostalAddress_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_StructuredPostalAddress);
		m_StructuredPostalAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredPostalAddress) m_StructuredPostalAddress->SetParent(m_myself.getPointer());
		if (m_StructuredPostalAddress && m_StructuredPostalAddress->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredPostalAddress_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StructuredPostalAddress->UseTypeAttribute = true;
		}
		m_StructuredPostalAddress_Exist = true;
		if(m_StructuredPostalAddress != Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr())
		{
			m_StructuredPostalAddress->SetContentName(XMLString::transcode("StructuredPostalAddress"));
		}
	// Dc1Factory::DeleteObject(m_PostalAddress);
	m_PostalAddress = Mp7JrsPlaceType_PostalAddress_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidateStructuredPostalAddress()
{
	m_StructuredPostalAddress_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPlaceType::SetInternalCoordinates(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetInternalCoordinates().");
	}
	if (m_InternalCoordinates != item || m_InternalCoordinates_Exist == false)
	{
		XMLString::release(&m_InternalCoordinates);
		m_InternalCoordinates = item;
		m_InternalCoordinates_Exist = true;
	// Dc1Factory::DeleteObject(m_StructuredInternalCoordinates);
	m_StructuredInternalCoordinates = Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidateInternalCoordinates()
{
	m_InternalCoordinates_Exist = false;
}
/* element */
void Mp7JrsPlaceType::SetStructuredInternalCoordinates(const Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetStructuredInternalCoordinates().");
	}
	if (m_StructuredInternalCoordinates != item || m_StructuredInternalCoordinates_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_StructuredInternalCoordinates);
		m_StructuredInternalCoordinates = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredInternalCoordinates) m_StructuredInternalCoordinates->SetParent(m_myself.getPointer());
		if (m_StructuredInternalCoordinates && m_StructuredInternalCoordinates->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredInternalCoordinates_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StructuredInternalCoordinates->UseTypeAttribute = true;
		}
		m_StructuredInternalCoordinates_Exist = true;
		if(m_StructuredInternalCoordinates != Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr())
		{
			m_StructuredInternalCoordinates->SetContentName(XMLString::transcode("StructuredInternalCoordinates"));
		}
	XMLString::release(&this->m_InternalCoordinates);
	m_InternalCoordinates = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidateStructuredInternalCoordinates()
{
	m_StructuredInternalCoordinates_Exist = false;
}
void Mp7JrsPlaceType::SetElectronicAddress(const Mp7JrsPlaceType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetElectronicAddress().");
	}
	if (m_ElectronicAddress != item)
	{
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		m_ElectronicAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ElectronicAddress) m_ElectronicAddress->SetParent(m_myself.getPointer());
		if(m_ElectronicAddress != Mp7JrsPlaceType_ElectronicAddress_CollectionPtr())
		{
			m_ElectronicAddress->SetContentName(XMLString::transcode("ElectronicAddress"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsPlaceType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsPlaceType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsPlaceType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsPlaceType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsPlaceType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsPlaceType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsPlaceType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsPlaceType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsPlaceType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsPlaceType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsPlaceType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsPlaceType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsPlaceType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsPlaceType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsPlaceType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsPlaceType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsPlaceType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPlaceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Name is item of type TextualType
		// in element collection PlaceType_Name_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NameTerm")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:NameTerm is item of type ControlledTermUseType
		// in element collection PlaceType_NameTerm_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_NameTerm_CollectionPtr coll = GetNameTerm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_NameTerm_CollectionType; // FTT, check this
				SetNameTerm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PlaceDescription")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PlaceDescription is item of type TextualType
		// in element collection PlaceType_PlaceDescription_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_PlaceDescription_CollectionPtr coll = GetPlaceDescription();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_PlaceDescription_CollectionType; // FTT, check this
				SetPlaceDescription(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Role")) == 0)
	{
		// Role is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRole()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRole(p, client);
					if((p = GetRole()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GeographicPosition")) == 0)
	{
		// GeographicPosition is simple element PlaceType_GeographicPosition_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGeographicPosition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_GeographicPosition_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlaceType_GeographicPosition_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGeographicPosition(p, client);
					if((p = GetGeographicPosition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AstronomicalBody")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AstronomicalBody is item of type TermUseType
		// in element collection PlaceType_AstronomicalBody_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_AstronomicalBody_CollectionPtr coll = GetAstronomicalBody();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_AstronomicalBody_CollectionType; // FTT, check this
				SetAstronomicalBody(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Region")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Region is item of type regionCode
		// in element collection PlaceType_Region_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_Region_CollectionPtr coll = GetRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_Region_CollectionType; // FTT, check this
				SetRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:regionCode")))) != empty)
			{
				// Is type allowed
				Mp7JrsregionCodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AdministrativeUnit")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AdministrativeUnit is item of type PlaceType_AdministrativeUnit_LocalType
		// in element collection PlaceType_AdministrativeUnit_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr coll = GetAdministrativeUnit();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_AdministrativeUnit_CollectionType; // FTT, check this
				SetAdministrativeUnit(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_AdministrativeUnit_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlaceType_AdministrativeUnit_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PostalAddress")) == 0)
	{
		// PostalAddress is simple element PlaceType_PostalAddress_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPostalAddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_PostalAddress_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlaceType_PostalAddress_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPostalAddress(p, client);
					if((p = GetPostalAddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredPostalAddress")) == 0)
	{
		// StructuredPostalAddress is simple element PlaceType_StructuredPostalAddress_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStructuredPostalAddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredPostalAddress_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStructuredPostalAddress(p, client);
					if((p = GetStructuredPostalAddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredInternalCoordinates")) == 0)
	{
		// StructuredInternalCoordinates is simple element PlaceType_StructuredInternalCoordinates_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStructuredInternalCoordinates()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType_StructuredInternalCoordinates_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStructuredInternalCoordinates(p, client);
					if((p = GetStructuredInternalCoordinates()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ElectronicAddress")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ElectronicAddress is item of type ElectronicAddressType
		// in element collection PlaceType_ElectronicAddress_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_ElectronicAddress_CollectionPtr coll = GetElectronicAddress();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_ElectronicAddress_CollectionType; // FTT, check this
				SetElectronicAddress(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType")))) != empty)
			{
				// Is type allowed
				Mp7JrsElectronicAddressPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PlaceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PlaceType");
	}
	return result;
}

XMLCh * Mp7JrsPlaceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPlaceType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPlaceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PlaceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Name != Mp7JrsPlaceType_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_NameTerm != Mp7JrsPlaceType_NameTerm_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_NameTerm->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PlaceDescription != Mp7JrsPlaceType_PlaceDescription_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PlaceDescription->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Role != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Role->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Role"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Role->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GeographicPosition != Mp7JrsPlaceType_GeographicPosition_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GeographicPosition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GeographicPosition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GeographicPosition->Serialize(doc, element, &elem);
	}
	if (m_AstronomicalBody != Mp7JrsPlaceType_AstronomicalBody_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AstronomicalBody->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Region != Mp7JrsPlaceType_Region_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Region->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_AdministrativeUnit != Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AdministrativeUnit->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_PostalAddress != Mp7JrsPlaceType_PostalAddress_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PostalAddress->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PostalAddress"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PostalAddress->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StructuredPostalAddress != Mp7JrsPlaceType_StructuredPostalAddress_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StructuredPostalAddress->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StructuredPostalAddress"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StructuredPostalAddress->Serialize(doc, element, &elem);
	}
	 // String
	if(m_InternalCoordinates != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_InternalCoordinates, X("urn:mpeg:mpeg7:schema:2004"), X("InternalCoordinates"), true);
	// Class
	
	if (m_StructuredInternalCoordinates != Mp7JrsPlaceType_StructuredInternalCoordinates_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StructuredInternalCoordinates->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StructuredInternalCoordinates"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StructuredInternalCoordinates->Serialize(doc, element, &elem);
	}
	if (m_ElectronicAddress != Mp7JrsPlaceType_ElectronicAddress_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ElectronicAddress->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPlaceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_Name_CollectionPtr tmp = CreatePlaceType_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("NameTerm"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("NameTerm")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_NameTerm_CollectionPtr tmp = CreatePlaceType_NameTerm_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNameTerm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PlaceDescription"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PlaceDescription")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_PlaceDescription_CollectionPtr tmp = CreatePlaceType_PlaceDescription_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPlaceDescription(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Role"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Role")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRole(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GeographicPosition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GeographicPosition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType_GeographicPosition_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGeographicPosition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AstronomicalBody"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AstronomicalBody")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_AstronomicalBody_CollectionPtr tmp = CreatePlaceType_AstronomicalBody_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAstronomicalBody(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Region"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Region")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_Region_CollectionPtr tmp = CreatePlaceType_Region_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AdministrativeUnit"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AdministrativeUnit")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr tmp = CreatePlaceType_AdministrativeUnit_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAdministrativeUnit(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PostalAddress"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PostalAddress")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType_PostalAddress_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPostalAddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StructuredPostalAddress"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StructuredPostalAddress")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType_StructuredPostalAddress_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStructuredPostalAddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("InternalCoordinates"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetInternalCoordinates(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetInternalCoordinates(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StructuredInternalCoordinates"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StructuredInternalCoordinates")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType_StructuredInternalCoordinates_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStructuredInternalCoordinates(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ElectronicAddress"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ElectronicAddress")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_ElectronicAddress_CollectionPtr tmp = CreatePlaceType_ElectronicAddress_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetElectronicAddress(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPlaceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	Mp7JrsPlaceType_Name_CollectionPtr tmp = CreatePlaceType_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("NameTerm")) == 0))
  {
	Mp7JrsPlaceType_NameTerm_CollectionPtr tmp = CreatePlaceType_NameTerm_CollectionType; // FTT, check this
	this->SetNameTerm(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PlaceDescription")) == 0))
  {
	Mp7JrsPlaceType_PlaceDescription_CollectionPtr tmp = CreatePlaceType_PlaceDescription_CollectionType; // FTT, check this
	this->SetPlaceDescription(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Role")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetRole(child);
  }
  if (XMLString::compareString(elementname, X("GeographicPosition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType_GeographicPosition_LocalType; // FTT, check this
	}
	this->SetGeographicPosition(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AstronomicalBody")) == 0))
  {
	Mp7JrsPlaceType_AstronomicalBody_CollectionPtr tmp = CreatePlaceType_AstronomicalBody_CollectionType; // FTT, check this
	this->SetAstronomicalBody(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Region")) == 0))
  {
	Mp7JrsPlaceType_Region_CollectionPtr tmp = CreatePlaceType_Region_CollectionType; // FTT, check this
	this->SetRegion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AdministrativeUnit")) == 0))
  {
	Mp7JrsPlaceType_AdministrativeUnit_CollectionPtr tmp = CreatePlaceType_AdministrativeUnit_CollectionType; // FTT, check this
	this->SetAdministrativeUnit(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("PostalAddress")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType_PostalAddress_LocalType; // FTT, check this
	}
	this->SetPostalAddress(child);
  }
  if (XMLString::compareString(elementname, X("StructuredPostalAddress")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType_StructuredPostalAddress_LocalType; // FTT, check this
	}
	this->SetStructuredPostalAddress(child);
  }
  if (XMLString::compareString(elementname, X("StructuredInternalCoordinates")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType_StructuredInternalCoordinates_LocalType; // FTT, check this
	}
	this->SetStructuredInternalCoordinates(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ElectronicAddress")) == 0))
  {
	Mp7JrsPlaceType_ElectronicAddress_CollectionPtr tmp = CreatePlaceType_ElectronicAddress_CollectionType; // FTT, check this
	this->SetElectronicAddress(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPlaceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetNameTerm() != Dc1NodePtr())
		result.Insert(GetNameTerm());
	if (GetPlaceDescription() != Dc1NodePtr())
		result.Insert(GetPlaceDescription());
	if (GetRole() != Dc1NodePtr())
		result.Insert(GetRole());
	if (GetGeographicPosition() != Dc1NodePtr())
		result.Insert(GetGeographicPosition());
	if (GetAstronomicalBody() != Dc1NodePtr())
		result.Insert(GetAstronomicalBody());
	if (GetRegion() != Dc1NodePtr())
		result.Insert(GetRegion());
	if (GetAdministrativeUnit() != Dc1NodePtr())
		result.Insert(GetAdministrativeUnit());
	if (GetElectronicAddress() != Dc1NodePtr())
		result.Insert(GetElectronicAddress());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetPostalAddress() != Dc1NodePtr())
		result.Insert(GetPostalAddress());
	if (GetStructuredPostalAddress() != Dc1NodePtr())
		result.Insert(GetStructuredPostalAddress());
	if (GetStructuredInternalCoordinates() != Dc1NodePtr())
		result.Insert(GetStructuredInternalCoordinates());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPlaceType_ExtMethodImpl.h


