
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtImplInclude.h


#include "Mp7JrsLinguisticEntityType.h"
#include "Mp7JrsSentencesType_CollectionType.h"
#include "Mp7JrsLinguisticEntityType_start_LocalType.h"
#include "Mp7JrsLinguisticEntityType_length_LocalType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrstermReferenceListType.h"
#include "Mp7JrsLinguisticEntityType_edit_LocalType.h"
#include "Mp7JrsLinguisticEntityType_MediaLocator_CollectionType.h"
#include "Mp7JrsLinguisticEntityType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSentencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaLocator
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsSentencesType_LocalType.h" // Choice collection Sentences
#include "Mp7JrsSentencesType.h" // Choice collection element Sentences
#include "Mp7JrsSyntacticConstituentType.h" // Choice collection element Sentence
#include "Mp7JrsLinguisticDocumentType.h" // Choice collection element Quotation

#include <assert.h>
IMp7JrsSentencesType::IMp7JrsSentencesType()
{

// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtPropInit.h

}

IMp7JrsSentencesType::~IMp7JrsSentencesType()
{
// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtPropCleanup.h

}

Mp7JrsSentencesType::Mp7JrsSentencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSentencesType::~Mp7JrsSentencesType()
{
	Cleanup();
}

void Mp7JrsSentencesType::Init()
{
	// Init base
	m_Base = CreateLinguisticEntityType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_synthesis = Mp7JrssynthesisType::UninitializedEnumeration;
	m_synthesis_Default = Mp7JrssynthesisType::coordination; // Default enumeration
	m_synthesis_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SentencesType_LocalType = Mp7JrsSentencesType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtMyPropInit.h

}

void Mp7JrsSentencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SentencesType_LocalType);
}

void Mp7JrsSentencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SentencesTypePtr, since we
	// might need GetBase(), which isn't defined in ISentencesType
	const Dc1Ptr< Mp7JrsSentencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsLinguisticEntityPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSentencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existsynthesis())
	{
		this->Setsynthesis(tmp->Getsynthesis());
	}
	else
	{
		Invalidatesynthesis();
	}
	}
		// Dc1Factory::DeleteObject(m_SentencesType_LocalType);
		this->SetSentencesType_LocalType(Dc1Factory::CloneObject(tmp->GetSentencesType_LocalType()));
}

Dc1NodePtr Mp7JrsSentencesType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSentencesType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsLinguisticEntityType > Mp7JrsSentencesType::GetBase() const
{
	return m_Base;
}

Mp7JrssynthesisType::Enumeration Mp7JrsSentencesType::Getsynthesis() const
{
	if (this->Existsynthesis()) {
		return m_synthesis;
	} else {
		return m_synthesis_Default;
	}
}

bool Mp7JrsSentencesType::Existsynthesis() const
{
	return m_synthesis_Exist;
}
void Mp7JrsSentencesType::Setsynthesis(Mp7JrssynthesisType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setsynthesis().");
	}
	m_synthesis = item;
	m_synthesis_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatesynthesis()
{
	m_synthesis_Exist = false;
}
Mp7JrsSentencesType_CollectionPtr Mp7JrsSentencesType::GetSentencesType_LocalType() const
{
		return m_SentencesType_LocalType;
}

void Mp7JrsSentencesType::SetSentencesType_LocalType(const Mp7JrsSentencesType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetSentencesType_LocalType().");
	}
	if (m_SentencesType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SentencesType_LocalType);
		m_SentencesType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SentencesType_LocalType) m_SentencesType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSentencesType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsSentencesType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsSentencesType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsLinguisticEntityType_start_LocalPtr Mp7JrsSentencesType::Getstart() const
{
	return GetBase()->Getstart();
}

bool Mp7JrsSentencesType::Existstart() const
{
	return GetBase()->Existstart();
}
void Mp7JrsSentencesType::Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setstart().");
	}
	GetBase()->Setstart(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatestart()
{
	GetBase()->Invalidatestart();
}
Mp7JrsLinguisticEntityType_length_LocalPtr Mp7JrsSentencesType::Getlength() const
{
	return GetBase()->Getlength();
}

bool Mp7JrsSentencesType::Existlength() const
{
	return GetBase()->Existlength();
}
void Mp7JrsSentencesType::Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setlength().");
	}
	GetBase()->Setlength(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatelength()
{
	GetBase()->Invalidatelength();
}
Mp7JrstermReferencePtr Mp7JrsSentencesType::Gettype() const
{
	return GetBase()->Gettype();
}

bool Mp7JrsSentencesType::Existtype() const
{
	return GetBase()->Existtype();
}
void Mp7JrsSentencesType::Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatetype()
{
	GetBase()->Invalidatetype();
}
Mp7JrstermReferencePtr Mp7JrsSentencesType::Getdepend() const
{
	return GetBase()->Getdepend();
}

bool Mp7JrsSentencesType::Existdepend() const
{
	return GetBase()->Existdepend();
}
void Mp7JrsSentencesType::Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setdepend().");
	}
	GetBase()->Setdepend(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatedepend()
{
	GetBase()->Invalidatedepend();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::Getequal() const
{
	return GetBase()->Getequal();
}

bool Mp7JrsSentencesType::Existequal() const
{
	return GetBase()->Existequal();
}
void Mp7JrsSentencesType::Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setequal().");
	}
	GetBase()->Setequal(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidateequal()
{
	GetBase()->Invalidateequal();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::Getsemantics() const
{
	return GetBase()->Getsemantics();
}

bool Mp7JrsSentencesType::Existsemantics() const
{
	return GetBase()->Existsemantics();
}
void Mp7JrsSentencesType::Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setsemantics().");
	}
	GetBase()->Setsemantics(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatesemantics()
{
	GetBase()->Invalidatesemantics();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::GetcompoundSemantics() const
{
	return GetBase()->GetcompoundSemantics();
}

bool Mp7JrsSentencesType::ExistcompoundSemantics() const
{
	return GetBase()->ExistcompoundSemantics();
}
void Mp7JrsSentencesType::SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetcompoundSemantics().");
	}
	GetBase()->SetcompoundSemantics(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatecompoundSemantics()
{
	GetBase()->InvalidatecompoundSemantics();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::Getoperator() const
{
	return GetBase()->Getoperator();
}

bool Mp7JrsSentencesType::Existoperator() const
{
	return GetBase()->Existoperator();
}
void Mp7JrsSentencesType::Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setoperator().");
	}
	GetBase()->Setoperator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidateoperator()
{
	GetBase()->Invalidateoperator();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::Getcopy() const
{
	return GetBase()->Getcopy();
}

bool Mp7JrsSentencesType::Existcopy() const
{
	return GetBase()->Existcopy();
}
void Mp7JrsSentencesType::Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setcopy().");
	}
	GetBase()->Setcopy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatecopy()
{
	GetBase()->Invalidatecopy();
}
Mp7JrstermReferenceListPtr Mp7JrsSentencesType::GetnoCopy() const
{
	return GetBase()->GetnoCopy();
}

bool Mp7JrsSentencesType::ExistnoCopy() const
{
	return GetBase()->ExistnoCopy();
}
void Mp7JrsSentencesType::SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetnoCopy().");
	}
	GetBase()->SetnoCopy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatenoCopy()
{
	GetBase()->InvalidatenoCopy();
}
Mp7JrstermReferencePtr Mp7JrsSentencesType::Getsubstitute() const
{
	return GetBase()->Getsubstitute();
}

bool Mp7JrsSentencesType::Existsubstitute() const
{
	return GetBase()->Existsubstitute();
}
void Mp7JrsSentencesType::Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setsubstitute().");
	}
	GetBase()->Setsubstitute(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidatesubstitute()
{
	GetBase()->Invalidatesubstitute();
}
Mp7JrstermReferencePtr Mp7JrsSentencesType::GetinScope() const
{
	return GetBase()->GetinScope();
}

bool Mp7JrsSentencesType::ExistinScope() const
{
	return GetBase()->ExistinScope();
}
void Mp7JrsSentencesType::SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetinScope().");
	}
	GetBase()->SetinScope(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidateinScope()
{
	GetBase()->InvalidateinScope();
}
Mp7JrsLinguisticEntityType_edit_LocalPtr Mp7JrsSentencesType::Getedit() const
{
	return GetBase()->Getedit();
}

bool Mp7JrsSentencesType::Existedit() const
{
	return GetBase()->Existedit();
}
void Mp7JrsSentencesType::Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setedit().");
	}
	GetBase()->Setedit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidateedit()
{
	GetBase()->Invalidateedit();
}
Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr Mp7JrsSentencesType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsLinguisticEntityType_Relation_CollectionPtr Mp7JrsSentencesType::GetRelation() const
{
	return GetBase()->GetRelation();
}

void Mp7JrsSentencesType::SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSentencesType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSentencesType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSentencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSentencesType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSentencesType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSentencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSentencesType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSentencesType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSentencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSentencesType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSentencesType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSentencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSentencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSentencesType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSentencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSentencesType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSentencesType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSentencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSentencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 20 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("synthesis")) == 0)
	{
		// synthesis is simple attribute synthesisType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrssynthesisType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Sentences")) == 0)
	{
		// Sentences is contained in itemtype SentencesType_LocalType
		// in choice collection SentencesType_CollectionType

		context->Found = true;
		Mp7JrsSentencesType_CollectionPtr coll = GetSentencesType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSentencesType_CollectionType; // FTT, check this
				SetSentencesType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetSentences()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSentencesType_LocalPtr item = CreateSentencesType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SentencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSentencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->SetSentences(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetSentences()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Sentence")) == 0)
	{
		// Sentence is contained in itemtype SentencesType_LocalType
		// in choice collection SentencesType_CollectionType

		context->Found = true;
		Mp7JrsSentencesType_CollectionPtr coll = GetSentencesType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSentencesType_CollectionType; // FTT, check this
				SetSentencesType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetSentence()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSentencesType_LocalPtr item = CreateSentencesType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSyntacticConstituentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->SetSentence(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetSentence()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quotation")) == 0)
	{
		// Quotation is contained in itemtype SentencesType_LocalType
		// in choice collection SentencesType_CollectionType

		context->Found = true;
		Mp7JrsSentencesType_CollectionPtr coll = GetSentencesType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSentencesType_CollectionType; // FTT, check this
				SetSentencesType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSentencesType_LocalPtr item = CreateSentencesType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsLinguisticDocumentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->SetQuotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSentencesType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SentencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SentencesType");
	}
	return result;
}

XMLCh * Mp7JrsSentencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSentencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSentencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SentencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_synthesis_Exist)
	{
	// Enumeration
	if(m_synthesis != Mp7JrssynthesisType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrssynthesisType::ToText(m_synthesis);
		element->setAttributeNS(X(""), X("synthesis"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_SentencesType_LocalType != Mp7JrsSentencesType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SentencesType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSentencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("synthesis")))
	{
		this->Setsynthesis(Mp7JrssynthesisType::Parse(parent->getAttribute(X("synthesis"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsLinguisticEntityType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Sentences
			Dc1Util::HasNodeName(parent, X("Sentences"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Sentences")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Sentence
			Dc1Util::HasNodeName(parent, X("Sentence"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Sentence")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Quotation
			Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Quotation")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSentencesType_CollectionPtr tmp = CreateSentencesType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSentencesType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSentencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Sentences"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Sentences")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Sentence"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Sentence")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Quotation")) == 0)
	)
  {
	Mp7JrsSentencesType_CollectionPtr tmp = CreateSentencesType_CollectionType; // FTT, check this
	this->SetSentencesType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSentencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSentencesType_LocalType() != Dc1NodePtr())
		result.Insert(GetSentencesType_LocalType());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSentencesType_ExtMethodImpl.h


