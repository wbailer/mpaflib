
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsPreferenceConditionType_Time_CollectionType.h"
#include "Mp7JrsPreferenceConditionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPreferenceConditionType_Time_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Time

#include <assert.h>
IMp7JrsPreferenceConditionType::IMp7JrsPreferenceConditionType()
{

// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtPropInit.h

}

IMp7JrsPreferenceConditionType::~IMp7JrsPreferenceConditionType()
{
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtPropCleanup.h

}

Mp7JrsPreferenceConditionType::Mp7JrsPreferenceConditionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPreferenceConditionType::~Mp7JrsPreferenceConditionType()
{
	Cleanup();
}

void Mp7JrsPreferenceConditionType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Place = Mp7JrsPlacePtr(); // Class
	m_Place_Exist = false;
	m_Time = Mp7JrsPreferenceConditionType_Time_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtMyPropInit.h

}

void Mp7JrsPreferenceConditionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Place);
	// Dc1Factory::DeleteObject(m_Time);
}

void Mp7JrsPreferenceConditionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PreferenceConditionTypePtr, since we
	// might need GetBase(), which isn't defined in IPreferenceConditionType
	const Dc1Ptr< Mp7JrsPreferenceConditionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPreferenceConditionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidPlace())
	{
		// Dc1Factory::DeleteObject(m_Place);
		this->SetPlace(Dc1Factory::CloneObject(tmp->GetPlace()));
	}
	else
	{
		InvalidatePlace();
	}
		// Dc1Factory::DeleteObject(m_Time);
		this->SetTime(Dc1Factory::CloneObject(tmp->GetTime()));
}

Dc1NodePtr Mp7JrsPreferenceConditionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPreferenceConditionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsPreferenceConditionType::GetBase() const
{
	return m_Base;
}

Mp7JrsPlacePtr Mp7JrsPreferenceConditionType::GetPlace() const
{
		return m_Place;
}

// Element is optional
bool Mp7JrsPreferenceConditionType::IsValidPlace() const
{
	return m_Place_Exist;
}

Mp7JrsPreferenceConditionType_Time_CollectionPtr Mp7JrsPreferenceConditionType::GetTime() const
{
		return m_Time;
}

void Mp7JrsPreferenceConditionType::SetPlace(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType::SetPlace().");
	}
	if (m_Place != item || m_Place_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Place);
		m_Place = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Place) m_Place->SetParent(m_myself.getPointer());
		if (m_Place && m_Place->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Place->UseTypeAttribute = true;
		}
		m_Place_Exist = true;
		if(m_Place != Mp7JrsPlacePtr())
		{
			m_Place->SetContentName(XMLString::transcode("Place"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPreferenceConditionType::InvalidatePlace()
{
	m_Place_Exist = false;
}
void Mp7JrsPreferenceConditionType::SetTime(const Mp7JrsPreferenceConditionType_Time_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType::SetTime().");
	}
	if (m_Time != item)
	{
		// Dc1Factory::DeleteObject(m_Time);
		m_Time = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Time) m_Time->SetParent(m_myself.getPointer());
		if(m_Time != Mp7JrsPreferenceConditionType_Time_CollectionPtr())
		{
			m_Time->SetContentName(XMLString::transcode("Time"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPreferenceConditionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Place")) == 0)
	{
		// Place is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPlace()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPlace(p, client);
					if((p = GetPlace()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Time")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Time is item of type PreferenceConditionType_Time_LocalType
		// in element collection PreferenceConditionType_Time_CollectionType
		
		context->Found = true;
		Mp7JrsPreferenceConditionType_Time_CollectionPtr coll = GetTime();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePreferenceConditionType_Time_CollectionType; // FTT, check this
				SetTime(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType_Time_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPreferenceConditionType_Time_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PreferenceConditionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PreferenceConditionType");
	}
	return result;
}

XMLCh * Mp7JrsPreferenceConditionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPreferenceConditionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPreferenceConditionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PreferenceConditionType"));
	// Element serialization:
	// Class
	
	if (m_Place != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Place->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Place"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Place->Serialize(doc, element, &elem);
	}
	if (m_Time != Mp7JrsPreferenceConditionType_Time_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Time->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPreferenceConditionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Place"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Place")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPlace(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Time"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Time")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPreferenceConditionType_Time_CollectionPtr tmp = CreatePreferenceConditionType_Time_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTime(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPreferenceConditionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Place")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetPlace(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Time")) == 0))
  {
	Mp7JrsPreferenceConditionType_Time_CollectionPtr tmp = CreatePreferenceConditionType_Time_CollectionType; // FTT, check this
	this->SetTime(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPreferenceConditionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPlace() != Dc1NodePtr())
		result.Insert(GetPlace());
	if (GetTime() != Dc1NodePtr())
		result.Insert(GetTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_ExtMethodImpl.h


