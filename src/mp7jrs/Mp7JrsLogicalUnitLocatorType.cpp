
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtImplInclude.h


#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsLogicalUnitLocatorType_CollectionType.h"
#include "Mp7JrsInlineMediaType.h"
#include "Mp7JrsLogicalUnitLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsLogicalUnitLocatorType_LocalType.h" // Choice collection LogicalUnit
#include "Mp7JrsLogicalUnitLocatorType_LogicalUnit_LocalType0.h" // Choice collection element LogicalUnit
#include "Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0.h" // Choice collection element ReferenceUnit

#include <assert.h>
IMp7JrsLogicalUnitLocatorType::IMp7JrsLogicalUnitLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtPropInit.h

}

IMp7JrsLogicalUnitLocatorType::~IMp7JrsLogicalUnitLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtPropCleanup.h

}

Mp7JrsLogicalUnitLocatorType::Mp7JrsLogicalUnitLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsLogicalUnitLocatorType::~Mp7JrsLogicalUnitLocatorType()
{
	Cleanup();
}

void Mp7JrsLogicalUnitLocatorType::Init()
{
	// Init base
	m_Base = CreateMediaLocatorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_LogicalUnitLocatorType_LocalType = Mp7JrsLogicalUnitLocatorType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtMyPropInit.h

}

void Mp7JrsLogicalUnitLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_LocalType);
}

void Mp7JrsLogicalUnitLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use LogicalUnitLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in ILogicalUnitLocatorType
	const Dc1Ptr< Mp7JrsLogicalUnitLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaLocatorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsLogicalUnitLocatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_LocalType);
		this->SetLogicalUnitLocatorType_LocalType(Dc1Factory::CloneObject(tmp->GetLogicalUnitLocatorType_LocalType()));
}

Dc1NodePtr Mp7JrsLogicalUnitLocatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsLogicalUnitLocatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMediaLocatorType > Mp7JrsLogicalUnitLocatorType::GetBase() const
{
	return m_Base;
}

Mp7JrsLogicalUnitLocatorType_CollectionPtr Mp7JrsLogicalUnitLocatorType::GetLogicalUnitLocatorType_LocalType() const
{
		return m_LogicalUnitLocatorType_LocalType;
}

void Mp7JrsLogicalUnitLocatorType::SetLogicalUnitLocatorType_LocalType(const Mp7JrsLogicalUnitLocatorType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType::SetLogicalUnitLocatorType_LocalType().");
	}
	if (m_LogicalUnitLocatorType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_LocalType);
		m_LogicalUnitLocatorType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LogicalUnitLocatorType_LocalType) m_LogicalUnitLocatorType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsLogicalUnitLocatorType::GetMediaUri() const
{
	return GetBase()->GetMediaUri();
}

Mp7JrsInlineMediaPtr Mp7JrsLogicalUnitLocatorType::GetInlineMedia() const
{
	return GetBase()->GetInlineMedia();
}

unsigned Mp7JrsLogicalUnitLocatorType::GetStreamID() const
{
	return GetBase()->GetStreamID();
}

// Element is optional
bool Mp7JrsLogicalUnitLocatorType::IsValidStreamID() const
{
	return GetBase()->IsValidStreamID();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsLogicalUnitLocatorType::SetMediaUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType::SetMediaUri().");
	}
	GetBase()->SetMediaUri(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsLogicalUnitLocatorType::SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType::SetInlineMedia().");
	}
	GetBase()->SetInlineMedia(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLogicalUnitLocatorType::SetStreamID(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType::SetStreamID().");
	}
	GetBase()->SetStreamID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLogicalUnitLocatorType::InvalidateStreamID()
{
	GetBase()->InvalidateStreamID();
}

Dc1NodeEnum Mp7JrsLogicalUnitLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("LogicalUnit")) == 0)
	{
		// LogicalUnit is contained in itemtype LogicalUnitLocatorType_LocalType
		// in choice collection LogicalUnitLocatorType_CollectionType

		context->Found = true;
		Mp7JrsLogicalUnitLocatorType_CollectionPtr coll = GetLogicalUnitLocatorType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateLogicalUnitLocatorType_CollectionType; // FTT, check this
				SetLogicalUnitLocatorType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->GetLogicalUnit()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsLogicalUnitLocatorType_LocalPtr item = CreateLogicalUnitLocatorType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_LogicalUnit_LocalType0")))) != empty)
			{
				// Is type allowed
				Mp7JrsLogicalUnitLocatorType_LogicalUnit_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->SetLogicalUnit(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->GetLogicalUnit()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ReferenceUnit")) == 0)
	{
		// ReferenceUnit is contained in itemtype LogicalUnitLocatorType_LocalType
		// in choice collection LogicalUnitLocatorType_CollectionType

		context->Found = true;
		Mp7JrsLogicalUnitLocatorType_CollectionPtr coll = GetLogicalUnitLocatorType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateLogicalUnitLocatorType_CollectionType; // FTT, check this
				SetLogicalUnitLocatorType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->GetReferenceUnit()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsLogicalUnitLocatorType_LocalPtr item = CreateLogicalUnitLocatorType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogicalUnitLocatorType_ReferenceUnit_LocalType0")))) != empty)
			{
				// Is type allowed
				Mp7JrsLogicalUnitLocatorType_ReferenceUnit_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->SetReferenceUnit(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsLogicalUnitLocatorType_LocalPtr)coll->elementAt(i))->GetReferenceUnit()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for LogicalUnitLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "LogicalUnitLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsLogicalUnitLocatorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsLogicalUnitLocatorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsLogicalUnitLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("LogicalUnitLocatorType"));
	// Element serialization:
	if (m_LogicalUnitLocatorType_LocalType != Mp7JrsLogicalUnitLocatorType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_LogicalUnitLocatorType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsLogicalUnitLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaLocatorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:LogicalUnit
			Dc1Util::HasNodeName(parent, X("LogicalUnit"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:LogicalUnit")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ReferenceUnit
			Dc1Util::HasNodeName(parent, X("ReferenceUnit"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ReferenceUnit")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsLogicalUnitLocatorType_CollectionPtr tmp = CreateLogicalUnitLocatorType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLogicalUnitLocatorType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsLogicalUnitLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("LogicalUnit"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("LogicalUnit")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ReferenceUnit"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ReferenceUnit")) == 0)
	)
  {
	Mp7JrsLogicalUnitLocatorType_CollectionPtr tmp = CreateLogicalUnitLocatorType_CollectionType; // FTT, check this
	this->SetLogicalUnitLocatorType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsLogicalUnitLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLogicalUnitLocatorType_LocalType() != Dc1NodePtr())
		result.Insert(GetLogicalUnitLocatorType_LocalType());
	if (GetInlineMedia() != Dc1NodePtr())
		result.Insert(GetInlineMedia());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsLogicalUnitLocatorType_ExtMethodImpl.h
Mp7JrsmediaTimePointPtr Mp7JrsLogicalUnitLocatorType::GetMediaTimeBase(void) const
{
	return this->GetBase()->GetMediaTimeBase();
}
void Mp7JrsLogicalUnitLocatorType::SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client)
{
	return this->GetBase()->SetMediaTimeBase(timePoint, client);
}
// end extension included


