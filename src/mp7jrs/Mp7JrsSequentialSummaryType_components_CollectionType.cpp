
// Extension library class
// based on ValCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <stdio.h>
#include "Mp7JrsSequentialSummaryType_components_CollectionType.h"
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtImplInclude.h


Mp7JrsSequentialSummaryType_components_CollectionType::Mp7JrsSequentialSummaryType_components_CollectionType()
{
	m_Item = new Dc1ValueVectorOf <Mp7JrsSequentialSummaryType_components_LocalType::Enumeration>(0);
	
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtPropInit.h


}

Mp7JrsSequentialSummaryType_components_CollectionType::~Mp7JrsSequentialSummaryType_components_CollectionType()
{
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtPropCleanup.h


	delete m_Item;
}

void Mp7JrsSequentialSummaryType_components_CollectionType::DeepCopy(const Dc1NodePtr & original)
{
	const Mp7JrsSequentialSummaryType_components_CollectionPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: wrong argument type

	m_Item->removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		m_Item->addElement(tmp->elementAt(i));
	}
}

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtMethodImpl.h


void Mp7JrsSequentialSummaryType_components_CollectionType::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void Mp7JrsSequentialSummaryType_components_CollectionType::addElement(const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration & toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType_components_CollectionType::addElement().");
	}
	m_Item->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType_components_CollectionType::setElementAt(const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration & toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType_components_CollectionType::setElementAt().");
	}
	m_Item->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType_components_CollectionType::insertElementAt(const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration & toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType_components_CollectionType::insertElementAt().");
	}
	m_Item->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType_components_CollectionType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType_components_CollectionType::removeAllElements().");
	}
	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType_components_CollectionType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType_components_CollectionType::removeElementAt().");
	}
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsSequentialSummaryType_components_CollectionType::containsElement(const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration &toCheck)
{
	return m_Item->containsElement(toCheck);
}

unsigned int Mp7JrsSequentialSummaryType_components_CollectionType::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration Mp7JrsSequentialSummaryType_components_CollectionType::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

Mp7JrsSequentialSummaryType_components_LocalType::Enumeration Mp7JrsSequentialSummaryType_components_CollectionType::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

unsigned int Mp7JrsSequentialSummaryType_components_CollectionType::size() const
{
	return (unsigned int)m_Item->size();
}

void Mp7JrsSequentialSummaryType_components_CollectionType::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration * Mp7JrsSequentialSummaryType_components_CollectionType::rawData() const
{
	return m_Item->rawData();
}

int Mp7JrsSequentialSummaryType_components_CollectionType::elementIndexOf(const Mp7JrsSequentialSummaryType_components_LocalType::Enumeration &toCheck) const
{
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		if(m_Item->elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1; // Not found
}


XMLCh * Mp7JrsSequentialSummaryType_components_CollectionType::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * tmp = Mp7JrsSequentialSummaryType_components_LocalType::ToText(m_Item->elementAt(i));
		Dc1Util::CatString(&buf, tmp);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		if(i < m_Item->size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer
	return buf;
}

bool Mp7JrsSequentialSummaryType_components_CollectionType::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(txt);
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			m_Item->addElement(Mp7JrsSequentialSummaryType_components_LocalType::Parse(tmp->elementAt(i)));
		}
	}
	delete tmp; // FTT release?
	return m_Item->size() > 0;
}

bool Mp7JrsSequentialSummaryType_components_CollectionType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsSequentialSummaryType_components_CollectionType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsSequentialSummaryType_components_CollectionType::ContentToString() const
{
	// Mp7JrsSequentialSummaryType_components_CollectionType has no attributes so forward it to ToText()
	return ToText();
}

/**

 * Item type is TextOnly.
 */
void Mp7JrsSequentialSummaryType_components_CollectionType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
		if(this->GetContentName() != (XMLCh*)NULL)
		{
// OLD 			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			element->appendChild(elem);
			*newElem = elem;
			element = elem;
		}
	} else {
		element = doc->getDocumentElement();
	}
	*newElem = element;

	if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
	{
		// FTT This has to be solved in all classes using  setAttribute(X("xsi:type")!
		element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SequentialSummaryType_components_CollectionType")))));
	}

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtPreSerialize.h


	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * txt = Mp7JrsSequentialSummaryType_components_LocalType::ToText(m_Item->elementAt(i));
		element->appendChild(doc->createTextNode(txt));
		if(i < m_Item->size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&txt);
	}
}





bool Mp7JrsSequentialSummaryType_components_CollectionType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
	if(parent == NULL)
	{
		return false;
	}
	* current = parent; // Init with first element - check this!

	// Deserialize enum collection
	// Collection is simpleType list
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			m_Item->addElement(Mp7JrsSequentialSummaryType_components_LocalType::Parse(tmp->elementAt(i)));
			// TODO Check do we need elementnames?
		}
	}
	delete tmp; // FTT release?

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_components_CollectionType_ExtPostDeserialize.h


	return true;
}
