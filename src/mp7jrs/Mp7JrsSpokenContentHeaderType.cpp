
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsSpokenContentHeaderType_CollectionType.h"
#include "Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionType.h"
#include "Mp7JrsSpokenContentHeaderType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSpokenContentHeaderType_LocalType.h" // Choice collection WordLexicon
#include "Mp7JrsWordLexiconType.h" // Choice collection element WordLexicon
#include "Mp7JrsPhoneLexiconType.h" // Choice collection element PhoneLexicon
#include "Mp7JrsConfusionCountType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ConfusionInfo
#include "Mp7JrsSpeakerInfoType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SpeakerInfo

#include <assert.h>
IMp7JrsSpokenContentHeaderType::IMp7JrsSpokenContentHeaderType()
{

// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtPropInit.h

}

IMp7JrsSpokenContentHeaderType::~IMp7JrsSpokenContentHeaderType()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtPropCleanup.h

}

Mp7JrsSpokenContentHeaderType::Mp7JrsSpokenContentHeaderType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpokenContentHeaderType::~Mp7JrsSpokenContentHeaderType()
{
	Cleanup();
}

void Mp7JrsSpokenContentHeaderType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SpokenContentHeaderType_LocalType = Mp7JrsSpokenContentHeaderType_CollectionPtr(); // Collection
	m_ConfusionInfo = Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr(); // Collection
	m_DescriptionMetadata = Mp7JrsDescriptionMetadataPtr(); // Class
	m_DescriptionMetadata_Exist = false;
	m_SpeakerInfo = Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtMyPropInit.h

}

void Mp7JrsSpokenContentHeaderType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SpokenContentHeaderType_LocalType);
	// Dc1Factory::DeleteObject(m_ConfusionInfo);
	// Dc1Factory::DeleteObject(m_DescriptionMetadata);
	// Dc1Factory::DeleteObject(m_SpeakerInfo);
}

void Mp7JrsSpokenContentHeaderType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpokenContentHeaderTypePtr, since we
	// might need GetBase(), which isn't defined in ISpokenContentHeaderType
	const Dc1Ptr< Mp7JrsSpokenContentHeaderType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSpokenContentHeaderType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SpokenContentHeaderType_LocalType);
		this->SetSpokenContentHeaderType_LocalType(Dc1Factory::CloneObject(tmp->GetSpokenContentHeaderType_LocalType()));
		// Dc1Factory::DeleteObject(m_ConfusionInfo);
		this->SetConfusionInfo(Dc1Factory::CloneObject(tmp->GetConfusionInfo()));
	if (tmp->IsValidDescriptionMetadata())
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		this->SetDescriptionMetadata(Dc1Factory::CloneObject(tmp->GetDescriptionMetadata()));
	}
	else
	{
		InvalidateDescriptionMetadata();
	}
		// Dc1Factory::DeleteObject(m_SpeakerInfo);
		this->SetSpeakerInfo(Dc1Factory::CloneObject(tmp->GetSpeakerInfo()));
}

Dc1NodePtr Mp7JrsSpokenContentHeaderType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSpokenContentHeaderType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsSpokenContentHeaderType::GetBase() const
{
	return m_Base;
}

Mp7JrsSpokenContentHeaderType_CollectionPtr Mp7JrsSpokenContentHeaderType::GetSpokenContentHeaderType_LocalType() const
{
		return m_SpokenContentHeaderType_LocalType;
}

Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr Mp7JrsSpokenContentHeaderType::GetConfusionInfo() const
{
		return m_ConfusionInfo;
}

Mp7JrsDescriptionMetadataPtr Mp7JrsSpokenContentHeaderType::GetDescriptionMetadata() const
{
		return m_DescriptionMetadata;
}

// Element is optional
bool Mp7JrsSpokenContentHeaderType::IsValidDescriptionMetadata() const
{
	return m_DescriptionMetadata_Exist;
}

Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr Mp7JrsSpokenContentHeaderType::GetSpeakerInfo() const
{
		return m_SpeakerInfo;
}

void Mp7JrsSpokenContentHeaderType::SetSpokenContentHeaderType_LocalType(const Mp7JrsSpokenContentHeaderType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType::SetSpokenContentHeaderType_LocalType().");
	}
	if (m_SpokenContentHeaderType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SpokenContentHeaderType_LocalType);
		m_SpokenContentHeaderType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpokenContentHeaderType_LocalType) m_SpokenContentHeaderType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentHeaderType::SetConfusionInfo(const Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType::SetConfusionInfo().");
	}
	if (m_ConfusionInfo != item)
	{
		// Dc1Factory::DeleteObject(m_ConfusionInfo);
		m_ConfusionInfo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ConfusionInfo) m_ConfusionInfo->SetParent(m_myself.getPointer());
		if(m_ConfusionInfo != Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr())
		{
			m_ConfusionInfo->SetContentName(XMLString::transcode("ConfusionInfo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentHeaderType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType::SetDescriptionMetadata().");
	}
	if (m_DescriptionMetadata != item || m_DescriptionMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		m_DescriptionMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionMetadata) m_DescriptionMetadata->SetParent(m_myself.getPointer());
		if (m_DescriptionMetadata && m_DescriptionMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DescriptionMetadata->UseTypeAttribute = true;
		}
		m_DescriptionMetadata_Exist = true;
		if(m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
		{
			m_DescriptionMetadata->SetContentName(XMLString::transcode("DescriptionMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentHeaderType::InvalidateDescriptionMetadata()
{
	m_DescriptionMetadata_Exist = false;
}
void Mp7JrsSpokenContentHeaderType::SetSpeakerInfo(const Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType::SetSpeakerInfo().");
	}
	if (m_SpeakerInfo != item)
	{
		// Dc1Factory::DeleteObject(m_SpeakerInfo);
		m_SpeakerInfo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpeakerInfo) m_SpeakerInfo->SetParent(m_myself.getPointer());
		if(m_SpeakerInfo != Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr())
		{
			m_SpeakerInfo->SetContentName(XMLString::transcode("SpeakerInfo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSpokenContentHeaderType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSpokenContentHeaderType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSpokenContentHeaderType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentHeaderType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentHeaderType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsSpokenContentHeaderType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("WordLexicon")) == 0)
	{
		// WordLexicon is contained in itemtype SpokenContentHeaderType_LocalType
		// in choice collection SpokenContentHeaderType_CollectionType

		context->Found = true;
		Mp7JrsSpokenContentHeaderType_CollectionPtr coll = GetSpokenContentHeaderType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentHeaderType_CollectionType; // FTT, check this
				SetSpokenContentHeaderType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->GetWordLexicon()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpokenContentHeaderType_LocalPtr item = CreateSpokenContentHeaderType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordLexiconType")))) != empty)
			{
				// Is type allowed
				Mp7JrsWordLexiconPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->SetWordLexicon(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->GetWordLexicon()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PhoneLexicon")) == 0)
	{
		// PhoneLexicon is contained in itemtype SpokenContentHeaderType_LocalType
		// in choice collection SpokenContentHeaderType_CollectionType

		context->Found = true;
		Mp7JrsSpokenContentHeaderType_CollectionPtr coll = GetSpokenContentHeaderType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentHeaderType_CollectionType; // FTT, check this
				SetSpokenContentHeaderType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->GetPhoneLexicon()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpokenContentHeaderType_LocalPtr item = CreateSpokenContentHeaderType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneLexiconType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPhoneLexiconPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->SetPhoneLexicon(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpokenContentHeaderType_LocalPtr)coll->elementAt(i))->GetPhoneLexicon()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ConfusionInfo")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ConfusionInfo is item of type ConfusionCountType
		// in element collection SpokenContentHeaderType_ConfusionInfo_CollectionType
		
		context->Found = true;
		Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr coll = GetConfusionInfo();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentHeaderType_ConfusionInfo_CollectionType; // FTT, check this
				SetConfusionInfo(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConfusionCountType")))) != empty)
			{
				// Is type allowed
				Mp7JrsConfusionCountPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DescriptionMetadata")) == 0)
	{
		// DescriptionMetadata is simple element DescriptionMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptionMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptionMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptionMetadata(p, client);
					if((p = GetDescriptionMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpeakerInfo")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SpeakerInfo is item of type SpeakerInfoType
		// in element collection SpokenContentHeaderType_SpeakerInfo_CollectionType
		
		context->Found = true;
		Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr coll = GetSpeakerInfo();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentHeaderType_SpeakerInfo_CollectionType; // FTT, check this
				SetSpeakerInfo(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpeakerInfoPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpokenContentHeaderType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpokenContentHeaderType");
	}
	return result;
}

XMLCh * Mp7JrsSpokenContentHeaderType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSpokenContentHeaderType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSpokenContentHeaderType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpokenContentHeaderType"));
	// Element serialization:
	if (m_SpokenContentHeaderType_LocalType != Mp7JrsSpokenContentHeaderType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpokenContentHeaderType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ConfusionInfo != Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ConfusionInfo->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DescriptionMetadata->Serialize(doc, element, &elem);
	}
	if (m_SpeakerInfo != Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpeakerInfo->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpokenContentHeaderType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:WordLexicon
			Dc1Util::HasNodeName(parent, X("WordLexicon"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:WordLexicon")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:PhoneLexicon
			Dc1Util::HasNodeName(parent, X("PhoneLexicon"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PhoneLexicon")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentHeaderType_CollectionPtr tmp = CreateSpokenContentHeaderType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpokenContentHeaderType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ConfusionInfo"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ConfusionInfo")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr tmp = CreateSpokenContentHeaderType_ConfusionInfo_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetConfusionInfo(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionMetadata"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDescriptionMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SpeakerInfo"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SpeakerInfo")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr tmp = CreateSpokenContentHeaderType_SpeakerInfo_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpeakerInfo(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpokenContentHeaderType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("WordLexicon"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("WordLexicon")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("PhoneLexicon"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("PhoneLexicon")) == 0)
	)
  {
	Mp7JrsSpokenContentHeaderType_CollectionPtr tmp = CreateSpokenContentHeaderType_CollectionType; // FTT, check this
	this->SetSpokenContentHeaderType_LocalType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ConfusionInfo")) == 0))
  {
	Mp7JrsSpokenContentHeaderType_ConfusionInfo_CollectionPtr tmp = CreateSpokenContentHeaderType_ConfusionInfo_CollectionType; // FTT, check this
	this->SetConfusionInfo(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("DescriptionMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDescriptionMetadataType; // FTT, check this
	}
	this->SetDescriptionMetadata(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SpeakerInfo")) == 0))
  {
	Mp7JrsSpokenContentHeaderType_SpeakerInfo_CollectionPtr tmp = CreateSpokenContentHeaderType_SpeakerInfo_CollectionType; // FTT, check this
	this->SetSpeakerInfo(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpokenContentHeaderType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSpokenContentHeaderType_LocalType() != Dc1NodePtr())
		result.Insert(GetSpokenContentHeaderType_LocalType());
	if (GetConfusionInfo() != Dc1NodePtr())
		result.Insert(GetConfusionInfo());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetSpeakerInfo() != Dc1NodePtr())
		result.Insert(GetSpeakerInfo());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpokenContentHeaderType_ExtMethodImpl.h


