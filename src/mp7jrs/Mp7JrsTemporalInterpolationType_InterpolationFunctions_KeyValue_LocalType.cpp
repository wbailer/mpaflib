
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtImplInclude.h


#include "Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtPropInit.h

}

IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::~IMp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtPropCleanup.h

}

Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::~Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType()
{
	Cleanup();
}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Init()
{
	// Init base
	m_Base = 0.0f;

	// Init attributes
	m_type = Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::UninitializedEnumeration;
	m_type_Default = Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::firstOrder; // Default enumeration
	m_type_Exist = false;
	m_param = 0.0f; // Value
	m_param_Default = 0.0; // Default value
	m_param_Exist = false;



// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtMyPropInit.h

}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ITemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType
	const Dc1Ptr< Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	if (tmp->Existtype())
	{
		this->Settype(tmp->Gettype());
	}
	else
	{
		Invalidatetype();
	}
	}
	{
	if (tmp->Existparam())
	{
		this->Setparam(tmp->Getparam());
	}
	else
	{
		Invalidateparam();
	}
	}
}

Dc1NodePtr Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::SetContent(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::GetContent() const
{
	return m_Base;
}
Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::Enumeration Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Gettype() const
{
	if (this->Existtype()) {
		return m_type;
	} else {
		return m_type_Default;
	}
}

bool Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Settype(Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Settype().");
	}
	m_type = item;
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Invalidatetype()
{
	m_type_Exist = false;
}
float Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Getparam() const
{
	if (this->Existparam()) {
		return m_param;
	} else {
		return m_param_Default;
	}
}

bool Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Existparam() const
{
	return m_param_Exist;
}
void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Setparam(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Setparam().");
	}
	m_param = item;
	m_param_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Invalidateparam()
{
	m_param_Exist = false;
}

Dc1NodeEnum Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute TemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("param")) == 0)
	{
		// param is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::ContentToString() const
{
	// Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Enumeration
	if(m_type != Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::ToText(m_type);
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_param_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_param);
		element->setAttributeNS(X(""), X("param"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		this->Settype(Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_type_LocalType::Parse(parent->getAttribute(X("type"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("param")))
	{
		// deserialize value type
		this->Setparam(Dc1Convert::TextToFloat(parent->getAttribute(X("param"))));
		* current = parent;
	}

  m_Base = Dc1Convert::TextToFloat(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_InterpolationFunctions_KeyValue_LocalType_ExtMethodImpl.h


