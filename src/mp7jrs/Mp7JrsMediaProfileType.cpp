
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionType.h"
#include "Mp7JrsMediaFormatType.h"
#include "Mp7JrsMediaTranscodingHintsType.h"
#include "Mp7JrsMediaQualityType.h"
#include "Mp7JrsMediaProfileType_MediaInstance_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMediaProfileType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaProfileType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ComponentMediaProfile
#include "Mp7JrsMediaInstanceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaInstance

#include <assert.h>
IMp7JrsMediaProfileType::IMp7JrsMediaProfileType()
{

// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtPropInit.h

}

IMp7JrsMediaProfileType::~IMp7JrsMediaProfileType()
{
// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtPropCleanup.h

}

Mp7JrsMediaProfileType::Mp7JrsMediaProfileType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaProfileType::~Mp7JrsMediaProfileType()
{
	Cleanup();
}

void Mp7JrsMediaProfileType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_master = false; // Value
	m_master_Default = false; // Default value
	m_master_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_ComponentMediaProfile = Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr(); // Collection
	m_MediaFormat = Mp7JrsMediaFormatPtr(); // Class
	m_MediaFormat_Exist = false;
	m_MediaTranscodingHints = Mp7JrsMediaTranscodingHintsPtr(); // Class
	m_MediaTranscodingHints_Exist = false;
	m_MediaQuality = Mp7JrsMediaQualityPtr(); // Class
	m_MediaQuality_Exist = false;
	m_MediaInstance = Mp7JrsMediaProfileType_MediaInstance_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtMyPropInit.h

}

void Mp7JrsMediaProfileType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ComponentMediaProfile);
	// Dc1Factory::DeleteObject(m_MediaFormat);
	// Dc1Factory::DeleteObject(m_MediaTranscodingHints);
	// Dc1Factory::DeleteObject(m_MediaQuality);
	// Dc1Factory::DeleteObject(m_MediaInstance);
}

void Mp7JrsMediaProfileType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaProfileTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaProfileType
	const Dc1Ptr< Mp7JrsMediaProfileType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaProfileType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existmaster())
	{
		this->Setmaster(tmp->Getmaster());
	}
	else
	{
		Invalidatemaster();
	}
	}
		// Dc1Factory::DeleteObject(m_ComponentMediaProfile);
		this->SetComponentMediaProfile(Dc1Factory::CloneObject(tmp->GetComponentMediaProfile()));
	if (tmp->IsValidMediaFormat())
	{
		// Dc1Factory::DeleteObject(m_MediaFormat);
		this->SetMediaFormat(Dc1Factory::CloneObject(tmp->GetMediaFormat()));
	}
	else
	{
		InvalidateMediaFormat();
	}
	if (tmp->IsValidMediaTranscodingHints())
	{
		// Dc1Factory::DeleteObject(m_MediaTranscodingHints);
		this->SetMediaTranscodingHints(Dc1Factory::CloneObject(tmp->GetMediaTranscodingHints()));
	}
	else
	{
		InvalidateMediaTranscodingHints();
	}
	if (tmp->IsValidMediaQuality())
	{
		// Dc1Factory::DeleteObject(m_MediaQuality);
		this->SetMediaQuality(Dc1Factory::CloneObject(tmp->GetMediaQuality()));
	}
	else
	{
		InvalidateMediaQuality();
	}
		// Dc1Factory::DeleteObject(m_MediaInstance);
		this->SetMediaInstance(Dc1Factory::CloneObject(tmp->GetMediaInstance()));
}

Dc1NodePtr Mp7JrsMediaProfileType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaProfileType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsMediaProfileType::GetBase() const
{
	return m_Base;
}

bool Mp7JrsMediaProfileType::Getmaster() const
{
	if (this->Existmaster()) {
		return m_master;
	} else {
		return m_master_Default;
	}
}

bool Mp7JrsMediaProfileType::Existmaster() const
{
	return m_master_Exist;
}
void Mp7JrsMediaProfileType::Setmaster(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::Setmaster().");
	}
	m_master = item;
	m_master_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::Invalidatemaster()
{
	m_master_Exist = false;
}
Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr Mp7JrsMediaProfileType::GetComponentMediaProfile() const
{
		return m_ComponentMediaProfile;
}

Mp7JrsMediaFormatPtr Mp7JrsMediaProfileType::GetMediaFormat() const
{
		return m_MediaFormat;
}

// Element is optional
bool Mp7JrsMediaProfileType::IsValidMediaFormat() const
{
	return m_MediaFormat_Exist;
}

Mp7JrsMediaTranscodingHintsPtr Mp7JrsMediaProfileType::GetMediaTranscodingHints() const
{
		return m_MediaTranscodingHints;
}

// Element is optional
bool Mp7JrsMediaProfileType::IsValidMediaTranscodingHints() const
{
	return m_MediaTranscodingHints_Exist;
}

Mp7JrsMediaQualityPtr Mp7JrsMediaProfileType::GetMediaQuality() const
{
		return m_MediaQuality;
}

// Element is optional
bool Mp7JrsMediaProfileType::IsValidMediaQuality() const
{
	return m_MediaQuality_Exist;
}

Mp7JrsMediaProfileType_MediaInstance_CollectionPtr Mp7JrsMediaProfileType::GetMediaInstance() const
{
		return m_MediaInstance;
}

void Mp7JrsMediaProfileType::SetComponentMediaProfile(const Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetComponentMediaProfile().");
	}
	if (m_ComponentMediaProfile != item)
	{
		// Dc1Factory::DeleteObject(m_ComponentMediaProfile);
		m_ComponentMediaProfile = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ComponentMediaProfile) m_ComponentMediaProfile->SetParent(m_myself.getPointer());
		if(m_ComponentMediaProfile != Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr())
		{
			m_ComponentMediaProfile->SetContentName(XMLString::transcode("ComponentMediaProfile"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::SetMediaFormat(const Mp7JrsMediaFormatPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetMediaFormat().");
	}
	if (m_MediaFormat != item || m_MediaFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaFormat);
		m_MediaFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaFormat) m_MediaFormat->SetParent(m_myself.getPointer());
		if (m_MediaFormat && m_MediaFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaFormat->UseTypeAttribute = true;
		}
		m_MediaFormat_Exist = true;
		if(m_MediaFormat != Mp7JrsMediaFormatPtr())
		{
			m_MediaFormat->SetContentName(XMLString::transcode("MediaFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidateMediaFormat()
{
	m_MediaFormat_Exist = false;
}
void Mp7JrsMediaProfileType::SetMediaTranscodingHints(const Mp7JrsMediaTranscodingHintsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetMediaTranscodingHints().");
	}
	if (m_MediaTranscodingHints != item || m_MediaTranscodingHints_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaTranscodingHints);
		m_MediaTranscodingHints = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTranscodingHints) m_MediaTranscodingHints->SetParent(m_myself.getPointer());
		if (m_MediaTranscodingHints && m_MediaTranscodingHints->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTranscodingHints->UseTypeAttribute = true;
		}
		m_MediaTranscodingHints_Exist = true;
		if(m_MediaTranscodingHints != Mp7JrsMediaTranscodingHintsPtr())
		{
			m_MediaTranscodingHints->SetContentName(XMLString::transcode("MediaTranscodingHints"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidateMediaTranscodingHints()
{
	m_MediaTranscodingHints_Exist = false;
}
void Mp7JrsMediaProfileType::SetMediaQuality(const Mp7JrsMediaQualityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetMediaQuality().");
	}
	if (m_MediaQuality != item || m_MediaQuality_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaQuality);
		m_MediaQuality = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaQuality) m_MediaQuality->SetParent(m_myself.getPointer());
		if (m_MediaQuality && m_MediaQuality->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaQuality->UseTypeAttribute = true;
		}
		m_MediaQuality_Exist = true;
		if(m_MediaQuality != Mp7JrsMediaQualityPtr())
		{
			m_MediaQuality->SetContentName(XMLString::transcode("MediaQuality"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidateMediaQuality()
{
	m_MediaQuality_Exist = false;
}
void Mp7JrsMediaProfileType::SetMediaInstance(const Mp7JrsMediaProfileType_MediaInstance_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetMediaInstance().");
	}
	if (m_MediaInstance != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInstance);
		m_MediaInstance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInstance) m_MediaInstance->SetParent(m_myself.getPointer());
		if(m_MediaInstance != Mp7JrsMediaProfileType_MediaInstance_CollectionPtr())
		{
			m_MediaInstance->SetContentName(XMLString::transcode("MediaInstance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMediaProfileType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsMediaProfileType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsMediaProfileType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMediaProfileType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsMediaProfileType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsMediaProfileType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMediaProfileType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsMediaProfileType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsMediaProfileType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMediaProfileType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMediaProfileType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMediaProfileType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMediaProfileType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMediaProfileType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMediaProfileType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaProfileType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMediaProfileType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsMediaProfileType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaProfileType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaProfileType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("master")) == 0)
	{
		// master is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ComponentMediaProfile")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ComponentMediaProfile is item of type MediaProfileType
		// in element collection MediaProfileType_ComponentMediaProfile_CollectionType
		
		context->Found = true;
		Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr coll = GetComponentMediaProfile();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaProfileType_ComponentMediaProfile_CollectionType; // FTT, check this
				SetComponentMediaProfile(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaProfileType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaProfilePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaFormat")) == 0)
	{
		// MediaFormat is simple element MediaFormatType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaFormat(p, client);
					if((p = GetMediaFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTranscodingHints")) == 0)
	{
		// MediaTranscodingHints is simple element MediaTranscodingHintsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTranscodingHints()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTranscodingHintsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTranscodingHints(p, client);
					if((p = GetMediaTranscodingHints()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaQuality")) == 0)
	{
		// MediaQuality is simple element MediaQualityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaQuality()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaQualityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaQuality(p, client);
					if((p = GetMediaQuality()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaInstance")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaInstance is item of type MediaInstanceType
		// in element collection MediaProfileType_MediaInstance_CollectionType
		
		context->Found = true;
		Mp7JrsMediaProfileType_MediaInstance_CollectionPtr coll = GetMediaInstance();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaProfileType_MediaInstance_CollectionType; // FTT, check this
				SetMediaInstance(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInstanceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaInstancePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaProfileType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaProfileType");
	}
	return result;
}

XMLCh * Mp7JrsMediaProfileType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaProfileType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaProfileType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaProfileType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_master_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_master);
		element->setAttributeNS(X(""), X("master"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_ComponentMediaProfile != Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ComponentMediaProfile->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MediaFormat != Mp7JrsMediaFormatPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaFormat->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaTranscodingHints != Mp7JrsMediaTranscodingHintsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTranscodingHints->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTranscodingHints"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTranscodingHints->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaQuality != Mp7JrsMediaQualityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaQuality->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaQuality"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaQuality->Serialize(doc, element, &elem);
	}
	if (m_MediaInstance != Mp7JrsMediaProfileType_MediaInstance_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaInstance->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMediaProfileType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("master")))
	{
		// deserialize value type
		this->Setmaster(Dc1Convert::TextToBool(parent->getAttribute(X("master"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ComponentMediaProfile"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ComponentMediaProfile")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr tmp = CreateMediaProfileType_ComponentMediaProfile_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetComponentMediaProfile(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaFormat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTranscodingHints"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTranscodingHints")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTranscodingHintsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTranscodingHints(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaQuality"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaQuality")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaQualityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaQuality(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaInstance"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaInstance")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaProfileType_MediaInstance_CollectionPtr tmp = CreateMediaProfileType_MediaInstance_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaInstance(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaProfileType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ComponentMediaProfile")) == 0))
  {
	Mp7JrsMediaProfileType_ComponentMediaProfile_CollectionPtr tmp = CreateMediaProfileType_ComponentMediaProfile_CollectionType; // FTT, check this
	this->SetComponentMediaProfile(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MediaFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType; // FTT, check this
	}
	this->SetMediaFormat(child);
  }
  if (XMLString::compareString(elementname, X("MediaTranscodingHints")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTranscodingHintsType; // FTT, check this
	}
	this->SetMediaTranscodingHints(child);
  }
  if (XMLString::compareString(elementname, X("MediaQuality")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaQualityType; // FTT, check this
	}
	this->SetMediaQuality(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaInstance")) == 0))
  {
	Mp7JrsMediaProfileType_MediaInstance_CollectionPtr tmp = CreateMediaProfileType_MediaInstance_CollectionType; // FTT, check this
	this->SetMediaInstance(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaProfileType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetComponentMediaProfile() != Dc1NodePtr())
		result.Insert(GetComponentMediaProfile());
	if (GetMediaFormat() != Dc1NodePtr())
		result.Insert(GetMediaFormat());
	if (GetMediaTranscodingHints() != Dc1NodePtr())
		result.Insert(GetMediaTranscodingHints());
	if (GetMediaQuality() != Dc1NodePtr())
		result.Insert(GetMediaQuality());
	if (GetMediaInstance() != Dc1NodePtr())
		result.Insert(GetMediaInstance());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaProfileType_ExtMethodImpl.h


