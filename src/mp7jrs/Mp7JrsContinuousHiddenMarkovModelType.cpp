
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtImplInclude.h


#include "Mp7JrsStateTransitionModelType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType.h"
#include "Mp7JrsProbabilityMatrixType.h"
#include "Mp7JrsStateTransitionModelType_State_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsModelStateType.h" // Element collection urn:mpeg:mpeg7:schema:2004:State
#include "Mp7JrsDescriptorModelType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DescriptorModel
#include "Mp7JrsContinuousDistributionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ObservationDistribution

#include <assert.h>
IMp7JrsContinuousHiddenMarkovModelType::IMp7JrsContinuousHiddenMarkovModelType()
{

// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtPropInit.h

}

IMp7JrsContinuousHiddenMarkovModelType::~IMp7JrsContinuousHiddenMarkovModelType()
{
// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtPropCleanup.h

}

Mp7JrsContinuousHiddenMarkovModelType::Mp7JrsContinuousHiddenMarkovModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsContinuousHiddenMarkovModelType::~Mp7JrsContinuousHiddenMarkovModelType()
{
	Cleanup();
}

void Mp7JrsContinuousHiddenMarkovModelType::Init()
{
	// Init base
	m_Base = CreateStateTransitionModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_DescriptorModel = Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr(); // Collection
	m_ObservationDistribution = Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtMyPropInit.h

}

void Mp7JrsContinuousHiddenMarkovModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_DescriptorModel);
	// Dc1Factory::DeleteObject(m_ObservationDistribution);
}

void Mp7JrsContinuousHiddenMarkovModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContinuousHiddenMarkovModelTypePtr, since we
	// might need GetBase(), which isn't defined in IContinuousHiddenMarkovModelType
	const Dc1Ptr< Mp7JrsContinuousHiddenMarkovModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsStateTransitionModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsContinuousHiddenMarkovModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_DescriptorModel);
		this->SetDescriptorModel(Dc1Factory::CloneObject(tmp->GetDescriptorModel()));
		// Dc1Factory::DeleteObject(m_ObservationDistribution);
		this->SetObservationDistribution(Dc1Factory::CloneObject(tmp->GetObservationDistribution()));
}

Dc1NodePtr Mp7JrsContinuousHiddenMarkovModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsContinuousHiddenMarkovModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsStateTransitionModelType > Mp7JrsContinuousHiddenMarkovModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr Mp7JrsContinuousHiddenMarkovModelType::GetDescriptorModel() const
{
		return m_DescriptorModel;
}

Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr Mp7JrsContinuousHiddenMarkovModelType::GetObservationDistribution() const
{
		return m_ObservationDistribution;
}

void Mp7JrsContinuousHiddenMarkovModelType::SetDescriptorModel(const Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetDescriptorModel().");
	}
	if (m_DescriptorModel != item)
	{
		// Dc1Factory::DeleteObject(m_DescriptorModel);
		m_DescriptorModel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptorModel) m_DescriptorModel->SetParent(m_myself.getPointer());
		if(m_DescriptorModel != Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr())
		{
			m_DescriptorModel->SetContentName(XMLString::transcode("DescriptorModel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::SetObservationDistribution(const Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetObservationDistribution().");
	}
	if (m_ObservationDistribution != item)
	{
		// Dc1Factory::DeleteObject(m_ObservationDistribution);
		m_ObservationDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ObservationDistribution) m_ObservationDistribution->SetParent(m_myself.getPointer());
		if(m_ObservationDistribution != Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr())
		{
			m_ObservationDistribution->SetContentName(XMLString::transcode("ObservationDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsProbabilityMatrixPtr Mp7JrsContinuousHiddenMarkovModelType::GetInitial() const
{
	return GetBase()->GetInitial();
}

// Element is optional
bool Mp7JrsContinuousHiddenMarkovModelType::IsValidInitial() const
{
	return GetBase()->IsValidInitial();
}

Mp7JrsProbabilityMatrixPtr Mp7JrsContinuousHiddenMarkovModelType::GetTransitions() const
{
	return GetBase()->GetTransitions();
}

Mp7JrsStateTransitionModelType_State_CollectionPtr Mp7JrsContinuousHiddenMarkovModelType::GetState() const
{
	return GetBase()->GetState();
}

void Mp7JrsContinuousHiddenMarkovModelType::SetInitial(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetInitial().");
	}
	GetBase()->SetInitial(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::InvalidateInitial()
{
	GetBase()->InvalidateInitial();
}
void Mp7JrsContinuousHiddenMarkovModelType::SetTransitions(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetTransitions().");
	}
	GetBase()->SetTransitions(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::SetState(const Mp7JrsStateTransitionModelType_State_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetState().");
	}
	GetBase()->SetState(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsContinuousHiddenMarkovModelType::GetnumOfStates() const
{
	return GetBase()->GetBase()->GetnumOfStates();
}

void Mp7JrsContinuousHiddenMarkovModelType::SetnumOfStates(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetnumOfStates().");
	}
	GetBase()->GetBase()->SetnumOfStates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsContinuousHiddenMarkovModelType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsContinuousHiddenMarkovModelType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsContinuousHiddenMarkovModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsContinuousHiddenMarkovModelType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsContinuousHiddenMarkovModelType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsContinuousHiddenMarkovModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsContinuousHiddenMarkovModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsContinuousHiddenMarkovModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsContinuousHiddenMarkovModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsContinuousHiddenMarkovModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsContinuousHiddenMarkovModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsContinuousHiddenMarkovModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsContinuousHiddenMarkovModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsContinuousHiddenMarkovModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsContinuousHiddenMarkovModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsContinuousHiddenMarkovModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsContinuousHiddenMarkovModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsContinuousHiddenMarkovModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsContinuousHiddenMarkovModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsContinuousHiddenMarkovModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsContinuousHiddenMarkovModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContinuousHiddenMarkovModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsContinuousHiddenMarkovModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsContinuousHiddenMarkovModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContinuousHiddenMarkovModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsContinuousHiddenMarkovModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DescriptorModel")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DescriptorModel is item of type DescriptorModelType
		// in element collection ContinuousHiddenMarkovModelType_DescriptorModel_CollectionType
		
		context->Found = true;
		Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr coll = GetDescriptorModel();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContinuousHiddenMarkovModelType_DescriptorModel_CollectionType; // FTT, check this
				SetDescriptorModel(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptorModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ObservationDistribution")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ObservationDistribution is item of abstract type ContinuousDistributionType
		// in element collection ContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType
		
		context->Found = true;
		Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr coll = GetObservationDistribution();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType; // FTT, check this
				SetObservationDistribution(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsContinuousDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ContinuousHiddenMarkovModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ContinuousHiddenMarkovModelType");
	}
	return result;
}

XMLCh * Mp7JrsContinuousHiddenMarkovModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsContinuousHiddenMarkovModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsContinuousHiddenMarkovModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ContinuousHiddenMarkovModelType"));
	// Element serialization:
	if (m_DescriptorModel != Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DescriptorModel->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ObservationDistribution != Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ObservationDistribution->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsContinuousHiddenMarkovModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsStateTransitionModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DescriptorModel"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DescriptorModel")) == 0))
		{
			// Deserialize factory type
			Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr tmp = CreateContinuousHiddenMarkovModelType_DescriptorModel_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptorModel(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ObservationDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ObservationDistribution")) == 0))
		{
			// Deserialize factory type
			Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr tmp = CreateContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetObservationDistribution(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsContinuousHiddenMarkovModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DescriptorModel")) == 0))
  {
	Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr tmp = CreateContinuousHiddenMarkovModelType_DescriptorModel_CollectionType; // FTT, check this
	this->SetDescriptorModel(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ObservationDistribution")) == 0))
  {
	Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr tmp = CreateContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType; // FTT, check this
	this->SetObservationDistribution(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsContinuousHiddenMarkovModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptorModel() != Dc1NodePtr())
		result.Insert(GetDescriptorModel());
	if (GetObservationDistribution() != Dc1NodePtr())
		result.Insert(GetObservationDistribution());
	if (GetInitial() != Dc1NodePtr())
		result.Insert(GetInitial());
	if (GetTransitions() != Dc1NodePtr())
		result.Insert(GetTransitions());
	if (GetState() != Dc1NodePtr())
		result.Insert(GetState());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsContinuousHiddenMarkovModelType_ExtMethodImpl.h


