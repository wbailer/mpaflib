
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalType.h"
#include "Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalType.h"
#include "Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalType.h"
#include "Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType.h"
#include "Mp7JrsAdvancedFaceRecognitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsAdvancedFaceRecognitionType::IMp7JrsAdvancedFaceRecognitionType()
{

// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtPropInit.h

}

IMp7JrsAdvancedFaceRecognitionType::~IMp7JrsAdvancedFaceRecognitionType()
{
// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtPropCleanup.h

}

Mp7JrsAdvancedFaceRecognitionType::Mp7JrsAdvancedFaceRecognitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAdvancedFaceRecognitionType::~Mp7JrsAdvancedFaceRecognitionType()
{
	Cleanup();
}

void Mp7JrsAdvancedFaceRecognitionType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_FourierFeature = Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr(); // Class
	m_CentralFourierFeature = Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr(); // Class
	m_CompositeFeature = Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr(); // Class
	m_SubregionCompositeFeature = Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtMyPropInit.h

}

void Mp7JrsAdvancedFaceRecognitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_FourierFeature);
	// Dc1Factory::DeleteObject(m_CentralFourierFeature);
	// Dc1Factory::DeleteObject(m_CompositeFeature);
	// Dc1Factory::DeleteObject(m_SubregionCompositeFeature);
}

void Mp7JrsAdvancedFaceRecognitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AdvancedFaceRecognitionTypePtr, since we
	// might need GetBase(), which isn't defined in IAdvancedFaceRecognitionType
	const Dc1Ptr< Mp7JrsAdvancedFaceRecognitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAdvancedFaceRecognitionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_FourierFeature);
		this->SetFourierFeature(Dc1Factory::CloneObject(tmp->GetFourierFeature()));
		// Dc1Factory::DeleteObject(m_CentralFourierFeature);
		this->SetCentralFourierFeature(Dc1Factory::CloneObject(tmp->GetCentralFourierFeature()));
		// Dc1Factory::DeleteObject(m_CompositeFeature);
		this->SetCompositeFeature(Dc1Factory::CloneObject(tmp->GetCompositeFeature()));
		// Dc1Factory::DeleteObject(m_SubregionCompositeFeature);
		this->SetSubregionCompositeFeature(Dc1Factory::CloneObject(tmp->GetSubregionCompositeFeature()));
}

Dc1NodePtr Mp7JrsAdvancedFaceRecognitionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAdvancedFaceRecognitionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsAdvancedFaceRecognitionType::GetBase() const
{
	return m_Base;
}

Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr Mp7JrsAdvancedFaceRecognitionType::GetFourierFeature() const
{
		return m_FourierFeature;
}

Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr Mp7JrsAdvancedFaceRecognitionType::GetCentralFourierFeature() const
{
		return m_CentralFourierFeature;
}

Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr Mp7JrsAdvancedFaceRecognitionType::GetCompositeFeature() const
{
		return m_CompositeFeature;
}

Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr Mp7JrsAdvancedFaceRecognitionType::GetSubregionCompositeFeature() const
{
		return m_SubregionCompositeFeature;
}

void Mp7JrsAdvancedFaceRecognitionType::SetFourierFeature(const Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAdvancedFaceRecognitionType::SetFourierFeature().");
	}
	if (m_FourierFeature != item)
	{
		// Dc1Factory::DeleteObject(m_FourierFeature);
		m_FourierFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FourierFeature) m_FourierFeature->SetParent(m_myself.getPointer());
		if (m_FourierFeature && m_FourierFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_FourierFeature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FourierFeature->UseTypeAttribute = true;
		}
		if(m_FourierFeature != Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr())
		{
			m_FourierFeature->SetContentName(XMLString::transcode("FourierFeature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAdvancedFaceRecognitionType::SetCentralFourierFeature(const Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAdvancedFaceRecognitionType::SetCentralFourierFeature().");
	}
	if (m_CentralFourierFeature != item)
	{
		// Dc1Factory::DeleteObject(m_CentralFourierFeature);
		m_CentralFourierFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CentralFourierFeature) m_CentralFourierFeature->SetParent(m_myself.getPointer());
		if (m_CentralFourierFeature && m_CentralFourierFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CentralFourierFeature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CentralFourierFeature->UseTypeAttribute = true;
		}
		if(m_CentralFourierFeature != Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr())
		{
			m_CentralFourierFeature->SetContentName(XMLString::transcode("CentralFourierFeature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAdvancedFaceRecognitionType::SetCompositeFeature(const Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAdvancedFaceRecognitionType::SetCompositeFeature().");
	}
	if (m_CompositeFeature != item)
	{
		// Dc1Factory::DeleteObject(m_CompositeFeature);
		m_CompositeFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositeFeature) m_CompositeFeature->SetParent(m_myself.getPointer());
		if (m_CompositeFeature && m_CompositeFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CompositeFeature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompositeFeature->UseTypeAttribute = true;
		}
		if(m_CompositeFeature != Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr())
		{
			m_CompositeFeature->SetContentName(XMLString::transcode("CompositeFeature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAdvancedFaceRecognitionType::SetSubregionCompositeFeature(const Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAdvancedFaceRecognitionType::SetSubregionCompositeFeature().");
	}
	if (m_SubregionCompositeFeature != item)
	{
		// Dc1Factory::DeleteObject(m_SubregionCompositeFeature);
		m_SubregionCompositeFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SubregionCompositeFeature) m_SubregionCompositeFeature->SetParent(m_myself.getPointer());
		if (m_SubregionCompositeFeature && m_SubregionCompositeFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SubregionCompositeFeature->UseTypeAttribute = true;
		}
		if(m_SubregionCompositeFeature != Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr())
		{
			m_SubregionCompositeFeature->SetContentName(XMLString::transcode("SubregionCompositeFeature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAdvancedFaceRecognitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("FourierFeature")) == 0)
	{
		// FourierFeature is simple element AdvancedFaceRecognitionType_FourierFeature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFourierFeature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_FourierFeature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFourierFeature(p, client);
					if((p = GetFourierFeature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CentralFourierFeature")) == 0)
	{
		// CentralFourierFeature is simple element AdvancedFaceRecognitionType_CentralFourierFeature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCentralFourierFeature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CentralFourierFeature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCentralFourierFeature(p, client);
					if((p = GetCentralFourierFeature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CompositeFeature")) == 0)
	{
		// CompositeFeature is simple element AdvancedFaceRecognitionType_CompositeFeature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCompositeFeature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_CompositeFeature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCompositeFeature(p, client);
					if((p = GetCompositeFeature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SubregionCompositeFeature")) == 0)
	{
		// SubregionCompositeFeature is simple element AdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSubregionCompositeFeature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSubregionCompositeFeature(p, client);
					if((p = GetSubregionCompositeFeature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AdvancedFaceRecognitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AdvancedFaceRecognitionType");
	}
	return result;
}

XMLCh * Mp7JrsAdvancedFaceRecognitionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAdvancedFaceRecognitionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAdvancedFaceRecognitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AdvancedFaceRecognitionType"));
	// Element serialization:
	// Class
	
	if (m_FourierFeature != Mp7JrsAdvancedFaceRecognitionType_FourierFeature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FourierFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FourierFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FourierFeature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CentralFourierFeature != Mp7JrsAdvancedFaceRecognitionType_CentralFourierFeature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CentralFourierFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CentralFourierFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CentralFourierFeature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CompositeFeature != Mp7JrsAdvancedFaceRecognitionType_CompositeFeature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompositeFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompositeFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompositeFeature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SubregionCompositeFeature != Mp7JrsAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SubregionCompositeFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SubregionCompositeFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SubregionCompositeFeature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAdvancedFaceRecognitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FourierFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FourierFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAdvancedFaceRecognitionType_FourierFeature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFourierFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CentralFourierFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CentralFourierFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAdvancedFaceRecognitionType_CentralFourierFeature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCentralFourierFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompositeFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompositeFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAdvancedFaceRecognitionType_CompositeFeature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompositeFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SubregionCompositeFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SubregionCompositeFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSubregionCompositeFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAdvancedFaceRecognitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("FourierFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAdvancedFaceRecognitionType_FourierFeature_LocalType; // FTT, check this
	}
	this->SetFourierFeature(child);
  }
  if (XMLString::compareString(elementname, X("CentralFourierFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAdvancedFaceRecognitionType_CentralFourierFeature_LocalType; // FTT, check this
	}
	this->SetCentralFourierFeature(child);
  }
  if (XMLString::compareString(elementname, X("CompositeFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAdvancedFaceRecognitionType_CompositeFeature_LocalType; // FTT, check this
	}
	this->SetCompositeFeature(child);
  }
  if (XMLString::compareString(elementname, X("SubregionCompositeFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAdvancedFaceRecognitionType_SubregionCompositeFeature_LocalType; // FTT, check this
	}
	this->SetSubregionCompositeFeature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAdvancedFaceRecognitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFourierFeature() != Dc1NodePtr())
		result.Insert(GetFourierFeature());
	if (GetCentralFourierFeature() != Dc1NodePtr())
		result.Insert(GetCentralFourierFeature());
	if (GetCompositeFeature() != Dc1NodePtr())
		result.Insert(GetCompositeFeature());
	if (GetSubregionCompositeFeature() != Dc1NodePtr())
		result.Insert(GetSubregionCompositeFeature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAdvancedFaceRecognitionType_ExtMethodImpl.h


