
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtImplInclude.h


#include "Mp7JrsInlineTermDefinitionType_Name_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Definition_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsInlineTermDefinitionType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition
#include "Mp7JrsInlineTermDefinitionType_Term_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Term

#include <assert.h>
IMp7JrsInlineTermDefinitionType::IMp7JrsInlineTermDefinitionType()
{

// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtPropInit.h

}

IMp7JrsInlineTermDefinitionType::~IMp7JrsInlineTermDefinitionType()
{
// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtPropCleanup.h

}

Mp7JrsInlineTermDefinitionType::Mp7JrsInlineTermDefinitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInlineTermDefinitionType::~Mp7JrsInlineTermDefinitionType()
{
	Cleanup();
}

void Mp7JrsInlineTermDefinitionType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsInlineTermDefinitionType_Name_CollectionPtr(); // Collection
	m_Definition = Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr(); // Collection
	m_Term = Mp7JrsInlineTermDefinitionType_Term_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtMyPropInit.h

}

void Mp7JrsInlineTermDefinitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Definition);
	// Dc1Factory::DeleteObject(m_Term);
}

void Mp7JrsInlineTermDefinitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InlineTermDefinitionTypePtr, since we
	// might need GetBase(), which isn't defined in IInlineTermDefinitionType
	const Dc1Ptr< Mp7JrsInlineTermDefinitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
		// Dc1Factory::DeleteObject(m_Definition);
		this->SetDefinition(Dc1Factory::CloneObject(tmp->GetDefinition()));
		// Dc1Factory::DeleteObject(m_Term);
		this->SetTerm(Dc1Factory::CloneObject(tmp->GetTerm()));
}

Dc1NodePtr Mp7JrsInlineTermDefinitionType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsInlineTermDefinitionType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsInlineTermDefinitionType_Name_CollectionPtr Mp7JrsInlineTermDefinitionType::GetName() const
{
		return m_Name;
}

Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr Mp7JrsInlineTermDefinitionType::GetDefinition() const
{
		return m_Definition;
}

Mp7JrsInlineTermDefinitionType_Term_CollectionPtr Mp7JrsInlineTermDefinitionType::GetTerm() const
{
		return m_Term;
}

void Mp7JrsInlineTermDefinitionType::SetName(const Mp7JrsInlineTermDefinitionType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineTermDefinitionType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != Mp7JrsInlineTermDefinitionType_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInlineTermDefinitionType::SetDefinition(const Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineTermDefinitionType::SetDefinition().");
	}
	if (m_Definition != item)
	{
		// Dc1Factory::DeleteObject(m_Definition);
		m_Definition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Definition) m_Definition->SetParent(m_myself.getPointer());
		if(m_Definition != Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr())
		{
			m_Definition->SetContentName(XMLString::transcode("Definition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInlineTermDefinitionType::SetTerm(const Mp7JrsInlineTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInlineTermDefinitionType::SetTerm().");
	}
	if (m_Term != item)
	{
		// Dc1Factory::DeleteObject(m_Term);
		m_Term = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Term) m_Term->SetParent(m_myself.getPointer());
		if(m_Term != Mp7JrsInlineTermDefinitionType_Term_CollectionPtr())
		{
			m_Term->SetContentName(XMLString::transcode("Term"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsInlineTermDefinitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Name is item of type InlineTermDefinitionType_Name_LocalType
		// in element collection InlineTermDefinitionType_Name_CollectionType
		
		context->Found = true;
		Mp7JrsInlineTermDefinitionType_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInlineTermDefinitionType_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Name_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInlineTermDefinitionType_Name_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Definition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Definition is item of type TextualType
		// in element collection InlineTermDefinitionType_Definition_CollectionType
		
		context->Found = true;
		Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr coll = GetDefinition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInlineTermDefinitionType_Definition_CollectionType; // FTT, check this
				SetDefinition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Term")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Term is item of type InlineTermDefinitionType_Term_LocalType
		// in element collection InlineTermDefinitionType_Term_CollectionType
		
		context->Found = true;
		Mp7JrsInlineTermDefinitionType_Term_CollectionPtr coll = GetTerm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInlineTermDefinitionType_Term_CollectionType; // FTT, check this
				SetTerm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InlineTermDefinitionType_Term_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInlineTermDefinitionType_Term_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for InlineTermDefinitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "InlineTermDefinitionType");
	}
	return result;
}

XMLCh * Mp7JrsInlineTermDefinitionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsInlineTermDefinitionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsInlineTermDefinitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InlineTermDefinitionType"));
	// Element serialization:
	if (m_Name != Mp7JrsInlineTermDefinitionType_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Definition != Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Definition->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Term != Mp7JrsInlineTermDefinitionType_Term_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Term->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsInlineTermDefinitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInlineTermDefinitionType_Name_CollectionPtr tmp = CreateInlineTermDefinitionType_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Definition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Definition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr tmp = CreateInlineTermDefinitionType_Definition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDefinition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Term"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Term")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInlineTermDefinitionType_Term_CollectionPtr tmp = CreateInlineTermDefinitionType_Term_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTerm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInlineTermDefinitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	Mp7JrsInlineTermDefinitionType_Name_CollectionPtr tmp = CreateInlineTermDefinitionType_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Definition")) == 0))
  {
	Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr tmp = CreateInlineTermDefinitionType_Definition_CollectionType; // FTT, check this
	this->SetDefinition(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Term")) == 0))
  {
	Mp7JrsInlineTermDefinitionType_Term_CollectionPtr tmp = CreateInlineTermDefinitionType_Term_CollectionType; // FTT, check this
	this->SetTerm(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsInlineTermDefinitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInlineTermDefinitionType_ExtMethodImpl.h


