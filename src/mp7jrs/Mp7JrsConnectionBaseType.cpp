
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsConnectionBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsConnectionBaseType::IMp7JrsConnectionBaseType()
{

// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtPropInit.h

}

IMp7JrsConnectionBaseType::~IMp7JrsConnectionBaseType()
{
// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtPropCleanup.h

}

Mp7JrsConnectionBaseType::Mp7JrsConnectionBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsConnectionBaseType::~Mp7JrsConnectionBaseType()
{
	Cleanup();
}

void Mp7JrsConnectionBaseType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_dateFrom = Mp7JrstimePointPtr(); // Pattern
	m_dateFrom_Exist = false;
	m_dateTo = Mp7JrstimePointPtr(); // Pattern
	m_dateTo_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Role = Mp7JrsControlledTermUsePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtMyPropInit.h

}

void Mp7JrsConnectionBaseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_dateFrom); // Pattern
	// Dc1Factory::DeleteObject(m_dateTo); // Pattern
	// Dc1Factory::DeleteObject(m_Role);
}

void Mp7JrsConnectionBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ConnectionBaseTypePtr, since we
	// might need GetBase(), which isn't defined in IConnectionBaseType
	const Dc1Ptr< Mp7JrsConnectionBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsConnectionBaseType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_dateFrom); // Pattern
	if (tmp->ExistdateFrom())
	{
		this->SetdateFrom(Dc1Factory::CloneObject(tmp->GetdateFrom()));
	}
	else
	{
		InvalidatedateFrom();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_dateTo); // Pattern
	if (tmp->ExistdateTo())
	{
		this->SetdateTo(Dc1Factory::CloneObject(tmp->GetdateTo()));
	}
	else
	{
		InvalidatedateTo();
	}
	}
		// Dc1Factory::DeleteObject(m_Role);
		this->SetRole(Dc1Factory::CloneObject(tmp->GetRole()));
}

Dc1NodePtr Mp7JrsConnectionBaseType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsConnectionBaseType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsConnectionBaseType::GetBase() const
{
	return m_Base;
}

Mp7JrstimePointPtr Mp7JrsConnectionBaseType::GetdateFrom() const
{
	return m_dateFrom;
}

bool Mp7JrsConnectionBaseType::ExistdateFrom() const
{
	return m_dateFrom_Exist;
}
void Mp7JrsConnectionBaseType::SetdateFrom(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetdateFrom().");
	}
	m_dateFrom = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dateFrom) m_dateFrom->SetParent(m_myself.getPointer());
	m_dateFrom_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatedateFrom()
{
	m_dateFrom_Exist = false;
}
Mp7JrstimePointPtr Mp7JrsConnectionBaseType::GetdateTo() const
{
	return m_dateTo;
}

bool Mp7JrsConnectionBaseType::ExistdateTo() const
{
	return m_dateTo_Exist;
}
void Mp7JrsConnectionBaseType::SetdateTo(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetdateTo().");
	}
	m_dateTo = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dateTo) m_dateTo->SetParent(m_myself.getPointer());
	m_dateTo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatedateTo()
{
	m_dateTo_Exist = false;
}
Mp7JrsControlledTermUsePtr Mp7JrsConnectionBaseType::GetRole() const
{
		return m_Role;
}

void Mp7JrsConnectionBaseType::SetRole(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetRole().");
	}
	if (m_Role != item)
	{
		// Dc1Factory::DeleteObject(m_Role);
		m_Role = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Role) m_Role->SetParent(m_myself.getPointer());
		if (m_Role && m_Role->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Role->UseTypeAttribute = true;
		}
		if(m_Role != Mp7JrsControlledTermUsePtr())
		{
			m_Role->SetContentName(XMLString::transcode("Role"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsConnectionBaseType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsConnectionBaseType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsConnectionBaseType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsConnectionBaseType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsConnectionBaseType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsConnectionBaseType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsConnectionBaseType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsConnectionBaseType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsConnectionBaseType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsConnectionBaseType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsConnectionBaseType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsConnectionBaseType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsConnectionBaseType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsConnectionBaseType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsConnectionBaseType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConnectionBaseType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsConnectionBaseType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsConnectionBaseType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConnectionBaseType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsConnectionBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dateFrom")) == 0)
	{
		// dateFrom is simple attribute timePointType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstimePointPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dateTo")) == 0)
	{
		// dateTo is simple attribute timePointType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstimePointPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Role")) == 0)
	{
		// Role is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRole()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRole(p, client);
					if((p = GetRole()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ConnectionBaseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ConnectionBaseType");
	}
	return result;
}

XMLCh * Mp7JrsConnectionBaseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsConnectionBaseType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsConnectionBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ConnectionBaseType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dateFrom_Exist)
	{
	// Pattern
	if (m_dateFrom != Mp7JrstimePointPtr())
	{
		XMLCh * tmp = m_dateFrom->ToText();
		element->setAttributeNS(X(""), X("dateFrom"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dateTo_Exist)
	{
	// Pattern
	if (m_dateTo != Mp7JrstimePointPtr())
	{
		XMLCh * tmp = m_dateTo->ToText();
		element->setAttributeNS(X(""), X("dateTo"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Role != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Role->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Role"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Role->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsConnectionBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dateFrom")))
	{
		Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("dateFrom")));
		this->SetdateFrom(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dateTo")))
	{
		Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("dateTo")));
		this->SetdateTo(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Role"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Role")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRole(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsConnectionBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Role")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetRole(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsConnectionBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRole() != Dc1NodePtr())
		result.Insert(GetRole());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsConnectionBaseType_ExtMethodImpl.h


