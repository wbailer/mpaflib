
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "W3CIDREFS.h"
#include "Mp7JrsSummarySegmentType_Name_CollectionType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsSummarySegmentType_KeyFrame_CollectionType.h"
#include "Mp7JrsSummarySegmentType_KeySound_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSummarySegmentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsImageLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:KeyFrame

#include <assert.h>
IMp7JrsSummarySegmentType::IMp7JrsSummarySegmentType()
{

// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtPropInit.h

}

IMp7JrsSummarySegmentType::~IMp7JrsSummarySegmentType()
{
// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtPropCleanup.h

}

Mp7JrsSummarySegmentType::Mp7JrsSummarySegmentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSummarySegmentType::~Mp7JrsSummarySegmentType()
{
	Cleanup();
}

void Mp7JrsSummarySegmentType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_themeIDs = W3CIDREFSPtr(); // Collection
	m_themeIDs_Exist = false;
	m_order = 1; // Value
	m_order_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsSummarySegmentType_Name_CollectionPtr(); // Collection
	m_SourceID = Mp7JrsUniqueIDPtr(); // Class
	m_SourceID_Exist = false;
	m_KeyAudioVisualClip = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_KeyVideoClip = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_KeyVideoClip_Exist = false;
	m_KeyAudioClip = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_KeyAudioClip_Exist = false;
	m_KeyFrame = Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr(); // Collection
	m_KeySound = Mp7JrsSummarySegmentType_KeySound_CollectionPtr(); // Collection


// begin extension included
// file Mp7JrsSummarySegmentType_ExtMyPropInit.h

	m_Name = CreateSummarySegmentType_Name_CollectionType;

	m_KeyFrame = CreateSummarySegmentType_KeyFrame_CollectionType;
// end extension included

}

void Mp7JrsSummarySegmentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_themeIDs);
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_SourceID);
	// Dc1Factory::DeleteObject(m_KeyAudioVisualClip);
	// Dc1Factory::DeleteObject(m_KeyVideoClip);
	// Dc1Factory::DeleteObject(m_KeyAudioClip);
	// Dc1Factory::DeleteObject(m_KeyFrame);
	// Dc1Factory::DeleteObject(m_KeySound);
}

void Mp7JrsSummarySegmentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SummarySegmentTypePtr, since we
	// might need GetBase(), which isn't defined in ISummarySegmentType
	const Dc1Ptr< Mp7JrsSummarySegmentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSummarySegmentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_themeIDs);
	if (tmp->ExistthemeIDs())
	{
		this->SetthemeIDs(Dc1Factory::CloneObject(tmp->GetthemeIDs()));
	}
	else
	{
		InvalidatethemeIDs();
	}
	}
	{
	if (tmp->Existorder())
	{
		this->Setorder(tmp->Getorder());
	}
	else
	{
		Invalidateorder();
	}
	}
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	if (tmp->IsValidSourceID())
	{
		// Dc1Factory::DeleteObject(m_SourceID);
		this->SetSourceID(Dc1Factory::CloneObject(tmp->GetSourceID()));
	}
	else
	{
		InvalidateSourceID();
	}
		// Dc1Factory::DeleteObject(m_KeyAudioVisualClip);
		this->SetKeyAudioVisualClip(Dc1Factory::CloneObject(tmp->GetKeyAudioVisualClip()));
	if (tmp->IsValidKeyVideoClip())
	{
		// Dc1Factory::DeleteObject(m_KeyVideoClip);
		this->SetKeyVideoClip(Dc1Factory::CloneObject(tmp->GetKeyVideoClip()));
	}
	else
	{
		InvalidateKeyVideoClip();
	}
	if (tmp->IsValidKeyAudioClip())
	{
		// Dc1Factory::DeleteObject(m_KeyAudioClip);
		this->SetKeyAudioClip(Dc1Factory::CloneObject(tmp->GetKeyAudioClip()));
	}
	else
	{
		InvalidateKeyAudioClip();
	}
		// Dc1Factory::DeleteObject(m_KeyFrame);
		this->SetKeyFrame(Dc1Factory::CloneObject(tmp->GetKeyFrame()));
		// Dc1Factory::DeleteObject(m_KeySound);
		this->SetKeySound(Dc1Factory::CloneObject(tmp->GetKeySound()));
}

Dc1NodePtr Mp7JrsSummarySegmentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSummarySegmentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSummarySegmentType::GetBase() const
{
	return m_Base;
}

W3CIDREFSPtr Mp7JrsSummarySegmentType::GetthemeIDs() const
{
	return m_themeIDs;
}

bool Mp7JrsSummarySegmentType::ExistthemeIDs() const
{
	return m_themeIDs_Exist;
}
void Mp7JrsSummarySegmentType::SetthemeIDs(const W3CIDREFSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetthemeIDs().");
	}
	m_themeIDs = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_themeIDs) m_themeIDs->SetParent(m_myself.getPointer());
	m_themeIDs_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidatethemeIDs()
{
	m_themeIDs_Exist = false;
}
unsigned Mp7JrsSummarySegmentType::Getorder() const
{
	return m_order;
}

bool Mp7JrsSummarySegmentType::Existorder() const
{
	return m_order_Exist;
}
void Mp7JrsSummarySegmentType::Setorder(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::Setorder().");
	}
	m_order = item;
	m_order_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::Invalidateorder()
{
	m_order_Exist = false;
}
Mp7JrsSummarySegmentType_Name_CollectionPtr Mp7JrsSummarySegmentType::GetName() const
{
		return m_Name;
}

Mp7JrsUniqueIDPtr Mp7JrsSummarySegmentType::GetSourceID() const
{
		return m_SourceID;
}

// Element is optional
bool Mp7JrsSummarySegmentType::IsValidSourceID() const
{
	return m_SourceID_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSummarySegmentType::GetKeyAudioVisualClip() const
{
		return m_KeyAudioVisualClip;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSummarySegmentType::GetKeyVideoClip() const
{
		return m_KeyVideoClip;
}

// Element is optional
bool Mp7JrsSummarySegmentType::IsValidKeyVideoClip() const
{
	return m_KeyVideoClip_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSummarySegmentType::GetKeyAudioClip() const
{
		return m_KeyAudioClip;
}

// Element is optional
bool Mp7JrsSummarySegmentType::IsValidKeyAudioClip() const
{
	return m_KeyAudioClip_Exist;
}

Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr Mp7JrsSummarySegmentType::GetKeyFrame() const
{
		return m_KeyFrame;
}

Mp7JrsSummarySegmentType_KeySound_CollectionPtr Mp7JrsSummarySegmentType::GetKeySound() const
{
		return m_KeySound;
}

void Mp7JrsSummarySegmentType::SetName(const Mp7JrsSummarySegmentType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != Mp7JrsSummarySegmentType_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::SetSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetSourceID().");
	}
	if (m_SourceID != item || m_SourceID_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SourceID);
		m_SourceID = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SourceID) m_SourceID->SetParent(m_myself.getPointer());
		if (m_SourceID && m_SourceID->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SourceID->UseTypeAttribute = true;
		}
		m_SourceID_Exist = true;
		if(m_SourceID != Mp7JrsUniqueIDPtr())
		{
			m_SourceID->SetContentName(XMLString::transcode("SourceID"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidateSourceID()
{
	m_SourceID_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSummarySegmentType::SetKeyAudioVisualClip(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetKeyAudioVisualClip().");
	}
	if (m_KeyAudioVisualClip != item)
	{
		// Dc1Factory::DeleteObject(m_KeyAudioVisualClip);
		m_KeyAudioVisualClip = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeyAudioVisualClip) m_KeyAudioVisualClip->SetParent(m_myself.getPointer());
		if (m_KeyAudioVisualClip && m_KeyAudioVisualClip->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_KeyAudioVisualClip->UseTypeAttribute = true;
		}
		if(m_KeyAudioVisualClip != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_KeyAudioVisualClip->SetContentName(XMLString::transcode("KeyAudioVisualClip"));
		}
	// Dc1Factory::DeleteObject(m_KeyVideoClip);
	m_KeyVideoClip = Mp7JrsTemporalSegmentLocatorPtr();
	// Dc1Factory::DeleteObject(m_KeyAudioClip);
	m_KeyAudioClip = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsSummarySegmentType::SetKeyVideoClip(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetKeyVideoClip().");
	}
	if (m_KeyVideoClip != item || m_KeyVideoClip_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_KeyVideoClip);
		m_KeyVideoClip = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeyVideoClip) m_KeyVideoClip->SetParent(m_myself.getPointer());
		if (m_KeyVideoClip && m_KeyVideoClip->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_KeyVideoClip->UseTypeAttribute = true;
		}
		m_KeyVideoClip_Exist = true;
		if(m_KeyVideoClip != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_KeyVideoClip->SetContentName(XMLString::transcode("KeyVideoClip"));
		}
	// Dc1Factory::DeleteObject(m_KeyAudioVisualClip);
	m_KeyAudioVisualClip = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidateKeyVideoClip()
{
	m_KeyVideoClip_Exist = false;
}
void Mp7JrsSummarySegmentType::SetKeyAudioClip(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetKeyAudioClip().");
	}
	if (m_KeyAudioClip != item || m_KeyAudioClip_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_KeyAudioClip);
		m_KeyAudioClip = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeyAudioClip) m_KeyAudioClip->SetParent(m_myself.getPointer());
		if (m_KeyAudioClip && m_KeyAudioClip->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_KeyAudioClip->UseTypeAttribute = true;
		}
		m_KeyAudioClip_Exist = true;
		if(m_KeyAudioClip != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_KeyAudioClip->SetContentName(XMLString::transcode("KeyAudioClip"));
		}
	// Dc1Factory::DeleteObject(m_KeyAudioVisualClip);
	m_KeyAudioVisualClip = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidateKeyAudioClip()
{
	m_KeyAudioClip_Exist = false;
}
void Mp7JrsSummarySegmentType::SetKeyFrame(const Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetKeyFrame().");
	}
	if (m_KeyFrame != item)
	{
		// Dc1Factory::DeleteObject(m_KeyFrame);
		m_KeyFrame = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeyFrame) m_KeyFrame->SetParent(m_myself.getPointer());
		if(m_KeyFrame != Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr())
		{
			m_KeyFrame->SetContentName(XMLString::transcode("KeyFrame"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::SetKeySound(const Mp7JrsSummarySegmentType_KeySound_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetKeySound().");
	}
	if (m_KeySound != item)
	{
		// Dc1Factory::DeleteObject(m_KeySound);
		m_KeySound = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeySound) m_KeySound->SetParent(m_myself.getPointer());
		if(m_KeySound != Mp7JrsSummarySegmentType_KeySound_CollectionPtr())
		{
			m_KeySound->SetContentName(XMLString::transcode("KeySound"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSummarySegmentType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSummarySegmentType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSummarySegmentType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSummarySegmentType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSummarySegmentType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSummarySegmentType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSummarySegmentType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSummarySegmentType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSummarySegmentType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSummarySegmentType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSummarySegmentType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSummarySegmentType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSummarySegmentType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSummarySegmentType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSummarySegmentType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSummarySegmentType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSummarySegmentType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSummarySegmentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("themeIDs")) == 0)
	{
		// themeIDs is simple attribute IDREFS
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CIDREFSPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("order")) == 0)
	{
		// order is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Name is item of type TextualType
		// in element collection SummarySegmentType_Name_CollectionType
		
		context->Found = true;
		Mp7JrsSummarySegmentType_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentType_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SourceID")) == 0)
	{
		// SourceID is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSourceID()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSourceID(p, client);
					if((p = GetSourceID()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeyAudioVisualClip")) == 0)
	{
		// KeyAudioVisualClip is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetKeyAudioVisualClip()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetKeyAudioVisualClip(p, client);
					if((p = GetKeyAudioVisualClip()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeyVideoClip")) == 0)
	{
		// KeyVideoClip is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetKeyVideoClip()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetKeyVideoClip(p, client);
					if((p = GetKeyVideoClip()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeyAudioClip")) == 0)
	{
		// KeyAudioClip is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetKeyAudioClip()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetKeyAudioClip(p, client);
					if((p = GetKeyAudioClip()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeyFrame")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:KeyFrame is item of type ImageLocatorType
		// in element collection SummarySegmentType_KeyFrame_CollectionType
		
		context->Found = true;
		Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr coll = GetKeyFrame();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentType_KeyFrame_CollectionType; // FTT, check this
				SetKeyFrame(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeySound")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:KeySound is item of type TemporalSegmentLocatorType
		// in element collection SummarySegmentType_KeySound_CollectionType
		
		context->Found = true;
		Mp7JrsSummarySegmentType_KeySound_CollectionPtr coll = GetKeySound();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentType_KeySound_CollectionType; // FTT, check this
				SetKeySound(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SummarySegmentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SummarySegmentType");
	}
	return result;
}

XMLCh * Mp7JrsSummarySegmentType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSummarySegmentType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSummarySegmentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SummarySegmentType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_themeIDs_Exist)
	{
	// Collection
	if(m_themeIDs != W3CIDREFSPtr())
	{
		XMLCh * tmp = m_themeIDs->ToText();
		element->setAttributeNS(X(""), X("themeIDs"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_order_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_order);
		element->setAttributeNS(X(""), X("order"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Name != Mp7JrsSummarySegmentType_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_SourceID != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SourceID->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SourceID"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SourceID->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_KeyAudioVisualClip != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_KeyAudioVisualClip->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("KeyAudioVisualClip"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_KeyAudioVisualClip->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_KeyVideoClip != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_KeyVideoClip->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("KeyVideoClip"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_KeyVideoClip->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_KeyAudioClip != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_KeyAudioClip->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("KeyAudioClip"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_KeyAudioClip->Serialize(doc, element, &elem);
	}
	if (m_KeyFrame != Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_KeyFrame->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_KeySound != Mp7JrsSummarySegmentType_KeySound_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_KeySound->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSummarySegmentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("themeIDs")))
	{
		// Deserialize collection type
		W3CIDREFSPtr tmp = CreateIDREFS; // FTT, check this
		tmp->Parse(parent->getAttribute(X("themeIDs")));
		this->SetthemeIDs(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("order")))
	{
		// deserialize value type
		this->Setorder(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("order"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentType_Name_CollectionPtr tmp = CreateSummarySegmentType_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SourceID"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SourceID")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSourceID(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("KeyAudioVisualClip"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("KeyAudioVisualClip")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKeyAudioVisualClip(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("KeyVideoClip"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("KeyVideoClip")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKeyVideoClip(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("KeyAudioClip"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("KeyAudioClip")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKeyAudioClip(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("KeyFrame"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("KeyFrame")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr tmp = CreateSummarySegmentType_KeyFrame_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetKeyFrame(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("KeySound"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("KeySound")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentType_KeySound_CollectionPtr tmp = CreateSummarySegmentType_KeySound_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetKeySound(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSummarySegmentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSummarySegmentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	Mp7JrsSummarySegmentType_Name_CollectionPtr tmp = CreateSummarySegmentType_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("SourceID")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetSourceID(child);
  }
  if (XMLString::compareString(elementname, X("KeyAudioVisualClip")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetKeyAudioVisualClip(child);
  }
  if (XMLString::compareString(elementname, X("KeyVideoClip")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetKeyVideoClip(child);
  }
  if (XMLString::compareString(elementname, X("KeyAudioClip")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetKeyAudioClip(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("KeyFrame")) == 0))
  {
	Mp7JrsSummarySegmentType_KeyFrame_CollectionPtr tmp = CreateSummarySegmentType_KeyFrame_CollectionType; // FTT, check this
	this->SetKeyFrame(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("KeySound")) == 0))
  {
	Mp7JrsSummarySegmentType_KeySound_CollectionPtr tmp = CreateSummarySegmentType_KeySound_CollectionType; // FTT, check this
	this->SetKeySound(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSummarySegmentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetSourceID() != Dc1NodePtr())
		result.Insert(GetSourceID());
	if (GetKeyFrame() != Dc1NodePtr())
		result.Insert(GetKeyFrame());
	if (GetKeySound() != Dc1NodePtr())
		result.Insert(GetKeySound());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetKeyAudioVisualClip() != Dc1NodePtr())
		result.Insert(GetKeyAudioVisualClip());
	if (GetKeyVideoClip() != Dc1NodePtr())
		result.Insert(GetKeyVideoClip());
	if (GetKeyAudioClip() != Dc1NodePtr())
		result.Insert(GetKeyAudioClip());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsSummarySegmentType_ExtMethodImpl.h

#include "Mp7JrsImageLocatorType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsTextualBaseType.h"

void IMp7JrsSummarySegmentType::AddKeyframe(Mp7JrsmediaTimePointPtr timepoint, bool copyTimePoint, Dc1ClientID client) {
	if (GetKeyFrame()==NULL) SetKeyFrame(CreateSummarySegmentType_KeyFrame_CollectionType, client);
	Mp7JrsImageLocatorPtr locator = CreateImageLocatorType;
	
	Mp7JrsmediaTimePointPtr tp = timepoint;
	if (copyTimePoint) tp = (Mp7JrsmediaTimePointPtr) Dc1Factory::CloneObject(timepoint);

	locator->SetMediaTimePoint(tp, client);
	GetKeyFrame()->addElement(locator);
}

int IMp7JrsSummarySegmentType::GetNrKeyframes() {
	if (GetKeyFrame()==NULL) return 0;
	return GetKeyFrame()->size();
}

///////

void IMp7JrsSummarySegmentType::SetKeyVideoClip(Mp7JrsmediaTimePointPtr beginTime, Mp7JrsmediaTimePointPtr endTime, bool copyTimePoints, Dc1ClientID client) {
	if (GetKeyVideoClip()==NULL) SetKeyVideoClip(CreateTemporalSegmentLocatorType, client);
	GetKeyVideoClip()->SetMediaTime(CreateMediaTimeType, client);
	Mp7JrsmediaTimePointPtr beginTp = beginTime;
	Mp7JrsmediaTimePointPtr endTp = endTime;
	if (copyTimePoints) {
		beginTp = (Mp7JrsmediaTimePointPtr) Dc1Factory::CloneObject(beginTime);
		endTp = (Mp7JrsmediaTimePointPtr) Dc1Factory::CloneObject(endTime);
	}
	GetKeyVideoClip()->GetMediaTime()->SetTime(beginTp,endTp,client);
}

void IMp7JrsSummarySegmentType::GetKeyVideoClip(Mp7JrsmediaTimePointPtr& beginTime, Mp7JrsmediaTimePointPtr& endTime) {
	if (GetKeyVideoClip()==NULL) return;
	GetKeyVideoClip()->GetMediaTime()->GetTime(beginTime,endTime);
}


void IMp7JrsSummarySegmentType::AddName(XMLCh* name, Dc1ClientID client) {
	Mp7JrsTextualPtr text = CreateTextualType;
	text->SetContent(name, client);
	GetName()->addElement(text, client);
}
// end extension included


