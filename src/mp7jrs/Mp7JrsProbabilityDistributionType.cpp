
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtImplInclude.h


#include "Mp7JrsProbabilityModelType.h"
#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7JrsProbabilityDistributionType_Moment_CollectionType.h"
#include "Mp7JrsProbabilityDistributionType_Cumulant_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsProbabilityDistributionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsProbabilityDistributionType_Moment_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Moment
#include "Mp7JrsProbabilityDistributionType_Cumulant_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Cumulant

#include <assert.h>
IMp7JrsProbabilityDistributionType::IMp7JrsProbabilityDistributionType()
{

// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtPropInit.h

}

IMp7JrsProbabilityDistributionType::~IMp7JrsProbabilityDistributionType()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtPropCleanup.h

}

Mp7JrsProbabilityDistributionType::Mp7JrsProbabilityDistributionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsProbabilityDistributionType::~Mp7JrsProbabilityDistributionType()
{
	Cleanup();
}

void Mp7JrsProbabilityDistributionType::Init()
{
	// Init base
	m_Base = CreateProbabilityModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_dim = 0; // Value
	m_dim_Default = 1; // Default value
	m_dim_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Mean = Mp7JrsDoubleMatrixPtr(); // Class
	m_Mean_Exist = false;
	m_Variance = Mp7JrsDoubleMatrixPtr(); // Class
	m_Variance_Exist = false;
	m_Min = Mp7JrsDoubleMatrixPtr(); // Class
	m_Min_Exist = false;
	m_Max = Mp7JrsDoubleMatrixPtr(); // Class
	m_Max_Exist = false;
	m_Mode = Mp7JrsDoubleMatrixPtr(); // Class
	m_Mode_Exist = false;
	m_Median = Mp7JrsDoubleMatrixPtr(); // Class
	m_Median_Exist = false;
	m_Moment = Mp7JrsProbabilityDistributionType_Moment_CollectionPtr(); // Collection
	m_Cumulant = Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtMyPropInit.h

}

void Mp7JrsProbabilityDistributionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Mean);
	// Dc1Factory::DeleteObject(m_Variance);
	// Dc1Factory::DeleteObject(m_Min);
	// Dc1Factory::DeleteObject(m_Max);
	// Dc1Factory::DeleteObject(m_Mode);
	// Dc1Factory::DeleteObject(m_Median);
	// Dc1Factory::DeleteObject(m_Moment);
	// Dc1Factory::DeleteObject(m_Cumulant);
}

void Mp7JrsProbabilityDistributionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ProbabilityDistributionTypePtr, since we
	// might need GetBase(), which isn't defined in IProbabilityDistributionType
	const Dc1Ptr< Mp7JrsProbabilityDistributionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsProbabilityModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsProbabilityDistributionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existdim())
	{
		this->Setdim(tmp->Getdim());
	}
	else
	{
		Invalidatedim();
	}
	}
	if (tmp->IsValidMean())
	{
		// Dc1Factory::DeleteObject(m_Mean);
		this->SetMean(Dc1Factory::CloneObject(tmp->GetMean()));
	}
	else
	{
		InvalidateMean();
	}
	if (tmp->IsValidVariance())
	{
		// Dc1Factory::DeleteObject(m_Variance);
		this->SetVariance(Dc1Factory::CloneObject(tmp->GetVariance()));
	}
	else
	{
		InvalidateVariance();
	}
	if (tmp->IsValidMin())
	{
		// Dc1Factory::DeleteObject(m_Min);
		this->SetMin(Dc1Factory::CloneObject(tmp->GetMin()));
	}
	else
	{
		InvalidateMin();
	}
	if (tmp->IsValidMax())
	{
		// Dc1Factory::DeleteObject(m_Max);
		this->SetMax(Dc1Factory::CloneObject(tmp->GetMax()));
	}
	else
	{
		InvalidateMax();
	}
	if (tmp->IsValidMode())
	{
		// Dc1Factory::DeleteObject(m_Mode);
		this->SetMode(Dc1Factory::CloneObject(tmp->GetMode()));
	}
	else
	{
		InvalidateMode();
	}
	if (tmp->IsValidMedian())
	{
		// Dc1Factory::DeleteObject(m_Median);
		this->SetMedian(Dc1Factory::CloneObject(tmp->GetMedian()));
	}
	else
	{
		InvalidateMedian();
	}
		// Dc1Factory::DeleteObject(m_Moment);
		this->SetMoment(Dc1Factory::CloneObject(tmp->GetMoment()));
		// Dc1Factory::DeleteObject(m_Cumulant);
		this->SetCumulant(Dc1Factory::CloneObject(tmp->GetCumulant()));
}

Dc1NodePtr Mp7JrsProbabilityDistributionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsProbabilityDistributionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsProbabilityModelType > Mp7JrsProbabilityDistributionType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsProbabilityDistributionType::Getdim() const
{
	if (this->Existdim()) {
		return m_dim;
	} else {
		return m_dim_Default;
	}
}

bool Mp7JrsProbabilityDistributionType::Existdim() const
{
	return m_dim_Exist;
}
void Mp7JrsProbabilityDistributionType::Setdim(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::Setdim().");
	}
	m_dim = item;
	m_dim_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::Invalidatedim()
{
	m_dim_Exist = false;
}
Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetMean() const
{
		return m_Mean;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidMean() const
{
	return m_Mean_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetVariance() const
{
		return m_Variance;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidVariance() const
{
	return m_Variance_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetMin() const
{
		return m_Min;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidMin() const
{
	return m_Min_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetMax() const
{
		return m_Max;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidMax() const
{
	return m_Max_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetMode() const
{
		return m_Mode;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidMode() const
{
	return m_Mode_Exist;
}

Mp7JrsDoubleMatrixPtr Mp7JrsProbabilityDistributionType::GetMedian() const
{
		return m_Median;
}

// Element is optional
bool Mp7JrsProbabilityDistributionType::IsValidMedian() const
{
	return m_Median_Exist;
}

Mp7JrsProbabilityDistributionType_Moment_CollectionPtr Mp7JrsProbabilityDistributionType::GetMoment() const
{
		return m_Moment;
}

Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr Mp7JrsProbabilityDistributionType::GetCumulant() const
{
		return m_Cumulant;
}

void Mp7JrsProbabilityDistributionType::SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMean().");
	}
	if (m_Mean != item || m_Mean_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Mean);
		m_Mean = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Mean) m_Mean->SetParent(m_myself.getPointer());
		if (m_Mean && m_Mean->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Mean->UseTypeAttribute = true;
		}
		m_Mean_Exist = true;
		if(m_Mean != Mp7JrsDoubleMatrixPtr())
		{
			m_Mean->SetContentName(XMLString::transcode("Mean"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateMean()
{
	m_Mean_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetVariance().");
	}
	if (m_Variance != item || m_Variance_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Variance);
		m_Variance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Variance) m_Variance->SetParent(m_myself.getPointer());
		if (m_Variance && m_Variance->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Variance->UseTypeAttribute = true;
		}
		m_Variance_Exist = true;
		if(m_Variance != Mp7JrsDoubleMatrixPtr())
		{
			m_Variance->SetContentName(XMLString::transcode("Variance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateVariance()
{
	m_Variance_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMin().");
	}
	if (m_Min != item || m_Min_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Min);
		m_Min = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Min) m_Min->SetParent(m_myself.getPointer());
		if (m_Min && m_Min->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Min->UseTypeAttribute = true;
		}
		m_Min_Exist = true;
		if(m_Min != Mp7JrsDoubleMatrixPtr())
		{
			m_Min->SetContentName(XMLString::transcode("Min"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateMin()
{
	m_Min_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMax().");
	}
	if (m_Max != item || m_Max_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Max);
		m_Max = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max) m_Max->SetParent(m_myself.getPointer());
		if (m_Max && m_Max->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Max->UseTypeAttribute = true;
		}
		m_Max_Exist = true;
		if(m_Max != Mp7JrsDoubleMatrixPtr())
		{
			m_Max->SetContentName(XMLString::transcode("Max"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateMax()
{
	m_Max_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMode().");
	}
	if (m_Mode != item || m_Mode_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Mode);
		m_Mode = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Mode) m_Mode->SetParent(m_myself.getPointer());
		if (m_Mode && m_Mode->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Mode->UseTypeAttribute = true;
		}
		m_Mode_Exist = true;
		if(m_Mode != Mp7JrsDoubleMatrixPtr())
		{
			m_Mode->SetContentName(XMLString::transcode("Mode"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateMode()
{
	m_Mode_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMedian().");
	}
	if (m_Median != item || m_Median_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Median);
		m_Median = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Median) m_Median->SetParent(m_myself.getPointer());
		if (m_Median && m_Median->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Median->UseTypeAttribute = true;
		}
		m_Median_Exist = true;
		if(m_Median != Mp7JrsDoubleMatrixPtr())
		{
			m_Median->SetContentName(XMLString::transcode("Median"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidateMedian()
{
	m_Median_Exist = false;
}
void Mp7JrsProbabilityDistributionType::SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetMoment().");
	}
	if (m_Moment != item)
	{
		// Dc1Factory::DeleteObject(m_Moment);
		m_Moment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Moment) m_Moment->SetParent(m_myself.getPointer());
		if(m_Moment != Mp7JrsProbabilityDistributionType_Moment_CollectionPtr())
		{
			m_Moment->SetContentName(XMLString::transcode("Moment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetCumulant().");
	}
	if (m_Cumulant != item)
	{
		// Dc1Factory::DeleteObject(m_Cumulant);
		m_Cumulant = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Cumulant) m_Cumulant->SetParent(m_myself.getPointer());
		if(m_Cumulant != Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr())
		{
			m_Cumulant->SetContentName(XMLString::transcode("Cumulant"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsProbabilityDistributionType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsProbabilityDistributionType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsProbabilityDistributionType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsProbabilityDistributionType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsProbabilityDistributionType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsProbabilityDistributionType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsProbabilityDistributionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsProbabilityDistributionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsProbabilityDistributionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityDistributionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsProbabilityDistributionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsProbabilityDistributionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsProbabilityDistributionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsProbabilityDistributionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsProbabilityDistributionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityDistributionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsProbabilityDistributionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsProbabilityDistributionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsProbabilityDistributionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsProbabilityDistributionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsProbabilityDistributionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsProbabilityDistributionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsProbabilityDistributionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsProbabilityDistributionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dim")) == 0)
	{
		// dim is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Mean")) == 0)
	{
		// Mean is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMean()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMean(p, client);
					if((p = GetMean()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Variance")) == 0)
	{
		// Variance is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVariance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVariance(p, client);
					if((p = GetVariance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Min")) == 0)
	{
		// Min is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMin()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMin(p, client);
					if((p = GetMin()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max")) == 0)
	{
		// Max is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMax()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMax(p, client);
					if((p = GetMax()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Mode")) == 0)
	{
		// Mode is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMode()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMode(p, client);
					if((p = GetMode()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Median")) == 0)
	{
		// Median is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMedian()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMedian(p, client);
					if((p = GetMedian()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Moment")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Moment is item of type ProbabilityDistributionType_Moment_LocalType
		// in element collection ProbabilityDistributionType_Moment_CollectionType
		
		context->Found = true;
		Mp7JrsProbabilityDistributionType_Moment_CollectionPtr coll = GetMoment();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateProbabilityDistributionType_Moment_CollectionType; // FTT, check this
				SetMoment(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Moment_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityDistributionType_Moment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Cumulant")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Cumulant is item of type ProbabilityDistributionType_Cumulant_LocalType
		// in element collection ProbabilityDistributionType_Cumulant_CollectionType
		
		context->Found = true;
		Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr coll = GetCumulant();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateProbabilityDistributionType_Cumulant_CollectionType; // FTT, check this
				SetCumulant(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityDistributionType_Cumulant_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityDistributionType_Cumulant_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ProbabilityDistributionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ProbabilityDistributionType");
	}
	return result;
}

XMLCh * Mp7JrsProbabilityDistributionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsProbabilityDistributionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsProbabilityDistributionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ProbabilityDistributionType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dim_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_dim);
		element->setAttributeNS(X(""), X("dim"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Mean != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Mean->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Mean"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Mean->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Variance != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Variance->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Variance"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Variance->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Min != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Min->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Min"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Min->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Max != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Max->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Max"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Max->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Mode != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Mode->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Mode"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Mode->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Median != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Median->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Median"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Median->Serialize(doc, element, &elem);
	}
	if (m_Moment != Mp7JrsProbabilityDistributionType_Moment_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Moment->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Cumulant != Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Cumulant->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsProbabilityDistributionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dim")))
	{
		// deserialize value type
		this->Setdim(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("dim"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsProbabilityModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Mean"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Mean")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMean(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Variance"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Variance")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVariance(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Min"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Min")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMin(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Max"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Max")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMax(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Mode"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Mode")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMode(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Median"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Median")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMedian(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Moment"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Moment")) == 0))
		{
			// Deserialize factory type
			Mp7JrsProbabilityDistributionType_Moment_CollectionPtr tmp = CreateProbabilityDistributionType_Moment_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMoment(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Cumulant"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Cumulant")) == 0))
		{
			// Deserialize factory type
			Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr tmp = CreateProbabilityDistributionType_Cumulant_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCumulant(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsProbabilityDistributionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Mean")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetMean(child);
  }
  if (XMLString::compareString(elementname, X("Variance")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetVariance(child);
  }
  if (XMLString::compareString(elementname, X("Min")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetMin(child);
  }
  if (XMLString::compareString(elementname, X("Max")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetMax(child);
  }
  if (XMLString::compareString(elementname, X("Mode")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetMode(child);
  }
  if (XMLString::compareString(elementname, X("Median")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetMedian(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Moment")) == 0))
  {
	Mp7JrsProbabilityDistributionType_Moment_CollectionPtr tmp = CreateProbabilityDistributionType_Moment_CollectionType; // FTT, check this
	this->SetMoment(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Cumulant")) == 0))
  {
	Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr tmp = CreateProbabilityDistributionType_Cumulant_CollectionType; // FTT, check this
	this->SetCumulant(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsProbabilityDistributionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMode() != Dc1NodePtr())
		result.Insert(GetMode());
	if (GetMedian() != Dc1NodePtr())
		result.Insert(GetMedian());
	if (GetMoment() != Dc1NodePtr())
		result.Insert(GetMoment());
	if (GetCumulant() != Dc1NodePtr())
		result.Insert(GetCumulant());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_ExtMethodImpl.h


