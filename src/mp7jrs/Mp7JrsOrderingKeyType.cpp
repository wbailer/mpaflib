
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsOrderingKeyType_Selector_LocalType.h"
#include "Mp7JrsOrderingKeyType_Field_CollectionType.h"
#include "Mp7JrsOrderingKeyType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsOrderingKeyType_Field_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Field

#include <assert.h>
IMp7JrsOrderingKeyType::IMp7JrsOrderingKeyType()
{

// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtPropInit.h

}

IMp7JrsOrderingKeyType::~IMp7JrsOrderingKeyType()
{
// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtPropCleanup.h

}

Mp7JrsOrderingKeyType::Mp7JrsOrderingKeyType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsOrderingKeyType::~Mp7JrsOrderingKeyType()
{
	Cleanup();
}

void Mp7JrsOrderingKeyType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_name = NULL; // String
	m_name_Exist = false;
	m_semantics = NULL; // String
	m_semantics_Exist = false;
	m_direction = Mp7JrsOrderingKeyType_direction_LocalType::UninitializedEnumeration;
	m_direction_Default = Mp7JrsOrderingKeyType_direction_LocalType::descending; // Default enumeration
	m_direction_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Selector = Mp7JrsOrderingKeyType_Selector_LocalPtr(); // Class
	m_Field = Mp7JrsOrderingKeyType_Field_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtMyPropInit.h

}

void Mp7JrsOrderingKeyType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_name); // String
	XMLString::release(&m_semantics); // String
	// Dc1Factory::DeleteObject(m_Selector);
	// Dc1Factory::DeleteObject(m_Field);
}

void Mp7JrsOrderingKeyType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use OrderingKeyTypePtr, since we
	// might need GetBase(), which isn't defined in IOrderingKeyType
	const Dc1Ptr< Mp7JrsOrderingKeyType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsOrderingKeyType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_name); // String
	if (tmp->Existname())
	{
		this->Setname(XMLString::replicate(tmp->Getname()));
	}
	else
	{
		Invalidatename();
	}
	}
	{
	XMLString::release(&m_semantics); // String
	if (tmp->Existsemantics())
	{
		this->Setsemantics(XMLString::replicate(tmp->Getsemantics()));
	}
	else
	{
		Invalidatesemantics();
	}
	}
	{
	if (tmp->Existdirection())
	{
		this->Setdirection(tmp->Getdirection());
	}
	else
	{
		Invalidatedirection();
	}
	}
		// Dc1Factory::DeleteObject(m_Selector);
		this->SetSelector(Dc1Factory::CloneObject(tmp->GetSelector()));
		// Dc1Factory::DeleteObject(m_Field);
		this->SetField(Dc1Factory::CloneObject(tmp->GetField()));
}

Dc1NodePtr Mp7JrsOrderingKeyType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsOrderingKeyType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsOrderingKeyType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsOrderingKeyType::Getname() const
{
	return m_name;
}

bool Mp7JrsOrderingKeyType::Existname() const
{
	return m_name_Exist;
}
void Mp7JrsOrderingKeyType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::Setname().");
	}
	m_name = item;
	m_name_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOrderingKeyType::Invalidatename()
{
	m_name_Exist = false;
}
XMLCh * Mp7JrsOrderingKeyType::Getsemantics() const
{
	return m_semantics;
}

bool Mp7JrsOrderingKeyType::Existsemantics() const
{
	return m_semantics_Exist;
}
void Mp7JrsOrderingKeyType::Setsemantics(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::Setsemantics().");
	}
	m_semantics = item;
	m_semantics_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOrderingKeyType::Invalidatesemantics()
{
	m_semantics_Exist = false;
}
Mp7JrsOrderingKeyType_direction_LocalType::Enumeration Mp7JrsOrderingKeyType::Getdirection() const
{
	if (this->Existdirection()) {
		return m_direction;
	} else {
		return m_direction_Default;
	}
}

bool Mp7JrsOrderingKeyType::Existdirection() const
{
	return m_direction_Exist;
}
void Mp7JrsOrderingKeyType::Setdirection(Mp7JrsOrderingKeyType_direction_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::Setdirection().");
	}
	m_direction = item;
	m_direction_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOrderingKeyType::Invalidatedirection()
{
	m_direction_Exist = false;
}
Mp7JrsOrderingKeyType_Selector_LocalPtr Mp7JrsOrderingKeyType::GetSelector() const
{
		return m_Selector;
}

Mp7JrsOrderingKeyType_Field_CollectionPtr Mp7JrsOrderingKeyType::GetField() const
{
		return m_Field;
}

void Mp7JrsOrderingKeyType::SetSelector(const Mp7JrsOrderingKeyType_Selector_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::SetSelector().");
	}
	if (m_Selector != item)
	{
		// Dc1Factory::DeleteObject(m_Selector);
		m_Selector = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Selector) m_Selector->SetParent(m_myself.getPointer());
		if (m_Selector && m_Selector->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Selector_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Selector->UseTypeAttribute = true;
		}
		if(m_Selector != Mp7JrsOrderingKeyType_Selector_LocalPtr())
		{
			m_Selector->SetContentName(XMLString::transcode("Selector"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOrderingKeyType::SetField(const Mp7JrsOrderingKeyType_Field_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::SetField().");
	}
	if (m_Field != item)
	{
		// Dc1Factory::DeleteObject(m_Field);
		m_Field = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Field) m_Field->SetParent(m_myself.getPointer());
		if(m_Field != Mp7JrsOrderingKeyType_Field_CollectionPtr())
		{
			m_Field->SetContentName(XMLString::transcode("Field"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsOrderingKeyType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsOrderingKeyType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsOrderingKeyType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOrderingKeyType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOrderingKeyType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsOrderingKeyType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("name")) == 0)
	{
		// name is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("semantics")) == 0)
	{
		// semantics is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("direction")) == 0)
	{
		// direction is simple attribute OrderingKeyType_direction_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsOrderingKeyType_direction_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Selector")) == 0)
	{
		// Selector is simple element OrderingKeyType_Selector_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSelector()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Selector_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsOrderingKeyType_Selector_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSelector(p, client);
					if((p = GetSelector()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Field")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Field is item of type OrderingKeyType_Field_LocalType
		// in element collection OrderingKeyType_Field_CollectionType
		
		context->Found = true;
		Mp7JrsOrderingKeyType_Field_CollectionPtr coll = GetField();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateOrderingKeyType_Field_CollectionType; // FTT, check this
				SetField(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OrderingKeyType_Field_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsOrderingKeyType_Field_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for OrderingKeyType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "OrderingKeyType");
	}
	return result;
}

XMLCh * Mp7JrsOrderingKeyType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsOrderingKeyType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsOrderingKeyType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("OrderingKeyType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_name_Exist)
	{
	// String
	if(m_name != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("name"), m_name);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_semantics_Exist)
	{
	// String
	if(m_semantics != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("semantics"), m_semantics);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_direction_Exist)
	{
	// Enumeration
	if(m_direction != Mp7JrsOrderingKeyType_direction_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsOrderingKeyType_direction_LocalType::ToText(m_direction);
		element->setAttributeNS(X(""), X("direction"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Selector != Mp7JrsOrderingKeyType_Selector_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Selector->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Selector"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Selector->Serialize(doc, element, &elem);
	}
	if (m_Field != Mp7JrsOrderingKeyType_Field_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Field->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsOrderingKeyType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("name")))
	{
		// Deserialize string type
		this->Setname(Dc1Convert::TextToString(parent->getAttribute(X("name"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("semantics")))
	{
		// Deserialize string type
		this->Setsemantics(Dc1Convert::TextToString(parent->getAttribute(X("semantics"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("direction")))
	{
		this->Setdirection(Mp7JrsOrderingKeyType_direction_LocalType::Parse(parent->getAttribute(X("direction"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Selector"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Selector")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateOrderingKeyType_Selector_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSelector(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Field"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Field")) == 0))
		{
			// Deserialize factory type
			Mp7JrsOrderingKeyType_Field_CollectionPtr tmp = CreateOrderingKeyType_Field_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetField(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsOrderingKeyType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Selector")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateOrderingKeyType_Selector_LocalType; // FTT, check this
	}
	this->SetSelector(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Field")) == 0))
  {
	Mp7JrsOrderingKeyType_Field_CollectionPtr tmp = CreateOrderingKeyType_Field_CollectionType; // FTT, check this
	this->SetField(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsOrderingKeyType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSelector() != Dc1NodePtr())
		result.Insert(GetSelector());
	if (GetField() != Dc1NodePtr())
		result.Insert(GetField());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsOrderingKeyType_ExtMethodImpl.h


