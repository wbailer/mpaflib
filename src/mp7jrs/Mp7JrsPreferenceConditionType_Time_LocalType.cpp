
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtImplInclude.h


#include "Mp7JrsTimeType.h"
#include "Mp7JrsPreferenceConditionType_Time_recurrence_LocalType2.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsRelTimePointType.h"
#include "Mp7JrsRelIncrTimePointType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsIncrDurationType.h"
#include "Mp7JrsPreferenceConditionType_Time_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPreferenceConditionType_Time_LocalType::IMp7JrsPreferenceConditionType_Time_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtPropInit.h

}

IMp7JrsPreferenceConditionType_Time_LocalType::~IMp7JrsPreferenceConditionType_Time_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtPropCleanup.h

}

Mp7JrsPreferenceConditionType_Time_LocalType::Mp7JrsPreferenceConditionType_Time_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPreferenceConditionType_Time_LocalType::~Mp7JrsPreferenceConditionType_Time_LocalType()
{
	Cleanup();
}

void Mp7JrsPreferenceConditionType_Time_LocalType::Init()
{
	// Init base
	m_Base = CreateTimeType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_recurrence = Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr(); // Create emtpy class ptr
	m_recurrence_Exist = false;
	m_numOfRecurrences = 1; // Value
	m_numOfRecurrences_Exist = false;



// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtMyPropInit.h

}

void Mp7JrsPreferenceConditionType_Time_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_recurrence);
}

void Mp7JrsPreferenceConditionType_Time_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PreferenceConditionType_Time_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPreferenceConditionType_Time_LocalType
	const Dc1Ptr< Mp7JrsPreferenceConditionType_Time_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTimePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPreferenceConditionType_Time_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_recurrence);
	if (tmp->Existrecurrence())
	{
		this->Setrecurrence(Dc1Factory::CloneObject(tmp->Getrecurrence()));
	}
	else
	{
		Invalidaterecurrence();
	}
	}
	{
	if (tmp->ExistnumOfRecurrences())
	{
		this->SetnumOfRecurrences(tmp->GetnumOfRecurrences());
	}
	else
	{
		InvalidatenumOfRecurrences();
	}
	}
}

Dc1NodePtr Mp7JrsPreferenceConditionType_Time_LocalType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsPreferenceConditionType_Time_LocalType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsTimeType > Mp7JrsPreferenceConditionType_Time_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr Mp7JrsPreferenceConditionType_Time_LocalType::Getrecurrence() const
{
	if (this->Existrecurrence()) {
		return m_recurrence;
	} else {
		return m_recurrence_Default;
	}
}

bool Mp7JrsPreferenceConditionType_Time_LocalType::Existrecurrence() const
{
	return m_recurrence_Exist;
}
void Mp7JrsPreferenceConditionType_Time_LocalType::Setrecurrence(const Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::Setrecurrence().");
	}
	m_recurrence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_recurrence) m_recurrence->SetParent(m_myself.getPointer());
	m_recurrence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPreferenceConditionType_Time_LocalType::Invalidaterecurrence()
{
	m_recurrence_Exist = false;
}
unsigned Mp7JrsPreferenceConditionType_Time_LocalType::GetnumOfRecurrences() const
{
	return m_numOfRecurrences;
}

bool Mp7JrsPreferenceConditionType_Time_LocalType::ExistnumOfRecurrences() const
{
	return m_numOfRecurrences_Exist;
}
void Mp7JrsPreferenceConditionType_Time_LocalType::SetnumOfRecurrences(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetnumOfRecurrences().");
	}
	m_numOfRecurrences = item;
	m_numOfRecurrences_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPreferenceConditionType_Time_LocalType::InvalidatenumOfRecurrences()
{
	m_numOfRecurrences_Exist = false;
}
Mp7JrstimePointPtr Mp7JrsPreferenceConditionType_Time_LocalType::GetTimePoint() const
{
	return GetBase()->GetTimePoint();
}

Mp7JrsRelTimePointPtr Mp7JrsPreferenceConditionType_Time_LocalType::GetRelTimePoint() const
{
	return GetBase()->GetRelTimePoint();
}

Mp7JrsRelIncrTimePointPtr Mp7JrsPreferenceConditionType_Time_LocalType::GetRelIncrTimePoint() const
{
	return GetBase()->GetRelIncrTimePoint();
}

Mp7JrsdurationPtr Mp7JrsPreferenceConditionType_Time_LocalType::GetDuration() const
{
	return GetBase()->GetDuration();
}

Mp7JrsIncrDurationPtr Mp7JrsPreferenceConditionType_Time_LocalType::GetIncrDuration() const
{
	return GetBase()->GetIncrDuration();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPreferenceConditionType_Time_LocalType::SetTimePoint(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetTimePoint().");
	}
	GetBase()->SetTimePoint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPreferenceConditionType_Time_LocalType::SetRelTimePoint(const Mp7JrsRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetRelTimePoint().");
	}
	GetBase()->SetRelTimePoint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPreferenceConditionType_Time_LocalType::SetRelIncrTimePoint(const Mp7JrsRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetRelIncrTimePoint().");
	}
	GetBase()->SetRelIncrTimePoint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPreferenceConditionType_Time_LocalType::SetDuration(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetDuration().");
	}
	GetBase()->SetDuration(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPreferenceConditionType_Time_LocalType::SetIncrDuration(const Mp7JrsIncrDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPreferenceConditionType_Time_LocalType::SetIncrDuration().");
	}
	GetBase()->SetIncrDuration(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPreferenceConditionType_Time_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("recurrence")) == 0)
	{
		// recurrence is simple attribute PreferenceConditionType_Time_recurrence_LocalType2
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfRecurrences")) == 0)
	{
		// numOfRecurrences is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PreferenceConditionType_Time_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PreferenceConditionType_Time_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPreferenceConditionType_Time_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPreferenceConditionType_Time_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsPreferenceConditionType_Time_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PreferenceConditionType_Time_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_recurrence_Exist)
	{
	// Class
	if (m_recurrence != Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr())
	{
		XMLCh * tmp = m_recurrence->ToText();
		element->setAttributeNS(X(""), X("recurrence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_numOfRecurrences_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_numOfRecurrences);
		element->setAttributeNS(X(""), X("numOfRecurrences"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsPreferenceConditionType_Time_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("recurrence")))
	{
		// Deserialize class type
		Mp7JrsPreferenceConditionType_Time_recurrence_Local2Ptr tmp = CreatePreferenceConditionType_Time_recurrence_LocalType2; // FTT, check this
		tmp->Parse(parent->getAttribute(X("recurrence")));
		this->Setrecurrence(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfRecurrences")))
	{
		// deserialize value type
		this->SetnumOfRecurrences(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("numOfRecurrences"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsTimeType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPreferenceConditionType_Time_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPreferenceConditionType_Time_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimePoint() != Dc1NodePtr())
		result.Insert(GetTimePoint());
	if (GetRelTimePoint() != Dc1NodePtr())
		result.Insert(GetRelTimePoint());
	if (GetRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetRelIncrTimePoint());
	if (GetDuration() != Dc1NodePtr())
		result.Insert(GetDuration());
	if (GetIncrDuration() != Dc1NodePtr())
		result.Insert(GetIncrDuration());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPreferenceConditionType_Time_LocalType_ExtMethodImpl.h


