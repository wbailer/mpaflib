
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsShotType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsGlobalTransitionType.h"
#include "Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::~IMp7JrsEditedVideoEditingTemporalDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::~Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Shot = Mp7JrsShotPtr(); // Class
	m_ShotRef = Mp7JrsReferencePtr(); // Class
	m_GlobalTransition = Mp7JrsGlobalTransitionPtr(); // Class
	m_GlobalTransitionRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Shot);
	// Dc1Factory::DeleteObject(m_ShotRef);
	// Dc1Factory::DeleteObject(m_GlobalTransition);
	// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use EditedVideoEditingTemporalDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IEditedVideoEditingTemporalDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Shot);
		this->SetShot(Dc1Factory::CloneObject(tmp->GetShot()));
		// Dc1Factory::DeleteObject(m_ShotRef);
		this->SetShotRef(Dc1Factory::CloneObject(tmp->GetShotRef()));
		// Dc1Factory::DeleteObject(m_GlobalTransition);
		this->SetGlobalTransition(Dc1Factory::CloneObject(tmp->GetGlobalTransition()));
		// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
		this->SetGlobalTransitionRef(Dc1Factory::CloneObject(tmp->GetGlobalTransitionRef()));
}

Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsShotPtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetShot() const
{
		return m_Shot;
}

Mp7JrsReferencePtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetShotRef() const
{
		return m_ShotRef;
}

Mp7JrsGlobalTransitionPtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetGlobalTransition() const
{
		return m_GlobalTransition;
}

Mp7JrsReferencePtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetGlobalTransitionRef() const
{
		return m_GlobalTransitionRef;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetShot(const Mp7JrsShotPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetShot().");
	}
	if (m_Shot != item)
	{
		// Dc1Factory::DeleteObject(m_Shot);
		m_Shot = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Shot) m_Shot->SetParent(m_myself.getPointer());
		if (m_Shot && m_Shot->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Shot->UseTypeAttribute = true;
		}
		if(m_Shot != Mp7JrsShotPtr())
		{
			m_Shot->SetContentName(XMLString::transcode("Shot"));
		}
	// Dc1Factory::DeleteObject(m_ShotRef);
	m_ShotRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_GlobalTransition);
	m_GlobalTransition = Mp7JrsGlobalTransitionPtr();
	// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
	m_GlobalTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetShotRef().");
	}
	if (m_ShotRef != item)
	{
		// Dc1Factory::DeleteObject(m_ShotRef);
		m_ShotRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ShotRef) m_ShotRef->SetParent(m_myself.getPointer());
		if (m_ShotRef && m_ShotRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ShotRef->UseTypeAttribute = true;
		}
		if(m_ShotRef != Mp7JrsReferencePtr())
		{
			m_ShotRef->SetContentName(XMLString::transcode("ShotRef"));
		}
	// Dc1Factory::DeleteObject(m_Shot);
	m_Shot = Mp7JrsShotPtr();
	// Dc1Factory::DeleteObject(m_GlobalTransition);
	m_GlobalTransition = Mp7JrsGlobalTransitionPtr();
	// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
	m_GlobalTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetGlobalTransition(const Mp7JrsGlobalTransitionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetGlobalTransition().");
	}
	if (m_GlobalTransition != item)
	{
		// Dc1Factory::DeleteObject(m_GlobalTransition);
		m_GlobalTransition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GlobalTransition) m_GlobalTransition->SetParent(m_myself.getPointer());
		if (m_GlobalTransition && m_GlobalTransition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GlobalTransitionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GlobalTransition->UseTypeAttribute = true;
		}
		if(m_GlobalTransition != Mp7JrsGlobalTransitionPtr())
		{
			m_GlobalTransition->SetContentName(XMLString::transcode("GlobalTransition"));
		}
	// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
	m_GlobalTransitionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_Shot);
	m_Shot = Mp7JrsShotPtr();
	// Dc1Factory::DeleteObject(m_ShotRef);
	m_ShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetGlobalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::SetGlobalTransitionRef().");
	}
	if (m_GlobalTransitionRef != item)
	{
		// Dc1Factory::DeleteObject(m_GlobalTransitionRef);
		m_GlobalTransitionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GlobalTransitionRef) m_GlobalTransitionRef->SetParent(m_myself.getPointer());
		if (m_GlobalTransitionRef && m_GlobalTransitionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GlobalTransitionRef->UseTypeAttribute = true;
		}
		if(m_GlobalTransitionRef != Mp7JrsReferencePtr())
		{
			m_GlobalTransitionRef->SetContentName(XMLString::transcode("GlobalTransitionRef"));
		}
	// Dc1Factory::DeleteObject(m_GlobalTransition);
	m_GlobalTransition = Mp7JrsGlobalTransitionPtr();
	// Dc1Factory::DeleteObject(m_Shot);
	m_Shot = Mp7JrsShotPtr();
	// Dc1Factory::DeleteObject(m_ShotRef);
	m_ShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: EditedVideoEditingTemporalDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("EditedVideoEditingTemporalDecompositionType_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Shot != Mp7JrsShotPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Shot->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Shot"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Shot->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ShotRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ShotRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ShotRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ShotRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GlobalTransition != Mp7JrsGlobalTransitionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GlobalTransition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GlobalTransition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GlobalTransition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GlobalTransitionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GlobalTransitionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GlobalTransitionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GlobalTransitionRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Shot"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Shot")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShotType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShot(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ShotRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShotRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GlobalTransition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GlobalTransition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGlobalTransitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGlobalTransition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GlobalTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GlobalTransitionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGlobalTransitionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	} while(false);
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Shot")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShotType; // FTT, check this
	}
	this->SetShot(child);
  }
  if (XMLString::compareString(elementname, X("ShotRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetShotRef(child);
  }
  if (XMLString::compareString(elementname, X("GlobalTransition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGlobalTransitionType; // FTT, check this
	}
	this->SetGlobalTransition(child);
  }
  if (XMLString::compareString(elementname, X("GlobalTransitionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetGlobalTransitionRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetShot() != Dc1NodePtr())
		result.Insert(GetShot());
	if (GetShotRef() != Dc1NodePtr())
		result.Insert(GetShotRef());
	if (GetGlobalTransition() != Dc1NodePtr())
		result.Insert(GetGlobalTransition());
	if (GetGlobalTransitionRef() != Dc1NodePtr())
		result.Insert(GetGlobalTransitionRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType_ExtMethodImpl.h


