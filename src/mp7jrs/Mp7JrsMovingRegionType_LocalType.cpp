
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsVisualDSType.h"
#include "Mp7JrsVisualTimeSeriesType.h"
#include "Mp7JrsGofGopFeatureType.h"
#include "Mp7JrsMovingRegionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsMovingRegionType_LocalType::IMp7JrsMovingRegionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtPropInit.h

}

IMp7JrsMovingRegionType_LocalType::~IMp7JrsMovingRegionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtPropCleanup.h

}

Mp7JrsMovingRegionType_LocalType::Mp7JrsMovingRegionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMovingRegionType_LocalType::~Mp7JrsMovingRegionType_LocalType()
{
	Cleanup();
}

void Mp7JrsMovingRegionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_VisualDescriptor = Dc1Ptr< Dc1Node >(); // Class
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >(); // Class
	m_VisualTimeSeriesDescriptor = Dc1Ptr< Dc1Node >(); // Class
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsMovingRegionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
	// Dc1Factory::DeleteObject(m_GofGopFeature);
}

void Mp7JrsMovingRegionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MovingRegionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMovingRegionType_LocalType
	const Dc1Ptr< Mp7JrsMovingRegionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		this->SetVisualDescriptor(Dc1Factory::CloneObject(tmp->GetVisualDescriptor()));
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		this->SetVisualDescriptionScheme(Dc1Factory::CloneObject(tmp->GetVisualDescriptionScheme()));
		// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
		this->SetVisualTimeSeriesDescriptor(Dc1Factory::CloneObject(tmp->GetVisualTimeSeriesDescriptor()));
		// Dc1Factory::DeleteObject(m_GofGopFeature);
		this->SetGofGopFeature(Dc1Factory::CloneObject(tmp->GetGofGopFeature()));
}

Dc1NodePtr Mp7JrsMovingRegionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMovingRegionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1Ptr< Dc1Node > Mp7JrsMovingRegionType_LocalType::GetVisualDescriptor() const
{
		return m_VisualDescriptor;
}

Dc1Ptr< Dc1Node > Mp7JrsMovingRegionType_LocalType::GetVisualDescriptionScheme() const
{
		return m_VisualDescriptionScheme;
}

Dc1Ptr< Dc1Node > Mp7JrsMovingRegionType_LocalType::GetVisualTimeSeriesDescriptor() const
{
		return m_VisualTimeSeriesDescriptor;
}

Mp7JrsGofGopFeaturePtr Mp7JrsMovingRegionType_LocalType::GetGofGopFeature() const
{
		return m_GofGopFeature;
}

// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionType_LocalType::SetVisualDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType_LocalType::SetVisualDescriptor().");
	}
	if (m_VisualDescriptor != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		m_VisualDescriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptor) m_VisualDescriptor->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualDescriptor) m_VisualDescriptor->UseTypeAttribute = true;
		if(m_VisualDescriptor != Dc1Ptr< Dc1Node >())
		{
			m_VisualDescriptor->SetContentName(XMLString::transcode("VisualDescriptor"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
	m_VisualTimeSeriesDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType_LocalType::SetVisualDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType_LocalType::SetVisualDescriptionScheme().");
	}
	if (m_VisualDescriptionScheme != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		m_VisualDescriptionScheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptionScheme) m_VisualDescriptionScheme->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualDescriptionScheme) m_VisualDescriptionScheme->UseTypeAttribute = true;
		if(m_VisualDescriptionScheme != Dc1Ptr< Dc1Node >())
		{
			m_VisualDescriptionScheme->SetContentName(XMLString::transcode("VisualDescriptionScheme"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
	m_VisualTimeSeriesDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType_LocalType::SetVisualTimeSeriesDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType_LocalType::SetVisualTimeSeriesDescriptor().");
	}
	if (m_VisualTimeSeriesDescriptor != item)
	{
		// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
		m_VisualTimeSeriesDescriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualTimeSeriesDescriptor) m_VisualTimeSeriesDescriptor->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualTimeSeriesDescriptor) m_VisualTimeSeriesDescriptor->UseTypeAttribute = true;
		if(m_VisualTimeSeriesDescriptor != Dc1Ptr< Dc1Node >())
		{
			m_VisualTimeSeriesDescriptor->SetContentName(XMLString::transcode("VisualTimeSeriesDescriptor"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType_LocalType::SetGofGopFeature(const Mp7JrsGofGopFeaturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType_LocalType::SetGofGopFeature().");
	}
	if (m_GofGopFeature != item)
	{
		// Dc1Factory::DeleteObject(m_GofGopFeature);
		m_GofGopFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GofGopFeature) m_GofGopFeature->SetParent(m_myself.getPointer());
		if (m_GofGopFeature && m_GofGopFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GofGopFeature->UseTypeAttribute = true;
		}
		if(m_GofGopFeature != Mp7JrsGofGopFeaturePtr())
		{
			m_GofGopFeature->SetContentName(XMLString::transcode("GofGopFeature"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualTimeSeriesDescriptor);
	m_VisualTimeSeriesDescriptor = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: MovingRegionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsMovingRegionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsMovingRegionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMovingRegionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMovingRegionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_VisualDescriptor != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualDescriptor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualDescriptor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualDescriptor->UseTypeAttribute = true;
		}
		m_VisualDescriptor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VisualDescriptionScheme != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualDescriptionScheme->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualDescriptionScheme"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualDescriptionScheme->UseTypeAttribute = true;
		}
		m_VisualDescriptionScheme->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VisualTimeSeriesDescriptor != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualTimeSeriesDescriptor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualTimeSeriesDescriptor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualTimeSeriesDescriptor->UseTypeAttribute = true;
		}
		m_VisualTimeSeriesDescriptor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GofGopFeature != Mp7JrsGofGopFeaturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GofGopFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GofGopFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GofGopFeature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMovingRegionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualDescriptor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualDescriptor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualDescriptionScheme")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDSType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualDescriptionScheme(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualTimeSeriesDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualTimeSeriesDescriptor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualTimeSeriesType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualTimeSeriesDescriptor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GofGopFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGofGopFeatureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGofGopFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMovingRegionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("VisualDescriptor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDType; // FTT, check this
	}
	this->SetVisualDescriptor(child);
  }
  if (XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDSType; // FTT, check this
	}
	this->SetVisualDescriptionScheme(child);
  }
  if (XMLString::compareString(elementname, X("VisualTimeSeriesDescriptor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualTimeSeriesType; // FTT, check this
	}
	this->SetVisualTimeSeriesDescriptor(child);
  }
  if (XMLString::compareString(elementname, X("GofGopFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGofGopFeatureType; // FTT, check this
	}
	this->SetGofGopFeature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMovingRegionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVisualDescriptor() != Dc1NodePtr())
		result.Insert(GetVisualDescriptor());
	if (GetVisualDescriptionScheme() != Dc1NodePtr())
		result.Insert(GetVisualDescriptionScheme());
	if (GetVisualTimeSeriesDescriptor() != Dc1NodePtr())
		result.Insert(GetVisualTimeSeriesDescriptor());
	if (GetGofGopFeature() != Dc1NodePtr())
		result.Insert(GetGofGopFeature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMovingRegionType_LocalType_ExtMethodImpl.h


