
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsMatchingHintType_Hint_CollectionType.h"
#include "Mp7JrsMatchingHintType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMatchingHintType_Hint_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Hint

#include <assert.h>
IMp7JrsMatchingHintType::IMp7JrsMatchingHintType()
{

// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtPropInit.h

}

IMp7JrsMatchingHintType::~IMp7JrsMatchingHintType()
{
// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtPropCleanup.h

}

Mp7JrsMatchingHintType::Mp7JrsMatchingHintType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMatchingHintType::~Mp7JrsMatchingHintType()
{
	Cleanup();
}

void Mp7JrsMatchingHintType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_reliability = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_reliability->SetContent(0.0); // Use Value initialiser (xxx2)
	m_reliability_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Hint = Mp7JrsMatchingHintType_Hint_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtMyPropInit.h

}

void Mp7JrsMatchingHintType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_reliability);
	// Dc1Factory::DeleteObject(m_Hint);
}

void Mp7JrsMatchingHintType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MatchingHintTypePtr, since we
	// might need GetBase(), which isn't defined in IMatchingHintType
	const Dc1Ptr< Mp7JrsMatchingHintType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMatchingHintType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_reliability);
	if (tmp->Existreliability())
	{
		this->Setreliability(Dc1Factory::CloneObject(tmp->Getreliability()));
	}
	else
	{
		Invalidatereliability();
	}
	}
		// Dc1Factory::DeleteObject(m_Hint);
		this->SetHint(Dc1Factory::CloneObject(tmp->GetHint()));
}

Dc1NodePtr Mp7JrsMatchingHintType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMatchingHintType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsMatchingHintType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsMatchingHintType::Getreliability() const
{
	if (this->Existreliability()) {
		return m_reliability;
	} else {
		return m_reliability_Default;
	}
}

bool Mp7JrsMatchingHintType::Existreliability() const
{
	return m_reliability_Exist;
}
void Mp7JrsMatchingHintType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMatchingHintType::Setreliability().");
	}
	m_reliability = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_reliability) m_reliability->SetParent(m_myself.getPointer());
	m_reliability_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMatchingHintType::Invalidatereliability()
{
	m_reliability_Exist = false;
}
Mp7JrsMatchingHintType_Hint_CollectionPtr Mp7JrsMatchingHintType::GetHint() const
{
		return m_Hint;
}

void Mp7JrsMatchingHintType::SetHint(const Mp7JrsMatchingHintType_Hint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMatchingHintType::SetHint().");
	}
	if (m_Hint != item)
	{
		// Dc1Factory::DeleteObject(m_Hint);
		m_Hint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Hint) m_Hint->SetParent(m_myself.getPointer());
		if(m_Hint != Mp7JrsMatchingHintType_Hint_CollectionPtr())
		{
			m_Hint->SetContentName(XMLString::transcode("Hint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMatchingHintType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("reliability")) == 0)
	{
		// reliability is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Hint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Hint is item of type MatchingHintType_Hint_LocalType
		// in element collection MatchingHintType_Hint_CollectionType
		
		context->Found = true;
		Mp7JrsMatchingHintType_Hint_CollectionPtr coll = GetHint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMatchingHintType_Hint_CollectionType; // FTT, check this
				SetHint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MatchingHintType_Hint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMatchingHintType_Hint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MatchingHintType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MatchingHintType");
	}
	return result;
}

XMLCh * Mp7JrsMatchingHintType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMatchingHintType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMatchingHintType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MatchingHintType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_reliability_Exist)
	{
	// Class
	if (m_reliability != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_reliability->ToText();
		element->setAttributeNS(X(""), X("reliability"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Hint != Mp7JrsMatchingHintType_Hint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Hint->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMatchingHintType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("reliability")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("reliability")));
		this->Setreliability(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Hint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Hint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMatchingHintType_Hint_CollectionPtr tmp = CreateMatchingHintType_Hint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetHint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMatchingHintType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Hint")) == 0))
  {
	Mp7JrsMatchingHintType_Hint_CollectionPtr tmp = CreateMatchingHintType_Hint_CollectionType; // FTT, check this
	this->SetHint(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMatchingHintType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHint() != Dc1NodePtr())
		result.Insert(GetHint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMatchingHintType_ExtMethodImpl.h


