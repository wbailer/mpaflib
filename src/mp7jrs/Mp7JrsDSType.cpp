
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDSType_ExtImplInclude.h


#include "Mp7JrsMpeg7BaseType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDSType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsDSType::IMp7JrsDSType()
{

// no includefile for extension defined 
// file Mp7JrsDSType_ExtPropInit.h

}

IMp7JrsDSType::~IMp7JrsDSType()
{
// no includefile for extension defined 
// file Mp7JrsDSType_ExtPropCleanup.h

}

Mp7JrsDSType::Mp7JrsDSType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDSType::~Mp7JrsDSType()
{
	Cleanup();
}

void Mp7JrsDSType::Init()
{
	// Init base
	m_Base = CreateMpeg7BaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_id = NULL; // String
	m_id_Exist = false;
	m_timeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_timeBase_Exist = false;
	m_timeUnit = Mp7JrsdurationPtr(); // Pattern
	m_timeUnit_Exist = false;
	m_mediaTimeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_mediaTimeBase_Exist = false;
	m_mediaTimeUnit = Mp7JrsmediaDurationPtr(); // Pattern
	m_mediaTimeUnit_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Header = Mp7JrsDSType_Header_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDSType_ExtMyPropInit.h

}

void Mp7JrsDSType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDSType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_id); // String
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
	// Dc1Factory::DeleteObject(m_timeUnit); // Pattern
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
	// Dc1Factory::DeleteObject(m_Header);
}

void Mp7JrsDSType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DSTypePtr, since we
	// might need GetBase(), which isn't defined in IDSType
	const Dc1Ptr< Mp7JrsDSType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMpeg7BasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDSType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_id); // String
	if (tmp->Existid())
	{
		this->Setid(XMLString::replicate(tmp->Getid()));
	}
	else
	{
		Invalidateid();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
	if (tmp->ExisttimeBase())
	{
		this->SettimeBase(Dc1Factory::CloneObject(tmp->GettimeBase()));
	}
	else
	{
		InvalidatetimeBase();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeUnit); // Pattern
	if (tmp->ExisttimeUnit())
	{
		this->SettimeUnit(Dc1Factory::CloneObject(tmp->GettimeUnit()));
	}
	else
	{
		InvalidatetimeUnit();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
	if (tmp->ExistmediaTimeBase())
	{
		this->SetmediaTimeBase(Dc1Factory::CloneObject(tmp->GetmediaTimeBase()));
	}
	else
	{
		InvalidatemediaTimeBase();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
	if (tmp->ExistmediaTimeUnit())
	{
		this->SetmediaTimeUnit(Dc1Factory::CloneObject(tmp->GetmediaTimeUnit()));
	}
	else
	{
		InvalidatemediaTimeUnit();
	}
	}
		// Dc1Factory::DeleteObject(m_Header);
		this->SetHeader(Dc1Factory::CloneObject(tmp->GetHeader()));
}

Dc1NodePtr Mp7JrsDSType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsDSType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMpeg7BaseType > Mp7JrsDSType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsDSType::Getid() const
{
	return m_id;
}

bool Mp7JrsDSType::Existid() const
{
	return m_id_Exist;
}
void Mp7JrsDSType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::Setid().");
	}
	m_id = item;
	m_id_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDSType::Invalidateid()
{
	m_id_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsDSType::GettimeBase() const
{
	return m_timeBase;
}

bool Mp7JrsDSType::ExisttimeBase() const
{
	return m_timeBase_Exist;
}
void Mp7JrsDSType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::SettimeBase().");
	}
	m_timeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeBase) m_timeBase->SetParent(m_myself.getPointer());
	m_timeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDSType::InvalidatetimeBase()
{
	m_timeBase_Exist = false;
}
Mp7JrsdurationPtr Mp7JrsDSType::GettimeUnit() const
{
	return m_timeUnit;
}

bool Mp7JrsDSType::ExisttimeUnit() const
{
	return m_timeUnit_Exist;
}
void Mp7JrsDSType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::SettimeUnit().");
	}
	m_timeUnit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeUnit) m_timeUnit->SetParent(m_myself.getPointer());
	m_timeUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDSType::InvalidatetimeUnit()
{
	m_timeUnit_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsDSType::GetmediaTimeBase() const
{
	return m_mediaTimeBase;
}

bool Mp7JrsDSType::ExistmediaTimeBase() const
{
	return m_mediaTimeBase_Exist;
}
void Mp7JrsDSType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::SetmediaTimeBase().");
	}
	m_mediaTimeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeBase) m_mediaTimeBase->SetParent(m_myself.getPointer());
	m_mediaTimeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDSType::InvalidatemediaTimeBase()
{
	m_mediaTimeBase_Exist = false;
}
Mp7JrsmediaDurationPtr Mp7JrsDSType::GetmediaTimeUnit() const
{
	return m_mediaTimeUnit;
}

bool Mp7JrsDSType::ExistmediaTimeUnit() const
{
	return m_mediaTimeUnit_Exist;
}
void Mp7JrsDSType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::SetmediaTimeUnit().");
	}
	m_mediaTimeUnit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeUnit) m_mediaTimeUnit->SetParent(m_myself.getPointer());
	m_mediaTimeUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDSType::InvalidatemediaTimeUnit()
{
	m_mediaTimeUnit_Exist = false;
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDSType::GetHeader() const
{
		return m_Header;
}

void Mp7JrsDSType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDSType::SetHeader().");
	}
	if (m_Header != item)
	{
		// Dc1Factory::DeleteObject(m_Header);
		m_Header = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Header) m_Header->SetParent(m_myself.getPointer());
		if(m_Header != Mp7JrsDSType_Header_CollectionPtr())
		{
			m_Header->SetContentName(XMLString::transcode("Header"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDSType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("id")) == 0)
	{
		// id is simple attribute ID
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeBase")) == 0)
	{
		// timeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeUnit")) == 0)
	{
		// timeUnit is simple attribute durationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsdurationPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeBase")) == 0)
	{
		// mediaTimeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeUnit")) == 0)
	{
		// mediaTimeUnit is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Header")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Header is item of abstract type HeaderType
		// in element collection DSType_Header_CollectionType
		
		context->Found = true;
		Mp7JrsDSType_Header_CollectionPtr coll = GetHeader();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDSType_Header_CollectionType; // FTT, check this
				SetHeader(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsHeaderPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DSType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DSType");
	}
	return result;
}

XMLCh * Mp7JrsDSType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsDSType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsDSType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDSType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DSType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_id_Exist)
	{
	// String
	if(m_id != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("id"), m_id);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeBase_Exist)
	{
	// Pattern
	if (m_timeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_timeBase->ToText();
		element->setAttributeNS(X(""), X("timeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeUnit_Exist)
	{
	// Pattern
	if (m_timeUnit != Mp7JrsdurationPtr())
	{
		XMLCh * tmp = m_timeUnit->ToText();
		element->setAttributeNS(X(""), X("timeUnit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeBase_Exist)
	{
	// Pattern
	if (m_mediaTimeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_mediaTimeBase->ToText();
		element->setAttributeNS(X(""), X("mediaTimeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeUnit_Exist)
	{
	// Pattern
	if (m_mediaTimeUnit != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_mediaTimeUnit->ToText();
		element->setAttributeNS(X(""), X("mediaTimeUnit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Header != Mp7JrsDSType_Header_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Header->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDSType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("id")))
	{
		// Deserialize string type
		this->Setid(Dc1Convert::TextToString(parent->getAttribute(X("id"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeBase")));
		this->SettimeBase(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeUnit")))
	{
		Mp7JrsdurationPtr tmp = CreatedurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeUnit")));
		this->SettimeUnit(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeBase")));
		this->SetmediaTimeBase(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeUnit")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeUnit")));
		this->SetmediaTimeUnit(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsMpeg7BaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Header"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Header")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDSType_Header_CollectionPtr tmp = CreateDSType_Header_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetHeader(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDSType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDSType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Header")) == 0))
  {
	Mp7JrsDSType_Header_CollectionPtr tmp = CreateDSType_Header_CollectionType; // FTT, check this
	this->SetHeader(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDSType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDSType_ExtMethodImpl.h


