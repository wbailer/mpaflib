
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtImplInclude.h


#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDescriptionProfileType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsMpeg7Type.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMpeg7Type::IMp7JrsMpeg7Type()
{

// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtPropInit.h

}

IMp7JrsMpeg7Type::~IMp7JrsMpeg7Type()
{
// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtPropCleanup.h

}

Mp7JrsMpeg7Type::Mp7JrsMpeg7Type()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMpeg7Type::~Mp7JrsMpeg7Type()
{
	Cleanup();
}

void Mp7JrsMpeg7Type::Init()
{

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;
	m_timeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_timeBase_Exist = false;
	m_timeUnit = Mp7JrsdurationPtr(); // Pattern
	m_timeUnit_Exist = false;
	m_mediaTimeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_mediaTimeBase_Exist = false;
	m_mediaTimeUnit = Mp7JrsmediaDurationPtr(); // Pattern
	m_mediaTimeUnit_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_DescriptionProfile = Mp7JrsDescriptionProfilePtr(); // Class
	m_DescriptionProfile_Exist = false;
	m_DescriptionMetadata = Mp7JrsDescriptionMetadataPtr(); // Class
	m_DescriptionMetadata_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtMyPropInit.h

}

void Mp7JrsMpeg7Type::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtMyPropCleanup.h


	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
	// Dc1Factory::DeleteObject(m_timeUnit); // Pattern
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
	// Dc1Factory::DeleteObject(m_DescriptionProfile);
	// Dc1Factory::DeleteObject(m_DescriptionMetadata);
}

void Mp7JrsMpeg7Type::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Mpeg7TypePtr, since we
	// might need GetBase(), which isn't defined in IMpeg7Type
	const Dc1Ptr< Mp7JrsMpeg7Type > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeBase); // Pattern
	if (tmp->ExisttimeBase())
	{
		this->SettimeBase(Dc1Factory::CloneObject(tmp->GettimeBase()));
	}
	else
	{
		InvalidatetimeBase();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeUnit); // Pattern
	if (tmp->ExisttimeUnit())
	{
		this->SettimeUnit(Dc1Factory::CloneObject(tmp->GettimeUnit()));
	}
	else
	{
		InvalidatetimeUnit();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
	if (tmp->ExistmediaTimeBase())
	{
		this->SetmediaTimeBase(Dc1Factory::CloneObject(tmp->GetmediaTimeBase()));
	}
	else
	{
		InvalidatemediaTimeBase();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
	if (tmp->ExistmediaTimeUnit())
	{
		this->SetmediaTimeUnit(Dc1Factory::CloneObject(tmp->GetmediaTimeUnit()));
	}
	else
	{
		InvalidatemediaTimeUnit();
	}
	}
	if (tmp->IsValidDescriptionProfile())
	{
		// Dc1Factory::DeleteObject(m_DescriptionProfile);
		this->SetDescriptionProfile(Dc1Factory::CloneObject(tmp->GetDescriptionProfile()));
	}
	else
	{
		InvalidateDescriptionProfile();
	}
	if (tmp->IsValidDescriptionMetadata())
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		this->SetDescriptionMetadata(Dc1Factory::CloneObject(tmp->GetDescriptionMetadata()));
	}
	else
	{
		InvalidateDescriptionMetadata();
	}
}

Dc1NodePtr Mp7JrsMpeg7Type::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMpeg7Type::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsMpeg7Type::Getlang() const
{
	return m_lang;
}

bool Mp7JrsMpeg7Type::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsMpeg7Type::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsMpeg7Type::GettimeBase() const
{
	return m_timeBase;
}

bool Mp7JrsMpeg7Type::ExisttimeBase() const
{
	return m_timeBase_Exist;
}
void Mp7JrsMpeg7Type::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SettimeBase().");
	}
	m_timeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeBase) m_timeBase->SetParent(m_myself.getPointer());
	m_timeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidatetimeBase()
{
	m_timeBase_Exist = false;
}
Mp7JrsdurationPtr Mp7JrsMpeg7Type::GettimeUnit() const
{
	return m_timeUnit;
}

bool Mp7JrsMpeg7Type::ExisttimeUnit() const
{
	return m_timeUnit_Exist;
}
void Mp7JrsMpeg7Type::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SettimeUnit().");
	}
	m_timeUnit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeUnit) m_timeUnit->SetParent(m_myself.getPointer());
	m_timeUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidatetimeUnit()
{
	m_timeUnit_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsMpeg7Type::GetmediaTimeBase() const
{
	return m_mediaTimeBase;
}

bool Mp7JrsMpeg7Type::ExistmediaTimeBase() const
{
	return m_mediaTimeBase_Exist;
}
void Mp7JrsMpeg7Type::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SetmediaTimeBase().");
	}
	m_mediaTimeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeBase) m_mediaTimeBase->SetParent(m_myself.getPointer());
	m_mediaTimeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidatemediaTimeBase()
{
	m_mediaTimeBase_Exist = false;
}
Mp7JrsmediaDurationPtr Mp7JrsMpeg7Type::GetmediaTimeUnit() const
{
	return m_mediaTimeUnit;
}

bool Mp7JrsMpeg7Type::ExistmediaTimeUnit() const
{
	return m_mediaTimeUnit_Exist;
}
void Mp7JrsMpeg7Type::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SetmediaTimeUnit().");
	}
	m_mediaTimeUnit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeUnit) m_mediaTimeUnit->SetParent(m_myself.getPointer());
	m_mediaTimeUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidatemediaTimeUnit()
{
	m_mediaTimeUnit_Exist = false;
}
Mp7JrsDescriptionProfilePtr Mp7JrsMpeg7Type::GetDescriptionProfile() const
{
		return m_DescriptionProfile;
}

// Element is optional
bool Mp7JrsMpeg7Type::IsValidDescriptionProfile() const
{
	return m_DescriptionProfile_Exist;
}

Mp7JrsDescriptionMetadataPtr Mp7JrsMpeg7Type::GetDescriptionMetadata() const
{
		return m_DescriptionMetadata;
}

// Element is optional
bool Mp7JrsMpeg7Type::IsValidDescriptionMetadata() const
{
	return m_DescriptionMetadata_Exist;
}

void Mp7JrsMpeg7Type::SetDescriptionProfile(const Mp7JrsDescriptionProfilePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SetDescriptionProfile().");
	}
	if (m_DescriptionProfile != item || m_DescriptionProfile_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DescriptionProfile);
		m_DescriptionProfile = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionProfile) m_DescriptionProfile->SetParent(m_myself.getPointer());
		if (m_DescriptionProfile && m_DescriptionProfile->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionProfileType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DescriptionProfile->UseTypeAttribute = true;
		}
		m_DescriptionProfile_Exist = true;
		if(m_DescriptionProfile != Mp7JrsDescriptionProfilePtr())
		{
			m_DescriptionProfile->SetContentName(XMLString::transcode("DescriptionProfile"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidateDescriptionProfile()
{
	m_DescriptionProfile_Exist = false;
}
void Mp7JrsMpeg7Type::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7Type::SetDescriptionMetadata().");
	}
	if (m_DescriptionMetadata != item || m_DescriptionMetadata_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DescriptionMetadata);
		m_DescriptionMetadata = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionMetadata) m_DescriptionMetadata->SetParent(m_myself.getPointer());
		if (m_DescriptionMetadata && m_DescriptionMetadata->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DescriptionMetadata->UseTypeAttribute = true;
		}
		m_DescriptionMetadata_Exist = true;
		if(m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
		{
			m_DescriptionMetadata->SetContentName(XMLString::transcode("DescriptionMetadata"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7Type::InvalidateDescriptionMetadata()
{
	m_DescriptionMetadata_Exist = false;
}

Dc1NodeEnum Mp7JrsMpeg7Type::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeBase")) == 0)
	{
		// timeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeUnit")) == 0)
	{
		// timeUnit is simple attribute durationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsdurationPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeBase")) == 0)
	{
		// mediaTimeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeUnit")) == 0)
	{
		// mediaTimeUnit is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DescriptionProfile")) == 0)
	{
		// DescriptionProfile is simple element DescriptionProfileType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptionProfile()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionProfileType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptionProfilePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptionProfile(p, client);
					if((p = GetDescriptionProfile()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DescriptionMetadata")) == 0)
	{
		// DescriptionMetadata is simple element DescriptionMetadataType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptionMetadata()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptionMetadataType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptionMetadataPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptionMetadata(p, client);
					if((p = GetDescriptionMetadata()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Mpeg7Type
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Mpeg7Type");
	}
	return result;
}

XMLCh * Mp7JrsMpeg7Type::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMpeg7Type::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMpeg7Type::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Mpeg7Type"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeBase_Exist)
	{
	// Pattern
	if (m_timeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_timeBase->ToText();
		element->setAttributeNS(X(""), X("timeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeUnit_Exist)
	{
	// Pattern
	if (m_timeUnit != Mp7JrsdurationPtr())
	{
		XMLCh * tmp = m_timeUnit->ToText();
		element->setAttributeNS(X(""), X("timeUnit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeBase_Exist)
	{
	// Pattern
	if (m_mediaTimeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_mediaTimeBase->ToText();
		element->setAttributeNS(X(""), X("mediaTimeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeUnit_Exist)
	{
	// Pattern
	if (m_mediaTimeUnit != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_mediaTimeUnit->ToText();
		element->setAttributeNS(X(""), X("mediaTimeUnit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_DescriptionProfile != Mp7JrsDescriptionProfilePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionProfile->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionProfile"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DescriptionProfile->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DescriptionMetadata != Mp7JrsDescriptionMetadataPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionMetadata->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionMetadata"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DescriptionMetadata->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMpeg7Type::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeBase")));
		this->SettimeBase(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeUnit")))
	{
		Mp7JrsdurationPtr tmp = CreatedurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeUnit")));
		this->SettimeUnit(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeBase")));
		this->SetmediaTimeBase(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeUnit")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeUnit")));
		this->SetmediaTimeUnit(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionProfile"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionProfile")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDescriptionProfileType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionProfile(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionMetadata"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionMetadata")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDescriptionMetadataType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionMetadata(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMpeg7Type::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("DescriptionProfile")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDescriptionProfileType; // FTT, check this
	}
	this->SetDescriptionProfile(child);
  }
  if (XMLString::compareString(elementname, X("DescriptionMetadata")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDescriptionMetadataType; // FTT, check this
	}
	this->SetDescriptionMetadata(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMpeg7Type::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptionProfile() != Dc1NodePtr())
		result.Insert(GetDescriptionProfile());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMpeg7Type_ExtMethodImpl.h


