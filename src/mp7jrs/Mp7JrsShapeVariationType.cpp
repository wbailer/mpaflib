
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsShapeVariationType_StaticShapeVariation_LocalType.h"
#include "Mp7JrsShapeVariationType_DynamicShapeVariation_LocalType.h"
#include "Mp7JrsShapeVariationType_StatisticalVariation_LocalType.h"
#include "Mp7JrsShapeVariationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsShapeVariationType::IMp7JrsShapeVariationType()
{

// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtPropInit.h

}

IMp7JrsShapeVariationType::~IMp7JrsShapeVariationType()
{
// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtPropCleanup.h

}

Mp7JrsShapeVariationType::Mp7JrsShapeVariationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsShapeVariationType::~Mp7JrsShapeVariationType()
{
	Cleanup();
}

void Mp7JrsShapeVariationType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_StaticShapeVariation = Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr(); // Class
	m_DynamicShapeVariation = Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr(); // Class
	m_StatisticalVariation = Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtMyPropInit.h

}

void Mp7JrsShapeVariationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_StaticShapeVariation);
	// Dc1Factory::DeleteObject(m_DynamicShapeVariation);
	// Dc1Factory::DeleteObject(m_StatisticalVariation);
}

void Mp7JrsShapeVariationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ShapeVariationTypePtr, since we
	// might need GetBase(), which isn't defined in IShapeVariationType
	const Dc1Ptr< Mp7JrsShapeVariationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsShapeVariationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_StaticShapeVariation);
		this->SetStaticShapeVariation(Dc1Factory::CloneObject(tmp->GetStaticShapeVariation()));
		// Dc1Factory::DeleteObject(m_DynamicShapeVariation);
		this->SetDynamicShapeVariation(Dc1Factory::CloneObject(tmp->GetDynamicShapeVariation()));
		// Dc1Factory::DeleteObject(m_StatisticalVariation);
		this->SetStatisticalVariation(Dc1Factory::CloneObject(tmp->GetStatisticalVariation()));
}

Dc1NodePtr Mp7JrsShapeVariationType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsShapeVariationType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsShapeVariationType::GetBase() const
{
	return m_Base;
}

Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr Mp7JrsShapeVariationType::GetStaticShapeVariation() const
{
		return m_StaticShapeVariation;
}

Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr Mp7JrsShapeVariationType::GetDynamicShapeVariation() const
{
		return m_DynamicShapeVariation;
}

Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr Mp7JrsShapeVariationType::GetStatisticalVariation() const
{
		return m_StatisticalVariation;
}

void Mp7JrsShapeVariationType::SetStaticShapeVariation(const Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShapeVariationType::SetStaticShapeVariation().");
	}
	if (m_StaticShapeVariation != item)
	{
		// Dc1Factory::DeleteObject(m_StaticShapeVariation);
		m_StaticShapeVariation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StaticShapeVariation) m_StaticShapeVariation->SetParent(m_myself.getPointer());
		if (m_StaticShapeVariation && m_StaticShapeVariation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StaticShapeVariation_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StaticShapeVariation->UseTypeAttribute = true;
		}
		if(m_StaticShapeVariation != Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr())
		{
			m_StaticShapeVariation->SetContentName(XMLString::transcode("StaticShapeVariation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShapeVariationType::SetDynamicShapeVariation(const Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShapeVariationType::SetDynamicShapeVariation().");
	}
	if (m_DynamicShapeVariation != item)
	{
		// Dc1Factory::DeleteObject(m_DynamicShapeVariation);
		m_DynamicShapeVariation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DynamicShapeVariation) m_DynamicShapeVariation->SetParent(m_myself.getPointer());
		if (m_DynamicShapeVariation && m_DynamicShapeVariation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_DynamicShapeVariation_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DynamicShapeVariation->UseTypeAttribute = true;
		}
		if(m_DynamicShapeVariation != Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr())
		{
			m_DynamicShapeVariation->SetContentName(XMLString::transcode("DynamicShapeVariation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShapeVariationType::SetStatisticalVariation(const Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShapeVariationType::SetStatisticalVariation().");
	}
	if (m_StatisticalVariation != item)
	{
		// Dc1Factory::DeleteObject(m_StatisticalVariation);
		m_StatisticalVariation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StatisticalVariation) m_StatisticalVariation->SetParent(m_myself.getPointer());
		if (m_StatisticalVariation && m_StatisticalVariation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StatisticalVariation_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StatisticalVariation->UseTypeAttribute = true;
		}
		if(m_StatisticalVariation != Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr())
		{
			m_StatisticalVariation->SetContentName(XMLString::transcode("StatisticalVariation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsShapeVariationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("StaticShapeVariation")) == 0)
	{
		// StaticShapeVariation is simple element ShapeVariationType_StaticShapeVariation_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStaticShapeVariation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StaticShapeVariation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStaticShapeVariation(p, client);
					if((p = GetStaticShapeVariation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DynamicShapeVariation")) == 0)
	{
		// DynamicShapeVariation is simple element ShapeVariationType_DynamicShapeVariation_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDynamicShapeVariation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_DynamicShapeVariation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDynamicShapeVariation(p, client);
					if((p = GetDynamicShapeVariation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StatisticalVariation")) == 0)
	{
		// StatisticalVariation is simple element ShapeVariationType_StatisticalVariation_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStatisticalVariation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType_StatisticalVariation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStatisticalVariation(p, client);
					if((p = GetStatisticalVariation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ShapeVariationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ShapeVariationType");
	}
	return result;
}

XMLCh * Mp7JrsShapeVariationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsShapeVariationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsShapeVariationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ShapeVariationType"));
	// Element serialization:
	// Class
	
	if (m_StaticShapeVariation != Mp7JrsShapeVariationType_StaticShapeVariation_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StaticShapeVariation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StaticShapeVariation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StaticShapeVariation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DynamicShapeVariation != Mp7JrsShapeVariationType_DynamicShapeVariation_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DynamicShapeVariation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DynamicShapeVariation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DynamicShapeVariation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StatisticalVariation != Mp7JrsShapeVariationType_StatisticalVariation_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StatisticalVariation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StatisticalVariation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StatisticalVariation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsShapeVariationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StaticShapeVariation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StaticShapeVariation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShapeVariationType_StaticShapeVariation_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStaticShapeVariation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DynamicShapeVariation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DynamicShapeVariation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShapeVariationType_DynamicShapeVariation_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDynamicShapeVariation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StatisticalVariation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StatisticalVariation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShapeVariationType_StatisticalVariation_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStatisticalVariation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsShapeVariationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("StaticShapeVariation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShapeVariationType_StaticShapeVariation_LocalType; // FTT, check this
	}
	this->SetStaticShapeVariation(child);
  }
  if (XMLString::compareString(elementname, X("DynamicShapeVariation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShapeVariationType_DynamicShapeVariation_LocalType; // FTT, check this
	}
	this->SetDynamicShapeVariation(child);
  }
  if (XMLString::compareString(elementname, X("StatisticalVariation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShapeVariationType_StatisticalVariation_LocalType; // FTT, check this
	}
	this->SetStatisticalVariation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsShapeVariationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStaticShapeVariation() != Dc1NodePtr())
		result.Insert(GetStaticShapeVariation());
	if (GetDynamicShapeVariation() != Dc1NodePtr())
		result.Insert(GetDynamicShapeVariation());
	if (GetStatisticalVariation() != Dc1NodePtr())
		result.Insert(GetStatisticalVariation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsShapeVariationType_ExtMethodImpl.h


