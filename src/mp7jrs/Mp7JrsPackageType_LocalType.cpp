
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtImplInclude.h


#include "Mp7JrsPackageType_Scheme_LocalType.h"
#include "Mp7JrsPackageType.h"
#include "Mp7JrsPackageType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsPackageType_LocalType::IMp7JrsPackageType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtPropInit.h

}

IMp7JrsPackageType_LocalType::~IMp7JrsPackageType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtPropCleanup.h

}

Mp7JrsPackageType_LocalType::Mp7JrsPackageType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPackageType_LocalType::~Mp7JrsPackageType_LocalType()
{
	Cleanup();
}

void Mp7JrsPackageType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Scheme = Mp7JrsPackageType_Scheme_LocalPtr(); // Class
	m_Package = Mp7JrsPackagePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtMyPropInit.h

}

void Mp7JrsPackageType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Scheme);
	// Dc1Factory::DeleteObject(m_Package);
}

void Mp7JrsPackageType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PackageType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPackageType_LocalType
	const Dc1Ptr< Mp7JrsPackageType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Scheme);
		this->SetScheme(Dc1Factory::CloneObject(tmp->GetScheme()));
		// Dc1Factory::DeleteObject(m_Package);
		this->SetPackage(Dc1Factory::CloneObject(tmp->GetPackage()));
}

Dc1NodePtr Mp7JrsPackageType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPackageType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsPackageType_Scheme_LocalPtr Mp7JrsPackageType_LocalType::GetScheme() const
{
		return m_Scheme;
}

Mp7JrsPackagePtr Mp7JrsPackageType_LocalType::GetPackage() const
{
		return m_Package;
}

// implementing setter for choice 
/* element */
void Mp7JrsPackageType_LocalType::SetScheme(const Mp7JrsPackageType_Scheme_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPackageType_LocalType::SetScheme().");
	}
	if (m_Scheme != item)
	{
		// Dc1Factory::DeleteObject(m_Scheme);
		m_Scheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Scheme) m_Scheme->SetParent(m_myself.getPointer());
		if (m_Scheme && m_Scheme->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType_Scheme_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Scheme->UseTypeAttribute = true;
		}
		if(m_Scheme != Mp7JrsPackageType_Scheme_LocalPtr())
		{
			m_Scheme->SetContentName(XMLString::transcode("Scheme"));
		}
	// Dc1Factory::DeleteObject(m_Package);
	m_Package = Mp7JrsPackagePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPackageType_LocalType::SetPackage(const Mp7JrsPackagePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPackageType_LocalType::SetPackage().");
	}
	if (m_Package != item)
	{
		// Dc1Factory::DeleteObject(m_Package);
		m_Package = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Package) m_Package->SetParent(m_myself.getPointer());
		if (m_Package && m_Package->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Package->UseTypeAttribute = true;
		}
		if(m_Package != Mp7JrsPackagePtr())
		{
			m_Package->SetContentName(XMLString::transcode("Package"));
		}
	// Dc1Factory::DeleteObject(m_Scheme);
	m_Scheme = Mp7JrsPackageType_Scheme_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: PackageType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsPackageType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsPackageType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPackageType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPackageType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Scheme != Mp7JrsPackageType_Scheme_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Scheme->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Scheme"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Scheme->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Package != Mp7JrsPackagePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Package->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Package"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Package->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPackageType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Scheme"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Scheme")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePackageType_Scheme_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetScheme(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Package"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Package")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePackageType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPackage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPackageType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Scheme")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePackageType_Scheme_LocalType; // FTT, check this
	}
	this->SetScheme(child);
  }
  if (XMLString::compareString(elementname, X("Package")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePackageType; // FTT, check this
	}
	this->SetPackage(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPackageType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetScheme() != Dc1NodePtr())
		result.Insert(GetScheme());
	if (GetPackage() != Dc1NodePtr())
		result.Insert(GetPackage());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPackageType_LocalType_ExtMethodImpl.h


