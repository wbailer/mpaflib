
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned16.h"
#include "Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionType.h"
#include "Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType.h"
#include "Mp7JrsSpokenContentLatticeType_Block_Node_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:WordLink
#include "Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PhoneLink

#include <assert.h>
IMp7JrsSpokenContentLatticeType_Block_Node_LocalType::IMp7JrsSpokenContentLatticeType_Block_Node_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtPropInit.h

}

IMp7JrsSpokenContentLatticeType_Block_Node_LocalType::~IMp7JrsSpokenContentLatticeType_Block_Node_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtPropCleanup.h

}

Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Mp7JrsSpokenContentLatticeType_Block_Node_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::~Mp7JrsSpokenContentLatticeType_Block_Node_LocalType()
{
	Cleanup();
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Init()
{

	// Init attributes
	m_num = Createunsigned16; // Create a valid class, FTT, check this
	m_num->SetContent(0); // Use Value initialiser (xxx2)
	m_timeOffset = Createunsigned16; // Create a valid class, FTT, check this
	m_timeOffset->SetContent(0); // Use Value initialiser (xxx2)
	m_speakerInfoRef = NULL; // String
	m_speakerInfoRef_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_WordLink = Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr(); // Collection
	m_PhoneLink = Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_num);
	// Dc1Factory::DeleteObject(m_timeOffset);
	XMLString::release(&m_speakerInfoRef); // String
	// Dc1Factory::DeleteObject(m_WordLink);
	// Dc1Factory::DeleteObject(m_PhoneLink);
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpokenContentLatticeType_Block_Node_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpokenContentLatticeType_Block_Node_LocalType
	const Dc1Ptr< Mp7JrsSpokenContentLatticeType_Block_Node_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_num);
		this->Setnum(Dc1Factory::CloneObject(tmp->Getnum()));
	}
	{
	// Dc1Factory::DeleteObject(m_timeOffset);
		this->SettimeOffset(Dc1Factory::CloneObject(tmp->GettimeOffset()));
	}
	{
	XMLString::release(&m_speakerInfoRef); // String
	if (tmp->ExistspeakerInfoRef())
	{
		this->SetspeakerInfoRef(XMLString::replicate(tmp->GetspeakerInfoRef()));
	}
	else
	{
		InvalidatespeakerInfoRef();
	}
	}
		// Dc1Factory::DeleteObject(m_WordLink);
		this->SetWordLink(Dc1Factory::CloneObject(tmp->GetWordLink()));
		// Dc1Factory::DeleteObject(m_PhoneLink);
		this->SetPhoneLink(Dc1Factory::CloneObject(tmp->GetPhoneLink()));
}

Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned16Ptr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Getnum() const
{
	return m_num;
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Setnum(const Mp7Jrsunsigned16Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Setnum().");
	}
	m_num = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_num) m_num->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7Jrsunsigned16Ptr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GettimeOffset() const
{
	return m_timeOffset;
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SettimeOffset(const Mp7Jrsunsigned16Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SettimeOffset().");
	}
	m_timeOffset = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeOffset) m_timeOffset->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetspeakerInfoRef() const
{
	return m_speakerInfoRef;
}

bool Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::ExistspeakerInfoRef() const
{
	return m_speakerInfoRef_Exist;
}
void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetspeakerInfoRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetspeakerInfoRef().");
	}
	m_speakerInfoRef = item;
	m_speakerInfoRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::InvalidatespeakerInfoRef()
{
	m_speakerInfoRef_Exist = false;
}
Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetWordLink() const
{
		return m_WordLink;
}

Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetPhoneLink() const
{
		return m_PhoneLink;
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetWordLink(const Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetWordLink().");
	}
	if (m_WordLink != item)
	{
		// Dc1Factory::DeleteObject(m_WordLink);
		m_WordLink = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WordLink) m_WordLink->SetParent(m_myself.getPointer());
		if(m_WordLink != Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr())
		{
			m_WordLink->SetContentName(XMLString::transcode("WordLink"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetPhoneLink(const Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::SetPhoneLink().");
	}
	if (m_PhoneLink != item)
	{
		// Dc1Factory::DeleteObject(m_PhoneLink);
		m_PhoneLink = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PhoneLink) m_PhoneLink->SetParent(m_myself.getPointer());
		if(m_PhoneLink != Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr())
		{
			m_PhoneLink->SetContentName(XMLString::transcode("PhoneLink"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("num")) == 0)
	{
		// num is simple attribute unsigned16
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned16Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeOffset")) == 0)
	{
		// timeOffset is simple attribute unsigned16
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned16Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("speakerInfoRef")) == 0)
	{
		// speakerInfoRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("WordLink")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:WordLink is item of type SpokenContentLatticeType_Block_Node_WordLink_LocalType
		// in element collection SpokenContentLatticeType_Block_Node_WordLink_CollectionType
		
		context->Found = true;
		Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr coll = GetWordLink();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentLatticeType_Block_Node_WordLink_CollectionType; // FTT, check this
				SetWordLink(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 127))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_WordLink_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PhoneLink")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PhoneLink is item of type SpokenContentLatticeType_Block_Node_PhoneLink_LocalType
		// in element collection SpokenContentLatticeType_Block_Node_PhoneLink_CollectionType
		
		context->Found = true;
		Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr coll = GetPhoneLink();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType; // FTT, check this
				SetPhoneLink(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 127))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_PhoneLink_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpokenContentLatticeType_Block_Node_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpokenContentLatticeType_Block_Node_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpokenContentLatticeType_Block_Node_LocalType"));
	// Attribute Serialization:
	// Class
	if (m_num != Mp7Jrsunsigned16Ptr())
	{
		XMLCh * tmp = m_num->ToText();
		element->setAttributeNS(X(""), X("num"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Class
	if (m_timeOffset != Mp7Jrsunsigned16Ptr())
	{
		XMLCh * tmp = m_timeOffset->ToText();
		element->setAttributeNS(X(""), X("timeOffset"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_speakerInfoRef_Exist)
	{
	// String
	if(m_speakerInfoRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("speakerInfoRef"), m_speakerInfoRef);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_WordLink != Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_WordLink->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PhoneLink != Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PhoneLink->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("num")))
	{
		// Deserialize class type
		Mp7Jrsunsigned16Ptr tmp = Createunsigned16; // FTT, check this
		tmp->Parse(parent->getAttribute(X("num")));
		this->Setnum(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeOffset")))
	{
		// Deserialize class type
		Mp7Jrsunsigned16Ptr tmp = Createunsigned16; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeOffset")));
		this->SettimeOffset(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("speakerInfoRef")))
	{
		// Deserialize string type
		this->SetspeakerInfoRef(Dc1Convert::TextToString(parent->getAttribute(X("speakerInfoRef"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("WordLink"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("WordLink")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_WordLink_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWordLink(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PhoneLink"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PhoneLink")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPhoneLink(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("WordLink")) == 0))
  {
	Mp7JrsSpokenContentLatticeType_Block_Node_WordLink_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_WordLink_CollectionType; // FTT, check this
	this->SetWordLink(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PhoneLink")) == 0))
  {
	Mp7JrsSpokenContentLatticeType_Block_Node_PhoneLink_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_PhoneLink_CollectionType; // FTT, check this
	this->SetPhoneLink(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpokenContentLatticeType_Block_Node_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetWordLink() != Dc1NodePtr())
		result.Insert(GetWordLink());
	if (GetPhoneLink() != Dc1NodePtr())
		result.Insert(GetPhoneLink());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_Node_LocalType_ExtMethodImpl.h


