
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned12.h"
#include "Mp7JrsColorQuantizationType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsColorQuantizationType_LocalType::IMp7JrsColorQuantizationType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtPropInit.h

}

IMp7JrsColorQuantizationType_LocalType::~IMp7JrsColorQuantizationType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtPropCleanup.h

}

Mp7JrsColorQuantizationType_LocalType::Mp7JrsColorQuantizationType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsColorQuantizationType_LocalType::~Mp7JrsColorQuantizationType_LocalType()
{
	Cleanup();
}

void Mp7JrsColorQuantizationType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Component = Mp7JrsColorQuantizationType_Component_LocalType::UninitializedEnumeration; // Enumeration
	m_NumOfBins = Mp7Jrsunsigned12Ptr(); // Class with content 


// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtMyPropInit.h

}

void Mp7JrsColorQuantizationType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_NumOfBins);
}

void Mp7JrsColorQuantizationType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ColorQuantizationType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IColorQuantizationType_LocalType
	const Dc1Ptr< Mp7JrsColorQuantizationType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetComponent(tmp->GetComponent());
		// Dc1Factory::DeleteObject(m_NumOfBins);
		this->SetNumOfBins(Dc1Factory::CloneObject(tmp->GetNumOfBins()));
}

Dc1NodePtr Mp7JrsColorQuantizationType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsColorQuantizationType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsColorQuantizationType_Component_LocalType::Enumeration Mp7JrsColorQuantizationType_LocalType::GetComponent() const
{
		return m_Component;
}

Mp7Jrsunsigned12Ptr Mp7JrsColorQuantizationType_LocalType::GetNumOfBins() const
{
		return m_NumOfBins;
}

void Mp7JrsColorQuantizationType_LocalType::SetComponent(Mp7JrsColorQuantizationType_Component_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorQuantizationType_LocalType::SetComponent().");
	}
	if (m_Component != item)
	{
		// nothing to free here, hopefully
		m_Component = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorQuantizationType_LocalType::SetNumOfBins(const Mp7Jrsunsigned12Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorQuantizationType_LocalType::SetNumOfBins().");
	}
	if (m_NumOfBins != item)
	{
		// Dc1Factory::DeleteObject(m_NumOfBins);
		m_NumOfBins = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NumOfBins) m_NumOfBins->SetParent(m_myself.getPointer());
		if (m_NumOfBins && m_NumOfBins->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_NumOfBins->UseTypeAttribute = true;
		}
		if(m_NumOfBins != Mp7Jrsunsigned12Ptr())
		{
			m_NumOfBins->SetContentName(XMLString::transcode("NumOfBins"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: ColorQuantizationType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsColorQuantizationType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsColorQuantizationType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsColorQuantizationType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsColorQuantizationType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ColorQuantizationType_LocalType"));
	// Element serialization:
	if(m_Component != Mp7JrsColorQuantizationType_Component_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsColorQuantizationType_Component_LocalType::ToText(m_Component);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Component"), false);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_NumOfBins != Mp7Jrsunsigned12Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_NumOfBins->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("NumOfBins"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_NumOfBins->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsColorQuantizationType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Component"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Component")) == 0))
		{
			Mp7JrsColorQuantizationType_Component_LocalType::Enumeration tmp = Mp7JrsColorQuantizationType_Component_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsColorQuantizationType_Component_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetComponent(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("NumOfBins"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("NumOfBins")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned12; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetNumOfBins(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsColorQuantizationType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("NumOfBins")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned12; // FTT, check this
	}
	this->SetNumOfBins(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsColorQuantizationType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetNumOfBins() != Dc1NodePtr())
		result.Insert(GetNumOfBins());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_LocalType_ExtMethodImpl.h


