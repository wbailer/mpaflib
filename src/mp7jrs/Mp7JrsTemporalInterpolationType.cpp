
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtImplInclude.h


#include "Mp7JrsTemporalInterpolationType_WholeInterval_LocalType.h"
#include "Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType.h"
#include "Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionType.h"
#include "Mp7JrsTemporalInterpolationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTemporalInterpolationType_InterpolationFunctions_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:InterpolationFunctions

#include <assert.h>
IMp7JrsTemporalInterpolationType::IMp7JrsTemporalInterpolationType()
{

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtPropInit.h

}

IMp7JrsTemporalInterpolationType::~IMp7JrsTemporalInterpolationType()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtPropCleanup.h

}

Mp7JrsTemporalInterpolationType::Mp7JrsTemporalInterpolationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTemporalInterpolationType::~Mp7JrsTemporalInterpolationType()
{
	Cleanup();
}

void Mp7JrsTemporalInterpolationType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_WholeInterval = Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr(); // Class
	m_KeyTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr(); // Class
	m_InterpolationFunctions = Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtMyPropInit.h

}

void Mp7JrsTemporalInterpolationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_WholeInterval);
	// Dc1Factory::DeleteObject(m_KeyTimePoint);
	// Dc1Factory::DeleteObject(m_InterpolationFunctions);
}

void Mp7JrsTemporalInterpolationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TemporalInterpolationTypePtr, since we
	// might need GetBase(), which isn't defined in ITemporalInterpolationType
	const Dc1Ptr< Mp7JrsTemporalInterpolationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_WholeInterval);
		this->SetWholeInterval(Dc1Factory::CloneObject(tmp->GetWholeInterval()));
		// Dc1Factory::DeleteObject(m_KeyTimePoint);
		this->SetKeyTimePoint(Dc1Factory::CloneObject(tmp->GetKeyTimePoint()));
		// Dc1Factory::DeleteObject(m_InterpolationFunctions);
		this->SetInterpolationFunctions(Dc1Factory::CloneObject(tmp->GetInterpolationFunctions()));
}

Dc1NodePtr Mp7JrsTemporalInterpolationType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTemporalInterpolationType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr Mp7JrsTemporalInterpolationType::GetWholeInterval() const
{
		return m_WholeInterval;
}

Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr Mp7JrsTemporalInterpolationType::GetKeyTimePoint() const
{
		return m_KeyTimePoint;
}

Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr Mp7JrsTemporalInterpolationType::GetInterpolationFunctions() const
{
		return m_InterpolationFunctions;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsTemporalInterpolationType::SetWholeInterval(const Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType::SetWholeInterval().");
	}
	if (m_WholeInterval != item)
	{
		// Dc1Factory::DeleteObject(m_WholeInterval);
		m_WholeInterval = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WholeInterval) m_WholeInterval->SetParent(m_myself.getPointer());
		if (m_WholeInterval && m_WholeInterval->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_WholeInterval_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_WholeInterval->UseTypeAttribute = true;
		}
		if(m_WholeInterval != Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr())
		{
			m_WholeInterval->SetContentName(XMLString::transcode("WholeInterval"));
		}
	// Dc1Factory::DeleteObject(m_KeyTimePoint);
	m_KeyTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTemporalInterpolationType::SetKeyTimePoint(const Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType::SetKeyTimePoint().");
	}
	if (m_KeyTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_KeyTimePoint);
		m_KeyTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_KeyTimePoint) m_KeyTimePoint->SetParent(m_myself.getPointer());
		if (m_KeyTimePoint && m_KeyTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_KeyTimePoint->UseTypeAttribute = true;
		}
		if(m_KeyTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr())
		{
			m_KeyTimePoint->SetContentName(XMLString::transcode("KeyTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_WholeInterval);
	m_WholeInterval = Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTemporalInterpolationType::SetInterpolationFunctions(const Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType::SetInterpolationFunctions().");
	}
	if (m_InterpolationFunctions != item)
	{
		// Dc1Factory::DeleteObject(m_InterpolationFunctions);
		m_InterpolationFunctions = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InterpolationFunctions) m_InterpolationFunctions->SetParent(m_myself.getPointer());
		if(m_InterpolationFunctions != Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr())
		{
			m_InterpolationFunctions->SetContentName(XMLString::transcode("InterpolationFunctions"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTemporalInterpolationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("WholeInterval")) == 0)
	{
		// WholeInterval is simple element TemporalInterpolationType_WholeInterval_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWholeInterval()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_WholeInterval_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWholeInterval(p, client);
					if((p = GetWholeInterval()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeyTimePoint")) == 0)
	{
		// KeyTimePoint is simple element TemporalInterpolationType_KeyTimePoint_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetKeyTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_KeyTimePoint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetKeyTimePoint(p, client);
					if((p = GetKeyTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("InterpolationFunctions")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:InterpolationFunctions is item of type TemporalInterpolationType_InterpolationFunctions_LocalType
		// in element collection TemporalInterpolationType_InterpolationFunctions_CollectionType
		
		context->Found = true;
		Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr coll = GetInterpolationFunctions();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTemporalInterpolationType_InterpolationFunctions_CollectionType; // FTT, check this
				SetInterpolationFunctions(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 15))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalInterpolationType_InterpolationFunctions_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalInterpolationType_InterpolationFunctions_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TemporalInterpolationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TemporalInterpolationType");
	}
	return result;
}

XMLCh * Mp7JrsTemporalInterpolationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTemporalInterpolationType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTemporalInterpolationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TemporalInterpolationType"));
	// Element serialization:
	// Class
	
	if (m_WholeInterval != Mp7JrsTemporalInterpolationType_WholeInterval_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_WholeInterval->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("WholeInterval"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_WholeInterval->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_KeyTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_KeyTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("KeyTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_KeyTimePoint->Serialize(doc, element, &elem);
	}
	if (m_InterpolationFunctions != Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_InterpolationFunctions->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsTemporalInterpolationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("WholeInterval"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("WholeInterval")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalInterpolationType_WholeInterval_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetWholeInterval(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("KeyTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("KeyTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalInterpolationType_KeyTimePoint_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetKeyTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("InterpolationFunctions"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("InterpolationFunctions")) == 0))
		{
			// Deserialize factory type
			Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr tmp = CreateTemporalInterpolationType_InterpolationFunctions_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInterpolationFunctions(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTemporalInterpolationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("WholeInterval")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalInterpolationType_WholeInterval_LocalType; // FTT, check this
	}
	this->SetWholeInterval(child);
  }
  if (XMLString::compareString(elementname, X("KeyTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalInterpolationType_KeyTimePoint_LocalType; // FTT, check this
	}
	this->SetKeyTimePoint(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("InterpolationFunctions")) == 0))
  {
	Mp7JrsTemporalInterpolationType_InterpolationFunctions_CollectionPtr tmp = CreateTemporalInterpolationType_InterpolationFunctions_CollectionType; // FTT, check this
	this->SetInterpolationFunctions(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTemporalInterpolationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInterpolationFunctions() != Dc1NodePtr())
		result.Insert(GetInterpolationFunctions());
	if (GetWholeInterval() != Dc1NodePtr())
		result.Insert(GetWholeInterval());
	if (GetKeyTimePoint() != Dc1NodePtr())
		result.Insert(GetKeyTimePoint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_ExtMethodImpl.h


