
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtImplInclude.h


#include "Mp7JrsAudioDSType.h"
#include "Mp7JrsDistributionPerceptualAttributeType.h"
#include "Mp7JrsClassificationPerceptualAttributeType.h"
#include "Mp7JrsLinearPerceptualAttributeType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsPerceptualAttributeDSType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsPerceptualAttributeDSType::IMp7JrsPerceptualAttributeDSType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtPropInit.h

}

IMp7JrsPerceptualAttributeDSType::~IMp7JrsPerceptualAttributeDSType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtPropCleanup.h

}

Mp7JrsPerceptualAttributeDSType::Mp7JrsPerceptualAttributeDSType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptualAttributeDSType::~Mp7JrsPerceptualAttributeDSType()
{
	Cleanup();
}

void Mp7JrsPerceptualAttributeDSType::Init()
{
	// Init base
	m_Base = CreateAudioDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Distribution = Mp7JrsDistributionPerceptualAttributePtr(); // Class
	m_Distribution_Exist = false;
	m_Classification = Mp7JrsClassificationPerceptualAttributePtr(); // Class
	m_Classification_Exist = false;
	m_LinearPerception = Mp7JrsLinearPerceptualAttributePtr(); // Class
	m_LinearPerception_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtMyPropInit.h

}

void Mp7JrsPerceptualAttributeDSType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Distribution);
	// Dc1Factory::DeleteObject(m_Classification);
	// Dc1Factory::DeleteObject(m_LinearPerception);
}

void Mp7JrsPerceptualAttributeDSType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PerceptualAttributeDSTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptualAttributeDSType
	const Dc1Ptr< Mp7JrsPerceptualAttributeDSType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPerceptualAttributeDSType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidDistribution())
	{
		// Dc1Factory::DeleteObject(m_Distribution);
		this->SetDistribution(Dc1Factory::CloneObject(tmp->GetDistribution()));
	}
	else
	{
		InvalidateDistribution();
	}
	if (tmp->IsValidClassification())
	{
		// Dc1Factory::DeleteObject(m_Classification);
		this->SetClassification(Dc1Factory::CloneObject(tmp->GetClassification()));
	}
	else
	{
		InvalidateClassification();
	}
	if (tmp->IsValidLinearPerception())
	{
		// Dc1Factory::DeleteObject(m_LinearPerception);
		this->SetLinearPerception(Dc1Factory::CloneObject(tmp->GetLinearPerception()));
	}
	else
	{
		InvalidateLinearPerception();
	}
}

Dc1NodePtr Mp7JrsPerceptualAttributeDSType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPerceptualAttributeDSType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDSType > Mp7JrsPerceptualAttributeDSType::GetBase() const
{
	return m_Base;
}

Mp7JrsDistributionPerceptualAttributePtr Mp7JrsPerceptualAttributeDSType::GetDistribution() const
{
		return m_Distribution;
}

// Element is optional
bool Mp7JrsPerceptualAttributeDSType::IsValidDistribution() const
{
	return m_Distribution_Exist;
}

Mp7JrsClassificationPerceptualAttributePtr Mp7JrsPerceptualAttributeDSType::GetClassification() const
{
		return m_Classification;
}

// Element is optional
bool Mp7JrsPerceptualAttributeDSType::IsValidClassification() const
{
	return m_Classification_Exist;
}

Mp7JrsLinearPerceptualAttributePtr Mp7JrsPerceptualAttributeDSType::GetLinearPerception() const
{
		return m_LinearPerception;
}

// Element is optional
bool Mp7JrsPerceptualAttributeDSType::IsValidLinearPerception() const
{
	return m_LinearPerception_Exist;
}

void Mp7JrsPerceptualAttributeDSType::SetDistribution(const Mp7JrsDistributionPerceptualAttributePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetDistribution().");
	}
	if (m_Distribution != item || m_Distribution_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Distribution);
		m_Distribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Distribution) m_Distribution->SetParent(m_myself.getPointer());
		if (m_Distribution && m_Distribution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DistributionPerceptualAttributeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Distribution->UseTypeAttribute = true;
		}
		m_Distribution_Exist = true;
		if(m_Distribution != Mp7JrsDistributionPerceptualAttributePtr())
		{
			m_Distribution->SetContentName(XMLString::transcode("Distribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidateDistribution()
{
	m_Distribution_Exist = false;
}
void Mp7JrsPerceptualAttributeDSType::SetClassification(const Mp7JrsClassificationPerceptualAttributePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetClassification().");
	}
	if (m_Classification != item || m_Classification_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Classification);
		m_Classification = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Classification) m_Classification->SetParent(m_myself.getPointer());
		if (m_Classification && m_Classification->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPerceptualAttributeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Classification->UseTypeAttribute = true;
		}
		m_Classification_Exist = true;
		if(m_Classification != Mp7JrsClassificationPerceptualAttributePtr())
		{
			m_Classification->SetContentName(XMLString::transcode("Classification"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidateClassification()
{
	m_Classification_Exist = false;
}
void Mp7JrsPerceptualAttributeDSType::SetLinearPerception(const Mp7JrsLinearPerceptualAttributePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetLinearPerception().");
	}
	if (m_LinearPerception != item || m_LinearPerception_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_LinearPerception);
		m_LinearPerception = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LinearPerception) m_LinearPerception->SetParent(m_myself.getPointer());
		if (m_LinearPerception && m_LinearPerception->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinearPerceptualAttributeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LinearPerception->UseTypeAttribute = true;
		}
		m_LinearPerception_Exist = true;
		if(m_LinearPerception != Mp7JrsLinearPerceptualAttributePtr())
		{
			m_LinearPerception->SetContentName(XMLString::transcode("LinearPerception"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidateLinearPerception()
{
	m_LinearPerception_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsPerceptualAttributeDSType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsPerceptualAttributeDSType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsPerceptualAttributeDSType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsPerceptualAttributeDSType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsPerceptualAttributeDSType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsPerceptualAttributeDSType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsPerceptualAttributeDSType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsPerceptualAttributeDSType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsPerceptualAttributeDSType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsPerceptualAttributeDSType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsPerceptualAttributeDSType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsPerceptualAttributeDSType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsPerceptualAttributeDSType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsPerceptualAttributeDSType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsPerceptualAttributeDSType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsPerceptualAttributeDSType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsPerceptualAttributeDSType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsPerceptualAttributeDSType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualAttributeDSType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsPerceptualAttributeDSType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsPerceptualAttributeDSType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualAttributeDSType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPerceptualAttributeDSType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Distribution")) == 0)
	{
		// Distribution is simple element DistributionPerceptualAttributeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDistribution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DistributionPerceptualAttributeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDistributionPerceptualAttributePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDistribution(p, client);
					if((p = GetDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Classification")) == 0)
	{
		// Classification is simple element ClassificationPerceptualAttributeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetClassification()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPerceptualAttributeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPerceptualAttributePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetClassification(p, client);
					if((p = GetClassification()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LinearPerception")) == 0)
	{
		// LinearPerception is simple element LinearPerceptualAttributeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLinearPerception()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinearPerceptualAttributeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsLinearPerceptualAttributePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLinearPerception(p, client);
					if((p = GetLinearPerception()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PerceptualAttributeDSType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PerceptualAttributeDSType");
	}
	return result;
}

XMLCh * Mp7JrsPerceptualAttributeDSType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPerceptualAttributeDSType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPerceptualAttributeDSType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualAttributeDSType"));
	// Element serialization:
	// Class
	
	if (m_Distribution != Mp7JrsDistributionPerceptualAttributePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Distribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Distribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Distribution->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Classification != Mp7JrsClassificationPerceptualAttributePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Classification->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Classification"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Classification->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_LinearPerception != Mp7JrsLinearPerceptualAttributePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LinearPerception->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LinearPerception"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LinearPerception->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPerceptualAttributeDSType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Distribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Distribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDistributionPerceptualAttributeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Classification"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Classification")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateClassificationPerceptualAttributeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetClassification(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LinearPerception"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LinearPerception")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateLinearPerceptualAttributeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLinearPerception(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptualAttributeDSType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Distribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDistributionPerceptualAttributeType; // FTT, check this
	}
	this->SetDistribution(child);
  }
  if (XMLString::compareString(elementname, X("Classification")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateClassificationPerceptualAttributeType; // FTT, check this
	}
	this->SetClassification(child);
  }
  if (XMLString::compareString(elementname, X("LinearPerception")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateLinearPerceptualAttributeType; // FTT, check this
	}
	this->SetLinearPerception(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptualAttributeDSType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDistribution() != Dc1NodePtr())
		result.Insert(GetDistribution());
	if (GetClassification() != Dc1NodePtr())
		result.Insert(GetClassification());
	if (GetLinearPerception() != Dc1NodePtr())
		result.Insert(GetLinearPerception());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptualAttributeDSType_ExtMethodImpl.h


