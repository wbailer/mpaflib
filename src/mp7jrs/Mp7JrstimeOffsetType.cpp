
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtImplInclude.h


#include "Mp7JrstimeOffsetType.h"

Mp7JrstimeOffsetType::Mp7JrstimeOffsetType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrstimeOffsetType::~Mp7JrstimeOffsetType()
{
		Cleanup();
}

void Mp7JrstimeOffsetType::Init()
{	   
		m_OffsetSign = 1;
		m_Days = -1;
		m_Hours = -1;
		m_Minutes = -1;
		m_Seconds = -1;
		m_NumberOfFractions = -1;
		m_Time_Exist = false;
		m_FractionsPerSecond = -1;
		m_TimezoneSign = 1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_Timezone_Exist = false;

// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtPropInit.h

}

void Mp7JrstimeOffsetType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtPropCleanup.h


}

void Mp7JrstimeOffsetType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrstimeOffsetType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrstimeOffsetType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrstimeOffsetType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrstimeOffsetType::GetOffsetSign() const
{
		return m_OffsetSign;
}

void Mp7JrstimeOffsetType::SetOffsetSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetOffsetSign().");
		}
		m_OffsetSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetDays() const
{
		return m_Days;
}

void Mp7JrstimeOffsetType::SetDays(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetDays().");
		}
		m_Days = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetHours() const
{
		return m_Hours;
}

void Mp7JrstimeOffsetType::SetHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetHours().");
		}
		m_Hours = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetMinutes() const
{
		return m_Minutes;
}

void Mp7JrstimeOffsetType::SetMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetMinutes().");
		}
		m_Minutes = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetSeconds() const
{
		return m_Seconds;
}

void Mp7JrstimeOffsetType::SetSeconds(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetSeconds().");
		}
		m_Seconds = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetNumberOfFractions() const
{
		return m_NumberOfFractions;
}

void Mp7JrstimeOffsetType::SetNumberOfFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetNumberOfFractions().");
		}
		m_NumberOfFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrstimeOffsetType::GetTime_Exist() const
{
		return m_Time_Exist;
}

void Mp7JrstimeOffsetType::SetTime_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetTime_Exist().");
		}
		m_Time_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetFractionsPerSecond() const
{
		return m_FractionsPerSecond;
}

void Mp7JrstimeOffsetType::SetFractionsPerSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetFractionsPerSecond().");
		}
		m_FractionsPerSecond = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void Mp7JrstimeOffsetType::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void Mp7JrstimeOffsetType::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimeOffsetType::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void Mp7JrstimeOffsetType::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrstimeOffsetType::GetTimezone_Exist() const
{
		return m_Timezone_Exist;
}

void Mp7JrstimeOffsetType::SetTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimeOffsetType::SetTimezone_Exist().");
		}
		m_Timezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrstimeOffsetType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_OffsetSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}

		Dc1Util::CatString(&buf, X("P"));

		if(m_Days >= 0)
		{
			tmp = Dc1Convert::BinToText(m_Days);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X("D"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Time_Exist)
		{
			Dc1Util::CatString(&buf, X("T"));

			if(m_Hours >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Hours);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("H"));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Minutes >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Minutes);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("M"));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Seconds >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Seconds);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("S"));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_NumberOfFractions >= 0)
			{
				tmp = Dc1Convert::BinToText(m_NumberOfFractions);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("N"));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_FractionsPerSecond >= 0)
		{
			tmp = Dc1Convert::BinToText(m_FractionsPerSecond);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X("F"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Timezone_Exist)
		{
			if(m_TimezoneSign > 0)
			{
				Dc1Util::CatString(&buf, X("+"));
			}
			else if(m_TimezoneSign < 0)
			{
				Dc1Util::CatString(&buf, X("-"));
			}

			XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X(":"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrstimeOffsetType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^\\-?P(\\d+D)?(T(\\d+H)?(\\d+M)?(\\d+S)?(\\d+N)?)?(\\d+F)?((\\-|\\+)\\d{2}:\\d{2}Z)?$");
		// This is for Xerces 3.1.1+
		if(r.matches(txt, &m))
		{
				for(int i = 0; i < m.getNoGroups(); i++)
				{
						int i1 = m.getStartPos(i);
						int i2 = Dc1Util::GetNextEndPos(m, i);
						if(i1 == i2) continue; // ignore missing optional or wrong group
						switch(i)
						{
								case 0:
										if(i1 >= 0 && i2 - i1 > 1)
										{
												// \\-?P
												m_OffsetSign = -1;
										}
								break;

								case 1:
										if(i1 >= 0)
										{
												// (\\d+D)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Days = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								case 2:
										if(i1 >= 0)
										{
												// T
												// Nothing to do
										}
								break;

								case 3:
										if(i1 >= 0)
										{
												// (\\d+H)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Hours = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 4:
										if(i1 >= 0)
										{
												// (\\d+M)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Minutes = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 5:
										if(i1 >= 0)
										{
												// (\\d+S)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Seconds = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 6:
										if(i1 >= 0)
										{
												// (\\d+N)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_NumberOfFractions = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 7:
										if(i1 >= 0)
										{
												// (\\d+F)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_FractionsPerSecond = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								case 8:
										if(i1 >= 0)
										{
												// ((\\-|\\+)\\d{2}:\\d{2}Z)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 1);
												m_TimezoneSign = Dc1Convert::TextToSign(substring);

												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i1 + 3);
												m_TimezoneHours = Dc1Convert::TextToUnsignedInt(substring);

												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 4, i2 - 1);
												m_TimezoneMinutes = Dc1Convert::TextToUnsignedInt(substring);

												m_Timezone_Exist = true;
										}
								break;

								default: return true;
								break;

						}
				}
		}
		else return false;
		return true;
}

bool Mp7JrstimeOffsetType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrstimeOffsetType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrstimeOffsetType::ContentToString() const
{
	// Mp7JrstimeOffsetType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrstimeOffsetType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timeOffsetType")))));
		}

// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool Mp7JrstimeOffsetType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file Mp7JrstimeOffsetType_ExtMethodImpl.h








