
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtImplInclude.h


#include "Mp7JrsVideoViewGraphType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsVideoViewGraphType_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsVideoViewGraphType_LocalType0::IMp7JrsVideoViewGraphType_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtPropInit.h

}

IMp7JrsVideoViewGraphType_LocalType0::~IMp7JrsVideoViewGraphType_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtPropCleanup.h

}

Mp7JrsVideoViewGraphType_LocalType0::Mp7JrsVideoViewGraphType_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoViewGraphType_LocalType0::~Mp7JrsVideoViewGraphType_LocalType0()
{
	Cleanup();
}

void Mp7JrsVideoViewGraphType_LocalType0::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TimeFrequencyChild = Mp7JrsVideoViewGraphPtr(); // Class
	m_TimeFrequencyChildRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtMyPropInit.h

}

void Mp7JrsVideoViewGraphType_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TimeFrequencyChild);
	// Dc1Factory::DeleteObject(m_TimeFrequencyChildRef);
}

void Mp7JrsVideoViewGraphType_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoViewGraphType_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in IVideoViewGraphType_LocalType0
	const Dc1Ptr< Mp7JrsVideoViewGraphType_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_TimeFrequencyChild);
		this->SetTimeFrequencyChild(Dc1Factory::CloneObject(tmp->GetTimeFrequencyChild()));
		// Dc1Factory::DeleteObject(m_TimeFrequencyChildRef);
		this->SetTimeFrequencyChildRef(Dc1Factory::CloneObject(tmp->GetTimeFrequencyChildRef()));
}

Dc1NodePtr Mp7JrsVideoViewGraphType_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoViewGraphType_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsVideoViewGraphPtr Mp7JrsVideoViewGraphType_LocalType0::GetTimeFrequencyChild() const
{
		return m_TimeFrequencyChild;
}

Mp7JrsReferencePtr Mp7JrsVideoViewGraphType_LocalType0::GetTimeFrequencyChildRef() const
{
		return m_TimeFrequencyChildRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsVideoViewGraphType_LocalType0::SetTimeFrequencyChild(const Mp7JrsVideoViewGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_LocalType0::SetTimeFrequencyChild().");
	}
	if (m_TimeFrequencyChild != item)
	{
		// Dc1Factory::DeleteObject(m_TimeFrequencyChild);
		m_TimeFrequencyChild = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimeFrequencyChild) m_TimeFrequencyChild->SetParent(m_myself.getPointer());
		if (m_TimeFrequencyChild && m_TimeFrequencyChild->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimeFrequencyChild->UseTypeAttribute = true;
		}
		if(m_TimeFrequencyChild != Mp7JrsVideoViewGraphPtr())
		{
			m_TimeFrequencyChild->SetContentName(XMLString::transcode("TimeFrequencyChild"));
		}
	// Dc1Factory::DeleteObject(m_TimeFrequencyChildRef);
	m_TimeFrequencyChildRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsVideoViewGraphType_LocalType0::SetTimeFrequencyChildRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_LocalType0::SetTimeFrequencyChildRef().");
	}
	if (m_TimeFrequencyChildRef != item)
	{
		// Dc1Factory::DeleteObject(m_TimeFrequencyChildRef);
		m_TimeFrequencyChildRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimeFrequencyChildRef) m_TimeFrequencyChildRef->SetParent(m_myself.getPointer());
		if (m_TimeFrequencyChildRef && m_TimeFrequencyChildRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimeFrequencyChildRef->UseTypeAttribute = true;
		}
		if(m_TimeFrequencyChildRef != Mp7JrsReferencePtr())
		{
			m_TimeFrequencyChildRef->SetContentName(XMLString::transcode("TimeFrequencyChildRef"));
		}
	// Dc1Factory::DeleteObject(m_TimeFrequencyChild);
	m_TimeFrequencyChild = Mp7JrsVideoViewGraphPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: VideoViewGraphType_LocalType0: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsVideoViewGraphType_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsVideoViewGraphType_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoViewGraphType_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoViewGraphType_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_TimeFrequencyChild != Mp7JrsVideoViewGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TimeFrequencyChild->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TimeFrequencyChild"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TimeFrequencyChild->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TimeFrequencyChildRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TimeFrequencyChildRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TimeFrequencyChildRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TimeFrequencyChildRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVideoViewGraphType_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TimeFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimeFrequencyChild")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoViewGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTimeFrequencyChild(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TimeFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimeFrequencyChildRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTimeFrequencyChildRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoViewGraphType_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TimeFrequencyChild")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoViewGraphType; // FTT, check this
	}
	this->SetTimeFrequencyChild(child);
  }
  if (XMLString::compareString(elementname, X("TimeFrequencyChildRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetTimeFrequencyChildRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoViewGraphType_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimeFrequencyChild() != Dc1NodePtr())
		result.Insert(GetTimeFrequencyChild());
	if (GetTimeFrequencyChildRef() != Dc1NodePtr())
		result.Insert(GetTimeFrequencyChildRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_LocalType0_ExtMethodImpl.h


