
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsTextureBrowsingType_CollectionType.h"
#include "Mp7JrsTextureBrowsingType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTextureBrowsingType::IMp7JrsTextureBrowsingType()
{

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtPropInit.h

}

IMp7JrsTextureBrowsingType::~IMp7JrsTextureBrowsingType()
{
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtPropCleanup.h

}

Mp7JrsTextureBrowsingType::Mp7JrsTextureBrowsingType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextureBrowsingType::~Mp7JrsTextureBrowsingType()
{
	Cleanup();
}

void Mp7JrsTextureBrowsingType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Regularity = Mp7JrsTextureBrowsingType_Regularity_LocalType::UninitializedEnumeration; // Enumeration
	m_TextureBrowsingType_LocalType = Mp7JrsTextureBrowsingType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtMyPropInit.h

}

void Mp7JrsTextureBrowsingType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_TextureBrowsingType_LocalType);
}

void Mp7JrsTextureBrowsingType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextureBrowsingTypePtr, since we
	// might need GetBase(), which isn't defined in ITextureBrowsingType
	const Dc1Ptr< Mp7JrsTextureBrowsingType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsTextureBrowsingType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		this->SetRegularity(tmp->GetRegularity());
		// Dc1Factory::DeleteObject(m_TextureBrowsingType_LocalType);
		this->SetTextureBrowsingType_LocalType(Dc1Factory::CloneObject(tmp->GetTextureBrowsingType_LocalType()));
}

Dc1NodePtr Mp7JrsTextureBrowsingType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsTextureBrowsingType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsTextureBrowsingType::GetBase() const
{
	return m_Base;
}

Mp7JrsTextureBrowsingType_Regularity_LocalType::Enumeration Mp7JrsTextureBrowsingType::GetRegularity() const
{
		return m_Regularity;
}

Mp7JrsTextureBrowsingType_CollectionPtr Mp7JrsTextureBrowsingType::GetTextureBrowsingType_LocalType() const
{
		return m_TextureBrowsingType_LocalType;
}

void Mp7JrsTextureBrowsingType::SetRegularity(Mp7JrsTextureBrowsingType_Regularity_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextureBrowsingType::SetRegularity().");
	}
	if (m_Regularity != item)
	{
		// nothing to free here, hopefully
		m_Regularity = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextureBrowsingType::SetTextureBrowsingType_LocalType(const Mp7JrsTextureBrowsingType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextureBrowsingType::SetTextureBrowsingType_LocalType().");
	}
	if (m_TextureBrowsingType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_TextureBrowsingType_LocalType);
		m_TextureBrowsingType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextureBrowsingType_LocalType) m_TextureBrowsingType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTextureBrowsingType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	if (isentrypoint)
	{
		// No Dc1NodePtr subelement to get or create for TextureBrowsingType
		Dc1XPathParseContext::ErrorNoSubElement(context, "TextureBrowsingType");
	}
	return result;
}

XMLCh * Mp7JrsTextureBrowsingType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsTextureBrowsingType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsTextureBrowsingType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextureBrowsingType"));
	// Element serialization:
	if(m_Regularity != Mp7JrsTextureBrowsingType_Regularity_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsTextureBrowsingType_Regularity_LocalType::ToText(m_Regularity);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Regularity"), false);
		XMLString::release(&tmp);
	}
	if (m_TextureBrowsingType_LocalType != Mp7JrsTextureBrowsingType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TextureBrowsingType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsTextureBrowsingType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Regularity"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Regularity")) == 0))
		{
			Mp7JrsTextureBrowsingType_Regularity_LocalType::Enumeration tmp = Mp7JrsTextureBrowsingType_Regularity_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsTextureBrowsingType_Regularity_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetRegularity(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Direction")) == 0)
			(Dc1Util::HasNodeName(parent, X("Direction"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Scale")) == 0)
			(Dc1Util::HasNodeName(parent, X("Scale"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsTextureBrowsingType_CollectionPtr tmp = CreateTextureBrowsingType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTextureBrowsingType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextureBrowsingType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsTextureBrowsingType_CollectionPtr tmp = CreateTextureBrowsingType_CollectionType; // FTT, check this
	this->SetTextureBrowsingType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextureBrowsingType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTextureBrowsingType_LocalType() != Dc1NodePtr())
		result.Insert(GetTextureBrowsingType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_ExtMethodImpl.h


