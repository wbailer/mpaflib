
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtImplInclude.h


#include "Mp7JrsGeographicPointType_longitude_LocalType.h"
#include "Mp7JrsGeographicPointType_latitude_LocalType.h"
#include "Mp7JrsGeographicPointType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsGeographicPointType::IMp7JrsGeographicPointType()
{

// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtPropInit.h

}

IMp7JrsGeographicPointType::~IMp7JrsGeographicPointType()
{
// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtPropCleanup.h

}

Mp7JrsGeographicPointType::Mp7JrsGeographicPointType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGeographicPointType::~Mp7JrsGeographicPointType()
{
	Cleanup();
}

void Mp7JrsGeographicPointType::Init()
{

	// Init attributes
	m_longitude = CreateGeographicPointType_longitude_LocalType; // Create a valid class, FTT, check this
	m_longitude->SetContent(-180.0); // Use Value initialiser (xxx2)
	m_latitude = CreateGeographicPointType_latitude_LocalType; // Create a valid class, FTT, check this
	m_latitude->SetContent(-90.0); // Use Value initialiser (xxx2)
	m_altitude = 0.0; // Value
	m_altitude_Exist = false;



// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtMyPropInit.h

}

void Mp7JrsGeographicPointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_longitude);
	// Dc1Factory::DeleteObject(m_latitude);
}

void Mp7JrsGeographicPointType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GeographicPointTypePtr, since we
	// might need GetBase(), which isn't defined in IGeographicPointType
	const Dc1Ptr< Mp7JrsGeographicPointType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_longitude);
		this->Setlongitude(Dc1Factory::CloneObject(tmp->Getlongitude()));
	}
	{
	// Dc1Factory::DeleteObject(m_latitude);
		this->Setlatitude(Dc1Factory::CloneObject(tmp->Getlatitude()));
	}
	{
	if (tmp->Existaltitude())
	{
		this->Setaltitude(tmp->Getaltitude());
	}
	else
	{
		Invalidatealtitude();
	}
	}
}

Dc1NodePtr Mp7JrsGeographicPointType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsGeographicPointType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsGeographicPointType_longitude_LocalPtr Mp7JrsGeographicPointType::Getlongitude() const
{
	return m_longitude;
}

void Mp7JrsGeographicPointType::Setlongitude(const Mp7JrsGeographicPointType_longitude_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeographicPointType::Setlongitude().");
	}
	m_longitude = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_longitude) m_longitude->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsGeographicPointType_latitude_LocalPtr Mp7JrsGeographicPointType::Getlatitude() const
{
	return m_latitude;
}

void Mp7JrsGeographicPointType::Setlatitude(const Mp7JrsGeographicPointType_latitude_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeographicPointType::Setlatitude().");
	}
	m_latitude = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_latitude) m_latitude->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

double Mp7JrsGeographicPointType::Getaltitude() const
{
	return m_altitude;
}

bool Mp7JrsGeographicPointType::Existaltitude() const
{
	return m_altitude_Exist;
}
void Mp7JrsGeographicPointType::Setaltitude(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeographicPointType::Setaltitude().");
	}
	m_altitude = item;
	m_altitude_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeographicPointType::Invalidatealtitude()
{
	m_altitude_Exist = false;
}

Dc1NodeEnum Mp7JrsGeographicPointType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsGeographicPointType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsGeographicPointType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsGeographicPointType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GeographicPointType"));
	// Attribute Serialization:
	// Class
	if (m_longitude != Mp7JrsGeographicPointType_longitude_LocalPtr())
	{
		XMLCh * tmp = m_longitude->ToText();
		element->setAttributeNS(X(""), X("longitude"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Class
	if (m_latitude != Mp7JrsGeographicPointType_latitude_LocalPtr())
	{
		XMLCh * tmp = m_latitude->ToText();
		element->setAttributeNS(X(""), X("latitude"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_altitude_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_altitude);
		element->setAttributeNS(X(""), X("altitude"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsGeographicPointType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("longitude")))
	{
		// Deserialize class type
		Mp7JrsGeographicPointType_longitude_LocalPtr tmp = CreateGeographicPointType_longitude_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("longitude")));
		this->Setlongitude(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("latitude")))
	{
		// Deserialize class type
		Mp7JrsGeographicPointType_latitude_LocalPtr tmp = CreateGeographicPointType_latitude_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("latitude")));
		this->Setlatitude(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("altitude")))
	{
		// deserialize value type
		this->Setaltitude(Dc1Convert::TextToDouble(parent->getAttribute(X("altitude"))));
		* current = parent;
	}

// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGeographicPointType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsGeographicPointType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGeographicPointType_ExtMethodImpl.h


