
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsRegionLocatorType.h"
#include "Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType::IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtPropInit.h

}

IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType::~IMp7JrsInkMediaInformationType_OverlaidMedia_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtPropCleanup.h

}

Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::~Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType()
{
	Cleanup();
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Init()
{

	// Init attributes
	m_fieldIDRef = NULL; // String
	m_fieldIDRef_Exist = false;
	m_translationX = 0; // Value
	m_translationX_Default = 0; // Default value
	m_translationX_Exist = false;
	m_translationY = 0; // Value
	m_translationY_Default = 0; // Default value
	m_translationY_Exist = false;
	m_rotation = 0.0f; // Value
	m_rotation_Default = 0.0; // Default value
	m_rotation_Exist = false;
	m_scaleX = 0.0f; // Value
	m_scaleX_Default = 1.0; // Default value
	m_scaleX_Exist = false;
	m_scaleY = 0.0f; // Value
	m_scaleY_Default = 1.0; // Default value
	m_scaleY_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_OverlaidMediaLocator = Mp7JrsMediaLocatorPtr(); // Class
	m_OverlaidMediaRef = Mp7JrsReferencePtr(); // Class
	m_MediaRegionLocator = Mp7JrsRegionLocatorPtr(); // Class
	m_MediaRegionLocator_Exist = false;


// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMyPropInit.h

}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_fieldIDRef); // String
	// Dc1Factory::DeleteObject(m_OverlaidMediaLocator);
	// Dc1Factory::DeleteObject(m_OverlaidMediaRef);
	// Dc1Factory::DeleteObject(m_MediaRegionLocator);
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InkMediaInformationType_OverlaidMedia_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IInkMediaInformationType_OverlaidMedia_LocalType
	const Dc1Ptr< Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_fieldIDRef); // String
	if (tmp->ExistfieldIDRef())
	{
		this->SetfieldIDRef(XMLString::replicate(tmp->GetfieldIDRef()));
	}
	else
	{
		InvalidatefieldIDRef();
	}
	}
	{
	if (tmp->ExisttranslationX())
	{
		this->SettranslationX(tmp->GettranslationX());
	}
	else
	{
		InvalidatetranslationX();
	}
	}
	{
	if (tmp->ExisttranslationY())
	{
		this->SettranslationY(tmp->GettranslationY());
	}
	else
	{
		InvalidatetranslationY();
	}
	}
	{
	if (tmp->Existrotation())
	{
		this->Setrotation(tmp->Getrotation());
	}
	else
	{
		Invalidaterotation();
	}
	}
	{
	if (tmp->ExistscaleX())
	{
		this->SetscaleX(tmp->GetscaleX());
	}
	else
	{
		InvalidatescaleX();
	}
	}
	{
	if (tmp->ExistscaleY())
	{
		this->SetscaleY(tmp->GetscaleY());
	}
	else
	{
		InvalidatescaleY();
	}
	}
		// Dc1Factory::DeleteObject(m_OverlaidMediaLocator);
		this->SetOverlaidMediaLocator(Dc1Factory::CloneObject(tmp->GetOverlaidMediaLocator()));
		// Dc1Factory::DeleteObject(m_OverlaidMediaRef);
		this->SetOverlaidMediaRef(Dc1Factory::CloneObject(tmp->GetOverlaidMediaRef()));
	if (tmp->IsValidMediaRegionLocator())
	{
		// Dc1Factory::DeleteObject(m_MediaRegionLocator);
		this->SetMediaRegionLocator(Dc1Factory::CloneObject(tmp->GetMediaRegionLocator()));
	}
	else
	{
		InvalidateMediaRegionLocator();
	}
}

Dc1NodePtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetfieldIDRef() const
{
	return m_fieldIDRef;
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ExistfieldIDRef() const
{
	return m_fieldIDRef_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetfieldIDRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetfieldIDRef().");
	}
	m_fieldIDRef = item;
	m_fieldIDRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidatefieldIDRef()
{
	m_fieldIDRef_Exist = false;
}
int Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GettranslationX() const
{
	if (this->ExisttranslationX()) {
		return m_translationX;
	} else {
		return m_translationX_Default;
	}
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ExisttranslationX() const
{
	return m_translationX_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SettranslationX(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SettranslationX().");
	}
	m_translationX = item;
	m_translationX_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidatetranslationX()
{
	m_translationX_Exist = false;
}
int Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GettranslationY() const
{
	if (this->ExisttranslationY()) {
		return m_translationY;
	} else {
		return m_translationY_Default;
	}
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ExisttranslationY() const
{
	return m_translationY_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SettranslationY(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SettranslationY().");
	}
	m_translationY = item;
	m_translationY_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidatetranslationY()
{
	m_translationY_Exist = false;
}
float Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Getrotation() const
{
	if (this->Existrotation()) {
		return m_rotation;
	} else {
		return m_rotation_Default;
	}
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Existrotation() const
{
	return m_rotation_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Setrotation(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Setrotation().");
	}
	m_rotation = item;
	m_rotation_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Invalidaterotation()
{
	m_rotation_Exist = false;
}
float Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetscaleX() const
{
	if (this->ExistscaleX()) {
		return m_scaleX;
	} else {
		return m_scaleX_Default;
	}
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ExistscaleX() const
{
	return m_scaleX_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetscaleX(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetscaleX().");
	}
	m_scaleX = item;
	m_scaleX_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidatescaleX()
{
	m_scaleX_Exist = false;
}
float Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetscaleY() const
{
	if (this->ExistscaleY()) {
		return m_scaleY;
	} else {
		return m_scaleY_Default;
	}
}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ExistscaleY() const
{
	return m_scaleY_Exist;
}
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetscaleY(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetscaleY().");
	}
	m_scaleY = item;
	m_scaleY_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidatescaleY()
{
	m_scaleY_Exist = false;
}
Mp7JrsMediaLocatorPtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetOverlaidMediaLocator() const
{
		return m_OverlaidMediaLocator;
}

Mp7JrsReferencePtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetOverlaidMediaRef() const
{
		return m_OverlaidMediaRef;
}

Mp7JrsRegionLocatorPtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetMediaRegionLocator() const
{
		return m_MediaRegionLocator;
}

// Element is optional
bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::IsValidMediaRegionLocator() const
{
	return m_MediaRegionLocator_Exist;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetOverlaidMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetOverlaidMediaLocator().");
	}
	if (m_OverlaidMediaLocator != item)
	{
		// Dc1Factory::DeleteObject(m_OverlaidMediaLocator);
		m_OverlaidMediaLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_OverlaidMediaLocator) m_OverlaidMediaLocator->SetParent(m_myself.getPointer());
		if (m_OverlaidMediaLocator && m_OverlaidMediaLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_OverlaidMediaLocator->UseTypeAttribute = true;
		}
		if(m_OverlaidMediaLocator != Mp7JrsMediaLocatorPtr())
		{
			m_OverlaidMediaLocator->SetContentName(XMLString::transcode("OverlaidMediaLocator"));
		}
	// Dc1Factory::DeleteObject(m_OverlaidMediaRef);
	m_OverlaidMediaRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetOverlaidMediaRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetOverlaidMediaRef().");
	}
	if (m_OverlaidMediaRef != item)
	{
		// Dc1Factory::DeleteObject(m_OverlaidMediaRef);
		m_OverlaidMediaRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_OverlaidMediaRef) m_OverlaidMediaRef->SetParent(m_myself.getPointer());
		if (m_OverlaidMediaRef && m_OverlaidMediaRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_OverlaidMediaRef->UseTypeAttribute = true;
		}
		if(m_OverlaidMediaRef != Mp7JrsReferencePtr())
		{
			m_OverlaidMediaRef->SetContentName(XMLString::transcode("OverlaidMediaRef"));
		}
	// Dc1Factory::DeleteObject(m_OverlaidMediaLocator);
	m_OverlaidMediaLocator = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetMediaRegionLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::SetMediaRegionLocator().");
	}
	if (m_MediaRegionLocator != item || m_MediaRegionLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaRegionLocator);
		m_MediaRegionLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRegionLocator) m_MediaRegionLocator->SetParent(m_myself.getPointer());
		if (m_MediaRegionLocator && m_MediaRegionLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRegionLocator->UseTypeAttribute = true;
		}
		m_MediaRegionLocator_Exist = true;
		if(m_MediaRegionLocator != Mp7JrsRegionLocatorPtr())
		{
			m_MediaRegionLocator->SetContentName(XMLString::transcode("MediaRegionLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::InvalidateMediaRegionLocator()
{
	m_MediaRegionLocator_Exist = false;
}

Dc1NodeEnum Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  6, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("fieldIDRef")) == 0)
	{
		// fieldIDRef is simple attribute IDREF
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("translationX")) == 0)
	{
		// translationX is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("translationY")) == 0)
	{
		// translationY is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("rotation")) == 0)
	{
		// rotation is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("scaleX")) == 0)
	{
		// scaleX is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("scaleY")) == 0)
	{
		// scaleY is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("OverlaidMediaLocator")) == 0)
	{
		// OverlaidMediaLocator is simple element MediaLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOverlaidMediaLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOverlaidMediaLocator(p, client);
					if((p = GetOverlaidMediaLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("OverlaidMediaRef")) == 0)
	{
		// OverlaidMediaRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOverlaidMediaRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOverlaidMediaRef(p, client);
					if((p = GetOverlaidMediaRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRegionLocator")) == 0)
	{
		// MediaRegionLocator is simple element RegionLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRegionLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRegionLocator(p, client);
					if((p = GetMediaRegionLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for InkMediaInformationType_OverlaidMedia_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "InkMediaInformationType_OverlaidMedia_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InkMediaInformationType_OverlaidMedia_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_fieldIDRef_Exist)
	{
	// String
	if(m_fieldIDRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("fieldIDRef"), m_fieldIDRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_translationX_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_translationX);
		element->setAttributeNS(X(""), X("translationX"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_translationY_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_translationY);
		element->setAttributeNS(X(""), X("translationY"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_rotation_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_rotation);
		element->setAttributeNS(X(""), X("rotation"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_scaleX_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_scaleX);
		element->setAttributeNS(X(""), X("scaleX"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_scaleY_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_scaleY);
		element->setAttributeNS(X(""), X("scaleY"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_OverlaidMediaLocator != Mp7JrsMediaLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_OverlaidMediaLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("OverlaidMediaLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_OverlaidMediaLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_OverlaidMediaRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_OverlaidMediaRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("OverlaidMediaRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_OverlaidMediaRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaRegionLocator != Mp7JrsRegionLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRegionLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRegionLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRegionLocator->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("fieldIDRef")))
	{
		// Deserialize string type
		this->SetfieldIDRef(Dc1Convert::TextToString(parent->getAttribute(X("fieldIDRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("translationX")))
	{
		// deserialize value type
		this->SettranslationX(Dc1Convert::TextToInt(parent->getAttribute(X("translationX"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("translationY")))
	{
		// deserialize value type
		this->SettranslationY(Dc1Convert::TextToInt(parent->getAttribute(X("translationY"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("rotation")))
	{
		// deserialize value type
		this->Setrotation(Dc1Convert::TextToFloat(parent->getAttribute(X("rotation"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("scaleX")))
	{
		// deserialize value type
		this->SetscaleX(Dc1Convert::TextToFloat(parent->getAttribute(X("scaleX"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("scaleY")))
	{
		// deserialize value type
		this->SetscaleY(Dc1Convert::TextToFloat(parent->getAttribute(X("scaleY"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("OverlaidMediaLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("OverlaidMediaLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOverlaidMediaLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("OverlaidMediaRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("OverlaidMediaRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOverlaidMediaRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRegionLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRegionLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRegionLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("OverlaidMediaLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaLocatorType; // FTT, check this
	}
	this->SetOverlaidMediaLocator(child);
  }
  if (XMLString::compareString(elementname, X("OverlaidMediaRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetOverlaidMediaRef(child);
  }
  if (XMLString::compareString(elementname, X("MediaRegionLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionLocatorType; // FTT, check this
	}
	this->SetMediaRegionLocator(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaRegionLocator() != Dc1NodePtr())
		result.Insert(GetMediaRegionLocator());
	if (GetOverlaidMediaLocator() != Dc1NodePtr())
		result.Insert(GetOverlaidMediaLocator());
	if (GetOverlaidMediaRef() != Dc1NodePtr())
		result.Insert(GetOverlaidMediaRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType_ExtMethodImpl.h


