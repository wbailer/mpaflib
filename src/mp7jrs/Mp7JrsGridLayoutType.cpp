
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtImplInclude.h


#include "Mp7Jrsunsigned8.h"
#include "Mp7JrsGridLayoutType_descriptorMask_LocalType.h"
#include "Mp7JrsGridLayoutType_Descriptor_CollectionType.h"
#include "Mp7JrsGridLayoutType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsVisualDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Descriptor

#include <assert.h>
IMp7JrsGridLayoutType::IMp7JrsGridLayoutType()
{

// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtPropInit.h

}

IMp7JrsGridLayoutType::~IMp7JrsGridLayoutType()
{
// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtPropCleanup.h

}

Mp7JrsGridLayoutType::Mp7JrsGridLayoutType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGridLayoutType::~Mp7JrsGridLayoutType()
{
	Cleanup();
}

void Mp7JrsGridLayoutType::Init()
{

	// Init attributes
	m_numOfPartX = Createunsigned8; // Create a valid class, FTT, check this
	m_numOfPartX->SetContent(0); // Use Value initialiser (xxx2)
	m_numOfPartY = Createunsigned8; // Create a valid class, FTT, check this
	m_numOfPartY->SetContent(0); // Use Value initialiser (xxx2)
	m_descriptorMask = Mp7JrsGridLayoutType_descriptorMask_LocalPtr(); // Pattern
	m_descriptorMask_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Descriptor = Mp7JrsGridLayoutType_Descriptor_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtMyPropInit.h

}

void Mp7JrsGridLayoutType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_numOfPartX);
	// Dc1Factory::DeleteObject(m_numOfPartY);
	// Dc1Factory::DeleteObject(m_descriptorMask); // Pattern
	// Dc1Factory::DeleteObject(m_Descriptor);
}

void Mp7JrsGridLayoutType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GridLayoutTypePtr, since we
	// might need GetBase(), which isn't defined in IGridLayoutType
	const Dc1Ptr< Mp7JrsGridLayoutType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_numOfPartX);
		this->SetnumOfPartX(Dc1Factory::CloneObject(tmp->GetnumOfPartX()));
	}
	{
	// Dc1Factory::DeleteObject(m_numOfPartY);
		this->SetnumOfPartY(Dc1Factory::CloneObject(tmp->GetnumOfPartY()));
	}
	{
	// Dc1Factory::DeleteObject(m_descriptorMask); // Pattern
	if (tmp->ExistdescriptorMask())
	{
		this->SetdescriptorMask(Dc1Factory::CloneObject(tmp->GetdescriptorMask()));
	}
	else
	{
		InvalidatedescriptorMask();
	}
	}
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
}

Dc1NodePtr Mp7JrsGridLayoutType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsGridLayoutType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned8Ptr Mp7JrsGridLayoutType::GetnumOfPartX() const
{
	return m_numOfPartX;
}

void Mp7JrsGridLayoutType::SetnumOfPartX(const Mp7Jrsunsigned8Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGridLayoutType::SetnumOfPartX().");
	}
	m_numOfPartX = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_numOfPartX) m_numOfPartX->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7Jrsunsigned8Ptr Mp7JrsGridLayoutType::GetnumOfPartY() const
{
	return m_numOfPartY;
}

void Mp7JrsGridLayoutType::SetnumOfPartY(const Mp7Jrsunsigned8Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGridLayoutType::SetnumOfPartY().");
	}
	m_numOfPartY = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_numOfPartY) m_numOfPartY->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsGridLayoutType_descriptorMask_LocalPtr Mp7JrsGridLayoutType::GetdescriptorMask() const
{
	return m_descriptorMask;
}

bool Mp7JrsGridLayoutType::ExistdescriptorMask() const
{
	return m_descriptorMask_Exist;
}
void Mp7JrsGridLayoutType::SetdescriptorMask(const Mp7JrsGridLayoutType_descriptorMask_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGridLayoutType::SetdescriptorMask().");
	}
	m_descriptorMask = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_descriptorMask) m_descriptorMask->SetParent(m_myself.getPointer());
	m_descriptorMask_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGridLayoutType::InvalidatedescriptorMask()
{
	m_descriptorMask_Exist = false;
}
Mp7JrsGridLayoutType_Descriptor_CollectionPtr Mp7JrsGridLayoutType::GetDescriptor() const
{
		return m_Descriptor;
}

void Mp7JrsGridLayoutType::SetDescriptor(const Mp7JrsGridLayoutType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGridLayoutType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != Mp7JrsGridLayoutType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGridLayoutType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfPartX")) == 0)
	{
		// numOfPartX is simple attribute unsigned8
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned8Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfPartY")) == 0)
	{
		// numOfPartY is simple attribute unsigned8
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned8Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("descriptorMask")) == 0)
	{
		// descriptorMask is simple attribute GridLayoutType_descriptorMask_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsGridLayoutType_descriptorMask_LocalPtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Descriptor is item of abstract type VisualDType
		// in element collection GridLayoutType_Descriptor_CollectionType
		
		context->Found = true;
		Mp7JrsGridLayoutType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateGridLayoutType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65025))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GridLayoutType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GridLayoutType");
	}
	return result;
}

XMLCh * Mp7JrsGridLayoutType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsGridLayoutType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsGridLayoutType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GridLayoutType"));
	// Attribute Serialization:
	// Class
	if (m_numOfPartX != Mp7Jrsunsigned8Ptr())
	{
		XMLCh * tmp = m_numOfPartX->ToText();
		element->setAttributeNS(X(""), X("numOfPartX"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Class
	if (m_numOfPartY != Mp7Jrsunsigned8Ptr())
	{
		XMLCh * tmp = m_numOfPartY->ToText();
		element->setAttributeNS(X(""), X("numOfPartY"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_descriptorMask_Exist)
	{
	// Pattern
	if (m_descriptorMask != Mp7JrsGridLayoutType_descriptorMask_LocalPtr())
	{
		XMLCh * tmp = m_descriptorMask->ToText();
		element->setAttributeNS(X(""), X("descriptorMask"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Descriptor != Mp7JrsGridLayoutType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsGridLayoutType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfPartX")))
	{
		// Deserialize class type
		Mp7Jrsunsigned8Ptr tmp = Createunsigned8; // FTT, check this
		tmp->Parse(parent->getAttribute(X("numOfPartX")));
		this->SetnumOfPartX(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfPartY")))
	{
		// Deserialize class type
		Mp7Jrsunsigned8Ptr tmp = Createunsigned8; // FTT, check this
		tmp->Parse(parent->getAttribute(X("numOfPartY")));
		this->SetnumOfPartY(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("descriptorMask")))
	{
		Mp7JrsGridLayoutType_descriptorMask_LocalPtr tmp = CreateGridLayoutType_descriptorMask_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("descriptorMask")));
		this->SetdescriptorMask(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			Mp7JrsGridLayoutType_Descriptor_CollectionPtr tmp = CreateGridLayoutType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGridLayoutType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	Mp7JrsGridLayoutType_Descriptor_CollectionPtr tmp = CreateGridLayoutType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGridLayoutType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGridLayoutType_ExtMethodImpl.h


