
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrsColorSamplingType_Field_Component_Offset_LocalType.h"
#include "Mp7JrsColorSamplingType_Field_Component_Period_LocalType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrsInlineTermDefinitionType_Name_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Definition_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsColorSamplingType_Field_Component_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsInlineTermDefinitionType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition
#include "Mp7JrsInlineTermDefinitionType_Term_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Term

#include <assert.h>
IMp7JrsColorSamplingType_Field_Component_LocalType::IMp7JrsColorSamplingType_Field_Component_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtPropInit.h

}

IMp7JrsColorSamplingType_Field_Component_LocalType::~IMp7JrsColorSamplingType_Field_Component_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtPropCleanup.h

}

Mp7JrsColorSamplingType_Field_Component_LocalType::Mp7JrsColorSamplingType_Field_Component_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsColorSamplingType_Field_Component_LocalType::~Mp7JrsColorSamplingType_Field_Component_LocalType()
{
	Cleanup();
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::Init()
{
	// Init base
	m_Base = CreateTermUseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Offset = Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr(); // Class
	m_Offset_Exist = false;
	m_Period = Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr(); // Class
	m_Period_Exist = false;


// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtMyPropInit.h

}

void Mp7JrsColorSamplingType_Field_Component_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Offset);
	// Dc1Factory::DeleteObject(m_Period);
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ColorSamplingType_Field_Component_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IColorSamplingType_Field_Component_LocalType
	const Dc1Ptr< Mp7JrsColorSamplingType_Field_Component_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTermUsePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsColorSamplingType_Field_Component_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidOffset())
	{
		// Dc1Factory::DeleteObject(m_Offset);
		this->SetOffset(Dc1Factory::CloneObject(tmp->GetOffset()));
	}
	else
	{
		InvalidateOffset();
	}
	if (tmp->IsValidPeriod())
	{
		// Dc1Factory::DeleteObject(m_Period);
		this->SetPeriod(Dc1Factory::CloneObject(tmp->GetPeriod()));
	}
	else
	{
		InvalidatePeriod();
	}
}

Dc1NodePtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTermUseType > Mp7JrsColorSamplingType_Field_Component_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetOffset() const
{
		return m_Offset;
}

// Element is optional
bool Mp7JrsColorSamplingType_Field_Component_LocalType::IsValidOffset() const
{
	return m_Offset_Exist;
}

Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetPeriod() const
{
		return m_Period;
}

// Element is optional
bool Mp7JrsColorSamplingType_Field_Component_LocalType::IsValidPeriod() const
{
	return m_Period_Exist;
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::SetOffset(const Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::SetOffset().");
	}
	if (m_Offset != item || m_Offset_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Offset);
		m_Offset = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Offset) m_Offset->SetParent(m_myself.getPointer());
		if (m_Offset && m_Offset->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Offset_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Offset->UseTypeAttribute = true;
		}
		m_Offset_Exist = true;
		if(m_Offset != Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr())
		{
			m_Offset->SetContentName(XMLString::transcode("Offset"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::InvalidateOffset()
{
	m_Offset_Exist = false;
}
void Mp7JrsColorSamplingType_Field_Component_LocalType::SetPeriod(const Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::SetPeriod().");
	}
	if (m_Period != item || m_Period_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Period);
		m_Period = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Period) m_Period->SetParent(m_myself.getPointer());
		if (m_Period && m_Period->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Period_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Period->UseTypeAttribute = true;
		}
		m_Period_Exist = true;
		if(m_Period != Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr())
		{
			m_Period->SetContentName(XMLString::transcode("Period"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::InvalidatePeriod()
{
	m_Period_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsColorSamplingType_Field_Component_LocalType::Gethref() const
{
	return GetBase()->Gethref();
}

bool Mp7JrsColorSamplingType_Field_Component_LocalType::Existhref() const
{
	return GetBase()->Existhref();
}
void Mp7JrsColorSamplingType_Field_Component_LocalType::Sethref(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::Sethref().");
	}
	GetBase()->Sethref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::Invalidatehref()
{
	GetBase()->Invalidatehref();
}
Mp7JrsInlineTermDefinitionType_Name_CollectionPtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetName() const
{
	return GetBase()->GetBase()->GetName();
}

Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetDefinition() const
{
	return GetBase()->GetBase()->GetDefinition();
}

Mp7JrsInlineTermDefinitionType_Term_CollectionPtr Mp7JrsColorSamplingType_Field_Component_LocalType::GetTerm() const
{
	return GetBase()->GetBase()->GetTerm();
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::SetName(const Mp7JrsInlineTermDefinitionType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::SetName().");
	}
	GetBase()->GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::SetDefinition(const Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::SetDefinition().");
	}
	GetBase()->GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSamplingType_Field_Component_LocalType::SetTerm(const Mp7JrsInlineTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSamplingType_Field_Component_LocalType::SetTerm().");
	}
	GetBase()->GetBase()->SetTerm(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsColorSamplingType_Field_Component_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Offset")) == 0)
	{
		// Offset is simple element ColorSamplingType_Field_Component_Offset_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOffset()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Offset_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOffset(p, client);
					if((p = GetOffset()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Period")) == 0)
	{
		// Period is simple element ColorSamplingType_Field_Component_Period_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPeriod()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType_Field_Component_Period_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPeriod(p, client);
					if((p = GetPeriod()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ColorSamplingType_Field_Component_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ColorSamplingType_Field_Component_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsColorSamplingType_Field_Component_LocalType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsColorSamplingType_Field_Component_LocalType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsColorSamplingType_Field_Component_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ColorSamplingType_Field_Component_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Offset != Mp7JrsColorSamplingType_Field_Component_Offset_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Offset->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Offset"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Offset->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Period != Mp7JrsColorSamplingType_Field_Component_Period_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Period->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Period"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Period->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsColorSamplingType_Field_Component_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTermUseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Offset"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Offset")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorSamplingType_Field_Component_Offset_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOffset(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Period"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Period")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorSamplingType_Field_Component_Period_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPeriod(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsColorSamplingType_Field_Component_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Offset")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorSamplingType_Field_Component_Offset_LocalType; // FTT, check this
	}
	this->SetOffset(child);
  }
  if (XMLString::compareString(elementname, X("Period")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorSamplingType_Field_Component_Period_LocalType; // FTT, check this
	}
	this->SetPeriod(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsColorSamplingType_Field_Component_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOffset() != Dc1NodePtr())
		result.Insert(GetOffset());
	if (GetPeriod() != Dc1NodePtr())
		result.Insert(GetPeriod());
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsColorSamplingType_Field_Component_LocalType_ExtMethodImpl.h


