
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtImplInclude.h


#include "Mp7JrsAudioDSType.h"
#include "Mp7JrsMelodySequenceType_NoteArray_Note_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMelodySequenceType_NoteArray_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMelodySequenceType_NoteArray_Note_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Note

#include <assert.h>
IMp7JrsMelodySequenceType_NoteArray_LocalType::IMp7JrsMelodySequenceType_NoteArray_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtPropInit.h

}

IMp7JrsMelodySequenceType_NoteArray_LocalType::~IMp7JrsMelodySequenceType_NoteArray_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtPropCleanup.h

}

Mp7JrsMelodySequenceType_NoteArray_LocalType::Mp7JrsMelodySequenceType_NoteArray_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMelodySequenceType_NoteArray_LocalType::~Mp7JrsMelodySequenceType_NoteArray_LocalType()
{
	Cleanup();
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::Init()
{
	// Init base
	m_Base = CreateAudioDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Note = Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtMyPropInit.h

}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Note);
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MelodySequenceType_NoteArray_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMelodySequenceType_NoteArray_LocalType
	const Dc1Ptr< Mp7JrsMelodySequenceType_NoteArray_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMelodySequenceType_NoteArray_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Note);
		this->SetNote(Dc1Factory::CloneObject(tmp->GetNote()));
}

Dc1NodePtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDSType > Mp7JrsMelodySequenceType_NoteArray_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetNote() const
{
		return m_Note;
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::SetNote(const Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SetNote().");
	}
	if (m_Note != item)
	{
		// Dc1Factory::DeleteObject(m_Note);
		m_Note = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Note) m_Note->SetParent(m_myself.getPointer());
		if(m_Note != Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr())
		{
			m_Note->SetContentName(XMLString::transcode("Note"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsMelodySequenceType_NoteArray_LocalType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMelodySequenceType_NoteArray_LocalType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMelodySequenceType_NoteArray_LocalType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMelodySequenceType_NoteArray_LocalType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_NoteArray_LocalType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMelodySequenceType_NoteArray_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Note")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Note is item of type MelodySequenceType_NoteArray_Note_LocalType
		// in element collection MelodySequenceType_NoteArray_Note_CollectionType
		
		context->Found = true;
		Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr coll = GetNote();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMelodySequenceType_NoteArray_Note_CollectionType; // FTT, check this
				SetNote(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_Note_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMelodySequenceType_NoteArray_Note_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MelodySequenceType_NoteArray_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MelodySequenceType_NoteArray_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMelodySequenceType_NoteArray_LocalType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMelodySequenceType_NoteArray_LocalType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMelodySequenceType_NoteArray_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MelodySequenceType_NoteArray_LocalType"));
	// Element serialization:
	if (m_Note != Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Note->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMelodySequenceType_NoteArray_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Note"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Note")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr tmp = CreateMelodySequenceType_NoteArray_Note_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNote(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMelodySequenceType_NoteArray_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Note")) == 0))
  {
	Mp7JrsMelodySequenceType_NoteArray_Note_CollectionPtr tmp = CreateMelodySequenceType_NoteArray_Note_CollectionType; // FTT, check this
	this->SetNote(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMelodySequenceType_NoteArray_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetNote() != Dc1NodePtr())
		result.Insert(GetNote());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_NoteArray_LocalType_ExtMethodImpl.h


