
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtImplInclude.h


#include "Mp7JrsSentencesType.h"
#include "Mp7JrsSyntacticConstituentType.h"
#include "Mp7JrsLinguisticDocumentType.h"
#include "Mp7JrsSentencesType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSentencesType_LocalType::IMp7JrsSentencesType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtPropInit.h

}

IMp7JrsSentencesType_LocalType::~IMp7JrsSentencesType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtPropCleanup.h

}

Mp7JrsSentencesType_LocalType::Mp7JrsSentencesType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSentencesType_LocalType::~Mp7JrsSentencesType_LocalType()
{
	Cleanup();
}

void Mp7JrsSentencesType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Sentences = Mp7JrsSentencesPtr(); // Class
	m_Sentence = Mp7JrsSyntacticConstituentPtr(); // Class
	m_Quotation = Mp7JrsLinguisticDocumentPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSentencesType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Sentences);
	// Dc1Factory::DeleteObject(m_Sentence);
	// Dc1Factory::DeleteObject(m_Quotation);
}

void Mp7JrsSentencesType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SentencesType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISentencesType_LocalType
	const Dc1Ptr< Mp7JrsSentencesType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Sentences);
		this->SetSentences(Dc1Factory::CloneObject(tmp->GetSentences()));
		// Dc1Factory::DeleteObject(m_Sentence);
		this->SetSentence(Dc1Factory::CloneObject(tmp->GetSentence()));
		// Dc1Factory::DeleteObject(m_Quotation);
		this->SetQuotation(Dc1Factory::CloneObject(tmp->GetQuotation()));
}

Dc1NodePtr Mp7JrsSentencesType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSentencesType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSentencesPtr Mp7JrsSentencesType_LocalType::GetSentences() const
{
		return m_Sentences;
}

Mp7JrsSyntacticConstituentPtr Mp7JrsSentencesType_LocalType::GetSentence() const
{
		return m_Sentence;
}

Mp7JrsLinguisticDocumentPtr Mp7JrsSentencesType_LocalType::GetQuotation() const
{
		return m_Quotation;
}

// implementing setter for choice 
/* element */
void Mp7JrsSentencesType_LocalType::SetSentences(const Mp7JrsSentencesPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType_LocalType::SetSentences().");
	}
	if (m_Sentences != item)
	{
		// Dc1Factory::DeleteObject(m_Sentences);
		m_Sentences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Sentences) m_Sentences->SetParent(m_myself.getPointer());
		if (m_Sentences && m_Sentences->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SentencesType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Sentences->UseTypeAttribute = true;
		}
		if(m_Sentences != Mp7JrsSentencesPtr())
		{
			m_Sentences->SetContentName(XMLString::transcode("Sentences"));
		}
	// Dc1Factory::DeleteObject(m_Sentence);
	m_Sentence = Mp7JrsSyntacticConstituentPtr();
	// Dc1Factory::DeleteObject(m_Quotation);
	m_Quotation = Mp7JrsLinguisticDocumentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSentencesType_LocalType::SetSentence(const Mp7JrsSyntacticConstituentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType_LocalType::SetSentence().");
	}
	if (m_Sentence != item)
	{
		// Dc1Factory::DeleteObject(m_Sentence);
		m_Sentence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Sentence) m_Sentence->SetParent(m_myself.getPointer());
		if (m_Sentence && m_Sentence->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Sentence->UseTypeAttribute = true;
		}
		if(m_Sentence != Mp7JrsSyntacticConstituentPtr())
		{
			m_Sentence->SetContentName(XMLString::transcode("Sentence"));
		}
	// Dc1Factory::DeleteObject(m_Sentences);
	m_Sentences = Mp7JrsSentencesPtr();
	// Dc1Factory::DeleteObject(m_Quotation);
	m_Quotation = Mp7JrsLinguisticDocumentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSentencesType_LocalType::SetQuotation(const Mp7JrsLinguisticDocumentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSentencesType_LocalType::SetQuotation().");
	}
	if (m_Quotation != item)
	{
		// Dc1Factory::DeleteObject(m_Quotation);
		m_Quotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Quotation) m_Quotation->SetParent(m_myself.getPointer());
		if (m_Quotation && m_Quotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Quotation->UseTypeAttribute = true;
		}
		if(m_Quotation != Mp7JrsLinguisticDocumentPtr())
		{
			m_Quotation->SetContentName(XMLString::transcode("Quotation"));
		}
	// Dc1Factory::DeleteObject(m_Sentences);
	m_Sentences = Mp7JrsSentencesPtr();
	// Dc1Factory::DeleteObject(m_Sentence);
	m_Sentence = Mp7JrsSyntacticConstituentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SentencesType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSentencesType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSentencesType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSentencesType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSentencesType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Sentences != Mp7JrsSentencesPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Sentences->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Sentences"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Sentences->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Sentence != Mp7JrsSyntacticConstituentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Sentence->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Sentence"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Sentence->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Quotation != Mp7JrsLinguisticDocumentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Quotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Quotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Quotation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSentencesType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Sentences"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Sentences")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSentencesType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSentences(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Sentence"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Sentence")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSyntacticConstituentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSentence(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Quotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateLinguisticDocumentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetQuotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSentencesType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Sentences")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSentencesType; // FTT, check this
	}
	this->SetSentences(child);
  }
  if (XMLString::compareString(elementname, X("Sentence")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSyntacticConstituentType; // FTT, check this
	}
	this->SetSentence(child);
  }
  if (XMLString::compareString(elementname, X("Quotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateLinguisticDocumentType; // FTT, check this
	}
	this->SetQuotation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSentencesType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSentences() != Dc1NodePtr())
		result.Insert(GetSentences());
	if (GetSentence() != Dc1NodePtr())
		result.Insert(GetSentence());
	if (GetQuotation() != Dc1NodePtr())
		result.Insert(GetQuotation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSentencesType_LocalType_ExtMethodImpl.h


