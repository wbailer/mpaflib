
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtImplInclude.h


#include "Mp7JrsAnalyticClipType.h"
#include "Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTemporalMaskType.h"
#include "Mp7JrsVideoSegmentType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsVideoSegmentType_Mosaic_CollectionType.h"
#include "Mp7JrsVideoSegmentType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsIntraCompositionShotType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsVideoSegmentType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsVisualTimeSeriesType.h" // Choice collection element VisualTimeSeriesDescriptor
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsMosaicType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Mosaic
#include "Mp7JrsVideoSegmentType_LocalType0.h" // Choice collection SpatialDecomposition
#include "Mp7JrsVideoSegmentSpatialDecompositionType.h" // Choice collection element SpatialDecomposition
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsVideoSegmentSpatioTemporalDecompositionType.h" // Choice collection element SpatioTemporalDecomposition
#include "Mp7JrsVideoSegmentMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AnalyticEditionAreaDecomposition
#include "Mp7JrsIntraCompositionShotEditingTemporalDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AnalyticEditingTemporalDecomposition

#include <assert.h>
IMp7JrsIntraCompositionShotType::IMp7JrsIntraCompositionShotType()
{

// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtPropInit.h

}

IMp7JrsIntraCompositionShotType::~IMp7JrsIntraCompositionShotType()
{
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtPropCleanup.h

}

Mp7JrsIntraCompositionShotType::Mp7JrsIntraCompositionShotType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsIntraCompositionShotType::~Mp7JrsIntraCompositionShotType()
{
	Cleanup();
}

void Mp7JrsIntraCompositionShotType::Init()
{
	// Init base
	m_Base = CreateAnalyticClipType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_AnalyticEditingTemporalDecomposition = Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtMyPropInit.h

}

void Mp7JrsIntraCompositionShotType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_AnalyticEditingTemporalDecomposition);
}

void Mp7JrsIntraCompositionShotType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IntraCompositionShotTypePtr, since we
	// might need GetBase(), which isn't defined in IIntraCompositionShotType
	const Dc1Ptr< Mp7JrsIntraCompositionShotType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAnalyticClipPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsIntraCompositionShotType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_AnalyticEditingTemporalDecomposition);
		this->SetAnalyticEditingTemporalDecomposition(Dc1Factory::CloneObject(tmp->GetAnalyticEditingTemporalDecomposition()));
}

Dc1NodePtr Mp7JrsIntraCompositionShotType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsIntraCompositionShotType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAnalyticClipType > Mp7JrsIntraCompositionShotType::GetBase() const
{
	return m_Base;
}

Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr Mp7JrsIntraCompositionShotType::GetAnalyticEditingTemporalDecomposition() const
{
		return m_AnalyticEditingTemporalDecomposition;
}

void Mp7JrsIntraCompositionShotType::SetAnalyticEditingTemporalDecomposition(const Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetAnalyticEditingTemporalDecomposition().");
	}
	if (m_AnalyticEditingTemporalDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_AnalyticEditingTemporalDecomposition);
		m_AnalyticEditingTemporalDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AnalyticEditingTemporalDecomposition) m_AnalyticEditingTemporalDecomposition->SetParent(m_myself.getPointer());
		if(m_AnalyticEditingTemporalDecomposition != Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr())
		{
			m_AnalyticEditingTemporalDecomposition->SetContentName(XMLString::transcode("AnalyticEditingTemporalDecomposition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsIntraCompositionShotType::GetlocationReliability() const
{
	return GetBase()->GetBase()->GetlocationReliability();
}

bool Mp7JrsIntraCompositionShotType::ExistlocationReliability() const
{
	return GetBase()->GetBase()->ExistlocationReliability();
}
void Mp7JrsIntraCompositionShotType::SetlocationReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetlocationReliability().");
	}
	GetBase()->GetBase()->SetlocationReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidatelocationReliability()
{
	GetBase()->GetBase()->InvalidatelocationReliability();
}
Mp7JrszeroToOnePtr Mp7JrsIntraCompositionShotType::GeteditingLevelReliability() const
{
	return GetBase()->GetBase()->GeteditingLevelReliability();
}

bool Mp7JrsIntraCompositionShotType::ExisteditingLevelReliability() const
{
	return GetBase()->GetBase()->ExisteditingLevelReliability();
}
void Mp7JrsIntraCompositionShotType::SeteditingLevelReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SeteditingLevelReliability().");
	}
	GetBase()->GetBase()->SeteditingLevelReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidateeditingLevelReliability()
{
	GetBase()->GetBase()->InvalidateeditingLevelReliability();
}
Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration Mp7JrsIntraCompositionShotType::Gettype() const
{
	return GetBase()->GetBase()->Gettype();
}

bool Mp7JrsIntraCompositionShotType::Existtype() const
{
	return GetBase()->GetBase()->Existtype();
}
void Mp7JrsIntraCompositionShotType::Settype(Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::Settype().");
	}
	GetBase()->GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::Invalidatetype()
{
	GetBase()->GetBase()->Invalidatetype();
}
Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr Mp7JrsIntraCompositionShotType::GetAnalyticEditionAreaDecomposition() const
{
	return GetBase()->GetBase()->GetAnalyticEditionAreaDecomposition();
}

void Mp7JrsIntraCompositionShotType::SetAnalyticEditionAreaDecomposition(const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetAnalyticEditionAreaDecomposition().");
	}
	GetBase()->GetBase()->SetAnalyticEditionAreaDecomposition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaTimePtr Mp7JrsIntraCompositionShotType::GetMediaTime() const
{
	return GetBase()->GetBase()->GetBase()->GetMediaTime();
}

Mp7JrsTemporalMaskPtr Mp7JrsIntraCompositionShotType::GetTemporalMask() const
{
	return GetBase()->GetBase()->GetBase()->GetTemporalMask();
}

Mp7JrsVideoSegmentType_CollectionPtr Mp7JrsIntraCompositionShotType::GetVideoSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetBase()->GetVideoSegmentType_LocalType();
}

Mp7JrsMultipleViewPtr Mp7JrsIntraCompositionShotType::GetMultipleView() const
{
	return GetBase()->GetBase()->GetBase()->GetMultipleView();
}

// Element is optional
bool Mp7JrsIntraCompositionShotType::IsValidMultipleView() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMultipleView();
}

Mp7JrsVideoSegmentType_Mosaic_CollectionPtr Mp7JrsIntraCompositionShotType::GetMosaic() const
{
	return GetBase()->GetBase()->GetBase()->GetMosaic();
}

Mp7JrsVideoSegmentType_Collection0Ptr Mp7JrsIntraCompositionShotType::GetVideoSegmentType_LocalType0() const
{
	return GetBase()->GetBase()->GetBase()->GetVideoSegmentType_LocalType0();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMediaTime().");
	}
	GetBase()->GetBase()->GetBase()->SetMediaTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotType::SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetTemporalMask().");
	}
	GetBase()->GetBase()->GetBase()->SetTemporalMask(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetVideoSegmentType_LocalType(const Mp7JrsVideoSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetVideoSegmentType_LocalType().");
	}
	GetBase()->GetBase()->GetBase()->SetVideoSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMultipleView().");
	}
	GetBase()->GetBase()->GetBase()->SetMultipleView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidateMultipleView()
{
	GetBase()->GetBase()->GetBase()->InvalidateMultipleView();
}
void Mp7JrsIntraCompositionShotType::SetMosaic(const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMosaic().");
	}
	GetBase()->GetBase()->GetBase()->SetMosaic(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetVideoSegmentType_LocalType0(const Mp7JrsVideoSegmentType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetVideoSegmentType_LocalType0().");
	}
	GetBase()->GetBase()->GetBase()->SetVideoSegmentType_LocalType0(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsIntraCompositionShotType::GetMediaInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsIntraCompositionShotType::GetMediaInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsIntraCompositionShotType::GetMediaLocator() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsIntraCompositionShotType::GetStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsIntraCompositionShotType::IsValidStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsIntraCompositionShotType::GetCreationInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsIntraCompositionShotType::GetCreationInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsIntraCompositionShotType::GetUsageInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsIntraCompositionShotType::GetUsageInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsIntraCompositionShotType::GetTextAnnotation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsIntraCompositionShotType::GetSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsIntraCompositionShotType::GetMatchingHint() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsIntraCompositionShotType::GetPointOfView() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsIntraCompositionShotType::GetRelation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMediaInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMediaInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMediaLocator().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetStructuralUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidateStructuralUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetCreationInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetCreationInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetUsageInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetUsageInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetTextAnnotation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetSegmentType_LocalType().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetMatchingHint().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetPointOfView().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetRelation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsIntraCompositionShotType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsIntraCompositionShotType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsIntraCompositionShotType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsIntraCompositionShotType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsIntraCompositionShotType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsIntraCompositionShotType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsIntraCompositionShotType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsIntraCompositionShotType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsIntraCompositionShotType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsIntraCompositionShotType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsIntraCompositionShotType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsIntraCompositionShotType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsIntraCompositionShotType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsIntraCompositionShotType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsIntraCompositionShotType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIntraCompositionShotType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsIntraCompositionShotType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsIntraCompositionShotType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsIntraCompositionShotType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("AnalyticEditingTemporalDecomposition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AnalyticEditingTemporalDecomposition is item of type IntraCompositionShotEditingTemporalDecompositionType
		// in element collection IntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType
		
		context->Found = true;
		Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr coll = GetAnalyticEditingTemporalDecomposition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType; // FTT, check this
				SetAnalyticEditingTemporalDecomposition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotEditingTemporalDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIntraCompositionShotEditingTemporalDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IntraCompositionShotType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IntraCompositionShotType");
	}
	return result;
}

XMLCh * Mp7JrsIntraCompositionShotType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsIntraCompositionShotType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsIntraCompositionShotType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("IntraCompositionShotType"));
	// Element serialization:
	if (m_AnalyticEditingTemporalDecomposition != Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AnalyticEditingTemporalDecomposition->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsIntraCompositionShotType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAnalyticClipType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AnalyticEditingTemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AnalyticEditingTemporalDecomposition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr tmp = CreateIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAnalyticEditingTemporalDecomposition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsIntraCompositionShotType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AnalyticEditingTemporalDecomposition")) == 0))
  {
	Mp7JrsIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionPtr tmp = CreateIntraCompositionShotType_AnalyticEditingTemporalDecomposition_CollectionType; // FTT, check this
	this->SetAnalyticEditingTemporalDecomposition(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsIntraCompositionShotType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAnalyticEditingTemporalDecomposition() != Dc1NodePtr())
		result.Insert(GetAnalyticEditingTemporalDecomposition());
	if (GetAnalyticEditionAreaDecomposition() != Dc1NodePtr())
		result.Insert(GetAnalyticEditionAreaDecomposition());
	if (GetVideoSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetMosaic() != Dc1NodePtr())
		result.Insert(GetMosaic());
	if (GetVideoSegmentType_LocalType0() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetTemporalMask() != Dc1NodePtr())
		result.Insert(GetTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotType_ExtMethodImpl.h


