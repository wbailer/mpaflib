
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtImplInclude.h


#include "Mp7JrsScalableSeriesType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsScalableSeriesType_Scaling_CollectionType.h"
#include "Mp7JrsSeriesOfScalarType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsScalableSeriesType_Scaling_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Scaling

#include <assert.h>
IMp7JrsSeriesOfScalarType::IMp7JrsSeriesOfScalarType()
{

// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtPropInit.h

}

IMp7JrsSeriesOfScalarType::~IMp7JrsSeriesOfScalarType()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtPropCleanup.h

}

Mp7JrsSeriesOfScalarType::Mp7JrsSeriesOfScalarType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSeriesOfScalarType::~Mp7JrsSeriesOfScalarType()
{
	Cleanup();
}

void Mp7JrsSeriesOfScalarType::Init()
{
	// Init base
	m_Base = CreateScalableSeriesType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Raw = Mp7JrsfloatVectorPtr(); // Collection
	m_Raw_Exist = false;
	m_Min = Mp7JrsfloatVectorPtr(); // Collection
	m_Min_Exist = false;
	m_Max = Mp7JrsfloatVectorPtr(); // Collection
	m_Max_Exist = false;
	m_Mean = Mp7JrsfloatVectorPtr(); // Collection
	m_Mean_Exist = false;
	m_Random = Mp7JrsfloatVectorPtr(); // Collection
	m_Random_Exist = false;
	m_First = Mp7JrsfloatVectorPtr(); // Collection
	m_First_Exist = false;
	m_Last = Mp7JrsfloatVectorPtr(); // Collection
	m_Last_Exist = false;
	m_Variance = Mp7JrsfloatVectorPtr(); // Collection
	m_Variance_Exist = false;
	m_Weight = Mp7JrsfloatVectorPtr(); // Collection
	m_Weight_Exist = false;
	m_LogBase = (0.0f); // Value

	m_LogBase_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtMyPropInit.h

}

void Mp7JrsSeriesOfScalarType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Raw);
	// Dc1Factory::DeleteObject(m_Min);
	// Dc1Factory::DeleteObject(m_Max);
	// Dc1Factory::DeleteObject(m_Mean);
	// Dc1Factory::DeleteObject(m_Random);
	// Dc1Factory::DeleteObject(m_First);
	// Dc1Factory::DeleteObject(m_Last);
	// Dc1Factory::DeleteObject(m_Variance);
	// Dc1Factory::DeleteObject(m_Weight);
}

void Mp7JrsSeriesOfScalarType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SeriesOfScalarTypePtr, since we
	// might need GetBase(), which isn't defined in ISeriesOfScalarType
	const Dc1Ptr< Mp7JrsSeriesOfScalarType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsScalableSeriesPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSeriesOfScalarType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidRaw())
	{
		// Dc1Factory::DeleteObject(m_Raw);
		this->SetRaw(Dc1Factory::CloneObject(tmp->GetRaw()));
	}
	else
	{
		InvalidateRaw();
	}
	if (tmp->IsValidMin())
	{
		// Dc1Factory::DeleteObject(m_Min);
		this->SetMin(Dc1Factory::CloneObject(tmp->GetMin()));
	}
	else
	{
		InvalidateMin();
	}
	if (tmp->IsValidMax())
	{
		// Dc1Factory::DeleteObject(m_Max);
		this->SetMax(Dc1Factory::CloneObject(tmp->GetMax()));
	}
	else
	{
		InvalidateMax();
	}
	if (tmp->IsValidMean())
	{
		// Dc1Factory::DeleteObject(m_Mean);
		this->SetMean(Dc1Factory::CloneObject(tmp->GetMean()));
	}
	else
	{
		InvalidateMean();
	}
	if (tmp->IsValidRandom())
	{
		// Dc1Factory::DeleteObject(m_Random);
		this->SetRandom(Dc1Factory::CloneObject(tmp->GetRandom()));
	}
	else
	{
		InvalidateRandom();
	}
	if (tmp->IsValidFirst())
	{
		// Dc1Factory::DeleteObject(m_First);
		this->SetFirst(Dc1Factory::CloneObject(tmp->GetFirst()));
	}
	else
	{
		InvalidateFirst();
	}
	if (tmp->IsValidLast())
	{
		// Dc1Factory::DeleteObject(m_Last);
		this->SetLast(Dc1Factory::CloneObject(tmp->GetLast()));
	}
	else
	{
		InvalidateLast();
	}
	if (tmp->IsValidVariance())
	{
		// Dc1Factory::DeleteObject(m_Variance);
		this->SetVariance(Dc1Factory::CloneObject(tmp->GetVariance()));
	}
	else
	{
		InvalidateVariance();
	}
	if (tmp->IsValidWeight())
	{
		// Dc1Factory::DeleteObject(m_Weight);
		this->SetWeight(Dc1Factory::CloneObject(tmp->GetWeight()));
	}
	else
	{
		InvalidateWeight();
	}
	if (tmp->IsValidLogBase())
	{
		this->SetLogBase(tmp->GetLogBase());
	}
	else
	{
		InvalidateLogBase();
	}
}

Dc1NodePtr Mp7JrsSeriesOfScalarType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsSeriesOfScalarType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsScalableSeriesType > Mp7JrsSeriesOfScalarType::GetBase() const
{
	return m_Base;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetRaw() const
{
		return m_Raw;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidRaw() const
{
	return m_Raw_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetMin() const
{
		return m_Min;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidMin() const
{
	return m_Min_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetMax() const
{
		return m_Max;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidMax() const
{
	return m_Max_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetMean() const
{
		return m_Mean;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidMean() const
{
	return m_Mean_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetRandom() const
{
		return m_Random;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidRandom() const
{
	return m_Random_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetFirst() const
{
		return m_First;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidFirst() const
{
	return m_First_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetLast() const
{
		return m_Last;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidLast() const
{
	return m_Last_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetVariance() const
{
		return m_Variance;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidVariance() const
{
	return m_Variance_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfScalarType::GetWeight() const
{
		return m_Weight;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidWeight() const
{
	return m_Weight_Exist;
}

float Mp7JrsSeriesOfScalarType::GetLogBase() const
{
		return m_LogBase;
}

// Element is optional
bool Mp7JrsSeriesOfScalarType::IsValidLogBase() const
{
	return m_LogBase_Exist;
}

void Mp7JrsSeriesOfScalarType::SetRaw(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetRaw().");
	}
	if (m_Raw != item || m_Raw_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Raw);
		m_Raw = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Raw) m_Raw->SetParent(m_myself.getPointer());
		m_Raw_Exist = true;
		if(m_Raw != Mp7JrsfloatVectorPtr())
		{
			m_Raw->SetContentName(XMLString::transcode("Raw"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateRaw()
{
	m_Raw_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetMin(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetMin().");
	}
	if (m_Min != item || m_Min_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Min);
		m_Min = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Min) m_Min->SetParent(m_myself.getPointer());
		m_Min_Exist = true;
		if(m_Min != Mp7JrsfloatVectorPtr())
		{
			m_Min->SetContentName(XMLString::transcode("Min"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateMin()
{
	m_Min_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetMax(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetMax().");
	}
	if (m_Max != item || m_Max_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Max);
		m_Max = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max) m_Max->SetParent(m_myself.getPointer());
		m_Max_Exist = true;
		if(m_Max != Mp7JrsfloatVectorPtr())
		{
			m_Max->SetContentName(XMLString::transcode("Max"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateMax()
{
	m_Max_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetMean(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetMean().");
	}
	if (m_Mean != item || m_Mean_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Mean);
		m_Mean = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Mean) m_Mean->SetParent(m_myself.getPointer());
		m_Mean_Exist = true;
		if(m_Mean != Mp7JrsfloatVectorPtr())
		{
			m_Mean->SetContentName(XMLString::transcode("Mean"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateMean()
{
	m_Mean_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetRandom(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetRandom().");
	}
	if (m_Random != item || m_Random_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Random);
		m_Random = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Random) m_Random->SetParent(m_myself.getPointer());
		m_Random_Exist = true;
		if(m_Random != Mp7JrsfloatVectorPtr())
		{
			m_Random->SetContentName(XMLString::transcode("Random"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateRandom()
{
	m_Random_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetFirst(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetFirst().");
	}
	if (m_First != item || m_First_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_First);
		m_First = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_First) m_First->SetParent(m_myself.getPointer());
		m_First_Exist = true;
		if(m_First != Mp7JrsfloatVectorPtr())
		{
			m_First->SetContentName(XMLString::transcode("First"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateFirst()
{
	m_First_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetLast(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetLast().");
	}
	if (m_Last != item || m_Last_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Last);
		m_Last = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Last) m_Last->SetParent(m_myself.getPointer());
		m_Last_Exist = true;
		if(m_Last != Mp7JrsfloatVectorPtr())
		{
			m_Last->SetContentName(XMLString::transcode("Last"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateLast()
{
	m_Last_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetVariance(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetVariance().");
	}
	if (m_Variance != item || m_Variance_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Variance);
		m_Variance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Variance) m_Variance->SetParent(m_myself.getPointer());
		m_Variance_Exist = true;
		if(m_Variance != Mp7JrsfloatVectorPtr())
		{
			m_Variance->SetContentName(XMLString::transcode("Variance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateVariance()
{
	m_Variance_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetWeight().");
	}
	if (m_Weight != item || m_Weight_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Weight);
		m_Weight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Weight) m_Weight->SetParent(m_myself.getPointer());
		m_Weight_Exist = true;
		if(m_Weight != Mp7JrsfloatVectorPtr())
		{
			m_Weight->SetContentName(XMLString::transcode("Weight"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateWeight()
{
	m_Weight_Exist = false;
}
void Mp7JrsSeriesOfScalarType::SetLogBase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetLogBase().");
	}
	if (m_LogBase != item || m_LogBase_Exist == false)
	{
		m_LogBase = item;
		m_LogBase_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfScalarType::InvalidateLogBase()
{
	m_LogBase_Exist = false;
}
unsigned Mp7JrsSeriesOfScalarType::GettotalNumOfSamples() const
{
	return GetBase()->GettotalNumOfSamples();
}

void Mp7JrsSeriesOfScalarType::SettotalNumOfSamples(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SettotalNumOfSamples().");
	}
	GetBase()->SettotalNumOfSamples(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableSeriesType_Scaling_CollectionPtr Mp7JrsSeriesOfScalarType::GetScaling() const
{
	return GetBase()->GetScaling();
}

void Mp7JrsSeriesOfScalarType::SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfScalarType::SetScaling().");
	}
	GetBase()->SetScaling(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSeriesOfScalarType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Raw")) == 0)
	{
		// Raw is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRaw()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRaw(p, client);
					if((p = GetRaw()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Min")) == 0)
	{
		// Min is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMin()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMin(p, client);
					if((p = GetMin()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max")) == 0)
	{
		// Max is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMax()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMax(p, client);
					if((p = GetMax()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Mean")) == 0)
	{
		// Mean is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMean()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMean(p, client);
					if((p = GetMean()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Random")) == 0)
	{
		// Random is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRandom()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRandom(p, client);
					if((p = GetRandom()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("First")) == 0)
	{
		// First is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFirst()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFirst(p, client);
					if((p = GetFirst()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Last")) == 0)
	{
		// Last is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLast()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLast(p, client);
					if((p = GetLast()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Variance")) == 0)
	{
		// Variance is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVariance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVariance(p, client);
					if((p = GetVariance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Weight")) == 0)
	{
		// Weight is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWeight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWeight(p, client);
					if((p = GetWeight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SeriesOfScalarType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SeriesOfScalarType");
	}
	return result;
}

XMLCh * Mp7JrsSeriesOfScalarType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSeriesOfScalarType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSeriesOfScalarType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SeriesOfScalarType"));
	// Element serialization:
	if (m_Raw != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Raw->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Min != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Min->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Max != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Max->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Mean != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Mean->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Random != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Random->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_First != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_First->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Last != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Last->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Variance != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Variance->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Weight != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Weight->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_LogBase_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_LogBase);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("LogBase"), true);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsSeriesOfScalarType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsScalableSeriesType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Raw")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRaw(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Min")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMin(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Max")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMax(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Mean")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMean(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Random")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRandom(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("First")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFirst(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Last")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLast(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Variance")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVariance(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Weight")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWeight(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("LogBase"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetLogBase(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSeriesOfScalarType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Raw")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetRaw(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Min")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetMin(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Max")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetMax(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Mean")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetMean(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Random")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetRandom(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("First")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetFirst(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Last")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetLast(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Variance")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetVariance(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Weight")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetWeight(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSeriesOfScalarType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRaw() != Dc1NodePtr())
		result.Insert(GetRaw());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetRandom() != Dc1NodePtr())
		result.Insert(GetRandom());
	if (GetFirst() != Dc1NodePtr())
		result.Insert(GetFirst());
	if (GetLast() != Dc1NodePtr())
		result.Insert(GetLast());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetWeight() != Dc1NodePtr())
		result.Insert(GetWeight());
	if (GetScaling() != Dc1NodePtr())
		result.Insert(GetScaling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSeriesOfScalarType_ExtMethodImpl.h


