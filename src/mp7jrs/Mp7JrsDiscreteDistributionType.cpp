
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtImplInclude.h


#include "Mp7JrsProbabilityDistributionType.h"
#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7JrsProbabilityDistributionType_Moment_CollectionType.h"
#include "Mp7JrsProbabilityDistributionType_Cumulant_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDiscreteDistributionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsProbabilityDistributionType_Moment_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Moment
#include "Mp7JrsProbabilityDistributionType_Cumulant_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Cumulant

#include <assert.h>
IMp7JrsDiscreteDistributionType::IMp7JrsDiscreteDistributionType()
{

// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtPropInit.h

}

IMp7JrsDiscreteDistributionType::~IMp7JrsDiscreteDistributionType()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtPropCleanup.h

}

Mp7JrsDiscreteDistributionType::Mp7JrsDiscreteDistributionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDiscreteDistributionType::~Mp7JrsDiscreteDistributionType()
{
	Cleanup();
}

void Mp7JrsDiscreteDistributionType::Init()
{
	// Init base
	m_Base = CreateProbabilityDistributionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMyPropInit.h

}

void Mp7JrsDiscreteDistributionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsDiscreteDistributionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DiscreteDistributionTypePtr, since we
	// might need GetBase(), which isn't defined in IDiscreteDistributionType
	const Dc1Ptr< Mp7JrsDiscreteDistributionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsProbabilityDistributionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDiscreteDistributionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrsDiscreteDistributionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDiscreteDistributionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsProbabilityDistributionType > Mp7JrsDiscreteDistributionType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsDiscreteDistributionType::Getdim() const
{
	return GetBase()->Getdim();
}

bool Mp7JrsDiscreteDistributionType::Existdim() const
{
	return GetBase()->Existdim();
}
void Mp7JrsDiscreteDistributionType::Setdim(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::Setdim().");
	}
	GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::Invalidatedim()
{
	GetBase()->Invalidatedim();
}
Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetMean() const
{
	return GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidMean() const
{
	return GetBase()->IsValidMean();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetVariance() const
{
	return GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidVariance() const
{
	return GetBase()->IsValidVariance();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetMin() const
{
	return GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidMin() const
{
	return GetBase()->IsValidMin();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetMax() const
{
	return GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidMax() const
{
	return GetBase()->IsValidMax();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetMode() const
{
	return GetBase()->GetMode();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidMode() const
{
	return GetBase()->IsValidMode();
}

Mp7JrsDoubleMatrixPtr Mp7JrsDiscreteDistributionType::GetMedian() const
{
	return GetBase()->GetMedian();
}

// Element is optional
bool Mp7JrsDiscreteDistributionType::IsValidMedian() const
{
	return GetBase()->IsValidMedian();
}

Mp7JrsProbabilityDistributionType_Moment_CollectionPtr Mp7JrsDiscreteDistributionType::GetMoment() const
{
	return GetBase()->GetMoment();
}

Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr Mp7JrsDiscreteDistributionType::GetCumulant() const
{
	return GetBase()->GetCumulant();
}

void Mp7JrsDiscreteDistributionType::SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMean().");
	}
	GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateMean()
{
	GetBase()->InvalidateMean();
}
void Mp7JrsDiscreteDistributionType::SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetVariance().");
	}
	GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateVariance()
{
	GetBase()->InvalidateVariance();
}
void Mp7JrsDiscreteDistributionType::SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMin().");
	}
	GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateMin()
{
	GetBase()->InvalidateMin();
}
void Mp7JrsDiscreteDistributionType::SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMax().");
	}
	GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateMax()
{
	GetBase()->InvalidateMax();
}
void Mp7JrsDiscreteDistributionType::SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMode().");
	}
	GetBase()->SetMode(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateMode()
{
	GetBase()->InvalidateMode();
}
void Mp7JrsDiscreteDistributionType::SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMedian().");
	}
	GetBase()->SetMedian(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidateMedian()
{
	GetBase()->InvalidateMedian();
}
void Mp7JrsDiscreteDistributionType::SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetMoment().");
	}
	GetBase()->SetMoment(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetCumulant().");
	}
	GetBase()->SetCumulant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsDiscreteDistributionType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsDiscreteDistributionType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsDiscreteDistributionType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsDiscreteDistributionType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsDiscreteDistributionType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsDiscreteDistributionType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsDiscreteDistributionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsDiscreteDistributionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsDiscreteDistributionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteDistributionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsDiscreteDistributionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsDiscreteDistributionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsDiscreteDistributionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsDiscreteDistributionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsDiscreteDistributionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsDiscreteDistributionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsDiscreteDistributionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsDiscreteDistributionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsDiscreteDistributionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsDiscreteDistributionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsDiscreteDistributionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDiscreteDistributionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDiscreteDistributionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsDiscreteDistributionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDiscreteDistributionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDiscreteDistributionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DiscreteDistributionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DiscreteDistributionType");
	}
	return result;
}

XMLCh * Mp7JrsDiscreteDistributionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDiscreteDistributionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDiscreteDistributionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DiscreteDistributionType"));
	// Element serialization:

}

bool Mp7JrsDiscreteDistributionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsProbabilityDistributionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDiscreteDistributionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDiscreteDistributionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMode() != Dc1NodePtr())
		result.Insert(GetMode());
	if (GetMedian() != Dc1NodePtr())
		result.Insert(GetMedian());
	if (GetMoment() != Dc1NodePtr())
		result.Insert(GetMoment());
	if (GetCumulant() != Dc1NodePtr())
		result.Insert(GetCumulant());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDiscreteDistributionType_ExtMethodImpl.h


