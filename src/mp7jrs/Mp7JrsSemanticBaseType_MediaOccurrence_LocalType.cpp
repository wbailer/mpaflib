
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtImplInclude.h


#include "Mp7JrsSemanticBaseType_MediaOccurrence_type_LocalType2.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsMaskType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AudioDescriptor
#include "Mp7JrsAudioDSType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme
#include "Mp7JrsVisualDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme

#include <assert.h>
IMp7JrsSemanticBaseType_MediaOccurrence_LocalType::IMp7JrsSemanticBaseType_MediaOccurrence_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtPropInit.h

}

IMp7JrsSemanticBaseType_MediaOccurrence_LocalType::~IMp7JrsSemanticBaseType_MediaOccurrence_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtPropCleanup.h

}

Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Mp7JrsSemanticBaseType_MediaOccurrence_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::~Mp7JrsSemanticBaseType_MediaOccurrence_LocalType()
{
	Cleanup();
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Init()
{

	// Init attributes
	m_type = Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr(); // Create emtpy class ptr
	m_type_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_MediaInformation = Mp7JrsMediaInformationPtr(); // Class
	m_MediaInformationRef = Mp7JrsReferencePtr(); // Class
	m_MediaLocator = Mp7JrsMediaLocatorPtr(); // Class
	m_Mask = Dc1Ptr< Dc1Node >(); // Class
	m_Mask_Exist = false;
	m_AudioDescriptor = Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr(); // Collection
	m_AudioDescriptionScheme = Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr(); // Collection
	m_VisualDescriptor = Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr(); // Collection
	m_VisualDescriptionScheme = Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMyPropInit.h

}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_MediaInformation);
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	// Dc1Factory::DeleteObject(m_MediaLocator);
	// Dc1Factory::DeleteObject(m_Mask);
	// Dc1Factory::DeleteObject(m_AudioDescriptor);
	// Dc1Factory::DeleteObject(m_AudioDescriptionScheme);
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticBaseType_MediaOccurrence_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticBaseType_MediaOccurrence_LocalType
	const Dc1Ptr< Mp7JrsSemanticBaseType_MediaOccurrence_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
		// Dc1Factory::DeleteObject(m_MediaInformation);
		this->SetMediaInformation(Dc1Factory::CloneObject(tmp->GetMediaInformation()));
		// Dc1Factory::DeleteObject(m_MediaInformationRef);
		this->SetMediaInformationRef(Dc1Factory::CloneObject(tmp->GetMediaInformationRef()));
		// Dc1Factory::DeleteObject(m_MediaLocator);
		this->SetMediaLocator(Dc1Factory::CloneObject(tmp->GetMediaLocator()));
	if (tmp->IsValidMask())
	{
		// Dc1Factory::DeleteObject(m_Mask);
		this->SetMask(Dc1Factory::CloneObject(tmp->GetMask()));
	}
	else
	{
		InvalidateMask();
	}
		// Dc1Factory::DeleteObject(m_AudioDescriptor);
		this->SetAudioDescriptor(Dc1Factory::CloneObject(tmp->GetAudioDescriptor()));
		// Dc1Factory::DeleteObject(m_AudioDescriptionScheme);
		this->SetAudioDescriptionScheme(Dc1Factory::CloneObject(tmp->GetAudioDescriptionScheme()));
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		this->SetVisualDescriptor(Dc1Factory::CloneObject(tmp->GetVisualDescriptor()));
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		this->SetVisualDescriptionScheme(Dc1Factory::CloneObject(tmp->GetVisualDescriptionScheme()));
}

Dc1NodePtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Gettype() const
{
	if (this->Existtype()) {
		return m_type;
	} else {
		return m_type_Default;
	}
}

bool Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Settype(const Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrsMediaInformationPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetMediaInformation() const
{
		return m_MediaInformation;
}

Mp7JrsReferencePtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetMediaInformationRef() const
{
		return m_MediaInformationRef;
}

Mp7JrsMediaLocatorPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetMediaLocator() const
{
		return m_MediaLocator;
}

Dc1Ptr< Dc1Node > Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetMask() const
{
		return m_Mask;
}

// Element is optional
bool Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::IsValidMask() const
{
	return m_Mask_Exist;
}

Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetAudioDescriptor() const
{
		return m_AudioDescriptor;
}

Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetAudioDescriptionScheme() const
{
		return m_AudioDescriptionScheme;
}

Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetVisualDescriptor() const
{
		return m_VisualDescriptor;
}

Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetVisualDescriptionScheme() const
{
		return m_VisualDescriptionScheme;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaInformation().");
	}
	if (m_MediaInformation != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInformation);
		m_MediaInformation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInformation) m_MediaInformation->SetParent(m_myself.getPointer());
		if (m_MediaInformation && m_MediaInformation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaInformation->UseTypeAttribute = true;
		}
		if(m_MediaInformation != Mp7JrsMediaInformationPtr())
		{
			m_MediaInformation->SetContentName(XMLString::transcode("MediaInformation"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	m_MediaInformationRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MediaLocator);
	m_MediaLocator = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaInformationRef().");
	}
	if (m_MediaInformationRef != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInformationRef);
		m_MediaInformationRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInformationRef) m_MediaInformationRef->SetParent(m_myself.getPointer());
		if (m_MediaInformationRef && m_MediaInformationRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaInformationRef->UseTypeAttribute = true;
		}
		if(m_MediaInformationRef != Mp7JrsReferencePtr())
		{
			m_MediaInformationRef->SetContentName(XMLString::transcode("MediaInformationRef"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformation);
	m_MediaInformation = Mp7JrsMediaInformationPtr();
	// Dc1Factory::DeleteObject(m_MediaLocator);
	m_MediaLocator = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMediaLocator().");
	}
	if (m_MediaLocator != item)
	{
		// Dc1Factory::DeleteObject(m_MediaLocator);
		m_MediaLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaLocator) m_MediaLocator->SetParent(m_myself.getPointer());
		if (m_MediaLocator && m_MediaLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaLocator->UseTypeAttribute = true;
		}
		if(m_MediaLocator != Mp7JrsMediaLocatorPtr())
		{
			m_MediaLocator->SetContentName(XMLString::transcode("MediaLocator"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformation);
	m_MediaInformation = Mp7JrsMediaInformationPtr();
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	m_MediaInformationRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMask(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetMask().");
	}
	if (m_Mask != item || m_Mask_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Mask);
		m_Mask = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Mask) m_Mask->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Mask) m_Mask->UseTypeAttribute = true;
		m_Mask_Exist = true;
		if(m_Mask != Dc1Ptr< Dc1Node >())
		{
			m_Mask->SetContentName(XMLString::transcode("Mask"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::InvalidateMask()
{
	m_Mask_Exist = false;
}
void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetAudioDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetAudioDescriptor().");
	}
	if (m_AudioDescriptor != item)
	{
		// Dc1Factory::DeleteObject(m_AudioDescriptor);
		m_AudioDescriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioDescriptor) m_AudioDescriptor->SetParent(m_myself.getPointer());
		if(m_AudioDescriptor != Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr())
		{
			m_AudioDescriptor->SetContentName(XMLString::transcode("AudioDescriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetAudioDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetAudioDescriptionScheme().");
	}
	if (m_AudioDescriptionScheme != item)
	{
		// Dc1Factory::DeleteObject(m_AudioDescriptionScheme);
		m_AudioDescriptionScheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioDescriptionScheme) m_AudioDescriptionScheme->SetParent(m_myself.getPointer());
		if(m_AudioDescriptionScheme != Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr())
		{
			m_AudioDescriptionScheme->SetContentName(XMLString::transcode("AudioDescriptionScheme"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetVisualDescriptor(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetVisualDescriptor().");
	}
	if (m_VisualDescriptor != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		m_VisualDescriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptor) m_VisualDescriptor->SetParent(m_myself.getPointer());
		if(m_VisualDescriptor != Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr())
		{
			m_VisualDescriptor->SetContentName(XMLString::transcode("VisualDescriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetVisualDescriptionScheme(const Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::SetVisualDescriptionScheme().");
	}
	if (m_VisualDescriptionScheme != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		m_VisualDescriptionScheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptionScheme) m_VisualDescriptionScheme->SetParent(m_myself.getPointer());
		if(m_VisualDescriptionScheme != Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr())
		{
			m_VisualDescriptionScheme->SetContentName(XMLString::transcode("VisualDescriptionScheme"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute SemanticBaseType_MediaOccurrence_type_LocalType2
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaInformation")) == 0)
	{
		// MediaInformation is simple element MediaInformationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaInformation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaInformationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaInformation(p, client);
					if((p = GetMediaInformation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaInformationRef")) == 0)
	{
		// MediaInformationRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaInformationRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaInformationRef(p, client);
					if((p = GetMediaInformationRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaLocator")) == 0)
	{
		// MediaLocator is simple element MediaLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaLocator(p, client);
					if((p = GetMediaLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Mask")) == 0)
	{
		// Mask is simple abstract element MaskType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMask()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMaskPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMask(p, client);
					if((p = GetMask()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioDescriptor")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AudioDescriptor is item of abstract type AudioDType
		// in element collection SemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr coll = GetAudioDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType; // FTT, check this
				SetAudioDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAudioDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioDescriptionScheme")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AudioDescriptionScheme is item of abstract type AudioDSType
		// in element collection SemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr coll = GetAudioDescriptionScheme();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType; // FTT, check this
				SetAudioDescriptionScheme(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAudioDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptor")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VisualDescriptor is item of abstract type VisualDType
		// in element collection SemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr coll = GetVisualDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType; // FTT, check this
				SetVisualDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptionScheme")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme is item of abstract type VisualDSType
		// in element collection SemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr coll = GetVisualDescriptionScheme();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType; // FTT, check this
				SetVisualDescriptionScheme(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticBaseType_MediaOccurrence_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticBaseType_MediaOccurrence_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticBaseType_MediaOccurrence_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Class
	if (m_type != Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_MediaInformation != Mp7JrsMediaInformationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaInformation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaInformation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaInformation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaInformationRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaInformationRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaInformationRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaInformationRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaLocator != Mp7JrsMediaLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Mask != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Mask->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Mask"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Mask->UseTypeAttribute = true;
		}
		m_Mask->Serialize(doc, element, &elem);
	}
	if (m_AudioDescriptor != Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioDescriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_AudioDescriptionScheme != Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioDescriptionScheme->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_VisualDescriptor != Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VisualDescriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_VisualDescriptionScheme != Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VisualDescriptionScheme->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize class type
		Mp7JrsSemanticBaseType_MediaOccurrence_type_Local2Ptr tmp = CreateSemanticBaseType_MediaOccurrence_type_LocalType2; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaInformation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaInformation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaInformationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaInformation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaInformationRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaInformationRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaInformationRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Mask"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Mask")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMaskType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMask(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AudioDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AudioDescriptor")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AudioDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AudioDescriptionScheme")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioDescriptionScheme(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VisualDescriptor")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVisualDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VisualDescriptionScheme")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVisualDescriptionScheme(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaInformation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaInformationType; // FTT, check this
	}
	this->SetMediaInformation(child);
  }
  if (XMLString::compareString(elementname, X("MediaInformationRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetMediaInformationRef(child);
  }
  if (XMLString::compareString(elementname, X("MediaLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaLocatorType; // FTT, check this
	}
	this->SetMediaLocator(child);
  }
  if (XMLString::compareString(elementname, X("Mask")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMaskType; // FTT, check this
	}
	this->SetMask(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AudioDescriptor")) == 0))
  {
	Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_AudioDescriptor_CollectionType; // FTT, check this
	this->SetAudioDescriptor(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AudioDescriptionScheme")) == 0))
  {
	Mp7JrsSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_AudioDescriptionScheme_CollectionType; // FTT, check this
	this->SetAudioDescriptionScheme(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VisualDescriptor")) == 0))
  {
	Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_VisualDescriptor_CollectionType; // FTT, check this
	this->SetVisualDescriptor(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0))
  {
	Mp7JrsSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_VisualDescriptionScheme_CollectionType; // FTT, check this
	this->SetVisualDescriptionScheme(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticBaseType_MediaOccurrence_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMask() != Dc1NodePtr())
		result.Insert(GetMask());
	if (GetAudioDescriptor() != Dc1NodePtr())
		result.Insert(GetAudioDescriptor());
	if (GetAudioDescriptionScheme() != Dc1NodePtr())
		result.Insert(GetAudioDescriptionScheme());
	if (GetVisualDescriptor() != Dc1NodePtr())
		result.Insert(GetVisualDescriptor());
	if (GetVisualDescriptionScheme() != Dc1NodePtr())
		result.Insert(GetVisualDescriptionScheme());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_MediaOccurrence_LocalType_ExtMethodImpl.h


