
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsLinguisticEntityType_start_LocalType.h"
#include "Mp7JrsLinguisticEntityType_length_LocalType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrstermReferenceListType.h"
#include "Mp7JrsLinguisticEntityType_edit_LocalType.h"
#include "Mp7JrsLinguisticEntityType_MediaLocator_CollectionType.h"
#include "Mp7JrsLinguisticEntityType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsLinguisticEntityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaLocator
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation

#include <assert.h>
IMp7JrsLinguisticEntityType::IMp7JrsLinguisticEntityType()
{

// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtPropInit.h

}

IMp7JrsLinguisticEntityType::~IMp7JrsLinguisticEntityType()
{
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtPropCleanup.h

}

Mp7JrsLinguisticEntityType::Mp7JrsLinguisticEntityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsLinguisticEntityType::~Mp7JrsLinguisticEntityType()
{
	Cleanup();
}

void Mp7JrsLinguisticEntityType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;
	m_start = Mp7JrsLinguisticEntityType_start_LocalPtr(); // Create emtpy class ptr
	m_start_Exist = false;
	m_length = Mp7JrsLinguisticEntityType_length_LocalPtr(); // Create emtpy class ptr
	m_length_Exist = false;
	m_type = Mp7JrstermReferencePtr(); // Create emtpy class ptr
	m_type_Exist = false;
	m_depend = Mp7JrstermReferencePtr(); // Create emtpy class ptr
	m_depend_Exist = false;
	m_equal = Mp7JrstermReferenceListPtr(); // Collection
	m_equal_Exist = false;
	m_semantics = Mp7JrstermReferenceListPtr(); // Collection
	m_semantics_Exist = false;
	m_compoundSemantics = Mp7JrstermReferenceListPtr(); // Collection
	m_compoundSemantics_Exist = false;
	m_operator = Mp7JrstermReferenceListPtr(); // Collection
	m_operator_Exist = false;
	m_copy = Mp7JrstermReferenceListPtr(); // Collection
	m_copy_Exist = false;
	m_noCopy = Mp7JrstermReferenceListPtr(); // Collection
	m_noCopy_Exist = false;
	m_substitute = Mp7JrstermReferencePtr(); // Create emtpy class ptr
	m_substitute_Exist = false;
	m_inScope = Mp7JrstermReferencePtr(); // Create emtpy class ptr
	m_inScope_Exist = false;
	m_edit = Mp7JrsLinguisticEntityType_edit_LocalPtr(); // Pattern
	m_edit_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_MediaLocator = Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr(); // Collection
	m_Relation = Mp7JrsLinguisticEntityType_Relation_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMyPropInit.h

}

void Mp7JrsLinguisticEntityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_start);
	// Dc1Factory::DeleteObject(m_length);
	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_depend);
	// Dc1Factory::DeleteObject(m_equal);
	// Dc1Factory::DeleteObject(m_semantics);
	// Dc1Factory::DeleteObject(m_compoundSemantics);
	// Dc1Factory::DeleteObject(m_operator);
	// Dc1Factory::DeleteObject(m_copy);
	// Dc1Factory::DeleteObject(m_noCopy);
	// Dc1Factory::DeleteObject(m_substitute);
	// Dc1Factory::DeleteObject(m_inScope);
	// Dc1Factory::DeleteObject(m_edit); // Pattern
	// Dc1Factory::DeleteObject(m_MediaLocator);
	// Dc1Factory::DeleteObject(m_Relation);
}

void Mp7JrsLinguisticEntityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use LinguisticEntityTypePtr, since we
	// might need GetBase(), which isn't defined in ILinguisticEntityType
	const Dc1Ptr< Mp7JrsLinguisticEntityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsLinguisticEntityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_start);
	if (tmp->Existstart())
	{
		this->Setstart(Dc1Factory::CloneObject(tmp->Getstart()));
	}
	else
	{
		Invalidatestart();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_length);
	if (tmp->Existlength())
	{
		this->Setlength(Dc1Factory::CloneObject(tmp->Getlength()));
	}
	else
	{
		Invalidatelength();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_depend);
	if (tmp->Existdepend())
	{
		this->Setdepend(Dc1Factory::CloneObject(tmp->Getdepend()));
	}
	else
	{
		Invalidatedepend();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_equal);
	if (tmp->Existequal())
	{
		this->Setequal(Dc1Factory::CloneObject(tmp->Getequal()));
	}
	else
	{
		Invalidateequal();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_semantics);
	if (tmp->Existsemantics())
	{
		this->Setsemantics(Dc1Factory::CloneObject(tmp->Getsemantics()));
	}
	else
	{
		Invalidatesemantics();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_compoundSemantics);
	if (tmp->ExistcompoundSemantics())
	{
		this->SetcompoundSemantics(Dc1Factory::CloneObject(tmp->GetcompoundSemantics()));
	}
	else
	{
		InvalidatecompoundSemantics();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_operator);
	if (tmp->Existoperator())
	{
		this->Setoperator(Dc1Factory::CloneObject(tmp->Getoperator()));
	}
	else
	{
		Invalidateoperator();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_copy);
	if (tmp->Existcopy())
	{
		this->Setcopy(Dc1Factory::CloneObject(tmp->Getcopy()));
	}
	else
	{
		Invalidatecopy();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_noCopy);
	if (tmp->ExistnoCopy())
	{
		this->SetnoCopy(Dc1Factory::CloneObject(tmp->GetnoCopy()));
	}
	else
	{
		InvalidatenoCopy();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_substitute);
	if (tmp->Existsubstitute())
	{
		this->Setsubstitute(Dc1Factory::CloneObject(tmp->Getsubstitute()));
	}
	else
	{
		Invalidatesubstitute();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_inScope);
	if (tmp->ExistinScope())
	{
		this->SetinScope(Dc1Factory::CloneObject(tmp->GetinScope()));
	}
	else
	{
		InvalidateinScope();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_edit); // Pattern
	if (tmp->Existedit())
	{
		this->Setedit(Dc1Factory::CloneObject(tmp->Getedit()));
	}
	else
	{
		Invalidateedit();
	}
	}
		// Dc1Factory::DeleteObject(m_MediaLocator);
		this->SetMediaLocator(Dc1Factory::CloneObject(tmp->GetMediaLocator()));
		// Dc1Factory::DeleteObject(m_Relation);
		this->SetRelation(Dc1Factory::CloneObject(tmp->GetRelation()));
}

Dc1NodePtr Mp7JrsLinguisticEntityType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsLinguisticEntityType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsLinguisticEntityType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsLinguisticEntityType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsLinguisticEntityType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsLinguisticEntityType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsLinguisticEntityType_start_LocalPtr Mp7JrsLinguisticEntityType::Getstart() const
{
	return m_start;
}

bool Mp7JrsLinguisticEntityType::Existstart() const
{
	return m_start_Exist;
}
void Mp7JrsLinguisticEntityType::Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setstart().");
	}
	m_start = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_start) m_start->SetParent(m_myself.getPointer());
	m_start_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatestart()
{
	m_start_Exist = false;
}
Mp7JrsLinguisticEntityType_length_LocalPtr Mp7JrsLinguisticEntityType::Getlength() const
{
	return m_length;
}

bool Mp7JrsLinguisticEntityType::Existlength() const
{
	return m_length_Exist;
}
void Mp7JrsLinguisticEntityType::Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setlength().");
	}
	m_length = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_length) m_length->SetParent(m_myself.getPointer());
	m_length_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatelength()
{
	m_length_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsLinguisticEntityType::Gettype() const
{
	return m_type;
}

bool Mp7JrsLinguisticEntityType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsLinguisticEntityType::Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsLinguisticEntityType::Getdepend() const
{
	return m_depend;
}

bool Mp7JrsLinguisticEntityType::Existdepend() const
{
	return m_depend_Exist;
}
void Mp7JrsLinguisticEntityType::Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setdepend().");
	}
	m_depend = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_depend) m_depend->SetParent(m_myself.getPointer());
	m_depend_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatedepend()
{
	m_depend_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::Getequal() const
{
	return m_equal;
}

bool Mp7JrsLinguisticEntityType::Existequal() const
{
	return m_equal_Exist;
}
void Mp7JrsLinguisticEntityType::Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setequal().");
	}
	m_equal = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_equal) m_equal->SetParent(m_myself.getPointer());
	m_equal_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidateequal()
{
	m_equal_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::Getsemantics() const
{
	return m_semantics;
}

bool Mp7JrsLinguisticEntityType::Existsemantics() const
{
	return m_semantics_Exist;
}
void Mp7JrsLinguisticEntityType::Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setsemantics().");
	}
	m_semantics = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_semantics) m_semantics->SetParent(m_myself.getPointer());
	m_semantics_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatesemantics()
{
	m_semantics_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::GetcompoundSemantics() const
{
	return m_compoundSemantics;
}

bool Mp7JrsLinguisticEntityType::ExistcompoundSemantics() const
{
	return m_compoundSemantics_Exist;
}
void Mp7JrsLinguisticEntityType::SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetcompoundSemantics().");
	}
	m_compoundSemantics = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_compoundSemantics) m_compoundSemantics->SetParent(m_myself.getPointer());
	m_compoundSemantics_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatecompoundSemantics()
{
	m_compoundSemantics_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::Getoperator() const
{
	return m_operator;
}

bool Mp7JrsLinguisticEntityType::Existoperator() const
{
	return m_operator_Exist;
}
void Mp7JrsLinguisticEntityType::Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setoperator().");
	}
	m_operator = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_operator) m_operator->SetParent(m_myself.getPointer());
	m_operator_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidateoperator()
{
	m_operator_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::Getcopy() const
{
	return m_copy;
}

bool Mp7JrsLinguisticEntityType::Existcopy() const
{
	return m_copy_Exist;
}
void Mp7JrsLinguisticEntityType::Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setcopy().");
	}
	m_copy = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_copy) m_copy->SetParent(m_myself.getPointer());
	m_copy_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatecopy()
{
	m_copy_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsLinguisticEntityType::GetnoCopy() const
{
	return m_noCopy;
}

bool Mp7JrsLinguisticEntityType::ExistnoCopy() const
{
	return m_noCopy_Exist;
}
void Mp7JrsLinguisticEntityType::SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetnoCopy().");
	}
	m_noCopy = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_noCopy) m_noCopy->SetParent(m_myself.getPointer());
	m_noCopy_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatenoCopy()
{
	m_noCopy_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsLinguisticEntityType::Getsubstitute() const
{
	return m_substitute;
}

bool Mp7JrsLinguisticEntityType::Existsubstitute() const
{
	return m_substitute_Exist;
}
void Mp7JrsLinguisticEntityType::Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setsubstitute().");
	}
	m_substitute = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_substitute) m_substitute->SetParent(m_myself.getPointer());
	m_substitute_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidatesubstitute()
{
	m_substitute_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsLinguisticEntityType::GetinScope() const
{
	return m_inScope;
}

bool Mp7JrsLinguisticEntityType::ExistinScope() const
{
	return m_inScope_Exist;
}
void Mp7JrsLinguisticEntityType::SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetinScope().");
	}
	m_inScope = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_inScope) m_inScope->SetParent(m_myself.getPointer());
	m_inScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidateinScope()
{
	m_inScope_Exist = false;
}
Mp7JrsLinguisticEntityType_edit_LocalPtr Mp7JrsLinguisticEntityType::Getedit() const
{
	return m_edit;
}

bool Mp7JrsLinguisticEntityType::Existedit() const
{
	return m_edit_Exist;
}
void Mp7JrsLinguisticEntityType::Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setedit().");
	}
	m_edit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_edit) m_edit->SetParent(m_myself.getPointer());
	m_edit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidateedit()
{
	m_edit_Exist = false;
}
Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr Mp7JrsLinguisticEntityType::GetMediaLocator() const
{
		return m_MediaLocator;
}

Mp7JrsLinguisticEntityType_Relation_CollectionPtr Mp7JrsLinguisticEntityType::GetRelation() const
{
		return m_Relation;
}

void Mp7JrsLinguisticEntityType::SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetMediaLocator().");
	}
	if (m_MediaLocator != item)
	{
		// Dc1Factory::DeleteObject(m_MediaLocator);
		m_MediaLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaLocator) m_MediaLocator->SetParent(m_myself.getPointer());
		if(m_MediaLocator != Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr())
		{
			m_MediaLocator->SetContentName(XMLString::transcode("MediaLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetRelation().");
	}
	if (m_Relation != item)
	{
		// Dc1Factory::DeleteObject(m_Relation);
		m_Relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relation) m_Relation->SetParent(m_myself.getPointer());
		if(m_Relation != Mp7JrsLinguisticEntityType_Relation_CollectionPtr())
		{
			m_Relation->SetContentName(XMLString::transcode("Relation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsLinguisticEntityType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsLinguisticEntityType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsLinguisticEntityType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsLinguisticEntityType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsLinguisticEntityType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsLinguisticEntityType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsLinguisticEntityType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsLinguisticEntityType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsLinguisticEntityType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsLinguisticEntityType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsLinguisticEntityType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsLinguisticEntityType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsLinguisticEntityType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsLinguisticEntityType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsLinguisticEntityType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsLinguisticEntityType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsLinguisticEntityType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsLinguisticEntityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  14, 19 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("start")) == 0)
	{
		// start is simple attribute LinguisticEntityType_start_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsLinguisticEntityType_start_LocalPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("length")) == 0)
	{
		// length is simple attribute LinguisticEntityType_length_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsLinguisticEntityType_length_LocalPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("depend")) == 0)
	{
		// depend is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("equal")) == 0)
	{
		// equal is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("semantics")) == 0)
	{
		// semantics is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("compoundSemantics")) == 0)
	{
		// compoundSemantics is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("operator")) == 0)
	{
		// operator is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("copy")) == 0)
	{
		// copy is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("noCopy")) == 0)
	{
		// noCopy is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("substitute")) == 0)
	{
		// substitute is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("inScope")) == 0)
	{
		// inScope is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("edit")) == 0)
	{
		// edit is simple attribute LinguisticEntityType_edit_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsLinguisticEntityType_edit_LocalPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaLocator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaLocator is item of type MediaLocatorType
		// in element collection LinguisticEntityType_MediaLocator_CollectionType
		
		context->Found = true;
		Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr coll = GetMediaLocator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateLinguisticEntityType_MediaLocator_CollectionType; // FTT, check this
				SetMediaLocator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Relation is item of type RelationType
		// in element collection LinguisticEntityType_Relation_CollectionType
		
		context->Found = true;
		Mp7JrsLinguisticEntityType_Relation_CollectionPtr coll = GetRelation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateLinguisticEntityType_Relation_CollectionType; // FTT, check this
				SetRelation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for LinguisticEntityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "LinguisticEntityType");
	}
	return result;
}

XMLCh * Mp7JrsLinguisticEntityType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsLinguisticEntityType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsLinguisticEntityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("LinguisticEntityType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_start_Exist)
	{
	// Class
	if (m_start != Mp7JrsLinguisticEntityType_start_LocalPtr())
	{
		XMLCh * tmp = m_start->ToText();
		element->setAttributeNS(X(""), X("start"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_length_Exist)
	{
	// Class
	if (m_length != Mp7JrsLinguisticEntityType_length_LocalPtr())
	{
		XMLCh * tmp = m_length->ToText();
		element->setAttributeNS(X(""), X("length"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Class
	if (m_type != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_depend_Exist)
	{
	// Class
	if (m_depend != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_depend->ToText();
		element->setAttributeNS(X(""), X("depend"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_equal_Exist)
	{
	// Collection
	if(m_equal != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_equal->ToText();
		element->setAttributeNS(X(""), X("equal"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_semantics_Exist)
	{
	// Collection
	if(m_semantics != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_semantics->ToText();
		element->setAttributeNS(X(""), X("semantics"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_compoundSemantics_Exist)
	{
	// Collection
	if(m_compoundSemantics != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_compoundSemantics->ToText();
		element->setAttributeNS(X(""), X("compoundSemantics"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_operator_Exist)
	{
	// Collection
	if(m_operator != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_operator->ToText();
		element->setAttributeNS(X(""), X("operator"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_copy_Exist)
	{
	// Collection
	if(m_copy != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_copy->ToText();
		element->setAttributeNS(X(""), X("copy"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_noCopy_Exist)
	{
	// Collection
	if(m_noCopy != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_noCopy->ToText();
		element->setAttributeNS(X(""), X("noCopy"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_substitute_Exist)
	{
	// Class
	if (m_substitute != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_substitute->ToText();
		element->setAttributeNS(X(""), X("substitute"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_inScope_Exist)
	{
	// Class
	if (m_inScope != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_inScope->ToText();
		element->setAttributeNS(X(""), X("inScope"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_edit_Exist)
	{
	// Pattern
	if (m_edit != Mp7JrsLinguisticEntityType_edit_LocalPtr())
	{
		XMLCh * tmp = m_edit->ToText();
		element->setAttributeNS(X(""), X("edit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_MediaLocator != Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaLocator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Relation != Mp7JrsLinguisticEntityType_Relation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relation->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsLinguisticEntityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("start")))
	{
		// Deserialize class type
		Mp7JrsLinguisticEntityType_start_LocalPtr tmp = CreateLinguisticEntityType_start_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("start")));
		this->Setstart(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("length")))
	{
		// Deserialize class type
		Mp7JrsLinguisticEntityType_length_LocalPtr tmp = CreateLinguisticEntityType_length_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("length")));
		this->Setlength(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("depend")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("depend")));
		this->Setdepend(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("equal")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("equal")));
		this->Setequal(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("semantics")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("semantics")));
		this->Setsemantics(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("compoundSemantics")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("compoundSemantics")));
		this->SetcompoundSemantics(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("operator")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("operator")));
		this->Setoperator(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("copy")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("copy")));
		this->Setcopy(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("noCopy")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("noCopy")));
		this->SetnoCopy(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("substitute")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("substitute")));
		this->Setsubstitute(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("inScope")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("inScope")));
		this->SetinScope(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("edit")))
	{
		Mp7JrsLinguisticEntityType_edit_LocalPtr tmp = CreateLinguisticEntityType_edit_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("edit")));
		this->Setedit(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaLocator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaLocator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr tmp = CreateLinguisticEntityType_MediaLocator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaLocator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsLinguisticEntityType_Relation_CollectionPtr tmp = CreateLinguisticEntityType_Relation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsLinguisticEntityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaLocator")) == 0))
  {
	Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr tmp = CreateLinguisticEntityType_MediaLocator_CollectionType; // FTT, check this
	this->SetMediaLocator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relation")) == 0))
  {
	Mp7JrsLinguisticEntityType_Relation_CollectionPtr tmp = CreateLinguisticEntityType_Relation_CollectionType; // FTT, check this
	this->SetRelation(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsLinguisticEntityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_ExtMethodImpl.h


