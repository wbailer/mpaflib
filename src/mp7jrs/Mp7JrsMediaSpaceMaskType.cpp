
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtImplInclude.h


#include "Mp7JrsMaskType.h"
#include "Mp7JrsMediaSpaceMaskType_SubInterval_CollectionType.h"
#include "Mp7JrsMediaSpaceMaskType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMediaSpaceMaskType_SubInterval_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SubInterval

#include <assert.h>
IMp7JrsMediaSpaceMaskType::IMp7JrsMediaSpaceMaskType()
{

// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtPropInit.h

}

IMp7JrsMediaSpaceMaskType::~IMp7JrsMediaSpaceMaskType()
{
// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtPropCleanup.h

}

Mp7JrsMediaSpaceMaskType::Mp7JrsMediaSpaceMaskType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaSpaceMaskType::~Mp7JrsMediaSpaceMaskType()
{
	Cleanup();
}

void Mp7JrsMediaSpaceMaskType::Init()
{
	// Init base
	m_Base = CreateMaskType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SubInterval = Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtMyPropInit.h

}

void Mp7JrsMediaSpaceMaskType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SubInterval);
}

void Mp7JrsMediaSpaceMaskType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaSpaceMaskTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaSpaceMaskType
	const Dc1Ptr< Mp7JrsMediaSpaceMaskType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMaskPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaSpaceMaskType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SubInterval);
		this->SetSubInterval(Dc1Factory::CloneObject(tmp->GetSubInterval()));
}

Dc1NodePtr Mp7JrsMediaSpaceMaskType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaSpaceMaskType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMaskType > Mp7JrsMediaSpaceMaskType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr Mp7JrsMediaSpaceMaskType::GetSubInterval() const
{
		return m_SubInterval;
}

void Mp7JrsMediaSpaceMaskType::SetSubInterval(const Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaSpaceMaskType::SetSubInterval().");
	}
	if (m_SubInterval != item)
	{
		// Dc1Factory::DeleteObject(m_SubInterval);
		m_SubInterval = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SubInterval) m_SubInterval->SetParent(m_myself.getPointer());
		if(m_SubInterval != Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr())
		{
			m_SubInterval->SetContentName(XMLString::transcode("SubInterval"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaSpaceMaskType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SubInterval")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SubInterval is item of type MediaSpaceMaskType_SubInterval_LocalType
		// in element collection MediaSpaceMaskType_SubInterval_CollectionType
		
		context->Found = true;
		Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr coll = GetSubInterval();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaSpaceMaskType_SubInterval_CollectionType; // FTT, check this
				SetSubInterval(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaSpaceMaskType_SubInterval_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaSpaceMaskType_SubInterval_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaSpaceMaskType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaSpaceMaskType");
	}
	return result;
}

XMLCh * Mp7JrsMediaSpaceMaskType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMediaSpaceMaskType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMediaSpaceMaskType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaSpaceMaskType"));
	// Element serialization:
	if (m_SubInterval != Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SubInterval->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMediaSpaceMaskType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMaskType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SubInterval"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SubInterval")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr tmp = CreateMediaSpaceMaskType_SubInterval_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSubInterval(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaSpaceMaskType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SubInterval")) == 0))
  {
	Mp7JrsMediaSpaceMaskType_SubInterval_CollectionPtr tmp = CreateMediaSpaceMaskType_SubInterval_CollectionType; // FTT, check this
	this->SetSubInterval(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaSpaceMaskType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSubInterval() != Dc1NodePtr())
		result.Insert(GetSubInterval());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaSpaceMaskType_ExtMethodImpl.h


