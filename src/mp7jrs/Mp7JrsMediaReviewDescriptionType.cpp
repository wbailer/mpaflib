
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentManagementType.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsKeywordAnnotationType.h"
#include "Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType.h"
#include "Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionType.h"
#include "Mp7JrsRatingType.h"
#include "Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsMediaReviewDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FreeTextReview
#include "Mp7JrsMediaQualityType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QualityRating

#include <assert.h>
IMp7JrsMediaReviewDescriptionType::IMp7JrsMediaReviewDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtPropInit.h

}

IMp7JrsMediaReviewDescriptionType::~IMp7JrsMediaReviewDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtPropCleanup.h

}

Mp7JrsMediaReviewDescriptionType::Mp7JrsMediaReviewDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaReviewDescriptionType::~Mp7JrsMediaReviewDescriptionType()
{
	Cleanup();
}

void Mp7JrsMediaReviewDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentManagementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_reviewTime = Mp7JrstimePointPtr(); // Pattern

	// Init elements (element, union sequence choice all any)
	
	m_ContentRef = Mp7JrsReferencePtr(); // Class
	m_Tags = Mp7JrsKeywordAnnotationPtr(); // Class
	m_Tags_Exist = false;
	m_FreeTextReview = Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr(); // Collection
	m_MediaRating = Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr(); // Collection
	m_IdentityRating = Mp7JrsRatingPtr(); // Class
	m_IdentityRating_Exist = false;
	m_QualityRating = Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtMyPropInit.h

}

void Mp7JrsMediaReviewDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_reviewTime); // Pattern
	// Dc1Factory::DeleteObject(m_ContentRef);
	// Dc1Factory::DeleteObject(m_Tags);
	// Dc1Factory::DeleteObject(m_FreeTextReview);
	// Dc1Factory::DeleteObject(m_MediaRating);
	// Dc1Factory::DeleteObject(m_IdentityRating);
	// Dc1Factory::DeleteObject(m_QualityRating);
}

void Mp7JrsMediaReviewDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaReviewDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaReviewDescriptionType
	const Dc1Ptr< Mp7JrsMediaReviewDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentManagementPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaReviewDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_reviewTime); // Pattern
		this->SetreviewTime(Dc1Factory::CloneObject(tmp->GetreviewTime()));
	}
		// Dc1Factory::DeleteObject(m_ContentRef);
		this->SetContentRef(Dc1Factory::CloneObject(tmp->GetContentRef()));
	if (tmp->IsValidTags())
	{
		// Dc1Factory::DeleteObject(m_Tags);
		this->SetTags(Dc1Factory::CloneObject(tmp->GetTags()));
	}
	else
	{
		InvalidateTags();
	}
		// Dc1Factory::DeleteObject(m_FreeTextReview);
		this->SetFreeTextReview(Dc1Factory::CloneObject(tmp->GetFreeTextReview()));
		// Dc1Factory::DeleteObject(m_MediaRating);
		this->SetMediaRating(Dc1Factory::CloneObject(tmp->GetMediaRating()));
	if (tmp->IsValidIdentityRating())
	{
		// Dc1Factory::DeleteObject(m_IdentityRating);
		this->SetIdentityRating(Dc1Factory::CloneObject(tmp->GetIdentityRating()));
	}
	else
	{
		InvalidateIdentityRating();
	}
		// Dc1Factory::DeleteObject(m_QualityRating);
		this->SetQualityRating(Dc1Factory::CloneObject(tmp->GetQualityRating()));
}

Dc1NodePtr Mp7JrsMediaReviewDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaReviewDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentManagementType > Mp7JrsMediaReviewDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrstimePointPtr Mp7JrsMediaReviewDescriptionType::GetreviewTime() const
{
	return m_reviewTime;
}

void Mp7JrsMediaReviewDescriptionType::SetreviewTime(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetreviewTime().");
	}
	m_reviewTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_reviewTime) m_reviewTime->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsReferencePtr Mp7JrsMediaReviewDescriptionType::GetContentRef() const
{
		return m_ContentRef;
}

Mp7JrsKeywordAnnotationPtr Mp7JrsMediaReviewDescriptionType::GetTags() const
{
		return m_Tags;
}

// Element is optional
bool Mp7JrsMediaReviewDescriptionType::IsValidTags() const
{
	return m_Tags_Exist;
}

Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr Mp7JrsMediaReviewDescriptionType::GetFreeTextReview() const
{
		return m_FreeTextReview;
}

Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr Mp7JrsMediaReviewDescriptionType::GetMediaRating() const
{
		return m_MediaRating;
}

Mp7JrsRatingPtr Mp7JrsMediaReviewDescriptionType::GetIdentityRating() const
{
		return m_IdentityRating;
}

// Element is optional
bool Mp7JrsMediaReviewDescriptionType::IsValidIdentityRating() const
{
	return m_IdentityRating_Exist;
}

Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr Mp7JrsMediaReviewDescriptionType::GetQualityRating() const
{
		return m_QualityRating;
}

void Mp7JrsMediaReviewDescriptionType::SetContentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetContentRef().");
	}
	if (m_ContentRef != item)
	{
		// Dc1Factory::DeleteObject(m_ContentRef);
		m_ContentRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContentRef) m_ContentRef->SetParent(m_myself.getPointer());
		if (m_ContentRef && m_ContentRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ContentRef->UseTypeAttribute = true;
		}
		if(m_ContentRef != Mp7JrsReferencePtr())
		{
			m_ContentRef->SetContentName(XMLString::transcode("ContentRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::SetTags(const Mp7JrsKeywordAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetTags().");
	}
	if (m_Tags != item || m_Tags_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Tags);
		m_Tags = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Tags) m_Tags->SetParent(m_myself.getPointer());
		if (m_Tags && m_Tags->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Tags->UseTypeAttribute = true;
		}
		m_Tags_Exist = true;
		if(m_Tags != Mp7JrsKeywordAnnotationPtr())
		{
			m_Tags->SetContentName(XMLString::transcode("Tags"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::InvalidateTags()
{
	m_Tags_Exist = false;
}
void Mp7JrsMediaReviewDescriptionType::SetFreeTextReview(const Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetFreeTextReview().");
	}
	if (m_FreeTextReview != item)
	{
		// Dc1Factory::DeleteObject(m_FreeTextReview);
		m_FreeTextReview = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FreeTextReview) m_FreeTextReview->SetParent(m_myself.getPointer());
		if(m_FreeTextReview != Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr())
		{
			m_FreeTextReview->SetContentName(XMLString::transcode("FreeTextReview"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::SetMediaRating(const Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetMediaRating().");
	}
	if (m_MediaRating != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRating);
		m_MediaRating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRating) m_MediaRating->SetParent(m_myself.getPointer());
		if(m_MediaRating != Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr())
		{
			m_MediaRating->SetContentName(XMLString::transcode("MediaRating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::SetIdentityRating(const Mp7JrsRatingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetIdentityRating().");
	}
	if (m_IdentityRating != item || m_IdentityRating_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_IdentityRating);
		m_IdentityRating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IdentityRating) m_IdentityRating->SetParent(m_myself.getPointer());
		if (m_IdentityRating && m_IdentityRating->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IdentityRating->UseTypeAttribute = true;
		}
		m_IdentityRating_Exist = true;
		if(m_IdentityRating != Mp7JrsRatingPtr())
		{
			m_IdentityRating->SetContentName(XMLString::transcode("IdentityRating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::InvalidateIdentityRating()
{
	m_IdentityRating_Exist = false;
}
void Mp7JrsMediaReviewDescriptionType::SetQualityRating(const Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetQualityRating().");
	}
	if (m_QualityRating != item)
	{
		// Dc1Factory::DeleteObject(m_QualityRating);
		m_QualityRating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QualityRating) m_QualityRating->SetParent(m_myself.getPointer());
		if(m_QualityRating != Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr())
		{
			m_QualityRating->SetContentName(XMLString::transcode("QualityRating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsMediaReviewDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsMediaReviewDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsMediaReviewDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsMediaReviewDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsMediaReviewDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsMediaReviewDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaReviewDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("reviewTime")) == 0)
	{
		// reviewTime is simple attribute timePointType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstimePointPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContentRef")) == 0)
	{
		// ContentRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContentRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContentRef(p, client);
					if((p = GetContentRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Tags")) == 0)
	{
		// Tags is simple element KeywordAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTags()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsKeywordAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTags(p, client);
					if((p = GetTags()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FreeTextReview")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FreeTextReview is item of type TextualType
		// in element collection MediaReviewDescriptionType_FreeTextReview_CollectionType
		
		context->Found = true;
		Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr coll = GetFreeTextReview();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaReviewDescriptionType_FreeTextReview_CollectionType; // FTT, check this
				SetFreeTextReview(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRating")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaRating is item of type RatingType
		// in element collection MediaReviewDescriptionType_MediaRating_CollectionType
		
		context->Found = true;
		Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr coll = GetMediaRating();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaReviewDescriptionType_MediaRating_CollectionType; // FTT, check this
				SetMediaRating(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRatingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IdentityRating")) == 0)
	{
		// IdentityRating is simple element RatingType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIdentityRating()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRatingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIdentityRating(p, client);
					if((p = GetIdentityRating()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("QualityRating")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:QualityRating is item of type MediaQualityType
		// in element collection MediaReviewDescriptionType_QualityRating_CollectionType
		
		context->Found = true;
		Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr coll = GetQualityRating();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaReviewDescriptionType_QualityRating_CollectionType; // FTT, check this
				SetQualityRating(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaQualityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaReviewDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaReviewDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsMediaReviewDescriptionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaReviewDescriptionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaReviewDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaReviewDescriptionType"));
	// Attribute Serialization:
	// Pattern
	if (m_reviewTime != Mp7JrstimePointPtr())
	{
		XMLCh * tmp = m_reviewTime->ToText();
		element->setAttributeNS(X(""), X("reviewTime"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_ContentRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ContentRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ContentRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ContentRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Tags != Mp7JrsKeywordAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Tags->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Tags"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Tags->Serialize(doc, element, &elem);
	}
	if (m_FreeTextReview != Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FreeTextReview->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaRating != Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaRating->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_IdentityRating != Mp7JrsRatingPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IdentityRating->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IdentityRating"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IdentityRating->Serialize(doc, element, &elem);
	}
	if (m_QualityRating != Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QualityRating->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMediaReviewDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("reviewTime")))
	{
		Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("reviewTime")));
		this->SetreviewTime(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsContentManagementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ContentRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ContentRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContentRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Tags"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Tags")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateKeywordAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTags(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FreeTextReview"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FreeTextReview")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr tmp = CreateMediaReviewDescriptionType_FreeTextReview_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFreeTextReview(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaRating"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaRating")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr tmp = CreateMediaReviewDescriptionType_MediaRating_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaRating(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IdentityRating"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IdentityRating")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRatingType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIdentityRating(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("QualityRating"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("QualityRating")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr tmp = CreateMediaReviewDescriptionType_QualityRating_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQualityRating(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaReviewDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ContentRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetContentRef(child);
  }
  if (XMLString::compareString(elementname, X("Tags")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateKeywordAnnotationType; // FTT, check this
	}
	this->SetTags(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FreeTextReview")) == 0))
  {
	Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr tmp = CreateMediaReviewDescriptionType_FreeTextReview_CollectionType; // FTT, check this
	this->SetFreeTextReview(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaRating")) == 0))
  {
	Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr tmp = CreateMediaReviewDescriptionType_MediaRating_CollectionType; // FTT, check this
	this->SetMediaRating(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("IdentityRating")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRatingType; // FTT, check this
	}
	this->SetIdentityRating(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("QualityRating")) == 0))
  {
	Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr tmp = CreateMediaReviewDescriptionType_QualityRating_CollectionType; // FTT, check this
	this->SetQualityRating(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaReviewDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContentRef() != Dc1NodePtr())
		result.Insert(GetContentRef());
	if (GetTags() != Dc1NodePtr())
		result.Insert(GetTags());
	if (GetFreeTextReview() != Dc1NodePtr())
		result.Insert(GetFreeTextReview());
	if (GetMediaRating() != Dc1NodePtr())
		result.Insert(GetMediaRating());
	if (GetIdentityRating() != Dc1NodePtr())
		result.Insert(GetIdentityRating());
	if (GetQualityRating() != Dc1NodePtr())
		result.Insert(GetQualityRating());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaReviewDescriptionType_ExtMethodImpl.h


