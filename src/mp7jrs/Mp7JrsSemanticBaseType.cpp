
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsAbstractionLevelType.h"
#include "Mp7JrsSemanticBaseType_Label_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsSemanticBaseType_Property_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_CollectionType.h"
#include "Mp7JrsSemanticBaseType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSemanticBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Label
#include "Mp7JrsSemanticBaseType_MediaOccurrence_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaOccurrence
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation

#include <assert.h>
IMp7JrsSemanticBaseType::IMp7JrsSemanticBaseType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtPropInit.h

}

IMp7JrsSemanticBaseType::~IMp7JrsSemanticBaseType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtPropCleanup.h

}

Mp7JrsSemanticBaseType::Mp7JrsSemanticBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticBaseType::~Mp7JrsSemanticBaseType()
{
	Cleanup();
}

void Mp7JrsSemanticBaseType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_AbstractionLevel = Mp7JrsAbstractionLevelPtr(); // Class
	m_AbstractionLevel_Exist = false;
	m_Label = Mp7JrsSemanticBaseType_Label_CollectionPtr(); // Collection
	m_Definition = Mp7JrsTextAnnotationPtr(); // Class
	m_Definition_Exist = false;
	m_Property = Mp7JrsSemanticBaseType_Property_CollectionPtr(); // Collection
	m_MediaOccurrence = Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr(); // Collection
	m_Relation = Mp7JrsSemanticBaseType_Relation_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtMyPropInit.h

}

void Mp7JrsSemanticBaseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_AbstractionLevel);
	// Dc1Factory::DeleteObject(m_Label);
	// Dc1Factory::DeleteObject(m_Definition);
	// Dc1Factory::DeleteObject(m_Property);
	// Dc1Factory::DeleteObject(m_MediaOccurrence);
	// Dc1Factory::DeleteObject(m_Relation);
}

void Mp7JrsSemanticBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticBaseTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticBaseType
	const Dc1Ptr< Mp7JrsSemanticBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSemanticBaseType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidAbstractionLevel())
	{
		// Dc1Factory::DeleteObject(m_AbstractionLevel);
		this->SetAbstractionLevel(Dc1Factory::CloneObject(tmp->GetAbstractionLevel()));
	}
	else
	{
		InvalidateAbstractionLevel();
	}
		// Dc1Factory::DeleteObject(m_Label);
		this->SetLabel(Dc1Factory::CloneObject(tmp->GetLabel()));
	if (tmp->IsValidDefinition())
	{
		// Dc1Factory::DeleteObject(m_Definition);
		this->SetDefinition(Dc1Factory::CloneObject(tmp->GetDefinition()));
	}
	else
	{
		InvalidateDefinition();
	}
		// Dc1Factory::DeleteObject(m_Property);
		this->SetProperty(Dc1Factory::CloneObject(tmp->GetProperty()));
		// Dc1Factory::DeleteObject(m_MediaOccurrence);
		this->SetMediaOccurrence(Dc1Factory::CloneObject(tmp->GetMediaOccurrence()));
		// Dc1Factory::DeleteObject(m_Relation);
		this->SetRelation(Dc1Factory::CloneObject(tmp->GetRelation()));
}

Dc1NodePtr Mp7JrsSemanticBaseType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSemanticBaseType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSemanticBaseType::GetBase() const
{
	return m_Base;
}

Mp7JrsAbstractionLevelPtr Mp7JrsSemanticBaseType::GetAbstractionLevel() const
{
		return m_AbstractionLevel;
}

// Element is optional
bool Mp7JrsSemanticBaseType::IsValidAbstractionLevel() const
{
	return m_AbstractionLevel_Exist;
}

Mp7JrsSemanticBaseType_Label_CollectionPtr Mp7JrsSemanticBaseType::GetLabel() const
{
		return m_Label;
}

Mp7JrsTextAnnotationPtr Mp7JrsSemanticBaseType::GetDefinition() const
{
		return m_Definition;
}

// Element is optional
bool Mp7JrsSemanticBaseType::IsValidDefinition() const
{
	return m_Definition_Exist;
}

Mp7JrsSemanticBaseType_Property_CollectionPtr Mp7JrsSemanticBaseType::GetProperty() const
{
		return m_Property;
}

Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr Mp7JrsSemanticBaseType::GetMediaOccurrence() const
{
		return m_MediaOccurrence;
}

Mp7JrsSemanticBaseType_Relation_CollectionPtr Mp7JrsSemanticBaseType::GetRelation() const
{
		return m_Relation;
}

void Mp7JrsSemanticBaseType::SetAbstractionLevel(const Mp7JrsAbstractionLevelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetAbstractionLevel().");
	}
	if (m_AbstractionLevel != item || m_AbstractionLevel_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AbstractionLevel);
		m_AbstractionLevel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AbstractionLevel) m_AbstractionLevel->SetParent(m_myself.getPointer());
		if (m_AbstractionLevel && m_AbstractionLevel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AbstractionLevelType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AbstractionLevel->UseTypeAttribute = true;
		}
		m_AbstractionLevel_Exist = true;
		if(m_AbstractionLevel != Mp7JrsAbstractionLevelPtr())
		{
			m_AbstractionLevel->SetContentName(XMLString::transcode("AbstractionLevel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidateAbstractionLevel()
{
	m_AbstractionLevel_Exist = false;
}
void Mp7JrsSemanticBaseType::SetLabel(const Mp7JrsSemanticBaseType_Label_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetLabel().");
	}
	if (m_Label != item)
	{
		// Dc1Factory::DeleteObject(m_Label);
		m_Label = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Label) m_Label->SetParent(m_myself.getPointer());
		if(m_Label != Mp7JrsSemanticBaseType_Label_CollectionPtr())
		{
			m_Label->SetContentName(XMLString::transcode("Label"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::SetDefinition(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetDefinition().");
	}
	if (m_Definition != item || m_Definition_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Definition);
		m_Definition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Definition) m_Definition->SetParent(m_myself.getPointer());
		if (m_Definition && m_Definition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Definition->UseTypeAttribute = true;
		}
		m_Definition_Exist = true;
		if(m_Definition != Mp7JrsTextAnnotationPtr())
		{
			m_Definition->SetContentName(XMLString::transcode("Definition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidateDefinition()
{
	m_Definition_Exist = false;
}
void Mp7JrsSemanticBaseType::SetProperty(const Mp7JrsSemanticBaseType_Property_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetProperty().");
	}
	if (m_Property != item)
	{
		// Dc1Factory::DeleteObject(m_Property);
		m_Property = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Property) m_Property->SetParent(m_myself.getPointer());
		if(m_Property != Mp7JrsSemanticBaseType_Property_CollectionPtr())
		{
			m_Property->SetContentName(XMLString::transcode("Property"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::SetMediaOccurrence(const Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetMediaOccurrence().");
	}
	if (m_MediaOccurrence != item)
	{
		// Dc1Factory::DeleteObject(m_MediaOccurrence);
		m_MediaOccurrence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaOccurrence) m_MediaOccurrence->SetParent(m_myself.getPointer());
		if(m_MediaOccurrence != Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr())
		{
			m_MediaOccurrence->SetContentName(XMLString::transcode("MediaOccurrence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::SetRelation(const Mp7JrsSemanticBaseType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetRelation().");
	}
	if (m_Relation != item)
	{
		// Dc1Factory::DeleteObject(m_Relation);
		m_Relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relation) m_Relation->SetParent(m_myself.getPointer());
		if(m_Relation != Mp7JrsSemanticBaseType_Relation_CollectionPtr())
		{
			m_Relation->SetContentName(XMLString::transcode("Relation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSemanticBaseType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSemanticBaseType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSemanticBaseType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSemanticBaseType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSemanticBaseType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSemanticBaseType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSemanticBaseType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSemanticBaseType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSemanticBaseType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSemanticBaseType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSemanticBaseType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSemanticBaseType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSemanticBaseType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSemanticBaseType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSemanticBaseType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticBaseType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSemanticBaseType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSemanticBaseType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticBaseType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSemanticBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("AbstractionLevel")) == 0)
	{
		// AbstractionLevel is simple element AbstractionLevelType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAbstractionLevel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AbstractionLevelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAbstractionLevelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAbstractionLevel(p, client);
					if((p = GetAbstractionLevel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Label")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Label is item of type TermUseType
		// in element collection SemanticBaseType_Label_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_Label_CollectionPtr coll = GetLabel();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_Label_CollectionType; // FTT, check this
				SetLabel(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Definition")) == 0)
	{
		// Definition is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDefinition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDefinition(p, client);
					if((p = GetDefinition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Property")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Property is item of type TermUseType
		// in element collection SemanticBaseType_Property_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_Property_CollectionPtr coll = GetProperty();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_Property_CollectionType; // FTT, check this
				SetProperty(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaOccurrence")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaOccurrence is item of type SemanticBaseType_MediaOccurrence_LocalType
		// in element collection SemanticBaseType_MediaOccurrence_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr coll = GetMediaOccurrence();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_MediaOccurrence_CollectionType; // FTT, check this
				SetMediaOccurrence(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticBaseType_MediaOccurrence_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticBaseType_MediaOccurrence_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Relation is item of type RelationType
		// in element collection SemanticBaseType_Relation_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticBaseType_Relation_CollectionPtr coll = GetRelation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticBaseType_Relation_CollectionType; // FTT, check this
				SetRelation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticBaseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticBaseType");
	}
	return result;
}

XMLCh * Mp7JrsSemanticBaseType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSemanticBaseType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSemanticBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticBaseType"));
	// Element serialization:
	// Class
	
	if (m_AbstractionLevel != Mp7JrsAbstractionLevelPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AbstractionLevel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AbstractionLevel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AbstractionLevel->Serialize(doc, element, &elem);
	}
	if (m_Label != Mp7JrsSemanticBaseType_Label_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Label->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Definition != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Definition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Definition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Definition->Serialize(doc, element, &elem);
	}
	if (m_Property != Mp7JrsSemanticBaseType_Property_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Property->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaOccurrence != Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaOccurrence->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Relation != Mp7JrsSemanticBaseType_Relation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relation->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSemanticBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AbstractionLevel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AbstractionLevel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAbstractionLevelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAbstractionLevel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Label"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Label")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_Label_CollectionPtr tmp = CreateSemanticBaseType_Label_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLabel(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Definition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Definition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDefinition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Property"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Property")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_Property_CollectionPtr tmp = CreateSemanticBaseType_Property_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetProperty(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaOccurrence"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaOccurrence")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaOccurrence(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticBaseType_Relation_CollectionPtr tmp = CreateSemanticBaseType_Relation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("AbstractionLevel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAbstractionLevelType; // FTT, check this
	}
	this->SetAbstractionLevel(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Label")) == 0))
  {
	Mp7JrsSemanticBaseType_Label_CollectionPtr tmp = CreateSemanticBaseType_Label_CollectionType; // FTT, check this
	this->SetLabel(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Definition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetDefinition(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Property")) == 0))
  {
	Mp7JrsSemanticBaseType_Property_CollectionPtr tmp = CreateSemanticBaseType_Property_CollectionType; // FTT, check this
	this->SetProperty(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaOccurrence")) == 0))
  {
	Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr tmp = CreateSemanticBaseType_MediaOccurrence_CollectionType; // FTT, check this
	this->SetMediaOccurrence(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relation")) == 0))
  {
	Mp7JrsSemanticBaseType_Relation_CollectionPtr tmp = CreateSemanticBaseType_Relation_CollectionType; // FTT, check this
	this->SetRelation(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAbstractionLevel() != Dc1NodePtr())
		result.Insert(GetAbstractionLevel());
	if (GetLabel() != Dc1NodePtr())
		result.Insert(GetLabel());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetProperty() != Dc1NodePtr())
		result.Insert(GetProperty());
	if (GetMediaOccurrence() != Dc1NodePtr())
		result.Insert(GetMediaOccurrence());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticBaseType_ExtMethodImpl.h


