
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtImplInclude.h


#include "Mp7JrsFragmentUpdateCommandType_LocalType0.h"
#include "Mp7JrsFragmentUpdateCommandType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFragmentUpdateCommandType::IMp7JrsFragmentUpdateCommandType()
{

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtPropInit.h

}

IMp7JrsFragmentUpdateCommandType::~IMp7JrsFragmentUpdateCommandType()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtPropCleanup.h

}

Mp7JrsFragmentUpdateCommandType::Mp7JrsFragmentUpdateCommandType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFragmentUpdateCommandType::~Mp7JrsFragmentUpdateCommandType()
{
	Cleanup();
}

void Mp7JrsFragmentUpdateCommandType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FragmentUpdateCommandType_LocalType = Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration; // Enumeration
	m_FragmentUpdateCommandType_LocalType0 = Mp7JrsFragmentUpdateCommandType_Local0Ptr(); // Class


// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtMyPropInit.h

}

void Mp7JrsFragmentUpdateCommandType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FragmentUpdateCommandType_LocalType0);
}

void Mp7JrsFragmentUpdateCommandType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FragmentUpdateCommandTypePtr, since we
	// might need GetBase(), which isn't defined in IFragmentUpdateCommandType
	const Dc1Ptr< Mp7JrsFragmentUpdateCommandType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if(tmp->GetFragmentUpdateCommandType_LocalType() != Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration)
	{
		this->SetFragmentUpdateCommandType_LocalType(tmp->GetFragmentUpdateCommandType_LocalType());
		return;
	}
	if(tmp->GetFragmentUpdateCommandType_LocalType0() != Mp7JrsFragmentUpdateCommandType_Local0Ptr())
	{
		this->SetFragmentUpdateCommandType_LocalType0(Dc1Factory::CloneObject(tmp->GetFragmentUpdateCommandType_LocalType0()));
		return; 
	}
	}
}

Dc1NodePtr Mp7JrsFragmentUpdateCommandType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFragmentUpdateCommandType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsFragmentUpdateCommandType_LocalType::Enumeration Mp7JrsFragmentUpdateCommandType::GetFragmentUpdateCommandType_LocalType() const
{
		return m_FragmentUpdateCommandType_LocalType;
}

Mp7JrsFragmentUpdateCommandType_Local0Ptr Mp7JrsFragmentUpdateCommandType::GetFragmentUpdateCommandType_LocalType0() const
{
		return m_FragmentUpdateCommandType_LocalType0;
}

void Mp7JrsFragmentUpdateCommandType::SetFragmentUpdateCommandType_LocalType(Mp7JrsFragmentUpdateCommandType_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateCommandType::SetFragmentUpdateCommandType_LocalType().");
	}
	if (m_FragmentUpdateCommandType_LocalType != item)
	{
		// nothing to free here, hopefully
		m_FragmentUpdateCommandType_LocalType = item;
	// Dc1Factory::DeleteObject(m_FragmentUpdateCommandType_LocalType0);
	m_FragmentUpdateCommandType_LocalType0 = Mp7JrsFragmentUpdateCommandType_Local0Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdateCommandType::SetFragmentUpdateCommandType_LocalType0(const Mp7JrsFragmentUpdateCommandType_Local0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateCommandType::SetFragmentUpdateCommandType_LocalType0().");
	}
	if (m_FragmentUpdateCommandType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_FragmentUpdateCommandType_LocalType0);
		m_FragmentUpdateCommandType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FragmentUpdateCommandType_LocalType0) m_FragmentUpdateCommandType_LocalType0->SetParent(m_myself.getPointer());
		if (m_FragmentUpdateCommandType_LocalType0 && m_FragmentUpdateCommandType_LocalType0->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType_LocalType0"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FragmentUpdateCommandType_LocalType0->UseTypeAttribute = true;
		}
		if(m_FragmentUpdateCommandType_LocalType0 != Mp7JrsFragmentUpdateCommandType_Local0Ptr())
		{
			m_FragmentUpdateCommandType_LocalType0->SetContentName(XMLString::transcode("FragmentUpdateCommandType_LocalType0"));
		}
	
	m_FragmentUpdateCommandType_LocalType = Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFragmentUpdateCommandType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("FragmentUpdateCommandType_LocalType0")) == 0)
	{
		// FragmentUpdateCommandType_LocalType0 is simple element FragmentUpdateCommandType_LocalType0
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFragmentUpdateCommandType_LocalType0()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FragmentUpdateCommandType_LocalType0")))) != empty)
			{
				// Is type allowed
				Mp7JrsFragmentUpdateCommandType_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFragmentUpdateCommandType_LocalType0(p, client);
					if((p = GetFragmentUpdateCommandType_LocalType0()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FragmentUpdateCommandType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FragmentUpdateCommandType");
	}
	return result;
}

XMLCh * Mp7JrsFragmentUpdateCommandType::ToText() const
{
	if(m_FragmentUpdateCommandType_LocalType != Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration)
	{
		return Mp7JrsFragmentUpdateCommandType_LocalType::ToText(m_FragmentUpdateCommandType_LocalType);
	}
	if(m_FragmentUpdateCommandType_LocalType0 != Mp7JrsFragmentUpdateCommandType_Local0Ptr())
	{
		return m_FragmentUpdateCommandType_LocalType0->ToText();
	}
	return XMLString::transcode("");
}


bool Mp7JrsFragmentUpdateCommandType::Parse(const XMLCh * const txt)
{
	// Parse stu enumeration
	{
		Mp7JrsFragmentUpdateCommandType_LocalType::Enumeration tmp = Mp7JrsFragmentUpdateCommandType_LocalType::Parse(txt);
		if(tmp != Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration)
		{
			this->SetFragmentUpdateCommandType_LocalType(tmp);
			return true;
		}
	}
	// Parse stu factory type
	{
		Mp7JrsFragmentUpdateCommandType_Local0Ptr tmp = CreateFragmentUpdateCommandType_LocalType0; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SetFragmentUpdateCommandType_LocalType0(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	return false;
}

bool Mp7JrsFragmentUpdateCommandType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsFragmentUpdateCommandType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsFragmentUpdateCommandType::ContentToString() const
{
	// Mp7JrsFragmentUpdateCommandType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsFragmentUpdateCommandType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdateCommandType"));
	// Element serialization:
	if(m_FragmentUpdateCommandType_LocalType != Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsFragmentUpdateCommandType_LocalType::ToText(m_FragmentUpdateCommandType_LocalType);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdateCommandType_LocalType"), false);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_FragmentUpdateCommandType_LocalType0 != Mp7JrsFragmentUpdateCommandType_Local0Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FragmentUpdateCommandType_LocalType0->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdateCommandType_LocalType0"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FragmentUpdateCommandType_LocalType0->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFragmentUpdateCommandType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("FragmentUpdateCommandType_LocalType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FragmentUpdateCommandType_LocalType")) == 0))
		{
			Mp7JrsFragmentUpdateCommandType_LocalType::Enumeration tmp = Mp7JrsFragmentUpdateCommandType_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsFragmentUpdateCommandType_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetFragmentUpdateCommandType_LocalType(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FragmentUpdateCommandType_LocalType0"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FragmentUpdateCommandType_LocalType0")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFragmentUpdateCommandType_LocalType0; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFragmentUpdateCommandType_LocalType0(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFragmentUpdateCommandType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FragmentUpdateCommandType_LocalType0")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFragmentUpdateCommandType_LocalType0; // FTT, check this
	}
	this->SetFragmentUpdateCommandType_LocalType0(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFragmentUpdateCommandType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFragmentUpdateCommandType_LocalType0() != Dc1NodePtr())
		result.Insert(GetFragmentUpdateCommandType_LocalType0());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateCommandType_ExtMethodImpl.h


