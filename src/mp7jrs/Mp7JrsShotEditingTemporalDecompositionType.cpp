
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtImplInclude.h


#include "Mp7JrsTemporalSegmentDecompositionType.h"
#include "Mp7JrsShotEditingTemporalDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsShotEditingTemporalDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsShotEditingTemporalDecompositionType_LocalType.h" // Sequence collection CompositionShot
#include "Mp7JrsCompositionShotType.h" // Sequence collection element CompositionShot
#include "Mp7JrsReferenceType.h" // Sequence collection element CompositionShotRef
#include "Mp7JrsCompositionTransitionType.h" // Sequence collection element CompositionTransition

#include <assert.h>
IMp7JrsShotEditingTemporalDecompositionType::IMp7JrsShotEditingTemporalDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtPropInit.h

}

IMp7JrsShotEditingTemporalDecompositionType::~IMp7JrsShotEditingTemporalDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtPropCleanup.h

}

Mp7JrsShotEditingTemporalDecompositionType::Mp7JrsShotEditingTemporalDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsShotEditingTemporalDecompositionType::~Mp7JrsShotEditingTemporalDecompositionType()
{
	Cleanup();
}

void Mp7JrsShotEditingTemporalDecompositionType::Init()
{
	// Init base
	m_Base = CreateTemporalSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ShotEditingTemporalDecompositionType_LocalType = Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtMyPropInit.h

}

void Mp7JrsShotEditingTemporalDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ShotEditingTemporalDecompositionType_LocalType);
}

void Mp7JrsShotEditingTemporalDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ShotEditingTemporalDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in IShotEditingTemporalDecompositionType
	const Dc1Ptr< Mp7JrsShotEditingTemporalDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTemporalSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsShotEditingTemporalDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ShotEditingTemporalDecompositionType_LocalType);
		this->SetShotEditingTemporalDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetShotEditingTemporalDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTemporalSegmentDecompositionType > Mp7JrsShotEditingTemporalDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr Mp7JrsShotEditingTemporalDecompositionType::GetShotEditingTemporalDecompositionType_LocalType() const
{
		return m_ShotEditingTemporalDecompositionType_LocalType;
}

void Mp7JrsShotEditingTemporalDecompositionType::SetShotEditingTemporalDecompositionType_LocalType(const Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SetShotEditingTemporalDecompositionType_LocalType().");
	}
	if (m_ShotEditingTemporalDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_ShotEditingTemporalDecompositionType_LocalType);
		m_ShotEditingTemporalDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ShotEditingTemporalDecompositionType_LocalType) m_ShotEditingTemporalDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsShotEditingTemporalDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsShotEditingTemporalDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsShotEditingTemporalDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsShotEditingTemporalDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsShotEditingTemporalDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsShotEditingTemporalDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsShotEditingTemporalDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsShotEditingTemporalDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsShotEditingTemporalDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsShotEditingTemporalDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsShotEditingTemporalDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsShotEditingTemporalDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsShotEditingTemporalDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsShotEditingTemporalDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsShotEditingTemporalDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsShotEditingTemporalDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsShotEditingTemporalDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsShotEditingTemporalDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsShotEditingTemporalDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsShotEditingTemporalDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsShotEditingTemporalDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsShotEditingTemporalDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsShotEditingTemporalDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsShotEditingTemporalDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShotEditingTemporalDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsShotEditingTemporalDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsShotEditingTemporalDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsShotEditingTemporalDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("CompositionShot")) == 0)
	{
		// CompositionShot is contained in itemtype ShotEditingTemporalDecompositionType_LocalType
		// in sequence collection ShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr coll = GetShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionShot()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsShotEditingTemporalDecompositionType_LocalPtr item = CreateShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionShot(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetCompositionShot());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionShot(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetCompositionShot());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCompositionShotPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetCompositionShot(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionShot()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CompositionShotRef")) == 0)
	{
		// CompositionShotRef is contained in itemtype ShotEditingTemporalDecompositionType_LocalType
		// in sequence collection ShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr coll = GetShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionShotRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsShotEditingTemporalDecompositionType_LocalPtr item = CreateShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionShotRef(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetCompositionShotRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionShotRef(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetCompositionShotRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetCompositionShotRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionShotRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CompositionTransition")) == 0)
	{
		// CompositionTransition is contained in itemtype ShotEditingTemporalDecompositionType_LocalType
		// in sequence collection ShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr coll = GetShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionTransition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsShotEditingTemporalDecompositionType_LocalPtr item = CreateShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionTransition(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetCompositionTransition());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionTransition(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetCompositionTransition());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCompositionTransitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetCompositionTransition(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionTransition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CompositionTransitionRef")) == 0)
	{
		// CompositionTransitionRef is contained in itemtype ShotEditingTemporalDecompositionType_LocalType
		// in sequence collection ShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr coll = GetShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionTransitionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsShotEditingTemporalDecompositionType_LocalPtr item = CreateShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionTransitionRef(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetCompositionTransitionRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetCompositionTransitionRef(
						((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetCompositionTransitionRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetCompositionTransitionRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetCompositionTransitionRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ShotEditingTemporalDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ShotEditingTemporalDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsShotEditingTemporalDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsShotEditingTemporalDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsShotEditingTemporalDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ShotEditingTemporalDecompositionType"));
	// Element serialization:
	if (m_ShotEditingTemporalDecompositionType_LocalType != Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ShotEditingTemporalDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsShotEditingTemporalDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTemporalSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CompositionShot")) == 0)
			(Dc1Util::HasNodeName(parent, X("CompositionShot"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CompositionShotRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("CompositionShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CompositionTransition")) == 0)
			(Dc1Util::HasNodeName(parent, X("CompositionTransition"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("CompositionTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr tmp = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetShotEditingTemporalDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsShotEditingTemporalDecompositionType_CollectionPtr tmp = CreateShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
	this->SetShotEditingTemporalDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsShotEditingTemporalDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetShotEditingTemporalDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetShotEditingTemporalDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_ExtMethodImpl.h


