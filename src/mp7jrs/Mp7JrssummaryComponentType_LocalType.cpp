
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrssummaryComponentType_LocalType_ExtImplInclude.h


#include "Mp7JrssummaryComponentType_LocalType.h"

// no includefile for extension defined 
// file Mp7JrssummaryComponentType_LocalType_ExtMethodImpl.h


const XMLCh * Mp7JrssummaryComponentType_LocalType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrssummaryComponentType_LocalType::ToText(Mp7JrssummaryComponentType_LocalType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/audio:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("audio");
			break;
		case /*Enumeration::*/visual:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("visual");
			break;
		case /*Enumeration::*/audioVisual:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("audioVisual");
			break;
		case /*Enumeration::*/textual:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("textual");
			break;
		case /*Enumeration::*/keyAudioClips:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keyAudioClips");
			break;
		case /*Enumeration::*/keyVideoClips:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keyVideoClips");
			break;
		case /*Enumeration::*/keyAudioVisualClips:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keyAudioVisualClips");
			break;
		case /*Enumeration::*/keyFrames:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keyFrames");
			break;
		case /*Enumeration::*/keySounds:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keySounds");
			break;
		case /*Enumeration::*/keyThemes:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("keyThemes");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrssummaryComponentType_LocalType::Enumeration Mp7JrssummaryComponentType_LocalType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrssummaryComponentType_LocalType::Enumeration result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "audio") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/audio;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "visual") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/visual;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "audioVisual") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/audioVisual;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "textual") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/textual;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keyAudioClips") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keyAudioClips;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keyVideoClips") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keyVideoClips;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keyAudioVisualClips") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keyAudioVisualClips;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keyFrames") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keyFrames;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keySounds") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keySounds;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "keyThemes") == 0)
	{
		result = Mp7JrssummaryComponentType_LocalType::/*Enumeration::*/keyThemes;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
