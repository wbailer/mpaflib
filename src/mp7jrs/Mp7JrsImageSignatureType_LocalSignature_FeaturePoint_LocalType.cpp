
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned8.h"
#include "Mp7Jrsunsigned4.h"
#include "Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType.h"
#include "Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtPropInit.h

}

IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::~IMp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtPropCleanup.h

}

Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::~Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType()
{
	Cleanup();
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_XCoord = Mp7Jrsunsigned8Ptr(); // Class with content 
	m_YCoord = Mp7Jrsunsigned8Ptr(); // Class with content 
	m_Direction = Mp7Jrsunsigned4Ptr(); // Class with content 
	m_LocalSignature = Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtMyPropInit.h

}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_XCoord);
	// Dc1Factory::DeleteObject(m_YCoord);
	// Dc1Factory::DeleteObject(m_Direction);
	// Dc1Factory::DeleteObject(m_LocalSignature);
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ImageSignatureType_LocalSignature_FeaturePoint_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IImageSignatureType_LocalSignature_FeaturePoint_LocalType
	const Dc1Ptr< Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_XCoord);
		this->SetXCoord(Dc1Factory::CloneObject(tmp->GetXCoord()));
		// Dc1Factory::DeleteObject(m_YCoord);
		this->SetYCoord(Dc1Factory::CloneObject(tmp->GetYCoord()));
		// Dc1Factory::DeleteObject(m_Direction);
		this->SetDirection(Dc1Factory::CloneObject(tmp->GetDirection()));
		// Dc1Factory::DeleteObject(m_LocalSignature);
		this->SetLocalSignature(Dc1Factory::CloneObject(tmp->GetLocalSignature()));
}

Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned8Ptr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetXCoord() const
{
		return m_XCoord;
}

Mp7Jrsunsigned8Ptr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetYCoord() const
{
		return m_YCoord;
}

Mp7Jrsunsigned4Ptr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetDirection() const
{
		return m_Direction;
}

Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetLocalSignature() const
{
		return m_LocalSignature;
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetXCoord(const Mp7Jrsunsigned8Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetXCoord().");
	}
	if (m_XCoord != item)
	{
		// Dc1Factory::DeleteObject(m_XCoord);
		m_XCoord = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_XCoord) m_XCoord->SetParent(m_myself.getPointer());
		if (m_XCoord && m_XCoord->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_XCoord->UseTypeAttribute = true;
		}
		if(m_XCoord != Mp7Jrsunsigned8Ptr())
		{
			m_XCoord->SetContentName(XMLString::transcode("XCoord"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetYCoord(const Mp7Jrsunsigned8Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetYCoord().");
	}
	if (m_YCoord != item)
	{
		// Dc1Factory::DeleteObject(m_YCoord);
		m_YCoord = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YCoord) m_YCoord->SetParent(m_myself.getPointer());
		if (m_YCoord && m_YCoord->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YCoord->UseTypeAttribute = true;
		}
		if(m_YCoord != Mp7Jrsunsigned8Ptr())
		{
			m_YCoord->SetContentName(XMLString::transcode("YCoord"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetDirection(const Mp7Jrsunsigned4Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetDirection().");
	}
	if (m_Direction != item)
	{
		// Dc1Factory::DeleteObject(m_Direction);
		m_Direction = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Direction) m_Direction->SetParent(m_myself.getPointer());
		if (m_Direction && m_Direction->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned4"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Direction->UseTypeAttribute = true;
		}
		if(m_Direction != Mp7Jrsunsigned4Ptr())
		{
			m_Direction->SetContentName(XMLString::transcode("Direction"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetLocalSignature(const Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::SetLocalSignature().");
	}
	if (m_LocalSignature != item)
	{
		// Dc1Factory::DeleteObject(m_LocalSignature);
		m_LocalSignature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LocalSignature) m_LocalSignature->SetParent(m_myself.getPointer());
		if (m_LocalSignature && m_LocalSignature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LocalSignature->UseTypeAttribute = true;
		}
		if(m_LocalSignature != Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr())
		{
			m_LocalSignature->SetContentName(XMLString::transcode("LocalSignature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("XCoord")) == 0)
	{
		// XCoord is simple element unsigned8
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetXCoord()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned8Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetXCoord(p, client);
					if((p = GetXCoord()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YCoord")) == 0)
	{
		// YCoord is simple element unsigned8
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYCoord()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned8Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYCoord(p, client);
					if((p = GetYCoord()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Direction")) == 0)
	{
		// Direction is simple element unsigned4
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDirection()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned4")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned4Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDirection(p, client);
					if((p = GetDirection()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LocalSignature")) == 0)
	{
		// LocalSignature is simple element ImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLocalSignature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLocalSignature(p, client);
					if((p = GetLocalSignature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ImageSignatureType_LocalSignature_FeaturePoint_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ImageSignatureType_LocalSignature_FeaturePoint_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ImageSignatureType_LocalSignature_FeaturePoint_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_XCoord != Mp7Jrsunsigned8Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_XCoord->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("XCoord"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_XCoord->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_YCoord != Mp7Jrsunsigned8Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YCoord->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YCoord"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YCoord->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Direction != Mp7Jrsunsigned4Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Direction->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Direction"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Direction->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_LocalSignature != Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LocalSignature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LocalSignature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LocalSignature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("XCoord"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("XCoord")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned8; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetXCoord(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YCoord"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YCoord")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned8; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYCoord(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Direction"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Direction")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned4; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDirection(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LocalSignature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LocalSignature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLocalSignature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("XCoord")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned8; // FTT, check this
	}
	this->SetXCoord(child);
  }
  if (XMLString::compareString(elementname, X("YCoord")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned8; // FTT, check this
	}
	this->SetYCoord(child);
  }
  if (XMLString::compareString(elementname, X("Direction")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned4; // FTT, check this
	}
	this->SetDirection(child);
  }
  if (XMLString::compareString(elementname, X("LocalSignature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageSignatureType_LocalSignature_FeaturePoint_LocalSignature_LocalType; // FTT, check this
	}
	this->SetLocalSignature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetXCoord() != Dc1NodePtr())
		result.Insert(GetXCoord());
	if (GetYCoord() != Dc1NodePtr())
		result.Insert(GetYCoord());
	if (GetDirection() != Dc1NodePtr())
		result.Insert(GetDirection());
	if (GetLocalSignature() != Dc1NodePtr())
		result.Insert(GetLocalSignature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_LocalSignature_FeaturePoint_LocalType_ExtMethodImpl.h


