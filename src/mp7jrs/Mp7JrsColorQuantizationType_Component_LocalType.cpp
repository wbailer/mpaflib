
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_Component_LocalType_ExtImplInclude.h


#include "Mp7JrsColorQuantizationType_Component_LocalType.h"

// no includefile for extension defined 
// file Mp7JrsColorQuantizationType_Component_LocalType_ExtMethodImpl.h


const XMLCh * Mp7JrsColorQuantizationType_Component_LocalType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrsColorQuantizationType_Component_LocalType::ToText(Mp7JrsColorQuantizationType_Component_LocalType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/R:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("R");
			break;
		case /*Enumeration::*/G:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("G");
			break;
		case /*Enumeration::*/B:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("B");
			break;
		case /*Enumeration::*/Y:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Y");
			break;
		case /*Enumeration::*/Cb:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Cb");
			break;
		case /*Enumeration::*/Cr:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Cr");
			break;
		case /*Enumeration::*/H:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("H");
			break;
		case /*Enumeration::*/S:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("S");
			break;
		case /*Enumeration::*/V:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("V");
			break;
		case /*Enumeration::*/Max:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Max");
			break;
		case /*Enumeration::*/Min:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Min");
			break;
		case /*Enumeration::*/Diff:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Diff");
			break;
		case /*Enumeration::*/Sum:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("Sum");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrsColorQuantizationType_Component_LocalType::Enumeration Mp7JrsColorQuantizationType_Component_LocalType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrsColorQuantizationType_Component_LocalType::Enumeration result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "R") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/R;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "G") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/G;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "B") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/B;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Y") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Y;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Cb") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Cb;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Cr") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Cr;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "H") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/H;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "S") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/S;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "V") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/V;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Max") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Max;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Min") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Min;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Diff") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Diff;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "Sum") == 0)
	{
		result = Mp7JrsColorQuantizationType_Component_LocalType::/*Enumeration::*/Sum;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
