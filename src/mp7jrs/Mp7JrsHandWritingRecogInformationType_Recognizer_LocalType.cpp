
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType::IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtPropInit.h

}

IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType::~IMp7JrsHandWritingRecogInformationType_Recognizer_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtPropCleanup.h

}

Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::~Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType()
{
	Cleanup();
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsTermUsePtr(); // Class
	m_Name_Exist = false;
	m_Owner = Dc1Ptr< Dc1Node >(); // Class
	m_Owner_Exist = false;
	m_Information = Mp7JrsTextAnnotationPtr(); // Class
	m_Information_Exist = false;


// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtMyPropInit.h

}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Owner);
	// Dc1Factory::DeleteObject(m_Information);
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HandWritingRecogInformationType_Recognizer_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IHandWritingRecogInformationType_Recognizer_LocalType
	const Dc1Ptr< Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidName())
	{
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	}
	else
	{
		InvalidateName();
	}
	if (tmp->IsValidOwner())
	{
		// Dc1Factory::DeleteObject(m_Owner);
		this->SetOwner(Dc1Factory::CloneObject(tmp->GetOwner()));
	}
	else
	{
		InvalidateOwner();
	}
	if (tmp->IsValidInformation())
	{
		// Dc1Factory::DeleteObject(m_Information);
		this->SetInformation(Dc1Factory::CloneObject(tmp->GetInformation()));
	}
	else
	{
		InvalidateInformation();
	}
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTermUsePtr Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetName() const
{
		return m_Name;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::IsValidName() const
{
	return m_Name_Exist;
}

Dc1Ptr< Dc1Node > Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetOwner() const
{
		return m_Owner;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::IsValidOwner() const
{
	return m_Owner_Exist;
}

Mp7JrsTextAnnotationPtr Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetInformation() const
{
		return m_Information;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::IsValidInformation() const
{
	return m_Information_Exist;
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetName(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetName().");
	}
	if (m_Name != item || m_Name_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if (m_Name && m_Name->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Name->UseTypeAttribute = true;
		}
		m_Name_Exist = true;
		if(m_Name != Mp7JrsTermUsePtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::InvalidateName()
{
	m_Name_Exist = false;
}
void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetOwner(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetOwner().");
	}
	if (m_Owner != item || m_Owner_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Owner);
		m_Owner = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Owner) m_Owner->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Owner) m_Owner->UseTypeAttribute = true;
		m_Owner_Exist = true;
		if(m_Owner != Dc1Ptr< Dc1Node >())
		{
			m_Owner->SetContentName(XMLString::transcode("Owner"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::InvalidateOwner()
{
	m_Owner_Exist = false;
}
void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetInformation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::SetInformation().");
	}
	if (m_Information != item || m_Information_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Information);
		m_Information = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Information) m_Information->SetParent(m_myself.getPointer());
		if (m_Information && m_Information->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Information->UseTypeAttribute = true;
		}
		m_Information_Exist = true;
		if(m_Information != Mp7JrsTextAnnotationPtr())
		{
			m_Information->SetContentName(XMLString::transcode("Information"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::InvalidateInformation()
{
	m_Information_Exist = false;
}

Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// Name is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetName(p, client);
					if((p = GetName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Owner")) == 0)
	{
		// Owner is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOwner()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOwner(p, client);
					if((p = GetOwner()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Information")) == 0)
	{
		// Information is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInformation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInformation(p, client);
					if((p = GetInformation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HandWritingRecogInformationType_Recognizer_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HandWritingRecogInformationType_Recognizer_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogInformationType_Recognizer_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Name != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Name->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Name"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Name->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Owner != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Owner->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Owner"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Owner->UseTypeAttribute = true;
		}
		m_Owner->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Information != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Information->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Information"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Information->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Owner"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Owner")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOwner(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Information"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Information")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInformation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Name")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetName(child);
  }
  if (XMLString::compareString(elementname, X("Owner")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetOwner(child);
  }
  if (XMLString::compareString(elementname, X("Information")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetInformation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetOwner() != Dc1NodePtr())
		result.Insert(GetOwner());
	if (GetInformation() != Dc1NodePtr())
		result.Insert(GetInformation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_Recognizer_LocalType_ExtMethodImpl.h


