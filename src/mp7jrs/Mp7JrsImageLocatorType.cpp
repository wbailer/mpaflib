
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtImplInclude.h


#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsMediaRelTimePointType.h"
#include "Mp7JrsMediaRelIncrTimePointType.h"
#include "Mp7JrsImageLocatorType_BytePosition_LocalType.h"
#include "Mp7JrsInlineMediaType.h"
#include "Mp7JrsImageLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsImageLocatorType::IMp7JrsImageLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtPropInit.h

}

IMp7JrsImageLocatorType::~IMp7JrsImageLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtPropCleanup.h

}

Mp7JrsImageLocatorType::Mp7JrsImageLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsImageLocatorType::~Mp7JrsImageLocatorType()
{
	Cleanup();
}

void Mp7JrsImageLocatorType::Init()
{
	// Init base
	m_Base = CreateMediaLocatorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr(); // Pattern
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr(); // Class
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr(); // Class with content 
	m_BytePosition = Mp7JrsImageLocatorType_BytePosition_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtMyPropInit.h

}

void Mp7JrsImageLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	// Dc1Factory::DeleteObject(m_BytePosition);
}

void Mp7JrsImageLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ImageLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in IImageLocatorType
	const Dc1Ptr< Mp7JrsImageLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaLocatorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsImageLocatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		this->SetMediaTimePoint(Dc1Factory::CloneObject(tmp->GetMediaTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		this->SetMediaRelTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		this->SetMediaRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelIncrTimePoint()));
		// Dc1Factory::DeleteObject(m_BytePosition);
		this->SetBytePosition(Dc1Factory::CloneObject(tmp->GetBytePosition()));
}

Dc1NodePtr Mp7JrsImageLocatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsImageLocatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMediaLocatorType > Mp7JrsImageLocatorType::GetBase() const
{
	return m_Base;
}

Mp7JrsmediaTimePointPtr Mp7JrsImageLocatorType::GetMediaTimePoint() const
{
		return m_MediaTimePoint;
}

Mp7JrsMediaRelTimePointPtr Mp7JrsImageLocatorType::GetMediaRelTimePoint() const
{
		return m_MediaRelTimePoint;
}

Mp7JrsMediaRelIncrTimePointPtr Mp7JrsImageLocatorType::GetMediaRelIncrTimePoint() const
{
		return m_MediaRelIncrTimePoint;
}

Mp7JrsImageLocatorType_BytePosition_LocalPtr Mp7JrsImageLocatorType::GetBytePosition() const
{
		return m_BytePosition;
}

// implementing setter for choice 
/* element */
void Mp7JrsImageLocatorType::SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetMediaTimePoint().");
	}
	if (m_MediaTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		m_MediaTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimePoint) m_MediaTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaTimePoint && m_MediaTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
		{
			m_MediaTimePoint->SetContentName(XMLString::transcode("MediaTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	// Dc1Factory::DeleteObject(m_BytePosition);
	m_BytePosition = Mp7JrsImageLocatorType_BytePosition_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsImageLocatorType::SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetMediaRelTimePoint().");
	}
	if (m_MediaRelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		m_MediaRelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelTimePoint) m_MediaRelTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelTimePoint && m_MediaRelTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
		{
			m_MediaRelTimePoint->SetContentName(XMLString::transcode("MediaRelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	// Dc1Factory::DeleteObject(m_BytePosition);
	m_BytePosition = Mp7JrsImageLocatorType_BytePosition_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsImageLocatorType::SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetMediaRelIncrTimePoint().");
	}
	if (m_MediaRelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		m_MediaRelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelIncrTimePoint) m_MediaRelIncrTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelIncrTimePoint && m_MediaRelIncrTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelIncrTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
		{
			m_MediaRelIncrTimePoint->SetContentName(XMLString::transcode("MediaRelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_BytePosition);
	m_BytePosition = Mp7JrsImageLocatorType_BytePosition_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsImageLocatorType::SetBytePosition(const Mp7JrsImageLocatorType_BytePosition_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetBytePosition().");
	}
	if (m_BytePosition != item)
	{
		// Dc1Factory::DeleteObject(m_BytePosition);
		m_BytePosition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BytePosition) m_BytePosition->SetParent(m_myself.getPointer());
		if (m_BytePosition && m_BytePosition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType_BytePosition_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BytePosition->UseTypeAttribute = true;
		}
		if(m_BytePosition != Mp7JrsImageLocatorType_BytePosition_LocalPtr())
		{
			m_BytePosition->SetContentName(XMLString::transcode("BytePosition"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsImageLocatorType::GetMediaUri() const
{
	return GetBase()->GetMediaUri();
}

Mp7JrsInlineMediaPtr Mp7JrsImageLocatorType::GetInlineMedia() const
{
	return GetBase()->GetInlineMedia();
}

unsigned Mp7JrsImageLocatorType::GetStreamID() const
{
	return GetBase()->GetStreamID();
}

// Element is optional
bool Mp7JrsImageLocatorType::IsValidStreamID() const
{
	return GetBase()->IsValidStreamID();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsImageLocatorType::SetMediaUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetMediaUri().");
	}
	GetBase()->SetMediaUri(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsImageLocatorType::SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetInlineMedia().");
	}
	GetBase()->SetInlineMedia(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageLocatorType::SetStreamID(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageLocatorType::SetStreamID().");
	}
	GetBase()->SetStreamID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageLocatorType::InvalidateStreamID()
{
	GetBase()->InvalidateStreamID();
}

Dc1NodeEnum Mp7JrsImageLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTimePoint")) == 0)
	{
		// MediaTimePoint is simple element mediaTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimePoint(p, client);
					if((p = GetMediaTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelTimePoint")) == 0)
	{
		// MediaRelTimePoint is simple element MediaRelTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelTimePoint(p, client);
					if((p = GetMediaRelTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelIncrTimePoint")) == 0)
	{
		// MediaRelIncrTimePoint is simple element MediaRelIncrTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelIncrTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelIncrTimePoint(p, client);
					if((p = GetMediaRelIncrTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BytePosition")) == 0)
	{
		// BytePosition is simple element ImageLocatorType_BytePosition_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBytePosition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType_BytePosition_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageLocatorType_BytePosition_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBytePosition(p, client);
					if((p = GetBytePosition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ImageLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ImageLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsImageLocatorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsImageLocatorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsImageLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ImageLocatorType"));
	// Element serialization:
	if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MediaTimePoint");
//		m_MediaTimePoint->SetContentName(contentname);
		m_MediaTimePoint->UseTypeAttribute = this->UseTypeAttribute;
		m_MediaTimePoint->Serialize(doc, element);
	}
	// Class
	
	if (m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelTimePoint->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelIncrTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelIncrTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelIncrTimePoint->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_BytePosition != Mp7JrsImageLocatorType_BytePosition_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BytePosition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BytePosition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BytePosition->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsImageLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaLocatorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimePoint")) == 0))
		{
			Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelIncrTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelIncrTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelIncrTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BytePosition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BytePosition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageLocatorType_BytePosition_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBytePosition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsImageLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsImageLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("MediaTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaTimePointType; // FTT, check this
	}
	this->SetMediaTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelTimePointType; // FTT, check this
	}
	this->SetMediaRelTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelIncrTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelIncrTimePointType; // FTT, check this
	}
	this->SetMediaRelIncrTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("BytePosition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageLocatorType_BytePosition_LocalType; // FTT, check this
	}
	this->SetBytePosition(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsImageLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaTimePoint());
	if (GetMediaRelTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelTimePoint());
	if (GetMediaRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelIncrTimePoint());
	if (GetBytePosition() != Dc1NodePtr())
		result.Insert(GetBytePosition());
	if (GetInlineMedia() != Dc1NodePtr())
		result.Insert(GetInlineMedia());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsImageLocatorType_ExtMethodImpl.h
Mp7JrsmediaTimePointPtr Mp7JrsImageLocatorType::GetMediaTimeBase(void) const
{
	return this->GetBase()->GetMediaTimeBase();
}
void Mp7JrsImageLocatorType::SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client)
{
	return this->GetBase()->SetMediaTimeBase(timePoint, client);
}
// end extension included


