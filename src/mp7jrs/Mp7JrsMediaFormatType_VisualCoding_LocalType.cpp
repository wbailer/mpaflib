
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaFormatType_VisualCoding_Format_LocalType.h"
#include "Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalType.h"
#include "Mp7JrsMediaFormatType_VisualCoding_Frame_LocalType.h"
#include "Mp7JrsColorSamplingType.h"
#include "Mp7JrsMediaFormatType_VisualCoding_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaFormatType_VisualCoding_LocalType::IMp7JrsMediaFormatType_VisualCoding_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtPropInit.h

}

IMp7JrsMediaFormatType_VisualCoding_LocalType::~IMp7JrsMediaFormatType_VisualCoding_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaFormatType_VisualCoding_LocalType::Mp7JrsMediaFormatType_VisualCoding_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaFormatType_VisualCoding_LocalType::~Mp7JrsMediaFormatType_VisualCoding_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Format = Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr(); // Class
	m_Format_Exist = false;
	m_Pixel = Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr(); // Class
	m_Pixel_Exist = false;
	m_Frame = Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr(); // Class
	m_Frame_Exist = false;
	m_ColorSampling = Mp7JrsColorSamplingPtr(); // Class
	m_ColorSampling_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Format);
	// Dc1Factory::DeleteObject(m_Pixel);
	// Dc1Factory::DeleteObject(m_Frame);
	// Dc1Factory::DeleteObject(m_ColorSampling);
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaFormatType_VisualCoding_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaFormatType_VisualCoding_LocalType
	const Dc1Ptr< Mp7JrsMediaFormatType_VisualCoding_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidFormat())
	{
		// Dc1Factory::DeleteObject(m_Format);
		this->SetFormat(Dc1Factory::CloneObject(tmp->GetFormat()));
	}
	else
	{
		InvalidateFormat();
	}
	if (tmp->IsValidPixel())
	{
		// Dc1Factory::DeleteObject(m_Pixel);
		this->SetPixel(Dc1Factory::CloneObject(tmp->GetPixel()));
	}
	else
	{
		InvalidatePixel();
	}
	if (tmp->IsValidFrame())
	{
		// Dc1Factory::DeleteObject(m_Frame);
		this->SetFrame(Dc1Factory::CloneObject(tmp->GetFrame()));
	}
	else
	{
		InvalidateFrame();
	}
	if (tmp->IsValidColorSampling())
	{
		// Dc1Factory::DeleteObject(m_ColorSampling);
		this->SetColorSampling(Dc1Factory::CloneObject(tmp->GetColorSampling()));
	}
	else
	{
		InvalidateColorSampling();
	}
}

Dc1NodePtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetFormat() const
{
		return m_Format;
}

// Element is optional
bool Mp7JrsMediaFormatType_VisualCoding_LocalType::IsValidFormat() const
{
	return m_Format_Exist;
}

Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetPixel() const
{
		return m_Pixel;
}

// Element is optional
bool Mp7JrsMediaFormatType_VisualCoding_LocalType::IsValidPixel() const
{
	return m_Pixel_Exist;
}

Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetFrame() const
{
		return m_Frame;
}

// Element is optional
bool Mp7JrsMediaFormatType_VisualCoding_LocalType::IsValidFrame() const
{
	return m_Frame_Exist;
}

Mp7JrsColorSamplingPtr Mp7JrsMediaFormatType_VisualCoding_LocalType::GetColorSampling() const
{
		return m_ColorSampling;
}

// Element is optional
bool Mp7JrsMediaFormatType_VisualCoding_LocalType::IsValidColorSampling() const
{
	return m_ColorSampling_Exist;
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::SetFormat(const Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_VisualCoding_LocalType::SetFormat().");
	}
	if (m_Format != item || m_Format_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Format);
		m_Format = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Format) m_Format->SetParent(m_myself.getPointer());
		if (m_Format && m_Format->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Format->UseTypeAttribute = true;
		}
		m_Format_Exist = true;
		if(m_Format != Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr())
		{
			m_Format->SetContentName(XMLString::transcode("Format"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::InvalidateFormat()
{
	m_Format_Exist = false;
}
void Mp7JrsMediaFormatType_VisualCoding_LocalType::SetPixel(const Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_VisualCoding_LocalType::SetPixel().");
	}
	if (m_Pixel != item || m_Pixel_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Pixel);
		m_Pixel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Pixel) m_Pixel->SetParent(m_myself.getPointer());
		if (m_Pixel && m_Pixel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Pixel_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Pixel->UseTypeAttribute = true;
		}
		m_Pixel_Exist = true;
		if(m_Pixel != Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr())
		{
			m_Pixel->SetContentName(XMLString::transcode("Pixel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::InvalidatePixel()
{
	m_Pixel_Exist = false;
}
void Mp7JrsMediaFormatType_VisualCoding_LocalType::SetFrame(const Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_VisualCoding_LocalType::SetFrame().");
	}
	if (m_Frame != item || m_Frame_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Frame);
		m_Frame = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Frame) m_Frame->SetParent(m_myself.getPointer());
		if (m_Frame && m_Frame->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Frame_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Frame->UseTypeAttribute = true;
		}
		m_Frame_Exist = true;
		if(m_Frame != Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr())
		{
			m_Frame->SetContentName(XMLString::transcode("Frame"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::InvalidateFrame()
{
	m_Frame_Exist = false;
}
void Mp7JrsMediaFormatType_VisualCoding_LocalType::SetColorSampling(const Mp7JrsColorSamplingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_VisualCoding_LocalType::SetColorSampling().");
	}
	if (m_ColorSampling != item || m_ColorSampling_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorSampling);
		m_ColorSampling = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorSampling) m_ColorSampling->SetParent(m_myself.getPointer());
		if (m_ColorSampling && m_ColorSampling->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorSampling->UseTypeAttribute = true;
		}
		m_ColorSampling_Exist = true;
		if(m_ColorSampling != Mp7JrsColorSamplingPtr())
		{
			m_ColorSampling->SetContentName(XMLString::transcode("ColorSampling"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_VisualCoding_LocalType::InvalidateColorSampling()
{
	m_ColorSampling_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaFormatType_VisualCoding_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Format")) == 0)
	{
		// Format is simple element MediaFormatType_VisualCoding_Format_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Format_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFormat(p, client);
					if((p = GetFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Pixel")) == 0)
	{
		// Pixel is simple element MediaFormatType_VisualCoding_Pixel_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPixel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Pixel_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPixel(p, client);
					if((p = GetPixel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Frame")) == 0)
	{
		// Frame is simple element MediaFormatType_VisualCoding_Frame_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFrame()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_Frame_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFrame(p, client);
					if((p = GetFrame()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ColorSampling")) == 0)
	{
		// ColorSampling is simple element ColorSamplingType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorSampling()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSamplingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorSamplingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorSampling(p, client);
					if((p = GetColorSampling()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaFormatType_VisualCoding_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaFormatType_VisualCoding_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMediaFormatType_VisualCoding_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaFormatType_VisualCoding_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaFormatType_VisualCoding_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormatType_VisualCoding_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Format != Mp7JrsMediaFormatType_VisualCoding_Format_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Format->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Format"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Format->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Pixel != Mp7JrsMediaFormatType_VisualCoding_Pixel_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Pixel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Pixel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Pixel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Frame != Mp7JrsMediaFormatType_VisualCoding_Frame_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Frame->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Frame"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Frame->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ColorSampling != Mp7JrsColorSamplingPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorSampling->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorSampling"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorSampling->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaFormatType_VisualCoding_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Format"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Format")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_VisualCoding_Format_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Pixel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Pixel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_VisualCoding_Pixel_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPixel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Frame"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Frame")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_VisualCoding_Frame_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrame(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorSampling"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorSampling")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorSamplingType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorSampling(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaFormatType_VisualCoding_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Format")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_VisualCoding_Format_LocalType; // FTT, check this
	}
	this->SetFormat(child);
  }
  if (XMLString::compareString(elementname, X("Pixel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_VisualCoding_Pixel_LocalType; // FTT, check this
	}
	this->SetPixel(child);
  }
  if (XMLString::compareString(elementname, X("Frame")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_VisualCoding_Frame_LocalType; // FTT, check this
	}
	this->SetFrame(child);
  }
  if (XMLString::compareString(elementname, X("ColorSampling")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorSamplingType; // FTT, check this
	}
	this->SetColorSampling(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaFormatType_VisualCoding_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFormat() != Dc1NodePtr())
		result.Insert(GetFormat());
	if (GetPixel() != Dc1NodePtr())
		result.Insert(GetPixel());
	if (GetFrame() != Dc1NodePtr())
		result.Insert(GetFrame());
	if (GetColorSampling() != Dc1NodePtr())
		result.Insert(GetColorSampling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_VisualCoding_LocalType_ExtMethodImpl.h


