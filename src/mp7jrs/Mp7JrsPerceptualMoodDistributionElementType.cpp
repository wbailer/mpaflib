
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrsPerceptualMoodDistributionElementType_score_LocalType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsPerceptualMoodDistributionElementType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPerceptualMoodDistributionElementType::IMp7JrsPerceptualMoodDistributionElementType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtPropInit.h

}

IMp7JrsPerceptualMoodDistributionElementType::~IMp7JrsPerceptualMoodDistributionElementType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtPropCleanup.h

}

Mp7JrsPerceptualMoodDistributionElementType::Mp7JrsPerceptualMoodDistributionElementType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptualMoodDistributionElementType::~Mp7JrsPerceptualMoodDistributionElementType()
{
	Cleanup();
}

void Mp7JrsPerceptualMoodDistributionElementType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_id = Mp7JrstermReferencePtr(); // Create emtpy class ptr
	m_score = CreatePerceptualMoodDistributionElementType_score_LocalType; // Create a valid class, FTT, check this
	m_score->SetContent(1); // Use Value initialiser (xxx2)



// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMyPropInit.h

}

void Mp7JrsPerceptualMoodDistributionElementType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_id);
	// Dc1Factory::DeleteObject(m_score);
}

void Mp7JrsPerceptualMoodDistributionElementType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PerceptualMoodDistributionElementTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptualMoodDistributionElementType
	const Dc1Ptr< Mp7JrsPerceptualMoodDistributionElementType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPerceptualMoodDistributionElementType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_id);
		this->Setid(Dc1Factory::CloneObject(tmp->Getid()));
	}
	{
	// Dc1Factory::DeleteObject(m_score);
		this->Setscore(Dc1Factory::CloneObject(tmp->Getscore()));
	}
}

Dc1NodePtr Mp7JrsPerceptualMoodDistributionElementType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPerceptualMoodDistributionElementType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsPerceptualMoodDistributionElementType::GetBase() const
{
	return m_Base;
}

Mp7JrstermReferencePtr Mp7JrsPerceptualMoodDistributionElementType::Getid() const
{
	return m_id;
}

void Mp7JrsPerceptualMoodDistributionElementType::Setid(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualMoodDistributionElementType::Setid().");
	}
	m_id = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_id) m_id->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr Mp7JrsPerceptualMoodDistributionElementType::Getscore() const
{
	return m_score;
}

void Mp7JrsPerceptualMoodDistributionElementType::Setscore(const Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualMoodDistributionElementType::Setscore().");
	}
	m_score = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_score) m_score->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsPerceptualMoodDistributionElementType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsPerceptualMoodDistributionElementType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsPerceptualMoodDistributionElementType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualMoodDistributionElementType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualMoodDistributionElementType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsPerceptualMoodDistributionElementType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("id")) == 0)
	{
		// id is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("score")) == 0)
	{
		// score is simple attribute PerceptualMoodDistributionElementType_score_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PerceptualMoodDistributionElementType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PerceptualMoodDistributionElementType");
	}
	return result;
}

XMLCh * Mp7JrsPerceptualMoodDistributionElementType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPerceptualMoodDistributionElementType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsPerceptualMoodDistributionElementType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualMoodDistributionElementType"));
	// Attribute Serialization:
	// Class
	if (m_id != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_id->ToText();
		element->setAttributeNS(X(""), X("id"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Class
	if (m_score != Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr())
	{
		XMLCh * tmp = m_score->ToText();
		element->setAttributeNS(X(""), X("score"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsPerceptualMoodDistributionElementType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("id")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("id")));
		this->Setid(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("score")))
	{
		// Deserialize class type
		Mp7JrsPerceptualMoodDistributionElementType_score_LocalPtr tmp = CreatePerceptualMoodDistributionElementType_score_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("score")));
		this->Setscore(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptualMoodDistributionElementType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptualMoodDistributionElementType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionElementType_ExtMethodImpl.h


