
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtImplInclude.h


#include "Mp7Jrsunsigned11.h"
#include "Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalType.h"
#include "Mp7JrsNonMixtureAmountOfMotionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsNonMixtureAmountOfMotionType::IMp7JrsNonMixtureAmountOfMotionType()
{

// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtPropInit.h

}

IMp7JrsNonMixtureAmountOfMotionType::~IMp7JrsNonMixtureAmountOfMotionType()
{
// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtPropCleanup.h

}

Mp7JrsNonMixtureAmountOfMotionType::Mp7JrsNonMixtureAmountOfMotionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsNonMixtureAmountOfMotionType::~Mp7JrsNonMixtureAmountOfMotionType()
{
	Cleanup();
}

void Mp7JrsNonMixtureAmountOfMotionType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TrackLeft = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_TrackRight = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_BoomDown = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_BoomUp = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_DollyForward = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_DollyBackward = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_PanLeft = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_PanRight = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_TiltDown = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_TiltUp = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_RollClockwise = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_ZoomIn = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_ZoomOut = Mp7Jrsunsigned11Ptr(); // Class with content 
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtMyPropInit.h

}

void Mp7JrsNonMixtureAmountOfMotionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TrackLeft);
	// Dc1Factory::DeleteObject(m_TrackRight);
	// Dc1Factory::DeleteObject(m_BoomDown);
	// Dc1Factory::DeleteObject(m_BoomUp);
	// Dc1Factory::DeleteObject(m_DollyForward);
	// Dc1Factory::DeleteObject(m_DollyBackward);
	// Dc1Factory::DeleteObject(m_PanLeft);
	// Dc1Factory::DeleteObject(m_PanRight);
	// Dc1Factory::DeleteObject(m_TiltDown);
	// Dc1Factory::DeleteObject(m_TiltUp);
	// Dc1Factory::DeleteObject(m_RollClockwise);
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	// Dc1Factory::DeleteObject(m_ZoomIn);
	// Dc1Factory::DeleteObject(m_ZoomOut);
	// Dc1Factory::DeleteObject(m_Fixed);
}

void Mp7JrsNonMixtureAmountOfMotionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use NonMixtureAmountOfMotionTypePtr, since we
	// might need GetBase(), which isn't defined in INonMixtureAmountOfMotionType
	const Dc1Ptr< Mp7JrsNonMixtureAmountOfMotionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_TrackLeft);
		this->SetTrackLeft(Dc1Factory::CloneObject(tmp->GetTrackLeft()));
		// Dc1Factory::DeleteObject(m_TrackRight);
		this->SetTrackRight(Dc1Factory::CloneObject(tmp->GetTrackRight()));
		// Dc1Factory::DeleteObject(m_BoomDown);
		this->SetBoomDown(Dc1Factory::CloneObject(tmp->GetBoomDown()));
		// Dc1Factory::DeleteObject(m_BoomUp);
		this->SetBoomUp(Dc1Factory::CloneObject(tmp->GetBoomUp()));
		// Dc1Factory::DeleteObject(m_DollyForward);
		this->SetDollyForward(Dc1Factory::CloneObject(tmp->GetDollyForward()));
		// Dc1Factory::DeleteObject(m_DollyBackward);
		this->SetDollyBackward(Dc1Factory::CloneObject(tmp->GetDollyBackward()));
		// Dc1Factory::DeleteObject(m_PanLeft);
		this->SetPanLeft(Dc1Factory::CloneObject(tmp->GetPanLeft()));
		// Dc1Factory::DeleteObject(m_PanRight);
		this->SetPanRight(Dc1Factory::CloneObject(tmp->GetPanRight()));
		// Dc1Factory::DeleteObject(m_TiltDown);
		this->SetTiltDown(Dc1Factory::CloneObject(tmp->GetTiltDown()));
		// Dc1Factory::DeleteObject(m_TiltUp);
		this->SetTiltUp(Dc1Factory::CloneObject(tmp->GetTiltUp()));
		// Dc1Factory::DeleteObject(m_RollClockwise);
		this->SetRollClockwise(Dc1Factory::CloneObject(tmp->GetRollClockwise()));
		// Dc1Factory::DeleteObject(m_RollAnticlockwise);
		this->SetRollAnticlockwise(Dc1Factory::CloneObject(tmp->GetRollAnticlockwise()));
		// Dc1Factory::DeleteObject(m_ZoomIn);
		this->SetZoomIn(Dc1Factory::CloneObject(tmp->GetZoomIn()));
		// Dc1Factory::DeleteObject(m_ZoomOut);
		this->SetZoomOut(Dc1Factory::CloneObject(tmp->GetZoomOut()));
		// Dc1Factory::DeleteObject(m_Fixed);
		this->SetFixed(Dc1Factory::CloneObject(tmp->GetFixed()));
}

Dc1NodePtr Mp7JrsNonMixtureAmountOfMotionType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsNonMixtureAmountOfMotionType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetTrackLeft() const
{
		return m_TrackLeft;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetTrackRight() const
{
		return m_TrackRight;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetBoomDown() const
{
		return m_BoomDown;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetBoomUp() const
{
		return m_BoomUp;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetDollyForward() const
{
		return m_DollyForward;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetDollyBackward() const
{
		return m_DollyBackward;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetPanLeft() const
{
		return m_PanLeft;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetPanRight() const
{
		return m_PanRight;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetTiltDown() const
{
		return m_TiltDown;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetTiltUp() const
{
		return m_TiltUp;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetRollClockwise() const
{
		return m_RollClockwise;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetRollAnticlockwise() const
{
		return m_RollAnticlockwise;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetZoomIn() const
{
		return m_ZoomIn;
}

Mp7Jrsunsigned11Ptr Mp7JrsNonMixtureAmountOfMotionType::GetZoomOut() const
{
		return m_ZoomOut;
}

Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr Mp7JrsNonMixtureAmountOfMotionType::GetFixed() const
{
		return m_Fixed;
}

// implementing setter for choice 
/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetTrackLeft(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetTrackLeft().");
	}
	if (m_TrackLeft != item)
	{
		// Dc1Factory::DeleteObject(m_TrackLeft);
		m_TrackLeft = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TrackLeft) m_TrackLeft->SetParent(m_myself.getPointer());
		if (m_TrackLeft && m_TrackLeft->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TrackLeft->UseTypeAttribute = true;
		}
		if(m_TrackLeft != Mp7Jrsunsigned11Ptr())
		{
			m_TrackLeft->SetContentName(XMLString::transcode("TrackLeft"));
		}
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetTrackRight(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetTrackRight().");
	}
	if (m_TrackRight != item)
	{
		// Dc1Factory::DeleteObject(m_TrackRight);
		m_TrackRight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TrackRight) m_TrackRight->SetParent(m_myself.getPointer());
		if (m_TrackRight && m_TrackRight->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TrackRight->UseTypeAttribute = true;
		}
		if(m_TrackRight != Mp7Jrsunsigned11Ptr())
		{
			m_TrackRight->SetContentName(XMLString::transcode("TrackRight"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetBoomDown(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetBoomDown().");
	}
	if (m_BoomDown != item)
	{
		// Dc1Factory::DeleteObject(m_BoomDown);
		m_BoomDown = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BoomDown) m_BoomDown->SetParent(m_myself.getPointer());
		if (m_BoomDown && m_BoomDown->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BoomDown->UseTypeAttribute = true;
		}
		if(m_BoomDown != Mp7Jrsunsigned11Ptr())
		{
			m_BoomDown->SetContentName(XMLString::transcode("BoomDown"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetBoomUp(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetBoomUp().");
	}
	if (m_BoomUp != item)
	{
		// Dc1Factory::DeleteObject(m_BoomUp);
		m_BoomUp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BoomUp) m_BoomUp->SetParent(m_myself.getPointer());
		if (m_BoomUp && m_BoomUp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BoomUp->UseTypeAttribute = true;
		}
		if(m_BoomUp != Mp7Jrsunsigned11Ptr())
		{
			m_BoomUp->SetContentName(XMLString::transcode("BoomUp"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetDollyForward(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetDollyForward().");
	}
	if (m_DollyForward != item)
	{
		// Dc1Factory::DeleteObject(m_DollyForward);
		m_DollyForward = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DollyForward) m_DollyForward->SetParent(m_myself.getPointer());
		if (m_DollyForward && m_DollyForward->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DollyForward->UseTypeAttribute = true;
		}
		if(m_DollyForward != Mp7Jrsunsigned11Ptr())
		{
			m_DollyForward->SetContentName(XMLString::transcode("DollyForward"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetDollyBackward(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetDollyBackward().");
	}
	if (m_DollyBackward != item)
	{
		// Dc1Factory::DeleteObject(m_DollyBackward);
		m_DollyBackward = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DollyBackward) m_DollyBackward->SetParent(m_myself.getPointer());
		if (m_DollyBackward && m_DollyBackward->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DollyBackward->UseTypeAttribute = true;
		}
		if(m_DollyBackward != Mp7Jrsunsigned11Ptr())
		{
			m_DollyBackward->SetContentName(XMLString::transcode("DollyBackward"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetPanLeft(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetPanLeft().");
	}
	if (m_PanLeft != item)
	{
		// Dc1Factory::DeleteObject(m_PanLeft);
		m_PanLeft = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PanLeft) m_PanLeft->SetParent(m_myself.getPointer());
		if (m_PanLeft && m_PanLeft->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PanLeft->UseTypeAttribute = true;
		}
		if(m_PanLeft != Mp7Jrsunsigned11Ptr())
		{
			m_PanLeft->SetContentName(XMLString::transcode("PanLeft"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetPanRight(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetPanRight().");
	}
	if (m_PanRight != item)
	{
		// Dc1Factory::DeleteObject(m_PanRight);
		m_PanRight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PanRight) m_PanRight->SetParent(m_myself.getPointer());
		if (m_PanRight && m_PanRight->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PanRight->UseTypeAttribute = true;
		}
		if(m_PanRight != Mp7Jrsunsigned11Ptr())
		{
			m_PanRight->SetContentName(XMLString::transcode("PanRight"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetTiltDown(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetTiltDown().");
	}
	if (m_TiltDown != item)
	{
		// Dc1Factory::DeleteObject(m_TiltDown);
		m_TiltDown = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TiltDown) m_TiltDown->SetParent(m_myself.getPointer());
		if (m_TiltDown && m_TiltDown->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TiltDown->UseTypeAttribute = true;
		}
		if(m_TiltDown != Mp7Jrsunsigned11Ptr())
		{
			m_TiltDown->SetContentName(XMLString::transcode("TiltDown"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetTiltUp(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetTiltUp().");
	}
	if (m_TiltUp != item)
	{
		// Dc1Factory::DeleteObject(m_TiltUp);
		m_TiltUp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TiltUp) m_TiltUp->SetParent(m_myself.getPointer());
		if (m_TiltUp && m_TiltUp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TiltUp->UseTypeAttribute = true;
		}
		if(m_TiltUp != Mp7Jrsunsigned11Ptr())
		{
			m_TiltUp->SetContentName(XMLString::transcode("TiltUp"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetRollClockwise(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetRollClockwise().");
	}
	if (m_RollClockwise != item)
	{
		// Dc1Factory::DeleteObject(m_RollClockwise);
		m_RollClockwise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RollClockwise) m_RollClockwise->SetParent(m_myself.getPointer());
		if (m_RollClockwise && m_RollClockwise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RollClockwise->UseTypeAttribute = true;
		}
		if(m_RollClockwise != Mp7Jrsunsigned11Ptr())
		{
			m_RollClockwise->SetContentName(XMLString::transcode("RollClockwise"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetRollAnticlockwise(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetRollAnticlockwise().");
	}
	if (m_RollAnticlockwise != item)
	{
		// Dc1Factory::DeleteObject(m_RollAnticlockwise);
		m_RollAnticlockwise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RollAnticlockwise) m_RollAnticlockwise->SetParent(m_myself.getPointer());
		if (m_RollAnticlockwise && m_RollAnticlockwise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RollAnticlockwise->UseTypeAttribute = true;
		}
		if(m_RollAnticlockwise != Mp7Jrsunsigned11Ptr())
		{
			m_RollAnticlockwise->SetContentName(XMLString::transcode("RollAnticlockwise"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetZoomIn(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetZoomIn().");
	}
	if (m_ZoomIn != item)
	{
		// Dc1Factory::DeleteObject(m_ZoomIn);
		m_ZoomIn = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ZoomIn) m_ZoomIn->SetParent(m_myself.getPointer());
		if (m_ZoomIn && m_ZoomIn->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ZoomIn->UseTypeAttribute = true;
		}
		if(m_ZoomIn != Mp7Jrsunsigned11Ptr())
		{
			m_ZoomIn->SetContentName(XMLString::transcode("ZoomIn"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetZoomOut(const Mp7Jrsunsigned11Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetZoomOut().");
	}
	if (m_ZoomOut != item)
	{
		// Dc1Factory::DeleteObject(m_ZoomOut);
		m_ZoomOut = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ZoomOut) m_ZoomOut->SetParent(m_myself.getPointer());
		if (m_ZoomOut && m_ZoomOut->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ZoomOut->UseTypeAttribute = true;
		}
		if(m_ZoomOut != Mp7Jrsunsigned11Ptr())
		{
			m_ZoomOut->SetContentName(XMLString::transcode("ZoomOut"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_Fixed);
	m_Fixed = Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsNonMixtureAmountOfMotionType::SetFixed(const Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNonMixtureAmountOfMotionType::SetFixed().");
	}
	if (m_Fixed != item)
	{
		// Dc1Factory::DeleteObject(m_Fixed);
		m_Fixed = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Fixed) m_Fixed->SetParent(m_myself.getPointer());
		if (m_Fixed && m_Fixed->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonMixtureAmountOfMotionType_Fixed_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Fixed->UseTypeAttribute = true;
		}
		if(m_Fixed != Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr())
		{
			m_Fixed->SetContentName(XMLString::transcode("Fixed"));
		}
	// Dc1Factory::DeleteObject(m_TrackLeft);
	m_TrackLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TrackRight);
	m_TrackRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomDown);
	m_BoomDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_BoomUp);
	m_BoomUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyForward);
	m_DollyForward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_DollyBackward);
	m_DollyBackward = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanLeft);
	m_PanLeft = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_PanRight);
	m_PanRight = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltDown);
	m_TiltDown = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_TiltUp);
	m_TiltUp = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollClockwise);
	m_RollClockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	m_RollAnticlockwise = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomIn);
	m_ZoomIn = Mp7Jrsunsigned11Ptr();
	// Dc1Factory::DeleteObject(m_ZoomOut);
	m_ZoomOut = Mp7Jrsunsigned11Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsNonMixtureAmountOfMotionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TrackLeft")) == 0)
	{
		// TrackLeft is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTrackLeft()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTrackLeft(p, client);
					if((p = GetTrackLeft()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TrackRight")) == 0)
	{
		// TrackRight is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTrackRight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTrackRight(p, client);
					if((p = GetTrackRight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BoomDown")) == 0)
	{
		// BoomDown is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBoomDown()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBoomDown(p, client);
					if((p = GetBoomDown()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BoomUp")) == 0)
	{
		// BoomUp is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBoomUp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBoomUp(p, client);
					if((p = GetBoomUp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DollyForward")) == 0)
	{
		// DollyForward is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDollyForward()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDollyForward(p, client);
					if((p = GetDollyForward()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DollyBackward")) == 0)
	{
		// DollyBackward is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDollyBackward()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDollyBackward(p, client);
					if((p = GetDollyBackward()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PanLeft")) == 0)
	{
		// PanLeft is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPanLeft()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPanLeft(p, client);
					if((p = GetPanLeft()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PanRight")) == 0)
	{
		// PanRight is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPanRight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPanRight(p, client);
					if((p = GetPanRight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TiltDown")) == 0)
	{
		// TiltDown is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTiltDown()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTiltDown(p, client);
					if((p = GetTiltDown()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TiltUp")) == 0)
	{
		// TiltUp is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTiltUp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTiltUp(p, client);
					if((p = GetTiltUp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RollClockwise")) == 0)
	{
		// RollClockwise is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRollClockwise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRollClockwise(p, client);
					if((p = GetRollClockwise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RollAnticlockwise")) == 0)
	{
		// RollAnticlockwise is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRollAnticlockwise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRollAnticlockwise(p, client);
					if((p = GetRollAnticlockwise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ZoomIn")) == 0)
	{
		// ZoomIn is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetZoomIn()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetZoomIn(p, client);
					if((p = GetZoomIn()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ZoomOut")) == 0)
	{
		// ZoomOut is simple element unsigned11
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetZoomOut()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned11")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned11Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetZoomOut(p, client);
					if((p = GetZoomOut()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Fixed")) == 0)
	{
		// Fixed is simple element NonMixtureAmountOfMotionType_Fixed_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFixed()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonMixtureAmountOfMotionType_Fixed_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFixed(p, client);
					if((p = GetFixed()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for NonMixtureAmountOfMotionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "NonMixtureAmountOfMotionType");
	}
	return result;
}

XMLCh * Mp7JrsNonMixtureAmountOfMotionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsNonMixtureAmountOfMotionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsNonMixtureAmountOfMotionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("NonMixtureAmountOfMotionType"));
	// Element serialization:
	// Class with content		
	
	if (m_TrackLeft != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TrackLeft->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TrackLeft"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TrackLeft->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TrackRight != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TrackRight->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TrackRight"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TrackRight->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_BoomDown != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BoomDown->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BoomDown"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BoomDown->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_BoomUp != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BoomUp->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BoomUp"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BoomUp->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_DollyForward != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DollyForward->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DollyForward"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DollyForward->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_DollyBackward != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DollyBackward->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DollyBackward"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DollyBackward->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PanLeft != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PanLeft->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PanLeft"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PanLeft->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PanRight != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PanRight->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PanRight"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PanRight->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TiltDown != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TiltDown->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TiltDown"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TiltDown->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TiltUp != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TiltUp->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TiltUp"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TiltUp->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_RollClockwise != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RollClockwise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RollClockwise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RollClockwise->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_RollAnticlockwise != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RollAnticlockwise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RollAnticlockwise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RollAnticlockwise->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_ZoomIn != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ZoomIn->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ZoomIn"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ZoomIn->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_ZoomOut != Mp7Jrsunsigned11Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ZoomOut->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ZoomOut"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ZoomOut->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Fixed != Mp7JrsNonMixtureAmountOfMotionType_Fixed_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Fixed->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Fixed"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Fixed->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsNonMixtureAmountOfMotionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TrackLeft"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TrackLeft")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTrackLeft(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TrackRight"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TrackRight")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTrackRight(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BoomDown"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BoomDown")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBoomDown(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BoomUp"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BoomUp")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBoomUp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DollyForward"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DollyForward")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDollyForward(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DollyBackward"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DollyBackward")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDollyBackward(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PanLeft"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PanLeft")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPanLeft(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PanRight"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PanRight")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPanRight(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TiltDown"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TiltDown")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTiltDown(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TiltUp"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TiltUp")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTiltUp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RollClockwise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RollClockwise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRollClockwise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RollAnticlockwise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RollAnticlockwise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRollAnticlockwise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ZoomIn"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ZoomIn")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetZoomIn(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ZoomOut"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ZoomOut")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned11; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetZoomOut(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Fixed"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Fixed")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNonMixtureAmountOfMotionType_Fixed_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFixed(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsNonMixtureAmountOfMotionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TrackLeft")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetTrackLeft(child);
  }
  if (XMLString::compareString(elementname, X("TrackRight")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetTrackRight(child);
  }
  if (XMLString::compareString(elementname, X("BoomDown")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetBoomDown(child);
  }
  if (XMLString::compareString(elementname, X("BoomUp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetBoomUp(child);
  }
  if (XMLString::compareString(elementname, X("DollyForward")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetDollyForward(child);
  }
  if (XMLString::compareString(elementname, X("DollyBackward")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetDollyBackward(child);
  }
  if (XMLString::compareString(elementname, X("PanLeft")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetPanLeft(child);
  }
  if (XMLString::compareString(elementname, X("PanRight")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetPanRight(child);
  }
  if (XMLString::compareString(elementname, X("TiltDown")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetTiltDown(child);
  }
  if (XMLString::compareString(elementname, X("TiltUp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetTiltUp(child);
  }
  if (XMLString::compareString(elementname, X("RollClockwise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetRollClockwise(child);
  }
  if (XMLString::compareString(elementname, X("RollAnticlockwise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetRollAnticlockwise(child);
  }
  if (XMLString::compareString(elementname, X("ZoomIn")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetZoomIn(child);
  }
  if (XMLString::compareString(elementname, X("ZoomOut")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned11; // FTT, check this
	}
	this->SetZoomOut(child);
  }
  if (XMLString::compareString(elementname, X("Fixed")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNonMixtureAmountOfMotionType_Fixed_LocalType; // FTT, check this
	}
	this->SetFixed(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsNonMixtureAmountOfMotionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTrackLeft() != Dc1NodePtr())
		result.Insert(GetTrackLeft());
	if (GetTrackRight() != Dc1NodePtr())
		result.Insert(GetTrackRight());
	if (GetBoomDown() != Dc1NodePtr())
		result.Insert(GetBoomDown());
	if (GetBoomUp() != Dc1NodePtr())
		result.Insert(GetBoomUp());
	if (GetDollyForward() != Dc1NodePtr())
		result.Insert(GetDollyForward());
	if (GetDollyBackward() != Dc1NodePtr())
		result.Insert(GetDollyBackward());
	if (GetPanLeft() != Dc1NodePtr())
		result.Insert(GetPanLeft());
	if (GetPanRight() != Dc1NodePtr())
		result.Insert(GetPanRight());
	if (GetTiltDown() != Dc1NodePtr())
		result.Insert(GetTiltDown());
	if (GetTiltUp() != Dc1NodePtr())
		result.Insert(GetTiltUp());
	if (GetRollClockwise() != Dc1NodePtr())
		result.Insert(GetRollClockwise());
	if (GetRollAnticlockwise() != Dc1NodePtr())
		result.Insert(GetRollAnticlockwise());
	if (GetZoomIn() != Dc1NodePtr())
		result.Insert(GetZoomIn());
	if (GetZoomOut() != Dc1NodePtr())
		result.Insert(GetZoomOut());
	if (GetFixed() != Dc1NodePtr())
		result.Insert(GetFixed());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsNonMixtureAmountOfMotionType_ExtMethodImpl.h


