
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsInkMediaInformationType_InputDevice_LocalType.h"
#include "Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalType.h"
#include "Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionType.h"
#include "Mp7JrsInkMediaInformationType_Brush_CollectionType.h"
#include "Mp7JrsInkMediaInformationType_Param_CollectionType.h"
#include "Mp7JrsInkMediaInformationType_Style_LocalType2.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsInkMediaInformationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsInkMediaInformationType_OverlaidMedia_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OverlaidMedia
#include "Mp7JrsInkMediaInformationType_Brush_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Brush
#include "Mp7JrsInkMediaInformationType_Param_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Param

#include <assert.h>
IMp7JrsInkMediaInformationType::IMp7JrsInkMediaInformationType()
{

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtPropInit.h

}

IMp7JrsInkMediaInformationType::~IMp7JrsInkMediaInformationType()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtPropCleanup.h

}

Mp7JrsInkMediaInformationType::Mp7JrsInkMediaInformationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInkMediaInformationType::~Mp7JrsInkMediaInformationType()
{
	Cleanup();
}

void Mp7JrsInkMediaInformationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_InputDevice = Mp7JrsInkMediaInformationType_InputDevice_LocalPtr(); // Class
	m_InputDevice_Exist = false;
	m_WritingFieldLayout = Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr(); // Class
	m_WritingFieldLayout_Exist = false;
	m_OverlaidMedia = Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr(); // Collection
	m_Brush = Mp7JrsInkMediaInformationType_Brush_CollectionPtr(); // Collection
	m_Param = Mp7JrsInkMediaInformationType_Param_CollectionPtr(); // Collection
	m_Handedness = Mp7JrsInkMediaInformationType_Handedness_LocalType::UninitializedEnumeration; // Enumeration
	m_Handedness_Exist = false;
	m_Style = Mp7JrsInkMediaInformationType_Style_Local2Ptr(); // Class
	m_Style_Exist = false;


// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtMyPropInit.h

}

void Mp7JrsInkMediaInformationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_InputDevice);
	// Dc1Factory::DeleteObject(m_WritingFieldLayout);
	// Dc1Factory::DeleteObject(m_OverlaidMedia);
	// Dc1Factory::DeleteObject(m_Brush);
	// Dc1Factory::DeleteObject(m_Param);
	// Dc1Factory::DeleteObject(m_Style);
}

void Mp7JrsInkMediaInformationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InkMediaInformationTypePtr, since we
	// might need GetBase(), which isn't defined in IInkMediaInformationType
	const Dc1Ptr< Mp7JrsInkMediaInformationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsInkMediaInformationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidInputDevice())
	{
		// Dc1Factory::DeleteObject(m_InputDevice);
		this->SetInputDevice(Dc1Factory::CloneObject(tmp->GetInputDevice()));
	}
	else
	{
		InvalidateInputDevice();
	}
	if (tmp->IsValidWritingFieldLayout())
	{
		// Dc1Factory::DeleteObject(m_WritingFieldLayout);
		this->SetWritingFieldLayout(Dc1Factory::CloneObject(tmp->GetWritingFieldLayout()));
	}
	else
	{
		InvalidateWritingFieldLayout();
	}
		// Dc1Factory::DeleteObject(m_OverlaidMedia);
		this->SetOverlaidMedia(Dc1Factory::CloneObject(tmp->GetOverlaidMedia()));
		// Dc1Factory::DeleteObject(m_Brush);
		this->SetBrush(Dc1Factory::CloneObject(tmp->GetBrush()));
		// Dc1Factory::DeleteObject(m_Param);
		this->SetParam(Dc1Factory::CloneObject(tmp->GetParam()));
	if (tmp->IsValidHandedness())
	{
		this->SetHandedness(tmp->GetHandedness());
	}
	else
	{
		InvalidateHandedness();
	}
	if (tmp->IsValidStyle())
	{
		// Dc1Factory::DeleteObject(m_Style);
		this->SetStyle(Dc1Factory::CloneObject(tmp->GetStyle()));
	}
	else
	{
		InvalidateStyle();
	}
}

Dc1NodePtr Mp7JrsInkMediaInformationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsInkMediaInformationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsInkMediaInformationType::GetBase() const
{
	return m_Base;
}

Mp7JrsInkMediaInformationType_InputDevice_LocalPtr Mp7JrsInkMediaInformationType::GetInputDevice() const
{
		return m_InputDevice;
}

// Element is optional
bool Mp7JrsInkMediaInformationType::IsValidInputDevice() const
{
	return m_InputDevice_Exist;
}

Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr Mp7JrsInkMediaInformationType::GetWritingFieldLayout() const
{
		return m_WritingFieldLayout;
}

// Element is optional
bool Mp7JrsInkMediaInformationType::IsValidWritingFieldLayout() const
{
	return m_WritingFieldLayout_Exist;
}

Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr Mp7JrsInkMediaInformationType::GetOverlaidMedia() const
{
		return m_OverlaidMedia;
}

Mp7JrsInkMediaInformationType_Brush_CollectionPtr Mp7JrsInkMediaInformationType::GetBrush() const
{
		return m_Brush;
}

Mp7JrsInkMediaInformationType_Param_CollectionPtr Mp7JrsInkMediaInformationType::GetParam() const
{
		return m_Param;
}

Mp7JrsInkMediaInformationType_Handedness_LocalType::Enumeration Mp7JrsInkMediaInformationType::GetHandedness() const
{
		return m_Handedness;
}

// Element is optional
bool Mp7JrsInkMediaInformationType::IsValidHandedness() const
{
	return m_Handedness_Exist;
}

Mp7JrsInkMediaInformationType_Style_Local2Ptr Mp7JrsInkMediaInformationType::GetStyle() const
{
		return m_Style;
}

// Element is optional
bool Mp7JrsInkMediaInformationType::IsValidStyle() const
{
	return m_Style_Exist;
}

void Mp7JrsInkMediaInformationType::SetInputDevice(const Mp7JrsInkMediaInformationType_InputDevice_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetInputDevice().");
	}
	if (m_InputDevice != item || m_InputDevice_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_InputDevice);
		m_InputDevice = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InputDevice) m_InputDevice->SetParent(m_myself.getPointer());
		if (m_InputDevice && m_InputDevice->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_InputDevice_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InputDevice->UseTypeAttribute = true;
		}
		m_InputDevice_Exist = true;
		if(m_InputDevice != Mp7JrsInkMediaInformationType_InputDevice_LocalPtr())
		{
			m_InputDevice->SetContentName(XMLString::transcode("InputDevice"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidateInputDevice()
{
	m_InputDevice_Exist = false;
}
void Mp7JrsInkMediaInformationType::SetWritingFieldLayout(const Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetWritingFieldLayout().");
	}
	if (m_WritingFieldLayout != item || m_WritingFieldLayout_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_WritingFieldLayout);
		m_WritingFieldLayout = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WritingFieldLayout) m_WritingFieldLayout->SetParent(m_myself.getPointer());
		if (m_WritingFieldLayout && m_WritingFieldLayout->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_WritingFieldLayout_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_WritingFieldLayout->UseTypeAttribute = true;
		}
		m_WritingFieldLayout_Exist = true;
		if(m_WritingFieldLayout != Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr())
		{
			m_WritingFieldLayout->SetContentName(XMLString::transcode("WritingFieldLayout"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidateWritingFieldLayout()
{
	m_WritingFieldLayout_Exist = false;
}
void Mp7JrsInkMediaInformationType::SetOverlaidMedia(const Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetOverlaidMedia().");
	}
	if (m_OverlaidMedia != item)
	{
		// Dc1Factory::DeleteObject(m_OverlaidMedia);
		m_OverlaidMedia = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_OverlaidMedia) m_OverlaidMedia->SetParent(m_myself.getPointer());
		if(m_OverlaidMedia != Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr())
		{
			m_OverlaidMedia->SetContentName(XMLString::transcode("OverlaidMedia"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::SetBrush(const Mp7JrsInkMediaInformationType_Brush_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetBrush().");
	}
	if (m_Brush != item)
	{
		// Dc1Factory::DeleteObject(m_Brush);
		m_Brush = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Brush) m_Brush->SetParent(m_myself.getPointer());
		if(m_Brush != Mp7JrsInkMediaInformationType_Brush_CollectionPtr())
		{
			m_Brush->SetContentName(XMLString::transcode("Brush"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::SetParam(const Mp7JrsInkMediaInformationType_Param_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetParam().");
	}
	if (m_Param != item)
	{
		// Dc1Factory::DeleteObject(m_Param);
		m_Param = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Param) m_Param->SetParent(m_myself.getPointer());
		if(m_Param != Mp7JrsInkMediaInformationType_Param_CollectionPtr())
		{
			m_Param->SetContentName(XMLString::transcode("Param"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::SetHandedness(Mp7JrsInkMediaInformationType_Handedness_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetHandedness().");
	}
	if (m_Handedness != item || m_Handedness_Exist == false)
	{
		// nothing to free here, hopefully
		m_Handedness = item;
		m_Handedness_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidateHandedness()
{
	m_Handedness_Exist = false;
}
void Mp7JrsInkMediaInformationType::SetStyle(const Mp7JrsInkMediaInformationType_Style_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetStyle().");
	}
	if (m_Style != item || m_Style_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Style);
		m_Style = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Style) m_Style->SetParent(m_myself.getPointer());
		if (m_Style && m_Style->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType2"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Style->UseTypeAttribute = true;
		}
		m_Style_Exist = true;
		if(m_Style != Mp7JrsInkMediaInformationType_Style_Local2Ptr())
		{
			m_Style->SetContentName(XMLString::transcode("Style"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidateStyle()
{
	m_Style_Exist = false;
}
XMLCh * Mp7JrsInkMediaInformationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsInkMediaInformationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsInkMediaInformationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsInkMediaInformationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsInkMediaInformationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsInkMediaInformationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsInkMediaInformationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsInkMediaInformationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsInkMediaInformationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsInkMediaInformationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsInkMediaInformationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsInkMediaInformationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsInkMediaInformationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsInkMediaInformationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsInkMediaInformationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsInkMediaInformationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsInkMediaInformationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsInkMediaInformationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("InputDevice")) == 0)
	{
		// InputDevice is simple element InkMediaInformationType_InputDevice_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInputDevice()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_InputDevice_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_InputDevice_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInputDevice(p, client);
					if((p = GetInputDevice()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("WritingFieldLayout")) == 0)
	{
		// WritingFieldLayout is simple element InkMediaInformationType_WritingFieldLayout_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWritingFieldLayout()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_WritingFieldLayout_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWritingFieldLayout(p, client);
					if((p = GetWritingFieldLayout()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("OverlaidMedia")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:OverlaidMedia is item of type InkMediaInformationType_OverlaidMedia_LocalType
		// in element collection InkMediaInformationType_OverlaidMedia_CollectionType
		
		context->Found = true;
		Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr coll = GetOverlaidMedia();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInkMediaInformationType_OverlaidMedia_CollectionType; // FTT, check this
				SetOverlaidMedia(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_OverlaidMedia_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_OverlaidMedia_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Brush")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Brush is item of type InkMediaInformationType_Brush_LocalType
		// in element collection InkMediaInformationType_Brush_CollectionType
		
		context->Found = true;
		Mp7JrsInkMediaInformationType_Brush_CollectionPtr coll = GetBrush();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInkMediaInformationType_Brush_CollectionType; // FTT, check this
				SetBrush(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Brush_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_Brush_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Param")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Param is item of type InkMediaInformationType_Param_LocalType
		// in element collection InkMediaInformationType_Param_CollectionType
		
		context->Found = true;
		Mp7JrsInkMediaInformationType_Param_CollectionPtr coll = GetParam();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateInkMediaInformationType_Param_CollectionType; // FTT, check this
				SetParam(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Param_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_Param_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Style")) == 0)
	{
		// Style is simple element InkMediaInformationType_Style_LocalType2
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStyle()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InkMediaInformationType_Style_LocalType2")))) != empty)
			{
				// Is type allowed
				Mp7JrsInkMediaInformationType_Style_Local2Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStyle(p, client);
					if((p = GetStyle()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for InkMediaInformationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "InkMediaInformationType");
	}
	return result;
}

XMLCh * Mp7JrsInkMediaInformationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsInkMediaInformationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsInkMediaInformationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InkMediaInformationType"));
	// Element serialization:
	// Class
	
	if (m_InputDevice != Mp7JrsInkMediaInformationType_InputDevice_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InputDevice->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InputDevice"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InputDevice->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_WritingFieldLayout != Mp7JrsInkMediaInformationType_WritingFieldLayout_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_WritingFieldLayout->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("WritingFieldLayout"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_WritingFieldLayout->Serialize(doc, element, &elem);
	}
	if (m_OverlaidMedia != Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_OverlaidMedia->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Brush != Mp7JrsInkMediaInformationType_Brush_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Brush->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Param != Mp7JrsInkMediaInformationType_Param_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Param->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_Handedness != Mp7JrsInkMediaInformationType_Handedness_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsInkMediaInformationType_Handedness_LocalType::ToText(m_Handedness);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Handedness"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_Style != Mp7JrsInkMediaInformationType_Style_Local2Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Style->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Style"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Style->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsInkMediaInformationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InputDevice"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InputDevice")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateInkMediaInformationType_InputDevice_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInputDevice(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("WritingFieldLayout"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("WritingFieldLayout")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateInkMediaInformationType_WritingFieldLayout_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetWritingFieldLayout(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("OverlaidMedia"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("OverlaidMedia")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr tmp = CreateInkMediaInformationType_OverlaidMedia_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOverlaidMedia(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Brush"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Brush")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInkMediaInformationType_Brush_CollectionPtr tmp = CreateInkMediaInformationType_Brush_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetBrush(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Param"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Param")) == 0))
		{
			// Deserialize factory type
			Mp7JrsInkMediaInformationType_Param_CollectionPtr tmp = CreateInkMediaInformationType_Param_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetParam(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Handedness"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Handedness")) == 0))
		{
			Mp7JrsInkMediaInformationType_Handedness_LocalType::Enumeration tmp = Mp7JrsInkMediaInformationType_Handedness_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsInkMediaInformationType_Handedness_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetHandedness(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Style"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Style")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateInkMediaInformationType_Style_LocalType2; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStyle(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInkMediaInformationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("InputDevice")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateInkMediaInformationType_InputDevice_LocalType; // FTT, check this
	}
	this->SetInputDevice(child);
  }
  if (XMLString::compareString(elementname, X("WritingFieldLayout")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateInkMediaInformationType_WritingFieldLayout_LocalType; // FTT, check this
	}
	this->SetWritingFieldLayout(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("OverlaidMedia")) == 0))
  {
	Mp7JrsInkMediaInformationType_OverlaidMedia_CollectionPtr tmp = CreateInkMediaInformationType_OverlaidMedia_CollectionType; // FTT, check this
	this->SetOverlaidMedia(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Brush")) == 0))
  {
	Mp7JrsInkMediaInformationType_Brush_CollectionPtr tmp = CreateInkMediaInformationType_Brush_CollectionType; // FTT, check this
	this->SetBrush(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Param")) == 0))
  {
	Mp7JrsInkMediaInformationType_Param_CollectionPtr tmp = CreateInkMediaInformationType_Param_CollectionType; // FTT, check this
	this->SetParam(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Style")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateInkMediaInformationType_Style_LocalType2; // FTT, check this
	}
	this->SetStyle(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsInkMediaInformationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInputDevice() != Dc1NodePtr())
		result.Insert(GetInputDevice());
	if (GetWritingFieldLayout() != Dc1NodePtr())
		result.Insert(GetWritingFieldLayout());
	if (GetOverlaidMedia() != Dc1NodePtr())
		result.Insert(GetOverlaidMedia());
	if (GetBrush() != Dc1NodePtr())
		result.Insert(GetBrush());
	if (GetParam() != Dc1NodePtr())
		result.Insert(GetParam());
	if (GetStyle() != Dc1NodePtr())
		result.Insert(GetStyle());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_ExtMethodImpl.h


