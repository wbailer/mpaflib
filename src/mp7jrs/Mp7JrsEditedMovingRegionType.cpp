
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtImplInclude.h


#include "Mp7JrsMovingRegionType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsSpatioTemporalLocatorType.h"
#include "Mp7JrsSpatioTemporalMaskType.h"
#include "Mp7JrsMovingRegionType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsMovingRegionType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsEditedMovingRegionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsMovingRegionType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsVisualTimeSeriesType.h" // Choice collection element VisualTimeSeriesDescriptor
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsMovingRegionType_LocalType0.h" // Choice collection SpatialDecomposition
#include "Mp7JrsMovingRegionSpatialDecompositionType.h" // Choice collection element SpatialDecomposition
#include "Mp7JrsMovingRegionTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsMovingRegionSpatioTemporalDecompositionType.h" // Choice collection element SpatioTemporalDecomposition
#include "Mp7JrsMovingRegionMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition

#include <assert.h>
IMp7JrsEditedMovingRegionType::IMp7JrsEditedMovingRegionType()
{

// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtPropInit.h

}

IMp7JrsEditedMovingRegionType::~IMp7JrsEditedMovingRegionType()
{
// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtPropCleanup.h

}

Mp7JrsEditedMovingRegionType::Mp7JrsEditedMovingRegionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsEditedMovingRegionType::~Mp7JrsEditedMovingRegionType()
{
	Cleanup();
}

void Mp7JrsEditedMovingRegionType::Init()
{
	// Init base
	m_Base = CreateMovingRegionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_RegionDepthType = Mp7JrsTermUsePtr(); // Class
	m_RegionDepthType_Exist = false;
	m_BoundaryType = Mp7JrsTermUsePtr(); // Class
	m_BoundaryType_Exist = false;


// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtMyPropInit.h

}

void Mp7JrsEditedMovingRegionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_RegionDepthType);
	// Dc1Factory::DeleteObject(m_BoundaryType);
}

void Mp7JrsEditedMovingRegionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use EditedMovingRegionTypePtr, since we
	// might need GetBase(), which isn't defined in IEditedMovingRegionType
	const Dc1Ptr< Mp7JrsEditedMovingRegionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMovingRegionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsEditedMovingRegionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidRegionDepthType())
	{
		// Dc1Factory::DeleteObject(m_RegionDepthType);
		this->SetRegionDepthType(Dc1Factory::CloneObject(tmp->GetRegionDepthType()));
	}
	else
	{
		InvalidateRegionDepthType();
	}
	if (tmp->IsValidBoundaryType())
	{
		// Dc1Factory::DeleteObject(m_BoundaryType);
		this->SetBoundaryType(Dc1Factory::CloneObject(tmp->GetBoundaryType()));
	}
	else
	{
		InvalidateBoundaryType();
	}
}

Dc1NodePtr Mp7JrsEditedMovingRegionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsEditedMovingRegionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMovingRegionType > Mp7JrsEditedMovingRegionType::GetBase() const
{
	return m_Base;
}

Mp7JrsTermUsePtr Mp7JrsEditedMovingRegionType::GetRegionDepthType() const
{
		return m_RegionDepthType;
}

// Element is optional
bool Mp7JrsEditedMovingRegionType::IsValidRegionDepthType() const
{
	return m_RegionDepthType_Exist;
}

Mp7JrsTermUsePtr Mp7JrsEditedMovingRegionType::GetBoundaryType() const
{
		return m_BoundaryType;
}

// Element is optional
bool Mp7JrsEditedMovingRegionType::IsValidBoundaryType() const
{
	return m_BoundaryType_Exist;
}

void Mp7JrsEditedMovingRegionType::SetRegionDepthType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetRegionDepthType().");
	}
	if (m_RegionDepthType != item || m_RegionDepthType_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RegionDepthType);
		m_RegionDepthType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RegionDepthType) m_RegionDepthType->SetParent(m_myself.getPointer());
		if (m_RegionDepthType && m_RegionDepthType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RegionDepthType->UseTypeAttribute = true;
		}
		m_RegionDepthType_Exist = true;
		if(m_RegionDepthType != Mp7JrsTermUsePtr())
		{
			m_RegionDepthType->SetContentName(XMLString::transcode("RegionDepthType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidateRegionDepthType()
{
	m_RegionDepthType_Exist = false;
}
void Mp7JrsEditedMovingRegionType::SetBoundaryType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetBoundaryType().");
	}
	if (m_BoundaryType != item || m_BoundaryType_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BoundaryType);
		m_BoundaryType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BoundaryType) m_BoundaryType->SetParent(m_myself.getPointer());
		if (m_BoundaryType && m_BoundaryType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BoundaryType->UseTypeAttribute = true;
		}
		m_BoundaryType_Exist = true;
		if(m_BoundaryType != Mp7JrsTermUsePtr())
		{
			m_BoundaryType->SetContentName(XMLString::transcode("BoundaryType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidateBoundaryType()
{
	m_BoundaryType_Exist = false;
}
Mp7JrsSpatioTemporalLocatorPtr Mp7JrsEditedMovingRegionType::GetSpatioTemporalLocator() const
{
	return GetBase()->GetSpatioTemporalLocator();
}

Mp7JrsSpatioTemporalMaskPtr Mp7JrsEditedMovingRegionType::GetSpatioTemporalMask() const
{
	return GetBase()->GetSpatioTemporalMask();
}

Mp7JrsMovingRegionType_CollectionPtr Mp7JrsEditedMovingRegionType::GetMovingRegionType_LocalType() const
{
	return GetBase()->GetMovingRegionType_LocalType();
}

Mp7JrsMultipleViewPtr Mp7JrsEditedMovingRegionType::GetMultipleView() const
{
	return GetBase()->GetMultipleView();
}

// Element is optional
bool Mp7JrsEditedMovingRegionType::IsValidMultipleView() const
{
	return GetBase()->IsValidMultipleView();
}

Mp7JrsMovingRegionType_Collection0Ptr Mp7JrsEditedMovingRegionType::GetMovingRegionType_LocalType0() const
{
	return GetBase()->GetMovingRegionType_LocalType0();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsEditedMovingRegionType::SetSpatioTemporalLocator(const Mp7JrsSpatioTemporalLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetSpatioTemporalLocator().");
	}
	GetBase()->SetSpatioTemporalLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedMovingRegionType::SetSpatioTemporalMask(const Mp7JrsSpatioTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetSpatioTemporalMask().");
	}
	GetBase()->SetSpatioTemporalMask(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetMovingRegionType_LocalType(const Mp7JrsMovingRegionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMovingRegionType_LocalType().");
	}
	GetBase()->SetMovingRegionType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMultipleView().");
	}
	GetBase()->SetMultipleView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidateMultipleView()
{
	GetBase()->InvalidateMultipleView();
}
void Mp7JrsEditedMovingRegionType::SetMovingRegionType_LocalType0(const Mp7JrsMovingRegionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMovingRegionType_LocalType0().");
	}
	GetBase()->SetMovingRegionType_LocalType0(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsEditedMovingRegionType::GetMediaInformation() const
{
	return GetBase()->GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsEditedMovingRegionType::GetMediaInformationRef() const
{
	return GetBase()->GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsEditedMovingRegionType::GetMediaLocator() const
{
	return GetBase()->GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsEditedMovingRegionType::GetStructuralUnit() const
{
	return GetBase()->GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsEditedMovingRegionType::IsValidStructuralUnit() const
{
	return GetBase()->GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsEditedMovingRegionType::GetCreationInformation() const
{
	return GetBase()->GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsEditedMovingRegionType::GetCreationInformationRef() const
{
	return GetBase()->GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsEditedMovingRegionType::GetUsageInformation() const
{
	return GetBase()->GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsEditedMovingRegionType::GetUsageInformationRef() const
{
	return GetBase()->GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsEditedMovingRegionType::GetTextAnnotation() const
{
	return GetBase()->GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsEditedMovingRegionType::GetSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsEditedMovingRegionType::GetMatchingHint() const
{
	return GetBase()->GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsEditedMovingRegionType::GetPointOfView() const
{
	return GetBase()->GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsEditedMovingRegionType::GetRelation() const
{
	return GetBase()->GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsEditedMovingRegionType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMediaInformation().");
	}
	GetBase()->GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedMovingRegionType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMediaInformationRef().");
	}
	GetBase()->GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedMovingRegionType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMediaLocator().");
	}
	GetBase()->GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetStructuralUnit().");
	}
	GetBase()->GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidateStructuralUnit()
{
	GetBase()->GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsEditedMovingRegionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetCreationInformation().");
	}
	GetBase()->GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedMovingRegionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetCreationInformationRef().");
	}
	GetBase()->GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsEditedMovingRegionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetUsageInformation().");
	}
	GetBase()->GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsEditedMovingRegionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetUsageInformationRef().");
	}
	GetBase()->GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetTextAnnotation().");
	}
	GetBase()->GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetSegmentType_LocalType().");
	}
	GetBase()->GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetMatchingHint().");
	}
	GetBase()->GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetPointOfView().");
	}
	GetBase()->GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetRelation().");
	}
	GetBase()->GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsEditedMovingRegionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsEditedMovingRegionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsEditedMovingRegionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsEditedMovingRegionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsEditedMovingRegionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsEditedMovingRegionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsEditedMovingRegionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsEditedMovingRegionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsEditedMovingRegionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsEditedMovingRegionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsEditedMovingRegionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsEditedMovingRegionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsEditedMovingRegionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsEditedMovingRegionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsEditedMovingRegionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedMovingRegionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsEditedMovingRegionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsEditedMovingRegionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedMovingRegionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsEditedMovingRegionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("RegionDepthType")) == 0)
	{
		// RegionDepthType is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRegionDepthType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRegionDepthType(p, client);
					if((p = GetRegionDepthType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BoundaryType")) == 0)
	{
		// BoundaryType is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBoundaryType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBoundaryType(p, client);
					if((p = GetBoundaryType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for EditedMovingRegionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "EditedMovingRegionType");
	}
	return result;
}

XMLCh * Mp7JrsEditedMovingRegionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsEditedMovingRegionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsEditedMovingRegionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("EditedMovingRegionType"));
	// Element serialization:
	// Class
	
	if (m_RegionDepthType != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RegionDepthType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RegionDepthType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RegionDepthType->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_BoundaryType != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BoundaryType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BoundaryType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BoundaryType->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsEditedMovingRegionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMovingRegionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RegionDepthType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RegionDepthType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRegionDepthType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BoundaryType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BoundaryType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBoundaryType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsEditedMovingRegionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("RegionDepthType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetRegionDepthType(child);
  }
  if (XMLString::compareString(elementname, X("BoundaryType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetBoundaryType(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsEditedMovingRegionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRegionDepthType() != Dc1NodePtr())
		result.Insert(GetRegionDepthType());
	if (GetBoundaryType() != Dc1NodePtr())
		result.Insert(GetBoundaryType());
	if (GetMovingRegionType_LocalType() != Dc1NodePtr())
		result.Insert(GetMovingRegionType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetMovingRegionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetMovingRegionType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetSpatioTemporalLocator() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalLocator());
	if (GetSpatioTemporalMask() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsEditedMovingRegionType_ExtMethodImpl.h


