
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtImplInclude.h


#include "Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType.h"
#include "Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrssummaryComponentType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Summary

#include <assert.h>
IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtPropInit.h

}

IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::~IMp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtPropCleanup.h

}

Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::~Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType()
{
	Cleanup();
}

void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_OriginalContents = XMLString::transcode(""); //  Mandatory String
	m_Summary = Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtMyPropInit.h

}

void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtMyPropCleanup.h


	XMLString::release(&this->m_OriginalContents);
	this->m_OriginalContents = NULL;
	// Dc1Factory::DeleteObject(m_Summary);
}

void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RecordingRequestType_RecordingSourceTypePreferences_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IRecordingRequestType_RecordingSourceTypePreferences_LocalType
	const Dc1Ptr< Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetOriginalContents(XMLString::replicate(tmp->GetOriginalContents()));
		// Dc1Factory::DeleteObject(m_Summary);
		this->SetSummary(Dc1Factory::CloneObject(tmp->GetSummary()));
}

Dc1NodePtr Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetOriginalContents() const
{
		return m_OriginalContents;
}

Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetSummary() const
{
		return m_Summary;
}

// implementing setter for choice 
/* element */
void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::SetOriginalContents(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::SetOriginalContents().");
	}
	if (m_OriginalContents != item)
	{
		XMLString::release(&m_OriginalContents);
		m_OriginalContents = item;
	// Dc1Factory::DeleteObject(m_Summary);
	m_Summary = Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::SetSummary(const Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::SetSummary().");
	}
	if (m_Summary != item)
	{
		// Dc1Factory::DeleteObject(m_Summary);
		m_Summary = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Summary) m_Summary->SetParent(m_myself.getPointer());
		if(m_Summary != Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr())
		{
			m_Summary->SetContentName(XMLString::transcode("Summary"));
		}
	XMLString::release(&this->m_OriginalContents);
	m_OriginalContents = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Summary")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Summary is item of type summaryComponentType
		// in element collection RecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType
		
		context->Found = true;
		Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr coll = GetSummary();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType; // FTT, check this
				SetSummary(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:summaryComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrssummaryComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RecordingRequestType_RecordingSourceTypePreferences_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RecordingRequestType_RecordingSourceTypePreferences_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RecordingRequestType_RecordingSourceTypePreferences_LocalType"));
	// Element serialization:
	 // String
	if(m_OriginalContents != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_OriginalContents, X("urn:mpeg:mpeg7:schema:2004"), X("OriginalContents"), false);
	if (m_Summary != Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Summary->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("OriginalContents"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetOriginalContents(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetOriginalContents(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Summary"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Summary")) == 0))
		{
			// Deserialize factory type
			Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr tmp = CreateRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummary(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Summary")) == 0))
  {
	Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionPtr tmp = CreateRecordingRequestType_RecordingSourceTypePreferences_Summary_CollectionType; // FTT, check this
	this->SetSummary(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSummary() != Dc1NodePtr())
		result.Insert(GetSummary());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingSourceTypePreferences_LocalType_ExtMethodImpl.h


