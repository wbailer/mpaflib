
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsPerceptualMoodDistributionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPerceptualMoodDistributionElementType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PerceptualMood

#include <assert.h>
IMp7JrsPerceptualMoodDistributionType::IMp7JrsPerceptualMoodDistributionType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtPropInit.h

}

IMp7JrsPerceptualMoodDistributionType::~IMp7JrsPerceptualMoodDistributionType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtPropCleanup.h

}

Mp7JrsPerceptualMoodDistributionType::Mp7JrsPerceptualMoodDistributionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptualMoodDistributionType::~Mp7JrsPerceptualMoodDistributionType()
{
	Cleanup();
}

void Mp7JrsPerceptualMoodDistributionType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_PerceptualMood = Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtMyPropInit.h

}

void Mp7JrsPerceptualMoodDistributionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PerceptualMood);
}

void Mp7JrsPerceptualMoodDistributionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PerceptualMoodDistributionTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptualMoodDistributionType
	const Dc1Ptr< Mp7JrsPerceptualMoodDistributionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPerceptualMoodDistributionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_PerceptualMood);
		this->SetPerceptualMood(Dc1Factory::CloneObject(tmp->GetPerceptualMood()));
}

Dc1NodePtr Mp7JrsPerceptualMoodDistributionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPerceptualMoodDistributionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsPerceptualMoodDistributionType::GetBase() const
{
	return m_Base;
}

Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr Mp7JrsPerceptualMoodDistributionType::GetPerceptualMood() const
{
		return m_PerceptualMood;
}

void Mp7JrsPerceptualMoodDistributionType::SetPerceptualMood(const Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualMoodDistributionType::SetPerceptualMood().");
	}
	if (m_PerceptualMood != item)
	{
		// Dc1Factory::DeleteObject(m_PerceptualMood);
		m_PerceptualMood = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualMood) m_PerceptualMood->SetParent(m_myself.getPointer());
		if(m_PerceptualMood != Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr())
		{
			m_PerceptualMood->SetContentName(XMLString::transcode("PerceptualMood"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsPerceptualMoodDistributionType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsPerceptualMoodDistributionType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsPerceptualMoodDistributionType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualMoodDistributionType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptualMoodDistributionType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsPerceptualMoodDistributionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("PerceptualMood")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PerceptualMood is item of type PerceptualMoodDistributionElementType
		// in element collection PerceptualMoodDistributionType_PerceptualMood_CollectionType
		
		context->Found = true;
		Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr coll = GetPerceptualMood();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptualMoodDistributionType_PerceptualMood_CollectionType; // FTT, check this
				SetPerceptualMood(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionElementType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualMoodDistributionElementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PerceptualMoodDistributionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PerceptualMoodDistributionType");
	}
	return result;
}

XMLCh * Mp7JrsPerceptualMoodDistributionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPerceptualMoodDistributionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPerceptualMoodDistributionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualMoodDistributionType"));
	// Element serialization:
	if (m_PerceptualMood != Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PerceptualMood->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPerceptualMoodDistributionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PerceptualMood"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PerceptualMood")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr tmp = CreatePerceptualMoodDistributionType_PerceptualMood_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPerceptualMood(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptualMoodDistributionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PerceptualMood")) == 0))
  {
	Mp7JrsPerceptualMoodDistributionType_PerceptualMood_CollectionPtr tmp = CreatePerceptualMoodDistributionType_PerceptualMood_CollectionType; // FTT, check this
	this->SetPerceptualMood(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptualMoodDistributionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPerceptualMood() != Dc1NodePtr())
		result.Insert(GetPerceptualMood());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptualMoodDistributionType_ExtMethodImpl.h


