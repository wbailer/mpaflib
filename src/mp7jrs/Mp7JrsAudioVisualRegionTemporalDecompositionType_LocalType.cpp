
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsAudioVisualRegionType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::~IMp7JrsAudioVisualRegionTemporalDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::~Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr(); // Class
	m_AudioVisualRegionRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
}

void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioVisualRegionTemporalDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioVisualRegionTemporalDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_AudioVisualRegion);
		this->SetAudioVisualRegion(Dc1Factory::CloneObject(tmp->GetAudioVisualRegion()));
		// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
		this->SetAudioVisualRegionRef(Dc1Factory::CloneObject(tmp->GetAudioVisualRegionRef()));
}

Dc1NodePtr Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsAudioVisualRegionPtr Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetAudioVisualRegion() const
{
		return m_AudioVisualRegion;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetAudioVisualRegionRef() const
{
		return m_AudioVisualRegionRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::SetAudioVisualRegion(const Mp7JrsAudioVisualRegionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::SetAudioVisualRegion().");
	}
	if (m_AudioVisualRegion != item)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualRegion);
		m_AudioVisualRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualRegion) m_AudioVisualRegion->SetParent(m_myself.getPointer());
		if (m_AudioVisualRegion && m_AudioVisualRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualRegion->UseTypeAttribute = true;
		}
		if(m_AudioVisualRegion != Mp7JrsAudioVisualRegionPtr())
		{
			m_AudioVisualRegion->SetContentName(XMLString::transcode("AudioVisualRegion"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::SetAudioVisualRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::SetAudioVisualRegionRef().");
	}
	if (m_AudioVisualRegionRef != item)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
		m_AudioVisualRegionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualRegionRef) m_AudioVisualRegionRef->SetParent(m_myself.getPointer());
		if (m_AudioVisualRegionRef && m_AudioVisualRegionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualRegionRef->UseTypeAttribute = true;
		}
		if(m_AudioVisualRegionRef != Mp7JrsReferencePtr())
		{
			m_AudioVisualRegionRef->SetContentName(XMLString::transcode("AudioVisualRegionRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: AudioVisualRegionTemporalDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_AudioVisualRegion != Mp7JrsAudioVisualRegionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioVisualRegionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualRegionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualRegionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualRegionRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualRegionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualRegionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualRegionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("AudioVisualRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualRegionType; // FTT, check this
	}
	this->SetAudioVisualRegion(child);
  }
  if (XMLString::compareString(elementname, X("AudioVisualRegionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetAudioVisualRegionRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAudioVisualRegion() != Dc1NodePtr())
		result.Insert(GetAudioVisualRegion());
	if (GetAudioVisualRegionRef() != Dc1NodePtr())
		result.Insert(GetAudioVisualRegionRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionTemporalDecompositionType_LocalType_ExtMethodImpl.h


