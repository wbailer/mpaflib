
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsChordBaseType_NoteAdded_LocalType.h"
#include "Mp7JrsChordBaseType_NoteRemoved_LocalType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsChordBaseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsChordBaseType::IMp7JrsChordBaseType()
{

// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtPropInit.h

}

IMp7JrsChordBaseType::~IMp7JrsChordBaseType()
{
// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtPropCleanup.h

}

Mp7JrsChordBaseType::Mp7JrsChordBaseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsChordBaseType::~Mp7JrsChordBaseType()
{
	Cleanup();
}

void Mp7JrsChordBaseType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_RelativeChordNumber = 1; // Value
	m_Triad = Mp7JrsChordBaseType_Triad_LocalType::UninitializedEnumeration;
	m_Triad_Default = Mp7JrsChordBaseType_Triad_LocalType::major; // Default enumeration
	m_Triad_Exist = false;
	m_SeventhChord = Mp7JrsChordBaseType_SeventhChord_LocalType::UninitializedEnumeration;
	m_SeventhChord_Default = Mp7JrsChordBaseType_SeventhChord_LocalType::none; // Default enumeration
	m_SeventhChord_Exist = false;
	m_AlternateBass = 1; // Value
	m_AlternateBass_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_NoteAdded = Mp7JrsChordBaseType_NoteAdded_LocalPtr(); // Class with content 
	m_NoteAdded_Exist = false;
	m_NoteRemoved = Mp7JrsChordBaseType_NoteRemoved_LocalPtr(); // Class with content 
	m_NoteRemoved_Exist = false;


// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtMyPropInit.h

}

void Mp7JrsChordBaseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_NoteAdded);
	// Dc1Factory::DeleteObject(m_NoteRemoved);
}

void Mp7JrsChordBaseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ChordBaseTypePtr, since we
	// might need GetBase(), which isn't defined in IChordBaseType
	const Dc1Ptr< Mp7JrsChordBaseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsChordBaseType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->SetRelativeChordNumber(tmp->GetRelativeChordNumber());
	}
	{
	if (tmp->ExistTriad())
	{
		this->SetTriad(tmp->GetTriad());
	}
	else
	{
		InvalidateTriad();
	}
	}
	{
	if (tmp->ExistSeventhChord())
	{
		this->SetSeventhChord(tmp->GetSeventhChord());
	}
	else
	{
		InvalidateSeventhChord();
	}
	}
	{
	if (tmp->ExistAlternateBass())
	{
		this->SetAlternateBass(tmp->GetAlternateBass());
	}
	else
	{
		InvalidateAlternateBass();
	}
	}
	if (tmp->IsValidNoteAdded())
	{
		// Dc1Factory::DeleteObject(m_NoteAdded);
		this->SetNoteAdded(Dc1Factory::CloneObject(tmp->GetNoteAdded()));
	}
	else
	{
		InvalidateNoteAdded();
	}
	if (tmp->IsValidNoteRemoved())
	{
		// Dc1Factory::DeleteObject(m_NoteRemoved);
		this->SetNoteRemoved(Dc1Factory::CloneObject(tmp->GetNoteRemoved()));
	}
	else
	{
		InvalidateNoteRemoved();
	}
}

Dc1NodePtr Mp7JrsChordBaseType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsChordBaseType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsChordBaseType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsChordBaseType::GetRelativeChordNumber() const
{
	return m_RelativeChordNumber;
}

void Mp7JrsChordBaseType::SetRelativeChordNumber(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetRelativeChordNumber().");
	}
	m_RelativeChordNumber = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsChordBaseType_Triad_LocalType::Enumeration Mp7JrsChordBaseType::GetTriad() const
{
	if (this->ExistTriad()) {
		return m_Triad;
	} else {
		return m_Triad_Default;
	}
}

bool Mp7JrsChordBaseType::ExistTriad() const
{
	return m_Triad_Exist;
}
void Mp7JrsChordBaseType::SetTriad(Mp7JrsChordBaseType_Triad_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetTriad().");
	}
	m_Triad = item;
	m_Triad_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::InvalidateTriad()
{
	m_Triad_Exist = false;
}
Mp7JrsChordBaseType_SeventhChord_LocalType::Enumeration Mp7JrsChordBaseType::GetSeventhChord() const
{
	if (this->ExistSeventhChord()) {
		return m_SeventhChord;
	} else {
		return m_SeventhChord_Default;
	}
}

bool Mp7JrsChordBaseType::ExistSeventhChord() const
{
	return m_SeventhChord_Exist;
}
void Mp7JrsChordBaseType::SetSeventhChord(Mp7JrsChordBaseType_SeventhChord_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetSeventhChord().");
	}
	m_SeventhChord = item;
	m_SeventhChord_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::InvalidateSeventhChord()
{
	m_SeventhChord_Exist = false;
}
unsigned Mp7JrsChordBaseType::GetAlternateBass() const
{
	return m_AlternateBass;
}

bool Mp7JrsChordBaseType::ExistAlternateBass() const
{
	return m_AlternateBass_Exist;
}
void Mp7JrsChordBaseType::SetAlternateBass(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetAlternateBass().");
	}
	m_AlternateBass = item;
	m_AlternateBass_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::InvalidateAlternateBass()
{
	m_AlternateBass_Exist = false;
}
Mp7JrsChordBaseType_NoteAdded_LocalPtr Mp7JrsChordBaseType::GetNoteAdded() const
{
		return m_NoteAdded;
}

// Element is optional
bool Mp7JrsChordBaseType::IsValidNoteAdded() const
{
	return m_NoteAdded_Exist;
}

Mp7JrsChordBaseType_NoteRemoved_LocalPtr Mp7JrsChordBaseType::GetNoteRemoved() const
{
		return m_NoteRemoved;
}

// Element is optional
bool Mp7JrsChordBaseType::IsValidNoteRemoved() const
{
	return m_NoteRemoved_Exist;
}

void Mp7JrsChordBaseType::SetNoteAdded(const Mp7JrsChordBaseType_NoteAdded_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetNoteAdded().");
	}
	if (m_NoteAdded != item || m_NoteAdded_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_NoteAdded);
		m_NoteAdded = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NoteAdded) m_NoteAdded->SetParent(m_myself.getPointer());
		if (m_NoteAdded && m_NoteAdded->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteAdded_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_NoteAdded->UseTypeAttribute = true;
		}
		m_NoteAdded_Exist = true;
		if(m_NoteAdded != Mp7JrsChordBaseType_NoteAdded_LocalPtr())
		{
			m_NoteAdded->SetContentName(XMLString::transcode("NoteAdded"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::InvalidateNoteAdded()
{
	m_NoteAdded_Exist = false;
}
void Mp7JrsChordBaseType::SetNoteRemoved(const Mp7JrsChordBaseType_NoteRemoved_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::SetNoteRemoved().");
	}
	if (m_NoteRemoved != item || m_NoteRemoved_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_NoteRemoved);
		m_NoteRemoved = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NoteRemoved) m_NoteRemoved->SetParent(m_myself.getPointer());
		if (m_NoteRemoved && m_NoteRemoved->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteRemoved_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_NoteRemoved->UseTypeAttribute = true;
		}
		m_NoteRemoved_Exist = true;
		if(m_NoteRemoved != Mp7JrsChordBaseType_NoteRemoved_LocalPtr())
		{
			m_NoteRemoved->SetContentName(XMLString::transcode("NoteRemoved"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::InvalidateNoteRemoved()
{
	m_NoteRemoved_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsChordBaseType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsChordBaseType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsChordBaseType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsChordBaseType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsChordBaseType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsChordBaseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("RelativeChordNumber")) == 0)
	{
		// RelativeChordNumber is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("Triad")) == 0)
	{
		// Triad is simple attribute ChordBaseType_Triad_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsChordBaseType_Triad_LocalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("SeventhChord")) == 0)
	{
		// SeventhChord is simple attribute ChordBaseType_SeventhChord_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsChordBaseType_SeventhChord_LocalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("AlternateBass")) == 0)
	{
		// AlternateBass is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NoteAdded")) == 0)
	{
		// NoteAdded is simple element ChordBaseType_NoteAdded_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetNoteAdded()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteAdded_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsChordBaseType_NoteAdded_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetNoteAdded(p, client);
					if((p = GetNoteAdded()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NoteRemoved")) == 0)
	{
		// NoteRemoved is simple element ChordBaseType_NoteRemoved_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetNoteRemoved()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType_NoteRemoved_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsChordBaseType_NoteRemoved_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetNoteRemoved(p, client);
					if((p = GetNoteRemoved()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ChordBaseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ChordBaseType");
	}
	return result;
}

XMLCh * Mp7JrsChordBaseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsChordBaseType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsChordBaseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ChordBaseType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_RelativeChordNumber);
		element->setAttributeNS(X(""), X("RelativeChordNumber"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_Triad_Exist)
	{
	// Enumeration
	if(m_Triad != Mp7JrsChordBaseType_Triad_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsChordBaseType_Triad_LocalType::ToText(m_Triad);
		element->setAttributeNS(X(""), X("Triad"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_SeventhChord_Exist)
	{
	// Enumeration
	if(m_SeventhChord != Mp7JrsChordBaseType_SeventhChord_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsChordBaseType_SeventhChord_LocalType::ToText(m_SeventhChord);
		element->setAttributeNS(X(""), X("SeventhChord"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_AlternateBass_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_AlternateBass);
		element->setAttributeNS(X(""), X("AlternateBass"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class with content		
	
	if (m_NoteAdded != Mp7JrsChordBaseType_NoteAdded_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_NoteAdded->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("NoteAdded"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_NoteAdded->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_NoteRemoved != Mp7JrsChordBaseType_NoteRemoved_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_NoteRemoved->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("NoteRemoved"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_NoteRemoved->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsChordBaseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("RelativeChordNumber")))
	{
		// deserialize value type
		this->SetRelativeChordNumber(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("RelativeChordNumber"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("Triad")))
	{
		this->SetTriad(Mp7JrsChordBaseType_Triad_LocalType::Parse(parent->getAttribute(X("Triad"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("SeventhChord")))
	{
		this->SetSeventhChord(Mp7JrsChordBaseType_SeventhChord_LocalType::Parse(parent->getAttribute(X("SeventhChord"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("AlternateBass")))
	{
		// deserialize value type
		this->SetAlternateBass(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("AlternateBass"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("NoteAdded"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("NoteAdded")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateChordBaseType_NoteAdded_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetNoteAdded(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("NoteRemoved"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("NoteRemoved")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateChordBaseType_NoteRemoved_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetNoteRemoved(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsChordBaseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("NoteAdded")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateChordBaseType_NoteAdded_LocalType; // FTT, check this
	}
	this->SetNoteAdded(child);
  }
  if (XMLString::compareString(elementname, X("NoteRemoved")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateChordBaseType_NoteRemoved_LocalType; // FTT, check this
	}
	this->SetNoteRemoved(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsChordBaseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetNoteAdded() != Dc1NodePtr())
		result.Insert(GetNoteAdded());
	if (GetNoteRemoved() != Dc1NodePtr())
		result.Insert(GetNoteRemoved());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsChordBaseType_ExtMethodImpl.h


