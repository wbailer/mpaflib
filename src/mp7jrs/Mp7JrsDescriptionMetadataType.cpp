
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType_Creator_CollectionType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsDescriptionMetadataType_Instrument_CollectionType.h"
#include "Mp7JrsRightsType.h"
#include "Mp7JrsPackageType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsUniqueIDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PublicIdentifier
#include "Mp7JrsCreatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Creator
#include "Mp7JrsCreationToolType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Instrument

#include <assert.h>
IMp7JrsDescriptionMetadataType::IMp7JrsDescriptionMetadataType()
{

// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtPropInit.h

}

IMp7JrsDescriptionMetadataType::~IMp7JrsDescriptionMetadataType()
{
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtPropCleanup.h

}

Mp7JrsDescriptionMetadataType::Mp7JrsDescriptionMetadataType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDescriptionMetadataType::~Mp7JrsDescriptionMetadataType()
{
	Cleanup();
}

void Mp7JrsDescriptionMetadataType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Confidence = Mp7JrszeroToOnePtr(); // Class with content 
	m_Confidence_Exist = false;
	m_Version = NULL; // Optional String
	m_Version_Exist = false;
	m_LastUpdate = Mp7JrstimePointPtr(); // Pattern
	m_LastUpdate_Exist = false;
	m_Comment = Mp7JrsTextAnnotationPtr(); // Class
	m_Comment_Exist = false;
	m_PublicIdentifier = Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr(); // Collection
	m_PrivateIdentifier = Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr(); // Collection
	m_Creator = Mp7JrsDescriptionMetadataType_Creator_CollectionPtr(); // Collection
	m_CreationLocation = Mp7JrsPlacePtr(); // Class
	m_CreationLocation_Exist = false;
	m_CreationTime = Mp7JrstimePointPtr(); // Pattern
	m_CreationTime_Exist = false;
	m_Instrument = Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr(); // Collection
	m_Rights = Mp7JrsRightsPtr(); // Class
	m_Rights_Exist = false;
	m_Package = Mp7JrsPackagePtr(); // Class
	m_Package_Exist = false;


// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMyPropInit.h

}

void Mp7JrsDescriptionMetadataType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Confidence);
	XMLString::release(&this->m_Version);
	this->m_Version = NULL;
	// Dc1Factory::DeleteObject(m_LastUpdate);
	// Dc1Factory::DeleteObject(m_Comment);
	// Dc1Factory::DeleteObject(m_PublicIdentifier);
	// Dc1Factory::DeleteObject(m_PrivateIdentifier);
	// Dc1Factory::DeleteObject(m_Creator);
	// Dc1Factory::DeleteObject(m_CreationLocation);
	// Dc1Factory::DeleteObject(m_CreationTime);
	// Dc1Factory::DeleteObject(m_Instrument);
	// Dc1Factory::DeleteObject(m_Rights);
	// Dc1Factory::DeleteObject(m_Package);
}

void Mp7JrsDescriptionMetadataType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DescriptionMetadataTypePtr, since we
	// might need GetBase(), which isn't defined in IDescriptionMetadataType
	const Dc1Ptr< Mp7JrsDescriptionMetadataType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDescriptionMetadataType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidConfidence())
	{
		// Dc1Factory::DeleteObject(m_Confidence);
		this->SetConfidence(Dc1Factory::CloneObject(tmp->GetConfidence()));
	}
	else
	{
		InvalidateConfidence();
	}
	if (tmp->IsValidVersion())
	{
		this->SetVersion(XMLString::replicate(tmp->GetVersion()));
	}
	else
	{
		InvalidateVersion();
	}
	if (tmp->IsValidLastUpdate())
	{
		// Dc1Factory::DeleteObject(m_LastUpdate);
		this->SetLastUpdate(Dc1Factory::CloneObject(tmp->GetLastUpdate()));
	}
	else
	{
		InvalidateLastUpdate();
	}
	if (tmp->IsValidComment())
	{
		// Dc1Factory::DeleteObject(m_Comment);
		this->SetComment(Dc1Factory::CloneObject(tmp->GetComment()));
	}
	else
	{
		InvalidateComment();
	}
		// Dc1Factory::DeleteObject(m_PublicIdentifier);
		this->SetPublicIdentifier(Dc1Factory::CloneObject(tmp->GetPublicIdentifier()));
		// Dc1Factory::DeleteObject(m_PrivateIdentifier);
		this->SetPrivateIdentifier(Dc1Factory::CloneObject(tmp->GetPrivateIdentifier()));
		// Dc1Factory::DeleteObject(m_Creator);
		this->SetCreator(Dc1Factory::CloneObject(tmp->GetCreator()));
	if (tmp->IsValidCreationLocation())
	{
		// Dc1Factory::DeleteObject(m_CreationLocation);
		this->SetCreationLocation(Dc1Factory::CloneObject(tmp->GetCreationLocation()));
	}
	else
	{
		InvalidateCreationLocation();
	}
	if (tmp->IsValidCreationTime())
	{
		// Dc1Factory::DeleteObject(m_CreationTime);
		this->SetCreationTime(Dc1Factory::CloneObject(tmp->GetCreationTime()));
	}
	else
	{
		InvalidateCreationTime();
	}
		// Dc1Factory::DeleteObject(m_Instrument);
		this->SetInstrument(Dc1Factory::CloneObject(tmp->GetInstrument()));
	if (tmp->IsValidRights())
	{
		// Dc1Factory::DeleteObject(m_Rights);
		this->SetRights(Dc1Factory::CloneObject(tmp->GetRights()));
	}
	else
	{
		InvalidateRights();
	}
	if (tmp->IsValidPackage())
	{
		// Dc1Factory::DeleteObject(m_Package);
		this->SetPackage(Dc1Factory::CloneObject(tmp->GetPackage()));
	}
	else
	{
		InvalidatePackage();
	}
}

Dc1NodePtr Mp7JrsDescriptionMetadataType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDescriptionMetadataType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsDescriptionMetadataType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsDescriptionMetadataType::GetConfidence() const
{
		return m_Confidence;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidConfidence() const
{
	return m_Confidence_Exist;
}

XMLCh * Mp7JrsDescriptionMetadataType::GetVersion() const
{
		return m_Version;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidVersion() const
{
	return m_Version_Exist;
}

Mp7JrstimePointPtr Mp7JrsDescriptionMetadataType::GetLastUpdate() const
{
		return m_LastUpdate;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidLastUpdate() const
{
	return m_LastUpdate_Exist;
}

Mp7JrsTextAnnotationPtr Mp7JrsDescriptionMetadataType::GetComment() const
{
		return m_Comment;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidComment() const
{
	return m_Comment_Exist;
}

Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr Mp7JrsDescriptionMetadataType::GetPublicIdentifier() const
{
		return m_PublicIdentifier;
}

Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr Mp7JrsDescriptionMetadataType::GetPrivateIdentifier() const
{
		return m_PrivateIdentifier;
}

Mp7JrsDescriptionMetadataType_Creator_CollectionPtr Mp7JrsDescriptionMetadataType::GetCreator() const
{
		return m_Creator;
}

Mp7JrsPlacePtr Mp7JrsDescriptionMetadataType::GetCreationLocation() const
{
		return m_CreationLocation;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidCreationLocation() const
{
	return m_CreationLocation_Exist;
}

Mp7JrstimePointPtr Mp7JrsDescriptionMetadataType::GetCreationTime() const
{
		return m_CreationTime;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidCreationTime() const
{
	return m_CreationTime_Exist;
}

Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr Mp7JrsDescriptionMetadataType::GetInstrument() const
{
		return m_Instrument;
}

Mp7JrsRightsPtr Mp7JrsDescriptionMetadataType::GetRights() const
{
		return m_Rights;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidRights() const
{
	return m_Rights_Exist;
}

Mp7JrsPackagePtr Mp7JrsDescriptionMetadataType::GetPackage() const
{
		return m_Package;
}

// Element is optional
bool Mp7JrsDescriptionMetadataType::IsValidPackage() const
{
	return m_Package_Exist;
}

void Mp7JrsDescriptionMetadataType::SetConfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetConfidence().");
	}
	if (m_Confidence != item || m_Confidence_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Confidence);
		m_Confidence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Confidence) m_Confidence->SetParent(m_myself.getPointer());
		if (m_Confidence && m_Confidence->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Confidence->UseTypeAttribute = true;
		}
		m_Confidence_Exist = true;
		if(m_Confidence != Mp7JrszeroToOnePtr())
		{
			m_Confidence->SetContentName(XMLString::transcode("Confidence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateConfidence()
{
	m_Confidence_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetVersion(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetVersion().");
	}
	if (m_Version != item || m_Version_Exist == false)
	{
		XMLString::release(&m_Version);
		m_Version = item;
		m_Version_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateVersion()
{
	m_Version_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetLastUpdate(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetLastUpdate().");
	}
	if (m_LastUpdate != item || m_LastUpdate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_LastUpdate);
		m_LastUpdate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LastUpdate) m_LastUpdate->SetParent(m_myself.getPointer());
		if (m_LastUpdate && m_LastUpdate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LastUpdate->UseTypeAttribute = true;
		}
		m_LastUpdate_Exist = true;
		if(m_LastUpdate != Mp7JrstimePointPtr())
		{
			m_LastUpdate->SetContentName(XMLString::transcode("LastUpdate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateLastUpdate()
{
	m_LastUpdate_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetComment(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetComment().");
	}
	if (m_Comment != item || m_Comment_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Comment);
		m_Comment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Comment) m_Comment->SetParent(m_myself.getPointer());
		if (m_Comment && m_Comment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Comment->UseTypeAttribute = true;
		}
		m_Comment_Exist = true;
		if(m_Comment != Mp7JrsTextAnnotationPtr())
		{
			m_Comment->SetContentName(XMLString::transcode("Comment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateComment()
{
	m_Comment_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetPublicIdentifier(const Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetPublicIdentifier().");
	}
	if (m_PublicIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_PublicIdentifier);
		m_PublicIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PublicIdentifier) m_PublicIdentifier->SetParent(m_myself.getPointer());
		if(m_PublicIdentifier != Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr())
		{
			m_PublicIdentifier->SetContentName(XMLString::transcode("PublicIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::SetPrivateIdentifier(const Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetPrivateIdentifier().");
	}
	if (m_PrivateIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_PrivateIdentifier);
		m_PrivateIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PrivateIdentifier) m_PrivateIdentifier->SetParent(m_myself.getPointer());
		if(m_PrivateIdentifier != Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr())
		{
			m_PrivateIdentifier->SetContentName(XMLString::transcode("PrivateIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::SetCreator(const Mp7JrsDescriptionMetadataType_Creator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetCreator().");
	}
	if (m_Creator != item)
	{
		// Dc1Factory::DeleteObject(m_Creator);
		m_Creator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Creator) m_Creator->SetParent(m_myself.getPointer());
		if(m_Creator != Mp7JrsDescriptionMetadataType_Creator_CollectionPtr())
		{
			m_Creator->SetContentName(XMLString::transcode("Creator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::SetCreationLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetCreationLocation().");
	}
	if (m_CreationLocation != item || m_CreationLocation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CreationLocation);
		m_CreationLocation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationLocation) m_CreationLocation->SetParent(m_myself.getPointer());
		if (m_CreationLocation && m_CreationLocation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CreationLocation->UseTypeAttribute = true;
		}
		m_CreationLocation_Exist = true;
		if(m_CreationLocation != Mp7JrsPlacePtr())
		{
			m_CreationLocation->SetContentName(XMLString::transcode("CreationLocation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateCreationLocation()
{
	m_CreationLocation_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetCreationTime(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetCreationTime().");
	}
	if (m_CreationTime != item || m_CreationTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CreationTime);
		m_CreationTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationTime) m_CreationTime->SetParent(m_myself.getPointer());
		if (m_CreationTime && m_CreationTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CreationTime->UseTypeAttribute = true;
		}
		m_CreationTime_Exist = true;
		if(m_CreationTime != Mp7JrstimePointPtr())
		{
			m_CreationTime->SetContentName(XMLString::transcode("CreationTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateCreationTime()
{
	m_CreationTime_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetInstrument(const Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetInstrument().");
	}
	if (m_Instrument != item)
	{
		// Dc1Factory::DeleteObject(m_Instrument);
		m_Instrument = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Instrument) m_Instrument->SetParent(m_myself.getPointer());
		if(m_Instrument != Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr())
		{
			m_Instrument->SetContentName(XMLString::transcode("Instrument"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::SetRights(const Mp7JrsRightsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetRights().");
	}
	if (m_Rights != item || m_Rights_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Rights);
		m_Rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Rights) m_Rights->SetParent(m_myself.getPointer());
		if (m_Rights && m_Rights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Rights->UseTypeAttribute = true;
		}
		m_Rights_Exist = true;
		if(m_Rights != Mp7JrsRightsPtr())
		{
			m_Rights->SetContentName(XMLString::transcode("Rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidateRights()
{
	m_Rights_Exist = false;
}
void Mp7JrsDescriptionMetadataType::SetPackage(const Mp7JrsPackagePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::SetPackage().");
	}
	if (m_Package != item || m_Package_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Package);
		m_Package = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Package) m_Package->SetParent(m_myself.getPointer());
		if (m_Package && m_Package->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Package->UseTypeAttribute = true;
		}
		m_Package_Exist = true;
		if(m_Package != Mp7JrsPackagePtr())
		{
			m_Package->SetContentName(XMLString::transcode("Package"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::InvalidatePackage()
{
	m_Package_Exist = false;
}
XMLCh * Mp7JrsDescriptionMetadataType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsDescriptionMetadataType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsDescriptionMetadataType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDescriptionMetadataType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDescriptionMetadataType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsDescriptionMetadataType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Confidence")) == 0)
	{
		// Confidence is simple element zeroToOneType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetConfidence()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType")))) != empty)
			{
				// Is type allowed
				Mp7JrszeroToOnePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetConfidence(p, client);
					if((p = GetConfidence()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LastUpdate")) == 0)
	{
		// LastUpdate is simple element timePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLastUpdate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrstimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLastUpdate(p, client);
					if((p = GetLastUpdate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Comment")) == 0)
	{
		// Comment is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetComment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetComment(p, client);
					if((p = GetComment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PublicIdentifier")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PublicIdentifier is item of type UniqueIDType
		// in element collection DescriptionMetadataType_PublicIdentifier_CollectionType
		
		context->Found = true;
		Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr coll = GetPublicIdentifier();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDescriptionMetadataType_PublicIdentifier_CollectionType; // FTT, check this
				SetPublicIdentifier(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PrivateIdentifier")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Creator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Creator is item of type CreatorType
		// in element collection DescriptionMetadataType_Creator_CollectionType
		
		context->Found = true;
		Mp7JrsDescriptionMetadataType_Creator_CollectionPtr coll = GetCreator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDescriptionMetadataType_Creator_CollectionType; // FTT, check this
				SetCreator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationLocation")) == 0)
	{
		// CreationLocation is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCreationLocation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCreationLocation(p, client);
					if((p = GetCreationLocation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationTime")) == 0)
	{
		// CreationTime is simple element timePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCreationTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrstimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCreationTime(p, client);
					if((p = GetCreationTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Instrument")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Instrument is item of type CreationToolType
		// in element collection DescriptionMetadataType_Instrument_CollectionType
		
		context->Found = true;
		Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr coll = GetInstrument();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDescriptionMetadataType_Instrument_CollectionType; // FTT, check this
				SetInstrument(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Rights")) == 0)
	{
		// Rights is simple element RightsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRights()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRightsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRights(p, client);
					if((p = GetRights()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Package")) == 0)
	{
		// Package is simple element PackageType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPackage()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PackageType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPackagePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPackage(p, client);
					if((p = GetPackage()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DescriptionMetadataType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DescriptionMetadataType");
	}
	return result;
}

XMLCh * Mp7JrsDescriptionMetadataType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDescriptionMetadataType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDescriptionMetadataType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionMetadataType"));
	// Element serialization:
	// Class with content		
	
	if (m_Confidence != Mp7JrszeroToOnePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Confidence->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Confidence"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Confidence->Serialize(doc, element, &elem);
	}
	 // String
	if(m_Version != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Version, X("urn:mpeg:mpeg7:schema:2004"), X("Version"), true);
	if(m_LastUpdate != Mp7JrstimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("LastUpdate");
//		m_LastUpdate->SetContentName(contentname);
		m_LastUpdate->UseTypeAttribute = this->UseTypeAttribute;
		m_LastUpdate->Serialize(doc, element);
	}
	// Class
	
	if (m_Comment != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Comment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Comment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Comment->Serialize(doc, element, &elem);
	}
	if (m_PublicIdentifier != Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PublicIdentifier->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PrivateIdentifier != Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PrivateIdentifier->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Creator != Mp7JrsDescriptionMetadataType_Creator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Creator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_CreationLocation != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CreationLocation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CreationLocation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CreationLocation->Serialize(doc, element, &elem);
	}
	if(m_CreationTime != Mp7JrstimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("CreationTime");
//		m_CreationTime->SetContentName(contentname);
		m_CreationTime->UseTypeAttribute = this->UseTypeAttribute;
		m_CreationTime->Serialize(doc, element);
	}
	if (m_Instrument != Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Instrument->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Rights != Mp7JrsRightsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Rights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Rights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Rights->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Package != Mp7JrsPackagePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Package->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Package"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Package->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsDescriptionMetadataType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Confidence"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Confidence")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatezeroToOneType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetConfidence(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Version"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetVersion(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetVersion(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("LastUpdate"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LastUpdate")) == 0))
		{
			Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLastUpdate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Comment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Comment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetComment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PublicIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PublicIdentifier")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr tmp = CreateDescriptionMetadataType_PublicIdentifier_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPublicIdentifier(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PrivateIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PrivateIdentifier")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr tmp = CreateDescriptionMetadataType_PrivateIdentifier_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPrivateIdentifier(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Creator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Creator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDescriptionMetadataType_Creator_CollectionPtr tmp = CreateDescriptionMetadataType_Creator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CreationLocation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CreationLocation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCreationLocation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("CreationTime"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CreationTime")) == 0))
		{
			Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreationTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Instrument"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Instrument")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr tmp = CreateDescriptionMetadataType_Instrument_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInstrument(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Rights"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Rights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRightsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Package"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Package")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePackageType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPackage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDescriptionMetadataType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Confidence")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatezeroToOneType; // FTT, check this
	}
	this->SetConfidence(child);
  }
  if (XMLString::compareString(elementname, X("LastUpdate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimePointType; // FTT, check this
	}
	this->SetLastUpdate(child);
  }
  if (XMLString::compareString(elementname, X("Comment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetComment(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PublicIdentifier")) == 0))
  {
	Mp7JrsDescriptionMetadataType_PublicIdentifier_CollectionPtr tmp = CreateDescriptionMetadataType_PublicIdentifier_CollectionType; // FTT, check this
	this->SetPublicIdentifier(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PrivateIdentifier")) == 0))
  {
	Mp7JrsDescriptionMetadataType_PrivateIdentifier_CollectionPtr tmp = CreateDescriptionMetadataType_PrivateIdentifier_CollectionType; // FTT, check this
	this->SetPrivateIdentifier(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Creator")) == 0))
  {
	Mp7JrsDescriptionMetadataType_Creator_CollectionPtr tmp = CreateDescriptionMetadataType_Creator_CollectionType; // FTT, check this
	this->SetCreator(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("CreationLocation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetCreationLocation(child);
  }
  if (XMLString::compareString(elementname, X("CreationTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimePointType; // FTT, check this
	}
	this->SetCreationTime(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Instrument")) == 0))
  {
	Mp7JrsDescriptionMetadataType_Instrument_CollectionPtr tmp = CreateDescriptionMetadataType_Instrument_CollectionType; // FTT, check this
	this->SetInstrument(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Rights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRightsType; // FTT, check this
	}
	this->SetRights(child);
  }
  if (XMLString::compareString(elementname, X("Package")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePackageType; // FTT, check this
	}
	this->SetPackage(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDescriptionMetadataType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetConfidence() != Dc1NodePtr())
		result.Insert(GetConfidence());
	if (GetLastUpdate() != Dc1NodePtr())
		result.Insert(GetLastUpdate());
	if (GetComment() != Dc1NodePtr())
		result.Insert(GetComment());
	if (GetPublicIdentifier() != Dc1NodePtr())
		result.Insert(GetPublicIdentifier());
	if (GetPrivateIdentifier() != Dc1NodePtr())
		result.Insert(GetPrivateIdentifier());
	if (GetCreator() != Dc1NodePtr())
		result.Insert(GetCreator());
	if (GetCreationLocation() != Dc1NodePtr())
		result.Insert(GetCreationLocation());
	if (GetCreationTime() != Dc1NodePtr())
		result.Insert(GetCreationTime());
	if (GetInstrument() != Dc1NodePtr())
		result.Insert(GetInstrument());
	if (GetRights() != Dc1NodePtr())
		result.Insert(GetRights());
	if (GetPackage() != Dc1NodePtr())
		result.Insert(GetPackage());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDescriptionMetadataType_ExtMethodImpl.h


