
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsAudioSummaryComponentType_Title_CollectionType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsAudioSummaryComponentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Title

#include <assert.h>
IMp7JrsAudioSummaryComponentType::IMp7JrsAudioSummaryComponentType()
{

// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtPropInit.h

}

IMp7JrsAudioSummaryComponentType::~IMp7JrsAudioSummaryComponentType()
{
// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtPropCleanup.h

}

Mp7JrsAudioSummaryComponentType::Mp7JrsAudioSummaryComponentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioSummaryComponentType::~Mp7JrsAudioSummaryComponentType()
{
	Cleanup();
}

void Mp7JrsAudioSummaryComponentType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Title = Mp7JrsAudioSummaryComponentType_Title_CollectionPtr(); // Collection
	m_AudioSourceID = Mp7JrsUniqueIDPtr(); // Class
	m_AudioSourceID_Exist = false;
	m_AudioSourceLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_AudioSourceLocator_Exist = false;
	m_ComponentSourceTime = Mp7JrsMediaTimePtr(); // Class
	m_ComponentSourceTime_Exist = false;
	m_SoundLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_SoundLocator_Exist = false;
	m_SyncTime = Mp7JrsMediaTimePtr(); // Class
	m_SyncTime_Exist = false;


// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtMyPropInit.h

}

void Mp7JrsAudioSummaryComponentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Title);
	// Dc1Factory::DeleteObject(m_AudioSourceID);
	// Dc1Factory::DeleteObject(m_AudioSourceLocator);
	// Dc1Factory::DeleteObject(m_ComponentSourceTime);
	// Dc1Factory::DeleteObject(m_SoundLocator);
	// Dc1Factory::DeleteObject(m_SyncTime);
}

void Mp7JrsAudioSummaryComponentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioSummaryComponentTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioSummaryComponentType
	const Dc1Ptr< Mp7JrsAudioSummaryComponentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioSummaryComponentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Title);
		this->SetTitle(Dc1Factory::CloneObject(tmp->GetTitle()));
	if (tmp->IsValidAudioSourceID())
	{
		// Dc1Factory::DeleteObject(m_AudioSourceID);
		this->SetAudioSourceID(Dc1Factory::CloneObject(tmp->GetAudioSourceID()));
	}
	else
	{
		InvalidateAudioSourceID();
	}
	if (tmp->IsValidAudioSourceLocator())
	{
		// Dc1Factory::DeleteObject(m_AudioSourceLocator);
		this->SetAudioSourceLocator(Dc1Factory::CloneObject(tmp->GetAudioSourceLocator()));
	}
	else
	{
		InvalidateAudioSourceLocator();
	}
	if (tmp->IsValidComponentSourceTime())
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		this->SetComponentSourceTime(Dc1Factory::CloneObject(tmp->GetComponentSourceTime()));
	}
	else
	{
		InvalidateComponentSourceTime();
	}
	if (tmp->IsValidSoundLocator())
	{
		// Dc1Factory::DeleteObject(m_SoundLocator);
		this->SetSoundLocator(Dc1Factory::CloneObject(tmp->GetSoundLocator()));
	}
	else
	{
		InvalidateSoundLocator();
	}
	if (tmp->IsValidSyncTime())
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		this->SetSyncTime(Dc1Factory::CloneObject(tmp->GetSyncTime()));
	}
	else
	{
		InvalidateSyncTime();
	}
}

Dc1NodePtr Mp7JrsAudioSummaryComponentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioSummaryComponentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsAudioSummaryComponentType::GetBase() const
{
	return m_Base;
}

Mp7JrsAudioSummaryComponentType_Title_CollectionPtr Mp7JrsAudioSummaryComponentType::GetTitle() const
{
		return m_Title;
}

Mp7JrsUniqueIDPtr Mp7JrsAudioSummaryComponentType::GetAudioSourceID() const
{
		return m_AudioSourceID;
}

// Element is optional
bool Mp7JrsAudioSummaryComponentType::IsValidAudioSourceID() const
{
	return m_AudioSourceID_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsAudioSummaryComponentType::GetAudioSourceLocator() const
{
		return m_AudioSourceLocator;
}

// Element is optional
bool Mp7JrsAudioSummaryComponentType::IsValidAudioSourceLocator() const
{
	return m_AudioSourceLocator_Exist;
}

Mp7JrsMediaTimePtr Mp7JrsAudioSummaryComponentType::GetComponentSourceTime() const
{
		return m_ComponentSourceTime;
}

// Element is optional
bool Mp7JrsAudioSummaryComponentType::IsValidComponentSourceTime() const
{
	return m_ComponentSourceTime_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsAudioSummaryComponentType::GetSoundLocator() const
{
		return m_SoundLocator;
}

// Element is optional
bool Mp7JrsAudioSummaryComponentType::IsValidSoundLocator() const
{
	return m_SoundLocator_Exist;
}

Mp7JrsMediaTimePtr Mp7JrsAudioSummaryComponentType::GetSyncTime() const
{
		return m_SyncTime;
}

// Element is optional
bool Mp7JrsAudioSummaryComponentType::IsValidSyncTime() const
{
	return m_SyncTime_Exist;
}

void Mp7JrsAudioSummaryComponentType::SetTitle(const Mp7JrsAudioSummaryComponentType_Title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetTitle().");
	}
	if (m_Title != item)
	{
		// Dc1Factory::DeleteObject(m_Title);
		m_Title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Title) m_Title->SetParent(m_myself.getPointer());
		if(m_Title != Mp7JrsAudioSummaryComponentType_Title_CollectionPtr())
		{
			m_Title->SetContentName(XMLString::transcode("Title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::SetAudioSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetAudioSourceID().");
	}
	if (m_AudioSourceID != item || m_AudioSourceID_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioSourceID);
		m_AudioSourceID = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSourceID) m_AudioSourceID->SetParent(m_myself.getPointer());
		if (m_AudioSourceID && m_AudioSourceID->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSourceID->UseTypeAttribute = true;
		}
		m_AudioSourceID_Exist = true;
		if(m_AudioSourceID != Mp7JrsUniqueIDPtr())
		{
			m_AudioSourceID->SetContentName(XMLString::transcode("AudioSourceID"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::InvalidateAudioSourceID()
{
	m_AudioSourceID_Exist = false;
}
void Mp7JrsAudioSummaryComponentType::SetAudioSourceLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetAudioSourceLocator().");
	}
	if (m_AudioSourceLocator != item || m_AudioSourceLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioSourceLocator);
		m_AudioSourceLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSourceLocator) m_AudioSourceLocator->SetParent(m_myself.getPointer());
		if (m_AudioSourceLocator && m_AudioSourceLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSourceLocator->UseTypeAttribute = true;
		}
		m_AudioSourceLocator_Exist = true;
		if(m_AudioSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_AudioSourceLocator->SetContentName(XMLString::transcode("AudioSourceLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::InvalidateAudioSourceLocator()
{
	m_AudioSourceLocator_Exist = false;
}
void Mp7JrsAudioSummaryComponentType::SetComponentSourceTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetComponentSourceTime().");
	}
	if (m_ComponentSourceTime != item || m_ComponentSourceTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		m_ComponentSourceTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ComponentSourceTime) m_ComponentSourceTime->SetParent(m_myself.getPointer());
		if (m_ComponentSourceTime && m_ComponentSourceTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ComponentSourceTime->UseTypeAttribute = true;
		}
		m_ComponentSourceTime_Exist = true;
		if(m_ComponentSourceTime != Mp7JrsMediaTimePtr())
		{
			m_ComponentSourceTime->SetContentName(XMLString::transcode("ComponentSourceTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::InvalidateComponentSourceTime()
{
	m_ComponentSourceTime_Exist = false;
}
void Mp7JrsAudioSummaryComponentType::SetSoundLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetSoundLocator().");
	}
	if (m_SoundLocator != item || m_SoundLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SoundLocator);
		m_SoundLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SoundLocator) m_SoundLocator->SetParent(m_myself.getPointer());
		if (m_SoundLocator && m_SoundLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SoundLocator->UseTypeAttribute = true;
		}
		m_SoundLocator_Exist = true;
		if(m_SoundLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_SoundLocator->SetContentName(XMLString::transcode("SoundLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::InvalidateSoundLocator()
{
	m_SoundLocator_Exist = false;
}
void Mp7JrsAudioSummaryComponentType::SetSyncTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSummaryComponentType::SetSyncTime().");
	}
	if (m_SyncTime != item || m_SyncTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		m_SyncTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SyncTime) m_SyncTime->SetParent(m_myself.getPointer());
		if (m_SyncTime && m_SyncTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SyncTime->UseTypeAttribute = true;
		}
		m_SyncTime_Exist = true;
		if(m_SyncTime != Mp7JrsMediaTimePtr())
		{
			m_SyncTime->SetContentName(XMLString::transcode("SyncTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSummaryComponentType::InvalidateSyncTime()
{
	m_SyncTime_Exist = false;
}

Dc1NodeEnum Mp7JrsAudioSummaryComponentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Title")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Title is item of type TextualType
		// in element collection AudioSummaryComponentType_Title_CollectionType
		
		context->Found = true;
		Mp7JrsAudioSummaryComponentType_Title_CollectionPtr coll = GetTitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioSummaryComponentType_Title_CollectionType; // FTT, check this
				SetTitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSourceID")) == 0)
	{
		// AudioSourceID is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioSourceID()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioSourceID(p, client);
					if((p = GetAudioSourceID()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSourceLocator")) == 0)
	{
		// AudioSourceLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioSourceLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioSourceLocator(p, client);
					if((p = GetAudioSourceLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ComponentSourceTime")) == 0)
	{
		// ComponentSourceTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetComponentSourceTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetComponentSourceTime(p, client);
					if((p = GetComponentSourceTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SoundLocator")) == 0)
	{
		// SoundLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSoundLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSoundLocator(p, client);
					if((p = GetSoundLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SyncTime")) == 0)
	{
		// SyncTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSyncTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSyncTime(p, client);
					if((p = GetSyncTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioSummaryComponentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioSummaryComponentType");
	}
	return result;
}

XMLCh * Mp7JrsAudioSummaryComponentType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAudioSummaryComponentType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAudioSummaryComponentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioSummaryComponentType"));
	// Element serialization:
	if (m_Title != Mp7JrsAudioSummaryComponentType_Title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Title->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_AudioSourceID != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSourceID->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSourceID"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSourceID->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSourceLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSourceLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSourceLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ComponentSourceTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ComponentSourceTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ComponentSourceTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ComponentSourceTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SoundLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SoundLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SoundLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SoundLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SyncTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SyncTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SyncTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SyncTime->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioSummaryComponentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Title")) == 0))
		{
			// Deserialize factory type
			Mp7JrsAudioSummaryComponentType_Title_CollectionPtr tmp = CreateAudioSummaryComponentType_Title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSourceID"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSourceID")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSourceID(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSourceLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSourceLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSourceLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ComponentSourceTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ComponentSourceTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetComponentSourceTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SoundLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SoundLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSoundLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SyncTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SyncTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSyncTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioSummaryComponentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Title")) == 0))
  {
	Mp7JrsAudioSummaryComponentType_Title_CollectionPtr tmp = CreateAudioSummaryComponentType_Title_CollectionType; // FTT, check this
	this->SetTitle(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("AudioSourceID")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetAudioSourceID(child);
  }
  if (XMLString::compareString(elementname, X("AudioSourceLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetAudioSourceLocator(child);
  }
  if (XMLString::compareString(elementname, X("ComponentSourceTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetComponentSourceTime(child);
  }
  if (XMLString::compareString(elementname, X("SoundLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetSoundLocator(child);
  }
  if (XMLString::compareString(elementname, X("SyncTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetSyncTime(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioSummaryComponentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTitle() != Dc1NodePtr())
		result.Insert(GetTitle());
	if (GetAudioSourceID() != Dc1NodePtr())
		result.Insert(GetAudioSourceID());
	if (GetAudioSourceLocator() != Dc1NodePtr())
		result.Insert(GetAudioSourceLocator());
	if (GetComponentSourceTime() != Dc1NodePtr())
		result.Insert(GetComponentSourceTime());
	if (GetSoundLocator() != Dc1NodePtr())
		result.Insert(GetSoundLocator());
	if (GetSyncTime() != Dc1NodePtr())
		result.Insert(GetSyncTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioSummaryComponentType_ExtMethodImpl.h


