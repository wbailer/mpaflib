
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtImplInclude.h


#include "Mp7JrsImageLocatorType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsTitleMediaType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTitleMediaType::IMp7JrsTitleMediaType()
{

// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtPropInit.h

}

IMp7JrsTitleMediaType::~IMp7JrsTitleMediaType()
{
// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtPropCleanup.h

}

Mp7JrsTitleMediaType::Mp7JrsTitleMediaType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTitleMediaType::~Mp7JrsTitleMediaType()
{
	Cleanup();
}

void Mp7JrsTitleMediaType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TitleImage = Mp7JrsImageLocatorPtr(); // Class
	m_TitleImage_Exist = false;
	m_TitleVideo = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_TitleVideo_Exist = false;
	m_TitleAudio = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_TitleAudio_Exist = false;


// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtMyPropInit.h

}

void Mp7JrsTitleMediaType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TitleImage);
	// Dc1Factory::DeleteObject(m_TitleVideo);
	// Dc1Factory::DeleteObject(m_TitleAudio);
}

void Mp7JrsTitleMediaType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TitleMediaTypePtr, since we
	// might need GetBase(), which isn't defined in ITitleMediaType
	const Dc1Ptr< Mp7JrsTitleMediaType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidTitleImage())
	{
		// Dc1Factory::DeleteObject(m_TitleImage);
		this->SetTitleImage(Dc1Factory::CloneObject(tmp->GetTitleImage()));
	}
	else
	{
		InvalidateTitleImage();
	}
	if (tmp->IsValidTitleVideo())
	{
		// Dc1Factory::DeleteObject(m_TitleVideo);
		this->SetTitleVideo(Dc1Factory::CloneObject(tmp->GetTitleVideo()));
	}
	else
	{
		InvalidateTitleVideo();
	}
	if (tmp->IsValidTitleAudio())
	{
		// Dc1Factory::DeleteObject(m_TitleAudio);
		this->SetTitleAudio(Dc1Factory::CloneObject(tmp->GetTitleAudio()));
	}
	else
	{
		InvalidateTitleAudio();
	}
}

Dc1NodePtr Mp7JrsTitleMediaType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTitleMediaType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsImageLocatorPtr Mp7JrsTitleMediaType::GetTitleImage() const
{
		return m_TitleImage;
}

// Element is optional
bool Mp7JrsTitleMediaType::IsValidTitleImage() const
{
	return m_TitleImage_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsTitleMediaType::GetTitleVideo() const
{
		return m_TitleVideo;
}

// Element is optional
bool Mp7JrsTitleMediaType::IsValidTitleVideo() const
{
	return m_TitleVideo_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsTitleMediaType::GetTitleAudio() const
{
		return m_TitleAudio;
}

// Element is optional
bool Mp7JrsTitleMediaType::IsValidTitleAudio() const
{
	return m_TitleAudio_Exist;
}

void Mp7JrsTitleMediaType::SetTitleImage(const Mp7JrsImageLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleMediaType::SetTitleImage().");
	}
	if (m_TitleImage != item || m_TitleImage_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TitleImage);
		m_TitleImage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TitleImage) m_TitleImage->SetParent(m_myself.getPointer());
		if (m_TitleImage && m_TitleImage->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TitleImage->UseTypeAttribute = true;
		}
		m_TitleImage_Exist = true;
		if(m_TitleImage != Mp7JrsImageLocatorPtr())
		{
			m_TitleImage->SetContentName(XMLString::transcode("TitleImage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleMediaType::InvalidateTitleImage()
{
	m_TitleImage_Exist = false;
}
void Mp7JrsTitleMediaType::SetTitleVideo(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleMediaType::SetTitleVideo().");
	}
	if (m_TitleVideo != item || m_TitleVideo_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TitleVideo);
		m_TitleVideo = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TitleVideo) m_TitleVideo->SetParent(m_myself.getPointer());
		if (m_TitleVideo && m_TitleVideo->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TitleVideo->UseTypeAttribute = true;
		}
		m_TitleVideo_Exist = true;
		if(m_TitleVideo != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_TitleVideo->SetContentName(XMLString::transcode("TitleVideo"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleMediaType::InvalidateTitleVideo()
{
	m_TitleVideo_Exist = false;
}
void Mp7JrsTitleMediaType::SetTitleAudio(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleMediaType::SetTitleAudio().");
	}
	if (m_TitleAudio != item || m_TitleAudio_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TitleAudio);
		m_TitleAudio = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TitleAudio) m_TitleAudio->SetParent(m_myself.getPointer());
		if (m_TitleAudio && m_TitleAudio->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TitleAudio->UseTypeAttribute = true;
		}
		m_TitleAudio_Exist = true;
		if(m_TitleAudio != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_TitleAudio->SetContentName(XMLString::transcode("TitleAudio"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleMediaType::InvalidateTitleAudio()
{
	m_TitleAudio_Exist = false;
}

Dc1NodeEnum Mp7JrsTitleMediaType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TitleImage")) == 0)
	{
		// TitleImage is simple element ImageLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTitleImage()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTitleImage(p, client);
					if((p = GetTitleImage()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TitleVideo")) == 0)
	{
		// TitleVideo is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTitleVideo()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTitleVideo(p, client);
					if((p = GetTitleVideo()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TitleAudio")) == 0)
	{
		// TitleAudio is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTitleAudio()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTitleAudio(p, client);
					if((p = GetTitleAudio()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TitleMediaType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TitleMediaType");
	}
	return result;
}

XMLCh * Mp7JrsTitleMediaType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTitleMediaType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTitleMediaType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TitleMediaType"));
	// Element serialization:
	// Class
	
	if (m_TitleImage != Mp7JrsImageLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TitleImage->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TitleImage"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TitleImage->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TitleVideo != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TitleVideo->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TitleVideo"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TitleVideo->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TitleAudio != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TitleAudio->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TitleAudio"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TitleAudio->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTitleMediaType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TitleImage"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TitleImage")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitleImage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TitleVideo"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TitleVideo")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitleVideo(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TitleAudio"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TitleAudio")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitleAudio(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTitleMediaType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TitleImage")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageLocatorType; // FTT, check this
	}
	this->SetTitleImage(child);
  }
  if (XMLString::compareString(elementname, X("TitleVideo")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetTitleVideo(child);
  }
  if (XMLString::compareString(elementname, X("TitleAudio")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetTitleAudio(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTitleMediaType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTitleImage() != Dc1NodePtr())
		result.Insert(GetTitleImage());
	if (GetTitleVideo() != Dc1NodePtr())
		result.Insert(GetTitleVideo());
	if (GetTitleAudio() != Dc1NodePtr())
		result.Insert(GetTitleAudio());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTitleMediaType_ExtMethodImpl.h


