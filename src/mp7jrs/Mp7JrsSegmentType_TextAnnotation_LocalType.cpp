
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtImplInclude.h


#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_type_LocalType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsTextAnnotationType_CollectionType.h"
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextAnnotationType_LocalType.h" // Choice collection FreeTextAnnotation
#include "Mp7JrsTextualType.h" // Choice collection element FreeTextAnnotation
#include "Mp7JrsStructuredAnnotationType.h" // Choice collection element StructuredAnnotation
#include "Mp7JrsDependencyStructureType.h" // Choice collection element DependencyStructure
#include "Mp7JrsKeywordAnnotationType.h" // Choice collection element KeywordAnnotation

#include <assert.h>
IMp7JrsSegmentType_TextAnnotation_LocalType::IMp7JrsSegmentType_TextAnnotation_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtPropInit.h

}

IMp7JrsSegmentType_TextAnnotation_LocalType::~IMp7JrsSegmentType_TextAnnotation_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtPropCleanup.h

}

Mp7JrsSegmentType_TextAnnotation_LocalType::Mp7JrsSegmentType_TextAnnotation_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSegmentType_TextAnnotation_LocalType::~Mp7JrsSegmentType_TextAnnotation_LocalType()
{
	Cleanup();
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Init()
{
	// Init base
	m_Base = CreateTextAnnotationType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_type = Mp7JrsSegmentType_TextAnnotation_type_LocalPtr(); // Create emtpy class ptr
	m_type_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtMyPropInit.h

}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_type);
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SegmentType_TextAnnotation_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISegmentType_TextAnnotation_LocalType
	const Dc1Ptr< Mp7JrsSegmentType_TextAnnotation_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTextAnnotationPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSegmentType_TextAnnotation_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
}

Dc1NodePtr Mp7JrsSegmentType_TextAnnotation_LocalType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsSegmentType_TextAnnotation_LocalType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsTextAnnotationType > Mp7JrsSegmentType_TextAnnotation_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsSegmentType_TextAnnotation_type_LocalPtr Mp7JrsSegmentType_TextAnnotation_LocalType::Gettype() const
{
	return m_type;
}

bool Mp7JrsSegmentType_TextAnnotation_LocalType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsSegmentType_TextAnnotation_LocalType::Settype(const Mp7JrsSegmentType_TextAnnotation_type_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType_TextAnnotation_LocalType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsSegmentType_TextAnnotation_LocalType::Getrelevance() const
{
	return GetBase()->Getrelevance();
}

bool Mp7JrsSegmentType_TextAnnotation_LocalType::Existrelevance() const
{
	return GetBase()->Existrelevance();
}
void Mp7JrsSegmentType_TextAnnotation_LocalType::Setrelevance(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType_TextAnnotation_LocalType::Setrelevance().");
	}
	GetBase()->Setrelevance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Invalidaterelevance()
{
	GetBase()->Invalidaterelevance();
}
Mp7JrszeroToOnePtr Mp7JrsSegmentType_TextAnnotation_LocalType::Getconfidence() const
{
	return GetBase()->Getconfidence();
}

bool Mp7JrsSegmentType_TextAnnotation_LocalType::Existconfidence() const
{
	return GetBase()->Existconfidence();
}
void Mp7JrsSegmentType_TextAnnotation_LocalType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType_TextAnnotation_LocalType::Setconfidence().");
	}
	GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Invalidateconfidence()
{
	GetBase()->Invalidateconfidence();
}
XMLCh * Mp7JrsSegmentType_TextAnnotation_LocalType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsSegmentType_TextAnnotation_LocalType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsSegmentType_TextAnnotation_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType_TextAnnotation_LocalType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsTextAnnotationType_CollectionPtr Mp7JrsSegmentType_TextAnnotation_LocalType::GetTextAnnotationType_LocalType() const
{
	return GetBase()->GetTextAnnotationType_LocalType();
}

void Mp7JrsSegmentType_TextAnnotation_LocalType::SetTextAnnotationType_LocalType(const Mp7JrsTextAnnotationType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType_TextAnnotation_LocalType::SetTextAnnotationType_LocalType().");
	}
	GetBase()->SetTextAnnotationType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSegmentType_TextAnnotation_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute SegmentType_TextAnnotation_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSegmentType_TextAnnotation_type_LocalPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SegmentType_TextAnnotation_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SegmentType_TextAnnotation_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSegmentType_TextAnnotation_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSegmentType_TextAnnotation_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSegmentType_TextAnnotation_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SegmentType_TextAnnotation_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Class
	if (m_type != Mp7JrsSegmentType_TextAnnotation_type_LocalPtr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSegmentType_TextAnnotation_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize class type
		Mp7JrsSegmentType_TextAnnotation_type_LocalPtr tmp = CreateSegmentType_TextAnnotation_type_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsTextAnnotationType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSegmentType_TextAnnotation_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSegmentType_TextAnnotation_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTextAnnotationType_LocalType() != Dc1NodePtr())
		result.Insert(GetTextAnnotationType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSegmentType_TextAnnotation_LocalType_ExtMethodImpl.h


