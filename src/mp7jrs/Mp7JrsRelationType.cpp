
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRelationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrstermReferenceListType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsRelationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsRelationType::IMp7JrsRelationType()
{

// no includefile for extension defined 
// file Mp7JrsRelationType_ExtPropInit.h

}

IMp7JrsRelationType::~IMp7JrsRelationType()
{
// no includefile for extension defined 
// file Mp7JrsRelationType_ExtPropCleanup.h

}

Mp7JrsRelationType::Mp7JrsRelationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRelationType::~Mp7JrsRelationType()
{
	Cleanup();
}

void Mp7JrsRelationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_type = Mp7JrstermReferenceListPtr(); // Collection
	m_type_Exist = false;
	m_source = Mp7JrstermReferenceListPtr(); // Collection
	m_source_Exist = false;
	m_target = Mp7JrstermReferenceListPtr(); // Collection
	m_target_Exist = false;
	m_directed = false; // Value
	m_directed_Default = true; // Default value
	m_directed_Exist = false;
	m_strength = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_strength->SetContent(0.0); // Use Value initialiser (xxx2)
	m_strength_Exist = false;



// no includefile for extension defined 
// file Mp7JrsRelationType_ExtMyPropInit.h

}

void Mp7JrsRelationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRelationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_type);
	// Dc1Factory::DeleteObject(m_source);
	// Dc1Factory::DeleteObject(m_target);
	// Dc1Factory::DeleteObject(m_strength);
}

void Mp7JrsRelationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RelationTypePtr, since we
	// might need GetBase(), which isn't defined in IRelationType
	const Dc1Ptr< Mp7JrsRelationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsRelationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_source);
	if (tmp->Existsource())
	{
		this->Setsource(Dc1Factory::CloneObject(tmp->Getsource()));
	}
	else
	{
		Invalidatesource();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_target);
	if (tmp->Existtarget())
	{
		this->Settarget(Dc1Factory::CloneObject(tmp->Gettarget()));
	}
	else
	{
		Invalidatetarget();
	}
	}
	{
	if (tmp->Existdirected())
	{
		this->Setdirected(tmp->Getdirected());
	}
	else
	{
		Invalidatedirected();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_strength);
	if (tmp->Existstrength())
	{
		this->Setstrength(Dc1Factory::CloneObject(tmp->Getstrength()));
	}
	else
	{
		Invalidatestrength();
	}
	}
}

Dc1NodePtr Mp7JrsRelationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsRelationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsRelationType::GetBase() const
{
	return m_Base;
}

Mp7JrstermReferenceListPtr Mp7JrsRelationType::Gettype() const
{
	return m_type;
}

bool Mp7JrsRelationType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsRelationType::Settype(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsRelationType::Getsource() const
{
	return m_source;
}

bool Mp7JrsRelationType::Existsource() const
{
	return m_source_Exist;
}
void Mp7JrsRelationType::Setsource(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Setsource().");
	}
	m_source = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_source) m_source->SetParent(m_myself.getPointer());
	m_source_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidatesource()
{
	m_source_Exist = false;
}
Mp7JrstermReferenceListPtr Mp7JrsRelationType::Gettarget() const
{
	return m_target;
}

bool Mp7JrsRelationType::Existtarget() const
{
	return m_target_Exist;
}
void Mp7JrsRelationType::Settarget(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Settarget().");
	}
	m_target = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_target) m_target->SetParent(m_myself.getPointer());
	m_target_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidatetarget()
{
	m_target_Exist = false;
}
bool Mp7JrsRelationType::Getdirected() const
{
	if (this->Existdirected()) {
		return m_directed;
	} else {
		return m_directed_Default;
	}
}

bool Mp7JrsRelationType::Existdirected() const
{
	return m_directed_Exist;
}
void Mp7JrsRelationType::Setdirected(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Setdirected().");
	}
	m_directed = item;
	m_directed_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidatedirected()
{
	m_directed_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsRelationType::Getstrength() const
{
	if (this->Existstrength()) {
		return m_strength;
	} else {
		return m_strength_Default;
	}
}

bool Mp7JrsRelationType::Existstrength() const
{
	return m_strength_Exist;
}
void Mp7JrsRelationType::Setstrength(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Setstrength().");
	}
	m_strength = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_strength) m_strength->SetParent(m_myself.getPointer());
	m_strength_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidatestrength()
{
	m_strength_Exist = false;
}
XMLCh * Mp7JrsRelationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsRelationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsRelationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsRelationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsRelationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsRelationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsRelationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsRelationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsRelationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsRelationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsRelationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsRelationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsRelationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsRelationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsRelationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsRelationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsRelationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRelationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 10 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("source")) == 0)
	{
		// source is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("target")) == 0)
	{
		// target is simple attribute termReferenceListType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferenceListPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("directed")) == 0)
	{
		// directed is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("strength")) == 0)
	{
		// strength is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RelationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RelationType");
	}
	return result;
}

XMLCh * Mp7JrsRelationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRelationType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsRelationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsRelationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RelationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Collection
	if(m_type != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_source_Exist)
	{
	// Collection
	if(m_source != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_source->ToText();
		element->setAttributeNS(X(""), X("source"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_target_Exist)
	{
	// Collection
	if(m_target != Mp7JrstermReferenceListPtr())
	{
		XMLCh * tmp = m_target->ToText();
		element->setAttributeNS(X(""), X("target"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_directed_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_directed);
		element->setAttributeNS(X(""), X("directed"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_strength_Exist)
	{
	// Class
	if (m_strength != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_strength->ToText();
		element->setAttributeNS(X(""), X("strength"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsRelationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("source")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("source")));
		this->Setsource(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("target")))
	{
		// Deserialize collection type
		Mp7JrstermReferenceListPtr tmp = CreatetermReferenceListType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("target")));
		this->Settarget(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("directed")))
	{
		// deserialize value type
		this->Setdirected(Dc1Convert::TextToBool(parent->getAttribute(X("directed"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("strength")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("strength")));
		this->Setstrength(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsRelationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRelationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRelationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRelationType_ExtMethodImpl.h


