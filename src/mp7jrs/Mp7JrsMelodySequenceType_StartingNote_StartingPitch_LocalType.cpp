
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtImplInclude.h


#include "Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType.h"
#include "Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtPropInit.h

}

IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::~IMp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtPropCleanup.h

}

Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::~Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType()
{
	Cleanup();
}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Init()
{

	// Init attributes
	m_accidental = Mp7JrsdegreeAccidentalType::UninitializedEnumeration;
	m_accidental_Default = Mp7JrsdegreeAccidentalType::natural; // Default enumeration
	m_accidental_Exist = false;
	m_height = 0; // Value
	m_height_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_PitchNote = Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtMyPropInit.h

}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_PitchNote);
}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MelodySequenceType_StartingNote_StartingPitch_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMelodySequenceType_StartingNote_StartingPitch_LocalType
	const Dc1Ptr< Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->Existaccidental())
	{
		this->Setaccidental(tmp->Getaccidental());
	}
	else
	{
		Invalidateaccidental();
	}
	}
	{
	if (tmp->Existheight())
	{
		this->Setheight(tmp->Getheight());
	}
	else
	{
		Invalidateheight();
	}
	}
		// Dc1Factory::DeleteObject(m_PitchNote);
		this->SetPitchNote(Dc1Factory::CloneObject(tmp->GetPitchNote()));
}

Dc1NodePtr Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsdegreeAccidentalType::Enumeration Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Getaccidental() const
{
	if (this->Existaccidental()) {
		return m_accidental;
	} else {
		return m_accidental_Default;
	}
}

bool Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Existaccidental() const
{
	return m_accidental_Exist;
}
void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Setaccidental(Mp7JrsdegreeAccidentalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Setaccidental().");
	}
	m_accidental = item;
	m_accidental_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Invalidateaccidental()
{
	m_accidental_Exist = false;
}
int Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Getheight() const
{
	return m_height;
}

bool Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Existheight() const
{
	return m_height_Exist;
}
void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Setheight(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Setheight().");
	}
	m_height = item;
	m_height_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Invalidateheight()
{
	m_height_Exist = false;
}
Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::GetPitchNote() const
{
		return m_PitchNote;
}

void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::SetPitchNote(const Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::SetPitchNote().");
	}
	if (m_PitchNote != item)
	{
		// Dc1Factory::DeleteObject(m_PitchNote);
		m_PitchNote = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PitchNote) m_PitchNote->SetParent(m_myself.getPointer());
		if (m_PitchNote && m_PitchNote->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PitchNote->UseTypeAttribute = true;
		}
		if(m_PitchNote != Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr())
		{
			m_PitchNote->SetContentName(XMLString::transcode("PitchNote"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("accidental")) == 0)
	{
		// accidental is simple attribute degreeAccidentalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsdegreeAccidentalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("height")) == 0)
	{
		// height is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("PitchNote")) == 0)
	{
		// PitchNote is simple element MelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPitchNote()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPitchNote(p, client);
					if((p = GetPitchNote()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MelodySequenceType_StartingNote_StartingPitch_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MelodySequenceType_StartingNote_StartingPitch_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MelodySequenceType_StartingNote_StartingPitch_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_accidental_Exist)
	{
	// Enumeration
	if(m_accidental != Mp7JrsdegreeAccidentalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsdegreeAccidentalType::ToText(m_accidental);
		element->setAttributeNS(X(""), X("accidental"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_height_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_height);
		element->setAttributeNS(X(""), X("height"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_PitchNote != Mp7JrsMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PitchNote->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PitchNote"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PitchNote->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("accidental")))
	{
		this->Setaccidental(Mp7JrsdegreeAccidentalType::Parse(parent->getAttribute(X("accidental"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("height")))
	{
		// deserialize value type
		this->Setheight(Dc1Convert::TextToInt(parent->getAttribute(X("height"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PitchNote"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PitchNote")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPitchNote(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("PitchNote")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMelodySequenceType_StartingNote_StartingPitch_PitchNote_LocalType; // FTT, check this
	}
	this->SetPitchNote(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPitchNote() != Dc1NodePtr())
		result.Insert(GetPitchNote());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_StartingNote_StartingPitch_LocalType_ExtMethodImpl.h


