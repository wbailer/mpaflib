
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtImplInclude.h


#include "Mp7JrsMosaicType_WarpingParam_modelType_LocalType2.h"
#include "Mp7JrsMosaicType_WarpingParam_CollectionType.h"
#include "Mp7JrsMosaicType_WarpingParam_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMosaicType_WarpingParam_LocalType.h" // Sequence collection MediaTimePoint
#include "Mp7JrsmediaTimePointType.h" // Sequence collection element MediaTimePoint
#include "Mp7JrsMediaRelTimePointType.h" // Sequence collection element MediaRelTimePoint
#include "Mp7JrsMediaRelIncrTimePointType.h" // Sequence collection element MediaRelIncrTimePoint
#include "Mp7JrsfloatVector.h" // Sequence collection element MotionParam

#include <assert.h>
IMp7JrsMosaicType_WarpingParam_LocalType0::IMp7JrsMosaicType_WarpingParam_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtPropInit.h

}

IMp7JrsMosaicType_WarpingParam_LocalType0::~IMp7JrsMosaicType_WarpingParam_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtPropCleanup.h

}

Mp7JrsMosaicType_WarpingParam_LocalType0::Mp7JrsMosaicType_WarpingParam_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMosaicType_WarpingParam_LocalType0::~Mp7JrsMosaicType_WarpingParam_LocalType0()
{
	Cleanup();
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::Init()
{

	// Init attributes
	m_modelType = Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr(); // Create emtpy class ptr
	m_sourceSequenceWidth = 1; // Value
	m_sourceSequenceHeight = 1; // Value
	m_xOffset = 0.0f; // Value
	m_yOffset = 0.0f; // Value
	m_xOrigin = 0.0f; // Value
	m_yOrigin = 0.0f; // Value

	// Init elements (element, union sequence choice all any)
	
	m_MosaicType_WarpingParam_LocalType = Mp7JrsMosaicType_WarpingParam_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtMyPropInit.h

}

void Mp7JrsMosaicType_WarpingParam_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_modelType);
	// Dc1Factory::DeleteObject(m_MosaicType_WarpingParam_LocalType);
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MosaicType_WarpingParam_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in IMosaicType_WarpingParam_LocalType0
	const Dc1Ptr< Mp7JrsMosaicType_WarpingParam_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_modelType);
		this->SetmodelType(Dc1Factory::CloneObject(tmp->GetmodelType()));
	}
	{
		this->SetsourceSequenceWidth(tmp->GetsourceSequenceWidth());
	}
	{
		this->SetsourceSequenceHeight(tmp->GetsourceSequenceHeight());
	}
	{
		this->SetxOffset(tmp->GetxOffset());
	}
	{
		this->SetyOffset(tmp->GetyOffset());
	}
	{
		this->SetxOrigin(tmp->GetxOrigin());
	}
	{
		this->SetyOrigin(tmp->GetyOrigin());
	}
		// Dc1Factory::DeleteObject(m_MosaicType_WarpingParam_LocalType);
		this->SetMosaicType_WarpingParam_LocalType(Dc1Factory::CloneObject(tmp->GetMosaicType_WarpingParam_LocalType()));
}

Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr Mp7JrsMosaicType_WarpingParam_LocalType0::GetmodelType() const
{
	return m_modelType;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetmodelType(const Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetmodelType().");
	}
	m_modelType = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_modelType) m_modelType->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMosaicType_WarpingParam_LocalType0::GetsourceSequenceWidth() const
{
	return m_sourceSequenceWidth;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetsourceSequenceWidth(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetsourceSequenceWidth().");
	}
	m_sourceSequenceWidth = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMosaicType_WarpingParam_LocalType0::GetsourceSequenceHeight() const
{
	return m_sourceSequenceHeight;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetsourceSequenceHeight(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetsourceSequenceHeight().");
	}
	m_sourceSequenceHeight = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float Mp7JrsMosaicType_WarpingParam_LocalType0::GetxOffset() const
{
	return m_xOffset;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetxOffset(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetxOffset().");
	}
	m_xOffset = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float Mp7JrsMosaicType_WarpingParam_LocalType0::GetyOffset() const
{
	return m_yOffset;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetyOffset(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetyOffset().");
	}
	m_yOffset = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float Mp7JrsMosaicType_WarpingParam_LocalType0::GetxOrigin() const
{
	return m_xOrigin;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetxOrigin(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetxOrigin().");
	}
	m_xOrigin = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

float Mp7JrsMosaicType_WarpingParam_LocalType0::GetyOrigin() const
{
	return m_yOrigin;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetyOrigin(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetyOrigin().");
	}
	m_yOrigin = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMosaicType_WarpingParam_CollectionPtr Mp7JrsMosaicType_WarpingParam_LocalType0::GetMosaicType_WarpingParam_LocalType() const
{
		return m_MosaicType_WarpingParam_LocalType;
}

void Mp7JrsMosaicType_WarpingParam_LocalType0::SetMosaicType_WarpingParam_LocalType(const Mp7JrsMosaicType_WarpingParam_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType0::SetMosaicType_WarpingParam_LocalType().");
	}
	if (m_MosaicType_WarpingParam_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_MosaicType_WarpingParam_LocalType);
		m_MosaicType_WarpingParam_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MosaicType_WarpingParam_LocalType) m_MosaicType_WarpingParam_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMosaicType_WarpingParam_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("modelType")) == 0)
	{
		// modelType is simple attribute MosaicType_WarpingParam_modelType_LocalType2
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("sourceSequenceWidth")) == 0)
	{
		// sourceSequenceWidth is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("sourceSequenceHeight")) == 0)
	{
		// sourceSequenceHeight is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("xOffset")) == 0)
	{
		// xOffset is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("yOffset")) == 0)
	{
		// yOffset is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("xOrigin")) == 0)
	{
		// xOrigin is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("yOrigin")) == 0)
	{
		// yOrigin is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTimePoint")) == 0)
	{
		// MediaTimePoint is contained in itemtype MosaicType_WarpingParam_LocalType
		// in sequence collection MosaicType_WarpingParam_CollectionType

		context->Found = true;
		Mp7JrsMosaicType_WarpingParam_CollectionPtr coll = GetMosaicType_WarpingParam_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
				SetMosaicType_WarpingParam_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaTimePoint()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsMosaicType_WarpingParam_LocalPtr item = CreateMosaicType_WarpingParam_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j+1))->GetMediaTimePoint());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j-1))->GetMediaTimePoint());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->SetMediaTimePoint(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelTimePoint")) == 0)
	{
		// MediaRelTimePoint is contained in itemtype MosaicType_WarpingParam_LocalType
		// in sequence collection MosaicType_WarpingParam_CollectionType

		context->Found = true;
		Mp7JrsMosaicType_WarpingParam_CollectionPtr coll = GetMosaicType_WarpingParam_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
				SetMosaicType_WarpingParam_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaRelTimePoint()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsMosaicType_WarpingParam_LocalPtr item = CreateMosaicType_WarpingParam_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaRelTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j+1))->GetMediaRelTimePoint());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaRelTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j-1))->GetMediaRelTimePoint());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->SetMediaRelTimePoint(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaRelTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelIncrTimePoint")) == 0)
	{
		// MediaRelIncrTimePoint is contained in itemtype MosaicType_WarpingParam_LocalType
		// in sequence collection MosaicType_WarpingParam_CollectionType

		context->Found = true;
		Mp7JrsMosaicType_WarpingParam_CollectionPtr coll = GetMosaicType_WarpingParam_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
				SetMosaicType_WarpingParam_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaRelIncrTimePoint()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsMosaicType_WarpingParam_LocalPtr item = CreateMosaicType_WarpingParam_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaRelIncrTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j+1))->GetMediaRelIncrTimePoint());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMediaRelIncrTimePoint(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j-1))->GetMediaRelIncrTimePoint());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->SetMediaRelIncrTimePoint(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMediaRelIncrTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MotionParam")) == 0)
	{
		// MotionParam is contained in itemtype MosaicType_WarpingParam_LocalType
		// in sequence collection MosaicType_WarpingParam_CollectionType

		context->Found = true;
		Mp7JrsMosaicType_WarpingParam_CollectionPtr coll = GetMosaicType_WarpingParam_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
				SetMosaicType_WarpingParam_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMotionParam()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsMosaicType_WarpingParam_LocalPtr item = CreateMosaicType_WarpingParam_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMotionParam(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j+1))->GetMotionParam());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j))->SetMotionParam(
						((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(j-1))->GetMotionParam());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->SetMotionParam(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsMosaicType_WarpingParam_LocalPtr)coll->elementAt(i))->GetMotionParam()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MosaicType_WarpingParam_LocalType0
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MosaicType_WarpingParam_LocalType0");
	}
	return result;
}

XMLCh * Mp7JrsMosaicType_WarpingParam_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMosaicType_WarpingParam_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMosaicType_WarpingParam_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MosaicType_WarpingParam_LocalType0"));
	// Attribute Serialization:
	// Class
	if (m_modelType != Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr())
	{
		XMLCh * tmp = m_modelType->ToText();
		element->setAttributeNS(X(""), X("modelType"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_sourceSequenceWidth);
		element->setAttributeNS(X(""), X("sourceSequenceWidth"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_sourceSequenceHeight);
		element->setAttributeNS(X(""), X("sourceSequenceHeight"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_xOffset);
		element->setAttributeNS(X(""), X("xOffset"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_yOffset);
		element->setAttributeNS(X(""), X("yOffset"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_xOrigin);
		element->setAttributeNS(X(""), X("xOrigin"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_yOrigin);
		element->setAttributeNS(X(""), X("yOrigin"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	if (m_MosaicType_WarpingParam_LocalType != Mp7JrsMosaicType_WarpingParam_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MosaicType_WarpingParam_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMosaicType_WarpingParam_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("modelType")))
	{
		// Deserialize class type
		Mp7JrsMosaicType_WarpingParam_modelType_Local2Ptr tmp = CreateMosaicType_WarpingParam_modelType_LocalType2; // FTT, check this
		tmp->Parse(parent->getAttribute(X("modelType")));
		this->SetmodelType(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("sourceSequenceWidth")))
	{
		// deserialize value type
		this->SetsourceSequenceWidth(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("sourceSequenceWidth"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("sourceSequenceHeight")))
	{
		// deserialize value type
		this->SetsourceSequenceHeight(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("sourceSequenceHeight"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xOffset")))
	{
		// deserialize value type
		this->SetxOffset(Dc1Convert::TextToFloat(parent->getAttribute(X("xOffset"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yOffset")))
	{
		// deserialize value type
		this->SetyOffset(Dc1Convert::TextToFloat(parent->getAttribute(X("yOffset"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xOrigin")))
	{
		// deserialize value type
		this->SetxOrigin(Dc1Convert::TextToFloat(parent->getAttribute(X("xOrigin"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yOrigin")))
	{
		// deserialize value type
		this->SetyOrigin(Dc1Convert::TextToFloat(parent->getAttribute(X("yOrigin"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MediaTimePoint")) == 0)
			(Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePoint")) == 0)
			(Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePoint")) == 0)
			(Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MotionParam")) == 0)
			(Dc1Util::HasNodeName(parent, X("MotionParam"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsMosaicType_WarpingParam_CollectionPtr tmp = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMosaicType_WarpingParam_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsMosaicType_WarpingParam_CollectionPtr tmp = CreateMosaicType_WarpingParam_CollectionType; // FTT, check this
	this->SetMosaicType_WarpingParam_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMosaicType_WarpingParam_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMosaicType_WarpingParam_LocalType() != Dc1NodePtr())
		result.Insert(GetMosaicType_WarpingParam_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType0_ExtMethodImpl.h


