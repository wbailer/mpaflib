
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtImplInclude.h


#include "Mp7JrsModelType.h"
#include "Mp7JrsAnalyticModelType_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAnalyticModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAnalyticModelType_LocalType.h" // Choice collection Label
#include "Mp7JrsAnalyticModelType_Label_LocalType.h" // Choice collection element Label
#include "Mp7JrsAnalyticModelType_Semantics_LocalType.h" // Choice collection element Semantics

#include <assert.h>
IMp7JrsAnalyticModelType::IMp7JrsAnalyticModelType()
{

// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtPropInit.h

}

IMp7JrsAnalyticModelType::~IMp7JrsAnalyticModelType()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtPropCleanup.h

}

Mp7JrsAnalyticModelType::Mp7JrsAnalyticModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAnalyticModelType::~Mp7JrsAnalyticModelType()
{
	Cleanup();
}

void Mp7JrsAnalyticModelType::Init()
{
	// Init base
	m_Base = CreateModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_function = Mp7JrsAnalyticModelType_function_LocalType::UninitializedEnumeration;
	m_function_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_AnalyticModelType_LocalType = Mp7JrsAnalyticModelType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtMyPropInit.h

}

void Mp7JrsAnalyticModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_AnalyticModelType_LocalType);
}

void Mp7JrsAnalyticModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AnalyticModelTypePtr, since we
	// might need GetBase(), which isn't defined in IAnalyticModelType
	const Dc1Ptr< Mp7JrsAnalyticModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAnalyticModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existfunction())
	{
		this->Setfunction(tmp->Getfunction());
	}
	else
	{
		Invalidatefunction();
	}
	}
		// Dc1Factory::DeleteObject(m_AnalyticModelType_LocalType);
		this->SetAnalyticModelType_LocalType(Dc1Factory::CloneObject(tmp->GetAnalyticModelType_LocalType()));
}

Dc1NodePtr Mp7JrsAnalyticModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAnalyticModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsModelType > Mp7JrsAnalyticModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsAnalyticModelType_function_LocalType::Enumeration Mp7JrsAnalyticModelType::Getfunction() const
{
	return m_function;
}

bool Mp7JrsAnalyticModelType::Existfunction() const
{
	return m_function_Exist;
}
void Mp7JrsAnalyticModelType::Setfunction(Mp7JrsAnalyticModelType_function_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::Setfunction().");
	}
	m_function = item;
	m_function_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::Invalidatefunction()
{
	m_function_Exist = false;
}
Mp7JrsAnalyticModelType_CollectionPtr Mp7JrsAnalyticModelType::GetAnalyticModelType_LocalType() const
{
		return m_AnalyticModelType_LocalType;
}

void Mp7JrsAnalyticModelType::SetAnalyticModelType_LocalType(const Mp7JrsAnalyticModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SetAnalyticModelType_LocalType().");
	}
	if (m_AnalyticModelType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_AnalyticModelType_LocalType);
		m_AnalyticModelType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AnalyticModelType_LocalType) m_AnalyticModelType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsAnalyticModelType::Getconfidence() const
{
	return GetBase()->Getconfidence();
}

bool Mp7JrsAnalyticModelType::Existconfidence() const
{
	return GetBase()->Existconfidence();
}
void Mp7JrsAnalyticModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::Setconfidence().");
	}
	GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::Invalidateconfidence()
{
	GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsAnalyticModelType::Getreliability() const
{
	return GetBase()->Getreliability();
}

bool Mp7JrsAnalyticModelType::Existreliability() const
{
	return GetBase()->Existreliability();
}
void Mp7JrsAnalyticModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::Setreliability().");
	}
	GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::Invalidatereliability()
{
	GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsAnalyticModelType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsAnalyticModelType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsAnalyticModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsAnalyticModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsAnalyticModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAnalyticModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsAnalyticModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsAnalyticModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAnalyticModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAnalyticModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAnalyticModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAnalyticModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAnalyticModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAnalyticModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAnalyticModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAnalyticModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsAnalyticModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAnalyticModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("function")) == 0)
	{
		// function is simple attribute AnalyticModelType_function_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsAnalyticModelType_function_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Label")) == 0)
	{
		// Label is contained in itemtype AnalyticModelType_LocalType
		// in choice collection AnalyticModelType_CollectionType

		context->Found = true;
		Mp7JrsAnalyticModelType_CollectionPtr coll = GetAnalyticModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAnalyticModelType_CollectionType; // FTT, check this
				SetAnalyticModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->GetLabel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAnalyticModelType_LocalPtr item = CreateAnalyticModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Label_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAnalyticModelType_Label_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->SetLabel(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->GetLabel()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Semantics")) == 0)
	{
		// Semantics is contained in itemtype AnalyticModelType_LocalType
		// in choice collection AnalyticModelType_CollectionType

		context->Found = true;
		Mp7JrsAnalyticModelType_CollectionPtr coll = GetAnalyticModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAnalyticModelType_CollectionType; // FTT, check this
				SetAnalyticModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->GetSemantics()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsAnalyticModelType_LocalPtr item = CreateAnalyticModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Semantics_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAnalyticModelType_Semantics_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->SetSemantics(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsAnalyticModelType_LocalPtr)coll->elementAt(i))->GetSemantics()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AnalyticModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AnalyticModelType");
	}
	return result;
}

XMLCh * Mp7JrsAnalyticModelType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAnalyticModelType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAnalyticModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AnalyticModelType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_function_Exist)
	{
	// Enumeration
	if(m_function != Mp7JrsAnalyticModelType_function_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsAnalyticModelType_function_LocalType::ToText(m_function);
		element->setAttributeNS(X(""), X("function"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_AnalyticModelType_LocalType != Mp7JrsAnalyticModelType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AnalyticModelType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAnalyticModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("function")))
	{
		this->Setfunction(Mp7JrsAnalyticModelType_function_LocalType::Parse(parent->getAttribute(X("function"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Label
			Dc1Util::HasNodeName(parent, X("Label"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Label")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Semantics
			Dc1Util::HasNodeName(parent, X("Semantics"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Semantics")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsAnalyticModelType_CollectionPtr tmp = CreateAnalyticModelType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAnalyticModelType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAnalyticModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Label"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Label")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Semantics"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Semantics")) == 0)
	)
  {
	Mp7JrsAnalyticModelType_CollectionPtr tmp = CreateAnalyticModelType_CollectionType; // FTT, check this
	this->SetAnalyticModelType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAnalyticModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAnalyticModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetAnalyticModelType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_ExtMethodImpl.h


