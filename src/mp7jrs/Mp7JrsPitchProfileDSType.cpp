
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtImplInclude.h


#include "Mp7JrsMelodyType.h"
#include "Mp7JrsReferencePitchType.h"
#include "Mp7JrsProfileWeightsType.h"
#include "Mp7JrsMeterType.h"
#include "Mp7JrsscaleType.h"
#include "Mp7JrsKeyType.h"
#include "Mp7JrsMelodyContourType.h"
#include "Mp7JrsMelodyType_MelodySequence_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsPitchProfileDSType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMelodySequenceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MelodySequence

#include <assert.h>
IMp7JrsPitchProfileDSType::IMp7JrsPitchProfileDSType()
{

// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtPropInit.h

}

IMp7JrsPitchProfileDSType::~IMp7JrsPitchProfileDSType()
{
// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtPropCleanup.h

}

Mp7JrsPitchProfileDSType::Mp7JrsPitchProfileDSType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPitchProfileDSType::~Mp7JrsPitchProfileDSType()
{
	Cleanup();
}

void Mp7JrsPitchProfileDSType::Init()
{
	// Init base
	m_Base = CreateMelodyType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ReferencePitch = Mp7JrsReferencePitchPtr(); // Class
	m_ReferencePitch_Exist = false;
	m_TransposingRatio = (0.0f); // Value

	m_TransposingRatio_Exist = false;
	m_ProfileWeights = Mp7JrsProfileWeightsPtr(); // Class
	m_ProfileWeights_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtMyPropInit.h

}

void Mp7JrsPitchProfileDSType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ReferencePitch);
	// Dc1Factory::DeleteObject(m_ProfileWeights);
}

void Mp7JrsPitchProfileDSType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PitchProfileDSTypePtr, since we
	// might need GetBase(), which isn't defined in IPitchProfileDSType
	const Dc1Ptr< Mp7JrsPitchProfileDSType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMelodyPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPitchProfileDSType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidReferencePitch())
	{
		// Dc1Factory::DeleteObject(m_ReferencePitch);
		this->SetReferencePitch(Dc1Factory::CloneObject(tmp->GetReferencePitch()));
	}
	else
	{
		InvalidateReferencePitch();
	}
	if (tmp->IsValidTransposingRatio())
	{
		this->SetTransposingRatio(tmp->GetTransposingRatio());
	}
	else
	{
		InvalidateTransposingRatio();
	}
	if (tmp->IsValidProfileWeights())
	{
		// Dc1Factory::DeleteObject(m_ProfileWeights);
		this->SetProfileWeights(Dc1Factory::CloneObject(tmp->GetProfileWeights()));
	}
	else
	{
		InvalidateProfileWeights();
	}
}

Dc1NodePtr Mp7JrsPitchProfileDSType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPitchProfileDSType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMelodyType > Mp7JrsPitchProfileDSType::GetBase() const
{
	return m_Base;
}

Mp7JrsReferencePitchPtr Mp7JrsPitchProfileDSType::GetReferencePitch() const
{
		return m_ReferencePitch;
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidReferencePitch() const
{
	return m_ReferencePitch_Exist;
}

float Mp7JrsPitchProfileDSType::GetTransposingRatio() const
{
		return m_TransposingRatio;
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidTransposingRatio() const
{
	return m_TransposingRatio_Exist;
}

Mp7JrsProfileWeightsPtr Mp7JrsPitchProfileDSType::GetProfileWeights() const
{
		return m_ProfileWeights;
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidProfileWeights() const
{
	return m_ProfileWeights_Exist;
}

void Mp7JrsPitchProfileDSType::SetReferencePitch(const Mp7JrsReferencePitchPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetReferencePitch().");
	}
	if (m_ReferencePitch != item || m_ReferencePitch_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ReferencePitch);
		m_ReferencePitch = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ReferencePitch) m_ReferencePitch->SetParent(m_myself.getPointer());
		if (m_ReferencePitch && m_ReferencePitch->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ReferencePitch->UseTypeAttribute = true;
		}
		m_ReferencePitch_Exist = true;
		if(m_ReferencePitch != Mp7JrsReferencePitchPtr())
		{
			m_ReferencePitch->SetContentName(XMLString::transcode("ReferencePitch"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateReferencePitch()
{
	m_ReferencePitch_Exist = false;
}
void Mp7JrsPitchProfileDSType::SetTransposingRatio(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetTransposingRatio().");
	}
	if (m_TransposingRatio != item || m_TransposingRatio_Exist == false)
	{
		m_TransposingRatio = item;
		m_TransposingRatio_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateTransposingRatio()
{
	m_TransposingRatio_Exist = false;
}
void Mp7JrsPitchProfileDSType::SetProfileWeights(const Mp7JrsProfileWeightsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetProfileWeights().");
	}
	if (m_ProfileWeights != item || m_ProfileWeights_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ProfileWeights);
		m_ProfileWeights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ProfileWeights) m_ProfileWeights->SetParent(m_myself.getPointer());
		if (m_ProfileWeights && m_ProfileWeights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProfileWeightsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ProfileWeights->UseTypeAttribute = true;
		}
		m_ProfileWeights_Exist = true;
		if(m_ProfileWeights != Mp7JrsProfileWeightsPtr())
		{
			m_ProfileWeights->SetContentName(XMLString::transcode("ProfileWeights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateProfileWeights()
{
	m_ProfileWeights_Exist = false;
}
Mp7JrsMeterPtr Mp7JrsPitchProfileDSType::GetMeter() const
{
	return GetBase()->GetMeter();
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidMeter() const
{
	return GetBase()->IsValidMeter();
}

Mp7JrsscalePtr Mp7JrsPitchProfileDSType::GetScale() const
{
	return GetBase()->GetScale();
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidScale() const
{
	return GetBase()->IsValidScale();
}

Mp7JrsKeyPtr Mp7JrsPitchProfileDSType::GetKey() const
{
	return GetBase()->GetKey();
}

// Element is optional
bool Mp7JrsPitchProfileDSType::IsValidKey() const
{
	return GetBase()->IsValidKey();
}

Mp7JrsMelodyContourPtr Mp7JrsPitchProfileDSType::GetMelodyContour() const
{
	return GetBase()->GetMelodyContour();
}

Mp7JrsMelodyType_MelodySequence_CollectionPtr Mp7JrsPitchProfileDSType::GetMelodySequence() const
{
	return GetBase()->GetMelodySequence();
}

void Mp7JrsPitchProfileDSType::SetMeter(const Mp7JrsMeterPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetMeter().");
	}
	GetBase()->SetMeter(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateMeter()
{
	GetBase()->InvalidateMeter();
}
void Mp7JrsPitchProfileDSType::SetScale(const Mp7JrsscalePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetScale().");
	}
	GetBase()->SetScale(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateScale()
{
	GetBase()->InvalidateScale();
}
void Mp7JrsPitchProfileDSType::SetKey(const Mp7JrsKeyPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetKey().");
	}
	GetBase()->SetKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidateKey()
{
	GetBase()->InvalidateKey();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPitchProfileDSType::SetMelodyContour(const Mp7JrsMelodyContourPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetMelodyContour().");
	}
	GetBase()->SetMelodyContour(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPitchProfileDSType::SetMelodySequence(const Mp7JrsMelodyType_MelodySequence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetMelodySequence().");
	}
	GetBase()->SetMelodySequence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsPitchProfileDSType::Getchannels() const
{
	return GetBase()->GetBase()->Getchannels();
}

bool Mp7JrsPitchProfileDSType::Existchannels() const
{
	return GetBase()->GetBase()->Existchannels();
}
void Mp7JrsPitchProfileDSType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::Setchannels().");
	}
	GetBase()->GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::Invalidatechannels()
{
	GetBase()->GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsPitchProfileDSType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsPitchProfileDSType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsPitchProfileDSType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsPitchProfileDSType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsPitchProfileDSType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsPitchProfileDSType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsPitchProfileDSType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsPitchProfileDSType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsPitchProfileDSType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsPitchProfileDSType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsPitchProfileDSType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsPitchProfileDSType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsPitchProfileDSType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsPitchProfileDSType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsPitchProfileDSType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPitchProfileDSType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsPitchProfileDSType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsPitchProfileDSType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPitchProfileDSType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPitchProfileDSType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ReferencePitch")) == 0)
	{
		// ReferencePitch is simple element ReferencePitchType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetReferencePitch()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePitchPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetReferencePitch(p, client);
					if((p = GetReferencePitch()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ProfileWeights")) == 0)
	{
		// ProfileWeights is simple element ProfileWeightsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetProfileWeights()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProfileWeightsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProfileWeightsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetProfileWeights(p, client);
					if((p = GetProfileWeights()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PitchProfileDSType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PitchProfileDSType");
	}
	return result;
}

XMLCh * Mp7JrsPitchProfileDSType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPitchProfileDSType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPitchProfileDSType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PitchProfileDSType"));
	// Element serialization:
	// Class
	
	if (m_ReferencePitch != Mp7JrsReferencePitchPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ReferencePitch->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ReferencePitch"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ReferencePitch->Serialize(doc, element, &elem);
	}
	if(m_TransposingRatio_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_TransposingRatio);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("TransposingRatio"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_ProfileWeights != Mp7JrsProfileWeightsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ProfileWeights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ProfileWeights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ProfileWeights->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPitchProfileDSType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMelodyType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ReferencePitch"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ReferencePitch")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferencePitchType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetReferencePitch(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("TransposingRatio"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetTransposingRatio(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ProfileWeights"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ProfileWeights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateProfileWeightsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetProfileWeights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPitchProfileDSType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ReferencePitch")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferencePitchType; // FTT, check this
	}
	this->SetReferencePitch(child);
  }
  if (XMLString::compareString(elementname, X("ProfileWeights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateProfileWeightsType; // FTT, check this
	}
	this->SetProfileWeights(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPitchProfileDSType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetReferencePitch() != Dc1NodePtr())
		result.Insert(GetReferencePitch());
	if (GetProfileWeights() != Dc1NodePtr())
		result.Insert(GetProfileWeights());
	if (GetMeter() != Dc1NodePtr())
		result.Insert(GetMeter());
	if (GetScale() != Dc1NodePtr())
		result.Insert(GetScale());
	if (GetKey() != Dc1NodePtr())
		result.Insert(GetKey());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMelodyContour() != Dc1NodePtr())
		result.Insert(GetMelodyContour());
	if (GetMelodySequence() != Dc1NodePtr())
		result.Insert(GetMelodySequence());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPitchProfileDSType_ExtMethodImpl.h


