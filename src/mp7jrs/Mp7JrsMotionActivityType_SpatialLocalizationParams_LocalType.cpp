
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtImplInclude.h


#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalType.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalType.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalType.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtPropInit.h

}

IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::~IMp7JrsMotionActivityType_SpatialLocalizationParams_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtPropCleanup.h

}

Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::~Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType()
{
	Cleanup();
}

void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Vector4 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr(); // Class
	m_Vector16 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr(); // Class
	m_Vector64 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr(); // Class
	m_Vector256 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtMyPropInit.h

}

void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Vector4);
	// Dc1Factory::DeleteObject(m_Vector16);
	// Dc1Factory::DeleteObject(m_Vector64);
	// Dc1Factory::DeleteObject(m_Vector256);
}

void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MotionActivityType_SpatialLocalizationParams_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMotionActivityType_SpatialLocalizationParams_LocalType
	const Dc1Ptr< Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Vector4);
		this->SetVector4(Dc1Factory::CloneObject(tmp->GetVector4()));
		// Dc1Factory::DeleteObject(m_Vector16);
		this->SetVector16(Dc1Factory::CloneObject(tmp->GetVector16()));
		// Dc1Factory::DeleteObject(m_Vector64);
		this->SetVector64(Dc1Factory::CloneObject(tmp->GetVector64()));
		// Dc1Factory::DeleteObject(m_Vector256);
		this->SetVector256(Dc1Factory::CloneObject(tmp->GetVector256()));
}

Dc1NodePtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetVector4() const
{
		return m_Vector4;
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetVector16() const
{
		return m_Vector16;
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetVector64() const
{
		return m_Vector64;
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetVector256() const
{
		return m_Vector256;
}

// implementing setter for choice 
/* element */
void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector4(const Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector4().");
	}
	if (m_Vector4 != item)
	{
		// Dc1Factory::DeleteObject(m_Vector4);
		m_Vector4 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Vector4) m_Vector4->SetParent(m_myself.getPointer());
		if (m_Vector4 && m_Vector4->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Vector4->UseTypeAttribute = true;
		}
		if(m_Vector4 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr())
		{
			m_Vector4->SetContentName(XMLString::transcode("Vector4"));
		}
	// Dc1Factory::DeleteObject(m_Vector16);
	m_Vector16 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector64);
	m_Vector64 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector256);
	m_Vector256 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector16(const Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector16().");
	}
	if (m_Vector16 != item)
	{
		// Dc1Factory::DeleteObject(m_Vector16);
		m_Vector16 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Vector16) m_Vector16->SetParent(m_myself.getPointer());
		if (m_Vector16 && m_Vector16->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector16_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Vector16->UseTypeAttribute = true;
		}
		if(m_Vector16 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr())
		{
			m_Vector16->SetContentName(XMLString::transcode("Vector16"));
		}
	// Dc1Factory::DeleteObject(m_Vector4);
	m_Vector4 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector64);
	m_Vector64 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector256);
	m_Vector256 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector64(const Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector64().");
	}
	if (m_Vector64 != item)
	{
		// Dc1Factory::DeleteObject(m_Vector64);
		m_Vector64 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Vector64) m_Vector64->SetParent(m_myself.getPointer());
		if (m_Vector64 && m_Vector64->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector64_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Vector64->UseTypeAttribute = true;
		}
		if(m_Vector64 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr())
		{
			m_Vector64->SetContentName(XMLString::transcode("Vector64"));
		}
	// Dc1Factory::DeleteObject(m_Vector4);
	m_Vector4 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector16);
	m_Vector16 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector256);
	m_Vector256 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector256(const Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::SetVector256().");
	}
	if (m_Vector256 != item)
	{
		// Dc1Factory::DeleteObject(m_Vector256);
		m_Vector256 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Vector256) m_Vector256->SetParent(m_myself.getPointer());
		if (m_Vector256 && m_Vector256->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector256_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Vector256->UseTypeAttribute = true;
		}
		if(m_Vector256 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr())
		{
			m_Vector256->SetContentName(XMLString::transcode("Vector256"));
		}
	// Dc1Factory::DeleteObject(m_Vector4);
	m_Vector4 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector16);
	m_Vector16 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr();
	// Dc1Factory::DeleteObject(m_Vector64);
	m_Vector64 = Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Vector4")) == 0)
	{
		// Vector4 is simple element MotionActivityType_SpatialLocalizationParams_Vector4_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVector4()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVector4(p, client);
					if((p = GetVector4()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Vector16")) == 0)
	{
		// Vector16 is simple element MotionActivityType_SpatialLocalizationParams_Vector16_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVector16()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector16_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVector16(p, client);
					if((p = GetVector16()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Vector64")) == 0)
	{
		// Vector64 is simple element MotionActivityType_SpatialLocalizationParams_Vector64_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVector64()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector64_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVector64(p, client);
					if((p = GetVector64()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Vector256")) == 0)
	{
		// Vector256 is simple element MotionActivityType_SpatialLocalizationParams_Vector256_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVector256()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector256_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVector256(p, client);
					if((p = GetVector256()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MotionActivityType_SpatialLocalizationParams_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MotionActivityType_SpatialLocalizationParams_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MotionActivityType_SpatialLocalizationParams_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Vector4 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Vector4->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Vector4"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Vector4->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Vector16 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector16_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Vector16->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Vector16"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Vector16->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Vector64 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector64_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Vector64->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Vector64"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Vector64->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Vector256 != Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector256_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Vector256->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Vector256"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Vector256->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Vector4"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Vector4")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialLocalizationParams_Vector4_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVector4(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Vector16"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Vector16")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialLocalizationParams_Vector16_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVector16(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Vector64"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Vector64")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialLocalizationParams_Vector64_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVector64(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Vector256"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Vector256")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialLocalizationParams_Vector256_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVector256(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Vector4")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialLocalizationParams_Vector4_LocalType; // FTT, check this
	}
	this->SetVector4(child);
  }
  if (XMLString::compareString(elementname, X("Vector16")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialLocalizationParams_Vector16_LocalType; // FTT, check this
	}
	this->SetVector16(child);
  }
  if (XMLString::compareString(elementname, X("Vector64")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialLocalizationParams_Vector64_LocalType; // FTT, check this
	}
	this->SetVector64(child);
  }
  if (XMLString::compareString(elementname, X("Vector256")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialLocalizationParams_Vector256_LocalType; // FTT, check this
	}
	this->SetVector256(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVector4() != Dc1NodePtr())
		result.Insert(GetVector4());
	if (GetVector16() != Dc1NodePtr())
		result.Insert(GetVector16());
	if (GetVector64() != Dc1NodePtr())
		result.Insert(GetVector64());
	if (GetVector256() != Dc1NodePtr())
		result.Insert(GetVector256());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType_ExtMethodImpl.h


