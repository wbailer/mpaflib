
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtImplInclude.h


#include "Mp7JrsSegmentType.h"
#include "Mp7JrsRegionLocatorType.h"
#include "Mp7JrsSpatialMaskType.h"
#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsMediaRelTimePointType.h"
#include "Mp7JrsMediaRelIncrTimePointType.h"
#include "Mp7JrsStillRegionType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsStillRegionType_SpatialDecomposition_CollectionType.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsStillRegionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsStillRegionType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsGridLayoutType.h" // Choice collection element GridLayoutDescriptors
#include "Mp7JrsIlluminationInvariantColorType.h" // Choice collection element IlluminationInvariantColor
#include "Mp7JrsStillRegionSpatialDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SpatialDecomposition

#include <assert.h>
IMp7JrsStillRegionType::IMp7JrsStillRegionType()
{

// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtPropInit.h

}

IMp7JrsStillRegionType::~IMp7JrsStillRegionType()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtPropCleanup.h

}

Mp7JrsStillRegionType::Mp7JrsStillRegionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStillRegionType::~Mp7JrsStillRegionType()
{
	Cleanup();
}

void Mp7JrsStillRegionType::Init()
{
	// Init base
	m_Base = CreateSegmentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SpatialLocator = Mp7JrsRegionLocatorPtr(); // Class
	m_SpatialMask = Mp7JrsSpatialMaskPtr(); // Class
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr(); // Pattern
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr(); // Class
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr(); // Class with content 
	m_StillRegionType_LocalType = Mp7JrsStillRegionType_CollectionPtr(); // Collection
	m_MultipleView = Mp7JrsMultipleViewPtr(); // Class
	m_MultipleView_Exist = false;
	m_SpatialDecomposition = Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtMyPropInit.h

}

void Mp7JrsStillRegionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SpatialLocator);
	// Dc1Factory::DeleteObject(m_SpatialMask);
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	// Dc1Factory::DeleteObject(m_StillRegionType_LocalType);
	// Dc1Factory::DeleteObject(m_MultipleView);
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
}

void Mp7JrsStillRegionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StillRegionTypePtr, since we
	// might need GetBase(), which isn't defined in IStillRegionType
	const Dc1Ptr< Mp7JrsStillRegionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSegmentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStillRegionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SpatialLocator);
		this->SetSpatialLocator(Dc1Factory::CloneObject(tmp->GetSpatialLocator()));
		// Dc1Factory::DeleteObject(m_SpatialMask);
		this->SetSpatialMask(Dc1Factory::CloneObject(tmp->GetSpatialMask()));
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		this->SetMediaTimePoint(Dc1Factory::CloneObject(tmp->GetMediaTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		this->SetMediaRelTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		this->SetMediaRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelIncrTimePoint()));
		// Dc1Factory::DeleteObject(m_StillRegionType_LocalType);
		this->SetStillRegionType_LocalType(Dc1Factory::CloneObject(tmp->GetStillRegionType_LocalType()));
	if (tmp->IsValidMultipleView())
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		this->SetMultipleView(Dc1Factory::CloneObject(tmp->GetMultipleView()));
	}
	else
	{
		InvalidateMultipleView();
	}
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		this->SetSpatialDecomposition(Dc1Factory::CloneObject(tmp->GetSpatialDecomposition()));
}

Dc1NodePtr Mp7JrsStillRegionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsStillRegionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSegmentType > Mp7JrsStillRegionType::GetBase() const
{
	return m_Base;
}

Mp7JrsRegionLocatorPtr Mp7JrsStillRegionType::GetSpatialLocator() const
{
		return m_SpatialLocator;
}

Mp7JrsSpatialMaskPtr Mp7JrsStillRegionType::GetSpatialMask() const
{
		return m_SpatialMask;
}

Mp7JrsmediaTimePointPtr Mp7JrsStillRegionType::GetMediaTimePoint() const
{
		return m_MediaTimePoint;
}

Mp7JrsMediaRelTimePointPtr Mp7JrsStillRegionType::GetMediaRelTimePoint() const
{
		return m_MediaRelTimePoint;
}

Mp7JrsMediaRelIncrTimePointPtr Mp7JrsStillRegionType::GetMediaRelIncrTimePoint() const
{
		return m_MediaRelIncrTimePoint;
}

Mp7JrsStillRegionType_CollectionPtr Mp7JrsStillRegionType::GetStillRegionType_LocalType() const
{
		return m_StillRegionType_LocalType;
}

Mp7JrsMultipleViewPtr Mp7JrsStillRegionType::GetMultipleView() const
{
		return m_MultipleView;
}

// Element is optional
bool Mp7JrsStillRegionType::IsValidMultipleView() const
{
	return m_MultipleView_Exist;
}

Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr Mp7JrsStillRegionType::GetSpatialDecomposition() const
{
		return m_SpatialDecomposition;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType::SetSpatialLocator(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetSpatialLocator().");
	}
	if (m_SpatialLocator != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialLocator);
		m_SpatialLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialLocator) m_SpatialLocator->SetParent(m_myself.getPointer());
		if (m_SpatialLocator && m_SpatialLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialLocator->UseTypeAttribute = true;
		}
		if(m_SpatialLocator != Mp7JrsRegionLocatorPtr())
		{
			m_SpatialLocator->SetContentName(XMLString::transcode("SpatialLocator"));
		}
	// Dc1Factory::DeleteObject(m_SpatialMask);
	m_SpatialMask = Mp7JrsSpatialMaskPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetSpatialMask(const Mp7JrsSpatialMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetSpatialMask().");
	}
	if (m_SpatialMask != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialMask);
		m_SpatialMask = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialMask) m_SpatialMask->SetParent(m_myself.getPointer());
		if (m_SpatialMask && m_SpatialMask->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatialMaskType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialMask->UseTypeAttribute = true;
		}
		if(m_SpatialMask != Mp7JrsSpatialMaskPtr())
		{
			m_SpatialMask->SetContentName(XMLString::transcode("SpatialMask"));
		}
	// Dc1Factory::DeleteObject(m_SpatialLocator);
	m_SpatialLocator = Mp7JrsRegionLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType::SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaTimePoint().");
	}
	if (m_MediaTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		m_MediaTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimePoint) m_MediaTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaTimePoint && m_MediaTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
		{
			m_MediaTimePoint->SetContentName(XMLString::transcode("MediaTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaRelTimePoint().");
	}
	if (m_MediaRelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		m_MediaRelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelTimePoint) m_MediaRelTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelTimePoint && m_MediaRelTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
		{
			m_MediaRelTimePoint->SetContentName(XMLString::transcode("MediaRelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaRelIncrTimePoint().");
	}
	if (m_MediaRelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		m_MediaRelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelIncrTimePoint) m_MediaRelIncrTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelIncrTimePoint && m_MediaRelIncrTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelIncrTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
		{
			m_MediaRelIncrTimePoint->SetContentName(XMLString::transcode("MediaRelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetStillRegionType_LocalType(const Mp7JrsStillRegionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetStillRegionType_LocalType().");
	}
	if (m_StillRegionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegionType_LocalType);
		m_StillRegionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegionType_LocalType) m_StillRegionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMultipleView().");
	}
	if (m_MultipleView != item || m_MultipleView_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		m_MultipleView = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MultipleView) m_MultipleView->SetParent(m_myself.getPointer());
		if (m_MultipleView && m_MultipleView->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MultipleView->UseTypeAttribute = true;
		}
		m_MultipleView_Exist = true;
		if(m_MultipleView != Mp7JrsMultipleViewPtr())
		{
			m_MultipleView->SetContentName(XMLString::transcode("MultipleView"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidateMultipleView()
{
	m_MultipleView_Exist = false;
}
void Mp7JrsStillRegionType::SetSpatialDecomposition(const Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetSpatialDecomposition().");
	}
	if (m_SpatialDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		m_SpatialDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialDecomposition) m_SpatialDecomposition->SetParent(m_myself.getPointer());
		if(m_SpatialDecomposition != Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr())
		{
			m_SpatialDecomposition->SetContentName(XMLString::transcode("SpatialDecomposition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsStillRegionType::GetMediaInformation() const
{
	return GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegionType::GetMediaInformationRef() const
{
	return GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsStillRegionType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsStillRegionType::GetStructuralUnit() const
{
	return GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsStillRegionType::IsValidStructuralUnit() const
{
	return GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsStillRegionType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegionType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsStillRegionType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegionType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsStillRegionType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsStillRegionType::GetSegmentType_LocalType() const
{
	return GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsStillRegionType::GetMatchingHint() const
{
	return GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsStillRegionType::GetPointOfView() const
{
	return GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsStillRegionType::GetRelation() const
{
	return GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaInformation().");
	}
	GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaInformationRef().");
	}
	GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetStructuralUnit().");
	}
	GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidateStructuralUnit()
{
	GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetSegmentType_LocalType().");
	}
	GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetMatchingHint().");
	}
	GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetPointOfView().");
	}
	GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsStillRegionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsStillRegionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsStillRegionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsStillRegionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsStillRegionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsStillRegionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsStillRegionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsStillRegionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsStillRegionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsStillRegionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsStillRegionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsStillRegionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsStillRegionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsStillRegionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsStillRegionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStillRegionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SpatialLocator")) == 0)
	{
		// SpatialLocator is simple element RegionLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatialLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatialLocator(p, client);
					if((p = GetSpatialLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialMask")) == 0)
	{
		// SpatialMask is simple element SpatialMaskType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatialMask()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatialMaskType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatialMaskPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatialMask(p, client);
					if((p = GetSpatialMask()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTimePoint")) == 0)
	{
		// MediaTimePoint is simple element mediaTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimePoint(p, client);
					if((p = GetMediaTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelTimePoint")) == 0)
	{
		// MediaRelTimePoint is simple element MediaRelTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelTimePoint(p, client);
					if((p = GetMediaRelTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelIncrTimePoint")) == 0)
	{
		// MediaRelIncrTimePoint is simple element MediaRelIncrTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaRelIncrTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaRelIncrTimePoint(p, client);
					if((p = GetMediaRelIncrTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptor")) == 0)
	{
		// VisualDescriptor is contained in itemtype StillRegionType_LocalType
		// in choice collection StillRegionType_CollectionType

		context->Found = true;
		Mp7JrsStillRegionType_CollectionPtr coll = GetStillRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegionType_CollectionType; // FTT, check this
				SetStillRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegionType_LocalPtr item = CreateStillRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->SetVisualDescriptor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptionScheme")) == 0)
	{
		// VisualDescriptionScheme is contained in itemtype StillRegionType_LocalType
		// in choice collection StillRegionType_CollectionType

		context->Found = true;
		Mp7JrsStillRegionType_CollectionPtr coll = GetStillRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegionType_CollectionType; // FTT, check this
				SetStillRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegionType_LocalPtr item = CreateStillRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->SetVisualDescriptionScheme(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GridLayoutDescriptors")) == 0)
	{
		// GridLayoutDescriptors is contained in itemtype StillRegionType_LocalType
		// in choice collection StillRegionType_CollectionType

		context->Found = true;
		Mp7JrsStillRegionType_CollectionPtr coll = GetStillRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegionType_CollectionType; // FTT, check this
				SetStillRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetGridLayoutDescriptors()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegionType_LocalPtr item = CreateStillRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GridLayoutType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGridLayoutPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->SetGridLayoutDescriptors(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetGridLayoutDescriptors()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IlluminationInvariantColor")) == 0)
	{
		// IlluminationInvariantColor is contained in itemtype StillRegionType_LocalType
		// in choice collection StillRegionType_CollectionType

		context->Found = true;
		Mp7JrsStillRegionType_CollectionPtr coll = GetStillRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegionType_CollectionType; // FTT, check this
				SetStillRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetIlluminationInvariantColor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegionType_LocalPtr item = CreateStillRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIlluminationInvariantColorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->SetIlluminationInvariantColor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegionType_LocalPtr)coll->elementAt(i))->GetIlluminationInvariantColor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MultipleView")) == 0)
	{
		// MultipleView is simple element MultipleViewType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMultipleView()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMultipleViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMultipleView(p, client);
					if((p = GetMultipleView()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialDecomposition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SpatialDecomposition is item of type StillRegionSpatialDecompositionType
		// in element collection StillRegionType_SpatialDecomposition_CollectionType
		
		context->Found = true;
		Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr coll = GetSpatialDecomposition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegionType_SpatialDecomposition_CollectionType; // FTT, check this
				SetSpatialDecomposition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionSpatialDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStillRegionSpatialDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StillRegionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StillRegionType");
	}
	return result;
}

XMLCh * Mp7JrsStillRegionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStillRegionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStillRegionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StillRegionType"));
	// Element serialization:
	// Class
	
	if (m_SpatialLocator != Mp7JrsRegionLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatialMask != Mp7JrsSpatialMaskPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialMask->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialMask"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialMask->Serialize(doc, element, &elem);
	}
	if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MediaTimePoint");
//		m_MediaTimePoint->SetContentName(contentname);
		m_MediaTimePoint->UseTypeAttribute = this->UseTypeAttribute;
		m_MediaTimePoint->Serialize(doc, element);
	}
	// Class
	
	if (m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelTimePoint->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelIncrTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelIncrTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelIncrTimePoint->Serialize(doc, element, &elem);
	}
	if (m_StillRegionType_LocalType != Mp7JrsStillRegionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StillRegionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MultipleView != Mp7JrsMultipleViewPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MultipleView->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MultipleView"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MultipleView->Serialize(doc, element, &elem);
	}
	if (m_SpatialDecomposition != Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpatialDecomposition->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsStillRegionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSegmentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialMask"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialMask")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatialMaskType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialMask(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimePoint")) == 0))
		{
			Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelIncrTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelIncrTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelIncrTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptor
			Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptor")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme
			Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:GridLayoutDescriptors
			Dc1Util::HasNodeName(parent, X("GridLayoutDescriptors"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GridLayoutDescriptors")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColor
			Dc1Util::HasNodeName(parent, X("IlluminationInvariantColor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColor")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStillRegionType_CollectionPtr tmp = CreateStillRegionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStillRegionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MultipleView"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MultipleView")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMultipleViewType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMultipleView(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SpatialDecomposition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr tmp = CreateStillRegionType_SpatialDecomposition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpatialDecomposition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStillRegionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("SpatialLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionLocatorType; // FTT, check this
	}
	this->SetSpatialLocator(child);
  }
  if (XMLString::compareString(elementname, X("SpatialMask")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatialMaskType; // FTT, check this
	}
	this->SetSpatialMask(child);
  }
  if (XMLString::compareString(elementname, X("MediaTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaTimePointType; // FTT, check this
	}
	this->SetMediaTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelTimePointType; // FTT, check this
	}
	this->SetMediaRelTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelIncrTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelIncrTimePointType; // FTT, check this
	}
	this->SetMediaRelIncrTimePoint(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("GridLayoutDescriptors"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("GridLayoutDescriptors")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("IlluminationInvariantColor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("IlluminationInvariantColor")) == 0)
	)
  {
	Mp7JrsStillRegionType_CollectionPtr tmp = CreateStillRegionType_CollectionType; // FTT, check this
	this->SetStillRegionType_LocalType(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MultipleView")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMultipleViewType; // FTT, check this
	}
	this->SetMultipleView(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SpatialDecomposition")) == 0))
  {
	Mp7JrsStillRegionType_SpatialDecomposition_CollectionPtr tmp = CreateStillRegionType_SpatialDecomposition_CollectionType; // FTT, check this
	this->SetSpatialDecomposition(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStillRegionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStillRegionType_LocalType() != Dc1NodePtr())
		result.Insert(GetStillRegionType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetSpatialDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatialDecomposition());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetSpatialLocator() != Dc1NodePtr())
		result.Insert(GetSpatialLocator());
	if (GetSpatialMask() != Dc1NodePtr())
		result.Insert(GetSpatialMask());
	if (GetMediaTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaTimePoint());
	if (GetMediaRelTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelTimePoint());
	if (GetMediaRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelIncrTimePoint());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStillRegionType_ExtMethodImpl.h


