
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsReferencePitchType_BaseFrequency_LocalType.h"
#include "Mp7JrsReferencePitchType_BasePitchNumber_LocalType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsReferencePitchType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsReferencePitchType::IMp7JrsReferencePitchType()
{

// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtPropInit.h

}

IMp7JrsReferencePitchType::~IMp7JrsReferencePitchType()
{
// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtPropCleanup.h

}

Mp7JrsReferencePitchType::Mp7JrsReferencePitchType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsReferencePitchType::~Mp7JrsReferencePitchType()
{
	Cleanup();
}

void Mp7JrsReferencePitchType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_BaseFrequency = Mp7JrsReferencePitchType_BaseFrequency_LocalPtr(); // Class with content 
	m_BaseFrequency_Exist = false;
	m_BasePitchNumber = Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr(); // Class with content 
	m_BasePitchNumber_Exist = false;


// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtMyPropInit.h

}

void Mp7JrsReferencePitchType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_BaseFrequency);
	// Dc1Factory::DeleteObject(m_BasePitchNumber);
}

void Mp7JrsReferencePitchType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ReferencePitchTypePtr, since we
	// might need GetBase(), which isn't defined in IReferencePitchType
	const Dc1Ptr< Mp7JrsReferencePitchType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsReferencePitchType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidBaseFrequency())
	{
		// Dc1Factory::DeleteObject(m_BaseFrequency);
		this->SetBaseFrequency(Dc1Factory::CloneObject(tmp->GetBaseFrequency()));
	}
	else
	{
		InvalidateBaseFrequency();
	}
	if (tmp->IsValidBasePitchNumber())
	{
		// Dc1Factory::DeleteObject(m_BasePitchNumber);
		this->SetBasePitchNumber(Dc1Factory::CloneObject(tmp->GetBasePitchNumber()));
	}
	else
	{
		InvalidateBasePitchNumber();
	}
}

Dc1NodePtr Mp7JrsReferencePitchType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsReferencePitchType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsReferencePitchType::GetBase() const
{
	return m_Base;
}

Mp7JrsReferencePitchType_BaseFrequency_LocalPtr Mp7JrsReferencePitchType::GetBaseFrequency() const
{
		return m_BaseFrequency;
}

// Element is optional
bool Mp7JrsReferencePitchType::IsValidBaseFrequency() const
{
	return m_BaseFrequency_Exist;
}

Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr Mp7JrsReferencePitchType::GetBasePitchNumber() const
{
		return m_BasePitchNumber;
}

// Element is optional
bool Mp7JrsReferencePitchType::IsValidBasePitchNumber() const
{
	return m_BasePitchNumber_Exist;
}

void Mp7JrsReferencePitchType::SetBaseFrequency(const Mp7JrsReferencePitchType_BaseFrequency_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferencePitchType::SetBaseFrequency().");
	}
	if (m_BaseFrequency != item || m_BaseFrequency_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BaseFrequency);
		m_BaseFrequency = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BaseFrequency) m_BaseFrequency->SetParent(m_myself.getPointer());
		if (m_BaseFrequency && m_BaseFrequency->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BaseFrequency_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BaseFrequency->UseTypeAttribute = true;
		}
		m_BaseFrequency_Exist = true;
		if(m_BaseFrequency != Mp7JrsReferencePitchType_BaseFrequency_LocalPtr())
		{
			m_BaseFrequency->SetContentName(XMLString::transcode("BaseFrequency"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferencePitchType::InvalidateBaseFrequency()
{
	m_BaseFrequency_Exist = false;
}
void Mp7JrsReferencePitchType::SetBasePitchNumber(const Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferencePitchType::SetBasePitchNumber().");
	}
	if (m_BasePitchNumber != item || m_BasePitchNumber_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BasePitchNumber);
		m_BasePitchNumber = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BasePitchNumber) m_BasePitchNumber->SetParent(m_myself.getPointer());
		if (m_BasePitchNumber && m_BasePitchNumber->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BasePitchNumber_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BasePitchNumber->UseTypeAttribute = true;
		}
		m_BasePitchNumber_Exist = true;
		if(m_BasePitchNumber != Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr())
		{
			m_BasePitchNumber->SetContentName(XMLString::transcode("BasePitchNumber"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferencePitchType::InvalidateBasePitchNumber()
{
	m_BasePitchNumber_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsReferencePitchType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsReferencePitchType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsReferencePitchType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferencePitchType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferencePitchType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsReferencePitchType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("BaseFrequency")) == 0)
	{
		// BaseFrequency is simple element ReferencePitchType_BaseFrequency_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBaseFrequency()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BaseFrequency_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePitchType_BaseFrequency_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBaseFrequency(p, client);
					if((p = GetBaseFrequency()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BasePitchNumber")) == 0)
	{
		// BasePitchNumber is simple element ReferencePitchType_BasePitchNumber_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBasePitchNumber()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferencePitchType_BasePitchNumber_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBasePitchNumber(p, client);
					if((p = GetBasePitchNumber()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ReferencePitchType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ReferencePitchType");
	}
	return result;
}

XMLCh * Mp7JrsReferencePitchType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsReferencePitchType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsReferencePitchType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ReferencePitchType"));
	// Element serialization:
	// Class with content		
	
	if (m_BaseFrequency != Mp7JrsReferencePitchType_BaseFrequency_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BaseFrequency->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BaseFrequency"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BaseFrequency->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_BasePitchNumber != Mp7JrsReferencePitchType_BasePitchNumber_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BasePitchNumber->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BasePitchNumber"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BasePitchNumber->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsReferencePitchType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BaseFrequency"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BaseFrequency")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferencePitchType_BaseFrequency_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBaseFrequency(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BasePitchNumber"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BasePitchNumber")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferencePitchType_BasePitchNumber_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBasePitchNumber(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsReferencePitchType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("BaseFrequency")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferencePitchType_BaseFrequency_LocalType; // FTT, check this
	}
	this->SetBaseFrequency(child);
  }
  if (XMLString::compareString(elementname, X("BasePitchNumber")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferencePitchType_BasePitchNumber_LocalType; // FTT, check this
	}
	this->SetBasePitchNumber(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsReferencePitchType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetBaseFrequency() != Dc1NodePtr())
		result.Insert(GetBaseFrequency());
	if (GetBasePitchNumber() != Dc1NodePtr())
		result.Insert(GetBasePitchNumber());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsReferencePitchType_ExtMethodImpl.h


