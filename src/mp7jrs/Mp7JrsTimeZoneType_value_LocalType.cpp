
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtImplInclude.h


#include "Mp7JrsTimeZoneType_value_LocalType.h"

Mp7JrsTimeZoneType_value_LocalType::Mp7JrsTimeZoneType_value_LocalType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrsTimeZoneType_value_LocalType::~Mp7JrsTimeZoneType_value_LocalType()
{
		Cleanup();
}

void Mp7JrsTimeZoneType_value_LocalType::Init()
{	   
		m_TimezoneSign = 1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_Timezone_Exist = false;

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtPropInit.h

}

void Mp7JrsTimeZoneType_value_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtPropCleanup.h


}

void Mp7JrsTimeZoneType_value_LocalType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrsTimeZoneType_value_LocalType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrsTimeZoneType_value_LocalType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTimeZoneType_value_LocalType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrsTimeZoneType_value_LocalType::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void Mp7JrsTimeZoneType_value_LocalType::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeZoneType_value_LocalType::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsTimeZoneType_value_LocalType::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void Mp7JrsTimeZoneType_value_LocalType::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeZoneType_value_LocalType::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsTimeZoneType_value_LocalType::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void Mp7JrsTimeZoneType_value_LocalType::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeZoneType_value_LocalType::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsTimeZoneType_value_LocalType::GetTimezone_Exist() const
{
		return m_Timezone_Exist;
}

void Mp7JrsTimeZoneType_value_LocalType::SetTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeZoneType_value_LocalType::SetTimezone_Exist().");
		}
		m_Timezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrsTimeZoneType_value_LocalType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		if(m_Timezone_Exist) // Z
		{
			Dc1Util::CatString(&buf, X("Z"));
		}
		else // Offset
		{
			if(m_TimezoneSign > 0) // Check if '0' is an option
			{
				Dc1Util::CatString(&buf, X("+"));
			}
			else if(m_TimezoneSign < 0)
			{
				Dc1Util::CatString(&buf, X("-"));
			}

			XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
			XMLCh* tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X(":"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrsTimeZoneType_value_LocalType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^(Z|((\\-|\\+)\\d{2}:\\d{2}))?$");

		// This is for Xerces 3.1.1+
		if (r.matches(txt, &m))
		{
			for (int i = 0; i < m.getNoGroups(); i++)
			{
				int i1 = m.getStartPos(i);
				int i2 = Dc1Util::GetNextEndPos(m, i);
				if (i1 == i2) continue; // ignore missing optional or wrong group

				switch (i)
				{
				case 0: // Match
					break;
				case 1: // Z
					if (i1 >= 0 && i2 - i1 == 1)
					{
						m_Timezone_Exist = true;
					}
					break;
				case 2: // (-|+)hh:mm
					if (i1 >= 0 && i2 - i1 > 1)
					{
						// -|+
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 1);
						m_TimezoneSign = Dc1Convert::TextToSign(substring);
						// hh
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i1 + 3);
						m_TimezoneHours = Dc1Convert::TextToUnsignedInt(substring);
						// mm
						XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 4, i1 + 6);
						m_TimezoneMinutes = Dc1Convert::TextToUnsignedInt(substring);
					}
					break;
				default: return true;
					break;
				}
			}
		}
		else return false;
		return true;
}

bool Mp7JrsTimeZoneType_value_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsTimeZoneType_value_LocalType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsTimeZoneType_value_LocalType::ContentToString() const
{
	// Mp7JrsTimeZoneType_value_LocalType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsTimeZoneType_value_LocalType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeZoneType_value_LocalType")))));
		}

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtPreImplementCleanup.h


		if(this->GetContentName() != NULL)
		{
				element->appendChild(doc->createTextNode(this->GetContentName())); 
		}
}

bool Mp7JrsTimeZoneType_value_LocalType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// no includefile for extension defined 
// file Mp7JrsTimeZoneType_value_LocalType_ExtMethodImpl.h








