
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtImplInclude.h


#include "Mp7JrsGaussianDistributionType.h"
#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7JrsProbabilityDistributionType_Moment_CollectionType.h"
#include "Mp7JrsProbabilityDistributionType_Cumulant_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsGeneralizedGaussianDistributionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsProbabilityDistributionType_Moment_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Moment
#include "Mp7JrsProbabilityDistributionType_Cumulant_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Cumulant

#include <assert.h>
IMp7JrsGeneralizedGaussianDistributionType::IMp7JrsGeneralizedGaussianDistributionType()
{

// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtPropInit.h

}

IMp7JrsGeneralizedGaussianDistributionType::~IMp7JrsGeneralizedGaussianDistributionType()
{
// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtPropCleanup.h

}

Mp7JrsGeneralizedGaussianDistributionType::Mp7JrsGeneralizedGaussianDistributionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGeneralizedGaussianDistributionType::~Mp7JrsGeneralizedGaussianDistributionType()
{
	Cleanup();
}

void Mp7JrsGeneralizedGaussianDistributionType::Init()
{
	// Init base
	m_Base = CreateGaussianDistributionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Shape = Mp7JrsDoubleMatrixPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtMyPropInit.h

}

void Mp7JrsGeneralizedGaussianDistributionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Shape);
}

void Mp7JrsGeneralizedGaussianDistributionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GeneralizedGaussianDistributionTypePtr, since we
	// might need GetBase(), which isn't defined in IGeneralizedGaussianDistributionType
	const Dc1Ptr< Mp7JrsGeneralizedGaussianDistributionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsGaussianDistributionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsGeneralizedGaussianDistributionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Shape);
		this->SetShape(Dc1Factory::CloneObject(tmp->GetShape()));
}

Dc1NodePtr Mp7JrsGeneralizedGaussianDistributionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsGeneralizedGaussianDistributionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsGaussianDistributionType > Mp7JrsGeneralizedGaussianDistributionType::GetBase() const
{
	return m_Base;
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetShape() const
{
		return m_Shape;
}

void Mp7JrsGeneralizedGaussianDistributionType::SetShape(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetShape().");
	}
	if (m_Shape != item)
	{
		// Dc1Factory::DeleteObject(m_Shape);
		m_Shape = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Shape) m_Shape->SetParent(m_myself.getPointer());
		if (m_Shape && m_Shape->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Shape->UseTypeAttribute = true;
		}
		if(m_Shape != Mp7JrsDoubleMatrixPtr())
		{
			m_Shape->SetContentName(XMLString::transcode("Shape"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

double Mp7JrsGeneralizedGaussianDistributionType::GetDeterminant() const
{
	return GetBase()->GetDeterminant();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidDeterminant() const
{
	return GetBase()->IsValidDeterminant();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetCovarianceInverse() const
{
	return GetBase()->GetCovarianceInverse();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidCovarianceInverse() const
{
	return GetBase()->IsValidCovarianceInverse();
}

void Mp7JrsGeneralizedGaussianDistributionType::SetDeterminant(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetDeterminant().");
	}
	GetBase()->SetDeterminant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateDeterminant()
{
	GetBase()->InvalidateDeterminant();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetCovarianceInverse(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetCovarianceInverse().");
	}
	GetBase()->SetCovarianceInverse(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateCovarianceInverse()
{
	GetBase()->InvalidateCovarianceInverse();
}
unsigned Mp7JrsGeneralizedGaussianDistributionType::Getdim() const
{
	return GetBase()->GetBase()->GetBase()->Getdim();
}

bool Mp7JrsGeneralizedGaussianDistributionType::Existdim() const
{
	return GetBase()->GetBase()->GetBase()->Existdim();
}
void Mp7JrsGeneralizedGaussianDistributionType::Setdim(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::Setdim().");
	}
	GetBase()->GetBase()->GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::Invalidatedim()
{
	GetBase()->GetBase()->GetBase()->Invalidatedim();
}
Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetMean() const
{
	return GetBase()->GetBase()->GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidMean() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMean();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetVariance() const
{
	return GetBase()->GetBase()->GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidVariance() const
{
	return GetBase()->GetBase()->GetBase()->IsValidVariance();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetMin() const
{
	return GetBase()->GetBase()->GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidMin() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMin();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetMax() const
{
	return GetBase()->GetBase()->GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidMax() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMax();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetMode() const
{
	return GetBase()->GetBase()->GetBase()->GetMode();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidMode() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMode();
}

Mp7JrsDoubleMatrixPtr Mp7JrsGeneralizedGaussianDistributionType::GetMedian() const
{
	return GetBase()->GetBase()->GetBase()->GetMedian();
}

// Element is optional
bool Mp7JrsGeneralizedGaussianDistributionType::IsValidMedian() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMedian();
}

Mp7JrsProbabilityDistributionType_Moment_CollectionPtr Mp7JrsGeneralizedGaussianDistributionType::GetMoment() const
{
	return GetBase()->GetBase()->GetBase()->GetMoment();
}

Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr Mp7JrsGeneralizedGaussianDistributionType::GetCumulant() const
{
	return GetBase()->GetBase()->GetBase()->GetCumulant();
}

void Mp7JrsGeneralizedGaussianDistributionType::SetMean(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMean().");
	}
	GetBase()->GetBase()->GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateMean()
{
	GetBase()->GetBase()->GetBase()->InvalidateMean();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetVariance(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetVariance().");
	}
	GetBase()->GetBase()->GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateVariance()
{
	GetBase()->GetBase()->GetBase()->InvalidateVariance();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetMin(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMin().");
	}
	GetBase()->GetBase()->GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateMin()
{
	GetBase()->GetBase()->GetBase()->InvalidateMin();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetMax(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMax().");
	}
	GetBase()->GetBase()->GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateMax()
{
	GetBase()->GetBase()->GetBase()->InvalidateMax();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetMode(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMode().");
	}
	GetBase()->GetBase()->GetBase()->SetMode(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateMode()
{
	GetBase()->GetBase()->GetBase()->InvalidateMode();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetMedian(const Mp7JrsDoubleMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMedian().");
	}
	GetBase()->GetBase()->GetBase()->SetMedian(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidateMedian()
{
	GetBase()->GetBase()->GetBase()->InvalidateMedian();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetMoment(const Mp7JrsProbabilityDistributionType_Moment_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetMoment().");
	}
	GetBase()->GetBase()->GetBase()->SetMoment(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::SetCumulant(const Mp7JrsProbabilityDistributionType_Cumulant_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetCumulant().");
	}
	GetBase()->GetBase()->GetBase()->SetCumulant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsGeneralizedGaussianDistributionType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsGeneralizedGaussianDistributionType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsGeneralizedGaussianDistributionType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsGeneralizedGaussianDistributionType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsGeneralizedGaussianDistributionType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsGeneralizedGaussianDistributionType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsGeneralizedGaussianDistributionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsGeneralizedGaussianDistributionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsGeneralizedGaussianDistributionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsGeneralizedGaussianDistributionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsGeneralizedGaussianDistributionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsGeneralizedGaussianDistributionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsGeneralizedGaussianDistributionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsGeneralizedGaussianDistributionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsGeneralizedGaussianDistributionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsGeneralizedGaussianDistributionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsGeneralizedGaussianDistributionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsGeneralizedGaussianDistributionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsGeneralizedGaussianDistributionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsGeneralizedGaussianDistributionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGeneralizedGaussianDistributionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsGeneralizedGaussianDistributionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsGeneralizedGaussianDistributionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGeneralizedGaussianDistributionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGeneralizedGaussianDistributionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Shape")) == 0)
	{
		// Shape is simple element DoubleMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShape()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DoubleMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDoubleMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShape(p, client);
					if((p = GetShape()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GeneralizedGaussianDistributionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GeneralizedGaussianDistributionType");
	}
	return result;
}

XMLCh * Mp7JrsGeneralizedGaussianDistributionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsGeneralizedGaussianDistributionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsGeneralizedGaussianDistributionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GeneralizedGaussianDistributionType"));
	// Element serialization:
	// Class
	
	if (m_Shape != Mp7JrsDoubleMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Shape->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Shape"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Shape->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsGeneralizedGaussianDistributionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsGaussianDistributionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Shape"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Shape")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDoubleMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShape(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGeneralizedGaussianDistributionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Shape")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDoubleMatrixType; // FTT, check this
	}
	this->SetShape(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGeneralizedGaussianDistributionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetShape() != Dc1NodePtr())
		result.Insert(GetShape());
	if (GetCovarianceInverse() != Dc1NodePtr())
		result.Insert(GetCovarianceInverse());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMode() != Dc1NodePtr())
		result.Insert(GetMode());
	if (GetMedian() != Dc1NodePtr())
		result.Insert(GetMedian());
	if (GetMoment() != Dc1NodePtr())
		result.Insert(GetMoment());
	if (GetCumulant() != Dc1NodePtr())
		result.Insert(GetCumulant());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGeneralizedGaussianDistributionType_ExtMethodImpl.h


