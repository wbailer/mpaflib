
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtImplInclude.h


#include "Mp7JrsSegmentType.h"
#include "Mp7JrsSpatioTemporalLocatorType.h"
#include "Mp7JrsSpatioTemporalMaskType.h"
#include "Mp7JrsMovingRegionType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsMovingRegionType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMovingRegionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsMovingRegionType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsVisualTimeSeriesType.h" // Choice collection element VisualTimeSeriesDescriptor
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsMovingRegionType_LocalType0.h" // Choice collection SpatialDecomposition
#include "Mp7JrsMovingRegionSpatialDecompositionType.h" // Choice collection element SpatialDecomposition
#include "Mp7JrsMovingRegionTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsMovingRegionSpatioTemporalDecompositionType.h" // Choice collection element SpatioTemporalDecomposition
#include "Mp7JrsMovingRegionMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition

#include <assert.h>
IMp7JrsMovingRegionType::IMp7JrsMovingRegionType()
{

// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtPropInit.h

}

IMp7JrsMovingRegionType::~IMp7JrsMovingRegionType()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtPropCleanup.h

}

Mp7JrsMovingRegionType::Mp7JrsMovingRegionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMovingRegionType::~Mp7JrsMovingRegionType()
{
	Cleanup();
}

void Mp7JrsMovingRegionType::Init()
{
	// Init base
	m_Base = CreateSegmentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SpatioTemporalLocator = Mp7JrsSpatioTemporalLocatorPtr(); // Class
	m_SpatioTemporalMask = Mp7JrsSpatioTemporalMaskPtr(); // Class
	m_MovingRegionType_LocalType = Mp7JrsMovingRegionType_CollectionPtr(); // Collection
	m_MultipleView = Mp7JrsMultipleViewPtr(); // Class
	m_MultipleView_Exist = false;
	m_MovingRegionType_LocalType0 = Mp7JrsMovingRegionType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtMyPropInit.h

}

void Mp7JrsMovingRegionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SpatioTemporalLocator);
	// Dc1Factory::DeleteObject(m_SpatioTemporalMask);
	// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType);
	// Dc1Factory::DeleteObject(m_MultipleView);
	// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType0);
}

void Mp7JrsMovingRegionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MovingRegionTypePtr, since we
	// might need GetBase(), which isn't defined in IMovingRegionType
	const Dc1Ptr< Mp7JrsMovingRegionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSegmentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMovingRegionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SpatioTemporalLocator);
		this->SetSpatioTemporalLocator(Dc1Factory::CloneObject(tmp->GetSpatioTemporalLocator()));
		// Dc1Factory::DeleteObject(m_SpatioTemporalMask);
		this->SetSpatioTemporalMask(Dc1Factory::CloneObject(tmp->GetSpatioTemporalMask()));
		// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType);
		this->SetMovingRegionType_LocalType(Dc1Factory::CloneObject(tmp->GetMovingRegionType_LocalType()));
	if (tmp->IsValidMultipleView())
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		this->SetMultipleView(Dc1Factory::CloneObject(tmp->GetMultipleView()));
	}
	else
	{
		InvalidateMultipleView();
	}
		// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType0);
		this->SetMovingRegionType_LocalType0(Dc1Factory::CloneObject(tmp->GetMovingRegionType_LocalType0()));
}

Dc1NodePtr Mp7JrsMovingRegionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMovingRegionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSegmentType > Mp7JrsMovingRegionType::GetBase() const
{
	return m_Base;
}

Mp7JrsSpatioTemporalLocatorPtr Mp7JrsMovingRegionType::GetSpatioTemporalLocator() const
{
		return m_SpatioTemporalLocator;
}

Mp7JrsSpatioTemporalMaskPtr Mp7JrsMovingRegionType::GetSpatioTemporalMask() const
{
		return m_SpatioTemporalMask;
}

Mp7JrsMovingRegionType_CollectionPtr Mp7JrsMovingRegionType::GetMovingRegionType_LocalType() const
{
		return m_MovingRegionType_LocalType;
}

Mp7JrsMultipleViewPtr Mp7JrsMovingRegionType::GetMultipleView() const
{
		return m_MultipleView;
}

// Element is optional
bool Mp7JrsMovingRegionType::IsValidMultipleView() const
{
	return m_MultipleView_Exist;
}

Mp7JrsMovingRegionType_Collection0Ptr Mp7JrsMovingRegionType::GetMovingRegionType_LocalType0() const
{
		return m_MovingRegionType_LocalType0;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionType::SetSpatioTemporalLocator(const Mp7JrsSpatioTemporalLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetSpatioTemporalLocator().");
	}
	if (m_SpatioTemporalLocator != item)
	{
		// Dc1Factory::DeleteObject(m_SpatioTemporalLocator);
		m_SpatioTemporalLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatioTemporalLocator) m_SpatioTemporalLocator->SetParent(m_myself.getPointer());
		if (m_SpatioTemporalLocator && m_SpatioTemporalLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatioTemporalLocator->UseTypeAttribute = true;
		}
		if(m_SpatioTemporalLocator != Mp7JrsSpatioTemporalLocatorPtr())
		{
			m_SpatioTemporalLocator->SetContentName(XMLString::transcode("SpatioTemporalLocator"));
		}
	// Dc1Factory::DeleteObject(m_SpatioTemporalMask);
	m_SpatioTemporalMask = Mp7JrsSpatioTemporalMaskPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType::SetSpatioTemporalMask(const Mp7JrsSpatioTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetSpatioTemporalMask().");
	}
	if (m_SpatioTemporalMask != item)
	{
		// Dc1Factory::DeleteObject(m_SpatioTemporalMask);
		m_SpatioTemporalMask = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatioTemporalMask) m_SpatioTemporalMask->SetParent(m_myself.getPointer());
		if (m_SpatioTemporalMask && m_SpatioTemporalMask->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalMaskType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatioTemporalMask->UseTypeAttribute = true;
		}
		if(m_SpatioTemporalMask != Mp7JrsSpatioTemporalMaskPtr())
		{
			m_SpatioTemporalMask->SetContentName(XMLString::transcode("SpatioTemporalMask"));
		}
	// Dc1Factory::DeleteObject(m_SpatioTemporalLocator);
	m_SpatioTemporalLocator = Mp7JrsSpatioTemporalLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetMovingRegionType_LocalType(const Mp7JrsMovingRegionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMovingRegionType_LocalType().");
	}
	if (m_MovingRegionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType);
		m_MovingRegionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MovingRegionType_LocalType) m_MovingRegionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMultipleView().");
	}
	if (m_MultipleView != item || m_MultipleView_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		m_MultipleView = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MultipleView) m_MultipleView->SetParent(m_myself.getPointer());
		if (m_MultipleView && m_MultipleView->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MultipleView->UseTypeAttribute = true;
		}
		m_MultipleView_Exist = true;
		if(m_MultipleView != Mp7JrsMultipleViewPtr())
		{
			m_MultipleView->SetContentName(XMLString::transcode("MultipleView"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidateMultipleView()
{
	m_MultipleView_Exist = false;
}
void Mp7JrsMovingRegionType::SetMovingRegionType_LocalType0(const Mp7JrsMovingRegionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMovingRegionType_LocalType0().");
	}
	if (m_MovingRegionType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_MovingRegionType_LocalType0);
		m_MovingRegionType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MovingRegionType_LocalType0) m_MovingRegionType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsMovingRegionType::GetMediaInformation() const
{
	return GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsMovingRegionType::GetMediaInformationRef() const
{
	return GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsMovingRegionType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsMovingRegionType::GetStructuralUnit() const
{
	return GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsMovingRegionType::IsValidStructuralUnit() const
{
	return GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsMovingRegionType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsMovingRegionType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsMovingRegionType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsMovingRegionType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsMovingRegionType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsMovingRegionType::GetSegmentType_LocalType() const
{
	return GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsMovingRegionType::GetMatchingHint() const
{
	return GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsMovingRegionType::GetPointOfView() const
{
	return GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsMovingRegionType::GetRelation() const
{
	return GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMediaInformation().");
	}
	GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMediaInformationRef().");
	}
	GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetStructuralUnit().");
	}
	GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidateStructuralUnit()
{
	GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetSegmentType_LocalType().");
	}
	GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetMatchingHint().");
	}
	GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetPointOfView().");
	}
	GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMovingRegionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsMovingRegionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsMovingRegionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMovingRegionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMovingRegionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMovingRegionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMovingRegionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMovingRegionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMovingRegionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMovingRegionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMovingRegionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMovingRegionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMovingRegionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMovingRegionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMovingRegionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMovingRegionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMovingRegionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMovingRegionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SpatioTemporalLocator")) == 0)
	{
		// SpatioTemporalLocator is simple element SpatioTemporalLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatioTemporalLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatioTemporalLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatioTemporalLocator(p, client);
					if((p = GetSpatioTemporalLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatioTemporalMask")) == 0)
	{
		// SpatioTemporalMask is simple element SpatioTemporalMaskType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatioTemporalMask()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalMaskType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatioTemporalMaskPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatioTemporalMask(p, client);
					if((p = GetSpatioTemporalMask()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptor")) == 0)
	{
		// VisualDescriptor is contained in itemtype MovingRegionType_LocalType
		// in choice collection MovingRegionType_CollectionType

		context->Found = true;
		Mp7JrsMovingRegionType_CollectionPtr coll = GetMovingRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType; // FTT, check this
				SetMovingRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_LocalPtr item = CreateMovingRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->SetVisualDescriptor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptionScheme")) == 0)
	{
		// VisualDescriptionScheme is contained in itemtype MovingRegionType_LocalType
		// in choice collection MovingRegionType_CollectionType

		context->Found = true;
		Mp7JrsMovingRegionType_CollectionPtr coll = GetMovingRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType; // FTT, check this
				SetMovingRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_LocalPtr item = CreateMovingRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->SetVisualDescriptionScheme(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualTimeSeriesDescriptor")) == 0)
	{
		// VisualTimeSeriesDescriptor is contained in itemtype MovingRegionType_LocalType
		// in choice collection MovingRegionType_CollectionType

		context->Found = true;
		Mp7JrsMovingRegionType_CollectionPtr coll = GetMovingRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType; // FTT, check this
				SetMovingRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualTimeSeriesDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_LocalPtr item = CreateMovingRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualTimeSeriesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->SetVisualTimeSeriesDescriptor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetVisualTimeSeriesDescriptor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GofGopFeature")) == 0)
	{
		// GofGopFeature is contained in itemtype MovingRegionType_LocalType
		// in choice collection MovingRegionType_CollectionType

		context->Found = true;
		Mp7JrsMovingRegionType_CollectionPtr coll = GetMovingRegionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType; // FTT, check this
				SetMovingRegionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetGofGopFeature()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_LocalPtr item = CreateMovingRegionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGofGopFeaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->SetGofGopFeature(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_LocalPtr)coll->elementAt(i))->GetGofGopFeature()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MultipleView")) == 0)
	{
		// MultipleView is simple element MultipleViewType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMultipleView()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMultipleViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMultipleView(p, client);
					if((p = GetMultipleView()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialDecomposition")) == 0)
	{
		// SpatialDecomposition is contained in itemtype MovingRegionType_LocalType0
		// in choice collection MovingRegionType_CollectionType0

		context->Found = true;
		Mp7JrsMovingRegionType_Collection0Ptr coll = GetMovingRegionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType0; // FTT, check this
				SetMovingRegionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetSpatialDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_Local0Ptr item = CreateMovingRegionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatialDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMovingRegionSpatialDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->SetSpatialDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetSpatialDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TemporalDecomposition")) == 0)
	{
		// TemporalDecomposition is contained in itemtype MovingRegionType_LocalType0
		// in choice collection MovingRegionType_CollectionType0

		context->Found = true;
		Mp7JrsMovingRegionType_Collection0Ptr coll = GetMovingRegionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType0; // FTT, check this
				SetMovingRegionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetTemporalDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_Local0Ptr item = CreateMovingRegionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionTemporalDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMovingRegionTemporalDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->SetTemporalDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetTemporalDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatioTemporalDecomposition")) == 0)
	{
		// SpatioTemporalDecomposition is contained in itemtype MovingRegionType_LocalType0
		// in choice collection MovingRegionType_CollectionType0

		context->Found = true;
		Mp7JrsMovingRegionType_Collection0Ptr coll = GetMovingRegionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType0; // FTT, check this
				SetMovingRegionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetSpatioTemporalDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_Local0Ptr item = CreateMovingRegionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionSpatioTemporalDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMovingRegionSpatioTemporalDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->SetSpatioTemporalDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetSpatioTemporalDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaSourceDecomposition")) == 0)
	{
		// MediaSourceDecomposition is contained in itemtype MovingRegionType_LocalType0
		// in choice collection MovingRegionType_CollectionType0

		context->Found = true;
		Mp7JrsMovingRegionType_Collection0Ptr coll = GetMovingRegionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMovingRegionType_CollectionType0; // FTT, check this
				SetMovingRegionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetMediaSourceDecomposition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMovingRegionType_Local0Ptr item = CreateMovingRegionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionMediaSourceDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMovingRegionMediaSourceDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->SetMediaSourceDecomposition(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMovingRegionType_Local0Ptr)coll->elementAt(i))->GetMediaSourceDecomposition()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MovingRegionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MovingRegionType");
	}
	return result;
}

XMLCh * Mp7JrsMovingRegionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMovingRegionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMovingRegionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MovingRegionType"));
	// Element serialization:
	// Class
	
	if (m_SpatioTemporalLocator != Mp7JrsSpatioTemporalLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatioTemporalLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatioTemporalLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatioTemporalLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatioTemporalMask != Mp7JrsSpatioTemporalMaskPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatioTemporalMask->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatioTemporalMask"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatioTemporalMask->Serialize(doc, element, &elem);
	}
	if (m_MovingRegionType_LocalType != Mp7JrsMovingRegionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MovingRegionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MultipleView != Mp7JrsMultipleViewPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MultipleView->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MultipleView"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MultipleView->Serialize(doc, element, &elem);
	}
	if (m_MovingRegionType_LocalType0 != Mp7JrsMovingRegionType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MovingRegionType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMovingRegionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSegmentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatioTemporalLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatioTemporalLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatioTemporalLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatioTemporalLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatioTemporalMask"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatioTemporalMask")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatioTemporalMaskType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatioTemporalMask(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptor
			Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptor")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme
			Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:VisualTimeSeriesDescriptor
			Dc1Util::HasNodeName(parent, X("VisualTimeSeriesDescriptor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualTimeSeriesDescriptor")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:GofGopFeature
			Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GofGopFeature")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMovingRegionType_CollectionPtr tmp = CreateMovingRegionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMovingRegionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MultipleView"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MultipleView")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMultipleViewType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMultipleView(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SpatialDecomposition
			Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpatialDecomposition")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:TemporalDecomposition
			Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TemporalDecomposition")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SpatioTemporalDecomposition
			Dc1Util::HasNodeName(parent, X("SpatioTemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalDecomposition")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:MediaSourceDecomposition
			Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MediaSourceDecomposition")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMovingRegionType_Collection0Ptr tmp = CreateMovingRegionType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMovingRegionType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMovingRegionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("SpatioTemporalLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatioTemporalLocatorType; // FTT, check this
	}
	this->SetSpatioTemporalLocator(child);
  }
  if (XMLString::compareString(elementname, X("SpatioTemporalMask")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatioTemporalMaskType; // FTT, check this
	}
	this->SetSpatioTemporalMask(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualTimeSeriesDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualTimeSeriesDescriptor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("GofGopFeature")) == 0)
	)
  {
	Mp7JrsMovingRegionType_CollectionPtr tmp = CreateMovingRegionType_CollectionType; // FTT, check this
	this->SetMovingRegionType_LocalType(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MultipleView")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMultipleViewType; // FTT, check this
	}
	this->SetMultipleView(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpatialDecomposition")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TemporalDecomposition")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpatioTemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpatioTemporalDecomposition")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("MediaSourceDecomposition")) == 0)
	)
  {
	Mp7JrsMovingRegionType_Collection0Ptr tmp = CreateMovingRegionType_CollectionType0; // FTT, check this
	this->SetMovingRegionType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMovingRegionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMovingRegionType_LocalType() != Dc1NodePtr())
		result.Insert(GetMovingRegionType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetMovingRegionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetMovingRegionType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetSpatioTemporalLocator() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalLocator());
	if (GetSpatioTemporalMask() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMovingRegionType_ExtMethodImpl.h


