
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtImplInclude.h


#include "Mp7JrsFragmentReferenceType.h"
#include "Mp7JrsURIFragmentReferenceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsURIFragmentReferenceType::IMp7JrsURIFragmentReferenceType()
{

// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtPropInit.h

}

IMp7JrsURIFragmentReferenceType::~IMp7JrsURIFragmentReferenceType()
{
// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtPropCleanup.h

}

Mp7JrsURIFragmentReferenceType::Mp7JrsURIFragmentReferenceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsURIFragmentReferenceType::~Mp7JrsURIFragmentReferenceType()
{
	Cleanup();
}

void Mp7JrsURIFragmentReferenceType::Init()
{
	// Init base
	m_Base = CreateFragmentReferenceType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_href = NULL; // String



// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtMyPropInit.h

}

void Mp7JrsURIFragmentReferenceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_href); // String
}

void Mp7JrsURIFragmentReferenceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use URIFragmentReferenceTypePtr, since we
	// might need GetBase(), which isn't defined in IURIFragmentReferenceType
	const Dc1Ptr< Mp7JrsURIFragmentReferenceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsFragmentReferencePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsURIFragmentReferenceType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_href); // String
		this->Sethref(XMLString::replicate(tmp->Gethref()));
	}
}

Dc1NodePtr Mp7JrsURIFragmentReferenceType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsURIFragmentReferenceType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsFragmentReferenceType > Mp7JrsURIFragmentReferenceType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsURIFragmentReferenceType::Gethref() const
{
	return m_href;
}

void Mp7JrsURIFragmentReferenceType::Sethref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsURIFragmentReferenceType::Sethref().");
	}
	m_href = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsURIFragmentReferenceType::GetisDeferred() const
{
	return GetBase()->GetisDeferred();
}

bool Mp7JrsURIFragmentReferenceType::ExistisDeferred() const
{
	return GetBase()->ExistisDeferred();
}
void Mp7JrsURIFragmentReferenceType::SetisDeferred(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsURIFragmentReferenceType::SetisDeferred().");
	}
	GetBase()->SetisDeferred(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsURIFragmentReferenceType::InvalidateisDeferred()
{
	GetBase()->InvalidateisDeferred();
}

Dc1NodeEnum Mp7JrsURIFragmentReferenceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("href")) == 0)
	{
		// href is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for URIFragmentReferenceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "URIFragmentReferenceType");
	}
	return result;
}

XMLCh * Mp7JrsURIFragmentReferenceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsURIFragmentReferenceType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsURIFragmentReferenceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("URIFragmentReferenceType"));
	// Attribute Serialization:
	// String
	if(m_href != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("href"), m_href);
	}
	// Element serialization:

}

bool Mp7JrsURIFragmentReferenceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("href")))
	{
		// Deserialize string type
		this->Sethref(Dc1Convert::TextToString(parent->getAttribute(X("href"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsFragmentReferenceType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsURIFragmentReferenceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsURIFragmentReferenceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsURIFragmentReferenceType_ExtMethodImpl.h


