
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtImplInclude.h


#include "Mp7JrsauxiliaryLanguageType_LocalType0.h"
#include "Mp7JrsauxiliaryLanguageType_LocalType1.h"
#include "Mp7JrsauxiliaryLanguageType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsauxiliaryLanguageType::IMp7JrsauxiliaryLanguageType()
{

// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtPropInit.h

}

IMp7JrsauxiliaryLanguageType::~IMp7JrsauxiliaryLanguageType()
{
// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtPropCleanup.h

}

Mp7JrsauxiliaryLanguageType::Mp7JrsauxiliaryLanguageType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsauxiliaryLanguageType::~Mp7JrsauxiliaryLanguageType()
{
	Cleanup();
}

void Mp7JrsauxiliaryLanguageType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_auxiliaryLanguageType_LocalType = Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration; // Enumeration
	m_auxiliaryLanguageType_LocalType0 = Mp7JrsauxiliaryLanguageType_Local0Ptr(); // Class
	m_auxiliaryLanguageType_LocalType1 = Mp7JrsauxiliaryLanguageType_Local1Ptr(); // Class


// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtMyPropInit.h

}

void Mp7JrsauxiliaryLanguageType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType0);
	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType1);
}

void Mp7JrsauxiliaryLanguageType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use auxiliaryLanguageTypePtr, since we
	// might need GetBase(), which isn't defined in IauxiliaryLanguageType
	const Dc1Ptr< Mp7JrsauxiliaryLanguageType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if(tmp->GetauxiliaryLanguageType_LocalType() != Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration)
	{
		this->SetauxiliaryLanguageType_LocalType(tmp->GetauxiliaryLanguageType_LocalType());
		return;
	}
	if(tmp->GetauxiliaryLanguageType_LocalType0() != Mp7JrsauxiliaryLanguageType_Local0Ptr())
	{
		this->SetauxiliaryLanguageType_LocalType0(Dc1Factory::CloneObject(tmp->GetauxiliaryLanguageType_LocalType0()));
		return; 
	}
	if(tmp->GetauxiliaryLanguageType_LocalType1() != Mp7JrsauxiliaryLanguageType_Local1Ptr())
	{
		this->SetauxiliaryLanguageType_LocalType1(Dc1Factory::CloneObject(tmp->GetauxiliaryLanguageType_LocalType1()));
		return; 
	}
	}
}

Dc1NodePtr Mp7JrsauxiliaryLanguageType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsauxiliaryLanguageType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsauxiliaryLanguageType_LocalType::Enumeration Mp7JrsauxiliaryLanguageType::GetauxiliaryLanguageType_LocalType() const
{
		return m_auxiliaryLanguageType_LocalType;
}

Mp7JrsauxiliaryLanguageType_Local0Ptr Mp7JrsauxiliaryLanguageType::GetauxiliaryLanguageType_LocalType0() const
{
		return m_auxiliaryLanguageType_LocalType0;
}

Mp7JrsauxiliaryLanguageType_Local1Ptr Mp7JrsauxiliaryLanguageType::GetauxiliaryLanguageType_LocalType1() const
{
		return m_auxiliaryLanguageType_LocalType1;
}

void Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType(Mp7JrsauxiliaryLanguageType_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType().");
	}
	if (m_auxiliaryLanguageType_LocalType != item)
	{
		// nothing to free here, hopefully
		m_auxiliaryLanguageType_LocalType = item;
	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType0);
	m_auxiliaryLanguageType_LocalType0 = Mp7JrsauxiliaryLanguageType_Local0Ptr();
	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType1);
	m_auxiliaryLanguageType_LocalType1 = Mp7JrsauxiliaryLanguageType_Local1Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType0(const Mp7JrsauxiliaryLanguageType_Local0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType0().");
	}
	if (m_auxiliaryLanguageType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType0);
		m_auxiliaryLanguageType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_auxiliaryLanguageType_LocalType0) m_auxiliaryLanguageType_LocalType0->SetParent(m_myself.getPointer());
		if (m_auxiliaryLanguageType_LocalType0 && m_auxiliaryLanguageType_LocalType0->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType0"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_auxiliaryLanguageType_LocalType0->UseTypeAttribute = true;
		}
		if(m_auxiliaryLanguageType_LocalType0 != Mp7JrsauxiliaryLanguageType_Local0Ptr())
		{
			m_auxiliaryLanguageType_LocalType0->SetContentName(XMLString::transcode("auxiliaryLanguageType_LocalType0"));
		}
	
	m_auxiliaryLanguageType_LocalType = Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration;
	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType1);
	m_auxiliaryLanguageType_LocalType1 = Mp7JrsauxiliaryLanguageType_Local1Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType1(const Mp7JrsauxiliaryLanguageType_Local1Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsauxiliaryLanguageType::SetauxiliaryLanguageType_LocalType1().");
	}
	if (m_auxiliaryLanguageType_LocalType1 != item)
	{
		// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType1);
		m_auxiliaryLanguageType_LocalType1 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_auxiliaryLanguageType_LocalType1) m_auxiliaryLanguageType_LocalType1->SetParent(m_myself.getPointer());
		if (m_auxiliaryLanguageType_LocalType1 && m_auxiliaryLanguageType_LocalType1->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType1"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_auxiliaryLanguageType_LocalType1->UseTypeAttribute = true;
		}
		if(m_auxiliaryLanguageType_LocalType1 != Mp7JrsauxiliaryLanguageType_Local1Ptr())
		{
			m_auxiliaryLanguageType_LocalType1->SetContentName(XMLString::transcode("auxiliaryLanguageType_LocalType1"));
		}
	
	m_auxiliaryLanguageType_LocalType = Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration;
	// Dc1Factory::DeleteObject(m_auxiliaryLanguageType_LocalType0);
	m_auxiliaryLanguageType_LocalType0 = Mp7JrsauxiliaryLanguageType_Local0Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsauxiliaryLanguageType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("auxiliaryLanguageType_LocalType0")) == 0)
	{
		// auxiliaryLanguageType_LocalType0 is simple element auxiliaryLanguageType_LocalType0
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetauxiliaryLanguageType_LocalType0()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType0")))) != empty)
			{
				// Is type allowed
				Mp7JrsauxiliaryLanguageType_Local0Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetauxiliaryLanguageType_LocalType0(p, client);
					if((p = GetauxiliaryLanguageType_LocalType0()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("auxiliaryLanguageType_LocalType1")) == 0)
	{
		// auxiliaryLanguageType_LocalType1 is simple element auxiliaryLanguageType_LocalType1
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetauxiliaryLanguageType_LocalType1()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:auxiliaryLanguageType_LocalType1")))) != empty)
			{
				// Is type allowed
				Mp7JrsauxiliaryLanguageType_Local1Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetauxiliaryLanguageType_LocalType1(p, client);
					if((p = GetauxiliaryLanguageType_LocalType1()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for auxiliaryLanguageType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "auxiliaryLanguageType");
	}
	return result;
}

XMLCh * Mp7JrsauxiliaryLanguageType::ToText() const
{
	if(m_auxiliaryLanguageType_LocalType != Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration)
	{
		return Mp7JrsauxiliaryLanguageType_LocalType::ToText(m_auxiliaryLanguageType_LocalType);
	}
	if(m_auxiliaryLanguageType_LocalType0 != Mp7JrsauxiliaryLanguageType_Local0Ptr())
	{
		return m_auxiliaryLanguageType_LocalType0->ToText();
	}
	if(m_auxiliaryLanguageType_LocalType1 != Mp7JrsauxiliaryLanguageType_Local1Ptr())
	{
		return m_auxiliaryLanguageType_LocalType1->ToText();
	}
	return XMLString::transcode("");
}


bool Mp7JrsauxiliaryLanguageType::Parse(const XMLCh * const txt)
{
	// Parse stu enumeration
	{
		Mp7JrsauxiliaryLanguageType_LocalType::Enumeration tmp = Mp7JrsauxiliaryLanguageType_LocalType::Parse(txt);
		if(tmp != Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration)
		{
			this->SetauxiliaryLanguageType_LocalType(tmp);
			return true;
		}
	}
	// Parse stu factory type
	{
		Mp7JrsauxiliaryLanguageType_Local0Ptr tmp = CreateauxiliaryLanguageType_LocalType0; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SetauxiliaryLanguageType_LocalType0(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	// Parse stu factory type
	{
		Mp7JrsauxiliaryLanguageType_Local1Ptr tmp = CreateauxiliaryLanguageType_LocalType1; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SetauxiliaryLanguageType_LocalType1(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	return false;
}

bool Mp7JrsauxiliaryLanguageType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsauxiliaryLanguageType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsauxiliaryLanguageType::ContentToString() const
{
	// Mp7JrsauxiliaryLanguageType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsauxiliaryLanguageType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("auxiliaryLanguageType"));
	// Element serialization:
	if(m_auxiliaryLanguageType_LocalType != Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsauxiliaryLanguageType_LocalType::ToText(m_auxiliaryLanguageType_LocalType);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("auxiliaryLanguageType_LocalType"), false);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_auxiliaryLanguageType_LocalType0 != Mp7JrsauxiliaryLanguageType_Local0Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_auxiliaryLanguageType_LocalType0->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("auxiliaryLanguageType_LocalType0"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_auxiliaryLanguageType_LocalType0->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_auxiliaryLanguageType_LocalType1 != Mp7JrsauxiliaryLanguageType_Local1Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_auxiliaryLanguageType_LocalType1->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("auxiliaryLanguageType_LocalType1"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_auxiliaryLanguageType_LocalType1->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsauxiliaryLanguageType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("auxiliaryLanguageType_LocalType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("auxiliaryLanguageType_LocalType")) == 0))
		{
			Mp7JrsauxiliaryLanguageType_LocalType::Enumeration tmp = Mp7JrsauxiliaryLanguageType_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsauxiliaryLanguageType_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetauxiliaryLanguageType_LocalType(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("auxiliaryLanguageType_LocalType0"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("auxiliaryLanguageType_LocalType0")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateauxiliaryLanguageType_LocalType0; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetauxiliaryLanguageType_LocalType0(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("auxiliaryLanguageType_LocalType1"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("auxiliaryLanguageType_LocalType1")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateauxiliaryLanguageType_LocalType1; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetauxiliaryLanguageType_LocalType1(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsauxiliaryLanguageType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("auxiliaryLanguageType_LocalType0")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateauxiliaryLanguageType_LocalType0; // FTT, check this
	}
	this->SetauxiliaryLanguageType_LocalType0(child);
  }
  if (XMLString::compareString(elementname, X("auxiliaryLanguageType_LocalType1")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateauxiliaryLanguageType_LocalType1; // FTT, check this
	}
	this->SetauxiliaryLanguageType_LocalType1(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsauxiliaryLanguageType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetauxiliaryLanguageType_LocalType0() != Dc1NodePtr())
		result.Insert(GetauxiliaryLanguageType_LocalType0());
	if (GetauxiliaryLanguageType_LocalType1() != Dc1NodePtr())
		result.Insert(GetauxiliaryLanguageType_LocalType1());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsauxiliaryLanguageType_ExtMethodImpl.h


