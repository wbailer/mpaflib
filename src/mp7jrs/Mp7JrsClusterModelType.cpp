
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtImplInclude.h


#include "Mp7JrsAnalyticModelType.h"
#include "Mp7JrsClusterModelType_CollectionType.h"
#include "Mp7JrsClusterModelType_CollectionType0.h"
#include "Mp7JrsDescriptorModelType.h"
#include "Mp7JrsProbabilityModelType.h"
#include "Mp7JrsAnalyticModelType_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsClusterModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAnalyticModelType_LocalType.h" // Choice collection Label
#include "Mp7JrsAnalyticModelType_Label_LocalType.h" // Choice collection element Label
#include "Mp7JrsAnalyticModelType_Semantics_LocalType.h" // Choice collection element Semantics
#include "Mp7JrsClusterModelType_LocalType.h" // Choice collection Collection
#include "Mp7JrsCollectionType.h" // Choice collection element Collection
#include "Mp7JrsReferenceType.h" // Choice collection element CollectionRef
#include "Mp7JrsClusterModelType_LocalType0.h" // Choice collection ClusterModel
#include "Mp7JrsClusterModelType.h" // Choice collection element ClusterModel

#include <assert.h>
IMp7JrsClusterModelType::IMp7JrsClusterModelType()
{

// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtPropInit.h

}

IMp7JrsClusterModelType::~IMp7JrsClusterModelType()
{
// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtPropCleanup.h

}

Mp7JrsClusterModelType::Mp7JrsClusterModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClusterModelType::~Mp7JrsClusterModelType()
{
	Cleanup();
}

void Mp7JrsClusterModelType::Init()
{
	// Init base
	m_Base = CreateAnalyticModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ClusterModelType_LocalType = Mp7JrsClusterModelType_CollectionPtr(); // Collection
	m_ClusterModelType_LocalType0 = Mp7JrsClusterModelType_Collection0Ptr(); // Collection
	m_DescriptorModel = Mp7JrsDescriptorModelPtr(); // Class
	m_ProbabilityModel = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtMyPropInit.h

}

void Mp7JrsClusterModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType);
	// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType0);
	// Dc1Factory::DeleteObject(m_DescriptorModel);
	// Dc1Factory::DeleteObject(m_ProbabilityModel);
}

void Mp7JrsClusterModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClusterModelTypePtr, since we
	// might need GetBase(), which isn't defined in IClusterModelType
	const Dc1Ptr< Mp7JrsClusterModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAnalyticModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClusterModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType);
		this->SetClusterModelType_LocalType(Dc1Factory::CloneObject(tmp->GetClusterModelType_LocalType()));
		// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType0);
		this->SetClusterModelType_LocalType0(Dc1Factory::CloneObject(tmp->GetClusterModelType_LocalType0()));
		// Dc1Factory::DeleteObject(m_DescriptorModel);
		this->SetDescriptorModel(Dc1Factory::CloneObject(tmp->GetDescriptorModel()));
		// Dc1Factory::DeleteObject(m_ProbabilityModel);
		this->SetProbabilityModel(Dc1Factory::CloneObject(tmp->GetProbabilityModel()));
}

Dc1NodePtr Mp7JrsClusterModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClusterModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAnalyticModelType > Mp7JrsClusterModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsClusterModelType_CollectionPtr Mp7JrsClusterModelType::GetClusterModelType_LocalType() const
{
		return m_ClusterModelType_LocalType;
}

Mp7JrsClusterModelType_Collection0Ptr Mp7JrsClusterModelType::GetClusterModelType_LocalType0() const
{
		return m_ClusterModelType_LocalType0;
}

Mp7JrsDescriptorModelPtr Mp7JrsClusterModelType::GetDescriptorModel() const
{
		return m_DescriptorModel;
}

Dc1Ptr< Dc1Node > Mp7JrsClusterModelType::GetProbabilityModel() const
{
		return m_ProbabilityModel;
}

void Mp7JrsClusterModelType::SetClusterModelType_LocalType(const Mp7JrsClusterModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetClusterModelType_LocalType().");
	}
	if (m_ClusterModelType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType);
		m_ClusterModelType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClusterModelType_LocalType) m_ClusterModelType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::SetClusterModelType_LocalType0(const Mp7JrsClusterModelType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetClusterModelType_LocalType0().");
	}
	if (m_ClusterModelType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_ClusterModelType_LocalType0);
		m_ClusterModelType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClusterModelType_LocalType0) m_ClusterModelType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::SetDescriptorModel(const Mp7JrsDescriptorModelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetDescriptorModel().");
	}
	if (m_DescriptorModel != item)
	{
		// Dc1Factory::DeleteObject(m_DescriptorModel);
		m_DescriptorModel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptorModel) m_DescriptorModel->SetParent(m_myself.getPointer());
		if (m_DescriptorModel && m_DescriptorModel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorModelType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DescriptorModel->UseTypeAttribute = true;
		}
		if(m_DescriptorModel != Mp7JrsDescriptorModelPtr())
		{
			m_DescriptorModel->SetContentName(XMLString::transcode("DescriptorModel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::SetProbabilityModel(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetProbabilityModel().");
	}
	if (m_ProbabilityModel != item)
	{
		// Dc1Factory::DeleteObject(m_ProbabilityModel);
		m_ProbabilityModel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ProbabilityModel) m_ProbabilityModel->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_ProbabilityModel) m_ProbabilityModel->UseTypeAttribute = true;
		if(m_ProbabilityModel != Dc1Ptr< Dc1Node >())
		{
			m_ProbabilityModel->SetContentName(XMLString::transcode("ProbabilityModel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsAnalyticModelType_function_LocalType::Enumeration Mp7JrsClusterModelType::Getfunction() const
{
	return GetBase()->Getfunction();
}

bool Mp7JrsClusterModelType::Existfunction() const
{
	return GetBase()->Existfunction();
}
void Mp7JrsClusterModelType::Setfunction(Mp7JrsAnalyticModelType_function_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::Setfunction().");
	}
	GetBase()->Setfunction(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::Invalidatefunction()
{
	GetBase()->Invalidatefunction();
}
Mp7JrsAnalyticModelType_CollectionPtr Mp7JrsClusterModelType::GetAnalyticModelType_LocalType() const
{
	return GetBase()->GetAnalyticModelType_LocalType();
}

void Mp7JrsClusterModelType::SetAnalyticModelType_LocalType(const Mp7JrsAnalyticModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetAnalyticModelType_LocalType().");
	}
	GetBase()->SetAnalyticModelType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsClusterModelType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsClusterModelType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsClusterModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsClusterModelType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsClusterModelType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsClusterModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsClusterModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsClusterModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsClusterModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsClusterModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsClusterModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsClusterModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsClusterModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsClusterModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsClusterModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsClusterModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsClusterModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsClusterModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsClusterModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsClusterModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsClusterModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsClusterModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsClusterModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClusterModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Collection")) == 0)
	{
		// Collection is contained in itemtype ClusterModelType_LocalType
		// in choice collection ClusterModelType_CollectionType

		context->Found = true;
		Mp7JrsClusterModelType_CollectionPtr coll = GetClusterModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterModelType_CollectionType; // FTT, check this
				SetClusterModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterModelType_LocalPtr item = CreateClusterModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->SetCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CollectionRef")) == 0)
	{
		// CollectionRef is contained in itemtype ClusterModelType_LocalType
		// in choice collection ClusterModelType_CollectionType

		context->Found = true;
		Mp7JrsClusterModelType_CollectionPtr coll = GetClusterModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterModelType_CollectionType; // FTT, check this
				SetClusterModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterModelType_LocalPtr item = CreateClusterModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->SetCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterModelType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClusterModel")) == 0)
	{
		// ClusterModel is contained in itemtype ClusterModelType_LocalType0
		// in choice collection ClusterModelType_CollectionType0

		context->Found = true;
		Mp7JrsClusterModelType_Collection0Ptr coll = GetClusterModelType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterModelType_CollectionType0; // FTT, check this
				SetClusterModelType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->GetClusterModel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterModelType_Local0Ptr item = CreateClusterModelType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClusterModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->SetClusterModel(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->GetClusterModel()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClusterModelRef")) == 0)
	{
		// ClusterModelRef is contained in itemtype ClusterModelType_LocalType0
		// in choice collection ClusterModelType_CollectionType0

		context->Found = true;
		Mp7JrsClusterModelType_Collection0Ptr coll = GetClusterModelType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterModelType_CollectionType0; // FTT, check this
				SetClusterModelType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->GetClusterModelRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterModelType_Local0Ptr item = CreateClusterModelType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->SetClusterModelRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterModelType_Local0Ptr)coll->elementAt(i))->GetClusterModelRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DescriptorModel")) == 0)
	{
		// DescriptorModel is simple element DescriptorModelType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptorModel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DescriptorModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDescriptorModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptorModel(p, client);
					if((p = GetDescriptorModel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ProbabilityModel")) == 0)
	{
		// ProbabilityModel is simple abstract element ProbabilityModelType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetProbabilityModel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetProbabilityModel(p, client);
					if((p = GetProbabilityModel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClusterModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClusterModelType");
	}
	return result;
}

XMLCh * Mp7JrsClusterModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsClusterModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsClusterModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClusterModelType"));
	// Element serialization:
	if (m_ClusterModelType_LocalType != Mp7JrsClusterModelType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClusterModelType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ClusterModelType_LocalType0 != Mp7JrsClusterModelType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClusterModelType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_DescriptorModel != Mp7JrsDescriptorModelPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptorModel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptorModel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DescriptorModel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ProbabilityModel != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ProbabilityModel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ProbabilityModel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_ProbabilityModel->UseTypeAttribute = true;
		}
		m_ProbabilityModel->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsClusterModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAnalyticModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Collection
			Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Collection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:CollectionRef
			Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsClusterModelType_CollectionPtr tmp = CreateClusterModelType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClusterModelType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:ClusterModel
			Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModel")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ClusterModelRef
			Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModelRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsClusterModelType_Collection0Ptr tmp = CreateClusterModelType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClusterModelType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptorModel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptorModel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDescriptorModelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptorModel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ProbabilityModel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ProbabilityModel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateProbabilityModelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetProbabilityModel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClusterModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Collection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("CollectionRef")) == 0)
	)
  {
	Mp7JrsClusterModelType_CollectionPtr tmp = CreateClusterModelType_CollectionType; // FTT, check this
	this->SetClusterModelType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModel")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModelRef")) == 0)
	)
  {
	Mp7JrsClusterModelType_Collection0Ptr tmp = CreateClusterModelType_CollectionType0; // FTT, check this
	this->SetClusterModelType_LocalType0(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("DescriptorModel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDescriptorModelType; // FTT, check this
	}
	this->SetDescriptorModel(child);
  }
  if (XMLString::compareString(elementname, X("ProbabilityModel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateProbabilityModelType; // FTT, check this
	}
	this->SetProbabilityModel(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClusterModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetClusterModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetClusterModelType_LocalType());
	if (GetClusterModelType_LocalType0() != Dc1NodePtr())
		result.Insert(GetClusterModelType_LocalType0());
	if (GetAnalyticModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetAnalyticModelType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetDescriptorModel() != Dc1NodePtr())
		result.Insert(GetDescriptorModel());
	if (GetProbabilityModel() != Dc1NodePtr())
		result.Insert(GetProbabilityModel());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClusterModelType_ExtMethodImpl.h


