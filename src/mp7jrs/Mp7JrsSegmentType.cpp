
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSegmentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation

#include <assert.h>
IMp7JrsSegmentType::IMp7JrsSegmentType()
{

// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtPropInit.h

}

IMp7JrsSegmentType::~IMp7JrsSegmentType()
{
// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtPropCleanup.h

}

Mp7JrsSegmentType::Mp7JrsSegmentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSegmentType::~Mp7JrsSegmentType()
{
	Cleanup();
}

void Mp7JrsSegmentType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MediaInformation = Mp7JrsMediaInformationPtr(); // Class
	m_MediaInformationRef = Mp7JrsReferencePtr(); // Class
	m_MediaLocator = Mp7JrsMediaLocatorPtr(); // Class
	m_StructuralUnit = Mp7JrsControlledTermUsePtr(); // Class
	m_StructuralUnit_Exist = false;
	m_CreationInformation = Mp7JrsCreationInformationPtr(); // Class
	m_CreationInformationRef = Mp7JrsReferencePtr(); // Class
	m_UsageInformation = Mp7JrsUsageInformationPtr(); // Class
	m_UsageInformationRef = Mp7JrsReferencePtr(); // Class
	m_TextAnnotation = Mp7JrsSegmentType_TextAnnotation_CollectionPtr(); // Collection
	m_SegmentType_LocalType = Mp7JrsSegmentType_CollectionPtr(); // Collection
	m_MatchingHint = Mp7JrsSegmentType_MatchingHint_CollectionPtr(); // Collection
	m_PointOfView = Mp7JrsSegmentType_PointOfView_CollectionPtr(); // Collection
	m_Relation = Mp7JrsSegmentType_Relation_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtMyPropInit.h

}

void Mp7JrsSegmentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MediaInformation);
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	// Dc1Factory::DeleteObject(m_MediaLocator);
	// Dc1Factory::DeleteObject(m_StructuralUnit);
	// Dc1Factory::DeleteObject(m_CreationInformation);
	// Dc1Factory::DeleteObject(m_CreationInformationRef);
	// Dc1Factory::DeleteObject(m_UsageInformation);
	// Dc1Factory::DeleteObject(m_UsageInformationRef);
	// Dc1Factory::DeleteObject(m_TextAnnotation);
	// Dc1Factory::DeleteObject(m_SegmentType_LocalType);
	// Dc1Factory::DeleteObject(m_MatchingHint);
	// Dc1Factory::DeleteObject(m_PointOfView);
	// Dc1Factory::DeleteObject(m_Relation);
}

void Mp7JrsSegmentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SegmentTypePtr, since we
	// might need GetBase(), which isn't defined in ISegmentType
	const Dc1Ptr< Mp7JrsSegmentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSegmentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MediaInformation);
		this->SetMediaInformation(Dc1Factory::CloneObject(tmp->GetMediaInformation()));
		// Dc1Factory::DeleteObject(m_MediaInformationRef);
		this->SetMediaInformationRef(Dc1Factory::CloneObject(tmp->GetMediaInformationRef()));
		// Dc1Factory::DeleteObject(m_MediaLocator);
		this->SetMediaLocator(Dc1Factory::CloneObject(tmp->GetMediaLocator()));
	if (tmp->IsValidStructuralUnit())
	{
		// Dc1Factory::DeleteObject(m_StructuralUnit);
		this->SetStructuralUnit(Dc1Factory::CloneObject(tmp->GetStructuralUnit()));
	}
	else
	{
		InvalidateStructuralUnit();
	}
		// Dc1Factory::DeleteObject(m_CreationInformation);
		this->SetCreationInformation(Dc1Factory::CloneObject(tmp->GetCreationInformation()));
		// Dc1Factory::DeleteObject(m_CreationInformationRef);
		this->SetCreationInformationRef(Dc1Factory::CloneObject(tmp->GetCreationInformationRef()));
		// Dc1Factory::DeleteObject(m_UsageInformation);
		this->SetUsageInformation(Dc1Factory::CloneObject(tmp->GetUsageInformation()));
		// Dc1Factory::DeleteObject(m_UsageInformationRef);
		this->SetUsageInformationRef(Dc1Factory::CloneObject(tmp->GetUsageInformationRef()));
		// Dc1Factory::DeleteObject(m_TextAnnotation);
		this->SetTextAnnotation(Dc1Factory::CloneObject(tmp->GetTextAnnotation()));
		// Dc1Factory::DeleteObject(m_SegmentType_LocalType);
		this->SetSegmentType_LocalType(Dc1Factory::CloneObject(tmp->GetSegmentType_LocalType()));
		// Dc1Factory::DeleteObject(m_MatchingHint);
		this->SetMatchingHint(Dc1Factory::CloneObject(tmp->GetMatchingHint()));
		// Dc1Factory::DeleteObject(m_PointOfView);
		this->SetPointOfView(Dc1Factory::CloneObject(tmp->GetPointOfView()));
		// Dc1Factory::DeleteObject(m_Relation);
		this->SetRelation(Dc1Factory::CloneObject(tmp->GetRelation()));
}

Dc1NodePtr Mp7JrsSegmentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSegmentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSegmentType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaInformationPtr Mp7JrsSegmentType::GetMediaInformation() const
{
		return m_MediaInformation;
}

Mp7JrsReferencePtr Mp7JrsSegmentType::GetMediaInformationRef() const
{
		return m_MediaInformationRef;
}

Mp7JrsMediaLocatorPtr Mp7JrsSegmentType::GetMediaLocator() const
{
		return m_MediaLocator;
}

Mp7JrsControlledTermUsePtr Mp7JrsSegmentType::GetStructuralUnit() const
{
		return m_StructuralUnit;
}

// Element is optional
bool Mp7JrsSegmentType::IsValidStructuralUnit() const
{
	return m_StructuralUnit_Exist;
}

Mp7JrsCreationInformationPtr Mp7JrsSegmentType::GetCreationInformation() const
{
		return m_CreationInformation;
}

Mp7JrsReferencePtr Mp7JrsSegmentType::GetCreationInformationRef() const
{
		return m_CreationInformationRef;
}

Mp7JrsUsageInformationPtr Mp7JrsSegmentType::GetUsageInformation() const
{
		return m_UsageInformation;
}

Mp7JrsReferencePtr Mp7JrsSegmentType::GetUsageInformationRef() const
{
		return m_UsageInformationRef;
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsSegmentType::GetTextAnnotation() const
{
		return m_TextAnnotation;
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsSegmentType::GetSegmentType_LocalType() const
{
		return m_SegmentType_LocalType;
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsSegmentType::GetMatchingHint() const
{
		return m_MatchingHint;
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsSegmentType::GetPointOfView() const
{
		return m_PointOfView;
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsSegmentType::GetRelation() const
{
		return m_Relation;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetMediaInformation().");
	}
	if (m_MediaInformation != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInformation);
		m_MediaInformation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInformation) m_MediaInformation->SetParent(m_myself.getPointer());
		if (m_MediaInformation && m_MediaInformation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaInformation->UseTypeAttribute = true;
		}
		if(m_MediaInformation != Mp7JrsMediaInformationPtr())
		{
			m_MediaInformation->SetContentName(XMLString::transcode("MediaInformation"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	m_MediaInformationRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MediaLocator);
	m_MediaLocator = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetMediaInformationRef().");
	}
	if (m_MediaInformationRef != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInformationRef);
		m_MediaInformationRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInformationRef) m_MediaInformationRef->SetParent(m_myself.getPointer());
		if (m_MediaInformationRef && m_MediaInformationRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaInformationRef->UseTypeAttribute = true;
		}
		if(m_MediaInformationRef != Mp7JrsReferencePtr())
		{
			m_MediaInformationRef->SetContentName(XMLString::transcode("MediaInformationRef"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformation);
	m_MediaInformation = Mp7JrsMediaInformationPtr();
	// Dc1Factory::DeleteObject(m_MediaLocator);
	m_MediaLocator = Mp7JrsMediaLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetMediaLocator().");
	}
	if (m_MediaLocator != item)
	{
		// Dc1Factory::DeleteObject(m_MediaLocator);
		m_MediaLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaLocator) m_MediaLocator->SetParent(m_myself.getPointer());
		if (m_MediaLocator && m_MediaLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaLocator->UseTypeAttribute = true;
		}
		if(m_MediaLocator != Mp7JrsMediaLocatorPtr())
		{
			m_MediaLocator->SetContentName(XMLString::transcode("MediaLocator"));
		}
	// Dc1Factory::DeleteObject(m_MediaInformation);
	m_MediaInformation = Mp7JrsMediaInformationPtr();
	// Dc1Factory::DeleteObject(m_MediaInformationRef);
	m_MediaInformationRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetStructuralUnit().");
	}
	if (m_StructuralUnit != item || m_StructuralUnit_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_StructuralUnit);
		m_StructuralUnit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuralUnit) m_StructuralUnit->SetParent(m_myself.getPointer());
		if (m_StructuralUnit && m_StructuralUnit->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StructuralUnit->UseTypeAttribute = true;
		}
		m_StructuralUnit_Exist = true;
		if(m_StructuralUnit != Mp7JrsControlledTermUsePtr())
		{
			m_StructuralUnit->SetContentName(XMLString::transcode("StructuralUnit"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::InvalidateStructuralUnit()
{
	m_StructuralUnit_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetCreationInformation().");
	}
	if (m_CreationInformation != item)
	{
		// Dc1Factory::DeleteObject(m_CreationInformation);
		m_CreationInformation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationInformation) m_CreationInformation->SetParent(m_myself.getPointer());
		if (m_CreationInformation && m_CreationInformation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationInformationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CreationInformation->UseTypeAttribute = true;
		}
		if(m_CreationInformation != Mp7JrsCreationInformationPtr())
		{
			m_CreationInformation->SetContentName(XMLString::transcode("CreationInformation"));
		}
	// Dc1Factory::DeleteObject(m_CreationInformationRef);
	m_CreationInformationRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetCreationInformationRef().");
	}
	if (m_CreationInformationRef != item)
	{
		// Dc1Factory::DeleteObject(m_CreationInformationRef);
		m_CreationInformationRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationInformationRef) m_CreationInformationRef->SetParent(m_myself.getPointer());
		if (m_CreationInformationRef && m_CreationInformationRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CreationInformationRef->UseTypeAttribute = true;
		}
		if(m_CreationInformationRef != Mp7JrsReferencePtr())
		{
			m_CreationInformationRef->SetContentName(XMLString::transcode("CreationInformationRef"));
		}
	// Dc1Factory::DeleteObject(m_CreationInformation);
	m_CreationInformation = Mp7JrsCreationInformationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetUsageInformation().");
	}
	if (m_UsageInformation != item)
	{
		// Dc1Factory::DeleteObject(m_UsageInformation);
		m_UsageInformation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsageInformation) m_UsageInformation->SetParent(m_myself.getPointer());
		if (m_UsageInformation && m_UsageInformation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageInformationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UsageInformation->UseTypeAttribute = true;
		}
		if(m_UsageInformation != Mp7JrsUsageInformationPtr())
		{
			m_UsageInformation->SetContentName(XMLString::transcode("UsageInformation"));
		}
	// Dc1Factory::DeleteObject(m_UsageInformationRef);
	m_UsageInformationRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetUsageInformationRef().");
	}
	if (m_UsageInformationRef != item)
	{
		// Dc1Factory::DeleteObject(m_UsageInformationRef);
		m_UsageInformationRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsageInformationRef) m_UsageInformationRef->SetParent(m_myself.getPointer());
		if (m_UsageInformationRef && m_UsageInformationRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UsageInformationRef->UseTypeAttribute = true;
		}
		if(m_UsageInformationRef != Mp7JrsReferencePtr())
		{
			m_UsageInformationRef->SetContentName(XMLString::transcode("UsageInformationRef"));
		}
	// Dc1Factory::DeleteObject(m_UsageInformation);
	m_UsageInformation = Mp7JrsUsageInformationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetTextAnnotation().");
	}
	if (m_TextAnnotation != item)
	{
		// Dc1Factory::DeleteObject(m_TextAnnotation);
		m_TextAnnotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextAnnotation) m_TextAnnotation->SetParent(m_myself.getPointer());
		if(m_TextAnnotation != Mp7JrsSegmentType_TextAnnotation_CollectionPtr())
		{
			m_TextAnnotation->SetContentName(XMLString::transcode("TextAnnotation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetSegmentType_LocalType().");
	}
	if (m_SegmentType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SegmentType_LocalType);
		m_SegmentType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SegmentType_LocalType) m_SegmentType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetMatchingHint().");
	}
	if (m_MatchingHint != item)
	{
		// Dc1Factory::DeleteObject(m_MatchingHint);
		m_MatchingHint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MatchingHint) m_MatchingHint->SetParent(m_myself.getPointer());
		if(m_MatchingHint != Mp7JrsSegmentType_MatchingHint_CollectionPtr())
		{
			m_MatchingHint->SetContentName(XMLString::transcode("MatchingHint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetPointOfView().");
	}
	if (m_PointOfView != item)
	{
		// Dc1Factory::DeleteObject(m_PointOfView);
		m_PointOfView = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PointOfView) m_PointOfView->SetParent(m_myself.getPointer());
		if(m_PointOfView != Mp7JrsSegmentType_PointOfView_CollectionPtr())
		{
			m_PointOfView->SetContentName(XMLString::transcode("PointOfView"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetRelation().");
	}
	if (m_Relation != item)
	{
		// Dc1Factory::DeleteObject(m_Relation);
		m_Relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relation) m_Relation->SetParent(m_myself.getPointer());
		if(m_Relation != Mp7JrsSegmentType_Relation_CollectionPtr())
		{
			m_Relation->SetContentName(XMLString::transcode("Relation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSegmentType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSegmentType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSegmentType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSegmentType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSegmentType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSegmentType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSegmentType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSegmentType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSegmentType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSegmentType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSegmentType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSegmentType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSegmentType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSegmentType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSegmentType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSegmentType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSegmentType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSegmentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaInformation")) == 0)
	{
		// MediaInformation is simple element MediaInformationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaInformation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaInformationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaInformation(p, client);
					if((p = GetMediaInformation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaInformationRef")) == 0)
	{
		// MediaInformationRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaInformationRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaInformationRef(p, client);
					if((p = GetMediaInformationRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaLocator")) == 0)
	{
		// MediaLocator is simple element MediaLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaLocator(p, client);
					if((p = GetMediaLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuralUnit")) == 0)
	{
		// StructuralUnit is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStructuralUnit()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStructuralUnit(p, client);
					if((p = GetStructuralUnit()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationInformation")) == 0)
	{
		// CreationInformation is simple element CreationInformationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCreationInformation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationInformationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationInformationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCreationInformation(p, client);
					if((p = GetCreationInformation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationInformationRef")) == 0)
	{
		// CreationInformationRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCreationInformationRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCreationInformationRef(p, client);
					if((p = GetCreationInformationRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsageInformation")) == 0)
	{
		// UsageInformation is simple element UsageInformationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUsageInformation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageInformationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUsageInformationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUsageInformation(p, client);
					if((p = GetUsageInformation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsageInformationRef")) == 0)
	{
		// UsageInformationRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUsageInformationRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUsageInformationRef(p, client);
					if((p = GetUsageInformationRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextAnnotation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:TextAnnotation is item of type SegmentType_TextAnnotation_LocalType
		// in element collection SegmentType_TextAnnotation_CollectionType
		
		context->Found = true;
		Mp7JrsSegmentType_TextAnnotation_CollectionPtr coll = GetTextAnnotation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_TextAnnotation_CollectionType; // FTT, check this
				SetTextAnnotation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentType_TextAnnotation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSegmentType_TextAnnotation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Semantic")) == 0)
	{
		// Semantic is contained in itemtype SegmentType_LocalType
		// in choice collection SegmentType_CollectionType

		context->Found = true;
		Mp7JrsSegmentType_CollectionPtr coll = GetSegmentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_CollectionType; // FTT, check this
				SetSegmentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->GetSemantic()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentType_LocalPtr item = CreateSegmentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->SetSemantic(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->GetSemantic()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SemanticRef")) == 0)
	{
		// SemanticRef is contained in itemtype SegmentType_LocalType
		// in choice collection SegmentType_CollectionType

		context->Found = true;
		Mp7JrsSegmentType_CollectionPtr coll = GetSegmentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_CollectionType; // FTT, check this
				SetSegmentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->GetSemanticRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentType_LocalPtr item = CreateSegmentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->SetSemanticRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentType_LocalPtr)coll->elementAt(i))->GetSemanticRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MatchingHint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MatchingHint is item of type MatchingHintType
		// in element collection SegmentType_MatchingHint_CollectionType
		
		context->Found = true;
		Mp7JrsSegmentType_MatchingHint_CollectionPtr coll = GetMatchingHint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_MatchingHint_CollectionType; // FTT, check this
				SetMatchingHint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MatchingHintType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMatchingHintPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PointOfView")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PointOfView is item of type PointOfViewType
		// in element collection SegmentType_PointOfView_CollectionType
		
		context->Found = true;
		Mp7JrsSegmentType_PointOfView_CollectionPtr coll = GetPointOfView();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_PointOfView_CollectionType; // FTT, check this
				SetPointOfView(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PointOfViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPointOfViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Relation is item of type RelationType
		// in element collection SegmentType_Relation_CollectionType
		
		context->Found = true;
		Mp7JrsSegmentType_Relation_CollectionPtr coll = GetRelation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentType_Relation_CollectionType; // FTT, check this
				SetRelation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SegmentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SegmentType");
	}
	return result;
}

XMLCh * Mp7JrsSegmentType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSegmentType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSegmentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SegmentType"));
	// Element serialization:
	// Class
	
	if (m_MediaInformation != Mp7JrsMediaInformationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaInformation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaInformation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaInformation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaInformationRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaInformationRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaInformationRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaInformationRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaLocator != Mp7JrsMediaLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StructuralUnit != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StructuralUnit->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StructuralUnit"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StructuralUnit->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CreationInformation != Mp7JrsCreationInformationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CreationInformation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CreationInformation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CreationInformation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CreationInformationRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CreationInformationRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CreationInformationRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CreationInformationRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_UsageInformation != Mp7JrsUsageInformationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UsageInformation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UsageInformation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UsageInformation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_UsageInformationRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UsageInformationRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UsageInformationRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UsageInformationRef->Serialize(doc, element, &elem);
	}
	if (m_TextAnnotation != Mp7JrsSegmentType_TextAnnotation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TextAnnotation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SegmentType_LocalType != Mp7JrsSegmentType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SegmentType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MatchingHint != Mp7JrsSegmentType_MatchingHint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MatchingHint->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PointOfView != Mp7JrsSegmentType_PointOfView_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PointOfView->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Relation != Mp7JrsSegmentType_Relation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relation->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSegmentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaInformation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaInformation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaInformationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaInformation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaInformationRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaInformationRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaInformationRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StructuralUnit"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StructuralUnit")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStructuralUnit(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CreationInformation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CreationInformation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCreationInformationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCreationInformation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CreationInformationRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CreationInformationRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCreationInformationRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UsageInformation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UsageInformation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUsageInformationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUsageInformation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UsageInformationRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UsageInformationRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUsageInformationRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("TextAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("TextAnnotation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSegmentType_TextAnnotation_CollectionPtr tmp = CreateSegmentType_TextAnnotation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTextAnnotation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Semantic
			Dc1Util::HasNodeName(parent, X("Semantic"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Semantic")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SemanticRef
			Dc1Util::HasNodeName(parent, X("SemanticRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SemanticRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSegmentType_CollectionPtr tmp = CreateSegmentType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSegmentType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MatchingHint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MatchingHint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSegmentType_MatchingHint_CollectionPtr tmp = CreateSegmentType_MatchingHint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMatchingHint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PointOfView"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PointOfView")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSegmentType_PointOfView_CollectionPtr tmp = CreateSegmentType_PointOfView_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPointOfView(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSegmentType_Relation_CollectionPtr tmp = CreateSegmentType_Relation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSegmentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("MediaInformation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaInformationType; // FTT, check this
	}
	this->SetMediaInformation(child);
  }
  if (XMLString::compareString(elementname, X("MediaInformationRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetMediaInformationRef(child);
  }
  if (XMLString::compareString(elementname, X("MediaLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaLocatorType; // FTT, check this
	}
	this->SetMediaLocator(child);
  }
  if (XMLString::compareString(elementname, X("StructuralUnit")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetStructuralUnit(child);
  }
  if (XMLString::compareString(elementname, X("CreationInformation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCreationInformationType; // FTT, check this
	}
	this->SetCreationInformation(child);
  }
  if (XMLString::compareString(elementname, X("CreationInformationRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetCreationInformationRef(child);
  }
  if (XMLString::compareString(elementname, X("UsageInformation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUsageInformationType; // FTT, check this
	}
	this->SetUsageInformation(child);
  }
  if (XMLString::compareString(elementname, X("UsageInformationRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetUsageInformationRef(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("TextAnnotation")) == 0))
  {
	Mp7JrsSegmentType_TextAnnotation_CollectionPtr tmp = CreateSegmentType_TextAnnotation_CollectionType; // FTT, check this
	this->SetTextAnnotation(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Semantic"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Semantic")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SemanticRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SemanticRef")) == 0)
	)
  {
	Mp7JrsSegmentType_CollectionPtr tmp = CreateSegmentType_CollectionType; // FTT, check this
	this->SetSegmentType_LocalType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MatchingHint")) == 0))
  {
	Mp7JrsSegmentType_MatchingHint_CollectionPtr tmp = CreateSegmentType_MatchingHint_CollectionType; // FTT, check this
	this->SetMatchingHint(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PointOfView")) == 0))
  {
	Mp7JrsSegmentType_PointOfView_CollectionPtr tmp = CreateSegmentType_PointOfView_CollectionType; // FTT, check this
	this->SetPointOfView(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relation")) == 0))
  {
	Mp7JrsSegmentType_Relation_CollectionPtr tmp = CreateSegmentType_Relation_CollectionType; // FTT, check this
	this->SetRelation(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSegmentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSegmentType_ExtMethodImpl.h


