
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsUserActionListType_UserAction_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUserActionListType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsUserActionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UserAction

#include <assert.h>
IMp7JrsUserActionListType::IMp7JrsUserActionListType()
{

// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtPropInit.h

}

IMp7JrsUserActionListType::~IMp7JrsUserActionListType()
{
// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtPropCleanup.h

}

Mp7JrsUserActionListType::Mp7JrsUserActionListType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserActionListType::~Mp7JrsUserActionListType()
{
	Cleanup();
}

void Mp7JrsUserActionListType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_numOfInstances = 0; // Value
	m_numOfInstances_Exist = false;
	m_totalDuration = Mp7JrsdurationPtr(); // Pattern
	m_totalDuration_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_ActionType = Mp7JrsTermUsePtr(); // Class
	m_UserAction = Mp7JrsUserActionListType_UserAction_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtMyPropInit.h

}

void Mp7JrsUserActionListType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_totalDuration); // Pattern
	// Dc1Factory::DeleteObject(m_ActionType);
	// Dc1Factory::DeleteObject(m_UserAction);
}

void Mp7JrsUserActionListType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserActionListTypePtr, since we
	// might need GetBase(), which isn't defined in IUserActionListType
	const Dc1Ptr< Mp7JrsUserActionListType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUserActionListType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistnumOfInstances())
	{
		this->SetnumOfInstances(tmp->GetnumOfInstances());
	}
	else
	{
		InvalidatenumOfInstances();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_totalDuration); // Pattern
	if (tmp->ExisttotalDuration())
	{
		this->SettotalDuration(Dc1Factory::CloneObject(tmp->GettotalDuration()));
	}
	else
	{
		InvalidatetotalDuration();
	}
	}
		// Dc1Factory::DeleteObject(m_ActionType);
		this->SetActionType(Dc1Factory::CloneObject(tmp->GetActionType()));
		// Dc1Factory::DeleteObject(m_UserAction);
		this->SetUserAction(Dc1Factory::CloneObject(tmp->GetUserAction()));
}

Dc1NodePtr Mp7JrsUserActionListType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUserActionListType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsUserActionListType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsUserActionListType::GetnumOfInstances() const
{
	return m_numOfInstances;
}

bool Mp7JrsUserActionListType::ExistnumOfInstances() const
{
	return m_numOfInstances_Exist;
}
void Mp7JrsUserActionListType::SetnumOfInstances(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetnumOfInstances().");
	}
	m_numOfInstances = item;
	m_numOfInstances_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatenumOfInstances()
{
	m_numOfInstances_Exist = false;
}
Mp7JrsdurationPtr Mp7JrsUserActionListType::GettotalDuration() const
{
	return m_totalDuration;
}

bool Mp7JrsUserActionListType::ExisttotalDuration() const
{
	return m_totalDuration_Exist;
}
void Mp7JrsUserActionListType::SettotalDuration(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SettotalDuration().");
	}
	m_totalDuration = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_totalDuration) m_totalDuration->SetParent(m_myself.getPointer());
	m_totalDuration_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatetotalDuration()
{
	m_totalDuration_Exist = false;
}
Mp7JrsTermUsePtr Mp7JrsUserActionListType::GetActionType() const
{
		return m_ActionType;
}

Mp7JrsUserActionListType_UserAction_CollectionPtr Mp7JrsUserActionListType::GetUserAction() const
{
		return m_UserAction;
}

void Mp7JrsUserActionListType::SetActionType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetActionType().");
	}
	if (m_ActionType != item)
	{
		// Dc1Factory::DeleteObject(m_ActionType);
		m_ActionType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ActionType) m_ActionType->SetParent(m_myself.getPointer());
		if (m_ActionType && m_ActionType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ActionType->UseTypeAttribute = true;
		}
		if(m_ActionType != Mp7JrsTermUsePtr())
		{
			m_ActionType->SetContentName(XMLString::transcode("ActionType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::SetUserAction(const Mp7JrsUserActionListType_UserAction_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetUserAction().");
	}
	if (m_UserAction != item)
	{
		// Dc1Factory::DeleteObject(m_UserAction);
		m_UserAction = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserAction) m_UserAction->SetParent(m_myself.getPointer());
		if(m_UserAction != Mp7JrsUserActionListType_UserAction_CollectionPtr())
		{
			m_UserAction->SetContentName(XMLString::transcode("UserAction"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUserActionListType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsUserActionListType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsUserActionListType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUserActionListType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsUserActionListType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsUserActionListType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUserActionListType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsUserActionListType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsUserActionListType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUserActionListType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUserActionListType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUserActionListType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUserActionListType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUserActionListType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUserActionListType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionListType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUserActionListType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsUserActionListType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionListType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUserActionListType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfInstances")) == 0)
	{
		// numOfInstances is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("totalDuration")) == 0)
	{
		// totalDuration is simple attribute durationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsdurationPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ActionType")) == 0)
	{
		// ActionType is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetActionType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetActionType(p, client);
					if((p = GetActionType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserAction")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UserAction is item of type UserActionType
		// in element collection UserActionListType_UserAction_CollectionType
		
		context->Found = true;
		Mp7JrsUserActionListType_UserAction_CollectionPtr coll = GetUserAction();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserActionListType_UserAction_CollectionType; // FTT, check this
				SetUserAction(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserActionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserActionListType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserActionListType");
	}
	return result;
}

XMLCh * Mp7JrsUserActionListType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUserActionListType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUserActionListType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserActionListType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_numOfInstances_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_numOfInstances);
		element->setAttributeNS(X(""), X("numOfInstances"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_totalDuration_Exist)
	{
	// Pattern
	if (m_totalDuration != Mp7JrsdurationPtr())
	{
		XMLCh * tmp = m_totalDuration->ToText();
		element->setAttributeNS(X(""), X("totalDuration"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_ActionType != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ActionType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ActionType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ActionType->Serialize(doc, element, &elem);
	}
	if (m_UserAction != Mp7JrsUserActionListType_UserAction_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UserAction->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUserActionListType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfInstances")))
	{
		// deserialize value type
		this->SetnumOfInstances(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("numOfInstances"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("totalDuration")))
	{
		Mp7JrsdurationPtr tmp = CreatedurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("totalDuration")));
		this->SettotalDuration(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ActionType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ActionType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetActionType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UserAction"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UserAction")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserActionListType_UserAction_CollectionPtr tmp = CreateUserActionListType_UserAction_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUserAction(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserActionListType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ActionType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetActionType(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UserAction")) == 0))
  {
	Mp7JrsUserActionListType_UserAction_CollectionPtr tmp = CreateUserActionListType_UserAction_CollectionType; // FTT, check this
	this->SetUserAction(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserActionListType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetActionType() != Dc1NodePtr())
		result.Insert(GetActionType());
	if (GetUserAction() != Dc1NodePtr())
		result.Insert(GetUserAction());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserActionListType_ExtMethodImpl.h


