
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtImplInclude.h


#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsTextAnnotationType_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextAnnotationType_LocalType.h" // Choice collection FreeTextAnnotation
#include "Mp7JrsTextualType.h" // Choice collection element FreeTextAnnotation
#include "Mp7JrsStructuredAnnotationType.h" // Choice collection element StructuredAnnotation
#include "Mp7JrsDependencyStructureType.h" // Choice collection element DependencyStructure
#include "Mp7JrsKeywordAnnotationType.h" // Choice collection element KeywordAnnotation

#include <assert.h>
IMp7JrsTextAnnotationType::IMp7JrsTextAnnotationType()
{

// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtPropInit.h

}

IMp7JrsTextAnnotationType::~IMp7JrsTextAnnotationType()
{
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtPropCleanup.h

}

Mp7JrsTextAnnotationType::Mp7JrsTextAnnotationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextAnnotationType::~Mp7JrsTextAnnotationType()
{
	Cleanup();
}

void Mp7JrsTextAnnotationType::Init()
{

	// Init attributes
	m_relevance = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_relevance->SetContent(0.0); // Use Value initialiser (xxx2)
	m_relevance_Exist = false;
	m_confidence = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_confidence->SetContent(0.0); // Use Value initialiser (xxx2)
	m_confidence_Exist = false;
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_TextAnnotationType_LocalType = Mp7JrsTextAnnotationType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtMyPropInit.h

}

void Mp7JrsTextAnnotationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_relevance);
	// Dc1Factory::DeleteObject(m_confidence);
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_TextAnnotationType_LocalType);
}

void Mp7JrsTextAnnotationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextAnnotationTypePtr, since we
	// might need GetBase(), which isn't defined in ITextAnnotationType
	const Dc1Ptr< Mp7JrsTextAnnotationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_relevance);
	if (tmp->Existrelevance())
	{
		this->Setrelevance(Dc1Factory::CloneObject(tmp->Getrelevance()));
	}
	else
	{
		Invalidaterelevance();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_confidence);
	if (tmp->Existconfidence())
	{
		this->Setconfidence(Dc1Factory::CloneObject(tmp->Getconfidence()));
	}
	else
	{
		Invalidateconfidence();
	}
	}
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_TextAnnotationType_LocalType);
		this->SetTextAnnotationType_LocalType(Dc1Factory::CloneObject(tmp->GetTextAnnotationType_LocalType()));
}

Dc1NodePtr Mp7JrsTextAnnotationType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTextAnnotationType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrszeroToOnePtr Mp7JrsTextAnnotationType::Getrelevance() const
{
	return m_relevance;
}

bool Mp7JrsTextAnnotationType::Existrelevance() const
{
	return m_relevance_Exist;
}
void Mp7JrsTextAnnotationType::Setrelevance(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType::Setrelevance().");
	}
	m_relevance = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_relevance) m_relevance->SetParent(m_myself.getPointer());
	m_relevance_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextAnnotationType::Invalidaterelevance()
{
	m_relevance_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsTextAnnotationType::Getconfidence() const
{
	return m_confidence;
}

bool Mp7JrsTextAnnotationType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsTextAnnotationType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType::Setconfidence().");
	}
	m_confidence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_confidence) m_confidence->SetParent(m_myself.getPointer());
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextAnnotationType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
XMLCh * Mp7JrsTextAnnotationType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsTextAnnotationType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsTextAnnotationType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextAnnotationType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsTextAnnotationType_CollectionPtr Mp7JrsTextAnnotationType::GetTextAnnotationType_LocalType() const
{
		return m_TextAnnotationType_LocalType;
}

void Mp7JrsTextAnnotationType::SetTextAnnotationType_LocalType(const Mp7JrsTextAnnotationType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextAnnotationType::SetTextAnnotationType_LocalType().");
	}
	if (m_TextAnnotationType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_TextAnnotationType_LocalType);
		m_TextAnnotationType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextAnnotationType_LocalType) m_TextAnnotationType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTextAnnotationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("relevance")) == 0)
	{
		// relevance is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FreeTextAnnotation")) == 0)
	{
		// FreeTextAnnotation is contained in itemtype TextAnnotationType_LocalType
		// in choice collection TextAnnotationType_CollectionType

		context->Found = true;
		Mp7JrsTextAnnotationType_CollectionPtr coll = GetTextAnnotationType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextAnnotationType_CollectionType; // FTT, check this
				SetTextAnnotationType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetFreeTextAnnotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextAnnotationType_LocalPtr item = CreateTextAnnotationType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->SetFreeTextAnnotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetFreeTextAnnotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredAnnotation")) == 0)
	{
		// StructuredAnnotation is contained in itemtype TextAnnotationType_LocalType
		// in choice collection TextAnnotationType_CollectionType

		context->Found = true;
		Mp7JrsTextAnnotationType_CollectionPtr coll = GetTextAnnotationType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextAnnotationType_CollectionType; // FTT, check this
				SetTextAnnotationType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetStructuredAnnotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextAnnotationType_LocalPtr item = CreateTextAnnotationType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStructuredAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->SetStructuredAnnotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetStructuredAnnotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DependencyStructure")) == 0)
	{
		// DependencyStructure is contained in itemtype TextAnnotationType_LocalType
		// in choice collection TextAnnotationType_CollectionType

		context->Found = true;
		Mp7JrsTextAnnotationType_CollectionPtr coll = GetTextAnnotationType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextAnnotationType_CollectionType; // FTT, check this
				SetTextAnnotationType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetDependencyStructure()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextAnnotationType_LocalPtr item = CreateTextAnnotationType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->SetDependencyStructure(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetDependencyStructure()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("KeywordAnnotation")) == 0)
	{
		// KeywordAnnotation is contained in itemtype TextAnnotationType_LocalType
		// in choice collection TextAnnotationType_CollectionType

		context->Found = true;
		Mp7JrsTextAnnotationType_CollectionPtr coll = GetTextAnnotationType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextAnnotationType_CollectionType; // FTT, check this
				SetTextAnnotationType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetKeywordAnnotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsTextAnnotationType_LocalPtr item = CreateTextAnnotationType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsKeywordAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->SetKeywordAnnotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsTextAnnotationType_LocalPtr)coll->elementAt(i))->GetKeywordAnnotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TextAnnotationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TextAnnotationType");
	}
	return result;
}

XMLCh * Mp7JrsTextAnnotationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTextAnnotationType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTextAnnotationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextAnnotationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_relevance_Exist)
	{
	// Class
	if (m_relevance != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_relevance->ToText();
		element->setAttributeNS(X(""), X("relevance"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Class
	if (m_confidence != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_confidence->ToText();
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_TextAnnotationType_LocalType != Mp7JrsTextAnnotationType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TextAnnotationType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsTextAnnotationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("relevance")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("relevance")));
		this->Setrelevance(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("confidence")));
		this->Setconfidence(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:FreeTextAnnotation
			Dc1Util::HasNodeName(parent, X("FreeTextAnnotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FreeTextAnnotation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:StructuredAnnotation
			Dc1Util::HasNodeName(parent, X("StructuredAnnotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StructuredAnnotation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:DependencyStructure
			Dc1Util::HasNodeName(parent, X("DependencyStructure"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:DependencyStructure")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:KeywordAnnotation
			Dc1Util::HasNodeName(parent, X("KeywordAnnotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:KeywordAnnotation")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsTextAnnotationType_CollectionPtr tmp = CreateTextAnnotationType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTextAnnotationType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextAnnotationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FreeTextAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FreeTextAnnotation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("StructuredAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("StructuredAnnotation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("DependencyStructure"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("DependencyStructure")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("KeywordAnnotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("KeywordAnnotation")) == 0)
	)
  {
	Mp7JrsTextAnnotationType_CollectionPtr tmp = CreateTextAnnotationType_CollectionType; // FTT, check this
	this->SetTextAnnotationType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextAnnotationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTextAnnotationType_LocalType() != Dc1NodePtr())
		result.Insert(GetTextAnnotationType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextAnnotationType_ExtMethodImpl.h


