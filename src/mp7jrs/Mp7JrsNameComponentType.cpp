
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtImplInclude.h


#include "Mp7JrsTextualBaseType.h"
#include "Mp7JrsTextualBaseType_phoneticTranscription_CollectionType.h"
#include "Mp7JrsNameComponentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsNameComponentType::IMp7JrsNameComponentType()
{

// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtPropInit.h

}

IMp7JrsNameComponentType::~IMp7JrsNameComponentType()
{
// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtPropCleanup.h

}

Mp7JrsNameComponentType::Mp7JrsNameComponentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsNameComponentType::~Mp7JrsNameComponentType()
{
	Cleanup();
}

void Mp7JrsNameComponentType::Init()
{
	// Init base
	m_Base = CreateTextualBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_initial = NULL; // String
	m_initial_Exist = false;
	m_abbrev = NULL; // String
	m_abbrev_Exist = false;



// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtMyPropInit.h

}

void Mp7JrsNameComponentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_initial); // String
	XMLString::release(&m_abbrev); // String
}

void Mp7JrsNameComponentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use NameComponentTypePtr, since we
	// might need GetBase(), which isn't defined in INameComponentType
	const Dc1Ptr< Mp7JrsNameComponentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTextualBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsNameComponentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_initial); // String
	if (tmp->Existinitial())
	{
		this->Setinitial(XMLString::replicate(tmp->Getinitial()));
	}
	else
	{
		Invalidateinitial();
	}
	}
	{
	XMLString::release(&m_abbrev); // String
	if (tmp->Existabbrev())
	{
		this->Setabbrev(XMLString::replicate(tmp->Getabbrev()));
	}
	else
	{
		Invalidateabbrev();
	}
	}
}

Dc1NodePtr Mp7JrsNameComponentType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsNameComponentType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsTextualBaseType > Mp7JrsNameComponentType::GetBase() const
{
	return m_Base;
}

void Mp7JrsNameComponentType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsNameComponentType::GetContent() const
{
	return GetBase()->GetContent();
}
XMLCh * Mp7JrsNameComponentType::Getinitial() const
{
	return m_initial;
}

bool Mp7JrsNameComponentType::Existinitial() const
{
	return m_initial_Exist;
}
void Mp7JrsNameComponentType::Setinitial(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setinitial().");
	}
	m_initial = item;
	m_initial_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidateinitial()
{
	m_initial_Exist = false;
}
XMLCh * Mp7JrsNameComponentType::Getabbrev() const
{
	return m_abbrev;
}

bool Mp7JrsNameComponentType::Existabbrev() const
{
	return m_abbrev_Exist;
}
void Mp7JrsNameComponentType::Setabbrev(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setabbrev().");
	}
	m_abbrev = item;
	m_abbrev_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidateabbrev()
{
	m_abbrev_Exist = false;
}
XMLCh * Mp7JrsNameComponentType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsNameComponentType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsNameComponentType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr Mp7JrsNameComponentType::GetphoneticTranscription() const
{
	return GetBase()->GetphoneticTranscription();
}

bool Mp7JrsNameComponentType::ExistphoneticTranscription() const
{
	return GetBase()->ExistphoneticTranscription();
}
void Mp7JrsNameComponentType::SetphoneticTranscription(const Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::SetphoneticTranscription().");
	}
	GetBase()->SetphoneticTranscription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::InvalidatephoneticTranscription()
{
	GetBase()->InvalidatephoneticTranscription();
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsNameComponentType::GetphoneticAlphabet() const
{
	return GetBase()->GetphoneticAlphabet();
}

bool Mp7JrsNameComponentType::ExistphoneticAlphabet() const
{
	return GetBase()->ExistphoneticAlphabet();
}
void Mp7JrsNameComponentType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::SetphoneticAlphabet().");
	}
	GetBase()->SetphoneticAlphabet(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::InvalidatephoneticAlphabet()
{
	GetBase()->InvalidatephoneticAlphabet();
}
XMLCh * Mp7JrsNameComponentType::Getcharset() const
{
	return GetBase()->Getcharset();
}

bool Mp7JrsNameComponentType::Existcharset() const
{
	return GetBase()->Existcharset();
}
void Mp7JrsNameComponentType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setcharset().");
	}
	GetBase()->Setcharset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidatecharset()
{
	GetBase()->Invalidatecharset();
}
XMLCh * Mp7JrsNameComponentType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool Mp7JrsNameComponentType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void Mp7JrsNameComponentType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
XMLCh * Mp7JrsNameComponentType::Getscript() const
{
	return GetBase()->Getscript();
}

bool Mp7JrsNameComponentType::Existscript() const
{
	return GetBase()->Existscript();
}
void Mp7JrsNameComponentType::Setscript(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsNameComponentType::Setscript().");
	}
	GetBase()->Setscript(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsNameComponentType::Invalidatescript()
{
	GetBase()->Invalidatescript();
}

Dc1NodeEnum Mp7JrsNameComponentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("initial")) == 0)
	{
		// initial is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("abbrev")) == 0)
	{
		// abbrev is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for NameComponentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "NameComponentType");
	}
	return result;
}

XMLCh * Mp7JrsNameComponentType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsNameComponentType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsNameComponentType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsNameComponentType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsNameComponentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("NameComponentType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_initial_Exist)
	{
	// String
	if(m_initial != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("initial"), m_initial);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_abbrev_Exist)
	{
	// String
	if(m_abbrev != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("abbrev"), m_abbrev);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsNameComponentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("initial")))
	{
		// Deserialize string type
		this->Setinitial(Dc1Convert::TextToString(parent->getAttribute(X("initial"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("abbrev")))
	{
		// Deserialize string type
		this->Setabbrev(Dc1Convert::TextToString(parent->getAttribute(X("abbrev"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsTextualBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsNameComponentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsNameComponentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsNameComponentType_ExtMethodImpl.h


