
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// begin extension included
// file Mp7JrsControlledTermUseType_ExtImplInclude.h

#include "Mp7JrstermAliasReferenceType.h"
#include "Mp7JrstermURIReferenceType.h"
// end extension included


#include "Mp7JrsInlineTermDefinitionType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrsInlineTermDefinitionType_Name_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Definition_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsInlineTermDefinitionType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition
#include "Mp7JrsInlineTermDefinitionType_Term_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Term

#include <assert.h>
IMp7JrsControlledTermUseType::IMp7JrsControlledTermUseType()
{

// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtPropInit.h

}

IMp7JrsControlledTermUseType::~IMp7JrsControlledTermUseType()
{
// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtPropCleanup.h

}

Mp7JrsControlledTermUseType::Mp7JrsControlledTermUseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsControlledTermUseType::~Mp7JrsControlledTermUseType()
{
	Cleanup();
}

void Mp7JrsControlledTermUseType::Init()
{
	// Init base
	m_Base = CreateInlineTermDefinitionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_href = Mp7JrstermReferencePtr(); // Create emtpy class ptr



// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtMyPropInit.h

}

void Mp7JrsControlledTermUseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_href);
}

void Mp7JrsControlledTermUseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ControlledTermUseTypePtr, since we
	// might need GetBase(), which isn't defined in IControlledTermUseType
	const Dc1Ptr< Mp7JrsControlledTermUseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsInlineTermDefinitionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsControlledTermUseType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_href);
		this->Sethref(Dc1Factory::CloneObject(tmp->Gethref()));
	}
}

Dc1NodePtr Mp7JrsControlledTermUseType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsControlledTermUseType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsInlineTermDefinitionType > Mp7JrsControlledTermUseType::GetBase() const
{
	return m_Base;
}

Mp7JrstermReferencePtr Mp7JrsControlledTermUseType::Gethref() const
{
	return m_href;
}

void Mp7JrsControlledTermUseType::Sethref(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsControlledTermUseType::Sethref().");
	}
	m_href = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_href) m_href->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsInlineTermDefinitionType_Name_CollectionPtr Mp7JrsControlledTermUseType::GetName() const
{
	return GetBase()->GetName();
}

Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr Mp7JrsControlledTermUseType::GetDefinition() const
{
	return GetBase()->GetDefinition();
}

Mp7JrsInlineTermDefinitionType_Term_CollectionPtr Mp7JrsControlledTermUseType::GetTerm() const
{
	return GetBase()->GetTerm();
}

void Mp7JrsControlledTermUseType::SetName(const Mp7JrsInlineTermDefinitionType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsControlledTermUseType::SetName().");
	}
	GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsControlledTermUseType::SetDefinition(const Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsControlledTermUseType::SetDefinition().");
	}
	GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsControlledTermUseType::SetTerm(const Mp7JrsInlineTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsControlledTermUseType::SetTerm().");
	}
	GetBase()->SetTerm(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsControlledTermUseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("href")) == 0)
	{
		// href is simple attribute termReferenceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstermReferencePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ControlledTermUseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ControlledTermUseType");
	}
	return result;
}

XMLCh * Mp7JrsControlledTermUseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsControlledTermUseType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsControlledTermUseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ControlledTermUseType"));
	// Attribute Serialization:
	// Class
	if (m_href != Mp7JrstermReferencePtr())
	{
		XMLCh * tmp = m_href->ToText();
		element->setAttributeNS(X(""), X("href"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsControlledTermUseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("href")))
	{
		// Deserialize class type
		Mp7JrstermReferencePtr tmp = CreatetermReferenceType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("href")));
		this->Sethref(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsInlineTermDefinitionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsControlledTermUseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsControlledTermUseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsControlledTermUseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsControlledTermUseType_ExtMethodImpl.h


void IMp7JrsControlledTermUseType::SetTermReferenceURN(XMLCh* urn, XMLCh* term, XMLCh* name, Dc1ClientID client) 
{
	Mp7JrstermReferencePtr href = CreatetermReferenceType;
	Sethref(href,client);

	//FTT TODO This has to be tested
	Mp7JrstermAliasReferencePtr a = CreatetermAliasReferenceType;
	href->SettermAliasReferenceType(a);

	XMLCh* buf = new XMLCh[XMLString::stringLen(urn)+XMLString::stringLen(term)+3];
	XMLString::copyString(buf,urn);
	XMLString::catString(buf,X(":")); // FTT L":" doesn't work on 32bit unicode systems (like mac)
	XMLString::catString(buf,term);

	a->SetContent(XMLString::replicate(buf), client);

	delete [] buf;

	XMLString::release(&urn);
	XMLString::release(&term);

	if (name) SetTermName(name,client);
}
	
void IMp7JrsControlledTermUseType::SetTermReferenceAnyURI(XMLCh* uri, XMLCh* term, XMLCh* name, Dc1ClientID client) 
{
	Mp7JrstermReferencePtr href = CreatetermReferenceType;
	Sethref(href,client);

	//FTT TODO This has to be tested
	Mp7JrstermURIReferencePtr u = CreatetermURIReferenceType;
	href->SettermURIReferenceType(u);

	XMLCh* buf = new XMLCh[XMLString::stringLen(uri)+XMLString::stringLen(term)+3];
	XMLString::copyString(buf,uri);
	XMLString::catString(buf,X("/")); // FTT L"/" doesn't work on 32bit unicode systems (like mac)
	XMLString::catString(buf,term);

	u->SetContent(XMLString::replicate(buf),client);

	delete [] buf;

	XMLString::release(&uri);
	XMLString::release(&term);

	if (name) SetTermName(name,client);
}

void IMp7JrsControlledTermUseType::GetTermReference(XMLCh** uri, XMLCh** term) 
{
	//FTT TODO This has to be tested
	Mp7JrstermReferencePtr href = Gethref();
	if (!href) return;

	Mp7JrstermAliasReferencePtr hrefPattern = href->GettermAliasReferenceType();

	XMLCh* colon = XMLString::transcode(":");

	// URN case
	if (hrefPattern) {
		XMLCh* hrefPat = hrefPattern->GetContent();

		XMLCh* uriBuf = XMLString::replicate(hrefPat);

		int lastColon = XMLString::lastIndexOf(uriBuf,colon[0]); // FTT L doesn't work on mac
		XMLString::copyNString (uriBuf,hrefPat,lastColon); 

		XMLCh* termBuf = XMLString::replicate(hrefPat);
		hrefPat+=lastColon+1;
		XMLString::copyNString (termBuf,hrefPat,XMLString::stringLen(hrefPat));

		*uri = uriBuf;
		*term = termBuf;
	}
	// anyURI case
	else {

		Mp7JrstermURIReferencePtr hrefURI = href->GettermURIReferenceType();
		XMLCh* uriStr = hrefURI->GetContent();

		XMLCh* uriBuf = XMLString::replicate(uriStr);

		int lastSlash = XMLString::lastIndexOf(uriBuf,colon[0]); // FTT L doesn't work on mac
		XMLString::copyNString (uriBuf,uriStr,lastSlash); 

		XMLCh* termBuf = XMLString::replicate(uriStr);
		uriStr+=lastSlash+1;
		XMLString::copyNString (termBuf,uriStr,XMLString::stringLen(uriStr));

		*uri = uriBuf;
		*term = termBuf;
	}

	XMLString::release(&colon);
}

	
void IMp7JrsControlledTermUseType::SetTermName(XMLCh* name, Dc1ClientID client) 
{
	Mp7JrsInlineTermDefinitionType_Name_CollectionPtr nameColl = CreateInlineTermDefinitionType_Name_CollectionType;
	Mp7JrsInlineTermDefinitionType_Name_LocalPtr nameElem = CreateInlineTermDefinitionType_Name_LocalType;
	nameColl->addElement(nameElem,client);
	SetName(nameColl,client);

	nameElem->SetContent(name,client); 
	
}
// end extension included


