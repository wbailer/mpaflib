
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtImplInclude.h


#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionType.h"
#include "Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType.h" // Sequence collection SubUnit
#include "Mp7JrsMediaLocatorType.h" // Sequence collection element SubUnit

#include <assert.h>
IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtPropInit.h

}

IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::~IMp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtPropCleanup.h

}

Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::~Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0()
{
	Cleanup();
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Init()
{

	// Init attributes
	m_idref = NULL; // String
	m_idref_Exist = false;
	m_xpath = Mp7JrsxPathRefPtr(); // Pattern
	m_xpath_Exist = false;
	m_href = NULL; // String
	m_href_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_LogicalUnitLocatorType_ReferenceUnit_LocalType = Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtMyPropInit.h

}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtMyPropCleanup.h


	XMLString::release(&m_idref); // String
	// Dc1Factory::DeleteObject(m_xpath); // Pattern
	XMLString::release(&m_href); // String
	// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_ReferenceUnit_LocalType);
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use LogicalUnitLocatorType_ReferenceUnit_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in ILogicalUnitLocatorType_ReferenceUnit_LocalType0
	const Dc1Ptr< Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_idref); // String
	if (tmp->Existidref())
	{
		this->Setidref(XMLString::replicate(tmp->Getidref()));
	}
	else
	{
		Invalidateidref();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_xpath); // Pattern
	if (tmp->Existxpath())
	{
		this->Setxpath(Dc1Factory::CloneObject(tmp->Getxpath()));
	}
	else
	{
		Invalidatexpath();
	}
	}
	{
	XMLString::release(&m_href); // String
	if (tmp->Existhref())
	{
		this->Sethref(XMLString::replicate(tmp->Gethref()));
	}
	else
	{
		Invalidatehref();
	}
	}
		// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_ReferenceUnit_LocalType);
		this->SetLogicalUnitLocatorType_ReferenceUnit_LocalType(Dc1Factory::CloneObject(tmp->GetLogicalUnitLocatorType_ReferenceUnit_LocalType()));
}

Dc1NodePtr Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Getidref() const
{
	return m_idref;
}

bool Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Existidref() const
{
	return m_idref_Exist;
}
void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Setidref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Setidref().");
	}
	m_idref = item;
	m_idref_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Invalidateidref()
{
	m_idref_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Getxpath() const
{
	return m_xpath;
}

bool Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Existxpath() const
{
	return m_xpath_Exist;
}
void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Setxpath(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Setxpath().");
	}
	m_xpath = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_xpath) m_xpath->SetParent(m_myself.getPointer());
	m_xpath_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Invalidatexpath()
{
	m_xpath_Exist = false;
}
XMLCh * Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Gethref() const
{
	return m_href;
}

bool Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Existhref() const
{
	return m_href_Exist;
}
void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Sethref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Sethref().");
	}
	m_href = item;
	m_href_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Invalidatehref()
{
	m_href_Exist = false;
}
Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::GetLogicalUnitLocatorType_ReferenceUnit_LocalType() const
{
		return m_LogicalUnitLocatorType_ReferenceUnit_LocalType;
}

void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::SetLogicalUnitLocatorType_ReferenceUnit_LocalType(const Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::SetLogicalUnitLocatorType_ReferenceUnit_LocalType().");
	}
	if (m_LogicalUnitLocatorType_ReferenceUnit_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_LogicalUnitLocatorType_ReferenceUnit_LocalType);
		m_LogicalUnitLocatorType_ReferenceUnit_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LogicalUnitLocatorType_ReferenceUnit_LocalType) m_LogicalUnitLocatorType_ReferenceUnit_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("idref")) == 0)
	{
		// idref is simple attribute IDREF
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("xpath")) == 0)
	{
		// xpath is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("href")) == 0)
	{
		// href is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("SubUnit")) == 0)
	{
		// SubUnit is contained in itemtype LogicalUnitLocatorType_ReferenceUnit_LocalType
		// in sequence collection LogicalUnitLocatorType_ReferenceUnit_CollectionType

		context->Found = true;
		Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr coll = GetLogicalUnitLocatorType_ReferenceUnit_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateLogicalUnitLocatorType_ReferenceUnit_CollectionType; // FTT, check this
				SetLogicalUnitLocatorType_ReferenceUnit_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(i))->GetSubUnit()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr item = CreateLogicalUnitLocatorType_ReferenceUnit_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(j))->SetSubUnit(
						((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(j+1))->GetSubUnit());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(j))->SetSubUnit(
						((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(j-1))->GetSubUnit());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(i))->SetSubUnit(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalPtr)coll->elementAt(i))->GetSubUnit()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for LogicalUnitLocatorType_ReferenceUnit_LocalType0
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "LogicalUnitLocatorType_ReferenceUnit_LocalType0");
	}
	return result;
}

XMLCh * Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("LogicalUnitLocatorType_ReferenceUnit_LocalType0"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_idref_Exist)
	{
	// String
	if(m_idref != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("idref"), m_idref);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_xpath_Exist)
	{
	// Pattern
	if (m_xpath != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_xpath->ToText();
		element->setAttributeNS(X(""), X("xpath"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_href_Exist)
	{
	// String
	if(m_href != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("href"), m_href);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_LogicalUnitLocatorType_ReferenceUnit_LocalType != Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_LogicalUnitLocatorType_ReferenceUnit_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("idref")))
	{
		// Deserialize string type
		this->Setidref(Dc1Convert::TextToString(parent->getAttribute(X("idref"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xpath")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("xpath")));
		this->Setxpath(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("href")))
	{
		// Deserialize string type
		this->Sethref(Dc1Convert::TextToString(parent->getAttribute(X("href"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SubUnit")) == 0)
			(Dc1Util::HasNodeName(parent, X("SubUnit"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr tmp = CreateLogicalUnitLocatorType_ReferenceUnit_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLogicalUnitLocatorType_ReferenceUnit_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsLogicalUnitLocatorType_ReferenceUnit_CollectionPtr tmp = CreateLogicalUnitLocatorType_ReferenceUnit_CollectionType; // FTT, check this
	this->SetLogicalUnitLocatorType_ReferenceUnit_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLogicalUnitLocatorType_ReferenceUnit_LocalType() != Dc1NodePtr())
		result.Insert(GetLogicalUnitLocatorType_ReferenceUnit_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsLogicalUnitLocatorType_ReferenceUnit_LocalType0_ExtMethodImpl.h


