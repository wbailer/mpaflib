
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtImplInclude.h


#include "Mp7JrsIntegerMatrixType.h"
#include "Mp7JrsFloatMatrixType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsDType.h"
#include "Mp7JrsSemanticStateType_AttributeValuePair_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSemanticStateType_AttributeValuePair_LocalType::IMp7JrsSemanticStateType_AttributeValuePair_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtPropInit.h

}

IMp7JrsSemanticStateType_AttributeValuePair_LocalType::~IMp7JrsSemanticStateType_AttributeValuePair_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtPropCleanup.h

}

Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Mp7JrsSemanticStateType_AttributeValuePair_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticStateType_AttributeValuePair_LocalType::~Mp7JrsSemanticStateType_AttributeValuePair_LocalType()
{
	Cleanup();
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_BooleanValue = (false); // Value

	m_IntegerValue = (0); // Value

	m_FloatValue = (0.0f); // Value

	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr(); // Class
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr(); // Class
	m_TextValue = Mp7JrsTextualPtr(); // Class
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr(); // Class
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr(); // Class
	m_DescriptorValue = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMyPropInit.h

}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	// Dc1Factory::DeleteObject(m_TextValue);
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	// Dc1Factory::DeleteObject(m_DescriptorValue);
}

void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticStateType_AttributeValuePair_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticStateType_AttributeValuePair_LocalType
	const Dc1Ptr< Mp7JrsSemanticStateType_AttributeValuePair_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetBooleanValue(tmp->GetBooleanValue());
		this->SetIntegerValue(tmp->GetIntegerValue());
		this->SetFloatValue(tmp->GetFloatValue());
		// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
		this->SetIntegerMatrixValue(Dc1Factory::CloneObject(tmp->GetIntegerMatrixValue()));
		// Dc1Factory::DeleteObject(m_FloatMatrixValue);
		this->SetFloatMatrixValue(Dc1Factory::CloneObject(tmp->GetFloatMatrixValue()));
		// Dc1Factory::DeleteObject(m_TextValue);
		this->SetTextValue(Dc1Factory::CloneObject(tmp->GetTextValue()));
		// Dc1Factory::DeleteObject(m_TextAnnotationValue);
		this->SetTextAnnotationValue(Dc1Factory::CloneObject(tmp->GetTextAnnotationValue()));
		// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
		this->SetControlledTermUseValue(Dc1Factory::CloneObject(tmp->GetControlledTermUseValue()));
		// Dc1Factory::DeleteObject(m_DescriptorValue);
		this->SetDescriptorValue(Dc1Factory::CloneObject(tmp->GetDescriptorValue()));
}

Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetBooleanValue() const
{
		return m_BooleanValue;
}

int Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetIntegerValue() const
{
		return m_IntegerValue;
}

float Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetFloatValue() const
{
		return m_FloatValue;
}

Mp7JrsIntegerMatrixPtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetIntegerMatrixValue() const
{
		return m_IntegerMatrixValue;
}

Mp7JrsFloatMatrixPtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetFloatMatrixValue() const
{
		return m_FloatMatrixValue;
}

Mp7JrsTextualPtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetTextValue() const
{
		return m_TextValue;
}

Mp7JrsTextAnnotationPtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetTextAnnotationValue() const
{
		return m_TextAnnotationValue;
}

Mp7JrsControlledTermUsePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetControlledTermUseValue() const
{
		return m_ControlledTermUseValue;
}

Dc1Ptr< Dc1Node > Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetDescriptorValue() const
{
		return m_DescriptorValue;
}

// implementing setter for choice 
/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetBooleanValue(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetBooleanValue().");
	}
	if (m_BooleanValue != item)
	{
		m_BooleanValue = item;
		m_BooleanValue_Valid = true;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetIntegerValue(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetIntegerValue().");
	}
	if (m_IntegerValue != item)
	{
		m_IntegerValue = item;
		m_IntegerValue_Valid = true;
	
	m_BooleanValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetFloatValue(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetFloatValue().");
	}
	if (m_FloatValue != item)
	{
		m_FloatValue = item;
		m_FloatValue_Valid = true;
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetIntegerMatrixValue(const Mp7JrsIntegerMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetIntegerMatrixValue().");
	}
	if (m_IntegerMatrixValue != item)
	{
		// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
		m_IntegerMatrixValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IntegerMatrixValue) m_IntegerMatrixValue->SetParent(m_myself.getPointer());
		if (m_IntegerMatrixValue && m_IntegerMatrixValue->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IntegerMatrixValue->UseTypeAttribute = true;
		}
		if(m_IntegerMatrixValue != Mp7JrsIntegerMatrixPtr())
		{
			m_IntegerMatrixValue->SetContentName(XMLString::transcode("IntegerMatrixValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetFloatMatrixValue(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetFloatMatrixValue().");
	}
	if (m_FloatMatrixValue != item)
	{
		// Dc1Factory::DeleteObject(m_FloatMatrixValue);
		m_FloatMatrixValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FloatMatrixValue) m_FloatMatrixValue->SetParent(m_myself.getPointer());
		if (m_FloatMatrixValue && m_FloatMatrixValue->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FloatMatrixValue->UseTypeAttribute = true;
		}
		if(m_FloatMatrixValue != Mp7JrsFloatMatrixPtr())
		{
			m_FloatMatrixValue->SetContentName(XMLString::transcode("FloatMatrixValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetTextValue(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetTextValue().");
	}
	if (m_TextValue != item)
	{
		// Dc1Factory::DeleteObject(m_TextValue);
		m_TextValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextValue) m_TextValue->SetParent(m_myself.getPointer());
		if (m_TextValue && m_TextValue->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TextValue->UseTypeAttribute = true;
		}
		if(m_TextValue != Mp7JrsTextualPtr())
		{
			m_TextValue->SetContentName(XMLString::transcode("TextValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetTextAnnotationValue(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetTextAnnotationValue().");
	}
	if (m_TextAnnotationValue != item)
	{
		// Dc1Factory::DeleteObject(m_TextAnnotationValue);
		m_TextAnnotationValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextAnnotationValue) m_TextAnnotationValue->SetParent(m_myself.getPointer());
		if (m_TextAnnotationValue && m_TextAnnotationValue->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TextAnnotationValue->UseTypeAttribute = true;
		}
		if(m_TextAnnotationValue != Mp7JrsTextAnnotationPtr())
		{
			m_TextAnnotationValue->SetContentName(XMLString::transcode("TextAnnotationValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetControlledTermUseValue(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetControlledTermUseValue().");
	}
	if (m_ControlledTermUseValue != item)
	{
		// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
		m_ControlledTermUseValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ControlledTermUseValue) m_ControlledTermUseValue->SetParent(m_myself.getPointer());
		if (m_ControlledTermUseValue && m_ControlledTermUseValue->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ControlledTermUseValue->UseTypeAttribute = true;
		}
		if(m_ControlledTermUseValue != Mp7JrsControlledTermUsePtr())
		{
			m_ControlledTermUseValue->SetContentName(XMLString::transcode("ControlledTermUseValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_DescriptorValue);
	m_DescriptorValue = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetDescriptorValue(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticStateType_AttributeValuePair_LocalType::SetDescriptorValue().");
	}
	if (m_DescriptorValue != item)
	{
		// Dc1Factory::DeleteObject(m_DescriptorValue);
		m_DescriptorValue = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptorValue) m_DescriptorValue->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_DescriptorValue) m_DescriptorValue->UseTypeAttribute = true;
		if(m_DescriptorValue != Dc1Ptr< Dc1Node >())
		{
			m_DescriptorValue->SetContentName(XMLString::transcode("DescriptorValue"));
		}
	
	m_BooleanValue_Valid = false;
	
	m_IntegerValue_Valid = false;
	
	m_FloatValue_Valid = false;
	// Dc1Factory::DeleteObject(m_IntegerMatrixValue);
	m_IntegerMatrixValue = Mp7JrsIntegerMatrixPtr();
	// Dc1Factory::DeleteObject(m_FloatMatrixValue);
	m_FloatMatrixValue = Mp7JrsFloatMatrixPtr();
	// Dc1Factory::DeleteObject(m_TextValue);
	m_TextValue = Mp7JrsTextualPtr();
	// Dc1Factory::DeleteObject(m_TextAnnotationValue);
	m_TextAnnotationValue = Mp7JrsTextAnnotationPtr();
	// Dc1Factory::DeleteObject(m_ControlledTermUseValue);
	m_ControlledTermUseValue = Mp7JrsControlledTermUsePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SemanticStateType_AttributeValuePair_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSemanticStateType_AttributeValuePair_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	if(m_BooleanValue_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_BooleanValue);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("BooleanValue"), false);
		XMLString::release(&tmp);
	}
	if(m_IntegerValue_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_IntegerValue);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("IntegerValue"), false);
		XMLString::release(&tmp);
	}
	if(m_FloatValue_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_FloatValue);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("FloatValue"), false);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_IntegerMatrixValue != Mp7JrsIntegerMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IntegerMatrixValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IntegerMatrixValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IntegerMatrixValue->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FloatMatrixValue != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FloatMatrixValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FloatMatrixValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FloatMatrixValue->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TextValue != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TextValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TextValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TextValue->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TextAnnotationValue != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TextAnnotationValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TextAnnotationValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TextAnnotationValue->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ControlledTermUseValue != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ControlledTermUseValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ControlledTermUseValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ControlledTermUseValue->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DescriptorValue != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptorValue->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptorValue"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_DescriptorValue->UseTypeAttribute = true;
		}
		m_DescriptorValue->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSemanticStateType_AttributeValuePair_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("BooleanValue"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetBooleanValue(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("IntegerValue"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetIntegerValue(Dc1Convert::TextToInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("FloatValue"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetFloatValue(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IntegerMatrixValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IntegerMatrixValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIntegerMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIntegerMatrixValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FloatMatrixValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FloatMatrixValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFloatMatrixValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TextValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TextValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTextValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TextAnnotationValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TextAnnotationValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTextAnnotationValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ControlledTermUseValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ControlledTermUseValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetControlledTermUseValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptorValue"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptorValue")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptorValue(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticStateType_AttributeValuePair_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("IntegerMatrixValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIntegerMatrixType; // FTT, check this
	}
	this->SetIntegerMatrixValue(child);
  }
  if (XMLString::compareString(elementname, X("FloatMatrixValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetFloatMatrixValue(child);
  }
  if (XMLString::compareString(elementname, X("TextValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetTextValue(child);
  }
  if (XMLString::compareString(elementname, X("TextAnnotationValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetTextAnnotationValue(child);
  }
  if (XMLString::compareString(elementname, X("ControlledTermUseValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetControlledTermUseValue(child);
  }
  if (XMLString::compareString(elementname, X("DescriptorValue")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDType; // FTT, check this
	}
	this->SetDescriptorValue(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticStateType_AttributeValuePair_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetIntegerMatrixValue() != Dc1NodePtr())
		result.Insert(GetIntegerMatrixValue());
	if (GetFloatMatrixValue() != Dc1NodePtr())
		result.Insert(GetFloatMatrixValue());
	if (GetTextValue() != Dc1NodePtr())
		result.Insert(GetTextValue());
	if (GetTextAnnotationValue() != Dc1NodePtr())
		result.Insert(GetTextAnnotationValue());
	if (GetControlledTermUseValue() != Dc1NodePtr())
		result.Insert(GetControlledTermUseValue());
	if (GetDescriptorValue() != Dc1NodePtr())
		result.Insert(GetDescriptorValue());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticStateType_AttributeValuePair_LocalType_ExtMethodImpl.h


