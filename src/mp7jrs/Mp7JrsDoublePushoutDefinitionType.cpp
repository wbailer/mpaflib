
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtImplInclude.h


#include "Mp7JrsGraphicalRuleDefinitionType.h"
#include "Mp7JrsGraphType.h"
#include "Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsDoublePushoutDefinitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMorphismGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MorphismGraph

#include <assert.h>
IMp7JrsDoublePushoutDefinitionType::IMp7JrsDoublePushoutDefinitionType()
{

// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtPropInit.h

}

IMp7JrsDoublePushoutDefinitionType::~IMp7JrsDoublePushoutDefinitionType()
{
// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtPropCleanup.h

}

Mp7JrsDoublePushoutDefinitionType::Mp7JrsDoublePushoutDefinitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDoublePushoutDefinitionType::~Mp7JrsDoublePushoutDefinitionType()
{
	Cleanup();
}

void Mp7JrsDoublePushoutDefinitionType::Init()
{
	// Init base
	m_Base = CreateGraphicalRuleDefinitionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_LHSGraph = Mp7JrsGraphPtr(); // Class
	m_ContextGraph = Mp7JrsGraphPtr(); // Class
	m_RHSGraph = Mp7JrsGraphPtr(); // Class
	m_MorphismGraph = Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtMyPropInit.h

}

void Mp7JrsDoublePushoutDefinitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_LHSGraph);
	// Dc1Factory::DeleteObject(m_ContextGraph);
	// Dc1Factory::DeleteObject(m_RHSGraph);
	// Dc1Factory::DeleteObject(m_MorphismGraph);
}

void Mp7JrsDoublePushoutDefinitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DoublePushoutDefinitionTypePtr, since we
	// might need GetBase(), which isn't defined in IDoublePushoutDefinitionType
	const Dc1Ptr< Mp7JrsDoublePushoutDefinitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsGraphicalRuleDefinitionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDoublePushoutDefinitionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_LHSGraph);
		this->SetLHSGraph(Dc1Factory::CloneObject(tmp->GetLHSGraph()));
		// Dc1Factory::DeleteObject(m_ContextGraph);
		this->SetContextGraph(Dc1Factory::CloneObject(tmp->GetContextGraph()));
		// Dc1Factory::DeleteObject(m_RHSGraph);
		this->SetRHSGraph(Dc1Factory::CloneObject(tmp->GetRHSGraph()));
		// Dc1Factory::DeleteObject(m_MorphismGraph);
		this->SetMorphismGraph(Dc1Factory::CloneObject(tmp->GetMorphismGraph()));
}

Dc1NodePtr Mp7JrsDoublePushoutDefinitionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDoublePushoutDefinitionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsGraphicalRuleDefinitionType > Mp7JrsDoublePushoutDefinitionType::GetBase() const
{
	return m_Base;
}

Mp7JrsGraphPtr Mp7JrsDoublePushoutDefinitionType::GetLHSGraph() const
{
		return m_LHSGraph;
}

Mp7JrsGraphPtr Mp7JrsDoublePushoutDefinitionType::GetContextGraph() const
{
		return m_ContextGraph;
}

Mp7JrsGraphPtr Mp7JrsDoublePushoutDefinitionType::GetRHSGraph() const
{
		return m_RHSGraph;
}

Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr Mp7JrsDoublePushoutDefinitionType::GetMorphismGraph() const
{
		return m_MorphismGraph;
}

void Mp7JrsDoublePushoutDefinitionType::SetLHSGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetLHSGraph().");
	}
	if (m_LHSGraph != item)
	{
		// Dc1Factory::DeleteObject(m_LHSGraph);
		m_LHSGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LHSGraph) m_LHSGraph->SetParent(m_myself.getPointer());
		if (m_LHSGraph && m_LHSGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LHSGraph->UseTypeAttribute = true;
		}
		if(m_LHSGraph != Mp7JrsGraphPtr())
		{
			m_LHSGraph->SetContentName(XMLString::transcode("LHSGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::SetContextGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetContextGraph().");
	}
	if (m_ContextGraph != item)
	{
		// Dc1Factory::DeleteObject(m_ContextGraph);
		m_ContextGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContextGraph) m_ContextGraph->SetParent(m_myself.getPointer());
		if (m_ContextGraph && m_ContextGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ContextGraph->UseTypeAttribute = true;
		}
		if(m_ContextGraph != Mp7JrsGraphPtr())
		{
			m_ContextGraph->SetContentName(XMLString::transcode("ContextGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::SetRHSGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetRHSGraph().");
	}
	if (m_RHSGraph != item)
	{
		// Dc1Factory::DeleteObject(m_RHSGraph);
		m_RHSGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RHSGraph) m_RHSGraph->SetParent(m_myself.getPointer());
		if (m_RHSGraph && m_RHSGraph->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RHSGraph->UseTypeAttribute = true;
		}
		if(m_RHSGraph != Mp7JrsGraphPtr())
		{
			m_RHSGraph->SetContentName(XMLString::transcode("RHSGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::SetMorphismGraph(const Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetMorphismGraph().");
	}
	if (m_MorphismGraph != item)
	{
		// Dc1Factory::DeleteObject(m_MorphismGraph);
		m_MorphismGraph = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MorphismGraph) m_MorphismGraph->SetParent(m_myself.getPointer());
		if(m_MorphismGraph != Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr())
		{
			m_MorphismGraph->SetContentName(XMLString::transcode("MorphismGraph"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsGraphPtr Mp7JrsDoublePushoutDefinitionType::GetTemplateGraph() const
{
	return GetBase()->GetTemplateGraph();
}

// Element is optional
bool Mp7JrsDoublePushoutDefinitionType::IsValidTemplateGraph() const
{
	return GetBase()->IsValidTemplateGraph();
}

void Mp7JrsDoublePushoutDefinitionType::SetTemplateGraph(const Mp7JrsGraphPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetTemplateGraph().");
	}
	GetBase()->SetTemplateGraph(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::InvalidateTemplateGraph()
{
	GetBase()->InvalidateTemplateGraph();
}
XMLCh * Mp7JrsDoublePushoutDefinitionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsDoublePushoutDefinitionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsDoublePushoutDefinitionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsDoublePushoutDefinitionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsDoublePushoutDefinitionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsDoublePushoutDefinitionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsDoublePushoutDefinitionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsDoublePushoutDefinitionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsDoublePushoutDefinitionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsDoublePushoutDefinitionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsDoublePushoutDefinitionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsDoublePushoutDefinitionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsDoublePushoutDefinitionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsDoublePushoutDefinitionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsDoublePushoutDefinitionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoublePushoutDefinitionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsDoublePushoutDefinitionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsDoublePushoutDefinitionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoublePushoutDefinitionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDoublePushoutDefinitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("LHSGraph")) == 0)
	{
		// LHSGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLHSGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLHSGraph(p, client);
					if((p = GetLHSGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContextGraph")) == 0)
	{
		// ContextGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContextGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContextGraph(p, client);
					if((p = GetContextGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RHSGraph")) == 0)
	{
		// RHSGraph is simple element GraphType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRHSGraph()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRHSGraph(p, client);
					if((p = GetRHSGraph()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MorphismGraph")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MorphismGraph is item of type MorphismGraphType
		// in element collection DoublePushoutDefinitionType_MorphismGraph_CollectionType
		
		context->Found = true;
		Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr coll = GetMorphismGraph();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDoublePushoutDefinitionType_MorphismGraph_CollectionType; // FTT, check this
				SetMorphismGraph(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 2))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MorphismGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMorphismGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DoublePushoutDefinitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DoublePushoutDefinitionType");
	}
	return result;
}

XMLCh * Mp7JrsDoublePushoutDefinitionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDoublePushoutDefinitionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDoublePushoutDefinitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DoublePushoutDefinitionType"));
	// Element serialization:
	// Class
	
	if (m_LHSGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LHSGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LHSGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LHSGraph->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ContextGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ContextGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ContextGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ContextGraph->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_RHSGraph != Mp7JrsGraphPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RHSGraph->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RHSGraph"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RHSGraph->Serialize(doc, element, &elem);
	}
	if (m_MorphismGraph != Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MorphismGraph->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDoublePushoutDefinitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsGraphicalRuleDefinitionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LHSGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LHSGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLHSGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ContextGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ContextGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContextGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RHSGraph"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RHSGraph")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRHSGraph(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MorphismGraph"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MorphismGraph")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr tmp = CreateDoublePushoutDefinitionType_MorphismGraph_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMorphismGraph(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDoublePushoutDefinitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("LHSGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetLHSGraph(child);
  }
  if (XMLString::compareString(elementname, X("ContextGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetContextGraph(child);
  }
  if (XMLString::compareString(elementname, X("RHSGraph")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType; // FTT, check this
	}
	this->SetRHSGraph(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MorphismGraph")) == 0))
  {
	Mp7JrsDoublePushoutDefinitionType_MorphismGraph_CollectionPtr tmp = CreateDoublePushoutDefinitionType_MorphismGraph_CollectionType; // FTT, check this
	this->SetMorphismGraph(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDoublePushoutDefinitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLHSGraph() != Dc1NodePtr())
		result.Insert(GetLHSGraph());
	if (GetContextGraph() != Dc1NodePtr())
		result.Insert(GetContextGraph());
	if (GetRHSGraph() != Dc1NodePtr())
		result.Insert(GetRHSGraph());
	if (GetMorphismGraph() != Dc1NodePtr())
		result.Insert(GetMorphismGraph());
	if (GetTemplateGraph() != Dc1NodePtr())
		result.Insert(GetTemplateGraph());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDoublePushoutDefinitionType_ExtMethodImpl.h


