
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtImplInclude.h


#include "Mp7JrsOutputQCValueType.h"
#include "Mp7JrsSegmentOutputQCValueType.h"
#include "Mp7JrsQCItemResultType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsQCItemResultType_LocalType::IMp7JrsQCItemResultType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtPropInit.h

}

IMp7JrsQCItemResultType_LocalType::~IMp7JrsQCItemResultType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtPropCleanup.h

}

Mp7JrsQCItemResultType_LocalType::Mp7JrsQCItemResultType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsQCItemResultType_LocalType::~Mp7JrsQCItemResultType_LocalType()
{
	Cleanup();
}

void Mp7JrsQCItemResultType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Output = Mp7JrsOutputQCValuePtr(); // Class
	m_SegmentOutput = Mp7JrsSegmentOutputQCValuePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtMyPropInit.h

}

void Mp7JrsQCItemResultType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Output);
	// Dc1Factory::DeleteObject(m_SegmentOutput);
}

void Mp7JrsQCItemResultType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QCItemResultType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IQCItemResultType_LocalType
	const Dc1Ptr< Mp7JrsQCItemResultType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Output);
		this->SetOutput(Dc1Factory::CloneObject(tmp->GetOutput()));
		// Dc1Factory::DeleteObject(m_SegmentOutput);
		this->SetSegmentOutput(Dc1Factory::CloneObject(tmp->GetSegmentOutput()));
}

Dc1NodePtr Mp7JrsQCItemResultType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsQCItemResultType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsOutputQCValuePtr Mp7JrsQCItemResultType_LocalType::GetOutput() const
{
		return m_Output;
}

Mp7JrsSegmentOutputQCValuePtr Mp7JrsQCItemResultType_LocalType::GetSegmentOutput() const
{
		return m_SegmentOutput;
}

// implementing setter for choice 
/* element */
void Mp7JrsQCItemResultType_LocalType::SetOutput(const Mp7JrsOutputQCValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType_LocalType::SetOutput().");
	}
	if (m_Output != item)
	{
		// Dc1Factory::DeleteObject(m_Output);
		m_Output = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Output) m_Output->SetParent(m_myself.getPointer());
		if (m_Output && m_Output->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OutputQCValueType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Output->UseTypeAttribute = true;
		}
		if(m_Output != Mp7JrsOutputQCValuePtr())
		{
			m_Output->SetContentName(XMLString::transcode("Output"));
		}
	// Dc1Factory::DeleteObject(m_SegmentOutput);
	m_SegmentOutput = Mp7JrsSegmentOutputQCValuePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsQCItemResultType_LocalType::SetSegmentOutput(const Mp7JrsSegmentOutputQCValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType_LocalType::SetSegmentOutput().");
	}
	if (m_SegmentOutput != item)
	{
		// Dc1Factory::DeleteObject(m_SegmentOutput);
		m_SegmentOutput = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SegmentOutput) m_SegmentOutput->SetParent(m_myself.getPointer());
		if (m_SegmentOutput && m_SegmentOutput->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentOutputQCValueType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SegmentOutput->UseTypeAttribute = true;
		}
		if(m_SegmentOutput != Mp7JrsSegmentOutputQCValuePtr())
		{
			m_SegmentOutput->SetContentName(XMLString::transcode("SegmentOutput"));
		}
	// Dc1Factory::DeleteObject(m_Output);
	m_Output = Mp7JrsOutputQCValuePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: QCItemResultType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsQCItemResultType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsQCItemResultType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsQCItemResultType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsQCItemResultType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Output != Mp7JrsOutputQCValuePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Output->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Output"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Output->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SegmentOutput != Mp7JrsSegmentOutputQCValuePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SegmentOutput->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SegmentOutput"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SegmentOutput->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsQCItemResultType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Output"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Output")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateOutputQCValueType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOutput(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SegmentOutput"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SegmentOutput")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSegmentOutputQCValueType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSegmentOutput(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsQCItemResultType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Output")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateOutputQCValueType; // FTT, check this
	}
	this->SetOutput(child);
  }
  if (XMLString::compareString(elementname, X("SegmentOutput")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSegmentOutputQCValueType; // FTT, check this
	}
	this->SetSegmentOutput(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsQCItemResultType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOutput() != Dc1NodePtr())
		result.Insert(GetOutput());
	if (GetSegmentOutput() != Dc1NodePtr())
		result.Insert(GetSegmentOutput());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsQCItemResultType_LocalType_ExtMethodImpl.h


