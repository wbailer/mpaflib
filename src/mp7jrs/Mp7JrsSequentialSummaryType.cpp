
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtImplInclude.h


#include "Mp7JrsSummaryType.h"
#include "Mp7JrsSequentialSummaryType_components_CollectionType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionType.h"
#include "Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionType.h"
#include "Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionType.h"
#include "Mp7JrsSummaryType_Name_CollectionType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSequentialSummaryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsVisualSummaryComponentType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VisualSummaryComponent
#include "Mp7JrsAudioSummaryComponentType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AudioSummaryComponent
#include "Mp7JrsTextualSummaryComponentType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextualSummaryComponent

#include <assert.h>
IMp7JrsSequentialSummaryType::IMp7JrsSequentialSummaryType()
{

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtPropInit.h

}

IMp7JrsSequentialSummaryType::~IMp7JrsSequentialSummaryType()
{
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtPropCleanup.h

}

Mp7JrsSequentialSummaryType::Mp7JrsSequentialSummaryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSequentialSummaryType::~Mp7JrsSequentialSummaryType()
{
	Cleanup();
}

void Mp7JrsSequentialSummaryType::Init()
{
	// Init base
	m_Base = CreateSummaryType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_components = Mp7JrsSequentialSummaryType_components_CollectionPtr(); // Collection
	m_components_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_AudioVisualSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_AudioVisualSummaryLocator_Exist = false;
	m_VideoSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_VideoSummaryLocator_Exist = false;
	m_AudioSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_AudioSummaryLocator_Exist = false;
	m_VisualSummaryComponent = Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr(); // Collection
	m_AudioSummaryComponent = Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr(); // Collection
	m_TextualSummaryComponent = Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtMyPropInit.h

}

void Mp7JrsSequentialSummaryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_components);
	// Dc1Factory::DeleteObject(m_AudioVisualSummaryLocator);
	// Dc1Factory::DeleteObject(m_VideoSummaryLocator);
	// Dc1Factory::DeleteObject(m_AudioSummaryLocator);
	// Dc1Factory::DeleteObject(m_VisualSummaryComponent);
	// Dc1Factory::DeleteObject(m_AudioSummaryComponent);
	// Dc1Factory::DeleteObject(m_TextualSummaryComponent);
}

void Mp7JrsSequentialSummaryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SequentialSummaryTypePtr, since we
	// might need GetBase(), which isn't defined in ISequentialSummaryType
	const Dc1Ptr< Mp7JrsSequentialSummaryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSummaryPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSequentialSummaryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_components);
	if (tmp->Existcomponents())
	{
		this->Setcomponents(Dc1Factory::CloneObject(tmp->Getcomponents()));
	}
	else
	{
		Invalidatecomponents();
	}
	}
	if (tmp->IsValidAudioVisualSummaryLocator())
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSummaryLocator);
		this->SetAudioVisualSummaryLocator(Dc1Factory::CloneObject(tmp->GetAudioVisualSummaryLocator()));
	}
	else
	{
		InvalidateAudioVisualSummaryLocator();
	}
	if (tmp->IsValidVideoSummaryLocator())
	{
		// Dc1Factory::DeleteObject(m_VideoSummaryLocator);
		this->SetVideoSummaryLocator(Dc1Factory::CloneObject(tmp->GetVideoSummaryLocator()));
	}
	else
	{
		InvalidateVideoSummaryLocator();
	}
	if (tmp->IsValidAudioSummaryLocator())
	{
		// Dc1Factory::DeleteObject(m_AudioSummaryLocator);
		this->SetAudioSummaryLocator(Dc1Factory::CloneObject(tmp->GetAudioSummaryLocator()));
	}
	else
	{
		InvalidateAudioSummaryLocator();
	}
		// Dc1Factory::DeleteObject(m_VisualSummaryComponent);
		this->SetVisualSummaryComponent(Dc1Factory::CloneObject(tmp->GetVisualSummaryComponent()));
		// Dc1Factory::DeleteObject(m_AudioSummaryComponent);
		this->SetAudioSummaryComponent(Dc1Factory::CloneObject(tmp->GetAudioSummaryComponent()));
		// Dc1Factory::DeleteObject(m_TextualSummaryComponent);
		this->SetTextualSummaryComponent(Dc1Factory::CloneObject(tmp->GetTextualSummaryComponent()));
}

Dc1NodePtr Mp7JrsSequentialSummaryType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSequentialSummaryType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSummaryType > Mp7JrsSequentialSummaryType::GetBase() const
{
	return m_Base;
}

Mp7JrsSequentialSummaryType_components_CollectionPtr Mp7JrsSequentialSummaryType::Getcomponents() const
{
	return m_components;
}

bool Mp7JrsSequentialSummaryType::Existcomponents() const
{
	return m_components_Exist;
}
void Mp7JrsSequentialSummaryType::Setcomponents(const Mp7JrsSequentialSummaryType_components_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::Setcomponents().");
	}
	m_components = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_components) m_components->SetParent(m_myself.getPointer());
	m_components_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::Invalidatecomponents()
{
	m_components_Exist = false;
}
Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSequentialSummaryType::GetAudioVisualSummaryLocator() const
{
		return m_AudioVisualSummaryLocator;
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidAudioVisualSummaryLocator() const
{
	return m_AudioVisualSummaryLocator_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSequentialSummaryType::GetVideoSummaryLocator() const
{
		return m_VideoSummaryLocator;
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidVideoSummaryLocator() const
{
	return m_VideoSummaryLocator_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSequentialSummaryType::GetAudioSummaryLocator() const
{
		return m_AudioSummaryLocator;
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidAudioSummaryLocator() const
{
	return m_AudioSummaryLocator_Exist;
}

Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr Mp7JrsSequentialSummaryType::GetVisualSummaryComponent() const
{
		return m_VisualSummaryComponent;
}

Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr Mp7JrsSequentialSummaryType::GetAudioSummaryComponent() const
{
		return m_AudioSummaryComponent;
}

Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr Mp7JrsSequentialSummaryType::GetTextualSummaryComponent() const
{
		return m_TextualSummaryComponent;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSequentialSummaryType::SetAudioVisualSummaryLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetAudioVisualSummaryLocator().");
	}
	if (m_AudioVisualSummaryLocator != item || m_AudioVisualSummaryLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSummaryLocator);
		m_AudioVisualSummaryLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualSummaryLocator) m_AudioVisualSummaryLocator->SetParent(m_myself.getPointer());
		if (m_AudioVisualSummaryLocator && m_AudioVisualSummaryLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualSummaryLocator->UseTypeAttribute = true;
		}
		m_AudioVisualSummaryLocator_Exist = true;
		if(m_AudioVisualSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_AudioVisualSummaryLocator->SetContentName(XMLString::transcode("AudioVisualSummaryLocator"));
		}
	// Dc1Factory::DeleteObject(m_VideoSummaryLocator);
	m_VideoSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr();
	// Dc1Factory::DeleteObject(m_AudioSummaryLocator);
	m_AudioSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateAudioVisualSummaryLocator()
{
	m_AudioVisualSummaryLocator_Exist = false;
}
/* sequence */
void Mp7JrsSequentialSummaryType::SetVideoSummaryLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetVideoSummaryLocator().");
	}
	if (m_VideoSummaryLocator != item || m_VideoSummaryLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VideoSummaryLocator);
		m_VideoSummaryLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSummaryLocator) m_VideoSummaryLocator->SetParent(m_myself.getPointer());
		if (m_VideoSummaryLocator && m_VideoSummaryLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSummaryLocator->UseTypeAttribute = true;
		}
		m_VideoSummaryLocator_Exist = true;
		if(m_VideoSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_VideoSummaryLocator->SetContentName(XMLString::transcode("VideoSummaryLocator"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualSummaryLocator);
	m_AudioVisualSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateVideoSummaryLocator()
{
	m_VideoSummaryLocator_Exist = false;
}
void Mp7JrsSequentialSummaryType::SetAudioSummaryLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetAudioSummaryLocator().");
	}
	if (m_AudioSummaryLocator != item || m_AudioSummaryLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioSummaryLocator);
		m_AudioSummaryLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSummaryLocator) m_AudioSummaryLocator->SetParent(m_myself.getPointer());
		if (m_AudioSummaryLocator && m_AudioSummaryLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSummaryLocator->UseTypeAttribute = true;
		}
		m_AudioSummaryLocator_Exist = true;
		if(m_AudioSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_AudioSummaryLocator->SetContentName(XMLString::transcode("AudioSummaryLocator"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualSummaryLocator);
	m_AudioVisualSummaryLocator = Mp7JrsTemporalSegmentLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateAudioSummaryLocator()
{
	m_AudioSummaryLocator_Exist = false;
}
void Mp7JrsSequentialSummaryType::SetVisualSummaryComponent(const Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetVisualSummaryComponent().");
	}
	if (m_VisualSummaryComponent != item)
	{
		// Dc1Factory::DeleteObject(m_VisualSummaryComponent);
		m_VisualSummaryComponent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualSummaryComponent) m_VisualSummaryComponent->SetParent(m_myself.getPointer());
		if(m_VisualSummaryComponent != Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr())
		{
			m_VisualSummaryComponent->SetContentName(XMLString::transcode("VisualSummaryComponent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::SetAudioSummaryComponent(const Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetAudioSummaryComponent().");
	}
	if (m_AudioSummaryComponent != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSummaryComponent);
		m_AudioSummaryComponent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSummaryComponent) m_AudioSummaryComponent->SetParent(m_myself.getPointer());
		if(m_AudioSummaryComponent != Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr())
		{
			m_AudioSummaryComponent->SetContentName(XMLString::transcode("AudioSummaryComponent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::SetTextualSummaryComponent(const Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetTextualSummaryComponent().");
	}
	if (m_TextualSummaryComponent != item)
	{
		// Dc1Factory::DeleteObject(m_TextualSummaryComponent);
		m_TextualSummaryComponent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextualSummaryComponent) m_TextualSummaryComponent->SetParent(m_myself.getPointer());
		if(m_TextualSummaryComponent != Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr())
		{
			m_TextualSummaryComponent->SetContentName(XMLString::transcode("TextualSummaryComponent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSummaryType_Name_CollectionPtr Mp7JrsSequentialSummaryType::GetName() const
{
	return GetBase()->GetName();
}

Mp7JrsUniqueIDPtr Mp7JrsSequentialSummaryType::GetSourceID() const
{
	return GetBase()->GetSourceID();
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidSourceID() const
{
	return GetBase()->IsValidSourceID();
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsSequentialSummaryType::GetSourceLocator() const
{
	return GetBase()->GetSourceLocator();
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidSourceLocator() const
{
	return GetBase()->IsValidSourceLocator();
}

Mp7JrsReferencePtr Mp7JrsSequentialSummaryType::GetSourceInformation() const
{
	return GetBase()->GetSourceInformation();
}

// Element is optional
bool Mp7JrsSequentialSummaryType::IsValidSourceInformation() const
{
	return GetBase()->IsValidSourceInformation();
}

void Mp7JrsSequentialSummaryType::SetName(const Mp7JrsSummaryType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetName().");
	}
	GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::SetSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetSourceID().");
	}
	GetBase()->SetSourceID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateSourceID()
{
	GetBase()->InvalidateSourceID();
}
void Mp7JrsSequentialSummaryType::SetSourceLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetSourceLocator().");
	}
	GetBase()->SetSourceLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateSourceLocator()
{
	GetBase()->InvalidateSourceLocator();
}
void Mp7JrsSequentialSummaryType::SetSourceInformation(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetSourceInformation().");
	}
	GetBase()->SetSourceInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidateSourceInformation()
{
	GetBase()->InvalidateSourceInformation();
}
XMLCh * Mp7JrsSequentialSummaryType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSequentialSummaryType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSequentialSummaryType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSequentialSummaryType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSequentialSummaryType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSequentialSummaryType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSequentialSummaryType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSequentialSummaryType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSequentialSummaryType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSequentialSummaryType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSequentialSummaryType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSequentialSummaryType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSequentialSummaryType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSequentialSummaryType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSequentialSummaryType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSequentialSummaryType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSequentialSummaryType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSequentialSummaryType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSequentialSummaryType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSequentialSummaryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("components")) == 0)
	{
		// components is simple attribute SequentialSummaryType_components_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSequentialSummaryType_components_CollectionPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioVisualSummaryLocator")) == 0)
	{
		// AudioVisualSummaryLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioVisualSummaryLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioVisualSummaryLocator(p, client);
					if((p = GetAudioVisualSummaryLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VideoSummaryLocator")) == 0)
	{
		// VideoSummaryLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVideoSummaryLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVideoSummaryLocator(p, client);
					if((p = GetVideoSummaryLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSummaryLocator")) == 0)
	{
		// AudioSummaryLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioSummaryLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioSummaryLocator(p, client);
					if((p = GetAudioSummaryLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualSummaryComponent")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VisualSummaryComponent is item of type VisualSummaryComponentType
		// in element collection SequentialSummaryType_VisualSummaryComponent_CollectionType
		
		context->Found = true;
		Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr coll = GetVisualSummaryComponent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSequentialSummaryType_VisualSummaryComponent_CollectionType; // FTT, check this
				SetVisualSummaryComponent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VisualSummaryComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVisualSummaryComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSummaryComponent")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AudioSummaryComponent is item of type AudioSummaryComponentType
		// in element collection SequentialSummaryType_AudioSummaryComponent_CollectionType
		
		context->Found = true;
		Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr coll = GetAudioSummaryComponent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSequentialSummaryType_AudioSummaryComponent_CollectionType; // FTT, check this
				SetAudioSummaryComponent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSummaryComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSummaryComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextualSummaryComponent")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:TextualSummaryComponent is item of type TextualSummaryComponentType
		// in element collection SequentialSummaryType_TextualSummaryComponent_CollectionType
		
		context->Found = true;
		Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr coll = GetTextualSummaryComponent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSequentialSummaryType_TextualSummaryComponent_CollectionType; // FTT, check this
				SetTextualSummaryComponent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualSummaryComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualSummaryComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SequentialSummaryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SequentialSummaryType");
	}
	return result;
}

XMLCh * Mp7JrsSequentialSummaryType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSequentialSummaryType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSequentialSummaryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SequentialSummaryType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_components_Exist)
	{
	// Collection
	if(m_components != Mp7JrsSequentialSummaryType_components_CollectionPtr())
	{
		XMLCh * tmp = m_components->ToText();
		element->setAttributeNS(X(""), X("components"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_AudioVisualSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualSummaryLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualSummaryLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualSummaryLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VideoSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSummaryLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSummaryLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSummaryLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSummaryLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSummaryLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSummaryLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSummaryLocator->Serialize(doc, element, &elem);
	}
	if (m_VisualSummaryComponent != Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VisualSummaryComponent->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_AudioSummaryComponent != Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioSummaryComponent->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_TextualSummaryComponent != Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_TextualSummaryComponent->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSequentialSummaryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("components")))
	{
		// Deserialize collection type
		Mp7JrsSequentialSummaryType_components_CollectionPtr tmp = CreateSequentialSummaryType_components_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("components")));
		this->Setcomponents(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsSummaryType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualSummaryLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualSummaryLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualSummaryLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSummaryLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSummaryLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSummaryLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSummaryLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSummaryLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSummaryLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VisualSummaryComponent"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VisualSummaryComponent")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_VisualSummaryComponent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVisualSummaryComponent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AudioSummaryComponent"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AudioSummaryComponent")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_AudioSummaryComponent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioSummaryComponent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("TextualSummaryComponent"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("TextualSummaryComponent")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_TextualSummaryComponent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTextualSummaryComponent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSequentialSummaryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("AudioVisualSummaryLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetAudioVisualSummaryLocator(child);
  }
  if (XMLString::compareString(elementname, X("VideoSummaryLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetVideoSummaryLocator(child);
  }
  if (XMLString::compareString(elementname, X("AudioSummaryLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetAudioSummaryLocator(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VisualSummaryComponent")) == 0))
  {
	Mp7JrsSequentialSummaryType_VisualSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_VisualSummaryComponent_CollectionType; // FTT, check this
	this->SetVisualSummaryComponent(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AudioSummaryComponent")) == 0))
  {
	Mp7JrsSequentialSummaryType_AudioSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_AudioSummaryComponent_CollectionType; // FTT, check this
	this->SetAudioSummaryComponent(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("TextualSummaryComponent")) == 0))
  {
	Mp7JrsSequentialSummaryType_TextualSummaryComponent_CollectionPtr tmp = CreateSequentialSummaryType_TextualSummaryComponent_CollectionType; // FTT, check this
	this->SetTextualSummaryComponent(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSequentialSummaryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVisualSummaryComponent() != Dc1NodePtr())
		result.Insert(GetVisualSummaryComponent());
	if (GetAudioSummaryComponent() != Dc1NodePtr())
		result.Insert(GetAudioSummaryComponent());
	if (GetTextualSummaryComponent() != Dc1NodePtr())
		result.Insert(GetTextualSummaryComponent());
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetSourceID() != Dc1NodePtr())
		result.Insert(GetSourceID());
	if (GetSourceLocator() != Dc1NodePtr())
		result.Insert(GetSourceLocator());
	if (GetSourceInformation() != Dc1NodePtr())
		result.Insert(GetSourceInformation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetAudioVisualSummaryLocator() != Dc1NodePtr())
		result.Insert(GetAudioVisualSummaryLocator());
	if (GetVideoSummaryLocator() != Dc1NodePtr())
		result.Insert(GetVideoSummaryLocator());
	if (GetAudioSummaryLocator() != Dc1NodePtr())
		result.Insert(GetAudioSummaryLocator());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSequentialSummaryType_ExtMethodImpl.h


