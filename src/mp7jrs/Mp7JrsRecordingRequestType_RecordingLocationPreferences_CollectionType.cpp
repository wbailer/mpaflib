
// Based on RefCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include "Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType.h"
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType_ExtImplInclude.h


Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType()
{
	m_Item = new XERCES_CPP_NAMESPACE_QUALIFIER RefVectorOf <XMLCh>(0);

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType_ExtPropInit.h

}

Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::~Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType()
{

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType_ExtPropCleanup.h


	delete m_Item;
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::DeepCopy(const Dc1NodePtr &original)
{
	const Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: wrong argument type

	m_Item->removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		// This is necessary when Xerces and Mp7Jrs are in
        // different libraries
		XMLCh *text = tmp->elementAt(i);
		XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
		XMLString::copyString(ours, text);
		m_Item->addElement(ours);
	}
}

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType_ExtMethodImpl.h


void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::addElement(XMLCh * const toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::addElement().");
	}
	
	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toAdd) + 1];
	XMLString::copyString(ours, toAdd);
	m_Item->addElement(ours);	
		
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::setElementAt(XMLCh * const toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::setElementAt().");
	}
	
	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toSet) + 1];
	XMLString::copyString(ours, toSet);
	m_Item->setElementAt(ours,setAt);

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::insertElementAt(XMLCh * const toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::insertElementAt().");
	}

	// baw 27 04 2004: copy string to ensure that the string is allocated in Mp7Jrs
	XMLCh *ours = new XMLCh[XMLString::stringLen(toInsert) + 1];
	XMLString::copyString(ours, toInsert);
	m_Item->insertElementAt(ours,insertAt);

	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::orphanElementAt(unsigned int orphanAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::orphanElementAt().");
	}
	return m_Item->orphanElementAt(orphanAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::removeAllElements().");
	}
	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::removeElementAt().");
	}
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::containsElement(const XMLCh * toCheck)
{
	return m_Item->containsElement(toCheck);
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::cleanup()
{
	m_Item->cleanup();
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::reinitialize()
{
	m_Item->reinitialize();
}

unsigned int Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const XMLCh * Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

XMLCh * Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

unsigned int Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::size() const
{
	return (unsigned int)m_Item->size();
}

void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

// JRS: addition to Xerces collection
int Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::elementIndexOf(XMLCh * const toCheck) const
{
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		if(m_Item->elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1;
}

XMLCh * Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < m_Item->size(); i++)
	{
		XMLCh * tmp = (m_Item->elementAt(i));
		Dc1Util::CatString(&buf, tmp);
		if(i < m_Item->size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer;
	return buf;
}

bool Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
	XERCES_CPP_NAMESPACE_QUALIFIER BaseRefVectorOf<XMLCh> * tmp = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::tokenizeString(txt);
	if(tmp != NULL)
	{
		for(unsigned int i = 0; i < tmp->size(); i++)
		{
			// This is necessary when Xerces and Mp7Jrs are in
            // different libraries
			XMLCh *text = tmp->elementAt(i);
			XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
			XMLString::copyString(ours, text);
			m_Item->addElement(ours);
		}
	}
	delete tmp; 
	return m_Item->size() > 0;
}


void Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent,XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
	} else {
		element = doc->getDocumentElement();
	}

// no includefile for extension defined 
// file Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType_ExtPreSerialize.h


	// Collection is not simpleType list
	if (GetContentName()) {
		for(unsigned int i = 0; i < m_Item->size(); i++)
		{
// OLD			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			element->appendChild(elem);
			elem->appendChild(doc->createTextNode(m_Item->elementAt(i)));
		}
	}
}

bool Mp7JrsRecordingRequestType_RecordingLocationPreferences_CollectionType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
	// Deserialize collection
	if(parent == NULL)
	{
		return false;
	}

	* current = parent; // Init with first element - check this!
	// Deserialize XMLCh * collection
	// Collection is not simpleType list
	while((parent != NULL) && (
		Dc1Util::HasNodeName(parent, X("RecordingLocationPreferences"), X("urn:mpeg:mpeg7:schema:2004"))
// OLD2		(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:RecordingLocationPreferences")) == 0)
))
	{
		// This is necessary when Xerces uses a different heap
		XMLCh *text = Dc1Util::GetElementText(parent);
		XMLCh *ours = new XMLCh[XMLString::stringLen(text) + 1];
		XMLString::copyString(ours, text);
		m_Item->addElement(ours);
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// NOTE: Use the following includefile for extension
// #include "Dc1RelationType_target_CollectionType_ExtPostDeserialize.h"

	return true;
}

