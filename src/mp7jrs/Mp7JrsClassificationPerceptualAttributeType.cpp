
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsPerceptualLanguageType.h"
#include "Mp7JrsPerceptualSoundType.h"
#include "Mp7JrsPerceptualRecordingType.h"
#include "Mp7JrsPerceptualVocalsType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsClassificationPerceptualAttributeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsClassificationPerceptualAttributeType::IMp7JrsClassificationPerceptualAttributeType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtPropInit.h

}

IMp7JrsClassificationPerceptualAttributeType::~IMp7JrsClassificationPerceptualAttributeType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtPropCleanup.h

}

Mp7JrsClassificationPerceptualAttributeType::Mp7JrsClassificationPerceptualAttributeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationPerceptualAttributeType::~Mp7JrsClassificationPerceptualAttributeType()
{
	Cleanup();
}

void Mp7JrsClassificationPerceptualAttributeType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_PerceptualLanguage = Mp7JrsPerceptualLanguagePtr(); // Class
	m_PerceptualLanguage_Exist = false;
	m_PerceptualSound = Mp7JrsPerceptualSoundPtr(); // Class
	m_PerceptualSound_Exist = false;
	m_PerceptualRecording = Mp7JrsPerceptualRecordingPtr(); // Class
	m_PerceptualRecording_Exist = false;
	m_PerceptualVocals = Mp7JrsPerceptualVocalsPtr(); // Class
	m_PerceptualVocals_Exist = false;


// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtMyPropInit.h

}

void Mp7JrsClassificationPerceptualAttributeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PerceptualLanguage);
	// Dc1Factory::DeleteObject(m_PerceptualSound);
	// Dc1Factory::DeleteObject(m_PerceptualRecording);
	// Dc1Factory::DeleteObject(m_PerceptualVocals);
}

void Mp7JrsClassificationPerceptualAttributeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationPerceptualAttributeTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationPerceptualAttributeType
	const Dc1Ptr< Mp7JrsClassificationPerceptualAttributeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationPerceptualAttributeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidPerceptualLanguage())
	{
		// Dc1Factory::DeleteObject(m_PerceptualLanguage);
		this->SetPerceptualLanguage(Dc1Factory::CloneObject(tmp->GetPerceptualLanguage()));
	}
	else
	{
		InvalidatePerceptualLanguage();
	}
	if (tmp->IsValidPerceptualSound())
	{
		// Dc1Factory::DeleteObject(m_PerceptualSound);
		this->SetPerceptualSound(Dc1Factory::CloneObject(tmp->GetPerceptualSound()));
	}
	else
	{
		InvalidatePerceptualSound();
	}
	if (tmp->IsValidPerceptualRecording())
	{
		// Dc1Factory::DeleteObject(m_PerceptualRecording);
		this->SetPerceptualRecording(Dc1Factory::CloneObject(tmp->GetPerceptualRecording()));
	}
	else
	{
		InvalidatePerceptualRecording();
	}
	if (tmp->IsValidPerceptualVocals())
	{
		// Dc1Factory::DeleteObject(m_PerceptualVocals);
		this->SetPerceptualVocals(Dc1Factory::CloneObject(tmp->GetPerceptualVocals()));
	}
	else
	{
		InvalidatePerceptualVocals();
	}
}

Dc1NodePtr Mp7JrsClassificationPerceptualAttributeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationPerceptualAttributeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsClassificationPerceptualAttributeType::GetBase() const
{
	return m_Base;
}

Mp7JrsPerceptualLanguagePtr Mp7JrsClassificationPerceptualAttributeType::GetPerceptualLanguage() const
{
		return m_PerceptualLanguage;
}

// Element is optional
bool Mp7JrsClassificationPerceptualAttributeType::IsValidPerceptualLanguage() const
{
	return m_PerceptualLanguage_Exist;
}

Mp7JrsPerceptualSoundPtr Mp7JrsClassificationPerceptualAttributeType::GetPerceptualSound() const
{
		return m_PerceptualSound;
}

// Element is optional
bool Mp7JrsClassificationPerceptualAttributeType::IsValidPerceptualSound() const
{
	return m_PerceptualSound_Exist;
}

Mp7JrsPerceptualRecordingPtr Mp7JrsClassificationPerceptualAttributeType::GetPerceptualRecording() const
{
		return m_PerceptualRecording;
}

// Element is optional
bool Mp7JrsClassificationPerceptualAttributeType::IsValidPerceptualRecording() const
{
	return m_PerceptualRecording_Exist;
}

Mp7JrsPerceptualVocalsPtr Mp7JrsClassificationPerceptualAttributeType::GetPerceptualVocals() const
{
		return m_PerceptualVocals;
}

// Element is optional
bool Mp7JrsClassificationPerceptualAttributeType::IsValidPerceptualVocals() const
{
	return m_PerceptualVocals_Exist;
}

void Mp7JrsClassificationPerceptualAttributeType::SetPerceptualLanguage(const Mp7JrsPerceptualLanguagePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPerceptualAttributeType::SetPerceptualLanguage().");
	}
	if (m_PerceptualLanguage != item || m_PerceptualLanguage_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualLanguage);
		m_PerceptualLanguage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualLanguage) m_PerceptualLanguage->SetParent(m_myself.getPointer());
		if (m_PerceptualLanguage && m_PerceptualLanguage->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLanguageType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualLanguage->UseTypeAttribute = true;
		}
		m_PerceptualLanguage_Exist = true;
		if(m_PerceptualLanguage != Mp7JrsPerceptualLanguagePtr())
		{
			m_PerceptualLanguage->SetContentName(XMLString::transcode("PerceptualLanguage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPerceptualAttributeType::InvalidatePerceptualLanguage()
{
	m_PerceptualLanguage_Exist = false;
}
void Mp7JrsClassificationPerceptualAttributeType::SetPerceptualSound(const Mp7JrsPerceptualSoundPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPerceptualAttributeType::SetPerceptualSound().");
	}
	if (m_PerceptualSound != item || m_PerceptualSound_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualSound);
		m_PerceptualSound = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualSound) m_PerceptualSound->SetParent(m_myself.getPointer());
		if (m_PerceptualSound && m_PerceptualSound->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualSoundType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualSound->UseTypeAttribute = true;
		}
		m_PerceptualSound_Exist = true;
		if(m_PerceptualSound != Mp7JrsPerceptualSoundPtr())
		{
			m_PerceptualSound->SetContentName(XMLString::transcode("PerceptualSound"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPerceptualAttributeType::InvalidatePerceptualSound()
{
	m_PerceptualSound_Exist = false;
}
void Mp7JrsClassificationPerceptualAttributeType::SetPerceptualRecording(const Mp7JrsPerceptualRecordingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPerceptualAttributeType::SetPerceptualRecording().");
	}
	if (m_PerceptualRecording != item || m_PerceptualRecording_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualRecording);
		m_PerceptualRecording = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualRecording) m_PerceptualRecording->SetParent(m_myself.getPointer());
		if (m_PerceptualRecording && m_PerceptualRecording->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualRecordingType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualRecording->UseTypeAttribute = true;
		}
		m_PerceptualRecording_Exist = true;
		if(m_PerceptualRecording != Mp7JrsPerceptualRecordingPtr())
		{
			m_PerceptualRecording->SetContentName(XMLString::transcode("PerceptualRecording"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPerceptualAttributeType::InvalidatePerceptualRecording()
{
	m_PerceptualRecording_Exist = false;
}
void Mp7JrsClassificationPerceptualAttributeType::SetPerceptualVocals(const Mp7JrsPerceptualVocalsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPerceptualAttributeType::SetPerceptualVocals().");
	}
	if (m_PerceptualVocals != item || m_PerceptualVocals_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualVocals);
		m_PerceptualVocals = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualVocals) m_PerceptualVocals->SetParent(m_myself.getPointer());
		if (m_PerceptualVocals && m_PerceptualVocals->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualVocalsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualVocals->UseTypeAttribute = true;
		}
		m_PerceptualVocals_Exist = true;
		if(m_PerceptualVocals != Mp7JrsPerceptualVocalsPtr())
		{
			m_PerceptualVocals->SetContentName(XMLString::transcode("PerceptualVocals"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPerceptualAttributeType::InvalidatePerceptualVocals()
{
	m_PerceptualVocals_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsClassificationPerceptualAttributeType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsClassificationPerceptualAttributeType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsClassificationPerceptualAttributeType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPerceptualAttributeType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPerceptualAttributeType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsClassificationPerceptualAttributeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("PerceptualLanguage")) == 0)
	{
		// PerceptualLanguage is simple element PerceptualLanguageType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualLanguage()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLanguageType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualLanguagePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualLanguage(p, client);
					if((p = GetPerceptualLanguage()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualSound")) == 0)
	{
		// PerceptualSound is simple element PerceptualSoundType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualSound()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualSoundType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualSoundPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualSound(p, client);
					if((p = GetPerceptualSound()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualRecording")) == 0)
	{
		// PerceptualRecording is simple element PerceptualRecordingType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualRecording()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualRecordingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualRecordingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualRecording(p, client);
					if((p = GetPerceptualRecording()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualVocals")) == 0)
	{
		// PerceptualVocals is simple element PerceptualVocalsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualVocals()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualVocalsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualVocalsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualVocals(p, client);
					if((p = GetPerceptualVocals()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationPerceptualAttributeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationPerceptualAttributeType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationPerceptualAttributeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsClassificationPerceptualAttributeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsClassificationPerceptualAttributeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationPerceptualAttributeType"));
	// Element serialization:
	// Class
	
	if (m_PerceptualLanguage != Mp7JrsPerceptualLanguagePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualLanguage->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualLanguage"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualLanguage->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualSound != Mp7JrsPerceptualSoundPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualSound->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualSound"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualSound->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualRecording != Mp7JrsPerceptualRecordingPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualRecording->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualRecording"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualRecording->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualVocals != Mp7JrsPerceptualVocalsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualVocals->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualVocals"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualVocals->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsClassificationPerceptualAttributeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualLanguage")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualLanguageType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualLanguage(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualSound"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualSound")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualSoundType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualSound(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualRecording"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualRecording")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualRecordingType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualRecording(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualVocals"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualVocals")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualVocalsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualVocals(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationPerceptualAttributeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("PerceptualLanguage")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualLanguageType; // FTT, check this
	}
	this->SetPerceptualLanguage(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualSound")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualSoundType; // FTT, check this
	}
	this->SetPerceptualSound(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualRecording")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualRecordingType; // FTT, check this
	}
	this->SetPerceptualRecording(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualVocals")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualVocalsType; // FTT, check this
	}
	this->SetPerceptualVocals(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationPerceptualAttributeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPerceptualLanguage() != Dc1NodePtr())
		result.Insert(GetPerceptualLanguage());
	if (GetPerceptualSound() != Dc1NodePtr())
		result.Insert(GetPerceptualSound());
	if (GetPerceptualRecording() != Dc1NodePtr())
		result.Insert(GetPerceptualRecording());
	if (GetPerceptualVocals() != Dc1NodePtr())
		result.Insert(GetPerceptualVocals());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationPerceptualAttributeType_ExtMethodImpl.h


