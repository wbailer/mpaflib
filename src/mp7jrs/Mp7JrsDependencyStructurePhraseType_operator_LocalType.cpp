
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtImplInclude.h


#include "Mp7JrstermAliasReferenceType.h"
#include "Mp7JrstermURIReferenceType.h"
#include "Mp7JrsDependencyStructurePhraseType_operator_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsDependencyStructurePhraseType_operator_LocalType::IMp7JrsDependencyStructurePhraseType_operator_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtPropInit.h

}

IMp7JrsDependencyStructurePhraseType_operator_LocalType::~IMp7JrsDependencyStructurePhraseType_operator_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtPropCleanup.h

}

Mp7JrsDependencyStructurePhraseType_operator_LocalType::Mp7JrsDependencyStructurePhraseType_operator_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDependencyStructurePhraseType_operator_LocalType::~Mp7JrsDependencyStructurePhraseType_operator_LocalType()
{
	Cleanup();
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_dependencyOperatorType = Mp7JrsdependencyOperatorType::UninitializedEnumeration; // Enumeration
	m_termAliasReferenceType = Mp7JrstermAliasReferencePtr(); // Pattern
	m_termURIReferenceType = Mp7JrstermURIReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtMyPropInit.h

}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_termAliasReferenceType);
	// Dc1Factory::DeleteObject(m_termURIReferenceType);
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DependencyStructurePhraseType_operator_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IDependencyStructurePhraseType_operator_LocalType
	const Dc1Ptr< Mp7JrsDependencyStructurePhraseType_operator_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if(tmp->GetdependencyOperatorType() != Mp7JrsdependencyOperatorType::UninitializedEnumeration)
	{
		this->SetdependencyOperatorType(tmp->GetdependencyOperatorType());
		return;
	}
	if(tmp->GettermAliasReferenceType() != Mp7JrstermAliasReferencePtr())
	{
		this->SettermAliasReferenceType(Dc1Factory::CloneObject(tmp->GettermAliasReferenceType()));
		return;
	}
	if(tmp->GettermURIReferenceType() != Mp7JrstermURIReferencePtr())
	{
		this->SettermURIReferenceType(Dc1Factory::CloneObject(tmp->GettermURIReferenceType()));
		return; 
	}
	}
}

Dc1NodePtr Mp7JrsDependencyStructurePhraseType_operator_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsDependencyStructurePhraseType_operator_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsdependencyOperatorType::Enumeration Mp7JrsDependencyStructurePhraseType_operator_LocalType::GetdependencyOperatorType() const
{
		return m_dependencyOperatorType;
}

Mp7JrstermAliasReferencePtr Mp7JrsDependencyStructurePhraseType_operator_LocalType::GettermAliasReferenceType() const
{
		return m_termAliasReferenceType;
}

Mp7JrstermURIReferencePtr Mp7JrsDependencyStructurePhraseType_operator_LocalType::GettermURIReferenceType() const
{
		return m_termURIReferenceType;
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::SetdependencyOperatorType(Mp7JrsdependencyOperatorType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType_operator_LocalType::SetdependencyOperatorType().");
	}
	if (m_dependencyOperatorType != item)
	{
		// nothing to free here, hopefully
		m_dependencyOperatorType = item;
	// Dc1Factory::DeleteObject(m_termAliasReferenceType);
	m_termAliasReferenceType = Mp7JrstermAliasReferencePtr();
	// Dc1Factory::DeleteObject(m_termURIReferenceType);
	m_termURIReferenceType = Mp7JrstermURIReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::SettermAliasReferenceType(const Mp7JrstermAliasReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType_operator_LocalType::SettermAliasReferenceType().");
	}
	if (m_termAliasReferenceType != item)
	{
		// Dc1Factory::DeleteObject(m_termAliasReferenceType);
		m_termAliasReferenceType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_termAliasReferenceType) m_termAliasReferenceType->SetParent(m_myself.getPointer());
		if (m_termAliasReferenceType && m_termAliasReferenceType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termAliasReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_termAliasReferenceType->UseTypeAttribute = true;
		}
		if(m_termAliasReferenceType != Mp7JrstermAliasReferencePtr())
		{
			m_termAliasReferenceType->SetContentName(XMLString::transcode("termAliasReferenceType"));
		}
	
	m_dependencyOperatorType = Mp7JrsdependencyOperatorType::UninitializedEnumeration;
	// Dc1Factory::DeleteObject(m_termURIReferenceType);
	m_termURIReferenceType = Mp7JrstermURIReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::SettermURIReferenceType(const Mp7JrstermURIReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType_operator_LocalType::SettermURIReferenceType().");
	}
	if (m_termURIReferenceType != item)
	{
		// Dc1Factory::DeleteObject(m_termURIReferenceType);
		m_termURIReferenceType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_termURIReferenceType) m_termURIReferenceType->SetParent(m_myself.getPointer());
		if (m_termURIReferenceType && m_termURIReferenceType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termURIReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_termURIReferenceType->UseTypeAttribute = true;
		}
		if(m_termURIReferenceType != Mp7JrstermURIReferencePtr())
		{
			m_termURIReferenceType->SetContentName(XMLString::transcode("termURIReferenceType"));
		}
	
	m_dependencyOperatorType = Mp7JrsdependencyOperatorType::UninitializedEnumeration;
	// Dc1Factory::DeleteObject(m_termAliasReferenceType);
	m_termAliasReferenceType = Mp7JrstermAliasReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDependencyStructurePhraseType_operator_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("termAliasReferenceType")) == 0)
	{
		// termAliasReferenceType is simple element termAliasReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettermAliasReferenceType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termAliasReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrstermAliasReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettermAliasReferenceType(p, client);
					if((p = GettermAliasReferenceType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("termURIReferenceType")) == 0)
	{
		// termURIReferenceType is simple element termURIReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GettermURIReferenceType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:termURIReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrstermURIReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SettermURIReferenceType(p, client);
					if((p = GettermURIReferenceType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DependencyStructurePhraseType_operator_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DependencyStructurePhraseType_operator_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsDependencyStructurePhraseType_operator_LocalType::ToText() const
{
	if(m_dependencyOperatorType != Mp7JrsdependencyOperatorType::UninitializedEnumeration)
	{
		return Mp7JrsdependencyOperatorType::ToText(m_dependencyOperatorType);
	}
	if(m_termAliasReferenceType != Mp7JrstermAliasReferencePtr())
	{
		return m_termAliasReferenceType->ToText();
	}
	if(m_termURIReferenceType != Mp7JrstermURIReferencePtr())
	{
		return m_termURIReferenceType->ToText();
	}
	return XMLString::transcode("");
}


bool Mp7JrsDependencyStructurePhraseType_operator_LocalType::Parse(const XMLCh * const txt)
{
	// Parse stu enumeration
	{
		Mp7JrsdependencyOperatorType::Enumeration tmp = Mp7JrsdependencyOperatorType::Parse(txt);
		if(tmp != Mp7JrsdependencyOperatorType::UninitializedEnumeration)
		{
			this->SetdependencyOperatorType(tmp);
			return true;
		}
	}
	// Parse stu pattern type
	{
		Mp7JrstermAliasReferencePtr tmp = CreatetermAliasReferenceType; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SettermAliasReferenceType(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	// Parse stu factory type
	{
		Mp7JrstermURIReferencePtr tmp = CreatetermURIReferenceType; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SettermURIReferenceType(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	return false;
}

bool Mp7JrsDependencyStructurePhraseType_operator_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsDependencyStructurePhraseType_operator_LocalType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsDependencyStructurePhraseType_operator_LocalType::ContentToString() const
{
	// Mp7JrsDependencyStructurePhraseType_operator_LocalType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsDependencyStructurePhraseType_operator_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DependencyStructurePhraseType_operator_LocalType"));
	// Element serialization:
	if(m_dependencyOperatorType != Mp7JrsdependencyOperatorType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsdependencyOperatorType::ToText(m_dependencyOperatorType);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("dependencyOperatorType"), false);
		XMLString::release(&tmp);
	}
	if(m_termAliasReferenceType != Mp7JrstermAliasReferencePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("termAliasReferenceType");
//		m_termAliasReferenceType->SetContentName(contentname);
		m_termAliasReferenceType->UseTypeAttribute = this->UseTypeAttribute;
		m_termAliasReferenceType->Serialize(doc, element);
	}
	// Class
	
	if (m_termURIReferenceType != Mp7JrstermURIReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_termURIReferenceType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("termURIReferenceType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_termURIReferenceType->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsDependencyStructurePhraseType_operator_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("dependencyOperatorType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("dependencyOperatorType")) == 0))
		{
			Mp7JrsdependencyOperatorType::Enumeration tmp = Mp7JrsdependencyOperatorType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsdependencyOperatorType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetdependencyOperatorType(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("termAliasReferenceType"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("termAliasReferenceType")) == 0))
		{
			Mp7JrstermAliasReferencePtr tmp = CreatetermAliasReferenceType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettermAliasReferenceType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("termURIReferenceType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("termURIReferenceType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatetermURIReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SettermURIReferenceType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDependencyStructurePhraseType_operator_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("termAliasReferenceType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetermAliasReferenceType; // FTT, check this
	}
	this->SettermAliasReferenceType(child);
  }
  if (XMLString::compareString(elementname, X("termURIReferenceType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetermURIReferenceType; // FTT, check this
	}
	this->SettermURIReferenceType(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDependencyStructurePhraseType_operator_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GettermAliasReferenceType() != Dc1NodePtr())
		result.Insert(GettermAliasReferenceType());
	if (GettermURIReferenceType() != Dc1NodePtr())
		result.Insert(GettermURIReferenceType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_operator_LocalType_ExtMethodImpl.h


