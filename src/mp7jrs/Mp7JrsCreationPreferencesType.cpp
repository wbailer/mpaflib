
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsCreationPreferencesType_Title_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Creator_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Keyword_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Location_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_DatePeriod_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Tool_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsCreationPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsCreationPreferencesType_Title_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Title
#include "Mp7JrsCreationPreferencesType_Creator_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Creator
#include "Mp7JrsCreationPreferencesType_Keyword_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Keyword
#include "Mp7JrsCreationPreferencesType_Location_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Location
#include "Mp7JrsCreationPreferencesType_DatePeriod_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DatePeriod
#include "Mp7JrsCreationPreferencesType_Tool_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Tool

#include <assert.h>
IMp7JrsCreationPreferencesType::IMp7JrsCreationPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtPropInit.h

}

IMp7JrsCreationPreferencesType::~IMp7JrsCreationPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtPropCleanup.h

}

Mp7JrsCreationPreferencesType::Mp7JrsCreationPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreationPreferencesType::~Mp7JrsCreationPreferencesType()
{
	Cleanup();
}

void Mp7JrsCreationPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Title = Mp7JrsCreationPreferencesType_Title_CollectionPtr(); // Collection
	m_Creator = Mp7JrsCreationPreferencesType_Creator_CollectionPtr(); // Collection
	m_Keyword = Mp7JrsCreationPreferencesType_Keyword_CollectionPtr(); // Collection
	m_Location = Mp7JrsCreationPreferencesType_Location_CollectionPtr(); // Collection
	m_DatePeriod = Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr(); // Collection
	m_Tool = Mp7JrsCreationPreferencesType_Tool_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtMyPropInit.h

}

void Mp7JrsCreationPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_Title);
	// Dc1Factory::DeleteObject(m_Creator);
	// Dc1Factory::DeleteObject(m_Keyword);
	// Dc1Factory::DeleteObject(m_Location);
	// Dc1Factory::DeleteObject(m_DatePeriod);
	// Dc1Factory::DeleteObject(m_Tool);
}

void Mp7JrsCreationPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreationPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in ICreationPreferencesType
	const Dc1Ptr< Mp7JrsCreationPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCreationPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_Title);
		this->SetTitle(Dc1Factory::CloneObject(tmp->GetTitle()));
		// Dc1Factory::DeleteObject(m_Creator);
		this->SetCreator(Dc1Factory::CloneObject(tmp->GetCreator()));
		// Dc1Factory::DeleteObject(m_Keyword);
		this->SetKeyword(Dc1Factory::CloneObject(tmp->GetKeyword()));
		// Dc1Factory::DeleteObject(m_Location);
		this->SetLocation(Dc1Factory::CloneObject(tmp->GetLocation()));
		// Dc1Factory::DeleteObject(m_DatePeriod);
		this->SetDatePeriod(Dc1Factory::CloneObject(tmp->GetDatePeriod()));
		// Dc1Factory::DeleteObject(m_Tool);
		this->SetTool(Dc1Factory::CloneObject(tmp->GetTool()));
}

Dc1NodePtr Mp7JrsCreationPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCreationPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsCreationPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrspreferenceValuePtr Mp7JrsCreationPreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsCreationPreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsCreationPreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsCreationPreferencesType_Title_CollectionPtr Mp7JrsCreationPreferencesType::GetTitle() const
{
		return m_Title;
}

Mp7JrsCreationPreferencesType_Creator_CollectionPtr Mp7JrsCreationPreferencesType::GetCreator() const
{
		return m_Creator;
}

Mp7JrsCreationPreferencesType_Keyword_CollectionPtr Mp7JrsCreationPreferencesType::GetKeyword() const
{
		return m_Keyword;
}

Mp7JrsCreationPreferencesType_Location_CollectionPtr Mp7JrsCreationPreferencesType::GetLocation() const
{
		return m_Location;
}

Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr Mp7JrsCreationPreferencesType::GetDatePeriod() const
{
		return m_DatePeriod;
}

Mp7JrsCreationPreferencesType_Tool_CollectionPtr Mp7JrsCreationPreferencesType::GetTool() const
{
		return m_Tool;
}

void Mp7JrsCreationPreferencesType::SetTitle(const Mp7JrsCreationPreferencesType_Title_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetTitle().");
	}
	if (m_Title != item)
	{
		// Dc1Factory::DeleteObject(m_Title);
		m_Title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Title) m_Title->SetParent(m_myself.getPointer());
		if(m_Title != Mp7JrsCreationPreferencesType_Title_CollectionPtr())
		{
			m_Title->SetContentName(XMLString::transcode("Title"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::SetCreator(const Mp7JrsCreationPreferencesType_Creator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetCreator().");
	}
	if (m_Creator != item)
	{
		// Dc1Factory::DeleteObject(m_Creator);
		m_Creator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Creator) m_Creator->SetParent(m_myself.getPointer());
		if(m_Creator != Mp7JrsCreationPreferencesType_Creator_CollectionPtr())
		{
			m_Creator->SetContentName(XMLString::transcode("Creator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::SetKeyword(const Mp7JrsCreationPreferencesType_Keyword_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetKeyword().");
	}
	if (m_Keyword != item)
	{
		// Dc1Factory::DeleteObject(m_Keyword);
		m_Keyword = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Keyword) m_Keyword->SetParent(m_myself.getPointer());
		if(m_Keyword != Mp7JrsCreationPreferencesType_Keyword_CollectionPtr())
		{
			m_Keyword->SetContentName(XMLString::transcode("Keyword"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::SetLocation(const Mp7JrsCreationPreferencesType_Location_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetLocation().");
	}
	if (m_Location != item)
	{
		// Dc1Factory::DeleteObject(m_Location);
		m_Location = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Location) m_Location->SetParent(m_myself.getPointer());
		if(m_Location != Mp7JrsCreationPreferencesType_Location_CollectionPtr())
		{
			m_Location->SetContentName(XMLString::transcode("Location"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::SetDatePeriod(const Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetDatePeriod().");
	}
	if (m_DatePeriod != item)
	{
		// Dc1Factory::DeleteObject(m_DatePeriod);
		m_DatePeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DatePeriod) m_DatePeriod->SetParent(m_myself.getPointer());
		if(m_DatePeriod != Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr())
		{
			m_DatePeriod->SetContentName(XMLString::transcode("DatePeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::SetTool(const Mp7JrsCreationPreferencesType_Tool_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetTool().");
	}
	if (m_Tool != item)
	{
		// Dc1Factory::DeleteObject(m_Tool);
		m_Tool = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Tool) m_Tool->SetParent(m_myself.getPointer());
		if(m_Tool != Mp7JrsCreationPreferencesType_Tool_CollectionPtr())
		{
			m_Tool->SetContentName(XMLString::transcode("Tool"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsCreationPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsCreationPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsCreationPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsCreationPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsCreationPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsCreationPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsCreationPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsCreationPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsCreationPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsCreationPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsCreationPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsCreationPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsCreationPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsCreationPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsCreationPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsCreationPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsCreationPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCreationPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Title")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Title is item of type CreationPreferencesType_Title_LocalType
		// in element collection CreationPreferencesType_Title_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_Title_CollectionPtr coll = GetTitle();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_Title_CollectionType; // FTT, check this
				SetTitle(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Title_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_Title_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Creator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Creator is item of type CreationPreferencesType_Creator_LocalType
		// in element collection CreationPreferencesType_Creator_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_Creator_CollectionPtr coll = GetCreator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_Creator_CollectionType; // FTT, check this
				SetCreator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Creator_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_Creator_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Keyword")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Keyword is item of type CreationPreferencesType_Keyword_LocalType
		// in element collection CreationPreferencesType_Keyword_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_Keyword_CollectionPtr coll = GetKeyword();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_Keyword_CollectionType; // FTT, check this
				SetKeyword(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Keyword_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_Keyword_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Location")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Location is item of type CreationPreferencesType_Location_LocalType
		// in element collection CreationPreferencesType_Location_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_Location_CollectionPtr coll = GetLocation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_Location_CollectionType; // FTT, check this
				SetLocation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Location_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_Location_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DatePeriod")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DatePeriod is item of type CreationPreferencesType_DatePeriod_LocalType
		// in element collection CreationPreferencesType_DatePeriod_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr coll = GetDatePeriod();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_DatePeriod_CollectionType; // FTT, check this
				SetDatePeriod(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_DatePeriod_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_DatePeriod_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Tool")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Tool is item of type CreationPreferencesType_Tool_LocalType
		// in element collection CreationPreferencesType_Tool_CollectionType
		
		context->Found = true;
		Mp7JrsCreationPreferencesType_Tool_CollectionPtr coll = GetTool();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreationPreferencesType_Tool_CollectionType; // FTT, check this
				SetTool(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType_Tool_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesType_Tool_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreationPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreationPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsCreationPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsCreationPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsCreationPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreationPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Title != Mp7JrsCreationPreferencesType_Title_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Title->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Creator != Mp7JrsCreationPreferencesType_Creator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Creator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Keyword != Mp7JrsCreationPreferencesType_Keyword_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Keyword->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Location != Mp7JrsCreationPreferencesType_Location_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Location->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DatePeriod != Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DatePeriod->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Tool != Mp7JrsCreationPreferencesType_Tool_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Tool->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCreationPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Title")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_Title_CollectionPtr tmp = CreateCreationPreferencesType_Title_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTitle(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Creator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Creator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_Creator_CollectionPtr tmp = CreateCreationPreferencesType_Creator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Keyword"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Keyword")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_Keyword_CollectionPtr tmp = CreateCreationPreferencesType_Keyword_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetKeyword(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Location"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Location")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_Location_CollectionPtr tmp = CreateCreationPreferencesType_Location_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLocation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DatePeriod"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DatePeriod")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr tmp = CreateCreationPreferencesType_DatePeriod_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDatePeriod(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Tool"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Tool")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreationPreferencesType_Tool_CollectionPtr tmp = CreateCreationPreferencesType_Tool_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTool(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreationPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Title")) == 0))
  {
	Mp7JrsCreationPreferencesType_Title_CollectionPtr tmp = CreateCreationPreferencesType_Title_CollectionType; // FTT, check this
	this->SetTitle(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Creator")) == 0))
  {
	Mp7JrsCreationPreferencesType_Creator_CollectionPtr tmp = CreateCreationPreferencesType_Creator_CollectionType; // FTT, check this
	this->SetCreator(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Keyword")) == 0))
  {
	Mp7JrsCreationPreferencesType_Keyword_CollectionPtr tmp = CreateCreationPreferencesType_Keyword_CollectionType; // FTT, check this
	this->SetKeyword(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Location")) == 0))
  {
	Mp7JrsCreationPreferencesType_Location_CollectionPtr tmp = CreateCreationPreferencesType_Location_CollectionType; // FTT, check this
	this->SetLocation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DatePeriod")) == 0))
  {
	Mp7JrsCreationPreferencesType_DatePeriod_CollectionPtr tmp = CreateCreationPreferencesType_DatePeriod_CollectionType; // FTT, check this
	this->SetDatePeriod(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Tool")) == 0))
  {
	Mp7JrsCreationPreferencesType_Tool_CollectionPtr tmp = CreateCreationPreferencesType_Tool_CollectionType; // FTT, check this
	this->SetTool(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreationPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTitle() != Dc1NodePtr())
		result.Insert(GetTitle());
	if (GetCreator() != Dc1NodePtr())
		result.Insert(GetCreator());
	if (GetKeyword() != Dc1NodePtr())
		result.Insert(GetKeyword());
	if (GetLocation() != Dc1NodePtr())
		result.Insert(GetLocation());
	if (GetDatePeriod() != Dc1NodePtr())
		result.Insert(GetDatePeriod());
	if (GetTool() != Dc1NodePtr())
		result.Insert(GetTool());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_ExtMethodImpl.h


