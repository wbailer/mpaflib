
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtImplInclude.h


#include "Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType.h"
#include "Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType.h"
#include "Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType.h"
#include "Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsmediaTimePointType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaTimePoint
#include "Mp7JrsMediaRelTimePointType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaRelTimePoint
#include "Mp7JrsMediaRelIncrTimePointType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePoint

#include <assert.h>
IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtPropInit.h

}

IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::~IMp7JrsTemporalInterpolationType_KeyTimePoint_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtPropCleanup.h

}

Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::~Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType()
{
	Cleanup();
}

void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr(); // Collection
	m_MediaRelTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr(); // Collection
	m_MediaRelIncrTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtMyPropInit.h

}

void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
}

void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TemporalInterpolationType_KeyTimePoint_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ITemporalInterpolationType_KeyTimePoint_LocalType
	const Dc1Ptr< Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		this->SetMediaTimePoint(Dc1Factory::CloneObject(tmp->GetMediaTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		this->SetMediaRelTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		this->SetMediaRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelIncrTimePoint()));
}

Dc1NodePtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetMediaTimePoint() const
{
		return m_MediaTimePoint;
}

Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetMediaRelTimePoint() const
{
		return m_MediaRelTimePoint;
}

Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetMediaRelIncrTimePoint() const
{
		return m_MediaRelIncrTimePoint;
}

// implementing setter for choice 
/* element */
void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaTimePoint(const Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaTimePoint().");
	}
	if (m_MediaTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		m_MediaTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimePoint) m_MediaTimePoint->SetParent(m_myself.getPointer());
		if(m_MediaTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr())
		{
			m_MediaTimePoint->SetContentName(XMLString::transcode("MediaTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaRelTimePoint(const Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaRelTimePoint().");
	}
	if (m_MediaRelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		m_MediaRelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelTimePoint) m_MediaRelTimePoint->SetParent(m_myself.getPointer());
		if(m_MediaRelTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr())
		{
			m_MediaRelTimePoint->SetContentName(XMLString::transcode("MediaRelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaRelIncrTimePoint(const Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::SetMediaRelIncrTimePoint().");
	}
	if (m_MediaRelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		m_MediaRelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelIncrTimePoint) m_MediaRelIncrTimePoint->SetParent(m_myself.getPointer());
		if(m_MediaRelIncrTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr())
		{
			m_MediaRelIncrTimePoint->SetContentName(XMLString::transcode("MediaRelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTimePoint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaTimePoint is item of type mediaTimePointType
		// in element collection TemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType
		
		context->Found = true;
		Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr coll = GetMediaTimePoint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType; // FTT, check this
				SetMediaTimePoint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65535))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelTimePoint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaRelTimePoint is item of type MediaRelTimePointType
		// in element collection TemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType
		
		context->Found = true;
		Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr coll = GetMediaRelTimePoint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType; // FTT, check this
				SetMediaRelTimePoint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65535))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaRelIncrTimePoint")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePoint is item of type MediaRelIncrTimePointType
		// in element collection TemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType
		
		context->Found = true;
		Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr coll = GetMediaRelIncrTimePoint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType; // FTT, check this
				SetMediaRelIncrTimePoint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65535))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TemporalInterpolationType_KeyTimePoint_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TemporalInterpolationType_KeyTimePoint_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TemporalInterpolationType_KeyTimePoint_LocalType"));
	// Element serialization:
	if (m_MediaTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaTimePoint->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaRelTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaRelTimePoint->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MediaRelIncrTimePoint != Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaRelIncrTimePoint->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaTimePoint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaTimePoint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaRelTimePoint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaRelTimePoint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaRelIncrTimePoint")) == 0))
		{
			// Deserialize factory type
			Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaRelIncrTimePoint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaTimePoint")) == 0))
  {
	Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaTimePoint_CollectionType; // FTT, check this
	this->SetMediaTimePoint(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaRelTimePoint")) == 0))
  {
	Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaRelTimePoint_CollectionType; // FTT, check this
	this->SetMediaRelTimePoint(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaRelIncrTimePoint")) == 0))
  {
	Mp7JrsTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionPtr tmp = CreateTemporalInterpolationType_KeyTimePoint_MediaRelIncrTimePoint_CollectionType; // FTT, check this
	this->SetMediaRelIncrTimePoint(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaTimePoint());
	if (GetMediaRelTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelTimePoint());
	if (GetMediaRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelIncrTimePoint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTemporalInterpolationType_KeyTimePoint_LocalType_ExtMethodImpl.h


