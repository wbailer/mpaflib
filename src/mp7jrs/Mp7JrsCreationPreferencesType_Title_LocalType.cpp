
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtImplInclude.h


#include "Mp7JrsTitleType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsTitleType_type_LocalType2.h"
#include "Mp7JrsTextualBaseType_phoneticTranscription_CollectionType.h"
#include "Mp7JrsCreationPreferencesType_Title_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsCreationPreferencesType_Title_LocalType::IMp7JrsCreationPreferencesType_Title_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtPropInit.h

}

IMp7JrsCreationPreferencesType_Title_LocalType::~IMp7JrsCreationPreferencesType_Title_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtPropCleanup.h

}

Mp7JrsCreationPreferencesType_Title_LocalType::Mp7JrsCreationPreferencesType_Title_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreationPreferencesType_Title_LocalType::~Mp7JrsCreationPreferencesType_Title_LocalType()
{
	Cleanup();
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Init()
{
	// Init base
	m_Base = CreateTitleType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;



// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtMyPropInit.h

}

void Mp7JrsCreationPreferencesType_Title_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
}

void Mp7JrsCreationPreferencesType_Title_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreationPreferencesType_Title_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ICreationPreferencesType_Title_LocalType
	const Dc1Ptr< Mp7JrsCreationPreferencesType_Title_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTitlePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCreationPreferencesType_Title_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
}

Dc1NodePtr Mp7JrsCreationPreferencesType_Title_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCreationPreferencesType_Title_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTitleType > Mp7JrsCreationPreferencesType_Title_LocalType::GetBase() const
{
	return m_Base;
}

void Mp7JrsCreationPreferencesType_Title_LocalType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::SetContent().");
	}
	GetBase()->GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::GetContent() const
{
	return GetBase()->GetBase()->GetContent();
}
Mp7JrspreferenceValuePtr Mp7JrsCreationPreferencesType_Title_LocalType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsCreationPreferencesType_Title_LocalType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsTitleType_type_Local2Ptr Mp7JrsCreationPreferencesType_Title_LocalType::Gettype() const
{
	return GetBase()->Gettype();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Existtype() const
{
	return GetBase()->Existtype();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::Settype(const Mp7JrsTitleType_type_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Invalidatetype()
{
	GetBase()->Invalidatetype();
}
XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::Getlang() const
{
	return GetBase()->GetBase()->Getlang();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Existlang() const
{
	return GetBase()->GetBase()->Existlang();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::Setlang().");
	}
	GetBase()->GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Invalidatelang()
{
	GetBase()->GetBase()->Invalidatelang();
}
Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr Mp7JrsCreationPreferencesType_Title_LocalType::GetphoneticTranscription() const
{
	return GetBase()->GetBase()->GetphoneticTranscription();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::ExistphoneticTranscription() const
{
	return GetBase()->GetBase()->ExistphoneticTranscription();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::SetphoneticTranscription(const Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::SetphoneticTranscription().");
	}
	GetBase()->GetBase()->SetphoneticTranscription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::InvalidatephoneticTranscription()
{
	GetBase()->GetBase()->InvalidatephoneticTranscription();
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsCreationPreferencesType_Title_LocalType::GetphoneticAlphabet() const
{
	return GetBase()->GetBase()->GetphoneticAlphabet();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::ExistphoneticAlphabet() const
{
	return GetBase()->GetBase()->ExistphoneticAlphabet();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::SetphoneticAlphabet().");
	}
	GetBase()->GetBase()->SetphoneticAlphabet(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::InvalidatephoneticAlphabet()
{
	GetBase()->GetBase()->InvalidatephoneticAlphabet();
}
XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::Getcharset() const
{
	return GetBase()->GetBase()->Getcharset();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Existcharset() const
{
	return GetBase()->GetBase()->Existcharset();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::Setcharset().");
	}
	GetBase()->GetBase()->Setcharset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Invalidatecharset()
{
	GetBase()->GetBase()->Invalidatecharset();
}
XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::Getencoding() const
{
	return GetBase()->GetBase()->Getencoding();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Existencoding() const
{
	return GetBase()->GetBase()->Existencoding();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::Setencoding().");
	}
	GetBase()->GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Invalidateencoding()
{
	GetBase()->GetBase()->Invalidateencoding();
}
XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::Getscript() const
{
	return GetBase()->GetBase()->Getscript();
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Existscript() const
{
	return GetBase()->GetBase()->Existscript();
}
void Mp7JrsCreationPreferencesType_Title_LocalType::Setscript(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationPreferencesType_Title_LocalType::Setscript().");
	}
	GetBase()->GetBase()->Setscript(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Invalidatescript()
{
	GetBase()->GetBase()->Invalidatescript();
}

Dc1NodeEnum Mp7JrsCreationPreferencesType_Title_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreationPreferencesType_Title_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreationPreferencesType_Title_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsCreationPreferencesType_Title_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsCreationPreferencesType_Title_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsCreationPreferencesType_Title_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsCreationPreferencesType_Title_LocalType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsCreationPreferencesType_Title_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreationPreferencesType_Title_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsCreationPreferencesType_Title_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsTitleType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreationPreferencesType_Title_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreationPreferencesType_Title_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreationPreferencesType_Title_LocalType_ExtMethodImpl.h


