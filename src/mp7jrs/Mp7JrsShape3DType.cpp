
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7Jrsunsigned4.h"
#include "Mp7JrsShape3DType_Spectrum_LocalType.h"
#include "Mp7Jrsunsigned12.h"
#include "Mp7JrsShape3DType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsShape3DType::IMp7JrsShape3DType()
{

// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtPropInit.h

}

IMp7JrsShape3DType::~IMp7JrsShape3DType()
{
// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtPropCleanup.h

}

Mp7JrsShape3DType::Mp7JrsShape3DType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsShape3DType::~Mp7JrsShape3DType()
{
	Cleanup();
}

void Mp7JrsShape3DType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_bitsPerBin = Createunsigned4; // Create a valid class, FTT, check this
	m_bitsPerBin->SetContent(0); // Use Value initialiser (xxx2)
	m_bitsPerBin_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Spectrum = Mp7JrsShape3DType_Spectrum_LocalPtr(); // Class
	m_PlanarSurfaces = Mp7Jrsunsigned12Ptr(); // Class with content 
	m_SingularSurfaces = Mp7Jrsunsigned12Ptr(); // Class with content 


// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtMyPropInit.h

}

void Mp7JrsShape3DType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_bitsPerBin);
	// Dc1Factory::DeleteObject(m_Spectrum);
	// Dc1Factory::DeleteObject(m_PlanarSurfaces);
	// Dc1Factory::DeleteObject(m_SingularSurfaces);
}

void Mp7JrsShape3DType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Shape3DTypePtr, since we
	// might need GetBase(), which isn't defined in IShape3DType
	const Dc1Ptr< Mp7JrsShape3DType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsShape3DType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_bitsPerBin);
	if (tmp->ExistbitsPerBin())
	{
		this->SetbitsPerBin(Dc1Factory::CloneObject(tmp->GetbitsPerBin()));
	}
	else
	{
		InvalidatebitsPerBin();
	}
	}
		// Dc1Factory::DeleteObject(m_Spectrum);
		this->SetSpectrum(Dc1Factory::CloneObject(tmp->GetSpectrum()));
		// Dc1Factory::DeleteObject(m_PlanarSurfaces);
		this->SetPlanarSurfaces(Dc1Factory::CloneObject(tmp->GetPlanarSurfaces()));
		// Dc1Factory::DeleteObject(m_SingularSurfaces);
		this->SetSingularSurfaces(Dc1Factory::CloneObject(tmp->GetSingularSurfaces()));
}

Dc1NodePtr Mp7JrsShape3DType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsShape3DType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsShape3DType::GetBase() const
{
	return m_Base;
}

Mp7Jrsunsigned4Ptr Mp7JrsShape3DType::GetbitsPerBin() const
{
	if (this->ExistbitsPerBin()) {
		return m_bitsPerBin;
	} else {
		return m_bitsPerBin_Default;
	}
}

bool Mp7JrsShape3DType::ExistbitsPerBin() const
{
	return m_bitsPerBin_Exist;
}
void Mp7JrsShape3DType::SetbitsPerBin(const Mp7Jrsunsigned4Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShape3DType::SetbitsPerBin().");
	}
	m_bitsPerBin = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_bitsPerBin) m_bitsPerBin->SetParent(m_myself.getPointer());
	m_bitsPerBin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShape3DType::InvalidatebitsPerBin()
{
	m_bitsPerBin_Exist = false;
}
Mp7JrsShape3DType_Spectrum_LocalPtr Mp7JrsShape3DType::GetSpectrum() const
{
		return m_Spectrum;
}

Mp7Jrsunsigned12Ptr Mp7JrsShape3DType::GetPlanarSurfaces() const
{
		return m_PlanarSurfaces;
}

Mp7Jrsunsigned12Ptr Mp7JrsShape3DType::GetSingularSurfaces() const
{
		return m_SingularSurfaces;
}

void Mp7JrsShape3DType::SetSpectrum(const Mp7JrsShape3DType_Spectrum_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShape3DType::SetSpectrum().");
	}
	if (m_Spectrum != item)
	{
		// Dc1Factory::DeleteObject(m_Spectrum);
		m_Spectrum = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Spectrum) m_Spectrum->SetParent(m_myself.getPointer());
		if (m_Spectrum && m_Spectrum->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Shape3DType_Spectrum_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Spectrum->UseTypeAttribute = true;
		}
		if(m_Spectrum != Mp7JrsShape3DType_Spectrum_LocalPtr())
		{
			m_Spectrum->SetContentName(XMLString::transcode("Spectrum"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShape3DType::SetPlanarSurfaces(const Mp7Jrsunsigned12Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShape3DType::SetPlanarSurfaces().");
	}
	if (m_PlanarSurfaces != item)
	{
		// Dc1Factory::DeleteObject(m_PlanarSurfaces);
		m_PlanarSurfaces = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PlanarSurfaces) m_PlanarSurfaces->SetParent(m_myself.getPointer());
		if (m_PlanarSurfaces && m_PlanarSurfaces->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PlanarSurfaces->UseTypeAttribute = true;
		}
		if(m_PlanarSurfaces != Mp7Jrsunsigned12Ptr())
		{
			m_PlanarSurfaces->SetContentName(XMLString::transcode("PlanarSurfaces"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsShape3DType::SetSingularSurfaces(const Mp7Jrsunsigned12Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShape3DType::SetSingularSurfaces().");
	}
	if (m_SingularSurfaces != item)
	{
		// Dc1Factory::DeleteObject(m_SingularSurfaces);
		m_SingularSurfaces = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SingularSurfaces) m_SingularSurfaces->SetParent(m_myself.getPointer());
		if (m_SingularSurfaces && m_SingularSurfaces->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SingularSurfaces->UseTypeAttribute = true;
		}
		if(m_SingularSurfaces != Mp7Jrsunsigned12Ptr())
		{
			m_SingularSurfaces->SetContentName(XMLString::transcode("SingularSurfaces"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsShape3DType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("bitsPerBin")) == 0)
	{
		// bitsPerBin is simple attribute unsigned4
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned4Ptr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Spectrum")) == 0)
	{
		// Spectrum is simple element Shape3DType_Spectrum_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpectrum()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Shape3DType_Spectrum_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShape3DType_Spectrum_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpectrum(p, client);
					if((p = GetSpectrum()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PlanarSurfaces")) == 0)
	{
		// PlanarSurfaces is simple element unsigned12
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPlanarSurfaces()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned12Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPlanarSurfaces(p, client);
					if((p = GetPlanarSurfaces()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SingularSurfaces")) == 0)
	{
		// SingularSurfaces is simple element unsigned12
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSingularSurfaces()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned12")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned12Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSingularSurfaces(p, client);
					if((p = GetSingularSurfaces()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Shape3DType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Shape3DType");
	}
	return result;
}

XMLCh * Mp7JrsShape3DType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsShape3DType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsShape3DType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Shape3DType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_bitsPerBin_Exist)
	{
	// Class
	if (m_bitsPerBin != Mp7Jrsunsigned4Ptr())
	{
		XMLCh * tmp = m_bitsPerBin->ToText();
		element->setAttributeNS(X(""), X("bitsPerBin"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Spectrum != Mp7JrsShape3DType_Spectrum_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Spectrum->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Spectrum"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Spectrum->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PlanarSurfaces != Mp7Jrsunsigned12Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PlanarSurfaces->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PlanarSurfaces"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PlanarSurfaces->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_SingularSurfaces != Mp7Jrsunsigned12Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SingularSurfaces->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SingularSurfaces"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SingularSurfaces->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsShape3DType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("bitsPerBin")))
	{
		// Deserialize class type
		Mp7Jrsunsigned4Ptr tmp = Createunsigned4; // FTT, check this
		tmp->Parse(parent->getAttribute(X("bitsPerBin")));
		this->SetbitsPerBin(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Spectrum"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Spectrum")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShape3DType_Spectrum_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpectrum(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PlanarSurfaces"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PlanarSurfaces")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned12; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPlanarSurfaces(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SingularSurfaces"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SingularSurfaces")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned12; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSingularSurfaces(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsShape3DType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Spectrum")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShape3DType_Spectrum_LocalType; // FTT, check this
	}
	this->SetSpectrum(child);
  }
  if (XMLString::compareString(elementname, X("PlanarSurfaces")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned12; // FTT, check this
	}
	this->SetPlanarSurfaces(child);
  }
  if (XMLString::compareString(elementname, X("SingularSurfaces")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned12; // FTT, check this
	}
	this->SetSingularSurfaces(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsShape3DType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSpectrum() != Dc1NodePtr())
		result.Insert(GetSpectrum());
	if (GetPlanarSurfaces() != Dc1NodePtr())
		result.Insert(GetPlanarSurfaces());
	if (GetSingularSurfaces() != Dc1NodePtr())
		result.Insert(GetSingularSurfaces());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsShape3DType_ExtMethodImpl.h


