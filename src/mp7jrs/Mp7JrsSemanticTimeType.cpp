
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtImplInclude.h


#include "Mp7JrsSemanticBaseType.h"
#include "Mp7JrsTimeType.h"
#include "Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionType.h"
#include "Mp7JrsAbstractionLevelType.h"
#include "Mp7JrsSemanticBaseType_Label_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsSemanticBaseType_Property_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_CollectionType.h"
#include "Mp7JrsSemanticBaseType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSemanticTimeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Label
#include "Mp7JrsSemanticBaseType_MediaOccurrence_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaOccurrence
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SemanticTimeInterval

#include <assert.h>
IMp7JrsSemanticTimeType::IMp7JrsSemanticTimeType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtPropInit.h

}

IMp7JrsSemanticTimeType::~IMp7JrsSemanticTimeType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtPropCleanup.h

}

Mp7JrsSemanticTimeType::Mp7JrsSemanticTimeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticTimeType::~Mp7JrsSemanticTimeType()
{
	Cleanup();
}

void Mp7JrsSemanticTimeType::Init()
{
	// Init base
	m_Base = CreateSemanticBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Time = Mp7JrsTimePtr(); // Class
	m_Time_Exist = false;
	m_SemanticTimeInterval = Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtMyPropInit.h

}

void Mp7JrsSemanticTimeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Time);
	// Dc1Factory::DeleteObject(m_SemanticTimeInterval);
}

void Mp7JrsSemanticTimeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticTimeTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticTimeType
	const Dc1Ptr< Mp7JrsSemanticTimeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSemanticBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSemanticTimeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidTime())
	{
		// Dc1Factory::DeleteObject(m_Time);
		this->SetTime(Dc1Factory::CloneObject(tmp->GetTime()));
	}
	else
	{
		InvalidateTime();
	}
		// Dc1Factory::DeleteObject(m_SemanticTimeInterval);
		this->SetSemanticTimeInterval(Dc1Factory::CloneObject(tmp->GetSemanticTimeInterval()));
}

Dc1NodePtr Mp7JrsSemanticTimeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSemanticTimeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSemanticBaseType > Mp7JrsSemanticTimeType::GetBase() const
{
	return m_Base;
}

Mp7JrsTimePtr Mp7JrsSemanticTimeType::GetTime() const
{
		return m_Time;
}

// Element is optional
bool Mp7JrsSemanticTimeType::IsValidTime() const
{
	return m_Time_Exist;
}

Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr Mp7JrsSemanticTimeType::GetSemanticTimeInterval() const
{
		return m_SemanticTimeInterval;
}

void Mp7JrsSemanticTimeType::SetTime(const Mp7JrsTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetTime().");
	}
	if (m_Time != item || m_Time_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Time);
		m_Time = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Time) m_Time->SetParent(m_myself.getPointer());
		if (m_Time && m_Time->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Time->UseTypeAttribute = true;
		}
		m_Time_Exist = true;
		if(m_Time != Mp7JrsTimePtr())
		{
			m_Time->SetContentName(XMLString::transcode("Time"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidateTime()
{
	m_Time_Exist = false;
}
void Mp7JrsSemanticTimeType::SetSemanticTimeInterval(const Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetSemanticTimeInterval().");
	}
	if (m_SemanticTimeInterval != item)
	{
		// Dc1Factory::DeleteObject(m_SemanticTimeInterval);
		m_SemanticTimeInterval = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SemanticTimeInterval) m_SemanticTimeInterval->SetParent(m_myself.getPointer());
		if(m_SemanticTimeInterval != Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr())
		{
			m_SemanticTimeInterval->SetContentName(XMLString::transcode("SemanticTimeInterval"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsAbstractionLevelPtr Mp7JrsSemanticTimeType::GetAbstractionLevel() const
{
	return GetBase()->GetAbstractionLevel();
}

// Element is optional
bool Mp7JrsSemanticTimeType::IsValidAbstractionLevel() const
{
	return GetBase()->IsValidAbstractionLevel();
}

Mp7JrsSemanticBaseType_Label_CollectionPtr Mp7JrsSemanticTimeType::GetLabel() const
{
	return GetBase()->GetLabel();
}

Mp7JrsTextAnnotationPtr Mp7JrsSemanticTimeType::GetDefinition() const
{
	return GetBase()->GetDefinition();
}

// Element is optional
bool Mp7JrsSemanticTimeType::IsValidDefinition() const
{
	return GetBase()->IsValidDefinition();
}

Mp7JrsSemanticBaseType_Property_CollectionPtr Mp7JrsSemanticTimeType::GetProperty() const
{
	return GetBase()->GetProperty();
}

Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr Mp7JrsSemanticTimeType::GetMediaOccurrence() const
{
	return GetBase()->GetMediaOccurrence();
}

Mp7JrsSemanticBaseType_Relation_CollectionPtr Mp7JrsSemanticTimeType::GetRelation() const
{
	return GetBase()->GetRelation();
}

void Mp7JrsSemanticTimeType::SetAbstractionLevel(const Mp7JrsAbstractionLevelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetAbstractionLevel().");
	}
	GetBase()->SetAbstractionLevel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidateAbstractionLevel()
{
	GetBase()->InvalidateAbstractionLevel();
}
void Mp7JrsSemanticTimeType::SetLabel(const Mp7JrsSemanticBaseType_Label_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetLabel().");
	}
	GetBase()->SetLabel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::SetDefinition(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetDefinition().");
	}
	GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidateDefinition()
{
	GetBase()->InvalidateDefinition();
}
void Mp7JrsSemanticTimeType::SetProperty(const Mp7JrsSemanticBaseType_Property_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetProperty().");
	}
	GetBase()->SetProperty(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::SetMediaOccurrence(const Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetMediaOccurrence().");
	}
	GetBase()->SetMediaOccurrence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::SetRelation(const Mp7JrsSemanticBaseType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSemanticTimeType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSemanticTimeType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSemanticTimeType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSemanticTimeType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSemanticTimeType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSemanticTimeType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSemanticTimeType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSemanticTimeType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSemanticTimeType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSemanticTimeType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSemanticTimeType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSemanticTimeType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSemanticTimeType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSemanticTimeType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSemanticTimeType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSemanticTimeType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSemanticTimeType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSemanticTimeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Time")) == 0)
	{
		// Time is simple element TimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTime(p, client);
					if((p = GetTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SemanticTimeInterval")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SemanticTimeInterval is item of type SemanticTimeType_SemanticTimeInterval_LocalType
		// in element collection SemanticTimeType_SemanticTimeInterval_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr coll = GetSemanticTimeInterval();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticTimeType_SemanticTimeInterval_CollectionType; // FTT, check this
				SetSemanticTimeInterval(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticTimeType_SemanticTimeInterval_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticTimeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticTimeType");
	}
	return result;
}

XMLCh * Mp7JrsSemanticTimeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSemanticTimeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSemanticTimeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticTimeType"));
	// Element serialization:
	// Class
	
	if (m_Time != Mp7JrsTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Time->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Time"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Time->Serialize(doc, element, &elem);
	}
	if (m_SemanticTimeInterval != Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SemanticTimeInterval->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSemanticTimeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSemanticBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Time"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Time")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SemanticTimeInterval"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SemanticTimeInterval")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr tmp = CreateSemanticTimeType_SemanticTimeInterval_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSemanticTimeInterval(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticTimeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Time")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTimeType; // FTT, check this
	}
	this->SetTime(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SemanticTimeInterval")) == 0))
  {
	Mp7JrsSemanticTimeType_SemanticTimeInterval_CollectionPtr tmp = CreateSemanticTimeType_SemanticTimeInterval_CollectionType; // FTT, check this
	this->SetSemanticTimeInterval(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticTimeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTime() != Dc1NodePtr())
		result.Insert(GetTime());
	if (GetSemanticTimeInterval() != Dc1NodePtr())
		result.Insert(GetSemanticTimeInterval());
	if (GetAbstractionLevel() != Dc1NodePtr())
		result.Insert(GetAbstractionLevel());
	if (GetLabel() != Dc1NodePtr())
		result.Insert(GetLabel());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetProperty() != Dc1NodePtr())
		result.Insert(GetProperty());
	if (GetMediaOccurrence() != Dc1NodePtr())
		result.Insert(GetMediaOccurrence());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_ExtMethodImpl.h


