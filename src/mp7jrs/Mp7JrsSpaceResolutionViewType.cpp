
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtImplInclude.h


#include "Mp7JrsViewType.h"
#include "Mp7JrsPartitionType.h"
#include "Mp7JrsRegionLocatorType.h"
#include "Mp7JrsFilteringType.h"
#include "Mp7JrsSignalPlaneSampleType.h"
#include "Mp7JrsSignalType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSpaceResolutionViewType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsSpaceResolutionViewType::IMp7JrsSpaceResolutionViewType()
{

// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtPropInit.h

}

IMp7JrsSpaceResolutionViewType::~IMp7JrsSpaceResolutionViewType()
{
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtPropCleanup.h

}

Mp7JrsSpaceResolutionViewType::Mp7JrsSpaceResolutionViewType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpaceResolutionViewType::~Mp7JrsSpaceResolutionViewType()
{
	Cleanup();
}

void Mp7JrsSpaceResolutionViewType::Init()
{
	// Init base
	m_Base = CreateViewType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_order = Mp7JrsSpaceResolutionViewType_order_LocalType::UninitializedEnumeration;
	m_order_Default = Mp7JrsSpaceResolutionViewType_order_LocalType::spaceFrequency; // Default enumeration
	m_order_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SpacePartition = Mp7JrsPartitionPtr(); // Class
	m_SpaceRegion = Mp7JrsRegionLocatorPtr(); // Class
	m_FrequencyPartition = Mp7JrsPartitionPtr(); // Class
	m_FrequencyPartition_Exist = false;
	m_Filtering = Mp7JrsFilteringPtr(); // Class
	m_Filtering_Exist = false;
	m_DownSamplingFactor = Mp7JrsSignalPlaneSamplePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMyPropInit.h

}

void Mp7JrsSpaceResolutionViewType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SpacePartition);
	// Dc1Factory::DeleteObject(m_SpaceRegion);
	// Dc1Factory::DeleteObject(m_FrequencyPartition);
	// Dc1Factory::DeleteObject(m_Filtering);
	// Dc1Factory::DeleteObject(m_DownSamplingFactor);
}

void Mp7JrsSpaceResolutionViewType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpaceResolutionViewTypePtr, since we
	// might need GetBase(), which isn't defined in ISpaceResolutionViewType
	const Dc1Ptr< Mp7JrsSpaceResolutionViewType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsViewPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSpaceResolutionViewType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existorder())
	{
		this->Setorder(tmp->Getorder());
	}
	else
	{
		Invalidateorder();
	}
	}
		// Dc1Factory::DeleteObject(m_SpacePartition);
		this->SetSpacePartition(Dc1Factory::CloneObject(tmp->GetSpacePartition()));
		// Dc1Factory::DeleteObject(m_SpaceRegion);
		this->SetSpaceRegion(Dc1Factory::CloneObject(tmp->GetSpaceRegion()));
	if (tmp->IsValidFrequencyPartition())
	{
		// Dc1Factory::DeleteObject(m_FrequencyPartition);
		this->SetFrequencyPartition(Dc1Factory::CloneObject(tmp->GetFrequencyPartition()));
	}
	else
	{
		InvalidateFrequencyPartition();
	}
	if (tmp->IsValidFiltering())
	{
		// Dc1Factory::DeleteObject(m_Filtering);
		this->SetFiltering(Dc1Factory::CloneObject(tmp->GetFiltering()));
	}
	else
	{
		InvalidateFiltering();
	}
		// Dc1Factory::DeleteObject(m_DownSamplingFactor);
		this->SetDownSamplingFactor(Dc1Factory::CloneObject(tmp->GetDownSamplingFactor()));
}

Dc1NodePtr Mp7JrsSpaceResolutionViewType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSpaceResolutionViewType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsViewType > Mp7JrsSpaceResolutionViewType::GetBase() const
{
	return m_Base;
}

Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration Mp7JrsSpaceResolutionViewType::Getorder() const
{
	if (this->Existorder()) {
		return m_order;
	} else {
		return m_order_Default;
	}
}

bool Mp7JrsSpaceResolutionViewType::Existorder() const
{
	return m_order_Exist;
}
void Mp7JrsSpaceResolutionViewType::Setorder(Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::Setorder().");
	}
	m_order = item;
	m_order_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::Invalidateorder()
{
	m_order_Exist = false;
}
Mp7JrsPartitionPtr Mp7JrsSpaceResolutionViewType::GetSpacePartition() const
{
		return m_SpacePartition;
}

Mp7JrsRegionLocatorPtr Mp7JrsSpaceResolutionViewType::GetSpaceRegion() const
{
		return m_SpaceRegion;
}

Mp7JrsPartitionPtr Mp7JrsSpaceResolutionViewType::GetFrequencyPartition() const
{
		return m_FrequencyPartition;
}

// Element is optional
bool Mp7JrsSpaceResolutionViewType::IsValidFrequencyPartition() const
{
	return m_FrequencyPartition_Exist;
}

Mp7JrsFilteringPtr Mp7JrsSpaceResolutionViewType::GetFiltering() const
{
		return m_Filtering;
}

// Element is optional
bool Mp7JrsSpaceResolutionViewType::IsValidFiltering() const
{
	return m_Filtering_Exist;
}

Mp7JrsSignalPlaneSamplePtr Mp7JrsSpaceResolutionViewType::GetDownSamplingFactor() const
{
		return m_DownSamplingFactor;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSpaceResolutionViewType::SetSpacePartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetSpacePartition().");
	}
	if (m_SpacePartition != item)
	{
		// Dc1Factory::DeleteObject(m_SpacePartition);
		m_SpacePartition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpacePartition) m_SpacePartition->SetParent(m_myself.getPointer());
		if (m_SpacePartition && m_SpacePartition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpacePartition->UseTypeAttribute = true;
		}
		if(m_SpacePartition != Mp7JrsPartitionPtr())
		{
			m_SpacePartition->SetContentName(XMLString::transcode("SpacePartition"));
		}
	// Dc1Factory::DeleteObject(m_SpaceRegion);
	m_SpaceRegion = Mp7JrsRegionLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpaceResolutionViewType::SetSpaceRegion(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetSpaceRegion().");
	}
	if (m_SpaceRegion != item)
	{
		// Dc1Factory::DeleteObject(m_SpaceRegion);
		m_SpaceRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpaceRegion) m_SpaceRegion->SetParent(m_myself.getPointer());
		if (m_SpaceRegion && m_SpaceRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpaceRegion->UseTypeAttribute = true;
		}
		if(m_SpaceRegion != Mp7JrsRegionLocatorPtr())
		{
			m_SpaceRegion->SetContentName(XMLString::transcode("SpaceRegion"));
		}
	// Dc1Factory::DeleteObject(m_SpacePartition);
	m_SpacePartition = Mp7JrsPartitionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::SetFrequencyPartition(const Mp7JrsPartitionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetFrequencyPartition().");
	}
	if (m_FrequencyPartition != item || m_FrequencyPartition_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FrequencyPartition);
		m_FrequencyPartition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrequencyPartition) m_FrequencyPartition->SetParent(m_myself.getPointer());
		if (m_FrequencyPartition && m_FrequencyPartition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrequencyPartition->UseTypeAttribute = true;
		}
		m_FrequencyPartition_Exist = true;
		if(m_FrequencyPartition != Mp7JrsPartitionPtr())
		{
			m_FrequencyPartition->SetContentName(XMLString::transcode("FrequencyPartition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidateFrequencyPartition()
{
	m_FrequencyPartition_Exist = false;
}
void Mp7JrsSpaceResolutionViewType::SetFiltering(const Mp7JrsFilteringPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetFiltering().");
	}
	if (m_Filtering != item || m_Filtering_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Filtering);
		m_Filtering = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Filtering) m_Filtering->SetParent(m_myself.getPointer());
		if (m_Filtering && m_Filtering->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Filtering->UseTypeAttribute = true;
		}
		m_Filtering_Exist = true;
		if(m_Filtering != Mp7JrsFilteringPtr())
		{
			m_Filtering->SetContentName(XMLString::transcode("Filtering"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidateFiltering()
{
	m_Filtering_Exist = false;
}
void Mp7JrsSpaceResolutionViewType::SetDownSamplingFactor(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetDownSamplingFactor().");
	}
	if (m_DownSamplingFactor != item)
	{
		// Dc1Factory::DeleteObject(m_DownSamplingFactor);
		m_DownSamplingFactor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DownSamplingFactor) m_DownSamplingFactor->SetParent(m_myself.getPointer());
		if (m_DownSamplingFactor && m_DownSamplingFactor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DownSamplingFactor->UseTypeAttribute = true;
		}
		if(m_DownSamplingFactor != Mp7JrsSignalPlaneSamplePtr())
		{
			m_DownSamplingFactor->SetContentName(XMLString::transcode("DownSamplingFactor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSignalPtr Mp7JrsSpaceResolutionViewType::GetTarget() const
{
	return GetBase()->GetTarget();
}

Mp7JrsSignalPtr Mp7JrsSpaceResolutionViewType::GetSource() const
{
	return GetBase()->GetSource();
}

// Element is optional
bool Mp7JrsSpaceResolutionViewType::IsValidSource() const
{
	return GetBase()->IsValidSource();
}

void Mp7JrsSpaceResolutionViewType::SetTarget(const Mp7JrsSignalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetTarget().");
	}
	GetBase()->SetTarget(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetSource().");
	}
	GetBase()->SetSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidateSource()
{
	GetBase()->InvalidateSource();
}
XMLCh * Mp7JrsSpaceResolutionViewType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSpaceResolutionViewType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSpaceResolutionViewType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSpaceResolutionViewType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSpaceResolutionViewType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSpaceResolutionViewType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSpaceResolutionViewType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSpaceResolutionViewType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSpaceResolutionViewType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSpaceResolutionViewType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSpaceResolutionViewType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSpaceResolutionViewType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSpaceResolutionViewType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSpaceResolutionViewType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSpaceResolutionViewType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceResolutionViewType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSpaceResolutionViewType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSpaceResolutionViewType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceResolutionViewType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpaceResolutionViewType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("order")) == 0)
	{
		// order is simple attribute SpaceResolutionViewType_order_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSpaceResolutionViewType_order_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpacePartition")) == 0)
	{
		// SpacePartition is simple element PartitionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpacePartition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPartitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpacePartition(p, client);
					if((p = GetSpacePartition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpaceRegion")) == 0)
	{
		// SpaceRegion is simple element RegionLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpaceRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpaceRegion(p, client);
					if((p = GetSpaceRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrequencyPartition")) == 0)
	{
		// FrequencyPartition is simple element PartitionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFrequencyPartition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PartitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPartitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFrequencyPartition(p, client);
					if((p = GetFrequencyPartition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Filtering")) == 0)
	{
		// Filtering is simple element FilteringType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFiltering()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFilteringPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFiltering(p, client);
					if((p = GetFiltering()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DownSamplingFactor")) == 0)
	{
		// DownSamplingFactor is simple element SignalPlaneSampleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDownSamplingFactor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSignalPlaneSamplePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDownSamplingFactor(p, client);
					if((p = GetDownSamplingFactor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpaceResolutionViewType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpaceResolutionViewType");
	}
	return result;
}

XMLCh * Mp7JrsSpaceResolutionViewType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpaceResolutionViewType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpaceResolutionViewType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpaceResolutionViewType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_order_Exist)
	{
	// Enumeration
	if(m_order != Mp7JrsSpaceResolutionViewType_order_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSpaceResolutionViewType_order_LocalType::ToText(m_order);
		element->setAttributeNS(X(""), X("order"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_SpacePartition != Mp7JrsPartitionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpacePartition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpacePartition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpacePartition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpaceRegion != Mp7JrsRegionLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpaceRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpaceRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpaceRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FrequencyPartition != Mp7JrsPartitionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrequencyPartition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrequencyPartition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrequencyPartition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Filtering != Mp7JrsFilteringPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Filtering->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Filtering"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Filtering->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DownSamplingFactor != Mp7JrsSignalPlaneSamplePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DownSamplingFactor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DownSamplingFactor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DownSamplingFactor->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpaceResolutionViewType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("order")))
	{
		this->Setorder(Mp7JrsSpaceResolutionViewType_order_LocalType::Parse(parent->getAttribute(X("order"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsViewType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpacePartition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpacePartition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePartitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpacePartition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpaceRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpaceRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpaceRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrequencyPartition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrequencyPartition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePartitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrequencyPartition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Filtering"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Filtering")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFilteringType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFiltering(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DownSamplingFactor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DownSamplingFactor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSignalPlaneSampleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDownSamplingFactor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpaceResolutionViewType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("SpacePartition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePartitionType; // FTT, check this
	}
	this->SetSpacePartition(child);
  }
  if (XMLString::compareString(elementname, X("SpaceRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionLocatorType; // FTT, check this
	}
	this->SetSpaceRegion(child);
  }
  if (XMLString::compareString(elementname, X("FrequencyPartition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePartitionType; // FTT, check this
	}
	this->SetFrequencyPartition(child);
  }
  if (XMLString::compareString(elementname, X("Filtering")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFilteringType; // FTT, check this
	}
	this->SetFiltering(child);
  }
  if (XMLString::compareString(elementname, X("DownSamplingFactor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSignalPlaneSampleType; // FTT, check this
	}
	this->SetDownSamplingFactor(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpaceResolutionViewType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFrequencyPartition() != Dc1NodePtr())
		result.Insert(GetFrequencyPartition());
	if (GetFiltering() != Dc1NodePtr())
		result.Insert(GetFiltering());
	if (GetDownSamplingFactor() != Dc1NodePtr())
		result.Insert(GetDownSamplingFactor());
	if (GetTarget() != Dc1NodePtr())
		result.Insert(GetTarget());
	if (GetSource() != Dc1NodePtr())
		result.Insert(GetSource());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetSpacePartition() != Dc1NodePtr())
		result.Insert(GetSpacePartition());
	if (GetSpaceRegion() != Dc1NodePtr())
		result.Insert(GetSpaceRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpaceResolutionViewType_ExtMethodImpl.h


