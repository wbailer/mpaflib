
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsCompositionShotType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsCompositionTransitionType.h"
#include "Mp7JrsShotEditingTemporalDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsShotEditingTemporalDecompositionType_LocalType::IMp7JrsShotEditingTemporalDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsShotEditingTemporalDecompositionType_LocalType::~IMp7JrsShotEditingTemporalDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsShotEditingTemporalDecompositionType_LocalType::Mp7JrsShotEditingTemporalDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsShotEditingTemporalDecompositionType_LocalType::~Mp7JrsShotEditingTemporalDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsShotEditingTemporalDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_CompositionShot = Mp7JrsCompositionShotPtr(); // Class
	m_CompositionShotRef = Mp7JrsReferencePtr(); // Class
	m_CompositionTransition = Mp7JrsCompositionTransitionPtr(); // Class
	m_CompositionTransitionRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsShotEditingTemporalDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_CompositionShot);
	// Dc1Factory::DeleteObject(m_CompositionShotRef);
	// Dc1Factory::DeleteObject(m_CompositionTransition);
	// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
}

void Mp7JrsShotEditingTemporalDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ShotEditingTemporalDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IShotEditingTemporalDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsShotEditingTemporalDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_CompositionShot);
		this->SetCompositionShot(Dc1Factory::CloneObject(tmp->GetCompositionShot()));
		// Dc1Factory::DeleteObject(m_CompositionShotRef);
		this->SetCompositionShotRef(Dc1Factory::CloneObject(tmp->GetCompositionShotRef()));
		// Dc1Factory::DeleteObject(m_CompositionTransition);
		this->SetCompositionTransition(Dc1Factory::CloneObject(tmp->GetCompositionTransition()));
		// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
		this->SetCompositionTransitionRef(Dc1Factory::CloneObject(tmp->GetCompositionTransitionRef()));
}

Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsCompositionShotPtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetCompositionShot() const
{
		return m_CompositionShot;
}

Mp7JrsReferencePtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetCompositionShotRef() const
{
		return m_CompositionShotRef;
}

Mp7JrsCompositionTransitionPtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetCompositionTransition() const
{
		return m_CompositionTransition;
}

Mp7JrsReferencePtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetCompositionTransitionRef() const
{
		return m_CompositionTransitionRef;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionShot(const Mp7JrsCompositionShotPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionShot().");
	}
	if (m_CompositionShot != item)
	{
		// Dc1Factory::DeleteObject(m_CompositionShot);
		m_CompositionShot = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositionShot) m_CompositionShot->SetParent(m_myself.getPointer());
		if (m_CompositionShot && m_CompositionShot->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionShotType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompositionShot->UseTypeAttribute = true;
		}
		if(m_CompositionShot != Mp7JrsCompositionShotPtr())
		{
			m_CompositionShot->SetContentName(XMLString::transcode("CompositionShot"));
		}
	// Dc1Factory::DeleteObject(m_CompositionShotRef);
	m_CompositionShotRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_CompositionTransition);
	m_CompositionTransition = Mp7JrsCompositionTransitionPtr();
	// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
	m_CompositionTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionShotRef().");
	}
	if (m_CompositionShotRef != item)
	{
		// Dc1Factory::DeleteObject(m_CompositionShotRef);
		m_CompositionShotRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositionShotRef) m_CompositionShotRef->SetParent(m_myself.getPointer());
		if (m_CompositionShotRef && m_CompositionShotRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompositionShotRef->UseTypeAttribute = true;
		}
		if(m_CompositionShotRef != Mp7JrsReferencePtr())
		{
			m_CompositionShotRef->SetContentName(XMLString::transcode("CompositionShotRef"));
		}
	// Dc1Factory::DeleteObject(m_CompositionShot);
	m_CompositionShot = Mp7JrsCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_CompositionTransition);
	m_CompositionTransition = Mp7JrsCompositionTransitionPtr();
	// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
	m_CompositionTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionTransition(const Mp7JrsCompositionTransitionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionTransition().");
	}
	if (m_CompositionTransition != item)
	{
		// Dc1Factory::DeleteObject(m_CompositionTransition);
		m_CompositionTransition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositionTransition) m_CompositionTransition->SetParent(m_myself.getPointer());
		if (m_CompositionTransition && m_CompositionTransition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CompositionTransitionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompositionTransition->UseTypeAttribute = true;
		}
		if(m_CompositionTransition != Mp7JrsCompositionTransitionPtr())
		{
			m_CompositionTransition->SetContentName(XMLString::transcode("CompositionTransition"));
		}
	// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
	m_CompositionTransitionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_CompositionShot);
	m_CompositionShot = Mp7JrsCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_CompositionShotRef);
	m_CompositionShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsShotEditingTemporalDecompositionType_LocalType::SetCompositionTransitionRef().");
	}
	if (m_CompositionTransitionRef != item)
	{
		// Dc1Factory::DeleteObject(m_CompositionTransitionRef);
		m_CompositionTransitionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositionTransitionRef) m_CompositionTransitionRef->SetParent(m_myself.getPointer());
		if (m_CompositionTransitionRef && m_CompositionTransitionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompositionTransitionRef->UseTypeAttribute = true;
		}
		if(m_CompositionTransitionRef != Mp7JrsReferencePtr())
		{
			m_CompositionTransitionRef->SetContentName(XMLString::transcode("CompositionTransitionRef"));
		}
	// Dc1Factory::DeleteObject(m_CompositionTransition);
	m_CompositionTransition = Mp7JrsCompositionTransitionPtr();
	// Dc1Factory::DeleteObject(m_CompositionShot);
	m_CompositionShot = Mp7JrsCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_CompositionShotRef);
	m_CompositionShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: ShotEditingTemporalDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsShotEditingTemporalDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsShotEditingTemporalDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsShotEditingTemporalDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ShotEditingTemporalDecompositionType_LocalType"));
	// Element serialization:
	// Class
	
	if (m_CompositionShot != Mp7JrsCompositionShotPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompositionShot->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompositionShot"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompositionShot->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CompositionShotRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompositionShotRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompositionShotRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompositionShotRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CompositionTransition != Mp7JrsCompositionTransitionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompositionTransition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompositionTransition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompositionTransition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CompositionTransitionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompositionTransitionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompositionTransitionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompositionTransitionRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsShotEditingTemporalDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompositionShot"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompositionShot")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCompositionShotType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompositionShot(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompositionShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompositionShotRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompositionShotRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompositionTransition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompositionTransition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCompositionTransitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompositionTransition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompositionTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompositionTransitionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompositionTransitionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	} while(false);
// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsShotEditingTemporalDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("CompositionShot")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCompositionShotType; // FTT, check this
	}
	this->SetCompositionShot(child);
  }
  if (XMLString::compareString(elementname, X("CompositionShotRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetCompositionShotRef(child);
  }
  if (XMLString::compareString(elementname, X("CompositionTransition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCompositionTransitionType; // FTT, check this
	}
	this->SetCompositionTransition(child);
  }
  if (XMLString::compareString(elementname, X("CompositionTransitionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetCompositionTransitionRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsShotEditingTemporalDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCompositionShot() != Dc1NodePtr())
		result.Insert(GetCompositionShot());
	if (GetCompositionShotRef() != Dc1NodePtr())
		result.Insert(GetCompositionShotRef());
	if (GetCompositionTransition() != Dc1NodePtr())
		result.Insert(GetCompositionTransition());
	if (GetCompositionTransitionRef() != Dc1NodePtr())
		result.Insert(GetCompositionTransitionRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsShotEditingTemporalDecompositionType_LocalType_ExtMethodImpl.h


