
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsScalableColorType.h"
#include "Mp7JrsGoFGoPColorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsGoFGoPColorType::IMp7JrsGoFGoPColorType()
{

// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtPropInit.h

}

IMp7JrsGoFGoPColorType::~IMp7JrsGoFGoPColorType()
{
// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtPropCleanup.h

}

Mp7JrsGoFGoPColorType::Mp7JrsGoFGoPColorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGoFGoPColorType::~Mp7JrsGoFGoPColorType()
{
	Cleanup();
}

void Mp7JrsGoFGoPColorType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_aggregation = Mp7JrsGoFGoPColorType_aggregation_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_ScalableColor = Mp7JrsScalableColorPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtMyPropInit.h

}

void Mp7JrsGoFGoPColorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ScalableColor);
}

void Mp7JrsGoFGoPColorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GoFGoPColorTypePtr, since we
	// might need GetBase(), which isn't defined in IGoFGoPColorType
	const Dc1Ptr< Mp7JrsGoFGoPColorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsGoFGoPColorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->Setaggregation(tmp->Getaggregation());
	}
		// Dc1Factory::DeleteObject(m_ScalableColor);
		this->SetScalableColor(Dc1Factory::CloneObject(tmp->GetScalableColor()));
}

Dc1NodePtr Mp7JrsGoFGoPColorType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsGoFGoPColorType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsGoFGoPColorType::GetBase() const
{
	return m_Base;
}

Mp7JrsGoFGoPColorType_aggregation_LocalType::Enumeration Mp7JrsGoFGoPColorType::Getaggregation() const
{
	return m_aggregation;
}

void Mp7JrsGoFGoPColorType::Setaggregation(Mp7JrsGoFGoPColorType_aggregation_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGoFGoPColorType::Setaggregation().");
	}
	m_aggregation = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableColorPtr Mp7JrsGoFGoPColorType::GetScalableColor() const
{
		return m_ScalableColor;
}

void Mp7JrsGoFGoPColorType::SetScalableColor(const Mp7JrsScalableColorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGoFGoPColorType::SetScalableColor().");
	}
	if (m_ScalableColor != item)
	{
		// Dc1Factory::DeleteObject(m_ScalableColor);
		m_ScalableColor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ScalableColor) m_ScalableColor->SetParent(m_myself.getPointer());
		if (m_ScalableColor && m_ScalableColor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ScalableColor->UseTypeAttribute = true;
		}
		if(m_ScalableColor != Mp7JrsScalableColorPtr())
		{
			m_ScalableColor->SetContentName(XMLString::transcode("ScalableColor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGoFGoPColorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("aggregation")) == 0)
	{
		// aggregation is simple attribute GoFGoPColorType_aggregation_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsGoFGoPColorType_aggregation_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ScalableColor")) == 0)
	{
		// ScalableColor is simple element ScalableColorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetScalableColor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsScalableColorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetScalableColor(p, client);
					if((p = GetScalableColor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GoFGoPColorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GoFGoPColorType");
	}
	return result;
}

XMLCh * Mp7JrsGoFGoPColorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsGoFGoPColorType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsGoFGoPColorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GoFGoPColorType"));
	// Attribute Serialization:
	// Enumeration
	if(m_aggregation != Mp7JrsGoFGoPColorType_aggregation_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsGoFGoPColorType_aggregation_LocalType::ToText(m_aggregation);
		element->setAttributeNS(X(""), X("aggregation"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_ScalableColor != Mp7JrsScalableColorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ScalableColor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ScalableColor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ScalableColor->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsGoFGoPColorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("aggregation")))
	{
		this->Setaggregation(Mp7JrsGoFGoPColorType_aggregation_LocalType::Parse(parent->getAttribute(X("aggregation"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ScalableColor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ScalableColor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateScalableColorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetScalableColor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGoFGoPColorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ScalableColor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateScalableColorType; // FTT, check this
	}
	this->SetScalableColor(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGoFGoPColorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetScalableColor() != Dc1NodePtr())
		result.Insert(GetScalableColor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGoFGoPColorType_ExtMethodImpl.h


