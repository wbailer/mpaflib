
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtImplInclude.h


#include "Mp7JrscurrencyCode.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsFinancialType_AccountItem_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFinancialType_AccountItem_LocalType::IMp7JrsFinancialType_AccountItem_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtPropInit.h

}

IMp7JrsFinancialType_AccountItem_LocalType::~IMp7JrsFinancialType_AccountItem_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtPropCleanup.h

}

Mp7JrsFinancialType_AccountItem_LocalType::Mp7JrsFinancialType_AccountItem_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFinancialType_AccountItem_LocalType::~Mp7JrsFinancialType_AccountItem_LocalType()
{
	Cleanup();
}

void Mp7JrsFinancialType_AccountItem_LocalType::Init()
{

	// Init attributes
	m_currency = Mp7JrscurrencyCodePtr(); // Pattern
	m_value = 0.0; // Value

	// Init elements (element, union sequence choice all any)
	
	m_EffectiveDate = Mp7JrstimePointPtr(); // Pattern
	m_EffectiveDate_Exist = false;
	m_CostType = Mp7JrsTermUsePtr(); // Class
	m_IncomeType = Mp7JrsTermUsePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtMyPropInit.h

}

void Mp7JrsFinancialType_AccountItem_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_currency); // Pattern
	// Dc1Factory::DeleteObject(m_EffectiveDate);
	// Dc1Factory::DeleteObject(m_CostType);
	// Dc1Factory::DeleteObject(m_IncomeType);
}

void Mp7JrsFinancialType_AccountItem_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FinancialType_AccountItem_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IFinancialType_AccountItem_LocalType
	const Dc1Ptr< Mp7JrsFinancialType_AccountItem_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_currency); // Pattern
		this->Setcurrency(Dc1Factory::CloneObject(tmp->Getcurrency()));
	}
	{
		this->Setvalue(tmp->Getvalue());
	}
	if (tmp->IsValidEffectiveDate())
	{
		// Dc1Factory::DeleteObject(m_EffectiveDate);
		this->SetEffectiveDate(Dc1Factory::CloneObject(tmp->GetEffectiveDate()));
	}
	else
	{
		InvalidateEffectiveDate();
	}
		// Dc1Factory::DeleteObject(m_CostType);
		this->SetCostType(Dc1Factory::CloneObject(tmp->GetCostType()));
		// Dc1Factory::DeleteObject(m_IncomeType);
		this->SetIncomeType(Dc1Factory::CloneObject(tmp->GetIncomeType()));
}

Dc1NodePtr Mp7JrsFinancialType_AccountItem_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFinancialType_AccountItem_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrscurrencyCodePtr Mp7JrsFinancialType_AccountItem_LocalType::Getcurrency() const
{
	return m_currency;
}

void Mp7JrsFinancialType_AccountItem_LocalType::Setcurrency(const Mp7JrscurrencyCodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType_AccountItem_LocalType::Setcurrency().");
	}
	m_currency = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_currency) m_currency->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

double Mp7JrsFinancialType_AccountItem_LocalType::Getvalue() const
{
	return m_value;
}

void Mp7JrsFinancialType_AccountItem_LocalType::Setvalue(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType_AccountItem_LocalType::Setvalue().");
	}
	m_value = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrstimePointPtr Mp7JrsFinancialType_AccountItem_LocalType::GetEffectiveDate() const
{
		return m_EffectiveDate;
}

// Element is optional
bool Mp7JrsFinancialType_AccountItem_LocalType::IsValidEffectiveDate() const
{
	return m_EffectiveDate_Exist;
}

Mp7JrsTermUsePtr Mp7JrsFinancialType_AccountItem_LocalType::GetCostType() const
{
		return m_CostType;
}

Mp7JrsTermUsePtr Mp7JrsFinancialType_AccountItem_LocalType::GetIncomeType() const
{
		return m_IncomeType;
}

void Mp7JrsFinancialType_AccountItem_LocalType::SetEffectiveDate(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType_AccountItem_LocalType::SetEffectiveDate().");
	}
	if (m_EffectiveDate != item || m_EffectiveDate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_EffectiveDate);
		m_EffectiveDate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EffectiveDate) m_EffectiveDate->SetParent(m_myself.getPointer());
		if (m_EffectiveDate && m_EffectiveDate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_EffectiveDate->UseTypeAttribute = true;
		}
		m_EffectiveDate_Exist = true;
		if(m_EffectiveDate != Mp7JrstimePointPtr())
		{
			m_EffectiveDate->SetContentName(XMLString::transcode("EffectiveDate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFinancialType_AccountItem_LocalType::InvalidateEffectiveDate()
{
	m_EffectiveDate_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsFinancialType_AccountItem_LocalType::SetCostType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType_AccountItem_LocalType::SetCostType().");
	}
	if (m_CostType != item)
	{
		// Dc1Factory::DeleteObject(m_CostType);
		m_CostType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CostType) m_CostType->SetParent(m_myself.getPointer());
		if (m_CostType && m_CostType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CostType->UseTypeAttribute = true;
		}
		if(m_CostType != Mp7JrsTermUsePtr())
		{
			m_CostType->SetContentName(XMLString::transcode("CostType"));
		}
	// Dc1Factory::DeleteObject(m_IncomeType);
	m_IncomeType = Mp7JrsTermUsePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsFinancialType_AccountItem_LocalType::SetIncomeType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType_AccountItem_LocalType::SetIncomeType().");
	}
	if (m_IncomeType != item)
	{
		// Dc1Factory::DeleteObject(m_IncomeType);
		m_IncomeType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IncomeType) m_IncomeType->SetParent(m_myself.getPointer());
		if (m_IncomeType && m_IncomeType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IncomeType->UseTypeAttribute = true;
		}
		if(m_IncomeType != Mp7JrsTermUsePtr())
		{
			m_IncomeType->SetContentName(XMLString::transcode("IncomeType"));
		}
	// Dc1Factory::DeleteObject(m_CostType);
	m_CostType = Mp7JrsTermUsePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFinancialType_AccountItem_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("currency")) == 0)
	{
		// currency is simple attribute currencyCode
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrscurrencyCodePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("value")) == 0)
	{
		// value is simple attribute decimal
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EffectiveDate")) == 0)
	{
		// EffectiveDate is simple element timePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEffectiveDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrstimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEffectiveDate(p, client);
					if((p = GetEffectiveDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CostType")) == 0)
	{
		// CostType is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCostType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCostType(p, client);
					if((p = GetCostType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IncomeType")) == 0)
	{
		// IncomeType is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIncomeType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIncomeType(p, client);
					if((p = GetIncomeType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FinancialType_AccountItem_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FinancialType_AccountItem_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsFinancialType_AccountItem_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFinancialType_AccountItem_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFinancialType_AccountItem_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FinancialType_AccountItem_LocalType"));
	// Attribute Serialization:
	// Pattern
	if (m_currency != Mp7JrscurrencyCodePtr())
	{
		XMLCh * tmp = m_currency->ToText();
		element->setAttributeNS(X(""), X("currency"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_value);
		element->setAttributeNS(X(""), X("value"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	if(m_EffectiveDate != Mp7JrstimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("EffectiveDate");
//		m_EffectiveDate->SetContentName(contentname);
		m_EffectiveDate->UseTypeAttribute = this->UseTypeAttribute;
		m_EffectiveDate->Serialize(doc, element);
	}
	// Class
	
	if (m_CostType != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CostType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CostType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CostType->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_IncomeType != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IncomeType->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IncomeType"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IncomeType->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFinancialType_AccountItem_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("currency")))
	{
		Mp7JrscurrencyCodePtr tmp = CreatecurrencyCode; // FTT, check this
		tmp->Parse(parent->getAttribute(X("currency")));
		this->Setcurrency(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("value")))
	{
		// deserialize value type
		this->Setvalue(Dc1Convert::TextToDouble(parent->getAttribute(X("value"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("EffectiveDate"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("EffectiveDate")) == 0))
		{
			Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEffectiveDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CostType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CostType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCostType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IncomeType"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IncomeType")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIncomeType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFinancialType_AccountItem_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("EffectiveDate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimePointType; // FTT, check this
	}
	this->SetEffectiveDate(child);
  }
  if (XMLString::compareString(elementname, X("CostType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetCostType(child);
  }
  if (XMLString::compareString(elementname, X("IncomeType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetIncomeType(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFinancialType_AccountItem_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEffectiveDate() != Dc1NodePtr())
		result.Insert(GetEffectiveDate());
	if (GetCostType() != Dc1NodePtr())
		result.Insert(GetCostType());
	if (GetIncomeType() != Dc1NodePtr())
		result.Insert(GetIncomeType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFinancialType_AccountItem_LocalType_ExtMethodImpl.h


