
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsAudioLLDVectorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfVector

#include <assert.h>
IMp7JrsAudioLLDVectorType::IMp7JrsAudioLLDVectorType()
{

// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtPropInit.h

}

IMp7JrsAudioLLDVectorType::~IMp7JrsAudioLLDVectorType()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtPropCleanup.h

}

Mp7JrsAudioLLDVectorType::Mp7JrsAudioLLDVectorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioLLDVectorType::~Mp7JrsAudioLLDVectorType()
{
	Cleanup();
}

void Mp7JrsAudioLLDVectorType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Vector = Mp7JrsfloatVectorPtr(); // Collection
	m_SeriesOfVector = Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtMyPropInit.h

}

void Mp7JrsAudioLLDVectorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Vector);
	// Dc1Factory::DeleteObject(m_SeriesOfVector);
}

void Mp7JrsAudioLLDVectorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioLLDVectorTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioLLDVectorType
	const Dc1Ptr< Mp7JrsAudioLLDVectorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioLLDVectorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Vector);
		this->SetVector(Dc1Factory::CloneObject(tmp->GetVector()));
		// Dc1Factory::DeleteObject(m_SeriesOfVector);
		this->SetSeriesOfVector(Dc1Factory::CloneObject(tmp->GetSeriesOfVector()));
}

Dc1NodePtr Mp7JrsAudioLLDVectorType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioLLDVectorType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsAudioLLDVectorType::GetBase() const
{
	return m_Base;
}

Mp7JrsfloatVectorPtr Mp7JrsAudioLLDVectorType::GetVector() const
{
		return m_Vector;
}

Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr Mp7JrsAudioLLDVectorType::GetSeriesOfVector() const
{
		return m_SeriesOfVector;
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioLLDVectorType::SetVector(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDVectorType::SetVector().");
	}
	if (m_Vector != item)
	{
		// Dc1Factory::DeleteObject(m_Vector);
		m_Vector = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Vector) m_Vector->SetParent(m_myself.getPointer());
		if(m_Vector != Mp7JrsfloatVectorPtr())
		{
			m_Vector->SetContentName(XMLString::transcode("Vector"));
		}
	// Dc1Factory::DeleteObject(m_SeriesOfVector);
	m_SeriesOfVector = Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioLLDVectorType::SetSeriesOfVector(const Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDVectorType::SetSeriesOfVector().");
	}
	if (m_SeriesOfVector != item)
	{
		// Dc1Factory::DeleteObject(m_SeriesOfVector);
		m_SeriesOfVector = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SeriesOfVector) m_SeriesOfVector->SetParent(m_myself.getPointer());
		if(m_SeriesOfVector != Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr())
		{
			m_SeriesOfVector->SetContentName(XMLString::transcode("SeriesOfVector"));
		}
	// Dc1Factory::DeleteObject(m_Vector);
	m_Vector = Mp7JrsfloatVectorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsAudioLLDVectorType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsAudioLLDVectorType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsAudioLLDVectorType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioLLDVectorType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioLLDVectorType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsAudioLLDVectorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Vector")) == 0)
	{
		// Vector is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVector()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVector(p, client);
					if((p = GetVector()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SeriesOfVector")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SeriesOfVector is item of type AudioLLDVectorType_SeriesOfVector_LocalType
		// in element collection AudioLLDVectorType_SeriesOfVector_CollectionType
		
		context->Found = true;
		Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr coll = GetSeriesOfVector();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAudioLLDVectorType_SeriesOfVector_CollectionType; // FTT, check this
				SetSeriesOfVector(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioLLDVectorType_SeriesOfVector_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioLLDVectorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioLLDVectorType");
	}
	return result;
}

XMLCh * Mp7JrsAudioLLDVectorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAudioLLDVectorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAudioLLDVectorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioLLDVectorType"));
	// Element serialization:
	if (m_Vector != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Vector->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SeriesOfVector != Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SeriesOfVector->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAudioLLDVectorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Vector")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVector(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SeriesOfVector"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SeriesOfVector")) == 0))
		{
			// Deserialize factory type
			Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr tmp = CreateAudioLLDVectorType_SeriesOfVector_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSeriesOfVector(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioLLDVectorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Vector")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetVector(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SeriesOfVector")) == 0))
  {
	Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr tmp = CreateAudioLLDVectorType_SeriesOfVector_CollectionType; // FTT, check this
	this->SetSeriesOfVector(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioLLDVectorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVector() != Dc1NodePtr())
		result.Insert(GetVector());
	if (GetSeriesOfVector() != Dc1NodePtr())
		result.Insert(GetSeriesOfVector());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioLLDVectorType_ExtMethodImpl.h


