
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtImplInclude.h


#include "Mp7JrsFragmentUpdateContextType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFragmentUpdateContextType::IMp7JrsFragmentUpdateContextType()
{

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtPropInit.h

}

IMp7JrsFragmentUpdateContextType::~IMp7JrsFragmentUpdateContextType()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtPropCleanup.h

}

Mp7JrsFragmentUpdateContextType::Mp7JrsFragmentUpdateContextType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFragmentUpdateContextType::~Mp7JrsFragmentUpdateContextType()
{
	Cleanup();
}

void Mp7JrsFragmentUpdateContextType::Init()
{
	// Init base
	m_Base = CreateFragmentUpdateContextTypeBase; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_position = Mp7JrsFragmentUpdateContextType_position_LocalType::UninitializedEnumeration;
	m_position_Default = Mp7JrsFragmentUpdateContextType_position_LocalType::lastChild; // Default enumeration
	m_position_Exist = false;



// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtMyPropInit.h

}

void Mp7JrsFragmentUpdateContextType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsFragmentUpdateContextType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FragmentUpdateContextTypePtr, since we
	// might need GetBase(), which isn't defined in IFragmentUpdateContextType
	const Dc1Ptr< Mp7JrsFragmentUpdateContextType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsFragmentUpdateContextTypeBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsFragmentUpdateContextType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existposition())
	{
		this->Setposition(tmp->Getposition());
	}
	else
	{
		Invalidateposition();
	}
	}
}

Dc1NodePtr Mp7JrsFragmentUpdateContextType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsFragmentUpdateContextType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsFragmentUpdateContextTypeBase > Mp7JrsFragmentUpdateContextType::GetBase() const
{
	return m_Base;
}

Mp7JrsFragmentUpdateContextType_position_LocalType::Enumeration Mp7JrsFragmentUpdateContextType::Getposition() const
{
	if (this->Existposition()) {
		return m_position;
	} else {
		return m_position_Default;
	}
}

bool Mp7JrsFragmentUpdateContextType::Existposition() const
{
	return m_position_Exist;
}
void Mp7JrsFragmentUpdateContextType::Setposition(Mp7JrsFragmentUpdateContextType_position_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateContextType::Setposition().");
	}
	m_position = item;
	m_position_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFragmentUpdateContextType::Invalidateposition()
{
	m_position_Exist = false;
}
XMLCh * Mp7JrsFragmentUpdateContextType::GetContent() const
{
	return GetBase()->GetContent();
}

void Mp7JrsFragmentUpdateContextType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFragmentUpdateContextType::SetContent().");
	}
	GetBase()->SetContent(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFragmentUpdateContextType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("position")) == 0)
	{
		// position is simple attribute FragmentUpdateContextType_position_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsFragmentUpdateContextType_position_LocalType::Enumeration");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FragmentUpdateContextType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FragmentUpdateContextType");
	}
	return result;
}

XMLCh * Mp7JrsFragmentUpdateContextType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFragmentUpdateContextType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsFragmentUpdateContextType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsFragmentUpdateContextType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsFragmentUpdateContextType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	m_Base->Serialize(doc, element, newElem);

	assert(element);
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FragmentUpdateContextType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_position_Exist)
	{
	// Enumeration
	if(m_position != Mp7JrsFragmentUpdateContextType_position_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsFragmentUpdateContextType_position_LocalType::ToText(m_position);
		element->setAttributeNS(X(""), X("position"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsFragmentUpdateContextType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("position")))
	{
		this->Setposition(Mp7JrsFragmentUpdateContextType_position_LocalType::Parse(parent->getAttribute(X("position"))));
		* current = parent;
	}

  // Extensionbase is Mp7JrsFragmentUpdateContextTypeBase
  // Deserialize cce factory type
  if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
  }
// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFragmentUpdateContextType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFragmentUpdateContextType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFragmentUpdateContextType_ExtMethodImpl.h


