
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtImplInclude.h


#include "Mp7JrsIntegerMatrixType.h"
#include "Mp7Jrsdim_LocalType.h"
#include "Mp7JrsBoxListType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsBoxListType::IMp7JrsBoxListType()
{

// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtPropInit.h

}

IMp7JrsBoxListType::~IMp7JrsBoxListType()
{
// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtPropCleanup.h

}

Mp7JrsBoxListType::Mp7JrsBoxListType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsBoxListType::~Mp7JrsBoxListType()
{
	Cleanup();
}

void Mp7JrsBoxListType::Init()
{
	// Init base
	m_Base = CreateIntegerMatrixType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtMyPropInit.h

}

void Mp7JrsBoxListType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsBoxListType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use BoxListTypePtr, since we
	// might need GetBase(), which isn't defined in IBoxListType
	const Dc1Ptr< Mp7JrsBoxListType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsIntegerMatrixPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsBoxListType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrsBoxListType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsBoxListType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsIntegerMatrixType > Mp7JrsBoxListType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Mp7JrsintegerVector > Mp7JrsBoxListType::GetBaseCollection() const
{
	return GetBase()->GetBase();
}

void Mp7JrsBoxListType::Initialize(unsigned int maxElems)
{
	GetBase()->GetBase()->Initialize(maxElems);
}

void Mp7JrsBoxListType::addElement(const int &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::addElement().");
	}
	GetBase()->GetBase()->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBoxListType::setElementAt(const int &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::setElementAt().");
	}
	GetBase()->GetBase()->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
	m_myself.getPointer(), DC1_NA_NODE_UPDATED,
	client, m_myself.getPointer());
}

void Mp7JrsBoxListType::insertElementAt(const int &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::insertElementAt().");
	}
	GetBase()->GetBase()->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBoxListType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::removeAllElements().");
	}
	GetBase()->GetBase()->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBoxListType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::removeElementAt().");
	}
	GetBase()->GetBase()->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsBoxListType::containsElement(const int &toCheck)
{
	return GetBase()->GetBase()->containsElement(toCheck);
}

unsigned int Mp7JrsBoxListType::curCapacity() const
{
	return GetBase()->GetBase()->curCapacity();
}

const int Mp7JrsBoxListType::elementAt(unsigned int getAt) const
{
	return GetBase()->GetBase()->elementAt(getAt);
}

int Mp7JrsBoxListType::elementAt(unsigned int getAt)
{
	return GetBase()->GetBase()->elementAt(getAt);
}


unsigned int Mp7JrsBoxListType::size() const
{
	return GetBase()->GetBase()->size();
}

void Mp7JrsBoxListType::ensureExtraCapacity(unsigned int length)
{
	GetBase()->GetBase()->ensureExtraCapacity(length);
}

int Mp7JrsBoxListType::elementIndexOf(const int &toCheck) const
{
	return GetBase()->GetBase()->elementIndexOf(toCheck);
}

Mp7Jrsdim_LocalPtr Mp7JrsBoxListType::Getdim() const
{
	return GetBase()->Getdim();
}

void Mp7JrsBoxListType::Setdim(const Mp7Jrsdim_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBoxListType::Setdim().");
	}
	GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsBoxListType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for BoxListType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "BoxListType");
	}
	return result;
}

XMLCh * Mp7JrsBoxListType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsBoxListType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}

bool Mp7JrsBoxListType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsBoxListType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsBoxListType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("BoxListType"));
	// Element serialization:

}

bool Mp7JrsBoxListType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsIntegerMatrixType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsBoxListType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsBoxListType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsBoxListType_ExtMethodImpl.h


