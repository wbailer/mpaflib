
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtImplInclude.h


#include "Mp7JrsNameComponentType.h"
#include "Mp7JrsPersonNameType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsPersonNameType_LocalType::IMp7JrsPersonNameType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtPropInit.h

}

IMp7JrsPersonNameType_LocalType::~IMp7JrsPersonNameType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtPropCleanup.h

}

Mp7JrsPersonNameType_LocalType::Mp7JrsPersonNameType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPersonNameType_LocalType::~Mp7JrsPersonNameType_LocalType()
{
	Cleanup();
}

void Mp7JrsPersonNameType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_GivenName = Mp7JrsNameComponentPtr(); // Class
	m_LinkingName = Mp7JrsNameComponentPtr(); // Class
	m_LinkingName_Exist = false;
	m_FamilyName = Mp7JrsNameComponentPtr(); // Class
	m_FamilyName_Exist = false;
	m_Title = Mp7JrsNameComponentPtr(); // Class
	m_Title_Exist = false;
	m_Salutation = Mp7JrsNameComponentPtr(); // Class
	m_Salutation_Exist = false;
	m_Numeration = NULL; // Optional String
	m_Numeration_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMyPropInit.h

}

void Mp7JrsPersonNameType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_GivenName);
	// Dc1Factory::DeleteObject(m_LinkingName);
	// Dc1Factory::DeleteObject(m_FamilyName);
	// Dc1Factory::DeleteObject(m_Title);
	// Dc1Factory::DeleteObject(m_Salutation);
	XMLString::release(&this->m_Numeration);
	this->m_Numeration = NULL;
}

void Mp7JrsPersonNameType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PersonNameType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPersonNameType_LocalType
	const Dc1Ptr< Mp7JrsPersonNameType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_GivenName);
		this->SetGivenName(Dc1Factory::CloneObject(tmp->GetGivenName()));
	if (tmp->IsValidLinkingName())
	{
		// Dc1Factory::DeleteObject(m_LinkingName);
		this->SetLinkingName(Dc1Factory::CloneObject(tmp->GetLinkingName()));
	}
	else
	{
		InvalidateLinkingName();
	}
	if (tmp->IsValidFamilyName())
	{
		// Dc1Factory::DeleteObject(m_FamilyName);
		this->SetFamilyName(Dc1Factory::CloneObject(tmp->GetFamilyName()));
	}
	else
	{
		InvalidateFamilyName();
	}
	if (tmp->IsValidTitle())
	{
		// Dc1Factory::DeleteObject(m_Title);
		this->SetTitle(Dc1Factory::CloneObject(tmp->GetTitle()));
	}
	else
	{
		InvalidateTitle();
	}
	if (tmp->IsValidSalutation())
	{
		// Dc1Factory::DeleteObject(m_Salutation);
		this->SetSalutation(Dc1Factory::CloneObject(tmp->GetSalutation()));
	}
	else
	{
		InvalidateSalutation();
	}
	if (tmp->IsValidNumeration())
	{
		this->SetNumeration(XMLString::replicate(tmp->GetNumeration()));
	}
	else
	{
		InvalidateNumeration();
	}
}

Dc1NodePtr Mp7JrsPersonNameType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPersonNameType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsNameComponentPtr Mp7JrsPersonNameType_LocalType::GetGivenName() const
{
		return m_GivenName;
}

Mp7JrsNameComponentPtr Mp7JrsPersonNameType_LocalType::GetLinkingName() const
{
		return m_LinkingName;
}

// Element is optional
bool Mp7JrsPersonNameType_LocalType::IsValidLinkingName() const
{
	return m_LinkingName_Exist;
}

Mp7JrsNameComponentPtr Mp7JrsPersonNameType_LocalType::GetFamilyName() const
{
		return m_FamilyName;
}

// Element is optional
bool Mp7JrsPersonNameType_LocalType::IsValidFamilyName() const
{
	return m_FamilyName_Exist;
}

Mp7JrsNameComponentPtr Mp7JrsPersonNameType_LocalType::GetTitle() const
{
		return m_Title;
}

// Element is optional
bool Mp7JrsPersonNameType_LocalType::IsValidTitle() const
{
	return m_Title_Exist;
}

Mp7JrsNameComponentPtr Mp7JrsPersonNameType_LocalType::GetSalutation() const
{
		return m_Salutation;
}

// Element is optional
bool Mp7JrsPersonNameType_LocalType::IsValidSalutation() const
{
	return m_Salutation_Exist;
}

XMLCh * Mp7JrsPersonNameType_LocalType::GetNumeration() const
{
		return m_Numeration;
}

// Element is optional
bool Mp7JrsPersonNameType_LocalType::IsValidNumeration() const
{
	return m_Numeration_Exist;
}

// implementing setter for choice 
/* element */
void Mp7JrsPersonNameType_LocalType::SetGivenName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetGivenName().");
	}
	if (m_GivenName != item)
	{
		// Dc1Factory::DeleteObject(m_GivenName);
		m_GivenName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GivenName) m_GivenName->SetParent(m_myself.getPointer());
		if (m_GivenName && m_GivenName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GivenName->UseTypeAttribute = true;
		}
		if(m_GivenName != Mp7JrsNameComponentPtr())
		{
			m_GivenName->SetContentName(XMLString::transcode("GivenName"));
		}
	// Dc1Factory::DeleteObject(m_LinkingName);
	m_LinkingName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_FamilyName);
	m_FamilyName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Title);
	m_Title = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Salutation);
	m_Salutation = Mp7JrsNameComponentPtr();
	XMLString::release(&this->m_Numeration);
	m_Numeration = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPersonNameType_LocalType::SetLinkingName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetLinkingName().");
	}
	if (m_LinkingName != item || m_LinkingName_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_LinkingName);
		m_LinkingName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LinkingName) m_LinkingName->SetParent(m_myself.getPointer());
		if (m_LinkingName && m_LinkingName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LinkingName->UseTypeAttribute = true;
		}
		m_LinkingName_Exist = true;
		if(m_LinkingName != Mp7JrsNameComponentPtr())
		{
			m_LinkingName->SetContentName(XMLString::transcode("LinkingName"));
		}
	// Dc1Factory::DeleteObject(m_GivenName);
	m_GivenName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_FamilyName);
	m_FamilyName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Title);
	m_Title = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Salutation);
	m_Salutation = Mp7JrsNameComponentPtr();
	XMLString::release(&this->m_Numeration);
	m_Numeration = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType_LocalType::InvalidateLinkingName()
{
	m_LinkingName_Exist = false;
}
/* element */
void Mp7JrsPersonNameType_LocalType::SetFamilyName(const Mp7JrsNameComponentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetFamilyName().");
	}
	if (m_FamilyName != item || m_FamilyName_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FamilyName);
		m_FamilyName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FamilyName) m_FamilyName->SetParent(m_myself.getPointer());
		if (m_FamilyName && m_FamilyName->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FamilyName->UseTypeAttribute = true;
		}
		m_FamilyName_Exist = true;
		if(m_FamilyName != Mp7JrsNameComponentPtr())
		{
			m_FamilyName->SetContentName(XMLString::transcode("FamilyName"));
		}
	// Dc1Factory::DeleteObject(m_GivenName);
	m_GivenName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_LinkingName);
	m_LinkingName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Title);
	m_Title = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Salutation);
	m_Salutation = Mp7JrsNameComponentPtr();
	XMLString::release(&this->m_Numeration);
	m_Numeration = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType_LocalType::InvalidateFamilyName()
{
	m_FamilyName_Exist = false;
}
/* element */
void Mp7JrsPersonNameType_LocalType::SetTitle(const Mp7JrsNameComponentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetTitle().");
	}
	if (m_Title != item || m_Title_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Title);
		m_Title = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Title) m_Title->SetParent(m_myself.getPointer());
		if (m_Title && m_Title->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Title->UseTypeAttribute = true;
		}
		m_Title_Exist = true;
		if(m_Title != Mp7JrsNameComponentPtr())
		{
			m_Title->SetContentName(XMLString::transcode("Title"));
		}
	// Dc1Factory::DeleteObject(m_GivenName);
	m_GivenName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_LinkingName);
	m_LinkingName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_FamilyName);
	m_FamilyName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Salutation);
	m_Salutation = Mp7JrsNameComponentPtr();
	XMLString::release(&this->m_Numeration);
	m_Numeration = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType_LocalType::InvalidateTitle()
{
	m_Title_Exist = false;
}
/* element */
void Mp7JrsPersonNameType_LocalType::SetSalutation(const Mp7JrsNameComponentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetSalutation().");
	}
	if (m_Salutation != item || m_Salutation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Salutation);
		m_Salutation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Salutation) m_Salutation->SetParent(m_myself.getPointer());
		if (m_Salutation && m_Salutation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Salutation->UseTypeAttribute = true;
		}
		m_Salutation_Exist = true;
		if(m_Salutation != Mp7JrsNameComponentPtr())
		{
			m_Salutation->SetContentName(XMLString::transcode("Salutation"));
		}
	// Dc1Factory::DeleteObject(m_GivenName);
	m_GivenName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_LinkingName);
	m_LinkingName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_FamilyName);
	m_FamilyName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Title);
	m_Title = Mp7JrsNameComponentPtr();
	XMLString::release(&this->m_Numeration);
	m_Numeration = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType_LocalType::InvalidateSalutation()
{
	m_Salutation_Exist = false;
}
/* element */
void Mp7JrsPersonNameType_LocalType::SetNumeration(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType_LocalType::SetNumeration().");
	}
	if (m_Numeration != item || m_Numeration_Exist == false)
	{
		XMLString::release(&m_Numeration);
		m_Numeration = item;
		m_Numeration_Exist = true;
	// Dc1Factory::DeleteObject(m_GivenName);
	m_GivenName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_LinkingName);
	m_LinkingName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_FamilyName);
	m_FamilyName = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Title);
	m_Title = Mp7JrsNameComponentPtr();
	// Dc1Factory::DeleteObject(m_Salutation);
	m_Salutation = Mp7JrsNameComponentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType_LocalType::InvalidateNumeration()
{
	m_Numeration_Exist = false;
}
// Type: PersonNameType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsPersonNameType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsPersonNameType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPersonNameType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPersonNameType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_GivenName != Mp7JrsNameComponentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GivenName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GivenName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GivenName->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_LinkingName != Mp7JrsNameComponentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LinkingName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LinkingName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LinkingName->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FamilyName != Mp7JrsNameComponentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FamilyName->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FamilyName"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FamilyName->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Title != Mp7JrsNameComponentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Title->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Title"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Title->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Salutation != Mp7JrsNameComponentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Salutation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Salutation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Salutation->Serialize(doc, element, &elem);
	}
	 // String
	if(m_Numeration != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Numeration, X("urn:mpeg:mpeg7:schema:2004"), X("Numeration"), true);

}

bool Mp7JrsPersonNameType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GivenName"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GivenName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNameComponentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGivenName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LinkingName"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LinkingName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNameComponentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLinkingName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FamilyName"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FamilyName")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNameComponentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFamilyName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Title")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNameComponentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTitle(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Salutation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Salutation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNameComponentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSalutation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Numeration"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetNumeration(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetNumeration(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPersonNameType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("GivenName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNameComponentType; // FTT, check this
	}
	this->SetGivenName(child);
  }
  if (XMLString::compareString(elementname, X("LinkingName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNameComponentType; // FTT, check this
	}
	this->SetLinkingName(child);
  }
  if (XMLString::compareString(elementname, X("FamilyName")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNameComponentType; // FTT, check this
	}
	this->SetFamilyName(child);
  }
  if (XMLString::compareString(elementname, X("Title")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNameComponentType; // FTT, check this
	}
	this->SetTitle(child);
  }
  if (XMLString::compareString(elementname, X("Salutation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNameComponentType; // FTT, check this
	}
	this->SetSalutation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPersonNameType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetGivenName() != Dc1NodePtr())
		result.Insert(GetGivenName());
	if (GetLinkingName() != Dc1NodePtr())
		result.Insert(GetLinkingName());
	if (GetFamilyName() != Dc1NodePtr())
		result.Insert(GetFamilyName());
	if (GetTitle() != Dc1NodePtr())
		result.Insert(GetTitle());
	if (GetSalutation() != Dc1NodePtr())
		result.Insert(GetSalutation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPersonNameType_LocalType_ExtMethodImpl.h


