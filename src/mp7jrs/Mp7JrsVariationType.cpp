
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVariationType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsMultimediaContentType.h"
#include "Mp7JrsVariationType_VariationRelationship_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsVariationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsVariationType_VariationRelationship_LocalType2.h" // Element collection urn:mpeg:mpeg7:schema:2004:VariationRelationship

#include <assert.h>
IMp7JrsVariationType::IMp7JrsVariationType()
{

// no includefile for extension defined 
// file Mp7JrsVariationType_ExtPropInit.h

}

IMp7JrsVariationType::~IMp7JrsVariationType()
{
// no includefile for extension defined 
// file Mp7JrsVariationType_ExtPropCleanup.h

}

Mp7JrsVariationType::Mp7JrsVariationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVariationType::~Mp7JrsVariationType()
{
	Cleanup();
}

void Mp7JrsVariationType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_fidelity = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_fidelity->SetContent(0.0); // Use Value initialiser (xxx2)
	m_fidelity_Exist = false;
	m_priority = 0; // Value
	m_priority_Exist = false;
	m_timeOffset = Mp7JrsmediaDurationPtr(); // Pattern
	m_timeOffset_Exist = false;
	m_timeScale = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_timeScale->SetContent(0.0); // Use Value initialiser (xxx2)
	m_timeScale_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Source = Dc1Ptr< Dc1Node >(); // Class
	m_Source_Exist = false;
	m_Content = Dc1Ptr< Dc1Node >(); // Class
	m_VariationRelationship = Mp7JrsVariationType_VariationRelationship_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsVariationType_ExtMyPropInit.h

}

void Mp7JrsVariationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVariationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_fidelity);
	// Dc1Factory::DeleteObject(m_timeOffset); // Pattern
	// Dc1Factory::DeleteObject(m_timeScale);
	// Dc1Factory::DeleteObject(m_Source);
	// Dc1Factory::DeleteObject(m_Content);
	// Dc1Factory::DeleteObject(m_VariationRelationship);
}

void Mp7JrsVariationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VariationTypePtr, since we
	// might need GetBase(), which isn't defined in IVariationType
	const Dc1Ptr< Mp7JrsVariationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsVariationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_fidelity);
	if (tmp->Existfidelity())
	{
		this->Setfidelity(Dc1Factory::CloneObject(tmp->Getfidelity()));
	}
	else
	{
		Invalidatefidelity();
	}
	}
	{
	if (tmp->Existpriority())
	{
		this->Setpriority(tmp->Getpriority());
	}
	else
	{
		Invalidatepriority();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeOffset); // Pattern
	if (tmp->ExisttimeOffset())
	{
		this->SettimeOffset(Dc1Factory::CloneObject(tmp->GettimeOffset()));
	}
	else
	{
		InvalidatetimeOffset();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_timeScale);
	if (tmp->ExisttimeScale())
	{
		this->SettimeScale(Dc1Factory::CloneObject(tmp->GettimeScale()));
	}
	else
	{
		InvalidatetimeScale();
	}
	}
	if (tmp->IsValidSource())
	{
		// Dc1Factory::DeleteObject(m_Source);
		this->SetSource(Dc1Factory::CloneObject(tmp->GetSource()));
	}
	else
	{
		InvalidateSource();
	}
		// Dc1Factory::DeleteObject(m_Content);
		this->SetContent(Dc1Factory::CloneObject(tmp->GetContent()));
		// Dc1Factory::DeleteObject(m_VariationRelationship);
		this->SetVariationRelationship(Dc1Factory::CloneObject(tmp->GetVariationRelationship()));
}

Dc1NodePtr Mp7JrsVariationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsVariationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsVariationType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsVariationType::Getfidelity() const
{
	return m_fidelity;
}

bool Mp7JrsVariationType::Existfidelity() const
{
	return m_fidelity_Exist;
}
void Mp7JrsVariationType::Setfidelity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::Setfidelity().");
	}
	m_fidelity = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_fidelity) m_fidelity->SetParent(m_myself.getPointer());
	m_fidelity_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::Invalidatefidelity()
{
	m_fidelity_Exist = false;
}
unsigned Mp7JrsVariationType::Getpriority() const
{
	return m_priority;
}

bool Mp7JrsVariationType::Existpriority() const
{
	return m_priority_Exist;
}
void Mp7JrsVariationType::Setpriority(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::Setpriority().");
	}
	m_priority = item;
	m_priority_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::Invalidatepriority()
{
	m_priority_Exist = false;
}
Mp7JrsmediaDurationPtr Mp7JrsVariationType::GettimeOffset() const
{
	return m_timeOffset;
}

bool Mp7JrsVariationType::ExisttimeOffset() const
{
	return m_timeOffset_Exist;
}
void Mp7JrsVariationType::SettimeOffset(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SettimeOffset().");
	}
	m_timeOffset = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeOffset) m_timeOffset->SetParent(m_myself.getPointer());
	m_timeOffset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatetimeOffset()
{
	m_timeOffset_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsVariationType::GettimeScale() const
{
	return m_timeScale;
}

bool Mp7JrsVariationType::ExisttimeScale() const
{
	return m_timeScale_Exist;
}
void Mp7JrsVariationType::SettimeScale(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SettimeScale().");
	}
	m_timeScale = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_timeScale) m_timeScale->SetParent(m_myself.getPointer());
	m_timeScale_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatetimeScale()
{
	m_timeScale_Exist = false;
}
Dc1Ptr< Dc1Node > Mp7JrsVariationType::GetSource() const
{
		return m_Source;
}

// Element is optional
bool Mp7JrsVariationType::IsValidSource() const
{
	return m_Source_Exist;
}

Dc1Ptr< Dc1Node > Mp7JrsVariationType::GetContent() const
{
		return m_Content;
}

Mp7JrsVariationType_VariationRelationship_CollectionPtr Mp7JrsVariationType::GetVariationRelationship() const
{
		return m_VariationRelationship;
}

void Mp7JrsVariationType::SetSource(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetSource().");
	}
	if (m_Source != item || m_Source_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Source);
		m_Source = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Source) m_Source->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Source) m_Source->UseTypeAttribute = true;
		m_Source_Exist = true;
		if(m_Source != Dc1Ptr< Dc1Node >())
		{
			m_Source->SetContentName(XMLString::transcode("Source"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidateSource()
{
	m_Source_Exist = false;
}
void Mp7JrsVariationType::SetContent(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetContent().");
	}
	if (m_Content != item)
	{
		// Dc1Factory::DeleteObject(m_Content);
		m_Content = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Content) m_Content->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Content) m_Content->UseTypeAttribute = true;
		if(m_Content != Dc1Ptr< Dc1Node >())
		{
			m_Content->SetContentName(XMLString::transcode("Content"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::SetVariationRelationship(const Mp7JrsVariationType_VariationRelationship_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetVariationRelationship().");
	}
	if (m_VariationRelationship != item)
	{
		// Dc1Factory::DeleteObject(m_VariationRelationship);
		m_VariationRelationship = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VariationRelationship) m_VariationRelationship->SetParent(m_myself.getPointer());
		if(m_VariationRelationship != Mp7JrsVariationType_VariationRelationship_CollectionPtr())
		{
			m_VariationRelationship->SetContentName(XMLString::transcode("VariationRelationship"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsVariationType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsVariationType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsVariationType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsVariationType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsVariationType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsVariationType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsVariationType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsVariationType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsVariationType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsVariationType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsVariationType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsVariationType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsVariationType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsVariationType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsVariationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVariationType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsVariationType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsVariationType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVariationType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVariationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("fidelity")) == 0)
	{
		// fidelity is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("priority")) == 0)
	{
		// priority is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeOffset")) == 0)
	{
		// timeOffset is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("timeScale")) == 0)
	{
		// timeScale is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Source")) == 0)
	{
		// Source is simple abstract element MultimediaContentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSource()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMultimediaContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSource(p, client);
					if((p = GetSource()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Content")) == 0)
	{
		// Content is simple abstract element MultimediaContentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContent()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMultimediaContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContent(p, client);
					if((p = GetContent()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VariationRelationship")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VariationRelationship is item of type VariationType_VariationRelationship_LocalType2
		// in element collection VariationType_VariationRelationship_CollectionType
		
		context->Found = true;
		Mp7JrsVariationType_VariationRelationship_CollectionPtr coll = GetVariationRelationship();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVariationType_VariationRelationship_CollectionType; // FTT, check this
				SetVariationRelationship(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VariationType_VariationRelationship_LocalType2")))) != empty)
			{
				// Is type allowed
				Mp7JrsVariationType_VariationRelationship_Local2Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VariationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VariationType");
	}
	return result;
}

XMLCh * Mp7JrsVariationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVariationType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVariationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsVariationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VariationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_fidelity_Exist)
	{
	// Class
	if (m_fidelity != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_fidelity->ToText();
		element->setAttributeNS(X(""), X("fidelity"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_priority_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_priority);
		element->setAttributeNS(X(""), X("priority"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeOffset_Exist)
	{
	// Pattern
	if (m_timeOffset != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_timeOffset->ToText();
		element->setAttributeNS(X(""), X("timeOffset"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeScale_Exist)
	{
	// Class
	if (m_timeScale != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_timeScale->ToText();
		element->setAttributeNS(X(""), X("timeScale"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Source != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Source->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Source"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Source->UseTypeAttribute = true;
		}
		m_Source->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Content != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Content->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Content"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Content->UseTypeAttribute = true;
		}
		m_Content->Serialize(doc, element, &elem);
	}
	if (m_VariationRelationship != Mp7JrsVariationType_VariationRelationship_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VariationRelationship->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsVariationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("fidelity")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("fidelity")));
		this->Setfidelity(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("priority")))
	{
		// deserialize value type
		this->Setpriority(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("priority"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeOffset")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeOffset")));
		this->SettimeOffset(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeScale")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("timeScale")));
		this->SettimeScale(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Source"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Source")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMultimediaContentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Content")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMultimediaContentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContent(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VariationRelationship"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VariationRelationship")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVariationType_VariationRelationship_CollectionPtr tmp = CreateVariationType_VariationRelationship_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVariationRelationship(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVariationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVariationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Source")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMultimediaContentType; // FTT, check this
	}
	this->SetSource(child);
  }
  if (XMLString::compareString(elementname, X("Content")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMultimediaContentType; // FTT, check this
	}
	this->SetContent(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VariationRelationship")) == 0))
  {
	Mp7JrsVariationType_VariationRelationship_CollectionPtr tmp = CreateVariationType_VariationRelationship_CollectionType; // FTT, check this
	this->SetVariationRelationship(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVariationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSource() != Dc1NodePtr())
		result.Insert(GetSource());
	if (GetContent() != Dc1NodePtr())
		result.Insert(GetContent());
	if (GetVariationRelationship() != Dc1NodePtr())
		result.Insert(GetVariationRelationship());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVariationType_ExtMethodImpl.h


