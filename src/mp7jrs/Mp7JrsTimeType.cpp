
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTimeType_ExtImplInclude.h


#include "Mp7JrstimePointType.h"
#include "Mp7JrsRelTimePointType.h"
#include "Mp7JrsRelIncrTimePointType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsIncrDurationType.h"
#include "Mp7JrsTimeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTimeType::IMp7JrsTimeType()
{

// no includefile for extension defined 
// file Mp7JrsTimeType_ExtPropInit.h

}

IMp7JrsTimeType::~IMp7JrsTimeType()
{
// no includefile for extension defined 
// file Mp7JrsTimeType_ExtPropCleanup.h

}

Mp7JrsTimeType::Mp7JrsTimeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTimeType::~Mp7JrsTimeType()
{
	Cleanup();
}

void Mp7JrsTimeType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TimePoint = Mp7JrstimePointPtr(); // Pattern
	m_RelTimePoint = Mp7JrsRelTimePointPtr(); // Class
	m_RelIncrTimePoint = Mp7JrsRelIncrTimePointPtr(); // Class with content 
	m_Duration = Mp7JrsdurationPtr(); // Pattern
	m_IncrDuration = Mp7JrsIncrDurationPtr(); // Class with content 


// no includefile for extension defined 
// file Mp7JrsTimeType_ExtMyPropInit.h

}

void Mp7JrsTimeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTimeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TimePoint);
	// Dc1Factory::DeleteObject(m_RelTimePoint);
	// Dc1Factory::DeleteObject(m_RelIncrTimePoint);
	// Dc1Factory::DeleteObject(m_Duration);
	// Dc1Factory::DeleteObject(m_IncrDuration);
}

void Mp7JrsTimeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TimeTypePtr, since we
	// might need GetBase(), which isn't defined in ITimeType
	const Dc1Ptr< Mp7JrsTimeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_TimePoint);
		this->SetTimePoint(Dc1Factory::CloneObject(tmp->GetTimePoint()));
		// Dc1Factory::DeleteObject(m_RelTimePoint);
		this->SetRelTimePoint(Dc1Factory::CloneObject(tmp->GetRelTimePoint()));
		// Dc1Factory::DeleteObject(m_RelIncrTimePoint);
		this->SetRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetRelIncrTimePoint()));
		// Dc1Factory::DeleteObject(m_Duration);
		this->SetDuration(Dc1Factory::CloneObject(tmp->GetDuration()));
		// Dc1Factory::DeleteObject(m_IncrDuration);
		this->SetIncrDuration(Dc1Factory::CloneObject(tmp->GetIncrDuration()));
}

Dc1NodePtr Mp7JrsTimeType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTimeType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrstimePointPtr Mp7JrsTimeType::GetTimePoint() const
{
		return m_TimePoint;
}

Mp7JrsRelTimePointPtr Mp7JrsTimeType::GetRelTimePoint() const
{
		return m_RelTimePoint;
}

Mp7JrsRelIncrTimePointPtr Mp7JrsTimeType::GetRelIncrTimePoint() const
{
		return m_RelIncrTimePoint;
}

Mp7JrsdurationPtr Mp7JrsTimeType::GetDuration() const
{
		return m_Duration;
}

Mp7JrsIncrDurationPtr Mp7JrsTimeType::GetIncrDuration() const
{
		return m_IncrDuration;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsTimeType::SetTimePoint(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeType::SetTimePoint().");
	}
	if (m_TimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_TimePoint);
		m_TimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimePoint) m_TimePoint->SetParent(m_myself.getPointer());
		if (m_TimePoint && m_TimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimePoint->UseTypeAttribute = true;
		}
		if(m_TimePoint != Mp7JrstimePointPtr())
		{
			m_TimePoint->SetContentName(XMLString::transcode("TimePoint"));
		}
	// Dc1Factory::DeleteObject(m_RelTimePoint);
	m_RelTimePoint = Mp7JrsRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_RelIncrTimePoint);
	m_RelIncrTimePoint = Mp7JrsRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTimeType::SetRelTimePoint(const Mp7JrsRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeType::SetRelTimePoint().");
	}
	if (m_RelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_RelTimePoint);
		m_RelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RelTimePoint) m_RelTimePoint->SetParent(m_myself.getPointer());
		if (m_RelTimePoint && m_RelTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RelTimePoint->UseTypeAttribute = true;
		}
		if(m_RelTimePoint != Mp7JrsRelTimePointPtr())
		{
			m_RelTimePoint->SetContentName(XMLString::transcode("RelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_TimePoint);
	m_TimePoint = Mp7JrstimePointPtr();
	// Dc1Factory::DeleteObject(m_RelIncrTimePoint);
	m_RelIncrTimePoint = Mp7JrsRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTimeType::SetRelIncrTimePoint(const Mp7JrsRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeType::SetRelIncrTimePoint().");
	}
	if (m_RelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_RelIncrTimePoint);
		m_RelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RelIncrTimePoint) m_RelIncrTimePoint->SetParent(m_myself.getPointer());
		if (m_RelIncrTimePoint && m_RelIncrTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelIncrTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RelIncrTimePoint->UseTypeAttribute = true;
		}
		if(m_RelIncrTimePoint != Mp7JrsRelIncrTimePointPtr())
		{
			m_RelIncrTimePoint->SetContentName(XMLString::transcode("RelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_TimePoint);
	m_TimePoint = Mp7JrstimePointPtr();
	// Dc1Factory::DeleteObject(m_RelTimePoint);
	m_RelTimePoint = Mp7JrsRelTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsTimeType::SetDuration(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeType::SetDuration().");
	}
	if (m_Duration != item)
	{
		// Dc1Factory::DeleteObject(m_Duration);
		m_Duration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Duration) m_Duration->SetParent(m_myself.getPointer());
		if (m_Duration && m_Duration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:durationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Duration->UseTypeAttribute = true;
		}
		if(m_Duration != Mp7JrsdurationPtr())
		{
			m_Duration->SetContentName(XMLString::transcode("Duration"));
		}
	// Dc1Factory::DeleteObject(m_IncrDuration);
	m_IncrDuration = Mp7JrsIncrDurationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTimeType::SetIncrDuration(const Mp7JrsIncrDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTimeType::SetIncrDuration().");
	}
	if (m_IncrDuration != item)
	{
		// Dc1Factory::DeleteObject(m_IncrDuration);
		m_IncrDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IncrDuration) m_IncrDuration->SetParent(m_myself.getPointer());
		if (m_IncrDuration && m_IncrDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IncrDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IncrDuration->UseTypeAttribute = true;
		}
		if(m_IncrDuration != Mp7JrsIncrDurationPtr())
		{
			m_IncrDuration->SetContentName(XMLString::transcode("IncrDuration"));
		}
	// Dc1Factory::DeleteObject(m_Duration);
	m_Duration = Mp7JrsdurationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsTimeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TimePoint")) == 0)
	{
		// TimePoint is simple element timePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrstimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTimePoint(p, client);
					if((p = GetTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RelTimePoint")) == 0)
	{
		// RelTimePoint is simple element RelTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRelTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRelTimePoint(p, client);
					if((p = GetRelTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RelIncrTimePoint")) == 0)
	{
		// RelIncrTimePoint is simple element RelIncrTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRelIncrTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelIncrTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelIncrTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRelIncrTimePoint(p, client);
					if((p = GetRelIncrTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Duration")) == 0)
	{
		// Duration is simple element durationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:durationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsdurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDuration(p, client);
					if((p = GetDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IncrDuration")) == 0)
	{
		// IncrDuration is simple element IncrDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIncrDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IncrDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIncrDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIncrDuration(p, client);
					if((p = GetIncrDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TimeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TimeType");
	}
	return result;
}

XMLCh * Mp7JrsTimeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTimeType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTimeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TimeType"));
	// Element serialization:
	if(m_TimePoint != Mp7JrstimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("TimePoint");
//		m_TimePoint->SetContentName(contentname);
		m_TimePoint->UseTypeAttribute = this->UseTypeAttribute;
		m_TimePoint->Serialize(doc, element);
	}
	// Class
	
	if (m_RelTimePoint != Mp7JrsRelTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RelTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RelTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RelTimePoint->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_RelIncrTimePoint != Mp7JrsRelIncrTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RelIncrTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RelIncrTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RelIncrTimePoint->Serialize(doc, element, &elem);
	}
	if(m_Duration != Mp7JrsdurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("Duration");
//		m_Duration->SetContentName(contentname);
		m_Duration->UseTypeAttribute = this->UseTypeAttribute;
		m_Duration->Serialize(doc, element);
	}
	// Class with content		
	
	if (m_IncrDuration != Mp7JrsIncrDurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IncrDuration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IncrDuration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IncrDuration->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTimeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("TimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimePoint")) == 0))
		{
			Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RelTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRelTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RelIncrTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRelIncrTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelIncrTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("Duration"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Duration")) == 0))
		{
			Mp7JrsdurationPtr tmp = CreatedurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IncrDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IncrDuration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIncrDurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIncrDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsTimeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTimeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatetimePointType; // FTT, check this
	}
	this->SetTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("RelTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRelTimePointType; // FTT, check this
	}
	this->SetRelTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("RelIncrTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRelIncrTimePointType; // FTT, check this
	}
	this->SetRelIncrTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("Duration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatedurationType; // FTT, check this
	}
	this->SetDuration(child);
  }
  if (XMLString::compareString(elementname, X("IncrDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIncrDurationType; // FTT, check this
	}
	this->SetIncrDuration(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTimeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimePoint() != Dc1NodePtr())
		result.Insert(GetTimePoint());
	if (GetRelTimePoint() != Dc1NodePtr())
		result.Insert(GetRelTimePoint());
	if (GetRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetRelIncrTimePoint());
	if (GetDuration() != Dc1NodePtr())
		result.Insert(GetDuration());
	if (GetIncrDuration() != Dc1NodePtr())
		result.Insert(GetIncrDuration());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTimeType_ExtMethodImpl.h


