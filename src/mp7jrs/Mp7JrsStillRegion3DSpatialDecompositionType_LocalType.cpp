
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsStillRegionType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsStillRegion3DType.h"
#include "Mp7JrsStillRegion3DSpatialDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsStillRegion3DSpatialDecompositionType_LocalType::IMp7JrsStillRegion3DSpatialDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsStillRegion3DSpatialDecompositionType_LocalType::~IMp7JrsStillRegion3DSpatialDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Mp7JrsStillRegion3DSpatialDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::~Mp7JrsStillRegion3DSpatialDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_StillRegion = Mp7JrsStillRegionPtr(); // Class
	m_StillRegionRef = Mp7JrsReferencePtr(); // Class
	m_StillRegion3D = Mp7JrsStillRegion3DPtr(); // Class
	m_StillRegion3DRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_StillRegion);
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	// Dc1Factory::DeleteObject(m_StillRegion3D);
	// Dc1Factory::DeleteObject(m_StillRegion3DRef);
}

void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StillRegion3DSpatialDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IStillRegion3DSpatialDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsStillRegion3DSpatialDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_StillRegion);
		this->SetStillRegion(Dc1Factory::CloneObject(tmp->GetStillRegion()));
		// Dc1Factory::DeleteObject(m_StillRegionRef);
		this->SetStillRegionRef(Dc1Factory::CloneObject(tmp->GetStillRegionRef()));
		// Dc1Factory::DeleteObject(m_StillRegion3D);
		this->SetStillRegion3D(Dc1Factory::CloneObject(tmp->GetStillRegion3D()));
		// Dc1Factory::DeleteObject(m_StillRegion3DRef);
		this->SetStillRegion3DRef(Dc1Factory::CloneObject(tmp->GetStillRegion3DRef()));
}

Dc1NodePtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsStillRegionPtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetStillRegion() const
{
		return m_StillRegion;
}

Mp7JrsReferencePtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetStillRegionRef() const
{
		return m_StillRegionRef;
}

Mp7JrsStillRegion3DPtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetStillRegion3D() const
{
		return m_StillRegion3D;
}

Mp7JrsReferencePtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetStillRegion3DRef() const
{
		return m_StillRegion3DRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion(const Mp7JrsStillRegionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion().");
	}
	if (m_StillRegion != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegion);
		m_StillRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegion) m_StillRegion->SetParent(m_myself.getPointer());
		if (m_StillRegion && m_StillRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegion->UseTypeAttribute = true;
		}
		if(m_StillRegion != Mp7JrsStillRegionPtr())
		{
			m_StillRegion->SetContentName(XMLString::transcode("StillRegion"));
		}
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion3D);
	m_StillRegion3D = Mp7JrsStillRegion3DPtr();
	// Dc1Factory::DeleteObject(m_StillRegion3DRef);
	m_StillRegion3DRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegionRef().");
	}
	if (m_StillRegionRef != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegionRef);
		m_StillRegionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegionRef) m_StillRegionRef->SetParent(m_myself.getPointer());
		if (m_StillRegionRef && m_StillRegionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegionRef->UseTypeAttribute = true;
		}
		if(m_StillRegionRef != Mp7JrsReferencePtr())
		{
			m_StillRegionRef->SetContentName(XMLString::transcode("StillRegionRef"));
		}
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegion3D);
	m_StillRegion3D = Mp7JrsStillRegion3DPtr();
	// Dc1Factory::DeleteObject(m_StillRegion3DRef);
	m_StillRegion3DRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion3D(const Mp7JrsStillRegion3DPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion3D().");
	}
	if (m_StillRegion3D != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegion3D);
		m_StillRegion3D = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegion3D) m_StillRegion3D->SetParent(m_myself.getPointer());
		if (m_StillRegion3D && m_StillRegion3D->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegion3D->UseTypeAttribute = true;
		}
		if(m_StillRegion3D != Mp7JrsStillRegion3DPtr())
		{
			m_StillRegion3D->SetContentName(XMLString::transcode("StillRegion3D"));
		}
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion3DRef);
	m_StillRegion3DRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion3DRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::SetStillRegion3DRef().");
	}
	if (m_StillRegion3DRef != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegion3DRef);
		m_StillRegion3DRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegion3DRef) m_StillRegion3DRef->SetParent(m_myself.getPointer());
		if (m_StillRegion3DRef && m_StillRegion3DRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegion3DRef->UseTypeAttribute = true;
		}
		if(m_StillRegion3DRef != Mp7JrsReferencePtr())
		{
			m_StillRegion3DRef->SetContentName(XMLString::transcode("StillRegion3DRef"));
		}
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion3D);
	m_StillRegion3D = Mp7JrsStillRegion3DPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: StillRegion3DSpatialDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_StillRegion != Mp7JrsStillRegionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StillRegionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegionRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StillRegion3D != Mp7JrsStillRegion3DPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegion3D->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegion3D"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegion3D->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StillRegion3DRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegion3DRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegion3DRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegion3DRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateStillRegionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegion3D"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegion3D")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateStillRegion3DType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegion3D(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegion3DRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegion3DRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegion3DRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("StillRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateStillRegionType; // FTT, check this
	}
	this->SetStillRegion(child);
  }
  if (XMLString::compareString(elementname, X("StillRegionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetStillRegionRef(child);
  }
  if (XMLString::compareString(elementname, X("StillRegion3D")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateStillRegion3DType; // FTT, check this
	}
	this->SetStillRegion3D(child);
  }
  if (XMLString::compareString(elementname, X("StillRegion3DRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetStillRegion3DRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStillRegion3DSpatialDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStillRegion() != Dc1NodePtr())
		result.Insert(GetStillRegion());
	if (GetStillRegionRef() != Dc1NodePtr())
		result.Insert(GetStillRegionRef());
	if (GetStillRegion3D() != Dc1NodePtr())
		result.Insert(GetStillRegion3D());
	if (GetStillRegion3DRef() != Dc1NodePtr())
		result.Insert(GetStillRegion3DRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStillRegion3DSpatialDecompositionType_LocalType_ExtMethodImpl.h


