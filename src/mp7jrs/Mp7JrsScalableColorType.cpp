
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsScalableColorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsScalableColorType::IMp7JrsScalableColorType()
{

// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtPropInit.h

}

IMp7JrsScalableColorType::~IMp7JrsScalableColorType()
{
// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtPropCleanup.h

}

Mp7JrsScalableColorType::Mp7JrsScalableColorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsScalableColorType::~Mp7JrsScalableColorType()
{
	Cleanup();
}

void Mp7JrsScalableColorType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_numOfCoeff = Mp7JrsScalableColorType_numOfCoeff_LocalType::UninitializedEnumeration;
	m_numOfBitplanesDiscarded = Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_Coeff = Mp7JrsintegerVectorPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtMyPropInit.h

}

void Mp7JrsScalableColorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Coeff);
}

void Mp7JrsScalableColorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ScalableColorTypePtr, since we
	// might need GetBase(), which isn't defined in IScalableColorType
	const Dc1Ptr< Mp7JrsScalableColorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsScalableColorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->SetnumOfCoeff(tmp->GetnumOfCoeff());
	}
	{
		this->SetnumOfBitplanesDiscarded(tmp->GetnumOfBitplanesDiscarded());
	}
		// Dc1Factory::DeleteObject(m_Coeff);
		this->SetCoeff(Dc1Factory::CloneObject(tmp->GetCoeff()));
}

Dc1NodePtr Mp7JrsScalableColorType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsScalableColorType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsScalableColorType::GetBase() const
{
	return m_Base;
}

Mp7JrsScalableColorType_numOfCoeff_LocalType::Enumeration Mp7JrsScalableColorType::GetnumOfCoeff() const
{
	return m_numOfCoeff;
}

void Mp7JrsScalableColorType::SetnumOfCoeff(Mp7JrsScalableColorType_numOfCoeff_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsScalableColorType::SetnumOfCoeff().");
	}
	m_numOfCoeff = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::Enumeration Mp7JrsScalableColorType::GetnumOfBitplanesDiscarded() const
{
	return m_numOfBitplanesDiscarded;
}

void Mp7JrsScalableColorType::SetnumOfBitplanesDiscarded(Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsScalableColorType::SetnumOfBitplanesDiscarded().");
	}
	m_numOfBitplanesDiscarded = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsScalableColorType::GetCoeff() const
{
		return m_Coeff;
}

void Mp7JrsScalableColorType::SetCoeff(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsScalableColorType::SetCoeff().");
	}
	if (m_Coeff != item)
	{
		// Dc1Factory::DeleteObject(m_Coeff);
		m_Coeff = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Coeff) m_Coeff->SetParent(m_myself.getPointer());
		if(m_Coeff != Mp7JrsintegerVectorPtr())
		{
			m_Coeff->SetContentName(XMLString::transcode("Coeff"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsScalableColorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfCoeff")) == 0)
	{
		// numOfCoeff is simple attribute ScalableColorType_numOfCoeff_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsScalableColorType_numOfCoeff_LocalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfBitplanesDiscarded")) == 0)
	{
		// numOfBitplanesDiscarded is simple attribute ScalableColorType_numOfBitplanesDiscarded_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Coeff")) == 0)
	{
		// Coeff is simple element integerVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoeff()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:integerVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsintegerVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoeff(p, client);
					if((p = GetCoeff()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ScalableColorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ScalableColorType");
	}
	return result;
}

XMLCh * Mp7JrsScalableColorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsScalableColorType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsScalableColorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ScalableColorType"));
	// Attribute Serialization:
	// Enumeration
	if(m_numOfCoeff != Mp7JrsScalableColorType_numOfCoeff_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsScalableColorType_numOfCoeff_LocalType::ToText(m_numOfCoeff);
		element->setAttributeNS(X(""), X("numOfCoeff"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Enumeration
	if(m_numOfBitplanesDiscarded != Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::ToText(m_numOfBitplanesDiscarded);
		element->setAttributeNS(X(""), X("numOfBitplanesDiscarded"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	if (m_Coeff != Mp7JrsintegerVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Coeff->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsScalableColorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfCoeff")))
	{
		this->SetnumOfCoeff(Mp7JrsScalableColorType_numOfCoeff_LocalType::Parse(parent->getAttribute(X("numOfCoeff"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfBitplanesDiscarded")))
	{
		this->SetnumOfBitplanesDiscarded(Mp7JrsScalableColorType_numOfBitplanesDiscarded_LocalType::Parse(parent->getAttribute(X("numOfBitplanesDiscarded"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Coeff")) == 0))
		{
			// Deserialize factory type
			Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCoeff(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsScalableColorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Coeff")) == 0))
  {
	Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
	this->SetCoeff(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsScalableColorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCoeff() != Dc1NodePtr())
		result.Insert(GetCoeff());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsScalableColorType_ExtMethodImpl.h


