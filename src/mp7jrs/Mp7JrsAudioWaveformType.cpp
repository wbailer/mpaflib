
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtImplInclude.h


#include "Mp7JrsAudioLLDScalarType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsAudioWaveformType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfScalar

#include <assert.h>
IMp7JrsAudioWaveformType::IMp7JrsAudioWaveformType()
{

// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtPropInit.h

}

IMp7JrsAudioWaveformType::~IMp7JrsAudioWaveformType()
{
// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtPropCleanup.h

}

Mp7JrsAudioWaveformType::Mp7JrsAudioWaveformType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioWaveformType::~Mp7JrsAudioWaveformType()
{
	Cleanup();
}

void Mp7JrsAudioWaveformType::Init()
{
	// Init base
	m_Base = CreateAudioLLDScalarType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_minRange = 0.0f; // Value
	m_minRange_Exist = false;
	m_maxRange = 0.0f; // Value
	m_maxRange_Exist = false;



// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtMyPropInit.h

}

void Mp7JrsAudioWaveformType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsAudioWaveformType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioWaveformTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioWaveformType
	const Dc1Ptr< Mp7JrsAudioWaveformType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioLLDScalarPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioWaveformType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistminRange())
	{
		this->SetminRange(tmp->GetminRange());
	}
	else
	{
		InvalidateminRange();
	}
	}
	{
	if (tmp->ExistmaxRange())
	{
		this->SetmaxRange(tmp->GetmaxRange());
	}
	else
	{
		InvalidatemaxRange();
	}
	}
}

Dc1NodePtr Mp7JrsAudioWaveformType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioWaveformType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioLLDScalarType > Mp7JrsAudioWaveformType::GetBase() const
{
	return m_Base;
}

float Mp7JrsAudioWaveformType::GetminRange() const
{
	return m_minRange;
}

bool Mp7JrsAudioWaveformType::ExistminRange() const
{
	return m_minRange_Exist;
}
void Mp7JrsAudioWaveformType::SetminRange(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::SetminRange().");
	}
	m_minRange = item;
	m_minRange_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioWaveformType::InvalidateminRange()
{
	m_minRange_Exist = false;
}
float Mp7JrsAudioWaveformType::GetmaxRange() const
{
	return m_maxRange;
}

bool Mp7JrsAudioWaveformType::ExistmaxRange() const
{
	return m_maxRange_Exist;
}
void Mp7JrsAudioWaveformType::SetmaxRange(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::SetmaxRange().");
	}
	m_maxRange = item;
	m_maxRange_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioWaveformType::InvalidatemaxRange()
{
	m_maxRange_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsAudioWaveformType::Getconfidence() const
{
	return GetBase()->Getconfidence();
}

bool Mp7JrsAudioWaveformType::Existconfidence() const
{
	return GetBase()->Existconfidence();
}
void Mp7JrsAudioWaveformType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::Setconfidence().");
	}
	GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioWaveformType::Invalidateconfidence()
{
	GetBase()->Invalidateconfidence();
}
float Mp7JrsAudioWaveformType::GetScalar() const
{
	return GetBase()->GetScalar();
}

Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr Mp7JrsAudioWaveformType::GetSeriesOfScalar() const
{
	return GetBase()->GetSeriesOfScalar();
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioWaveformType::SetScalar(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::SetScalar().");
	}
	GetBase()->SetScalar(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioWaveformType::SetSeriesOfScalar(const Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::SetSeriesOfScalar().");
	}
	GetBase()->SetSeriesOfScalar(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsAudioWaveformType::Getchannels() const
{
	return GetBase()->GetBase()->Getchannels();
}

bool Mp7JrsAudioWaveformType::Existchannels() const
{
	return GetBase()->GetBase()->Existchannels();
}
void Mp7JrsAudioWaveformType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioWaveformType::Setchannels().");
	}
	GetBase()->GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioWaveformType::Invalidatechannels()
{
	GetBase()->GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsAudioWaveformType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("minRange")) == 0)
	{
		// minRange is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("maxRange")) == 0)
	{
		// maxRange is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioWaveformType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioWaveformType");
	}
	return result;
}

XMLCh * Mp7JrsAudioWaveformType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioWaveformType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsAudioWaveformType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioWaveformType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_minRange_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_minRange);
		element->setAttributeNS(X(""), X("minRange"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_maxRange_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_maxRange);
		element->setAttributeNS(X(""), X("maxRange"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsAudioWaveformType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("minRange")))
	{
		// deserialize value type
		this->SetminRange(Dc1Convert::TextToFloat(parent->getAttribute(X("minRange"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("maxRange")))
	{
		// deserialize value type
		this->SetmaxRange(Dc1Convert::TextToFloat(parent->getAttribute(X("maxRange"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioLLDScalarType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioWaveformType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioWaveformType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSeriesOfScalar() != Dc1NodePtr())
		result.Insert(GetSeriesOfScalar());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioWaveformType_ExtMethodImpl.h


