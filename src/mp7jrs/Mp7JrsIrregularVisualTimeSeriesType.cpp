
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtImplInclude.h


#include "Mp7JrsVisualTimeSeriesType.h"
#include "Mp7JrsIrregularVisualTimeSeriesType_CollectionType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsIrregularVisualTimeSeriesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsIrregularVisualTimeSeriesType_LocalType.h" // Sequence collection Descriptor
#include "Mp7JrsVisualDType.h" // Sequence collection element Descriptor
#include "Mp7Jrsunsigned32.h" // Sequence collection element Interval

#include <assert.h>
IMp7JrsIrregularVisualTimeSeriesType::IMp7JrsIrregularVisualTimeSeriesType()
{

// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtPropInit.h

}

IMp7JrsIrregularVisualTimeSeriesType::~IMp7JrsIrregularVisualTimeSeriesType()
{
// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtPropCleanup.h

}

Mp7JrsIrregularVisualTimeSeriesType::Mp7JrsIrregularVisualTimeSeriesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsIrregularVisualTimeSeriesType::~Mp7JrsIrregularVisualTimeSeriesType()
{
	Cleanup();
}

void Mp7JrsIrregularVisualTimeSeriesType::Init()
{
	// Init base
	m_Base = CreateVisualTimeSeriesType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_IrregularVisualTimeSeriesType_LocalType = Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtMyPropInit.h

}

void Mp7JrsIrregularVisualTimeSeriesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_IrregularVisualTimeSeriesType_LocalType);
}

void Mp7JrsIrregularVisualTimeSeriesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IrregularVisualTimeSeriesTypePtr, since we
	// might need GetBase(), which isn't defined in IIrregularVisualTimeSeriesType
	const Dc1Ptr< Mp7JrsIrregularVisualTimeSeriesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualTimeSeriesPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsIrregularVisualTimeSeriesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_IrregularVisualTimeSeriesType_LocalType);
		this->SetIrregularVisualTimeSeriesType_LocalType(Dc1Factory::CloneObject(tmp->GetIrregularVisualTimeSeriesType_LocalType()));
}

Dc1NodePtr Mp7JrsIrregularVisualTimeSeriesType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsIrregularVisualTimeSeriesType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsVisualTimeSeriesType > Mp7JrsIrregularVisualTimeSeriesType::GetBase() const
{
	return m_Base;
}

Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr Mp7JrsIrregularVisualTimeSeriesType::GetIrregularVisualTimeSeriesType_LocalType() const
{
		return m_IrregularVisualTimeSeriesType_LocalType;
}

void Mp7JrsIrregularVisualTimeSeriesType::SetIrregularVisualTimeSeriesType_LocalType(const Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIrregularVisualTimeSeriesType::SetIrregularVisualTimeSeriesType_LocalType().");
	}
	if (m_IrregularVisualTimeSeriesType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_IrregularVisualTimeSeriesType_LocalType);
		m_IrregularVisualTimeSeriesType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IrregularVisualTimeSeriesType_LocalType) m_IrregularVisualTimeSeriesType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsmediaDurationPtr Mp7JrsIrregularVisualTimeSeriesType::Getoffset() const
{
	return GetBase()->Getoffset();
}

bool Mp7JrsIrregularVisualTimeSeriesType::Existoffset() const
{
	return GetBase()->Existoffset();
}
void Mp7JrsIrregularVisualTimeSeriesType::Setoffset(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIrregularVisualTimeSeriesType::Setoffset().");
	}
	GetBase()->Setoffset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIrregularVisualTimeSeriesType::Invalidateoffset()
{
	GetBase()->Invalidateoffset();
}
Mp7JrsmediaDurationPtr Mp7JrsIrregularVisualTimeSeriesType::GetTimeIncr() const
{
	return GetBase()->GetTimeIncr();
}

void Mp7JrsIrregularVisualTimeSeriesType::SetTimeIncr(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIrregularVisualTimeSeriesType::SetTimeIncr().");
	}
	GetBase()->SetTimeIncr(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsIrregularVisualTimeSeriesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// Descriptor is contained in itemtype IrregularVisualTimeSeriesType_LocalType
		// in sequence collection IrregularVisualTimeSeriesType_CollectionType

		context->Found = true;
		Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr coll = GetIrregularVisualTimeSeriesType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateIrregularVisualTimeSeriesType_CollectionType; // FTT, check this
				SetIrregularVisualTimeSeriesType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->GetDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsIrregularVisualTimeSeriesType_LocalPtr item = CreateIrregularVisualTimeSeriesType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j))->SetDescriptor(
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j+1))->GetDescriptor());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j))->SetDescriptor(
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j-1))->GetDescriptor());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->SetDescriptor(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->GetDescriptor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Interval")) == 0)
	{
		// Interval is contained in itemtype IrregularVisualTimeSeriesType_LocalType
		// in sequence collection IrregularVisualTimeSeriesType_CollectionType

		context->Found = true;
		Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr coll = GetIrregularVisualTimeSeriesType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateIrregularVisualTimeSeriesType_CollectionType; // FTT, check this
				SetIrregularVisualTimeSeriesType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->GetInterval()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsIrregularVisualTimeSeriesType_LocalPtr item = CreateIrregularVisualTimeSeriesType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j))->SetInterval(
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j+1))->GetInterval());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j))->SetInterval(
						((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(j-1))->GetInterval());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->SetInterval(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsIrregularVisualTimeSeriesType_LocalPtr)coll->elementAt(i))->GetInterval()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IrregularVisualTimeSeriesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IrregularVisualTimeSeriesType");
	}
	return result;
}

XMLCh * Mp7JrsIrregularVisualTimeSeriesType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsIrregularVisualTimeSeriesType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsIrregularVisualTimeSeriesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("IrregularVisualTimeSeriesType"));
	// Element serialization:
	if (m_IrregularVisualTimeSeriesType_LocalType != Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_IrregularVisualTimeSeriesType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsIrregularVisualTimeSeriesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualTimeSeriesType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Descriptor")) == 0)
			(Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Interval")) == 0)
			(Dc1Util::HasNodeName(parent, X("Interval"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr tmp = CreateIrregularVisualTimeSeriesType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetIrregularVisualTimeSeriesType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsIrregularVisualTimeSeriesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsIrregularVisualTimeSeriesType_CollectionPtr tmp = CreateIrregularVisualTimeSeriesType_CollectionType; // FTT, check this
	this->SetIrregularVisualTimeSeriesType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsIrregularVisualTimeSeriesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetIrregularVisualTimeSeriesType_LocalType() != Dc1NodePtr())
		result.Insert(GetIrregularVisualTimeSeriesType_LocalType());
	if (GetTimeIncr() != Dc1NodePtr())
		result.Insert(GetTimeIncr());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsIrregularVisualTimeSeriesType_ExtMethodImpl.h


