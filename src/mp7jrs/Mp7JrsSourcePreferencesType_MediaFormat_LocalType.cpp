
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaFormatType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsMediaFormatType_BitRate_LocalType.h"
#include "Mp7JrsMediaFormatType_ScalableCoding_LocalType2.h"
#include "Mp7JrsMediaFormatType_VisualCoding_LocalType.h"
#include "Mp7JrsMediaFormatType_AudioCoding_LocalType.h"
#include "Mp7JrsSourcePreferencesType_MediaFormat_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSourcePreferencesType_MediaFormat_LocalType::IMp7JrsSourcePreferencesType_MediaFormat_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtPropInit.h

}

IMp7JrsSourcePreferencesType_MediaFormat_LocalType::~IMp7JrsSourcePreferencesType_MediaFormat_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtPropCleanup.h

}

Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Mp7JrsSourcePreferencesType_MediaFormat_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSourcePreferencesType_MediaFormat_LocalType::~Mp7JrsSourcePreferencesType_MediaFormat_LocalType()
{
	Cleanup();
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Init()
{
	// Init base
	m_Base = CreateMediaFormatType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtMyPropInit.h

}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SourcePreferencesType_MediaFormat_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISourcePreferencesType_MediaFormat_LocalType
	const Dc1Ptr< Mp7JrsSourcePreferencesType_MediaFormat_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaFormatPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSourcePreferencesType_MediaFormat_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
}

Dc1NodePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMediaFormatType > Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrspreferenceValuePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetContent() const
{
	return GetBase()->GetContent();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetMedium() const
{
	return GetBase()->GetMedium();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidMedium() const
{
	return GetBase()->IsValidMedium();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetFileFormat() const
{
	return GetBase()->GetFileFormat();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidFileFormat() const
{
	return GetBase()->IsValidFileFormat();
}

unsigned Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetFileSize() const
{
	return GetBase()->GetFileSize();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidFileSize() const
{
	return GetBase()->IsValidFileSize();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetSystem() const
{
	return GetBase()->GetSystem();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidSystem() const
{
	return GetBase()->IsValidSystem();
}

float Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetBandwidth() const
{
	return GetBase()->GetBandwidth();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidBandwidth() const
{
	return GetBase()->IsValidBandwidth();
}

Mp7JrsMediaFormatType_BitRate_LocalPtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetBitRate() const
{
	return GetBase()->GetBitRate();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidBitRate() const
{
	return GetBase()->IsValidBitRate();
}

unsigned Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetTargetChannelBitRate() const
{
	return GetBase()->GetTargetChannelBitRate();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidTargetChannelBitRate() const
{
	return GetBase()->IsValidTargetChannelBitRate();
}

Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetScalableCoding() const
{
	return GetBase()->GetScalableCoding();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidScalableCoding() const
{
	return GetBase()->IsValidScalableCoding();
}

Mp7JrsMediaFormatType_VisualCoding_LocalPtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetVisualCoding() const
{
	return GetBase()->GetVisualCoding();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidVisualCoding() const
{
	return GetBase()->IsValidVisualCoding();
}

Mp7JrsMediaFormatType_AudioCoding_LocalPtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetAudioCoding() const
{
	return GetBase()->GetAudioCoding();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidAudioCoding() const
{
	return GetBase()->IsValidAudioCoding();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetSceneCodingFormat() const
{
	return GetBase()->GetSceneCodingFormat();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidSceneCodingFormat() const
{
	return GetBase()->IsValidSceneCodingFormat();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetGraphicsCodingFormat() const
{
	return GetBase()->GetGraphicsCodingFormat();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidGraphicsCodingFormat() const
{
	return GetBase()->IsValidGraphicsCodingFormat();
}

Mp7JrsControlledTermUsePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetOtherCodingFormat() const
{
	return GetBase()->GetOtherCodingFormat();
}

// Element is optional
bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::IsValidOtherCodingFormat() const
{
	return GetBase()->IsValidOtherCodingFormat();
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetContent(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetContent().");
	}
	GetBase()->SetContent(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetMedium(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetMedium().");
	}
	GetBase()->SetMedium(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateMedium()
{
	GetBase()->InvalidateMedium();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetFileFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetFileFormat().");
	}
	GetBase()->SetFileFormat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateFileFormat()
{
	GetBase()->InvalidateFileFormat();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetFileSize(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetFileSize().");
	}
	GetBase()->SetFileSize(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateFileSize()
{
	GetBase()->InvalidateFileSize();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetSystem(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetSystem().");
	}
	GetBase()->SetSystem(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateSystem()
{
	GetBase()->InvalidateSystem();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetBandwidth(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetBandwidth().");
	}
	GetBase()->SetBandwidth(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateBandwidth()
{
	GetBase()->InvalidateBandwidth();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetBitRate(const Mp7JrsMediaFormatType_BitRate_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetBitRate().");
	}
	GetBase()->SetBitRate(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateBitRate()
{
	GetBase()->InvalidateBitRate();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetTargetChannelBitRate(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetTargetChannelBitRate().");
	}
	GetBase()->SetTargetChannelBitRate(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateTargetChannelBitRate()
{
	GetBase()->InvalidateTargetChannelBitRate();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetScalableCoding(const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetScalableCoding().");
	}
	GetBase()->SetScalableCoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateScalableCoding()
{
	GetBase()->InvalidateScalableCoding();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetVisualCoding(const Mp7JrsMediaFormatType_VisualCoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetVisualCoding().");
	}
	GetBase()->SetVisualCoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateVisualCoding()
{
	GetBase()->InvalidateVisualCoding();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetAudioCoding(const Mp7JrsMediaFormatType_AudioCoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetAudioCoding().");
	}
	GetBase()->SetAudioCoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateAudioCoding()
{
	GetBase()->InvalidateAudioCoding();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetSceneCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetSceneCodingFormat().");
	}
	GetBase()->SetSceneCodingFormat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateSceneCodingFormat()
{
	GetBase()->InvalidateSceneCodingFormat();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetGraphicsCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetGraphicsCodingFormat().");
	}
	GetBase()->SetGraphicsCodingFormat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateGraphicsCodingFormat()
{
	GetBase()->InvalidateGraphicsCodingFormat();
}
void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetOtherCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSourcePreferencesType_MediaFormat_LocalType::SetOtherCodingFormat().");
	}
	GetBase()->SetOtherCodingFormat(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::InvalidateOtherCodingFormat()
{
	GetBase()->InvalidateOtherCodingFormat();
}

Dc1NodeEnum Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SourcePreferencesType_MediaFormat_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SourcePreferencesType_MediaFormat_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSourcePreferencesType_MediaFormat_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SourcePreferencesType_MediaFormat_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSourcePreferencesType_MediaFormat_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsMediaFormatType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSourcePreferencesType_MediaFormat_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSourcePreferencesType_MediaFormat_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContent() != Dc1NodePtr())
		result.Insert(GetContent());
	if (GetMedium() != Dc1NodePtr())
		result.Insert(GetMedium());
	if (GetFileFormat() != Dc1NodePtr())
		result.Insert(GetFileFormat());
	if (GetSystem() != Dc1NodePtr())
		result.Insert(GetSystem());
	if (GetBitRate() != Dc1NodePtr())
		result.Insert(GetBitRate());
	if (GetScalableCoding() != Dc1NodePtr())
		result.Insert(GetScalableCoding());
	if (GetVisualCoding() != Dc1NodePtr())
		result.Insert(GetVisualCoding());
	if (GetAudioCoding() != Dc1NodePtr())
		result.Insert(GetAudioCoding());
	if (GetSceneCodingFormat() != Dc1NodePtr())
		result.Insert(GetSceneCodingFormat());
	if (GetGraphicsCodingFormat() != Dc1NodePtr())
		result.Insert(GetGraphicsCodingFormat());
	if (GetOtherCodingFormat() != Dc1NodePtr())
		result.Insert(GetOtherCodingFormat());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSourcePreferencesType_MediaFormat_LocalType_ExtMethodImpl.h


