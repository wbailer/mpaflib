
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtImplInclude.h


#include "Mp7JrsContinuousHiddenMarkovModelType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsAudioSpectrumBasisType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionType.h"
#include "Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionType.h"
#include "Mp7JrsProbabilityMatrixType.h"
#include "Mp7JrsStateTransitionModelType_State_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSoundModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsModelStateType.h" // Element collection urn:mpeg:mpeg7:schema:2004:State
#include "Mp7JrsDescriptorModelType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DescriptorModel
#include "Mp7JrsContinuousDistributionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ObservationDistribution

#include <assert.h>
IMp7JrsSoundModelType::IMp7JrsSoundModelType()
{

// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtPropInit.h

}

IMp7JrsSoundModelType::~IMp7JrsSoundModelType()
{
// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtPropCleanup.h

}

Mp7JrsSoundModelType::Mp7JrsSoundModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSoundModelType::~Mp7JrsSoundModelType()
{
	Cleanup();
}

void Mp7JrsSoundModelType::Init()
{
	// Init base
	m_Base = CreateContinuousHiddenMarkovModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_SoundModelRef = NULL; // String
	m_SoundModelRef_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SoundClassLabel = Mp7JrsTermUsePtr(); // Class
	m_SpectrumBasis = Mp7JrsAudioSpectrumBasisPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtMyPropInit.h

}

void Mp7JrsSoundModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_SoundModelRef); // String
	// Dc1Factory::DeleteObject(m_SoundClassLabel);
	// Dc1Factory::DeleteObject(m_SpectrumBasis);
}

void Mp7JrsSoundModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SoundModelTypePtr, since we
	// might need GetBase(), which isn't defined in ISoundModelType
	const Dc1Ptr< Mp7JrsSoundModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContinuousHiddenMarkovModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSoundModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_SoundModelRef); // String
	if (tmp->ExistSoundModelRef())
	{
		this->SetSoundModelRef(XMLString::replicate(tmp->GetSoundModelRef()));
	}
	else
	{
		InvalidateSoundModelRef();
	}
	}
		// Dc1Factory::DeleteObject(m_SoundClassLabel);
		this->SetSoundClassLabel(Dc1Factory::CloneObject(tmp->GetSoundClassLabel()));
		// Dc1Factory::DeleteObject(m_SpectrumBasis);
		this->SetSpectrumBasis(Dc1Factory::CloneObject(tmp->GetSpectrumBasis()));
}

Dc1NodePtr Mp7JrsSoundModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSoundModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContinuousHiddenMarkovModelType > Mp7JrsSoundModelType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsSoundModelType::GetSoundModelRef() const
{
	return m_SoundModelRef;
}

bool Mp7JrsSoundModelType::ExistSoundModelRef() const
{
	return m_SoundModelRef_Exist;
}
void Mp7JrsSoundModelType::SetSoundModelRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetSoundModelRef().");
	}
	m_SoundModelRef = item;
	m_SoundModelRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidateSoundModelRef()
{
	m_SoundModelRef_Exist = false;
}
Mp7JrsTermUsePtr Mp7JrsSoundModelType::GetSoundClassLabel() const
{
		return m_SoundClassLabel;
}

Mp7JrsAudioSpectrumBasisPtr Mp7JrsSoundModelType::GetSpectrumBasis() const
{
		return m_SpectrumBasis;
}

void Mp7JrsSoundModelType::SetSoundClassLabel(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetSoundClassLabel().");
	}
	if (m_SoundClassLabel != item)
	{
		// Dc1Factory::DeleteObject(m_SoundClassLabel);
		m_SoundClassLabel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SoundClassLabel) m_SoundClassLabel->SetParent(m_myself.getPointer());
		if (m_SoundClassLabel && m_SoundClassLabel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SoundClassLabel->UseTypeAttribute = true;
		}
		if(m_SoundClassLabel != Mp7JrsTermUsePtr())
		{
			m_SoundClassLabel->SetContentName(XMLString::transcode("SoundClassLabel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::SetSpectrumBasis(const Mp7JrsAudioSpectrumBasisPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetSpectrumBasis().");
	}
	if (m_SpectrumBasis != item)
	{
		// Dc1Factory::DeleteObject(m_SpectrumBasis);
		m_SpectrumBasis = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpectrumBasis) m_SpectrumBasis->SetParent(m_myself.getPointer());
		if (m_SpectrumBasis && m_SpectrumBasis->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumBasisType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpectrumBasis->UseTypeAttribute = true;
		}
		if(m_SpectrumBasis != Mp7JrsAudioSpectrumBasisPtr())
		{
			m_SpectrumBasis->SetContentName(XMLString::transcode("SpectrumBasis"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr Mp7JrsSoundModelType::GetDescriptorModel() const
{
	return GetBase()->GetDescriptorModel();
}

Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr Mp7JrsSoundModelType::GetObservationDistribution() const
{
	return GetBase()->GetObservationDistribution();
}

void Mp7JrsSoundModelType::SetDescriptorModel(const Mp7JrsContinuousHiddenMarkovModelType_DescriptorModel_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetDescriptorModel().");
	}
	GetBase()->SetDescriptorModel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::SetObservationDistribution(const Mp7JrsContinuousHiddenMarkovModelType_ObservationDistribution_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetObservationDistribution().");
	}
	GetBase()->SetObservationDistribution(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsProbabilityMatrixPtr Mp7JrsSoundModelType::GetInitial() const
{
	return GetBase()->GetBase()->GetInitial();
}

// Element is optional
bool Mp7JrsSoundModelType::IsValidInitial() const
{
	return GetBase()->GetBase()->IsValidInitial();
}

Mp7JrsProbabilityMatrixPtr Mp7JrsSoundModelType::GetTransitions() const
{
	return GetBase()->GetBase()->GetTransitions();
}

Mp7JrsStateTransitionModelType_State_CollectionPtr Mp7JrsSoundModelType::GetState() const
{
	return GetBase()->GetBase()->GetState();
}

void Mp7JrsSoundModelType::SetInitial(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetInitial().");
	}
	GetBase()->GetBase()->SetInitial(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidateInitial()
{
	GetBase()->GetBase()->InvalidateInitial();
}
void Mp7JrsSoundModelType::SetTransitions(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetTransitions().");
	}
	GetBase()->GetBase()->SetTransitions(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::SetState(const Mp7JrsStateTransitionModelType_State_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetState().");
	}
	GetBase()->GetBase()->SetState(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsSoundModelType::GetnumOfStates() const
{
	return GetBase()->GetBase()->GetBase()->GetnumOfStates();
}

void Mp7JrsSoundModelType::SetnumOfStates(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetnumOfStates().");
	}
	GetBase()->GetBase()->GetBase()->SetnumOfStates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsSoundModelType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsSoundModelType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsSoundModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsSoundModelType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsSoundModelType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsSoundModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsSoundModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsSoundModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsSoundModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSoundModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSoundModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSoundModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSoundModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSoundModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSoundModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSoundModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSoundModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSoundModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSoundModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSoundModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSoundModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSoundModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSoundModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSoundModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("SoundModelRef")) == 0)
	{
		// SoundModelRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SoundClassLabel")) == 0)
	{
		// SoundClassLabel is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSoundClassLabel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSoundClassLabel(p, client);
					if((p = GetSoundClassLabel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpectrumBasis")) == 0)
	{
		// SpectrumBasis is simple element AudioSpectrumBasisType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpectrumBasis()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSpectrumBasisType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSpectrumBasisPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpectrumBasis(p, client);
					if((p = GetSpectrumBasis()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SoundModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SoundModelType");
	}
	return result;
}

XMLCh * Mp7JrsSoundModelType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSoundModelType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSoundModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SoundModelType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_SoundModelRef_Exist)
	{
	// String
	if(m_SoundModelRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("SoundModelRef"), m_SoundModelRef);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_SoundClassLabel != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SoundClassLabel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SoundClassLabel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SoundClassLabel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpectrumBasis != Mp7JrsAudioSpectrumBasisPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpectrumBasis->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpectrumBasis"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpectrumBasis->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSoundModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("SoundModelRef")))
	{
		// Deserialize string type
		this->SetSoundModelRef(Dc1Convert::TextToString(parent->getAttribute(X("SoundModelRef"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsContinuousHiddenMarkovModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SoundClassLabel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SoundClassLabel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSoundClassLabel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpectrumBasis"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpectrumBasis")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioSpectrumBasisType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpectrumBasis(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSoundModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("SoundClassLabel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetSoundClassLabel(child);
  }
  if (XMLString::compareString(elementname, X("SpectrumBasis")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioSpectrumBasisType; // FTT, check this
	}
	this->SetSpectrumBasis(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSoundModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSoundClassLabel() != Dc1NodePtr())
		result.Insert(GetSoundClassLabel());
	if (GetSpectrumBasis() != Dc1NodePtr())
		result.Insert(GetSpectrumBasis());
	if (GetDescriptorModel() != Dc1NodePtr())
		result.Insert(GetDescriptorModel());
	if (GetObservationDistribution() != Dc1NodePtr())
		result.Insert(GetObservationDistribution());
	if (GetInitial() != Dc1NodePtr())
		result.Insert(GetInitial());
	if (GetTransitions() != Dc1NodePtr())
		result.Insert(GetTransitions());
	if (GetState() != Dc1NodePtr())
		result.Insert(GetState());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSoundModelType_ExtMethodImpl.h


