
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtImplInclude.h


#include "Mp7JrsSignalPlaneOriginType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSignalPlaneOriginType::IMp7JrsSignalPlaneOriginType()
{

// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtPropInit.h

}

IMp7JrsSignalPlaneOriginType::~IMp7JrsSignalPlaneOriginType()
{
// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtPropCleanup.h

}

Mp7JrsSignalPlaneOriginType::Mp7JrsSignalPlaneOriginType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSignalPlaneOriginType::~Mp7JrsSignalPlaneOriginType()
{
	Cleanup();
}

void Mp7JrsSignalPlaneOriginType::Init()
{

	// Init attributes
	m_xOrigin = Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::UninitializedEnumeration;
	m_xOrigin_Default = Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::left; // Default enumeration
	m_xOrigin_Exist = false;
	m_yOrigin = Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::UninitializedEnumeration;
	m_yOrigin_Default = Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::bottom; // Default enumeration
	m_yOrigin_Exist = false;
	m_zOrigin = Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::UninitializedEnumeration;
	m_zOrigin_Default = Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::back; // Default enumeration
	m_zOrigin_Exist = false;
	m_timeOrigin = Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::UninitializedEnumeration;
	m_timeOrigin_Default = Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::past; // Default enumeration
	m_timeOrigin_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtMyPropInit.h

}

void Mp7JrsSignalPlaneOriginType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtMyPropCleanup.h


}

void Mp7JrsSignalPlaneOriginType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SignalPlaneOriginTypePtr, since we
	// might need GetBase(), which isn't defined in ISignalPlaneOriginType
	const Dc1Ptr< Mp7JrsSignalPlaneOriginType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExistxOrigin())
	{
		this->SetxOrigin(tmp->GetxOrigin());
	}
	else
	{
		InvalidatexOrigin();
	}
	}
	{
	if (tmp->ExistyOrigin())
	{
		this->SetyOrigin(tmp->GetyOrigin());
	}
	else
	{
		InvalidateyOrigin();
	}
	}
	{
	if (tmp->ExistzOrigin())
	{
		this->SetzOrigin(tmp->GetzOrigin());
	}
	else
	{
		InvalidatezOrigin();
	}
	}
	{
	if (tmp->ExisttimeOrigin())
	{
		this->SettimeOrigin(tmp->GettimeOrigin());
	}
	else
	{
		InvalidatetimeOrigin();
	}
	}
}

Dc1NodePtr Mp7JrsSignalPlaneOriginType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSignalPlaneOriginType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::Enumeration Mp7JrsSignalPlaneOriginType::GetxOrigin() const
{
	if (this->ExistxOrigin()) {
		return m_xOrigin;
	} else {
		return m_xOrigin_Default;
	}
}

bool Mp7JrsSignalPlaneOriginType::ExistxOrigin() const
{
	return m_xOrigin_Exist;
}
void Mp7JrsSignalPlaneOriginType::SetxOrigin(Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneOriginType::SetxOrigin().");
	}
	m_xOrigin = item;
	m_xOrigin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneOriginType::InvalidatexOrigin()
{
	m_xOrigin_Exist = false;
}
Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::Enumeration Mp7JrsSignalPlaneOriginType::GetyOrigin() const
{
	if (this->ExistyOrigin()) {
		return m_yOrigin;
	} else {
		return m_yOrigin_Default;
	}
}

bool Mp7JrsSignalPlaneOriginType::ExistyOrigin() const
{
	return m_yOrigin_Exist;
}
void Mp7JrsSignalPlaneOriginType::SetyOrigin(Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneOriginType::SetyOrigin().");
	}
	m_yOrigin = item;
	m_yOrigin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneOriginType::InvalidateyOrigin()
{
	m_yOrigin_Exist = false;
}
Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::Enumeration Mp7JrsSignalPlaneOriginType::GetzOrigin() const
{
	if (this->ExistzOrigin()) {
		return m_zOrigin;
	} else {
		return m_zOrigin_Default;
	}
}

bool Mp7JrsSignalPlaneOriginType::ExistzOrigin() const
{
	return m_zOrigin_Exist;
}
void Mp7JrsSignalPlaneOriginType::SetzOrigin(Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneOriginType::SetzOrigin().");
	}
	m_zOrigin = item;
	m_zOrigin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneOriginType::InvalidatezOrigin()
{
	m_zOrigin_Exist = false;
}
Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::Enumeration Mp7JrsSignalPlaneOriginType::GettimeOrigin() const
{
	if (this->ExisttimeOrigin()) {
		return m_timeOrigin;
	} else {
		return m_timeOrigin_Default;
	}
}

bool Mp7JrsSignalPlaneOriginType::ExisttimeOrigin() const
{
	return m_timeOrigin_Exist;
}
void Mp7JrsSignalPlaneOriginType::SettimeOrigin(Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneOriginType::SettimeOrigin().");
	}
	m_timeOrigin = item;
	m_timeOrigin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneOriginType::InvalidatetimeOrigin()
{
	m_timeOrigin_Exist = false;
}

Dc1NodeEnum Mp7JrsSignalPlaneOriginType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsSignalPlaneOriginType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSignalPlaneOriginType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSignalPlaneOriginType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SignalPlaneOriginType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_xOrigin_Exist)
	{
	// Enumeration
	if(m_xOrigin != Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::ToText(m_xOrigin);
		element->setAttributeNS(X(""), X("xOrigin"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_yOrigin_Exist)
	{
	// Enumeration
	if(m_yOrigin != Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::ToText(m_yOrigin);
		element->setAttributeNS(X(""), X("yOrigin"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_zOrigin_Exist)
	{
	// Enumeration
	if(m_zOrigin != Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::ToText(m_zOrigin);
		element->setAttributeNS(X(""), X("zOrigin"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_timeOrigin_Exist)
	{
	// Enumeration
	if(m_timeOrigin != Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::ToText(m_timeOrigin);
		element->setAttributeNS(X(""), X("timeOrigin"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSignalPlaneOriginType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xOrigin")))
	{
		this->SetxOrigin(Mp7JrsSignalPlaneOriginType_xOrigin_LocalType::Parse(parent->getAttribute(X("xOrigin"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yOrigin")))
	{
		this->SetyOrigin(Mp7JrsSignalPlaneOriginType_yOrigin_LocalType::Parse(parent->getAttribute(X("yOrigin"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("zOrigin")))
	{
		this->SetzOrigin(Mp7JrsSignalPlaneOriginType_zOrigin_LocalType::Parse(parent->getAttribute(X("zOrigin"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("timeOrigin")))
	{
		this->SettimeOrigin(Mp7JrsSignalPlaneOriginType_timeOrigin_LocalType::Parse(parent->getAttribute(X("timeOrigin"))));
		* current = parent;
	}

// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSignalPlaneOriginType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsSignalPlaneOriginType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSignalPlaneOriginType_ExtMethodImpl.h


