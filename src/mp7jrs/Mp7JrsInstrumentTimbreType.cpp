
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtImplInclude.h


#include "Mp7JrsAudioDSType.h"
#include "Mp7JrsLogAttackTimeType.h"
#include "Mp7JrsHarmonicSpectralCentroidType.h"
#include "Mp7JrsHarmonicSpectralDeviationType.h"
#include "Mp7JrsHarmonicSpectralSpreadType.h"
#include "Mp7JrsHarmonicSpectralVariationType.h"
#include "Mp7JrsSpectralCentroidType.h"
#include "Mp7JrsTemporalCentroidType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsInstrumentTimbreType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsInstrumentTimbreType::IMp7JrsInstrumentTimbreType()
{

// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtPropInit.h

}

IMp7JrsInstrumentTimbreType::~IMp7JrsInstrumentTimbreType()
{
// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtPropCleanup.h

}

Mp7JrsInstrumentTimbreType::Mp7JrsInstrumentTimbreType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInstrumentTimbreType::~Mp7JrsInstrumentTimbreType()
{
	Cleanup();
}

void Mp7JrsInstrumentTimbreType::Init()
{
	// Init base
	m_Base = CreateAudioDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_LogAttackTime = Mp7JrsLogAttackTimePtr(); // Class
	m_LogAttackTime_Exist = false;
	m_HarmonicSpectralCentroid = Mp7JrsHarmonicSpectralCentroidPtr(); // Class
	m_HarmonicSpectralCentroid_Exist = false;
	m_HarmonicSpectralDeviation = Mp7JrsHarmonicSpectralDeviationPtr(); // Class
	m_HarmonicSpectralDeviation_Exist = false;
	m_HarmonicSpectralSpread = Mp7JrsHarmonicSpectralSpreadPtr(); // Class
	m_HarmonicSpectralSpread_Exist = false;
	m_HarmonicSpectralVariation = Mp7JrsHarmonicSpectralVariationPtr(); // Class
	m_HarmonicSpectralVariation_Exist = false;
	m_SpectralCentroid = Mp7JrsSpectralCentroidPtr(); // Class
	m_SpectralCentroid_Exist = false;
	m_TemporalCentroid = Mp7JrsTemporalCentroidPtr(); // Class
	m_TemporalCentroid_Exist = false;


// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtMyPropInit.h

}

void Mp7JrsInstrumentTimbreType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_LogAttackTime);
	// Dc1Factory::DeleteObject(m_HarmonicSpectralCentroid);
	// Dc1Factory::DeleteObject(m_HarmonicSpectralDeviation);
	// Dc1Factory::DeleteObject(m_HarmonicSpectralSpread);
	// Dc1Factory::DeleteObject(m_HarmonicSpectralVariation);
	// Dc1Factory::DeleteObject(m_SpectralCentroid);
	// Dc1Factory::DeleteObject(m_TemporalCentroid);
}

void Mp7JrsInstrumentTimbreType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InstrumentTimbreTypePtr, since we
	// might need GetBase(), which isn't defined in IInstrumentTimbreType
	const Dc1Ptr< Mp7JrsInstrumentTimbreType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsInstrumentTimbreType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidLogAttackTime())
	{
		// Dc1Factory::DeleteObject(m_LogAttackTime);
		this->SetLogAttackTime(Dc1Factory::CloneObject(tmp->GetLogAttackTime()));
	}
	else
	{
		InvalidateLogAttackTime();
	}
	if (tmp->IsValidHarmonicSpectralCentroid())
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralCentroid);
		this->SetHarmonicSpectralCentroid(Dc1Factory::CloneObject(tmp->GetHarmonicSpectralCentroid()));
	}
	else
	{
		InvalidateHarmonicSpectralCentroid();
	}
	if (tmp->IsValidHarmonicSpectralDeviation())
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralDeviation);
		this->SetHarmonicSpectralDeviation(Dc1Factory::CloneObject(tmp->GetHarmonicSpectralDeviation()));
	}
	else
	{
		InvalidateHarmonicSpectralDeviation();
	}
	if (tmp->IsValidHarmonicSpectralSpread())
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralSpread);
		this->SetHarmonicSpectralSpread(Dc1Factory::CloneObject(tmp->GetHarmonicSpectralSpread()));
	}
	else
	{
		InvalidateHarmonicSpectralSpread();
	}
	if (tmp->IsValidHarmonicSpectralVariation())
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralVariation);
		this->SetHarmonicSpectralVariation(Dc1Factory::CloneObject(tmp->GetHarmonicSpectralVariation()));
	}
	else
	{
		InvalidateHarmonicSpectralVariation();
	}
	if (tmp->IsValidSpectralCentroid())
	{
		// Dc1Factory::DeleteObject(m_SpectralCentroid);
		this->SetSpectralCentroid(Dc1Factory::CloneObject(tmp->GetSpectralCentroid()));
	}
	else
	{
		InvalidateSpectralCentroid();
	}
	if (tmp->IsValidTemporalCentroid())
	{
		// Dc1Factory::DeleteObject(m_TemporalCentroid);
		this->SetTemporalCentroid(Dc1Factory::CloneObject(tmp->GetTemporalCentroid()));
	}
	else
	{
		InvalidateTemporalCentroid();
	}
}

Dc1NodePtr Mp7JrsInstrumentTimbreType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsInstrumentTimbreType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDSType > Mp7JrsInstrumentTimbreType::GetBase() const
{
	return m_Base;
}

Mp7JrsLogAttackTimePtr Mp7JrsInstrumentTimbreType::GetLogAttackTime() const
{
		return m_LogAttackTime;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidLogAttackTime() const
{
	return m_LogAttackTime_Exist;
}

Mp7JrsHarmonicSpectralCentroidPtr Mp7JrsInstrumentTimbreType::GetHarmonicSpectralCentroid() const
{
		return m_HarmonicSpectralCentroid;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidHarmonicSpectralCentroid() const
{
	return m_HarmonicSpectralCentroid_Exist;
}

Mp7JrsHarmonicSpectralDeviationPtr Mp7JrsInstrumentTimbreType::GetHarmonicSpectralDeviation() const
{
		return m_HarmonicSpectralDeviation;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidHarmonicSpectralDeviation() const
{
	return m_HarmonicSpectralDeviation_Exist;
}

Mp7JrsHarmonicSpectralSpreadPtr Mp7JrsInstrumentTimbreType::GetHarmonicSpectralSpread() const
{
		return m_HarmonicSpectralSpread;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidHarmonicSpectralSpread() const
{
	return m_HarmonicSpectralSpread_Exist;
}

Mp7JrsHarmonicSpectralVariationPtr Mp7JrsInstrumentTimbreType::GetHarmonicSpectralVariation() const
{
		return m_HarmonicSpectralVariation;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidHarmonicSpectralVariation() const
{
	return m_HarmonicSpectralVariation_Exist;
}

Mp7JrsSpectralCentroidPtr Mp7JrsInstrumentTimbreType::GetSpectralCentroid() const
{
		return m_SpectralCentroid;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidSpectralCentroid() const
{
	return m_SpectralCentroid_Exist;
}

Mp7JrsTemporalCentroidPtr Mp7JrsInstrumentTimbreType::GetTemporalCentroid() const
{
		return m_TemporalCentroid;
}

// Element is optional
bool Mp7JrsInstrumentTimbreType::IsValidTemporalCentroid() const
{
	return m_TemporalCentroid_Exist;
}

void Mp7JrsInstrumentTimbreType::SetLogAttackTime(const Mp7JrsLogAttackTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetLogAttackTime().");
	}
	if (m_LogAttackTime != item || m_LogAttackTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_LogAttackTime);
		m_LogAttackTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LogAttackTime) m_LogAttackTime->SetParent(m_myself.getPointer());
		if (m_LogAttackTime && m_LogAttackTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogAttackTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LogAttackTime->UseTypeAttribute = true;
		}
		m_LogAttackTime_Exist = true;
		if(m_LogAttackTime != Mp7JrsLogAttackTimePtr())
		{
			m_LogAttackTime->SetContentName(XMLString::transcode("LogAttackTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateLogAttackTime()
{
	m_LogAttackTime_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetHarmonicSpectralCentroid(const Mp7JrsHarmonicSpectralCentroidPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetHarmonicSpectralCentroid().");
	}
	if (m_HarmonicSpectralCentroid != item || m_HarmonicSpectralCentroid_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralCentroid);
		m_HarmonicSpectralCentroid = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HarmonicSpectralCentroid) m_HarmonicSpectralCentroid->SetParent(m_myself.getPointer());
		if (m_HarmonicSpectralCentroid && m_HarmonicSpectralCentroid->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralCentroidType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HarmonicSpectralCentroid->UseTypeAttribute = true;
		}
		m_HarmonicSpectralCentroid_Exist = true;
		if(m_HarmonicSpectralCentroid != Mp7JrsHarmonicSpectralCentroidPtr())
		{
			m_HarmonicSpectralCentroid->SetContentName(XMLString::transcode("HarmonicSpectralCentroid"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateHarmonicSpectralCentroid()
{
	m_HarmonicSpectralCentroid_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetHarmonicSpectralDeviation(const Mp7JrsHarmonicSpectralDeviationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetHarmonicSpectralDeviation().");
	}
	if (m_HarmonicSpectralDeviation != item || m_HarmonicSpectralDeviation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralDeviation);
		m_HarmonicSpectralDeviation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HarmonicSpectralDeviation) m_HarmonicSpectralDeviation->SetParent(m_myself.getPointer());
		if (m_HarmonicSpectralDeviation && m_HarmonicSpectralDeviation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralDeviationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HarmonicSpectralDeviation->UseTypeAttribute = true;
		}
		m_HarmonicSpectralDeviation_Exist = true;
		if(m_HarmonicSpectralDeviation != Mp7JrsHarmonicSpectralDeviationPtr())
		{
			m_HarmonicSpectralDeviation->SetContentName(XMLString::transcode("HarmonicSpectralDeviation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateHarmonicSpectralDeviation()
{
	m_HarmonicSpectralDeviation_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetHarmonicSpectralSpread(const Mp7JrsHarmonicSpectralSpreadPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetHarmonicSpectralSpread().");
	}
	if (m_HarmonicSpectralSpread != item || m_HarmonicSpectralSpread_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralSpread);
		m_HarmonicSpectralSpread = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HarmonicSpectralSpread) m_HarmonicSpectralSpread->SetParent(m_myself.getPointer());
		if (m_HarmonicSpectralSpread && m_HarmonicSpectralSpread->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralSpreadType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HarmonicSpectralSpread->UseTypeAttribute = true;
		}
		m_HarmonicSpectralSpread_Exist = true;
		if(m_HarmonicSpectralSpread != Mp7JrsHarmonicSpectralSpreadPtr())
		{
			m_HarmonicSpectralSpread->SetContentName(XMLString::transcode("HarmonicSpectralSpread"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateHarmonicSpectralSpread()
{
	m_HarmonicSpectralSpread_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetHarmonicSpectralVariation(const Mp7JrsHarmonicSpectralVariationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetHarmonicSpectralVariation().");
	}
	if (m_HarmonicSpectralVariation != item || m_HarmonicSpectralVariation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HarmonicSpectralVariation);
		m_HarmonicSpectralVariation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HarmonicSpectralVariation) m_HarmonicSpectralVariation->SetParent(m_myself.getPointer());
		if (m_HarmonicSpectralVariation && m_HarmonicSpectralVariation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralVariationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HarmonicSpectralVariation->UseTypeAttribute = true;
		}
		m_HarmonicSpectralVariation_Exist = true;
		if(m_HarmonicSpectralVariation != Mp7JrsHarmonicSpectralVariationPtr())
		{
			m_HarmonicSpectralVariation->SetContentName(XMLString::transcode("HarmonicSpectralVariation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateHarmonicSpectralVariation()
{
	m_HarmonicSpectralVariation_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetSpectralCentroid(const Mp7JrsSpectralCentroidPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetSpectralCentroid().");
	}
	if (m_SpectralCentroid != item || m_SpectralCentroid_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SpectralCentroid);
		m_SpectralCentroid = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpectralCentroid) m_SpectralCentroid->SetParent(m_myself.getPointer());
		if (m_SpectralCentroid && m_SpectralCentroid->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpectralCentroidType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpectralCentroid->UseTypeAttribute = true;
		}
		m_SpectralCentroid_Exist = true;
		if(m_SpectralCentroid != Mp7JrsSpectralCentroidPtr())
		{
			m_SpectralCentroid->SetContentName(XMLString::transcode("SpectralCentroid"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateSpectralCentroid()
{
	m_SpectralCentroid_Exist = false;
}
void Mp7JrsInstrumentTimbreType::SetTemporalCentroid(const Mp7JrsTemporalCentroidPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetTemporalCentroid().");
	}
	if (m_TemporalCentroid != item || m_TemporalCentroid_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TemporalCentroid);
		m_TemporalCentroid = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalCentroid) m_TemporalCentroid->SetParent(m_myself.getPointer());
		if (m_TemporalCentroid && m_TemporalCentroid->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalCentroidType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemporalCentroid->UseTypeAttribute = true;
		}
		m_TemporalCentroid_Exist = true;
		if(m_TemporalCentroid != Mp7JrsTemporalCentroidPtr())
		{
			m_TemporalCentroid->SetContentName(XMLString::transcode("TemporalCentroid"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidateTemporalCentroid()
{
	m_TemporalCentroid_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsInstrumentTimbreType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsInstrumentTimbreType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsInstrumentTimbreType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsInstrumentTimbreType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsInstrumentTimbreType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsInstrumentTimbreType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsInstrumentTimbreType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsInstrumentTimbreType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsInstrumentTimbreType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsInstrumentTimbreType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsInstrumentTimbreType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsInstrumentTimbreType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsInstrumentTimbreType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsInstrumentTimbreType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsInstrumentTimbreType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsInstrumentTimbreType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsInstrumentTimbreType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsInstrumentTimbreType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInstrumentTimbreType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsInstrumentTimbreType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsInstrumentTimbreType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInstrumentTimbreType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsInstrumentTimbreType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("LogAttackTime")) == 0)
	{
		// LogAttackTime is simple element LogAttackTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLogAttackTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LogAttackTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsLogAttackTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLogAttackTime(p, client);
					if((p = GetLogAttackTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HarmonicSpectralCentroid")) == 0)
	{
		// HarmonicSpectralCentroid is simple element HarmonicSpectralCentroidType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHarmonicSpectralCentroid()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralCentroidType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHarmonicSpectralCentroidPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHarmonicSpectralCentroid(p, client);
					if((p = GetHarmonicSpectralCentroid()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HarmonicSpectralDeviation")) == 0)
	{
		// HarmonicSpectralDeviation is simple element HarmonicSpectralDeviationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHarmonicSpectralDeviation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralDeviationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHarmonicSpectralDeviationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHarmonicSpectralDeviation(p, client);
					if((p = GetHarmonicSpectralDeviation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HarmonicSpectralSpread")) == 0)
	{
		// HarmonicSpectralSpread is simple element HarmonicSpectralSpreadType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHarmonicSpectralSpread()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralSpreadType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHarmonicSpectralSpreadPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHarmonicSpectralSpread(p, client);
					if((p = GetHarmonicSpectralSpread()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HarmonicSpectralVariation")) == 0)
	{
		// HarmonicSpectralVariation is simple element HarmonicSpectralVariationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHarmonicSpectralVariation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HarmonicSpectralVariationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHarmonicSpectralVariationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHarmonicSpectralVariation(p, client);
					if((p = GetHarmonicSpectralVariation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpectralCentroid")) == 0)
	{
		// SpectralCentroid is simple element SpectralCentroidType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpectralCentroid()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpectralCentroidType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpectralCentroidPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpectralCentroid(p, client);
					if((p = GetSpectralCentroid()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TemporalCentroid")) == 0)
	{
		// TemporalCentroid is simple element TemporalCentroidType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTemporalCentroid()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalCentroidType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalCentroidPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTemporalCentroid(p, client);
					if((p = GetTemporalCentroid()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for InstrumentTimbreType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "InstrumentTimbreType");
	}
	return result;
}

XMLCh * Mp7JrsInstrumentTimbreType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsInstrumentTimbreType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsInstrumentTimbreType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InstrumentTimbreType"));
	// Element serialization:
	// Class
	
	if (m_LogAttackTime != Mp7JrsLogAttackTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LogAttackTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LogAttackTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LogAttackTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_HarmonicSpectralCentroid != Mp7JrsHarmonicSpectralCentroidPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HarmonicSpectralCentroid->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HarmonicSpectralCentroid"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HarmonicSpectralCentroid->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_HarmonicSpectralDeviation != Mp7JrsHarmonicSpectralDeviationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HarmonicSpectralDeviation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HarmonicSpectralDeviation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HarmonicSpectralDeviation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_HarmonicSpectralSpread != Mp7JrsHarmonicSpectralSpreadPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HarmonicSpectralSpread->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HarmonicSpectralSpread"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HarmonicSpectralSpread->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_HarmonicSpectralVariation != Mp7JrsHarmonicSpectralVariationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HarmonicSpectralVariation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HarmonicSpectralVariation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HarmonicSpectralVariation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpectralCentroid != Mp7JrsSpectralCentroidPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpectralCentroid->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpectralCentroid"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpectralCentroid->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TemporalCentroid != Mp7JrsTemporalCentroidPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemporalCentroid->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemporalCentroid"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemporalCentroid->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsInstrumentTimbreType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LogAttackTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LogAttackTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateLogAttackTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLogAttackTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HarmonicSpectralCentroid"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HarmonicSpectralCentroid")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateHarmonicSpectralCentroidType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHarmonicSpectralCentroid(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HarmonicSpectralDeviation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HarmonicSpectralDeviation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateHarmonicSpectralDeviationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHarmonicSpectralDeviation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HarmonicSpectralSpread"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HarmonicSpectralSpread")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateHarmonicSpectralSpreadType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHarmonicSpectralSpread(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HarmonicSpectralVariation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HarmonicSpectralVariation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateHarmonicSpectralVariationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHarmonicSpectralVariation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpectralCentroid"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpectralCentroid")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpectralCentroidType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpectralCentroid(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemporalCentroid"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemporalCentroid")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalCentroidType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemporalCentroid(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInstrumentTimbreType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("LogAttackTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateLogAttackTimeType; // FTT, check this
	}
	this->SetLogAttackTime(child);
  }
  if (XMLString::compareString(elementname, X("HarmonicSpectralCentroid")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateHarmonicSpectralCentroidType; // FTT, check this
	}
	this->SetHarmonicSpectralCentroid(child);
  }
  if (XMLString::compareString(elementname, X("HarmonicSpectralDeviation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateHarmonicSpectralDeviationType; // FTT, check this
	}
	this->SetHarmonicSpectralDeviation(child);
  }
  if (XMLString::compareString(elementname, X("HarmonicSpectralSpread")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateHarmonicSpectralSpreadType; // FTT, check this
	}
	this->SetHarmonicSpectralSpread(child);
  }
  if (XMLString::compareString(elementname, X("HarmonicSpectralVariation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateHarmonicSpectralVariationType; // FTT, check this
	}
	this->SetHarmonicSpectralVariation(child);
  }
  if (XMLString::compareString(elementname, X("SpectralCentroid")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpectralCentroidType; // FTT, check this
	}
	this->SetSpectralCentroid(child);
  }
  if (XMLString::compareString(elementname, X("TemporalCentroid")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalCentroidType; // FTT, check this
	}
	this->SetTemporalCentroid(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsInstrumentTimbreType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLogAttackTime() != Dc1NodePtr())
		result.Insert(GetLogAttackTime());
	if (GetHarmonicSpectralCentroid() != Dc1NodePtr())
		result.Insert(GetHarmonicSpectralCentroid());
	if (GetHarmonicSpectralDeviation() != Dc1NodePtr())
		result.Insert(GetHarmonicSpectralDeviation());
	if (GetHarmonicSpectralSpread() != Dc1NodePtr())
		result.Insert(GetHarmonicSpectralSpread());
	if (GetHarmonicSpectralVariation() != Dc1NodePtr())
		result.Insert(GetHarmonicSpectralVariation());
	if (GetSpectralCentroid() != Dc1NodePtr())
		result.Insert(GetSpectralCentroid());
	if (GetTemporalCentroid() != Dc1NodePtr())
		result.Insert(GetTemporalCentroid());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInstrumentTimbreType_ExtMethodImpl.h


