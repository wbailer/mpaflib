
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsImageSignatureType_GlobalSignatureA_LocalType.h"
#include "Mp7JrsImageSignatureType_GlobalSignatureB_LocalType.h"
#include "Mp7JrsImageSignatureType_LocalSignature_LocalType.h"
#include "Mp7JrsImageSignatureType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsImageSignatureType::IMp7JrsImageSignatureType()
{

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtPropInit.h

}

IMp7JrsImageSignatureType::~IMp7JrsImageSignatureType()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtPropCleanup.h

}

Mp7JrsImageSignatureType::Mp7JrsImageSignatureType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsImageSignatureType::~Mp7JrsImageSignatureType()
{
	Cleanup();
}

void Mp7JrsImageSignatureType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_GlobalSignatureA = Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr(); // Class
	m_GlobalSignatureB = Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr(); // Class
	m_LocalSignature = Mp7JrsImageSignatureType_LocalSignature_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtMyPropInit.h

}

void Mp7JrsImageSignatureType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_GlobalSignatureA);
	// Dc1Factory::DeleteObject(m_GlobalSignatureB);
	// Dc1Factory::DeleteObject(m_LocalSignature);
}

void Mp7JrsImageSignatureType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ImageSignatureTypePtr, since we
	// might need GetBase(), which isn't defined in IImageSignatureType
	const Dc1Ptr< Mp7JrsImageSignatureType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsImageSignatureType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_GlobalSignatureA);
		this->SetGlobalSignatureA(Dc1Factory::CloneObject(tmp->GetGlobalSignatureA()));
		// Dc1Factory::DeleteObject(m_GlobalSignatureB);
		this->SetGlobalSignatureB(Dc1Factory::CloneObject(tmp->GetGlobalSignatureB()));
		// Dc1Factory::DeleteObject(m_LocalSignature);
		this->SetLocalSignature(Dc1Factory::CloneObject(tmp->GetLocalSignature()));
}

Dc1NodePtr Mp7JrsImageSignatureType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsImageSignatureType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsImageSignatureType::GetBase() const
{
	return m_Base;
}

Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr Mp7JrsImageSignatureType::GetGlobalSignatureA() const
{
		return m_GlobalSignatureA;
}

Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr Mp7JrsImageSignatureType::GetGlobalSignatureB() const
{
		return m_GlobalSignatureB;
}

Mp7JrsImageSignatureType_LocalSignature_LocalPtr Mp7JrsImageSignatureType::GetLocalSignature() const
{
		return m_LocalSignature;
}

void Mp7JrsImageSignatureType::SetGlobalSignatureA(const Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType::SetGlobalSignatureA().");
	}
	if (m_GlobalSignatureA != item)
	{
		// Dc1Factory::DeleteObject(m_GlobalSignatureA);
		m_GlobalSignatureA = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GlobalSignatureA) m_GlobalSignatureA->SetParent(m_myself.getPointer());
		if (m_GlobalSignatureA && m_GlobalSignatureA->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureA_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GlobalSignatureA->UseTypeAttribute = true;
		}
		if(m_GlobalSignatureA != Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr())
		{
			m_GlobalSignatureA->SetContentName(XMLString::transcode("GlobalSignatureA"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType::SetGlobalSignatureB(const Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType::SetGlobalSignatureB().");
	}
	if (m_GlobalSignatureB != item)
	{
		// Dc1Factory::DeleteObject(m_GlobalSignatureB);
		m_GlobalSignatureB = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GlobalSignatureB) m_GlobalSignatureB->SetParent(m_myself.getPointer());
		if (m_GlobalSignatureB && m_GlobalSignatureB->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureB_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GlobalSignatureB->UseTypeAttribute = true;
		}
		if(m_GlobalSignatureB != Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr())
		{
			m_GlobalSignatureB->SetContentName(XMLString::transcode("GlobalSignatureB"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsImageSignatureType::SetLocalSignature(const Mp7JrsImageSignatureType_LocalSignature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsImageSignatureType::SetLocalSignature().");
	}
	if (m_LocalSignature != item)
	{
		// Dc1Factory::DeleteObject(m_LocalSignature);
		m_LocalSignature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LocalSignature) m_LocalSignature->SetParent(m_myself.getPointer());
		if (m_LocalSignature && m_LocalSignature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LocalSignature->UseTypeAttribute = true;
		}
		if(m_LocalSignature != Mp7JrsImageSignatureType_LocalSignature_LocalPtr())
		{
			m_LocalSignature->SetContentName(XMLString::transcode("LocalSignature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsImageSignatureType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("GlobalSignatureA")) == 0)
	{
		// GlobalSignatureA is simple element ImageSignatureType_GlobalSignatureA_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGlobalSignatureA()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureA_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGlobalSignatureA(p, client);
					if((p = GetGlobalSignatureA()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GlobalSignatureB")) == 0)
	{
		// GlobalSignatureB is simple element ImageSignatureType_GlobalSignatureB_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGlobalSignatureB()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_GlobalSignatureB_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGlobalSignatureB(p, client);
					if((p = GetGlobalSignatureB()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LocalSignature")) == 0)
	{
		// LocalSignature is simple element ImageSignatureType_LocalSignature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLocalSignature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageSignatureType_LocalSignature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageSignatureType_LocalSignature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLocalSignature(p, client);
					if((p = GetLocalSignature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ImageSignatureType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ImageSignatureType");
	}
	return result;
}

XMLCh * Mp7JrsImageSignatureType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsImageSignatureType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsImageSignatureType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ImageSignatureType"));
	// Element serialization:
	// Class
	
	if (m_GlobalSignatureA != Mp7JrsImageSignatureType_GlobalSignatureA_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GlobalSignatureA->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GlobalSignatureA"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GlobalSignatureA->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GlobalSignatureB != Mp7JrsImageSignatureType_GlobalSignatureB_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GlobalSignatureB->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GlobalSignatureB"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GlobalSignatureB->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_LocalSignature != Mp7JrsImageSignatureType_LocalSignature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LocalSignature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LocalSignature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LocalSignature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsImageSignatureType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GlobalSignatureA"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GlobalSignatureA")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageSignatureType_GlobalSignatureA_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGlobalSignatureA(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GlobalSignatureB"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GlobalSignatureB")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageSignatureType_GlobalSignatureB_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGlobalSignatureB(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LocalSignature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LocalSignature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageSignatureType_LocalSignature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLocalSignature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsImageSignatureType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("GlobalSignatureA")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageSignatureType_GlobalSignatureA_LocalType; // FTT, check this
	}
	this->SetGlobalSignatureA(child);
  }
  if (XMLString::compareString(elementname, X("GlobalSignatureB")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageSignatureType_GlobalSignatureB_LocalType; // FTT, check this
	}
	this->SetGlobalSignatureB(child);
  }
  if (XMLString::compareString(elementname, X("LocalSignature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageSignatureType_LocalSignature_LocalType; // FTT, check this
	}
	this->SetLocalSignature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsImageSignatureType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetGlobalSignatureA() != Dc1NodePtr())
		result.Insert(GetGlobalSignatureA());
	if (GetGlobalSignatureB() != Dc1NodePtr())
		result.Insert(GetGlobalSignatureB());
	if (GetLocalSignature() != Dc1NodePtr())
		result.Insert(GetLocalSignature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsImageSignatureType_ExtMethodImpl.h


