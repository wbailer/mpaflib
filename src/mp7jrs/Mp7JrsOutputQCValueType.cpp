
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtImplInclude.h


#include "Mp7JrsQCValueType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsQCValueType_track_LocalType.h"
#include "Mp7JrsOutputQCValueType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsOutputQCValueType::IMp7JrsOutputQCValueType()
{

// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtPropInit.h

}

IMp7JrsOutputQCValueType::~IMp7JrsOutputQCValueType()
{
// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtPropCleanup.h

}

Mp7JrsOutputQCValueType::Mp7JrsOutputQCValueType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsOutputQCValueType::~Mp7JrsOutputQCValueType()
{
	Cleanup();
}

void Mp7JrsOutputQCValueType::Init()
{
	// Init base
	m_Base = CreateQCValueType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_annotation = NULL; // String
	m_annotation_Exist = false;
	m_verificationMedia = NULL; // String
	m_verificationMedia_Exist = false;
	m_confidence = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_confidence->SetContent(0.0); // Use Value initialiser (xxx2)
	m_confidence_Exist = false;
	m_severity = 0; // Value
	m_severity_Exist = false;



// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtMyPropInit.h

}

void Mp7JrsOutputQCValueType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_annotation); // String
	XMLString::release(&m_verificationMedia); // String
	// Dc1Factory::DeleteObject(m_confidence);
}

void Mp7JrsOutputQCValueType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use OutputQCValueTypePtr, since we
	// might need GetBase(), which isn't defined in IOutputQCValueType
	const Dc1Ptr< Mp7JrsOutputQCValueType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsQCValuePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsOutputQCValueType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_annotation); // String
	if (tmp->Existannotation())
	{
		this->Setannotation(XMLString::replicate(tmp->Getannotation()));
	}
	else
	{
		Invalidateannotation();
	}
	}
	{
	XMLString::release(&m_verificationMedia); // String
	if (tmp->ExistverificationMedia())
	{
		this->SetverificationMedia(XMLString::replicate(tmp->GetverificationMedia()));
	}
	else
	{
		InvalidateverificationMedia();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_confidence);
	if (tmp->Existconfidence())
	{
		this->Setconfidence(Dc1Factory::CloneObject(tmp->Getconfidence()));
	}
	else
	{
		Invalidateconfidence();
	}
	}
	{
	if (tmp->Existseverity())
	{
		this->Setseverity(tmp->Getseverity());
	}
	else
	{
		Invalidateseverity();
	}
	}
}

Dc1NodePtr Mp7JrsOutputQCValueType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsOutputQCValueType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsQCValueType > Mp7JrsOutputQCValueType::GetBase() const
{
	return m_Base;
}

void Mp7JrsOutputQCValueType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsOutputQCValueType::GetContent() const
{
	return GetBase()->GetContent();
}
XMLCh * Mp7JrsOutputQCValueType::Getannotation() const
{
	return m_annotation;
}

bool Mp7JrsOutputQCValueType::Existannotation() const
{
	return m_annotation_Exist;
}
void Mp7JrsOutputQCValueType::Setannotation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setannotation().");
	}
	m_annotation = item;
	m_annotation_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidateannotation()
{
	m_annotation_Exist = false;
}
XMLCh * Mp7JrsOutputQCValueType::GetverificationMedia() const
{
	return m_verificationMedia;
}

bool Mp7JrsOutputQCValueType::ExistverificationMedia() const
{
	return m_verificationMedia_Exist;
}
void Mp7JrsOutputQCValueType::SetverificationMedia(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::SetverificationMedia().");
	}
	m_verificationMedia = item;
	m_verificationMedia_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::InvalidateverificationMedia()
{
	m_verificationMedia_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsOutputQCValueType::Getconfidence() const
{
	return m_confidence;
}

bool Mp7JrsOutputQCValueType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsOutputQCValueType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setconfidence().");
	}
	m_confidence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_confidence) m_confidence->SetParent(m_myself.getPointer());
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
unsigned Mp7JrsOutputQCValueType::Getseverity() const
{
	return m_severity;
}

bool Mp7JrsOutputQCValueType::Existseverity() const
{
	return m_severity_Exist;
}
void Mp7JrsOutputQCValueType::Setseverity(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setseverity().");
	}
	m_severity = item;
	m_severity_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidateseverity()
{
	m_severity_Exist = false;
}
XMLCh * Mp7JrsOutputQCValueType::Getname() const
{
	return GetBase()->Getname();
}

void Mp7JrsOutputQCValueType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setname().");
	}
	GetBase()->Setname(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsOutputQCValueType::Getunit() const
{
	return GetBase()->Getunit();
}

bool Mp7JrsOutputQCValueType::Existunit() const
{
	return GetBase()->Existunit();
}
void Mp7JrsOutputQCValueType::Setunit(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setunit().");
	}
	GetBase()->Setunit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidateunit()
{
	GetBase()->Invalidateunit();
}
Mp7JrsQCValueType_track_LocalPtr Mp7JrsOutputQCValueType::Gettrack() const
{
	return GetBase()->Gettrack();
}

bool Mp7JrsOutputQCValueType::Existtrack() const
{
	return GetBase()->Existtrack();
}
void Mp7JrsOutputQCValueType::Settrack(const Mp7JrsQCValueType_track_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Settrack().");
	}
	GetBase()->Settrack(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidatetrack()
{
	GetBase()->Invalidatetrack();
}
XMLCh * Mp7JrsOutputQCValueType::GetvalueRange() const
{
	return GetBase()->GetvalueRange();
}

bool Mp7JrsOutputQCValueType::ExistvalueRange() const
{
	return GetBase()->ExistvalueRange();
}
void Mp7JrsOutputQCValueType::SetvalueRange(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::SetvalueRange().");
	}
	GetBase()->SetvalueRange(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::InvalidatevalueRange()
{
	GetBase()->InvalidatevalueRange();
}
XMLCh * Mp7JrsOutputQCValueType::Gettype() const
{
	return GetBase()->Gettype();
}

bool Mp7JrsOutputQCValueType::Existtype() const
{
	return GetBase()->Existtype();
}
void Mp7JrsOutputQCValueType::Settype(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidatetype()
{
	GetBase()->Invalidatetype();
}
XMLCh * Mp7JrsOutputQCValueType::Getrepresentation() const
{
	return GetBase()->Getrepresentation();
}

bool Mp7JrsOutputQCValueType::Existrepresentation() const
{
	return GetBase()->Existrepresentation();
}
void Mp7JrsOutputQCValueType::Setrepresentation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsOutputQCValueType::Setrepresentation().");
	}
	GetBase()->Setrepresentation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsOutputQCValueType::Invalidaterepresentation()
{
	GetBase()->Invalidaterepresentation();
}

Dc1NodeEnum Mp7JrsOutputQCValueType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 10 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("annotation")) == 0)
	{
		// annotation is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("verificationMedia")) == 0)
	{
		// verificationMedia is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("severity")) == 0)
	{
		// severity is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for OutputQCValueType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "OutputQCValueType");
	}
	return result;
}

XMLCh * Mp7JrsOutputQCValueType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsOutputQCValueType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsOutputQCValueType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsOutputQCValueType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsOutputQCValueType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("OutputQCValueType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_annotation_Exist)
	{
	// String
	if(m_annotation != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("annotation"), m_annotation);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_verificationMedia_Exist)
	{
	// String
	if(m_verificationMedia != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("verificationMedia"), m_verificationMedia);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Class
	if (m_confidence != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_confidence->ToText();
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_severity_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_severity);
		element->setAttributeNS(X(""), X("severity"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsOutputQCValueType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("annotation")))
	{
		// Deserialize string type
		this->Setannotation(Dc1Convert::TextToString(parent->getAttribute(X("annotation"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("verificationMedia")))
	{
		// Deserialize string type
		this->SetverificationMedia(Dc1Convert::TextToString(parent->getAttribute(X("verificationMedia"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("confidence")));
		this->Setconfidence(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("severity")))
	{
		// deserialize value type
		this->Setseverity(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("severity"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsQCValueType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsOutputQCValueType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsOutputQCValueType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsOutputQCValueType_ExtMethodImpl.h


