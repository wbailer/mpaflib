
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtImplInclude.h


#include "Mp7JrsAudioVisualSegmentSpatialDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsAudioVisualSegmentType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsAudioVisualSegmentType_LocalType::IMp7JrsAudioVisualSegmentType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtPropInit.h

}

IMp7JrsAudioVisualSegmentType_LocalType::~IMp7JrsAudioVisualSegmentType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioVisualSegmentType_LocalType::Mp7JrsAudioVisualSegmentType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioVisualSegmentType_LocalType::~Mp7JrsAudioVisualSegmentType_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioVisualSegmentType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_SpatialDecomposition = Mp7JrsAudioVisualSegmentSpatialDecompositionPtr(); // Class
	m_TemporalDecomposition = Mp7JrsAudioVisualSegmentTemporalDecompositionPtr(); // Class
	m_SpatioTemporalDecomposition = Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr(); // Class
	m_MediaSourceDecomposition = Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioVisualSegmentType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
}

void Mp7JrsAudioVisualSegmentType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioVisualSegmentType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioVisualSegmentType_LocalType
	const Dc1Ptr< Mp7JrsAudioVisualSegmentType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		this->SetSpatialDecomposition(Dc1Factory::CloneObject(tmp->GetSpatialDecomposition()));
		// Dc1Factory::DeleteObject(m_TemporalDecomposition);
		this->SetTemporalDecomposition(Dc1Factory::CloneObject(tmp->GetTemporalDecomposition()));
		// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
		this->SetSpatioTemporalDecomposition(Dc1Factory::CloneObject(tmp->GetSpatioTemporalDecomposition()));
		// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
		this->SetMediaSourceDecomposition(Dc1Factory::CloneObject(tmp->GetMediaSourceDecomposition()));
}

Dc1NodePtr Mp7JrsAudioVisualSegmentType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAudioVisualSegmentType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsAudioVisualSegmentSpatialDecompositionPtr Mp7JrsAudioVisualSegmentType_LocalType::GetSpatialDecomposition() const
{
		return m_SpatialDecomposition;
}

Mp7JrsAudioVisualSegmentTemporalDecompositionPtr Mp7JrsAudioVisualSegmentType_LocalType::GetTemporalDecomposition() const
{
		return m_TemporalDecomposition;
}

Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr Mp7JrsAudioVisualSegmentType_LocalType::GetSpatioTemporalDecomposition() const
{
		return m_SpatioTemporalDecomposition;
}

Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr Mp7JrsAudioVisualSegmentType_LocalType::GetMediaSourceDecomposition() const
{
		return m_MediaSourceDecomposition;
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioVisualSegmentType_LocalType::SetSpatialDecomposition(const Mp7JrsAudioVisualSegmentSpatialDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualSegmentType_LocalType::SetSpatialDecomposition().");
	}
	if (m_SpatialDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		m_SpatialDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialDecomposition) m_SpatialDecomposition->SetParent(m_myself.getPointer());
		if (m_SpatialDecomposition && m_SpatialDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatialDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialDecomposition->UseTypeAttribute = true;
		}
		if(m_SpatialDecomposition != Mp7JrsAudioVisualSegmentSpatialDecompositionPtr())
		{
			m_SpatialDecomposition->SetContentName(XMLString::transcode("SpatialDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsAudioVisualSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualSegmentType_LocalType::SetTemporalDecomposition(const Mp7JrsAudioVisualSegmentTemporalDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualSegmentType_LocalType::SetTemporalDecomposition().");
	}
	if (m_TemporalDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_TemporalDecomposition);
		m_TemporalDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalDecomposition) m_TemporalDecomposition->SetParent(m_myself.getPointer());
		if (m_TemporalDecomposition && m_TemporalDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentTemporalDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemporalDecomposition->UseTypeAttribute = true;
		}
		if(m_TemporalDecomposition != Mp7JrsAudioVisualSegmentTemporalDecompositionPtr())
		{
			m_TemporalDecomposition->SetContentName(XMLString::transcode("TemporalDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsAudioVisualSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualSegmentType_LocalType::SetSpatioTemporalDecomposition(const Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualSegmentType_LocalType::SetSpatioTemporalDecomposition().");
	}
	if (m_SpatioTemporalDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
		m_SpatioTemporalDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatioTemporalDecomposition) m_SpatioTemporalDecomposition->SetParent(m_myself.getPointer());
		if (m_SpatioTemporalDecomposition && m_SpatioTemporalDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentSpatioTemporalDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatioTemporalDecomposition->UseTypeAttribute = true;
		}
		if(m_SpatioTemporalDecomposition != Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr())
		{
			m_SpatioTemporalDecomposition->SetContentName(XMLString::transcode("SpatioTemporalDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsAudioVisualSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsAudioVisualSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualSegmentType_LocalType::SetMediaSourceDecomposition(const Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualSegmentType_LocalType::SetMediaSourceDecomposition().");
	}
	if (m_MediaSourceDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
		m_MediaSourceDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaSourceDecomposition) m_MediaSourceDecomposition->SetParent(m_myself.getPointer());
		if (m_MediaSourceDecomposition && m_MediaSourceDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualSegmentMediaSourceDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaSourceDecomposition->UseTypeAttribute = true;
		}
		if(m_MediaSourceDecomposition != Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr())
		{
			m_MediaSourceDecomposition->SetContentName(XMLString::transcode("MediaSourceDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsAudioVisualSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsAudioVisualSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: AudioVisualSegmentType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsAudioVisualSegmentType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsAudioVisualSegmentType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioVisualSegmentType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioVisualSegmentType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_SpatialDecomposition != Mp7JrsAudioVisualSegmentSpatialDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TemporalDecomposition != Mp7JrsAudioVisualSegmentTemporalDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemporalDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemporalDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemporalDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatioTemporalDecomposition != Mp7JrsAudioVisualSegmentSpatioTemporalDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatioTemporalDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatioTemporalDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatioTemporalDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaSourceDecomposition != Mp7JrsAudioVisualSegmentMediaSourceDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaSourceDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaSourceDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaSourceDecomposition->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioVisualSegmentType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualSegmentSpatialDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemporalDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualSegmentTemporalDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemporalDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatioTemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatioTemporalDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualSegmentSpatioTemporalDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatioTemporalDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaSourceDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualSegmentMediaSourceDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaSourceDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioVisualSegmentType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("SpatialDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualSegmentSpatialDecompositionType; // FTT, check this
	}
	this->SetSpatialDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("TemporalDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualSegmentTemporalDecompositionType; // FTT, check this
	}
	this->SetTemporalDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("SpatioTemporalDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualSegmentSpatioTemporalDecompositionType; // FTT, check this
	}
	this->SetSpatioTemporalDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("MediaSourceDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualSegmentMediaSourceDecompositionType; // FTT, check this
	}
	this->SetMediaSourceDecomposition(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioVisualSegmentType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSpatialDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatialDecomposition());
	if (GetTemporalDecomposition() != Dc1NodePtr())
		result.Insert(GetTemporalDecomposition());
	if (GetSpatioTemporalDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalDecomposition());
	if (GetMediaSourceDecomposition() != Dc1NodePtr())
		result.Insert(GetMediaSourceDecomposition());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioVisualSegmentType_LocalType_ExtMethodImpl.h


