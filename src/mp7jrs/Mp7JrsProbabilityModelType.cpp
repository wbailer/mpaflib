
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtImplInclude.h


#include "Mp7JrsModelType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsProbabilityModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsProbabilityModelType::IMp7JrsProbabilityModelType()
{

// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtPropInit.h

}

IMp7JrsProbabilityModelType::~IMp7JrsProbabilityModelType()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtPropCleanup.h

}

Mp7JrsProbabilityModelType::Mp7JrsProbabilityModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsProbabilityModelType::~Mp7JrsProbabilityModelType()
{
	Cleanup();
}

void Mp7JrsProbabilityModelType::Init()
{
	// Init base
	m_Base = CreateModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtMyPropInit.h

}

void Mp7JrsProbabilityModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsProbabilityModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ProbabilityModelTypePtr, since we
	// might need GetBase(), which isn't defined in IProbabilityModelType
	const Dc1Ptr< Mp7JrsProbabilityModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsProbabilityModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrsProbabilityModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsProbabilityModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsModelType > Mp7JrsProbabilityModelType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsProbabilityModelType::Getconfidence() const
{
	return GetBase()->Getconfidence();
}

bool Mp7JrsProbabilityModelType::Existconfidence() const
{
	return GetBase()->Existconfidence();
}
void Mp7JrsProbabilityModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::Setconfidence().");
	}
	GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::Invalidateconfidence()
{
	GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsProbabilityModelType::Getreliability() const
{
	return GetBase()->Getreliability();
}

bool Mp7JrsProbabilityModelType::Existreliability() const
{
	return GetBase()->Existreliability();
}
void Mp7JrsProbabilityModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::Setreliability().");
	}
	GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::Invalidatereliability()
{
	GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsProbabilityModelType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsProbabilityModelType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsProbabilityModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsProbabilityModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsProbabilityModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsProbabilityModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsProbabilityModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsProbabilityModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsProbabilityModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsProbabilityModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsProbabilityModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsProbabilityModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsProbabilityModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsProbabilityModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsProbabilityModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsProbabilityModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityModelType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsProbabilityModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ProbabilityModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ProbabilityModelType");
	}
	return result;
}

XMLCh * Mp7JrsProbabilityModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsProbabilityModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsProbabilityModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ProbabilityModelType"));
	// Element serialization:

}

bool Mp7JrsProbabilityModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsProbabilityModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsProbabilityModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsProbabilityModelType_ExtMethodImpl.h


