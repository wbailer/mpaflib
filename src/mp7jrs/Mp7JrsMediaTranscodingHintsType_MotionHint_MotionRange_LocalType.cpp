
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtPropInit.h

}

IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::~IMp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::~Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Init()
{

	// Init attributes
	m_xLeft = 0; // Value
	m_xRight = 0; // Value
	m_yDown = 0; // Value
	m_yUp = 0; // Value



// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaTranscodingHintsType_MotionHint_MotionRange_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaTranscodingHintsType_MotionHint_MotionRange_LocalType
	const Dc1Ptr< Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
		this->SetxLeft(tmp->GetxLeft());
	}
	{
		this->SetxRight(tmp->GetxRight());
	}
	{
		this->SetyDown(tmp->GetyDown());
	}
	{
		this->SetyUp(tmp->GetyUp());
	}
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


unsigned Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetxLeft() const
{
	return m_xLeft;
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetxLeft(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetxLeft().");
	}
	m_xLeft = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetxRight() const
{
	return m_xRight;
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetxRight(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetxRight().");
	}
	m_xRight = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetyDown() const
{
	return m_yDown;
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetyDown(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetyDown().");
	}
	m_yDown = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetyUp() const
{
	return m_yUp;
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetyUp(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::SetyUp().");
	}
	m_yUp = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaTranscodingHintsType_MotionHint_MotionRange_LocalType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_xLeft);
		element->setAttributeNS(X(""), X("xLeft"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_xRight);
		element->setAttributeNS(X(""), X("xRight"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_yDown);
		element->setAttributeNS(X(""), X("yDown"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_yUp);
		element->setAttributeNS(X(""), X("yUp"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xLeft")))
	{
		// deserialize value type
		this->SetxLeft(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("xLeft"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xRight")))
	{
		// deserialize value type
		this->SetxRight(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("xRight"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yDown")))
	{
		// deserialize value type
		this->SetyDown(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("yDown"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yUp")))
	{
		// deserialize value type
		this->SetyUp(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("yUp"))));
		* current = parent;
	}

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType_ExtMethodImpl.h


