
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsEventType_ExtImplInclude.h


#include "Mp7JrsSemanticBaseType.h"
#include "Mp7JrsEventType_CollectionType.h"
#include "Mp7JrsEventType_SemanticPlace_CollectionType.h"
#include "Mp7JrsEventType_SemanticTime_CollectionType.h"
#include "Mp7JrsAbstractionLevelType.h"
#include "Mp7JrsSemanticBaseType_Label_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsSemanticBaseType_Property_CollectionType.h"
#include "Mp7JrsSemanticBaseType_MediaOccurrence_CollectionType.h"
#include "Mp7JrsSemanticBaseType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsEventType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Label
#include "Mp7JrsSemanticBaseType_MediaOccurrence_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaOccurrence
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsEventType_LocalType.h" // Choice collection Event
#include "Mp7JrsEventType.h" // Choice collection element Event
#include "Mp7JrsReferenceType.h" // Choice collection element EventRef
#include "Mp7JrsSemanticPlaceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SemanticPlace
#include "Mp7JrsSemanticTimeType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SemanticTime

#include <assert.h>
IMp7JrsEventType::IMp7JrsEventType()
{

// no includefile for extension defined 
// file Mp7JrsEventType_ExtPropInit.h

}

IMp7JrsEventType::~IMp7JrsEventType()
{
// no includefile for extension defined 
// file Mp7JrsEventType_ExtPropCleanup.h

}

Mp7JrsEventType::Mp7JrsEventType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsEventType::~Mp7JrsEventType()
{
	Cleanup();
}

void Mp7JrsEventType::Init()
{
	// Init base
	m_Base = CreateSemanticBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_EventType_LocalType = Mp7JrsEventType_CollectionPtr(); // Collection
	m_SemanticPlace = Mp7JrsEventType_SemanticPlace_CollectionPtr(); // Collection
	m_SemanticTime = Mp7JrsEventType_SemanticTime_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsEventType_ExtMyPropInit.h

}

void Mp7JrsEventType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsEventType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_EventType_LocalType);
	// Dc1Factory::DeleteObject(m_SemanticPlace);
	// Dc1Factory::DeleteObject(m_SemanticTime);
}

void Mp7JrsEventType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use EventTypePtr, since we
	// might need GetBase(), which isn't defined in IEventType
	const Dc1Ptr< Mp7JrsEventType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSemanticBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsEventType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_EventType_LocalType);
		this->SetEventType_LocalType(Dc1Factory::CloneObject(tmp->GetEventType_LocalType()));
		// Dc1Factory::DeleteObject(m_SemanticPlace);
		this->SetSemanticPlace(Dc1Factory::CloneObject(tmp->GetSemanticPlace()));
		// Dc1Factory::DeleteObject(m_SemanticTime);
		this->SetSemanticTime(Dc1Factory::CloneObject(tmp->GetSemanticTime()));
}

Dc1NodePtr Mp7JrsEventType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsEventType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSemanticBaseType > Mp7JrsEventType::GetBase() const
{
	return m_Base;
}

Mp7JrsEventType_CollectionPtr Mp7JrsEventType::GetEventType_LocalType() const
{
		return m_EventType_LocalType;
}

Mp7JrsEventType_SemanticPlace_CollectionPtr Mp7JrsEventType::GetSemanticPlace() const
{
		return m_SemanticPlace;
}

Mp7JrsEventType_SemanticTime_CollectionPtr Mp7JrsEventType::GetSemanticTime() const
{
		return m_SemanticTime;
}

void Mp7JrsEventType::SetEventType_LocalType(const Mp7JrsEventType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetEventType_LocalType().");
	}
	if (m_EventType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_EventType_LocalType);
		m_EventType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EventType_LocalType) m_EventType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::SetSemanticPlace(const Mp7JrsEventType_SemanticPlace_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetSemanticPlace().");
	}
	if (m_SemanticPlace != item)
	{
		// Dc1Factory::DeleteObject(m_SemanticPlace);
		m_SemanticPlace = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SemanticPlace) m_SemanticPlace->SetParent(m_myself.getPointer());
		if(m_SemanticPlace != Mp7JrsEventType_SemanticPlace_CollectionPtr())
		{
			m_SemanticPlace->SetContentName(XMLString::transcode("SemanticPlace"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::SetSemanticTime(const Mp7JrsEventType_SemanticTime_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetSemanticTime().");
	}
	if (m_SemanticTime != item)
	{
		// Dc1Factory::DeleteObject(m_SemanticTime);
		m_SemanticTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SemanticTime) m_SemanticTime->SetParent(m_myself.getPointer());
		if(m_SemanticTime != Mp7JrsEventType_SemanticTime_CollectionPtr())
		{
			m_SemanticTime->SetContentName(XMLString::transcode("SemanticTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsAbstractionLevelPtr Mp7JrsEventType::GetAbstractionLevel() const
{
	return GetBase()->GetAbstractionLevel();
}

// Element is optional
bool Mp7JrsEventType::IsValidAbstractionLevel() const
{
	return GetBase()->IsValidAbstractionLevel();
}

Mp7JrsSemanticBaseType_Label_CollectionPtr Mp7JrsEventType::GetLabel() const
{
	return GetBase()->GetLabel();
}

Mp7JrsTextAnnotationPtr Mp7JrsEventType::GetDefinition() const
{
	return GetBase()->GetDefinition();
}

// Element is optional
bool Mp7JrsEventType::IsValidDefinition() const
{
	return GetBase()->IsValidDefinition();
}

Mp7JrsSemanticBaseType_Property_CollectionPtr Mp7JrsEventType::GetProperty() const
{
	return GetBase()->GetProperty();
}

Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr Mp7JrsEventType::GetMediaOccurrence() const
{
	return GetBase()->GetMediaOccurrence();
}

Mp7JrsSemanticBaseType_Relation_CollectionPtr Mp7JrsEventType::GetRelation() const
{
	return GetBase()->GetRelation();
}

void Mp7JrsEventType::SetAbstractionLevel(const Mp7JrsAbstractionLevelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetAbstractionLevel().");
	}
	GetBase()->SetAbstractionLevel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidateAbstractionLevel()
{
	GetBase()->InvalidateAbstractionLevel();
}
void Mp7JrsEventType::SetLabel(const Mp7JrsSemanticBaseType_Label_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetLabel().");
	}
	GetBase()->SetLabel(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::SetDefinition(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetDefinition().");
	}
	GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidateDefinition()
{
	GetBase()->InvalidateDefinition();
}
void Mp7JrsEventType::SetProperty(const Mp7JrsSemanticBaseType_Property_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetProperty().");
	}
	GetBase()->SetProperty(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::SetMediaOccurrence(const Mp7JrsSemanticBaseType_MediaOccurrence_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetMediaOccurrence().");
	}
	GetBase()->SetMediaOccurrence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::SetRelation(const Mp7JrsSemanticBaseType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsEventType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsEventType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsEventType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsEventType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsEventType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsEventType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsEventType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsEventType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsEventType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsEventType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsEventType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsEventType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsEventType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsEventType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsEventType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEventType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsEventType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsEventType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEventType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsEventType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Event")) == 0)
	{
		// Event is contained in itemtype EventType_LocalType
		// in choice collection EventType_CollectionType

		context->Found = true;
		Mp7JrsEventType_CollectionPtr coll = GetEventType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEventType_CollectionType; // FTT, check this
				SetEventType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->GetEvent()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsEventType_LocalPtr item = CreateEventType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EventType")))) != empty)
			{
				// Is type allowed
				Mp7JrsEventPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->SetEvent(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->GetEvent()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EventRef")) == 0)
	{
		// EventRef is contained in itemtype EventType_LocalType
		// in choice collection EventType_CollectionType

		context->Found = true;
		Mp7JrsEventType_CollectionPtr coll = GetEventType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEventType_CollectionType; // FTT, check this
				SetEventType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->GetEventRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsEventType_LocalPtr item = CreateEventType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->SetEventRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsEventType_LocalPtr)coll->elementAt(i))->GetEventRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SemanticPlace")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SemanticPlace is item of type SemanticPlaceType
		// in element collection EventType_SemanticPlace_CollectionType
		
		context->Found = true;
		Mp7JrsEventType_SemanticPlace_CollectionPtr coll = GetSemanticPlace();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEventType_SemanticPlace_CollectionType; // FTT, check this
				SetSemanticPlace(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticPlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SemanticTime")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SemanticTime is item of type SemanticTimeType
		// in element collection EventType_SemanticTime_CollectionType
		
		context->Found = true;
		Mp7JrsEventType_SemanticTime_CollectionPtr coll = GetSemanticTime();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEventType_SemanticTime_CollectionType; // FTT, check this
				SetSemanticTime(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for EventType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "EventType");
	}
	return result;
}

XMLCh * Mp7JrsEventType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsEventType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsEventType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsEventType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("EventType"));
	// Element serialization:
	if (m_EventType_LocalType != Mp7JrsEventType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_EventType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SemanticPlace != Mp7JrsEventType_SemanticPlace_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SemanticPlace->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SemanticTime != Mp7JrsEventType_SemanticTime_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SemanticTime->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsEventType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSemanticBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Event
			Dc1Util::HasNodeName(parent, X("Event"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Event")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:EventRef
			Dc1Util::HasNodeName(parent, X("EventRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:EventRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsEventType_CollectionPtr tmp = CreateEventType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEventType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SemanticPlace"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SemanticPlace")) == 0))
		{
			// Deserialize factory type
			Mp7JrsEventType_SemanticPlace_CollectionPtr tmp = CreateEventType_SemanticPlace_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSemanticPlace(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SemanticTime"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SemanticTime")) == 0))
		{
			// Deserialize factory type
			Mp7JrsEventType_SemanticTime_CollectionPtr tmp = CreateEventType_SemanticTime_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSemanticTime(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsEventType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsEventType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Event"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Event")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("EventRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("EventRef")) == 0)
	)
  {
	Mp7JrsEventType_CollectionPtr tmp = CreateEventType_CollectionType; // FTT, check this
	this->SetEventType_LocalType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SemanticPlace")) == 0))
  {
	Mp7JrsEventType_SemanticPlace_CollectionPtr tmp = CreateEventType_SemanticPlace_CollectionType; // FTT, check this
	this->SetSemanticPlace(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SemanticTime")) == 0))
  {
	Mp7JrsEventType_SemanticTime_CollectionPtr tmp = CreateEventType_SemanticTime_CollectionType; // FTT, check this
	this->SetSemanticTime(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsEventType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEventType_LocalType() != Dc1NodePtr())
		result.Insert(GetEventType_LocalType());
	if (GetSemanticPlace() != Dc1NodePtr())
		result.Insert(GetSemanticPlace());
	if (GetSemanticTime() != Dc1NodePtr())
		result.Insert(GetSemanticTime());
	if (GetAbstractionLevel() != Dc1NodePtr())
		result.Insert(GetAbstractionLevel());
	if (GetLabel() != Dc1NodePtr())
		result.Insert(GetLabel());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetProperty() != Dc1NodePtr())
		result.Insert(GetProperty());
	if (GetMediaOccurrence() != Dc1NodePtr())
		result.Insert(GetMediaOccurrence());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsEventType_ExtMethodImpl.h


