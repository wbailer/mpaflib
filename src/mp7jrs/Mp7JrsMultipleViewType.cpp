
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtImplInclude.h


#include "Mp7JrsMultipleViewType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMultipleViewType_LocalType.h" // Sequence collection Descriptor
#include "Mp7JrsVisualDType.h" // Sequence collection element Descriptor

#include <assert.h>
IMp7JrsMultipleViewType::IMp7JrsMultipleViewType()
{

// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtPropInit.h

}

IMp7JrsMultipleViewType::~IMp7JrsMultipleViewType()
{
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtPropCleanup.h

}

Mp7JrsMultipleViewType::Mp7JrsMultipleViewType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMultipleViewType::~Mp7JrsMultipleViewType()
{
	Cleanup();
}

void Mp7JrsMultipleViewType::Init()
{

	// Init attributes
	m_fixedViewsFlag = false; // Value

	// Init elements (element, union sequence choice all any)
	
	m_MultipleViewType_LocalType = Mp7JrsMultipleViewType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtMyPropInit.h

}

void Mp7JrsMultipleViewType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MultipleViewType_LocalType);
}

void Mp7JrsMultipleViewType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MultipleViewTypePtr, since we
	// might need GetBase(), which isn't defined in IMultipleViewType
	const Dc1Ptr< Mp7JrsMultipleViewType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
		this->SetfixedViewsFlag(tmp->GetfixedViewsFlag());
	}
		// Dc1Factory::DeleteObject(m_MultipleViewType_LocalType);
		this->SetMultipleViewType_LocalType(Dc1Factory::CloneObject(tmp->GetMultipleViewType_LocalType()));
}

Dc1NodePtr Mp7JrsMultipleViewType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMultipleViewType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsMultipleViewType::GetfixedViewsFlag() const
{
	return m_fixedViewsFlag;
}

void Mp7JrsMultipleViewType::SetfixedViewsFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultipleViewType::SetfixedViewsFlag().");
	}
	m_fixedViewsFlag = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMultipleViewType_CollectionPtr Mp7JrsMultipleViewType::GetMultipleViewType_LocalType() const
{
		return m_MultipleViewType_LocalType;
}

void Mp7JrsMultipleViewType::SetMultipleViewType_LocalType(const Mp7JrsMultipleViewType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultipleViewType::SetMultipleViewType_LocalType().");
	}
	if (m_MultipleViewType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_MultipleViewType_LocalType);
		m_MultipleViewType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MultipleViewType_LocalType) m_MultipleViewType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMultipleViewType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("fixedViewsFlag")) == 0)
	{
		// fixedViewsFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// Descriptor is contained in itemtype MultipleViewType_LocalType
		// in sequence collection MultipleViewType_CollectionType

		context->Found = true;
		Mp7JrsMultipleViewType_CollectionPtr coll = GetMultipleViewType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMultipleViewType_CollectionType; // FTT, check this
				SetMultipleViewType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 16))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(i))->GetDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsMultipleViewType_LocalPtr item = CreateMultipleViewType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(j))->SetDescriptor(
						((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(j+1))->GetDescriptor());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(j))->SetDescriptor(
						((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(j-1))->GetDescriptor());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(i))->SetDescriptor(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsMultipleViewType_LocalPtr)coll->elementAt(i))->GetDescriptor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MultipleViewType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MultipleViewType");
	}
	return result;
}

XMLCh * Mp7JrsMultipleViewType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMultipleViewType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMultipleViewType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MultipleViewType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_fixedViewsFlag);
		element->setAttributeNS(X(""), X("fixedViewsFlag"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	if (m_MultipleViewType_LocalType != Mp7JrsMultipleViewType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MultipleViewType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMultipleViewType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("fixedViewsFlag")))
	{
		// deserialize value type
		this->SetfixedViewsFlag(Dc1Convert::TextToBool(parent->getAttribute(X("fixedViewsFlag"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IsVisible")) == 0)
			(Dc1Util::HasNodeName(parent, X("IsVisible"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Descriptor")) == 0)
			(Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsMultipleViewType_CollectionPtr tmp = CreateMultipleViewType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMultipleViewType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMultipleViewType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsMultipleViewType_CollectionPtr tmp = CreateMultipleViewType_CollectionType; // FTT, check this
	this->SetMultipleViewType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMultipleViewType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMultipleViewType_LocalType() != Dc1NodePtr())
		result.Insert(GetMultipleViewType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMultipleViewType_ExtMethodImpl.h


