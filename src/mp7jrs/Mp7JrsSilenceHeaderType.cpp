
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsSilenceHeaderType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSilenceHeaderType::IMp7JrsSilenceHeaderType()
{

// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtPropInit.h

}

IMp7JrsSilenceHeaderType::~IMp7JrsSilenceHeaderType()
{
// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtPropCleanup.h

}

Mp7JrsSilenceHeaderType::Mp7JrsSilenceHeaderType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSilenceHeaderType::~Mp7JrsSilenceHeaderType()
{
	Cleanup();
}

void Mp7JrsSilenceHeaderType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_minDuration = Mp7JrsmediaDurationPtr(); // Pattern



// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtMyPropInit.h

}

void Mp7JrsSilenceHeaderType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_minDuration); // Pattern
}

void Mp7JrsSilenceHeaderType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SilenceHeaderTypePtr, since we
	// might need GetBase(), which isn't defined in ISilenceHeaderType
	const Dc1Ptr< Mp7JrsSilenceHeaderType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSilenceHeaderType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_minDuration); // Pattern
		this->SetminDuration(Dc1Factory::CloneObject(tmp->GetminDuration()));
	}
}

Dc1NodePtr Mp7JrsSilenceHeaderType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSilenceHeaderType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsSilenceHeaderType::GetBase() const
{
	return m_Base;
}

Mp7JrsmediaDurationPtr Mp7JrsSilenceHeaderType::GetminDuration() const
{
	return m_minDuration;
}

void Mp7JrsSilenceHeaderType::SetminDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSilenceHeaderType::SetminDuration().");
	}
	m_minDuration = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_minDuration) m_minDuration->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSilenceHeaderType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSilenceHeaderType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSilenceHeaderType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSilenceHeaderType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSilenceHeaderType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsSilenceHeaderType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("minDuration")) == 0)
	{
		// minDuration is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SilenceHeaderType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SilenceHeaderType");
	}
	return result;
}

XMLCh * Mp7JrsSilenceHeaderType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSilenceHeaderType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSilenceHeaderType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SilenceHeaderType"));
	// Attribute Serialization:
	// Pattern
	if (m_minDuration != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_minDuration->ToText();
		element->setAttributeNS(X(""), X("minDuration"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsSilenceHeaderType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("minDuration")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("minDuration")));
		this->SetminDuration(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSilenceHeaderType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSilenceHeaderType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSilenceHeaderType_ExtMethodImpl.h


