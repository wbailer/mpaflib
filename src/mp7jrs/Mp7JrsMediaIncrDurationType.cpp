
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// begin extension included
// file Mp7JrsMediaIncrDurationType_ExtImplInclude.h
#include "Mp7JrsMediaTimeType.h"
// end extension included


#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsMediaIncrDurationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaIncrDurationType::IMp7JrsMediaIncrDurationType()
{

// no includefile for extension defined 
// file Mp7JrsMediaIncrDurationType_ExtPropInit.h

}

IMp7JrsMediaIncrDurationType::~IMp7JrsMediaIncrDurationType()
{
// no includefile for extension defined 
// file Mp7JrsMediaIncrDurationType_ExtPropCleanup.h

}

Mp7JrsMediaIncrDurationType::Mp7JrsMediaIncrDurationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaIncrDurationType::~Mp7JrsMediaIncrDurationType()
{
	Cleanup();
}

void Mp7JrsMediaIncrDurationType::Init()
{
	// Init base
	m_Base = 0;

	// Init attributes
	m_mediaTimeUnit = Mp7JrsmediaDurationPtr(); // Pattern
	m_mediaTimeUnit_Exist = false;



// begin extension included
// file Mp7JrsMediaIncrDurationType_ExtMyPropInit.h
	m_MediaDurationValid = false;
	m_ContentValid = true;
// end extension included

}

void Mp7JrsMediaIncrDurationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaIncrDurationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
}

void Mp7JrsMediaIncrDurationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaIncrDurationTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaIncrDurationType
	const Dc1Ptr< Mp7JrsMediaIncrDurationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	// Dc1Factory::DeleteObject(m_mediaTimeUnit); // Pattern
	if (tmp->ExistmediaTimeUnit())
	{
		this->SetmediaTimeUnit(Dc1Factory::CloneObject(tmp->GetmediaTimeUnit()));
	}
	else
	{
		InvalidatemediaTimeUnit();
	}
	}
}

Dc1NodePtr Mp7JrsMediaIncrDurationType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaIncrDurationType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsMediaIncrDurationType::SetContent(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIncrDurationType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
	// Special behavior:
	m_MediaDurationValid = false;
	m_ContentValid = true;
}

int Mp7JrsMediaIncrDurationType::GetContent() const
{
	return m_Base;
}
Mp7JrsmediaDurationPtr Mp7JrsMediaIncrDurationType::GetmediaTimeUnit() const
{
	return m_mediaTimeUnit;
}

bool Mp7JrsMediaIncrDurationType::ExistmediaTimeUnit() const
{
	return m_mediaTimeUnit_Exist;
}
void Mp7JrsMediaIncrDurationType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIncrDurationType::SetmediaTimeUnit().");
	}
	m_mediaTimeUnit = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeUnit) m_mediaTimeUnit->SetParent(m_myself.getPointer());
	m_mediaTimeUnit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaIncrDurationType::InvalidatemediaTimeUnit()
{
	m_mediaTimeUnit_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaIncrDurationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeUnit")) == 0)
	{
		// mediaTimeUnit is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaIncrDurationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaIncrDurationType");
	}
	return result;
}

XMLCh * Mp7JrsMediaIncrDurationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaIncrDurationType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsMediaIncrDurationType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsMediaIncrDurationType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsMediaIncrDurationType::ContentToString() const
{
	// Mp7JrsMediaIncrDurationType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsMediaIncrDurationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// Special behavior:
	// At this point, the node should aready be embeded in the MPEG-7 document.
	// If the content was not evaluated correctly at the beginning it should be possible now.
	if (m_MediaDurationValid && !m_ContentValid)
		SetMediaDuration(m_MediaDuration);

	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaIncrDurationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaIncrDurationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeUnit_Exist)
	{
	// Pattern
	if (m_mediaTimeUnit != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_mediaTimeUnit->ToText();
		element->setAttributeNS(X(""), X("mediaTimeUnit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsMediaIncrDurationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeUnit")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeUnit")));
		this->SetmediaTimeUnit(tmp);
		* current = parent;
	}

  m_Base = Dc1Convert::TextToInt(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file Mp7JrsMediaIncrDurationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaIncrDurationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaIncrDurationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsMediaIncrDurationType_ExtMethodImpl.h

void Mp7JrsMediaIncrDurationType::SetMediaDuration(Mp7JrsmediaDurationPtr duration, Dc1ClientID client) {
	if (duration == 0)
		return;
	long long value = duration->GetTotalNrFractions();
	bool valueCorrect = false;
	// check if the node is already embeded in the MPEG-7 document. In this case we can evaluate the correct Content
	Mp7JrsMediaTimePtr mediaTime = GetParent();
	if (mediaTime != 0) {
		Mp7JrsmediaTimePointPtr base;
		Mp7JrsmediaDurationPtr unit;
		mediaTime->resolveBaseAndUnit(base, unit);
		if (unit != 0 && unit->GetFractionsPerSecond() != 0 && duration->GetFractionsPerSecond() != 0) {
			value = (long long)((double)duration->GetTotalNrFractions() * (double)unit->GetFractionsPerSecond() / (double)duration->GetFractionsPerSecond() / (double)unit->GetTotalNrFractions());
			valueCorrect = true;
		}
	}
	SetContent((int)value, client);
	m_ContentValid = valueCorrect;
	m_MediaDuration = duration;
	m_MediaDurationValid = true;
}

Mp7JrsmediaDurationPtr Mp7JrsMediaIncrDurationType::GetMediaDuration() {
	if (!m_MediaDurationValid) {
		Mp7JrsMediaTimePtr mediaTime = GetParent();
		if (mediaTime != 0) {
			Mp7JrsmediaTimePointPtr base;
			Mp7JrsmediaDurationPtr unit;
			mediaTime->resolveBaseAndUnit(base, unit);
			if (unit != 0 && unit->GetFractionsPerSecond() != 0) {
				long long result = (long long)GetContent() * unit->GetTotalNrFractions();
				Mp7JrsmediaDurationPtr duration = CreatemediaDurationType;
				duration->SetFractionsPerSecond(unit->GetFractionsPerSecond());
				duration->SetTotalNrFractions(result, 0);
				m_MediaDuration = duration;
				m_MediaDurationValid = true;
			}
			else
				throw Dc1Exception(DC1_ERROR, "Cannot resolve mediaTimeUnit and mediaTimeBase to create a MediaDuration from a MediaIncrDuration.");
		}
		else
			throw Dc1Exception(DC1_ERROR, "Cannot resolve mediaTimeUnit and mediaTimeBase to create a MediaDuration from a MediaIncrDuration. Element is not within the document structure.");
	}
	return m_MediaDuration;
}
// end extension included


