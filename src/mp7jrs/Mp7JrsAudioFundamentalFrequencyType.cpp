
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtImplInclude.h


#include "Mp7JrsAudioLLDScalarType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsAudioFundamentalFrequencyType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDScalarType_SeriesOfScalar_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfScalar

#include <assert.h>
IMp7JrsAudioFundamentalFrequencyType::IMp7JrsAudioFundamentalFrequencyType()
{

// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtPropInit.h

}

IMp7JrsAudioFundamentalFrequencyType::~IMp7JrsAudioFundamentalFrequencyType()
{
// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtPropCleanup.h

}

Mp7JrsAudioFundamentalFrequencyType::Mp7JrsAudioFundamentalFrequencyType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioFundamentalFrequencyType::~Mp7JrsAudioFundamentalFrequencyType()
{
	Cleanup();
}

void Mp7JrsAudioFundamentalFrequencyType::Init()
{
	// Init base
	m_Base = CreateAudioLLDScalarType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_loLimit = 0.0f; // Value
	m_loLimit_Default = 25; // Default value
	m_loLimit_Exist = false;
	m_hiLimit = 0.0f; // Value
	m_hiLimit_Exist = false;



// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtMyPropInit.h

}

void Mp7JrsAudioFundamentalFrequencyType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsAudioFundamentalFrequencyType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioFundamentalFrequencyTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioFundamentalFrequencyType
	const Dc1Ptr< Mp7JrsAudioFundamentalFrequencyType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioLLDScalarPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioFundamentalFrequencyType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistloLimit())
	{
		this->SetloLimit(tmp->GetloLimit());
	}
	else
	{
		InvalidateloLimit();
	}
	}
	{
	if (tmp->ExisthiLimit())
	{
		this->SethiLimit(tmp->GethiLimit());
	}
	else
	{
		InvalidatehiLimit();
	}
	}
}

Dc1NodePtr Mp7JrsAudioFundamentalFrequencyType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioFundamentalFrequencyType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioLLDScalarType > Mp7JrsAudioFundamentalFrequencyType::GetBase() const
{
	return m_Base;
}

float Mp7JrsAudioFundamentalFrequencyType::GetloLimit() const
{
	if (this->ExistloLimit()) {
		return m_loLimit;
	} else {
		return m_loLimit_Default;
	}
}

bool Mp7JrsAudioFundamentalFrequencyType::ExistloLimit() const
{
	return m_loLimit_Exist;
}
void Mp7JrsAudioFundamentalFrequencyType::SetloLimit(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::SetloLimit().");
	}
	m_loLimit = item;
	m_loLimit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioFundamentalFrequencyType::InvalidateloLimit()
{
	m_loLimit_Exist = false;
}
float Mp7JrsAudioFundamentalFrequencyType::GethiLimit() const
{
	return m_hiLimit;
}

bool Mp7JrsAudioFundamentalFrequencyType::ExisthiLimit() const
{
	return m_hiLimit_Exist;
}
void Mp7JrsAudioFundamentalFrequencyType::SethiLimit(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::SethiLimit().");
	}
	m_hiLimit = item;
	m_hiLimit_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioFundamentalFrequencyType::InvalidatehiLimit()
{
	m_hiLimit_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsAudioFundamentalFrequencyType::Getconfidence() const
{
	return GetBase()->Getconfidence();
}

bool Mp7JrsAudioFundamentalFrequencyType::Existconfidence() const
{
	return GetBase()->Existconfidence();
}
void Mp7JrsAudioFundamentalFrequencyType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::Setconfidence().");
	}
	GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioFundamentalFrequencyType::Invalidateconfidence()
{
	GetBase()->Invalidateconfidence();
}
float Mp7JrsAudioFundamentalFrequencyType::GetScalar() const
{
	return GetBase()->GetScalar();
}

Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr Mp7JrsAudioFundamentalFrequencyType::GetSeriesOfScalar() const
{
	return GetBase()->GetSeriesOfScalar();
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioFundamentalFrequencyType::SetScalar(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::SetScalar().");
	}
	GetBase()->SetScalar(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioFundamentalFrequencyType::SetSeriesOfScalar(const Mp7JrsAudioLLDScalarType_SeriesOfScalar_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::SetSeriesOfScalar().");
	}
	GetBase()->SetSeriesOfScalar(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsAudioFundamentalFrequencyType::Getchannels() const
{
	return GetBase()->GetBase()->Getchannels();
}

bool Mp7JrsAudioFundamentalFrequencyType::Existchannels() const
{
	return GetBase()->GetBase()->Existchannels();
}
void Mp7JrsAudioFundamentalFrequencyType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioFundamentalFrequencyType::Setchannels().");
	}
	GetBase()->GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioFundamentalFrequencyType::Invalidatechannels()
{
	GetBase()->GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsAudioFundamentalFrequencyType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("loLimit")) == 0)
	{
		// loLimit is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("hiLimit")) == 0)
	{
		// hiLimit is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioFundamentalFrequencyType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioFundamentalFrequencyType");
	}
	return result;
}

XMLCh * Mp7JrsAudioFundamentalFrequencyType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioFundamentalFrequencyType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsAudioFundamentalFrequencyType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioFundamentalFrequencyType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_loLimit_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_loLimit);
		element->setAttributeNS(X(""), X("loLimit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_hiLimit_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_hiLimit);
		element->setAttributeNS(X(""), X("hiLimit"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsAudioFundamentalFrequencyType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("loLimit")))
	{
		// deserialize value type
		this->SetloLimit(Dc1Convert::TextToFloat(parent->getAttribute(X("loLimit"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("hiLimit")))
	{
		// deserialize value type
		this->SethiLimit(Dc1Convert::TextToFloat(parent->getAttribute(X("hiLimit"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioLLDScalarType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioFundamentalFrequencyType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioFundamentalFrequencyType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSeriesOfScalar() != Dc1NodePtr())
		result.Insert(GetSeriesOfScalar());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioFundamentalFrequencyType_ExtMethodImpl.h


