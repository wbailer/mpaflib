
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentAbstractionType.h"
#include "Mp7JrsSemanticDescriptionType_Semantics_CollectionType.h"
#include "Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionType.h"
#include "Mp7JrsContentDescriptionType_Affective_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsSemanticDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsAffectiveType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affective
#include "Mp7JrsSemanticType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Semantics
#include "Mp7JrsConceptCollectionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ConceptCollection

#include <assert.h>
IMp7JrsSemanticDescriptionType::IMp7JrsSemanticDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtPropInit.h

}

IMp7JrsSemanticDescriptionType::~IMp7JrsSemanticDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtPropCleanup.h

}

Mp7JrsSemanticDescriptionType::Mp7JrsSemanticDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticDescriptionType::~Mp7JrsSemanticDescriptionType()
{
	Cleanup();
}

void Mp7JrsSemanticDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentAbstractionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Semantics = Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr(); // Collection
	m_ConceptCollection = Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtMyPropInit.h

}

void Mp7JrsSemanticDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Semantics);
	// Dc1Factory::DeleteObject(m_ConceptCollection);
}

void Mp7JrsSemanticDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticDescriptionType
	const Dc1Ptr< Mp7JrsSemanticDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentAbstractionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSemanticDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Semantics);
		this->SetSemantics(Dc1Factory::CloneObject(tmp->GetSemantics()));
		// Dc1Factory::DeleteObject(m_ConceptCollection);
		this->SetConceptCollection(Dc1Factory::CloneObject(tmp->GetConceptCollection()));
}

Dc1NodePtr Mp7JrsSemanticDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSemanticDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentAbstractionType > Mp7JrsSemanticDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr Mp7JrsSemanticDescriptionType::GetSemantics() const
{
		return m_Semantics;
}

Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr Mp7JrsSemanticDescriptionType::GetConceptCollection() const
{
		return m_ConceptCollection;
}

// implementing setter for choice 
/* element */
void Mp7JrsSemanticDescriptionType::SetSemantics(const Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetSemantics().");
	}
	if (m_Semantics != item)
	{
		// Dc1Factory::DeleteObject(m_Semantics);
		m_Semantics = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Semantics) m_Semantics->SetParent(m_myself.getPointer());
		if(m_Semantics != Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr())
		{
			m_Semantics->SetContentName(XMLString::transcode("Semantics"));
		}
	// Dc1Factory::DeleteObject(m_ConceptCollection);
	m_ConceptCollection = Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSemanticDescriptionType::SetConceptCollection(const Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetConceptCollection().");
	}
	if (m_ConceptCollection != item)
	{
		// Dc1Factory::DeleteObject(m_ConceptCollection);
		m_ConceptCollection = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ConceptCollection) m_ConceptCollection->SetParent(m_myself.getPointer());
		if(m_ConceptCollection != Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr())
		{
			m_ConceptCollection->SetContentName(XMLString::transcode("ConceptCollection"));
		}
	// Dc1Factory::DeleteObject(m_Semantics);
	m_Semantics = Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsContentDescriptionType_Affective_CollectionPtr Mp7JrsSemanticDescriptionType::GetAffective() const
{
	return GetBase()->GetBase()->GetAffective();
}

void Mp7JrsSemanticDescriptionType::SetAffective(const Mp7JrsContentDescriptionType_Affective_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetAffective().");
	}
	GetBase()->GetBase()->SetAffective(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsSemanticDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsSemanticDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsSemanticDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsSemanticDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsSemanticDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsSemanticDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSemanticDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Semantics")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Semantics is item of type SemanticType
		// in element collection SemanticDescriptionType_Semantics_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr coll = GetSemantics();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticDescriptionType_Semantics_CollectionType; // FTT, check this
				SetSemantics(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SemanticType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ConceptCollection")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ConceptCollection is item of type ConceptCollectionType
		// in element collection SemanticDescriptionType_ConceptCollection_CollectionType
		
		context->Found = true;
		Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr coll = GetConceptCollection();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSemanticDescriptionType_ConceptCollection_CollectionType; // FTT, check this
				SetConceptCollection(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConceptCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsConceptCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsSemanticDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSemanticDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSemanticDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticDescriptionType"));
	// Element serialization:
	if (m_Semantics != Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Semantics->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ConceptCollection != Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ConceptCollection->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSemanticDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentAbstractionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Semantics"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Semantics")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr tmp = CreateSemanticDescriptionType_Semantics_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSemantics(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ConceptCollection"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ConceptCollection")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr tmp = CreateSemanticDescriptionType_ConceptCollection_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetConceptCollection(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Semantics")) == 0))
  {
	Mp7JrsSemanticDescriptionType_Semantics_CollectionPtr tmp = CreateSemanticDescriptionType_Semantics_CollectionType; // FTT, check this
	this->SetSemantics(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ConceptCollection")) == 0))
  {
	Mp7JrsSemanticDescriptionType_ConceptCollection_CollectionPtr tmp = CreateSemanticDescriptionType_ConceptCollection_CollectionType; // FTT, check this
	this->SetConceptCollection(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSemantics() != Dc1NodePtr())
		result.Insert(GetSemantics());
	if (GetConceptCollection() != Dc1NodePtr())
		result.Insert(GetConceptCollection());
	if (GetAffective() != Dc1NodePtr())
		result.Insert(GetAffective());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticDescriptionType_ExtMethodImpl.h


