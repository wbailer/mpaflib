
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtImplInclude.h


#include "Mp7JrsFigureTrajectoryType.h"
#include "Mp7JrsParameterTrajectoryType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsSpatioTemporalLocatorType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpatioTemporalLocatorType_LocalType::IMp7JrsSpatioTemporalLocatorType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtPropInit.h

}

IMp7JrsSpatioTemporalLocatorType_LocalType::~IMp7JrsSpatioTemporalLocatorType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtPropCleanup.h

}

Mp7JrsSpatioTemporalLocatorType_LocalType::Mp7JrsSpatioTemporalLocatorType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpatioTemporalLocatorType_LocalType::~Mp7JrsSpatioTemporalLocatorType_LocalType()
{
	Cleanup();
}

void Mp7JrsSpatioTemporalLocatorType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_FigureTrajectory = Mp7JrsFigureTrajectoryPtr(); // Class
	m_ParameterTrajectory = Mp7JrsParameterTrajectoryPtr(); // Class
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpatioTemporalLocatorType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_FigureTrajectory);
	// Dc1Factory::DeleteObject(m_ParameterTrajectory);
	// Dc1Factory::DeleteObject(m_MediaTime);
}

void Mp7JrsSpatioTemporalLocatorType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpatioTemporalLocatorType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpatioTemporalLocatorType_LocalType
	const Dc1Ptr< Mp7JrsSpatioTemporalLocatorType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_FigureTrajectory);
		this->SetFigureTrajectory(Dc1Factory::CloneObject(tmp->GetFigureTrajectory()));
		// Dc1Factory::DeleteObject(m_ParameterTrajectory);
		this->SetParameterTrajectory(Dc1Factory::CloneObject(tmp->GetParameterTrajectory()));
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
}

Dc1NodePtr Mp7JrsSpatioTemporalLocatorType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpatioTemporalLocatorType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsFigureTrajectoryPtr Mp7JrsSpatioTemporalLocatorType_LocalType::GetFigureTrajectory() const
{
		return m_FigureTrajectory;
}

Mp7JrsParameterTrajectoryPtr Mp7JrsSpatioTemporalLocatorType_LocalType::GetParameterTrajectory() const
{
		return m_ParameterTrajectory;
}

Mp7JrsMediaTimePtr Mp7JrsSpatioTemporalLocatorType_LocalType::GetMediaTime() const
{
		return m_MediaTime;
}

// implementing setter for choice 
/* element */
void Mp7JrsSpatioTemporalLocatorType_LocalType::SetFigureTrajectory(const Mp7JrsFigureTrajectoryPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatioTemporalLocatorType_LocalType::SetFigureTrajectory().");
	}
	if (m_FigureTrajectory != item)
	{
		// Dc1Factory::DeleteObject(m_FigureTrajectory);
		m_FigureTrajectory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FigureTrajectory) m_FigureTrajectory->SetParent(m_myself.getPointer());
		if (m_FigureTrajectory && m_FigureTrajectory->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FigureTrajectoryType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FigureTrajectory->UseTypeAttribute = true;
		}
		if(m_FigureTrajectory != Mp7JrsFigureTrajectoryPtr())
		{
			m_FigureTrajectory->SetContentName(XMLString::transcode("FigureTrajectory"));
		}
	// Dc1Factory::DeleteObject(m_ParameterTrajectory);
	m_ParameterTrajectory = Mp7JrsParameterTrajectoryPtr();
	// Dc1Factory::DeleteObject(m_MediaTime);
	m_MediaTime = Mp7JrsMediaTimePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpatioTemporalLocatorType_LocalType::SetParameterTrajectory(const Mp7JrsParameterTrajectoryPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatioTemporalLocatorType_LocalType::SetParameterTrajectory().");
	}
	if (m_ParameterTrajectory != item)
	{
		// Dc1Factory::DeleteObject(m_ParameterTrajectory);
		m_ParameterTrajectory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ParameterTrajectory) m_ParameterTrajectory->SetParent(m_myself.getPointer());
		if (m_ParameterTrajectory && m_ParameterTrajectory->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParameterTrajectoryType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ParameterTrajectory->UseTypeAttribute = true;
		}
		if(m_ParameterTrajectory != Mp7JrsParameterTrajectoryPtr())
		{
			m_ParameterTrajectory->SetContentName(XMLString::transcode("ParameterTrajectory"));
		}
	// Dc1Factory::DeleteObject(m_FigureTrajectory);
	m_FigureTrajectory = Mp7JrsFigureTrajectoryPtr();
	// Dc1Factory::DeleteObject(m_MediaTime);
	m_MediaTime = Mp7JrsMediaTimePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpatioTemporalLocatorType_LocalType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatioTemporalLocatorType_LocalType::SetMediaTime().");
	}
	if (m_MediaTime != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	// Dc1Factory::DeleteObject(m_FigureTrajectory);
	m_FigureTrajectory = Mp7JrsFigureTrajectoryPtr();
	// Dc1Factory::DeleteObject(m_ParameterTrajectory);
	m_ParameterTrajectory = Mp7JrsParameterTrajectoryPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SpatioTemporalLocatorType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpatioTemporalLocatorType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpatioTemporalLocatorType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpatioTemporalLocatorType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpatioTemporalLocatorType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_FigureTrajectory != Mp7JrsFigureTrajectoryPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FigureTrajectory->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FigureTrajectory"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FigureTrajectory->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ParameterTrajectory != Mp7JrsParameterTrajectoryPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ParameterTrajectory->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ParameterTrajectory"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ParameterTrajectory->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpatioTemporalLocatorType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FigureTrajectory"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FigureTrajectory")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFigureTrajectoryType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFigureTrajectory(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ParameterTrajectory"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ParameterTrajectory")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateParameterTrajectoryType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParameterTrajectory(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpatioTemporalLocatorType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("FigureTrajectory")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFigureTrajectoryType; // FTT, check this
	}
	this->SetFigureTrajectory(child);
  }
  if (XMLString::compareString(elementname, X("ParameterTrajectory")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateParameterTrajectoryType; // FTT, check this
	}
	this->SetParameterTrajectory(child);
  }
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpatioTemporalLocatorType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFigureTrajectory() != Dc1NodePtr())
		result.Insert(GetFigureTrajectory());
	if (GetParameterTrajectory() != Dc1NodePtr())
		result.Insert(GetParameterTrajectory());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpatioTemporalLocatorType_LocalType_ExtMethodImpl.h


