
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsMotionActivityType_Intensity_LocalType.h"
#include "Mp7Jrsunsigned3.h"
#include "Mp7JrsMotionActivityType_SpatialDistributionParams_LocalType.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalType.h"
#include "Mp7JrsMotionActivityType_TemporalParams_LocalType.h"
#include "Mp7JrsMotionActivityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMotionActivityType::IMp7JrsMotionActivityType()
{

// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtPropInit.h

}

IMp7JrsMotionActivityType::~IMp7JrsMotionActivityType()
{
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtPropCleanup.h

}

Mp7JrsMotionActivityType::Mp7JrsMotionActivityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMotionActivityType::~Mp7JrsMotionActivityType()
{
	Cleanup();
}

void Mp7JrsMotionActivityType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Intensity = Mp7JrsMotionActivityType_Intensity_LocalPtr(); // Class with content 
	m_DominantDirection = Mp7Jrsunsigned3Ptr(); // Class with content 
	m_DominantDirection_Exist = false;
	m_SpatialDistributionParams = Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr(); // Class
	m_SpatialDistributionParams_Exist = false;
	m_SpatialLocalizationParams = Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr(); // Class
	m_SpatialLocalizationParams_Exist = false;
	m_TemporalParams = Mp7JrsMotionActivityType_TemporalParams_LocalPtr(); // Class
	m_TemporalParams_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtMyPropInit.h

}

void Mp7JrsMotionActivityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Intensity);
	// Dc1Factory::DeleteObject(m_DominantDirection);
	// Dc1Factory::DeleteObject(m_SpatialDistributionParams);
	// Dc1Factory::DeleteObject(m_SpatialLocalizationParams);
	// Dc1Factory::DeleteObject(m_TemporalParams);
}

void Mp7JrsMotionActivityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MotionActivityTypePtr, since we
	// might need GetBase(), which isn't defined in IMotionActivityType
	const Dc1Ptr< Mp7JrsMotionActivityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMotionActivityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Intensity);
		this->SetIntensity(Dc1Factory::CloneObject(tmp->GetIntensity()));
	if (tmp->IsValidDominantDirection())
	{
		// Dc1Factory::DeleteObject(m_DominantDirection);
		this->SetDominantDirection(Dc1Factory::CloneObject(tmp->GetDominantDirection()));
	}
	else
	{
		InvalidateDominantDirection();
	}
	if (tmp->IsValidSpatialDistributionParams())
	{
		// Dc1Factory::DeleteObject(m_SpatialDistributionParams);
		this->SetSpatialDistributionParams(Dc1Factory::CloneObject(tmp->GetSpatialDistributionParams()));
	}
	else
	{
		InvalidateSpatialDistributionParams();
	}
	if (tmp->IsValidSpatialLocalizationParams())
	{
		// Dc1Factory::DeleteObject(m_SpatialLocalizationParams);
		this->SetSpatialLocalizationParams(Dc1Factory::CloneObject(tmp->GetSpatialLocalizationParams()));
	}
	else
	{
		InvalidateSpatialLocalizationParams();
	}
	if (tmp->IsValidTemporalParams())
	{
		// Dc1Factory::DeleteObject(m_TemporalParams);
		this->SetTemporalParams(Dc1Factory::CloneObject(tmp->GetTemporalParams()));
	}
	else
	{
		InvalidateTemporalParams();
	}
}

Dc1NodePtr Mp7JrsMotionActivityType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMotionActivityType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsMotionActivityType::GetBase() const
{
	return m_Base;
}

Mp7JrsMotionActivityType_Intensity_LocalPtr Mp7JrsMotionActivityType::GetIntensity() const
{
		return m_Intensity;
}

Mp7Jrsunsigned3Ptr Mp7JrsMotionActivityType::GetDominantDirection() const
{
		return m_DominantDirection;
}

// Element is optional
bool Mp7JrsMotionActivityType::IsValidDominantDirection() const
{
	return m_DominantDirection_Exist;
}

Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr Mp7JrsMotionActivityType::GetSpatialDistributionParams() const
{
		return m_SpatialDistributionParams;
}

// Element is optional
bool Mp7JrsMotionActivityType::IsValidSpatialDistributionParams() const
{
	return m_SpatialDistributionParams_Exist;
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr Mp7JrsMotionActivityType::GetSpatialLocalizationParams() const
{
		return m_SpatialLocalizationParams;
}

// Element is optional
bool Mp7JrsMotionActivityType::IsValidSpatialLocalizationParams() const
{
	return m_SpatialLocalizationParams_Exist;
}

Mp7JrsMotionActivityType_TemporalParams_LocalPtr Mp7JrsMotionActivityType::GetTemporalParams() const
{
		return m_TemporalParams;
}

// Element is optional
bool Mp7JrsMotionActivityType::IsValidTemporalParams() const
{
	return m_TemporalParams_Exist;
}

void Mp7JrsMotionActivityType::SetIntensity(const Mp7JrsMotionActivityType_Intensity_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType::SetIntensity().");
	}
	if (m_Intensity != item)
	{
		// Dc1Factory::DeleteObject(m_Intensity);
		m_Intensity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Intensity) m_Intensity->SetParent(m_myself.getPointer());
		if (m_Intensity && m_Intensity->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_Intensity_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Intensity->UseTypeAttribute = true;
		}
		if(m_Intensity != Mp7JrsMotionActivityType_Intensity_LocalPtr())
		{
			m_Intensity->SetContentName(XMLString::transcode("Intensity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionActivityType::SetDominantDirection(const Mp7Jrsunsigned3Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType::SetDominantDirection().");
	}
	if (m_DominantDirection != item || m_DominantDirection_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DominantDirection);
		m_DominantDirection = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DominantDirection) m_DominantDirection->SetParent(m_myself.getPointer());
		if (m_DominantDirection && m_DominantDirection->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned3"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DominantDirection->UseTypeAttribute = true;
		}
		m_DominantDirection_Exist = true;
		if(m_DominantDirection != Mp7Jrsunsigned3Ptr())
		{
			m_DominantDirection->SetContentName(XMLString::transcode("DominantDirection"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionActivityType::InvalidateDominantDirection()
{
	m_DominantDirection_Exist = false;
}
void Mp7JrsMotionActivityType::SetSpatialDistributionParams(const Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType::SetSpatialDistributionParams().");
	}
	if (m_SpatialDistributionParams != item || m_SpatialDistributionParams_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SpatialDistributionParams);
		m_SpatialDistributionParams = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialDistributionParams) m_SpatialDistributionParams->SetParent(m_myself.getPointer());
		if (m_SpatialDistributionParams && m_SpatialDistributionParams->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialDistributionParams_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialDistributionParams->UseTypeAttribute = true;
		}
		m_SpatialDistributionParams_Exist = true;
		if(m_SpatialDistributionParams != Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr())
		{
			m_SpatialDistributionParams->SetContentName(XMLString::transcode("SpatialDistributionParams"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionActivityType::InvalidateSpatialDistributionParams()
{
	m_SpatialDistributionParams_Exist = false;
}
void Mp7JrsMotionActivityType::SetSpatialLocalizationParams(const Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType::SetSpatialLocalizationParams().");
	}
	if (m_SpatialLocalizationParams != item || m_SpatialLocalizationParams_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SpatialLocalizationParams);
		m_SpatialLocalizationParams = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialLocalizationParams) m_SpatialLocalizationParams->SetParent(m_myself.getPointer());
		if (m_SpatialLocalizationParams && m_SpatialLocalizationParams->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialLocalizationParams->UseTypeAttribute = true;
		}
		m_SpatialLocalizationParams_Exist = true;
		if(m_SpatialLocalizationParams != Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr())
		{
			m_SpatialLocalizationParams->SetContentName(XMLString::transcode("SpatialLocalizationParams"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionActivityType::InvalidateSpatialLocalizationParams()
{
	m_SpatialLocalizationParams_Exist = false;
}
void Mp7JrsMotionActivityType::SetTemporalParams(const Mp7JrsMotionActivityType_TemporalParams_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMotionActivityType::SetTemporalParams().");
	}
	if (m_TemporalParams != item || m_TemporalParams_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TemporalParams);
		m_TemporalParams = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalParams) m_TemporalParams->SetParent(m_myself.getPointer());
		if (m_TemporalParams && m_TemporalParams->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_TemporalParams_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemporalParams->UseTypeAttribute = true;
		}
		m_TemporalParams_Exist = true;
		if(m_TemporalParams != Mp7JrsMotionActivityType_TemporalParams_LocalPtr())
		{
			m_TemporalParams->SetContentName(XMLString::transcode("TemporalParams"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMotionActivityType::InvalidateTemporalParams()
{
	m_TemporalParams_Exist = false;
}

Dc1NodeEnum Mp7JrsMotionActivityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Intensity")) == 0)
	{
		// Intensity is simple element MotionActivityType_Intensity_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIntensity()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_Intensity_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_Intensity_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIntensity(p, client);
					if((p = GetIntensity()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DominantDirection")) == 0)
	{
		// DominantDirection is simple element unsigned3
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDominantDirection()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned3")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned3Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDominantDirection(p, client);
					if((p = GetDominantDirection()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialDistributionParams")) == 0)
	{
		// SpatialDistributionParams is simple element MotionActivityType_SpatialDistributionParams_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatialDistributionParams()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialDistributionParams_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatialDistributionParams(p, client);
					if((p = GetSpatialDistributionParams()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialLocalizationParams")) == 0)
	{
		// SpatialLocalizationParams is simple element MotionActivityType_SpatialLocalizationParams_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatialLocalizationParams()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatialLocalizationParams(p, client);
					if((p = GetSpatialLocalizationParams()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TemporalParams")) == 0)
	{
		// TemporalParams is simple element MotionActivityType_TemporalParams_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTemporalParams()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionActivityType_TemporalParams_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionActivityType_TemporalParams_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTemporalParams(p, client);
					if((p = GetTemporalParams()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MotionActivityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MotionActivityType");
	}
	return result;
}

XMLCh * Mp7JrsMotionActivityType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMotionActivityType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMotionActivityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MotionActivityType"));
	// Element serialization:
	// Class with content		
	
	if (m_Intensity != Mp7JrsMotionActivityType_Intensity_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Intensity->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Intensity"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Intensity->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_DominantDirection != Mp7Jrsunsigned3Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DominantDirection->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DominantDirection"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DominantDirection->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatialDistributionParams != Mp7JrsMotionActivityType_SpatialDistributionParams_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialDistributionParams->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialDistributionParams"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialDistributionParams->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatialLocalizationParams != Mp7JrsMotionActivityType_SpatialLocalizationParams_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialLocalizationParams->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialLocalizationParams"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialLocalizationParams->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TemporalParams != Mp7JrsMotionActivityType_TemporalParams_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemporalParams->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemporalParams"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemporalParams->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMotionActivityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Intensity"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Intensity")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_Intensity_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIntensity(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DominantDirection"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DominantDirection")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned3; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDominantDirection(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialDistributionParams"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialDistributionParams")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialDistributionParams_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialDistributionParams(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialLocalizationParams"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialLocalizationParams")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_SpatialLocalizationParams_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialLocalizationParams(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemporalParams"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemporalParams")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionActivityType_TemporalParams_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemporalParams(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMotionActivityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Intensity")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_Intensity_LocalType; // FTT, check this
	}
	this->SetIntensity(child);
  }
  if (XMLString::compareString(elementname, X("DominantDirection")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned3; // FTT, check this
	}
	this->SetDominantDirection(child);
  }
  if (XMLString::compareString(elementname, X("SpatialDistributionParams")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialDistributionParams_LocalType; // FTT, check this
	}
	this->SetSpatialDistributionParams(child);
  }
  if (XMLString::compareString(elementname, X("SpatialLocalizationParams")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_SpatialLocalizationParams_LocalType; // FTT, check this
	}
	this->SetSpatialLocalizationParams(child);
  }
  if (XMLString::compareString(elementname, X("TemporalParams")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionActivityType_TemporalParams_LocalType; // FTT, check this
	}
	this->SetTemporalParams(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMotionActivityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetIntensity() != Dc1NodePtr())
		result.Insert(GetIntensity());
	if (GetDominantDirection() != Dc1NodePtr())
		result.Insert(GetDominantDirection());
	if (GetSpatialDistributionParams() != Dc1NodePtr())
		result.Insert(GetSpatialDistributionParams());
	if (GetSpatialLocalizationParams() != Dc1NodePtr())
		result.Insert(GetSpatialLocalizationParams());
	if (GetTemporalParams() != Dc1NodePtr())
		result.Insert(GetTemporalParams());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMotionActivityType_ExtMethodImpl.h


