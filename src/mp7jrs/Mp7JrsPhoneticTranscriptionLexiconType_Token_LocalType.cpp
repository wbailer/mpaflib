
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtImplInclude.h


#include "Mp7JrsWordType.h"
#include "Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType.h"
#include "Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtPropInit.h

}

IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::~IMp7JrsPhoneticTranscriptionLexiconType_Token_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtPropCleanup.h

}

Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::~Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType()
{
	Cleanup();
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Init()
{
	// Init base
	m_Base = CreateWordType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_phoneticTranscription = Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr(); // Collection
	m_lang = NULL; // String
	m_lang_Exist = false;



// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtMyPropInit.h

}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_phoneticTranscription);
	XMLString::release(&m_lang); // String
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PhoneticTranscriptionLexiconType_Token_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPhoneticTranscriptionLexiconType_Token_LocalType
	const Dc1Ptr< Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsWordPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_phoneticTranscription);
		this->SetphoneticTranscription(Dc1Factory::CloneObject(tmp->GetphoneticTranscription()));
	}
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
}

Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsWordType > Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetBase() const
{
	return m_Base;
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetContent() const
{
	return GetBase()->GetContent();
}
Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetphoneticTranscription() const
{
	return m_phoneticTranscription;
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::SetphoneticTranscription(const Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::SetphoneticTranscription().");
	}
	m_phoneticTranscription = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_phoneticTranscription) m_phoneticTranscription->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Invalidatelang()
{
	m_lang_Exist = false;
}

Dc1NodeEnum Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticTranscription")) == 0)
	{
		// phoneticTranscription is simple attribute PhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PhoneticTranscriptionLexiconType_Token_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PhoneticTranscriptionLexiconType_Token_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PhoneticTranscriptionLexiconType_Token_LocalType"));
	// Attribute Serialization:
	// Collection
	if(m_phoneticTranscription != Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr())
	{
		XMLCh * tmp = m_phoneticTranscription->ToText();
		element->setAttributeNS(X(""), X("phoneticTranscription"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticTranscription")))
	{
		// Deserialize collection type
		Mp7JrsPhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionPtr tmp = CreatePhoneticTranscriptionLexiconType_Token_phoneticTranscription_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("phoneticTranscription")));
		this->SetphoneticTranscription(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Extensionbase is Mp7JrsWordType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType_ExtMethodImpl.h


