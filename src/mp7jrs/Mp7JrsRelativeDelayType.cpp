
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtImplInclude.h


#include "Mp7JrsAudioLLDVectorType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsRelativeDelayType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfVector

#include <assert.h>
IMp7JrsRelativeDelayType::IMp7JrsRelativeDelayType()
{

// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtPropInit.h

}

IMp7JrsRelativeDelayType::~IMp7JrsRelativeDelayType()
{
// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtPropCleanup.h

}

Mp7JrsRelativeDelayType::Mp7JrsRelativeDelayType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRelativeDelayType::~Mp7JrsRelativeDelayType()
{
	Cleanup();
}

void Mp7JrsRelativeDelayType::Init()
{
	// Init base
	m_Base = CreateAudioLLDVectorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_Reliability = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_Reliability->SetContent(0.0); // Use Value initialiser (xxx2)
	m_Reliability_Exist = false;



// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtMyPropInit.h

}

void Mp7JrsRelativeDelayType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Reliability);
}

void Mp7JrsRelativeDelayType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RelativeDelayTypePtr, since we
	// might need GetBase(), which isn't defined in IRelativeDelayType
	const Dc1Ptr< Mp7JrsRelativeDelayType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioLLDVectorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsRelativeDelayType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_Reliability);
	if (tmp->ExistReliability())
	{
		this->SetReliability(Dc1Factory::CloneObject(tmp->GetReliability()));
	}
	else
	{
		InvalidateReliability();
	}
	}
}

Dc1NodePtr Mp7JrsRelativeDelayType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsRelativeDelayType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioLLDVectorType > Mp7JrsRelativeDelayType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsRelativeDelayType::GetReliability() const
{
	return m_Reliability;
}

bool Mp7JrsRelativeDelayType::ExistReliability() const
{
	return m_Reliability_Exist;
}
void Mp7JrsRelativeDelayType::SetReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelativeDelayType::SetReliability().");
	}
	m_Reliability = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_Reliability) m_Reliability->SetParent(m_myself.getPointer());
	m_Reliability_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelativeDelayType::InvalidateReliability()
{
	m_Reliability_Exist = false;
}
Mp7JrsfloatVectorPtr Mp7JrsRelativeDelayType::GetVector() const
{
	return GetBase()->GetVector();
}

Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr Mp7JrsRelativeDelayType::GetSeriesOfVector() const
{
	return GetBase()->GetSeriesOfVector();
}

// implementing setter for choice 
/* element */
void Mp7JrsRelativeDelayType::SetVector(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelativeDelayType::SetVector().");
	}
	GetBase()->SetVector(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsRelativeDelayType::SetSeriesOfVector(const Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelativeDelayType::SetSeriesOfVector().");
	}
	GetBase()->SetSeriesOfVector(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsRelativeDelayType::Getchannels() const
{
	return GetBase()->GetBase()->Getchannels();
}

bool Mp7JrsRelativeDelayType::Existchannels() const
{
	return GetBase()->GetBase()->Existchannels();
}
void Mp7JrsRelativeDelayType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRelativeDelayType::Setchannels().");
	}
	GetBase()->GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRelativeDelayType::Invalidatechannels()
{
	GetBase()->GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsRelativeDelayType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("Reliability")) == 0)
	{
		// Reliability is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RelativeDelayType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RelativeDelayType");
	}
	return result;
}

XMLCh * Mp7JrsRelativeDelayType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRelativeDelayType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsRelativeDelayType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RelativeDelayType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_Reliability_Exist)
	{
	// Class
	if (m_Reliability != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_Reliability->ToText();
		element->setAttributeNS(X(""), X("Reliability"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsRelativeDelayType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("Reliability")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("Reliability")));
		this->SetReliability(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioLLDVectorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRelativeDelayType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRelativeDelayType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVector() != Dc1NodePtr())
		result.Insert(GetVector());
	if (GetSeriesOfVector() != Dc1NodePtr())
		result.Insert(GetSeriesOfVector());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRelativeDelayType_ExtMethodImpl.h


