
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsUserIdentifierType.h"
#include "Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionType.h"
#include "Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionType.h"
#include "Mp7JrsUserPreferencesType_RecordingPreferences_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUserPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsFilteringAndSearchPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferences
#include "Mp7JrsBrowsingPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:BrowsingPreferences
#include "Mp7JrsRecordingPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RecordingPreferences

#include <assert.h>
IMp7JrsUserPreferencesType::IMp7JrsUserPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtPropInit.h

}

IMp7JrsUserPreferencesType::~IMp7JrsUserPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtPropCleanup.h

}

Mp7JrsUserPreferencesType::Mp7JrsUserPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserPreferencesType::~Mp7JrsUserPreferencesType()
{
	Cleanup();
}

void Mp7JrsUserPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_allowAutomaticUpdate = Mp7JrsuserChoiceType::UninitializedEnumeration;
	m_allowAutomaticUpdate_Default = Mp7JrsuserChoiceType::_false; // Default enumeration
	m_allowAutomaticUpdate_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_UserIdentifier = Mp7JrsUserIdentifierPtr(); // Class
	m_UserIdentifier_Exist = false;
	m_FilteringAndSearchPreferences = Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr(); // Collection
	m_BrowsingPreferences = Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr(); // Collection
	m_RecordingPreferences = Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtMyPropInit.h

}

void Mp7JrsUserPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_UserIdentifier);
	// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
	// Dc1Factory::DeleteObject(m_BrowsingPreferences);
	// Dc1Factory::DeleteObject(m_RecordingPreferences);
}

void Mp7JrsUserPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in IUserPreferencesType
	const Dc1Ptr< Mp7JrsUserPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUserPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistallowAutomaticUpdate())
	{
		this->SetallowAutomaticUpdate(tmp->GetallowAutomaticUpdate());
	}
	else
	{
		InvalidateallowAutomaticUpdate();
	}
	}
	if (tmp->IsValidUserIdentifier())
	{
		// Dc1Factory::DeleteObject(m_UserIdentifier);
		this->SetUserIdentifier(Dc1Factory::CloneObject(tmp->GetUserIdentifier()));
	}
	else
	{
		InvalidateUserIdentifier();
	}
		// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
		this->SetFilteringAndSearchPreferences(Dc1Factory::CloneObject(tmp->GetFilteringAndSearchPreferences()));
		// Dc1Factory::DeleteObject(m_BrowsingPreferences);
		this->SetBrowsingPreferences(Dc1Factory::CloneObject(tmp->GetBrowsingPreferences()));
		// Dc1Factory::DeleteObject(m_RecordingPreferences);
		this->SetRecordingPreferences(Dc1Factory::CloneObject(tmp->GetRecordingPreferences()));
}

Dc1NodePtr Mp7JrsUserPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUserPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsUserPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrsuserChoiceType::Enumeration Mp7JrsUserPreferencesType::GetallowAutomaticUpdate() const
{
	if (this->ExistallowAutomaticUpdate()) {
		return m_allowAutomaticUpdate;
	} else {
		return m_allowAutomaticUpdate_Default;
	}
}

bool Mp7JrsUserPreferencesType::ExistallowAutomaticUpdate() const
{
	return m_allowAutomaticUpdate_Exist;
}
void Mp7JrsUserPreferencesType::SetallowAutomaticUpdate(Mp7JrsuserChoiceType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetallowAutomaticUpdate().");
	}
	m_allowAutomaticUpdate = item;
	m_allowAutomaticUpdate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidateallowAutomaticUpdate()
{
	m_allowAutomaticUpdate_Exist = false;
}
Mp7JrsUserIdentifierPtr Mp7JrsUserPreferencesType::GetUserIdentifier() const
{
		return m_UserIdentifier;
}

// Element is optional
bool Mp7JrsUserPreferencesType::IsValidUserIdentifier() const
{
	return m_UserIdentifier_Exist;
}

Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr Mp7JrsUserPreferencesType::GetFilteringAndSearchPreferences() const
{
		return m_FilteringAndSearchPreferences;
}

Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr Mp7JrsUserPreferencesType::GetBrowsingPreferences() const
{
		return m_BrowsingPreferences;
}

Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr Mp7JrsUserPreferencesType::GetRecordingPreferences() const
{
		return m_RecordingPreferences;
}

void Mp7JrsUserPreferencesType::SetUserIdentifier(const Mp7JrsUserIdentifierPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetUserIdentifier().");
	}
	if (m_UserIdentifier != item || m_UserIdentifier_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_UserIdentifier);
		m_UserIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserIdentifier) m_UserIdentifier->SetParent(m_myself.getPointer());
		if (m_UserIdentifier && m_UserIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserIdentifierType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UserIdentifier->UseTypeAttribute = true;
		}
		m_UserIdentifier_Exist = true;
		if(m_UserIdentifier != Mp7JrsUserIdentifierPtr())
		{
			m_UserIdentifier->SetContentName(XMLString::transcode("UserIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidateUserIdentifier()
{
	m_UserIdentifier_Exist = false;
}
void Mp7JrsUserPreferencesType::SetFilteringAndSearchPreferences(const Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetFilteringAndSearchPreferences().");
	}
	if (m_FilteringAndSearchPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
		m_FilteringAndSearchPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FilteringAndSearchPreferences) m_FilteringAndSearchPreferences->SetParent(m_myself.getPointer());
		if(m_FilteringAndSearchPreferences != Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr())
		{
			m_FilteringAndSearchPreferences->SetContentName(XMLString::transcode("FilteringAndSearchPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::SetBrowsingPreferences(const Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetBrowsingPreferences().");
	}
	if (m_BrowsingPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_BrowsingPreferences);
		m_BrowsingPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BrowsingPreferences) m_BrowsingPreferences->SetParent(m_myself.getPointer());
		if(m_BrowsingPreferences != Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr())
		{
			m_BrowsingPreferences->SetContentName(XMLString::transcode("BrowsingPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::SetRecordingPreferences(const Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetRecordingPreferences().");
	}
	if (m_RecordingPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_RecordingPreferences);
		m_RecordingPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RecordingPreferences) m_RecordingPreferences->SetParent(m_myself.getPointer());
		if(m_RecordingPreferences != Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr())
		{
			m_RecordingPreferences->SetContentName(XMLString::transcode("RecordingPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUserPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsUserPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsUserPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUserPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsUserPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsUserPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUserPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsUserPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsUserPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUserPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUserPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUserPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUserPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUserPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUserPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUserPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsUserPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUserPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("allowAutomaticUpdate")) == 0)
	{
		// allowAutomaticUpdate is simple attribute userChoiceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsuserChoiceType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserIdentifier")) == 0)
	{
		// UserIdentifier is simple element UserIdentifierType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUserIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserIdentifierType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserIdentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUserIdentifier(p, client);
					if((p = GetUserIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FilteringAndSearchPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferences is item of type FilteringAndSearchPreferencesType
		// in element collection UserPreferencesType_FilteringAndSearchPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr coll = GetFilteringAndSearchPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
				SetFilteringAndSearchPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFilteringAndSearchPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BrowsingPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:BrowsingPreferences is item of type BrowsingPreferencesType
		// in element collection UserPreferencesType_BrowsingPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr coll = GetBrowsingPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserPreferencesType_BrowsingPreferences_CollectionType; // FTT, check this
				SetBrowsingPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsBrowsingPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RecordingPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:RecordingPreferences is item of type RecordingPreferencesType
		// in element collection UserPreferencesType_RecordingPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr coll = GetRecordingPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserPreferencesType_RecordingPreferences_CollectionType; // FTT, check this
				SetRecordingPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RecordingPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRecordingPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsUserPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUserPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUserPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_allowAutomaticUpdate_Exist)
	{
	// Enumeration
	if(m_allowAutomaticUpdate != Mp7JrsuserChoiceType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsuserChoiceType::ToText(m_allowAutomaticUpdate);
		element->setAttributeNS(X(""), X("allowAutomaticUpdate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_UserIdentifier != Mp7JrsUserIdentifierPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UserIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UserIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UserIdentifier->Serialize(doc, element, &elem);
	}
	if (m_FilteringAndSearchPreferences != Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FilteringAndSearchPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_BrowsingPreferences != Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_BrowsingPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_RecordingPreferences != Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RecordingPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUserPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("allowAutomaticUpdate")))
	{
		this->SetallowAutomaticUpdate(Mp7JrsuserChoiceType::Parse(parent->getAttribute(X("allowAutomaticUpdate"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UserIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UserIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUserIdentifierType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUserIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FilteringAndSearchPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FilteringAndSearchPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr tmp = CreateUserPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFilteringAndSearchPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("BrowsingPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("BrowsingPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr tmp = CreateUserPreferencesType_BrowsingPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetBrowsingPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RecordingPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RecordingPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr tmp = CreateUserPreferencesType_RecordingPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRecordingPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("UserIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUserIdentifierType; // FTT, check this
	}
	this->SetUserIdentifier(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FilteringAndSearchPreferences")) == 0))
  {
	Mp7JrsUserPreferencesType_FilteringAndSearchPreferences_CollectionPtr tmp = CreateUserPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
	this->SetFilteringAndSearchPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("BrowsingPreferences")) == 0))
  {
	Mp7JrsUserPreferencesType_BrowsingPreferences_CollectionPtr tmp = CreateUserPreferencesType_BrowsingPreferences_CollectionType; // FTT, check this
	this->SetBrowsingPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RecordingPreferences")) == 0))
  {
	Mp7JrsUserPreferencesType_RecordingPreferences_CollectionPtr tmp = CreateUserPreferencesType_RecordingPreferences_CollectionType; // FTT, check this
	this->SetRecordingPreferences(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetUserIdentifier() != Dc1NodePtr())
		result.Insert(GetUserIdentifier());
	if (GetFilteringAndSearchPreferences() != Dc1NodePtr())
		result.Insert(GetFilteringAndSearchPreferences());
	if (GetBrowsingPreferences() != Dc1NodePtr())
		result.Insert(GetBrowsingPreferences());
	if (GetRecordingPreferences() != Dc1NodePtr())
		result.Insert(GetRecordingPreferences());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserPreferencesType_ExtMethodImpl.h


