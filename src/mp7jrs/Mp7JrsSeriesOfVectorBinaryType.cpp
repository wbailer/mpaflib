
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtImplInclude.h


#include "Mp7JrsSeriesOfVectorType.h"
#include "Mp7JrsFloatMatrixType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsScalableSeriesType_Scaling_CollectionType.h"
#include "Mp7JrsSeriesOfVectorBinaryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsScalableSeriesType_Scaling_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Scaling

#include <assert.h>
IMp7JrsSeriesOfVectorBinaryType::IMp7JrsSeriesOfVectorBinaryType()
{

// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtPropInit.h

}

IMp7JrsSeriesOfVectorBinaryType::~IMp7JrsSeriesOfVectorBinaryType()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtPropCleanup.h

}

Mp7JrsSeriesOfVectorBinaryType::Mp7JrsSeriesOfVectorBinaryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSeriesOfVectorBinaryType::~Mp7JrsSeriesOfVectorBinaryType()
{
	Cleanup();
}

void Mp7JrsSeriesOfVectorBinaryType::Init()
{
	// Init base
	m_Base = CreateSeriesOfVectorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_VarianceScalewise = Mp7JrsFloatMatrixPtr(); // Class
	m_VarianceScalewise_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMyPropInit.h

}

void Mp7JrsSeriesOfVectorBinaryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_VarianceScalewise);
}

void Mp7JrsSeriesOfVectorBinaryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SeriesOfVectorBinaryTypePtr, since we
	// might need GetBase(), which isn't defined in ISeriesOfVectorBinaryType
	const Dc1Ptr< Mp7JrsSeriesOfVectorBinaryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSeriesOfVectorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSeriesOfVectorBinaryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidVarianceScalewise())
	{
		// Dc1Factory::DeleteObject(m_VarianceScalewise);
		this->SetVarianceScalewise(Dc1Factory::CloneObject(tmp->GetVarianceScalewise()));
	}
	else
	{
		InvalidateVarianceScalewise();
	}
}

Dc1NodePtr Mp7JrsSeriesOfVectorBinaryType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSeriesOfVectorBinaryType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSeriesOfVectorType > Mp7JrsSeriesOfVectorBinaryType::GetBase() const
{
	return m_Base;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetVarianceScalewise() const
{
		return m_VarianceScalewise;
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidVarianceScalewise() const
{
	return m_VarianceScalewise_Exist;
}

void Mp7JrsSeriesOfVectorBinaryType::SetVarianceScalewise(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetVarianceScalewise().");
	}
	if (m_VarianceScalewise != item || m_VarianceScalewise_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VarianceScalewise);
		m_VarianceScalewise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VarianceScalewise) m_VarianceScalewise->SetParent(m_myself.getPointer());
		if (m_VarianceScalewise && m_VarianceScalewise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VarianceScalewise->UseTypeAttribute = true;
		}
		m_VarianceScalewise_Exist = true;
		if(m_VarianceScalewise != Mp7JrsFloatMatrixPtr())
		{
			m_VarianceScalewise->SetContentName(XMLString::transcode("VarianceScalewise"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateVarianceScalewise()
{
	m_VarianceScalewise_Exist = false;
}
unsigned Mp7JrsSeriesOfVectorBinaryType::GetvectorSize() const
{
	return GetBase()->GetvectorSize();
}

bool Mp7JrsSeriesOfVectorBinaryType::ExistvectorSize() const
{
	return GetBase()->ExistvectorSize();
}
void Mp7JrsSeriesOfVectorBinaryType::SetvectorSize(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetvectorSize().");
	}
	GetBase()->SetvectorSize(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidatevectorSize()
{
	GetBase()->InvalidatevectorSize();
}
Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetRaw() const
{
	return GetBase()->GetRaw();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidRaw() const
{
	return GetBase()->IsValidRaw();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetMin() const
{
	return GetBase()->GetMin();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidMin() const
{
	return GetBase()->IsValidMin();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetMax() const
{
	return GetBase()->GetMax();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidMax() const
{
	return GetBase()->IsValidMax();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetMean() const
{
	return GetBase()->GetMean();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidMean() const
{
	return GetBase()->IsValidMean();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetRandom() const
{
	return GetBase()->GetRandom();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidRandom() const
{
	return GetBase()->IsValidRandom();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetFirst() const
{
	return GetBase()->GetFirst();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidFirst() const
{
	return GetBase()->IsValidFirst();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetLast() const
{
	return GetBase()->GetLast();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidLast() const
{
	return GetBase()->IsValidLast();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetVariance() const
{
	return GetBase()->GetVariance();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidVariance() const
{
	return GetBase()->IsValidVariance();
}

float Mp7JrsSeriesOfVectorBinaryType::GetLogBase() const
{
	return GetBase()->GetLogBase();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidLogBase() const
{
	return GetBase()->IsValidLogBase();
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorBinaryType::GetCovariance() const
{
	return GetBase()->GetCovariance();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidCovariance() const
{
	return GetBase()->IsValidCovariance();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorBinaryType::GetVarianceSummed() const
{
	return GetBase()->GetVarianceSummed();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidVarianceSummed() const
{
	return GetBase()->IsValidVarianceSummed();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorBinaryType::GetMaxSqDist() const
{
	return GetBase()->GetMaxSqDist();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidMaxSqDist() const
{
	return GetBase()->IsValidMaxSqDist();
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorBinaryType::GetWeight() const
{
	return GetBase()->GetWeight();
}

// Element is optional
bool Mp7JrsSeriesOfVectorBinaryType::IsValidWeight() const
{
	return GetBase()->IsValidWeight();
}

void Mp7JrsSeriesOfVectorBinaryType::SetRaw(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetRaw().");
	}
	GetBase()->SetRaw(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateRaw()
{
	GetBase()->InvalidateRaw();
}
void Mp7JrsSeriesOfVectorBinaryType::SetMin(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetMin().");
	}
	GetBase()->SetMin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateMin()
{
	GetBase()->InvalidateMin();
}
void Mp7JrsSeriesOfVectorBinaryType::SetMax(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetMax().");
	}
	GetBase()->SetMax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateMax()
{
	GetBase()->InvalidateMax();
}
void Mp7JrsSeriesOfVectorBinaryType::SetMean(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetMean().");
	}
	GetBase()->SetMean(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateMean()
{
	GetBase()->InvalidateMean();
}
void Mp7JrsSeriesOfVectorBinaryType::SetRandom(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetRandom().");
	}
	GetBase()->SetRandom(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateRandom()
{
	GetBase()->InvalidateRandom();
}
void Mp7JrsSeriesOfVectorBinaryType::SetFirst(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetFirst().");
	}
	GetBase()->SetFirst(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateFirst()
{
	GetBase()->InvalidateFirst();
}
void Mp7JrsSeriesOfVectorBinaryType::SetLast(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetLast().");
	}
	GetBase()->SetLast(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateLast()
{
	GetBase()->InvalidateLast();
}
void Mp7JrsSeriesOfVectorBinaryType::SetVariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetVariance().");
	}
	GetBase()->SetVariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateVariance()
{
	GetBase()->InvalidateVariance();
}
void Mp7JrsSeriesOfVectorBinaryType::SetLogBase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetLogBase().");
	}
	GetBase()->SetLogBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateLogBase()
{
	GetBase()->InvalidateLogBase();
}
void Mp7JrsSeriesOfVectorBinaryType::SetCovariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetCovariance().");
	}
	GetBase()->SetCovariance(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateCovariance()
{
	GetBase()->InvalidateCovariance();
}
void Mp7JrsSeriesOfVectorBinaryType::SetVarianceSummed(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetVarianceSummed().");
	}
	GetBase()->SetVarianceSummed(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateVarianceSummed()
{
	GetBase()->InvalidateVarianceSummed();
}
void Mp7JrsSeriesOfVectorBinaryType::SetMaxSqDist(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetMaxSqDist().");
	}
	GetBase()->SetMaxSqDist(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateMaxSqDist()
{
	GetBase()->InvalidateMaxSqDist();
}
void Mp7JrsSeriesOfVectorBinaryType::SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetWeight().");
	}
	GetBase()->SetWeight(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorBinaryType::InvalidateWeight()
{
	GetBase()->InvalidateWeight();
}
unsigned Mp7JrsSeriesOfVectorBinaryType::GettotalNumOfSamples() const
{
	return GetBase()->GetBase()->GettotalNumOfSamples();
}

void Mp7JrsSeriesOfVectorBinaryType::SettotalNumOfSamples(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SettotalNumOfSamples().");
	}
	GetBase()->GetBase()->SettotalNumOfSamples(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableSeriesType_Scaling_CollectionPtr Mp7JrsSeriesOfVectorBinaryType::GetScaling() const
{
	return GetBase()->GetBase()->GetScaling();
}

void Mp7JrsSeriesOfVectorBinaryType::SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorBinaryType::SetScaling().");
	}
	GetBase()->GetBase()->SetScaling(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSeriesOfVectorBinaryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VarianceScalewise")) == 0)
	{
		// VarianceScalewise is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVarianceScalewise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVarianceScalewise(p, client);
					if((p = GetVarianceScalewise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SeriesOfVectorBinaryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SeriesOfVectorBinaryType");
	}
	return result;
}

XMLCh * Mp7JrsSeriesOfVectorBinaryType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSeriesOfVectorBinaryType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSeriesOfVectorBinaryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SeriesOfVectorBinaryType"));
	// Element serialization:
	// Class
	
	if (m_VarianceScalewise != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VarianceScalewise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VarianceScalewise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VarianceScalewise->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSeriesOfVectorBinaryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSeriesOfVectorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VarianceScalewise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VarianceScalewise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVarianceScalewise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSeriesOfVectorBinaryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("VarianceScalewise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetVarianceScalewise(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSeriesOfVectorBinaryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVarianceScalewise() != Dc1NodePtr())
		result.Insert(GetVarianceScalewise());
	if (GetRaw() != Dc1NodePtr())
		result.Insert(GetRaw());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetRandom() != Dc1NodePtr())
		result.Insert(GetRandom());
	if (GetFirst() != Dc1NodePtr())
		result.Insert(GetFirst());
	if (GetLast() != Dc1NodePtr())
		result.Insert(GetLast());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetCovariance() != Dc1NodePtr())
		result.Insert(GetCovariance());
	if (GetVarianceSummed() != Dc1NodePtr())
		result.Insert(GetVarianceSummed());
	if (GetMaxSqDist() != Dc1NodePtr())
		result.Insert(GetMaxSqDist());
	if (GetWeight() != Dc1NodePtr())
		result.Insert(GetWeight());
	if (GetScaling() != Dc1NodePtr())
		result.Insert(GetScaling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorBinaryType_ExtMethodImpl.h


