
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsTextSegmentType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsTextDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsTextDecompositionType_LocalType::IMp7JrsTextDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsTextDecompositionType_LocalType::~IMp7JrsTextDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsTextDecompositionType_LocalType::Mp7JrsTextDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextDecompositionType_LocalType::~Mp7JrsTextDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsTextDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TextSegment = Mp7JrsTextSegmentPtr(); // Class
	m_TextSegmentRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsTextDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TextSegment);
	// Dc1Factory::DeleteObject(m_TextSegmentRef);
}

void Mp7JrsTextDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ITextDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsTextDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_TextSegment);
		this->SetTextSegment(Dc1Factory::CloneObject(tmp->GetTextSegment()));
		// Dc1Factory::DeleteObject(m_TextSegmentRef);
		this->SetTextSegmentRef(Dc1Factory::CloneObject(tmp->GetTextSegmentRef()));
}

Dc1NodePtr Mp7JrsTextDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTextDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTextSegmentPtr Mp7JrsTextDecompositionType_LocalType::GetTextSegment() const
{
		return m_TextSegment;
}

Mp7JrsReferencePtr Mp7JrsTextDecompositionType_LocalType::GetTextSegmentRef() const
{
		return m_TextSegmentRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsTextDecompositionType_LocalType::SetTextSegment(const Mp7JrsTextSegmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType_LocalType::SetTextSegment().");
	}
	if (m_TextSegment != item)
	{
		// Dc1Factory::DeleteObject(m_TextSegment);
		m_TextSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextSegment) m_TextSegment->SetParent(m_myself.getPointer());
		if (m_TextSegment && m_TextSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextSegmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TextSegment->UseTypeAttribute = true;
		}
		if(m_TextSegment != Mp7JrsTextSegmentPtr())
		{
			m_TextSegment->SetContentName(XMLString::transcode("TextSegment"));
		}
	// Dc1Factory::DeleteObject(m_TextSegmentRef);
	m_TextSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTextDecompositionType_LocalType::SetTextSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextDecompositionType_LocalType::SetTextSegmentRef().");
	}
	if (m_TextSegmentRef != item)
	{
		// Dc1Factory::DeleteObject(m_TextSegmentRef);
		m_TextSegmentRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextSegmentRef) m_TextSegmentRef->SetParent(m_myself.getPointer());
		if (m_TextSegmentRef && m_TextSegmentRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TextSegmentRef->UseTypeAttribute = true;
		}
		if(m_TextSegmentRef != Mp7JrsReferencePtr())
		{
			m_TextSegmentRef->SetContentName(XMLString::transcode("TextSegmentRef"));
		}
	// Dc1Factory::DeleteObject(m_TextSegment);
	m_TextSegment = Mp7JrsTextSegmentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: TextDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsTextDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsTextDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTextDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTextDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_TextSegment != Mp7JrsTextSegmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TextSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TextSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TextSegment->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TextSegmentRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TextSegmentRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TextSegmentRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TextSegmentRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTextDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TextSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TextSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextSegmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTextSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TextSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TextSegmentRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTextSegmentRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TextSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextSegmentType; // FTT, check this
	}
	this->SetTextSegment(child);
  }
  if (XMLString::compareString(elementname, X("TextSegmentRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetTextSegmentRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTextSegment() != Dc1NodePtr())
		result.Insert(GetTextSegment());
	if (GetTextSegmentRef() != Dc1NodePtr())
		result.Insert(GetTextSegmentRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextDecompositionType_LocalType_ExtMethodImpl.h


