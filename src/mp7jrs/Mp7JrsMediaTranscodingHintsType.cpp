
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType.h"
#include "Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalType.h"
#include "Mp7JrsMediaTranscodingHintsType_CodingHints_LocalType.h"
#include "Mp7JrsMediaTranscodingHintsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaTranscodingHintsType::IMp7JrsMediaTranscodingHintsType()
{

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtPropInit.h

}

IMp7JrsMediaTranscodingHintsType::~IMp7JrsMediaTranscodingHintsType()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtPropCleanup.h

}

Mp7JrsMediaTranscodingHintsType::Mp7JrsMediaTranscodingHintsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaTranscodingHintsType::~Mp7JrsMediaTranscodingHintsType()
{
	Cleanup();
}

void Mp7JrsMediaTranscodingHintsType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_difficulty = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_difficulty->SetContent(0.0); // Use Value initialiser (xxx2)
	m_difficulty_Exist = false;
	m_importance = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_importance->SetContent(0.0); // Use Value initialiser (xxx2)
	m_importance_Exist = false;
	m_spatialResolutionHint = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_spatialResolutionHint->SetContent(0.0); // Use Value initialiser (xxx2)
	m_spatialResolutionHint_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_MotionHint = Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr(); // Class
	m_MotionHint_Exist = false;
	m_ShapeHint = Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr(); // Class
	m_ShapeHint_Exist = false;
	m_CodingHints = Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr(); // Class
	m_CodingHints_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtMyPropInit.h

}

void Mp7JrsMediaTranscodingHintsType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_difficulty);
	// Dc1Factory::DeleteObject(m_importance);
	// Dc1Factory::DeleteObject(m_spatialResolutionHint);
	// Dc1Factory::DeleteObject(m_MotionHint);
	// Dc1Factory::DeleteObject(m_ShapeHint);
	// Dc1Factory::DeleteObject(m_CodingHints);
}

void Mp7JrsMediaTranscodingHintsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaTranscodingHintsTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaTranscodingHintsType
	const Dc1Ptr< Mp7JrsMediaTranscodingHintsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaTranscodingHintsType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_difficulty);
	if (tmp->Existdifficulty())
	{
		this->Setdifficulty(Dc1Factory::CloneObject(tmp->Getdifficulty()));
	}
	else
	{
		Invalidatedifficulty();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_importance);
	if (tmp->Existimportance())
	{
		this->Setimportance(Dc1Factory::CloneObject(tmp->Getimportance()));
	}
	else
	{
		Invalidateimportance();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_spatialResolutionHint);
	if (tmp->ExistspatialResolutionHint())
	{
		this->SetspatialResolutionHint(Dc1Factory::CloneObject(tmp->GetspatialResolutionHint()));
	}
	else
	{
		InvalidatespatialResolutionHint();
	}
	}
	if (tmp->IsValidMotionHint())
	{
		// Dc1Factory::DeleteObject(m_MotionHint);
		this->SetMotionHint(Dc1Factory::CloneObject(tmp->GetMotionHint()));
	}
	else
	{
		InvalidateMotionHint();
	}
	if (tmp->IsValidShapeHint())
	{
		// Dc1Factory::DeleteObject(m_ShapeHint);
		this->SetShapeHint(Dc1Factory::CloneObject(tmp->GetShapeHint()));
	}
	else
	{
		InvalidateShapeHint();
	}
	if (tmp->IsValidCodingHints())
	{
		// Dc1Factory::DeleteObject(m_CodingHints);
		this->SetCodingHints(Dc1Factory::CloneObject(tmp->GetCodingHints()));
	}
	else
	{
		InvalidateCodingHints();
	}
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaTranscodingHintsType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsMediaTranscodingHintsType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsMediaTranscodingHintsType::Getdifficulty() const
{
	return m_difficulty;
}

bool Mp7JrsMediaTranscodingHintsType::Existdifficulty() const
{
	return m_difficulty_Exist;
}
void Mp7JrsMediaTranscodingHintsType::Setdifficulty(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::Setdifficulty().");
	}
	m_difficulty = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_difficulty) m_difficulty->SetParent(m_myself.getPointer());
	m_difficulty_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::Invalidatedifficulty()
{
	m_difficulty_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsMediaTranscodingHintsType::Getimportance() const
{
	return m_importance;
}

bool Mp7JrsMediaTranscodingHintsType::Existimportance() const
{
	return m_importance_Exist;
}
void Mp7JrsMediaTranscodingHintsType::Setimportance(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::Setimportance().");
	}
	m_importance = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_importance) m_importance->SetParent(m_myself.getPointer());
	m_importance_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::Invalidateimportance()
{
	m_importance_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsMediaTranscodingHintsType::GetspatialResolutionHint() const
{
	return m_spatialResolutionHint;
}

bool Mp7JrsMediaTranscodingHintsType::ExistspatialResolutionHint() const
{
	return m_spatialResolutionHint_Exist;
}
void Mp7JrsMediaTranscodingHintsType::SetspatialResolutionHint(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::SetspatialResolutionHint().");
	}
	m_spatialResolutionHint = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_spatialResolutionHint) m_spatialResolutionHint->SetParent(m_myself.getPointer());
	m_spatialResolutionHint_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::InvalidatespatialResolutionHint()
{
	m_spatialResolutionHint_Exist = false;
}
Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr Mp7JrsMediaTranscodingHintsType::GetMotionHint() const
{
		return m_MotionHint;
}

// Element is optional
bool Mp7JrsMediaTranscodingHintsType::IsValidMotionHint() const
{
	return m_MotionHint_Exist;
}

Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr Mp7JrsMediaTranscodingHintsType::GetShapeHint() const
{
		return m_ShapeHint;
}

// Element is optional
bool Mp7JrsMediaTranscodingHintsType::IsValidShapeHint() const
{
	return m_ShapeHint_Exist;
}

Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr Mp7JrsMediaTranscodingHintsType::GetCodingHints() const
{
		return m_CodingHints;
}

// Element is optional
bool Mp7JrsMediaTranscodingHintsType::IsValidCodingHints() const
{
	return m_CodingHints_Exist;
}

void Mp7JrsMediaTranscodingHintsType::SetMotionHint(const Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::SetMotionHint().");
	}
	if (m_MotionHint != item || m_MotionHint_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MotionHint);
		m_MotionHint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionHint) m_MotionHint->SetParent(m_myself.getPointer());
		if (m_MotionHint && m_MotionHint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MotionHint->UseTypeAttribute = true;
		}
		m_MotionHint_Exist = true;
		if(m_MotionHint != Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr())
		{
			m_MotionHint->SetContentName(XMLString::transcode("MotionHint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::InvalidateMotionHint()
{
	m_MotionHint_Exist = false;
}
void Mp7JrsMediaTranscodingHintsType::SetShapeHint(const Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::SetShapeHint().");
	}
	if (m_ShapeHint != item || m_ShapeHint_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ShapeHint);
		m_ShapeHint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ShapeHint) m_ShapeHint->SetParent(m_myself.getPointer());
		if (m_ShapeHint && m_ShapeHint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_ShapeHint_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ShapeHint->UseTypeAttribute = true;
		}
		m_ShapeHint_Exist = true;
		if(m_ShapeHint != Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr())
		{
			m_ShapeHint->SetContentName(XMLString::transcode("ShapeHint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::InvalidateShapeHint()
{
	m_ShapeHint_Exist = false;
}
void Mp7JrsMediaTranscodingHintsType::SetCodingHints(const Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType::SetCodingHints().");
	}
	if (m_CodingHints != item || m_CodingHints_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CodingHints);
		m_CodingHints = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CodingHints) m_CodingHints->SetParent(m_myself.getPointer());
		if (m_CodingHints && m_CodingHints->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_CodingHints_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CodingHints->UseTypeAttribute = true;
		}
		m_CodingHints_Exist = true;
		if(m_CodingHints != Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr())
		{
			m_CodingHints->SetContentName(XMLString::transcode("CodingHints"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType::InvalidateCodingHints()
{
	m_CodingHints_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaTranscodingHintsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("difficulty")) == 0)
	{
		// difficulty is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("importance")) == 0)
	{
		// importance is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("spatialResolutionHint")) == 0)
	{
		// spatialResolutionHint is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MotionHint")) == 0)
	{
		// MotionHint is simple element MediaTranscodingHintsType_MotionHint_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMotionHint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMotionHint(p, client);
					if((p = GetMotionHint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ShapeHint")) == 0)
	{
		// ShapeHint is simple element MediaTranscodingHintsType_ShapeHint_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShapeHint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_ShapeHint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShapeHint(p, client);
					if((p = GetShapeHint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CodingHints")) == 0)
	{
		// CodingHints is simple element MediaTranscodingHintsType_CodingHints_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCodingHints()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_CodingHints_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCodingHints(p, client);
					if((p = GetCodingHints()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaTranscodingHintsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaTranscodingHintsType");
	}
	return result;
}

XMLCh * Mp7JrsMediaTranscodingHintsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaTranscodingHintsType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaTranscodingHintsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaTranscodingHintsType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_difficulty_Exist)
	{
	// Class
	if (m_difficulty != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_difficulty->ToText();
		element->setAttributeNS(X(""), X("difficulty"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_importance_Exist)
	{
	// Class
	if (m_importance != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_importance->ToText();
		element->setAttributeNS(X(""), X("importance"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_spatialResolutionHint_Exist)
	{
	// Class
	if (m_spatialResolutionHint != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_spatialResolutionHint->ToText();
		element->setAttributeNS(X(""), X("spatialResolutionHint"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_MotionHint != Mp7JrsMediaTranscodingHintsType_MotionHint_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MotionHint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MotionHint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MotionHint->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ShapeHint != Mp7JrsMediaTranscodingHintsType_ShapeHint_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ShapeHint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ShapeHint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ShapeHint->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CodingHints != Mp7JrsMediaTranscodingHintsType_CodingHints_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CodingHints->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CodingHints"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CodingHints->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaTranscodingHintsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("difficulty")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("difficulty")));
		this->Setdifficulty(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("importance")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("importance")));
		this->Setimportance(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("spatialResolutionHint")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("spatialResolutionHint")));
		this->SetspatialResolutionHint(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MotionHint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MotionHint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTranscodingHintsType_MotionHint_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMotionHint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ShapeHint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ShapeHint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTranscodingHintsType_ShapeHint_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShapeHint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CodingHints"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CodingHints")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTranscodingHintsType_CodingHints_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCodingHints(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("MotionHint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTranscodingHintsType_MotionHint_LocalType; // FTT, check this
	}
	this->SetMotionHint(child);
  }
  if (XMLString::compareString(elementname, X("ShapeHint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTranscodingHintsType_ShapeHint_LocalType; // FTT, check this
	}
	this->SetShapeHint(child);
  }
  if (XMLString::compareString(elementname, X("CodingHints")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTranscodingHintsType_CodingHints_LocalType; // FTT, check this
	}
	this->SetCodingHints(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaTranscodingHintsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMotionHint() != Dc1NodePtr())
		result.Insert(GetMotionHint());
	if (GetShapeHint() != Dc1NodePtr())
		result.Insert(GetShapeHint());
	if (GetCodingHints() != Dc1NodePtr())
		result.Insert(GetCodingHints());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_ExtMethodImpl.h


