
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtImplInclude.h


#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtPropInit.h

}

IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::~IMp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtPropCleanup.h

}

Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::~Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType()
{
	Cleanup();
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Init()
{

	// Init attributes
	m_index = NULL; // String
	m_index_Exist = false;
	m_frequency = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_frequency->SetContent(0.0); // Use Value initialiser (xxx2)
	m_frequency_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Text = Mp7JrsTermUsePtr(); // Class
	m_Text_Exist = false;
	m_InkSegmentRef = Mp7JrsReferencePtr(); // Class
	m_InkSegmentRef_Exist = false;


// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtMyPropInit.h

}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_index); // String
	// Dc1Factory::DeleteObject(m_frequency);
	// Dc1Factory::DeleteObject(m_Text);
	// Dc1Factory::DeleteObject(m_InkSegmentRef);
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HandWritingRecogInformationType_InkLexicon_Entry_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IHandWritingRecogInformationType_InkLexicon_Entry_LocalType
	const Dc1Ptr< Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_index); // String
	if (tmp->Existindex())
	{
		this->Setindex(XMLString::replicate(tmp->Getindex()));
	}
	else
	{
		Invalidateindex();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_frequency);
	if (tmp->Existfrequency())
	{
		this->Setfrequency(Dc1Factory::CloneObject(tmp->Getfrequency()));
	}
	else
	{
		Invalidatefrequency();
	}
	}
	if (tmp->IsValidText())
	{
		// Dc1Factory::DeleteObject(m_Text);
		this->SetText(Dc1Factory::CloneObject(tmp->GetText()));
	}
	else
	{
		InvalidateText();
	}
	if (tmp->IsValidInkSegmentRef())
	{
		// Dc1Factory::DeleteObject(m_InkSegmentRef);
		this->SetInkSegmentRef(Dc1Factory::CloneObject(tmp->GetInkSegmentRef()));
	}
	else
	{
		InvalidateInkSegmentRef();
	}
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Getindex() const
{
	return m_index;
}

bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Existindex() const
{
	return m_index_Exist;
}
void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Setindex(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Setindex().");
	}
	m_index = item;
	m_index_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Invalidateindex()
{
	m_index_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Getfrequency() const
{
	return m_frequency;
}

bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Existfrequency() const
{
	return m_frequency_Exist;
}
void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Setfrequency(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Setfrequency().");
	}
	m_frequency = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_frequency) m_frequency->SetParent(m_myself.getPointer());
	m_frequency_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Invalidatefrequency()
{
	m_frequency_Exist = false;
}
Mp7JrsTermUsePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetText() const
{
		return m_Text;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::IsValidText() const
{
	return m_Text_Exist;
}

Mp7JrsReferencePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetInkSegmentRef() const
{
		return m_InkSegmentRef;
}

// Element is optional
bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::IsValidInkSegmentRef() const
{
	return m_InkSegmentRef_Exist;
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::SetText(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::SetText().");
	}
	if (m_Text != item || m_Text_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Text);
		m_Text = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Text) m_Text->SetParent(m_myself.getPointer());
		if (m_Text && m_Text->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Text->UseTypeAttribute = true;
		}
		m_Text_Exist = true;
		if(m_Text != Mp7JrsTermUsePtr())
		{
			m_Text->SetContentName(XMLString::transcode("Text"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::InvalidateText()
{
	m_Text_Exist = false;
}
void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::SetInkSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::SetInkSegmentRef().");
	}
	if (m_InkSegmentRef != item || m_InkSegmentRef_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_InkSegmentRef);
		m_InkSegmentRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InkSegmentRef) m_InkSegmentRef->SetParent(m_myself.getPointer());
		if (m_InkSegmentRef && m_InkSegmentRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InkSegmentRef->UseTypeAttribute = true;
		}
		m_InkSegmentRef_Exist = true;
		if(m_InkSegmentRef != Mp7JrsReferencePtr())
		{
			m_InkSegmentRef->SetContentName(XMLString::transcode("InkSegmentRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::InvalidateInkSegmentRef()
{
	m_InkSegmentRef_Exist = false;
}

Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("index")) == 0)
	{
		// index is simple attribute ID
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("frequency")) == 0)
	{
		// frequency is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Text")) == 0)
	{
		// Text is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetText()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetText(p, client);
					if((p = GetText()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("InkSegmentRef")) == 0)
	{
		// InkSegmentRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInkSegmentRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInkSegmentRef(p, client);
					if((p = GetInkSegmentRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HandWritingRecogInformationType_InkLexicon_Entry_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HandWritingRecogInformationType_InkLexicon_Entry_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogInformationType_InkLexicon_Entry_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_index_Exist)
	{
	// String
	if(m_index != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("index"), m_index);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_frequency_Exist)
	{
	// Class
	if (m_frequency != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_frequency->ToText();
		element->setAttributeNS(X(""), X("frequency"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Text != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Text->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Text"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Text->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_InkSegmentRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InkSegmentRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InkSegmentRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InkSegmentRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("index")))
	{
		// Deserialize string type
		this->Setindex(Dc1Convert::TextToString(parent->getAttribute(X("index"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("frequency")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("frequency")));
		this->Setfrequency(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Text"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Text")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetText(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InkSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InkSegmentRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInkSegmentRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Text")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetText(child);
  }
  if (XMLString::compareString(elementname, X("InkSegmentRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetInkSegmentRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetText() != Dc1NodePtr())
		result.Insert(GetText());
	if (GetInkSegmentRef() != Dc1NodePtr())
		result.Insert(GetInkSegmentRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogInformationType_InkLexicon_Entry_LocalType_ExtMethodImpl.h


