
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtImplInclude.h


#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsLinguisticEntityType_start_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsLinguisticEntityType_start_LocalType::IMp7JrsLinguisticEntityType_start_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtPropInit.h

}

IMp7JrsLinguisticEntityType_start_LocalType::~IMp7JrsLinguisticEntityType_start_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtPropCleanup.h

}

Mp7JrsLinguisticEntityType_start_LocalType::Mp7JrsLinguisticEntityType_start_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsLinguisticEntityType_start_LocalType::~Mp7JrsLinguisticEntityType_start_LocalType()
{
	Cleanup();
}

void Mp7JrsLinguisticEntityType_start_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_nonNegativeInteger = (0); // Value
	m_nonNegativeInteger_Valid = false; // Union flag for value

	m_mediaTimePointType = Mp7JrsmediaTimePointPtr(); // Pattern


// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtMyPropInit.h

}

void Mp7JrsLinguisticEntityType_start_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtMyPropCleanup.h


	// m_nonNegativeInteger_Valid = false; // Flag for union values		
	// Dc1Factory::DeleteObject(m_mediaTimePointType);
}

void Mp7JrsLinguisticEntityType_start_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use LinguisticEntityType_start_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ILinguisticEntityType_start_LocalType
	const Dc1Ptr< Mp7JrsLinguisticEntityType_start_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	{
		if(tmp->m_nonNegativeInteger_Valid)
		{
			this->SetnonNegativeInteger(tmp->GetnonNegativeInteger());
			return; // Got everything we need
		}
		else
		{
			this->m_nonNegativeInteger_Valid = false;
		}
 	}
	if(tmp->GetmediaTimePointType() != Mp7JrsmediaTimePointPtr())
	{
		this->SetmediaTimePointType(Dc1Factory::CloneObject(tmp->GetmediaTimePointType()));
		return;
	}
	}
}

Dc1NodePtr Mp7JrsLinguisticEntityType_start_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsLinguisticEntityType_start_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


unsigned Mp7JrsLinguisticEntityType_start_LocalType::GetnonNegativeInteger() const
{
		return m_nonNegativeInteger;
}

// Value is part of a union
bool Mp7JrsLinguisticEntityType_start_LocalType::IsValidnonNegativeInteger() const
{
	return m_nonNegativeInteger_Valid;
}

Mp7JrsmediaTimePointPtr Mp7JrsLinguisticEntityType_start_LocalType::GetmediaTimePointType() const
{
		return m_mediaTimePointType;
}

void Mp7JrsLinguisticEntityType_start_LocalType::SetnonNegativeInteger(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType_start_LocalType::SetnonNegativeInteger().");
	}
	if (m_nonNegativeInteger != item)
	{
		m_nonNegativeInteger = item;
		m_nonNegativeInteger_Valid = true;
	// Dc1Factory::DeleteObject(m_mediaTimePointType);
	m_mediaTimePointType = Mp7JrsmediaTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsLinguisticEntityType_start_LocalType::SetmediaTimePointType(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsLinguisticEntityType_start_LocalType::SetmediaTimePointType().");
	}
	if (m_mediaTimePointType != item)
	{
		// Dc1Factory::DeleteObject(m_mediaTimePointType);
		m_mediaTimePointType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_mediaTimePointType) m_mediaTimePointType->SetParent(m_myself.getPointer());
		if (m_mediaTimePointType && m_mediaTimePointType->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_mediaTimePointType->UseTypeAttribute = true;
		}
		if(m_mediaTimePointType != Mp7JrsmediaTimePointPtr())
		{
			m_mediaTimePointType->SetContentName(XMLString::transcode("mediaTimePointType"));
		}
	
	m_nonNegativeInteger_Valid = false;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsLinguisticEntityType_start_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("mediaTimePointType")) == 0)
	{
		// mediaTimePointType is simple element mediaTimePointType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetmediaTimePointType()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaTimePointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetmediaTimePointType(p, client);
					if((p = GetmediaTimePointType()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for LinguisticEntityType_start_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "LinguisticEntityType_start_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsLinguisticEntityType_start_LocalType::ToText() const
{
	if(m_nonNegativeInteger != 0)
	{
		return Dc1Convert::BinToText(m_nonNegativeInteger);
	}
	if(m_mediaTimePointType != Mp7JrsmediaTimePointPtr())
	{
		return m_mediaTimePointType->ToText();
	}
	return XMLString::transcode("");
}


bool Mp7JrsLinguisticEntityType_start_LocalType::Parse(const XMLCh * const txt)
{
										
	// Parse stu value type "nonNegativeInteger"
	{
		
		unsigned tmp = Dc1Convert::TextToUnsignedInt(txt);
		if(tmp > 0)
		{
			this->SetnonNegativeInteger(tmp);
			return true;
		}
	}
	// Parse stu pattern type
	{
		Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
		if(tmp->Parse(txt))
		{
			this->SetmediaTimePointType(tmp);
			return true;
		}
		Dc1Factory::DeleteObject(tmp);
	}
	return false;
}

bool Mp7JrsLinguisticEntityType_start_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsLinguisticEntityType_start_LocalType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsLinguisticEntityType_start_LocalType::ContentToString() const
{
	// Mp7JrsLinguisticEntityType_start_LocalType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsLinguisticEntityType_start_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("LinguisticEntityType_start_LocalType"));
	// Element serialization:
	if(m_nonNegativeInteger_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_nonNegativeInteger);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("nonNegativeInteger"), false);
		XMLString::release(&tmp);
	}
	if(m_mediaTimePointType != Mp7JrsmediaTimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("mediaTimePointType");
//		m_mediaTimePointType->SetContentName(contentname);
		m_mediaTimePointType->UseTypeAttribute = this->UseTypeAttribute;
		m_mediaTimePointType->Serialize(doc, element);
	}

}

bool Mp7JrsLinguisticEntityType_start_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("nonNegativeInteger"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetnonNegativeInteger(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("mediaTimePointType"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("mediaTimePointType")) == 0))
		{
			Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetmediaTimePointType(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsLinguisticEntityType_start_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("mediaTimePointType")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaTimePointType; // FTT, check this
	}
	this->SetmediaTimePointType(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsLinguisticEntityType_start_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetmediaTimePointType() != Dc1NodePtr())
		result.Insert(GetmediaTimePointType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsLinguisticEntityType_start_LocalType_ExtMethodImpl.h


