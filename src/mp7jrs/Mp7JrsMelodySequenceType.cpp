
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtImplInclude.h


#include "Mp7JrsAudioDSType.h"
#include "Mp7JrsMelodySequenceType_StartingNote_LocalType.h"
#include "Mp7JrsMelodySequenceType_NoteArray_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMelodySequenceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMelodySequenceType_NoteArray_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:NoteArray

#include <assert.h>
IMp7JrsMelodySequenceType::IMp7JrsMelodySequenceType()
{

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtPropInit.h

}

IMp7JrsMelodySequenceType::~IMp7JrsMelodySequenceType()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtPropCleanup.h

}

Mp7JrsMelodySequenceType::Mp7JrsMelodySequenceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMelodySequenceType::~Mp7JrsMelodySequenceType()
{
	Cleanup();
}

void Mp7JrsMelodySequenceType::Init()
{
	// Init base
	m_Base = CreateAudioDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_StartingNote = Mp7JrsMelodySequenceType_StartingNote_LocalPtr(); // Class
	m_StartingNote_Exist = false;
	m_NoteArray = Mp7JrsMelodySequenceType_NoteArray_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtMyPropInit.h

}

void Mp7JrsMelodySequenceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_StartingNote);
	// Dc1Factory::DeleteObject(m_NoteArray);
}

void Mp7JrsMelodySequenceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MelodySequenceTypePtr, since we
	// might need GetBase(), which isn't defined in IMelodySequenceType
	const Dc1Ptr< Mp7JrsMelodySequenceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMelodySequenceType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidStartingNote())
	{
		// Dc1Factory::DeleteObject(m_StartingNote);
		this->SetStartingNote(Dc1Factory::CloneObject(tmp->GetStartingNote()));
	}
	else
	{
		InvalidateStartingNote();
	}
		// Dc1Factory::DeleteObject(m_NoteArray);
		this->SetNoteArray(Dc1Factory::CloneObject(tmp->GetNoteArray()));
}

Dc1NodePtr Mp7JrsMelodySequenceType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMelodySequenceType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDSType > Mp7JrsMelodySequenceType::GetBase() const
{
	return m_Base;
}

Mp7JrsMelodySequenceType_StartingNote_LocalPtr Mp7JrsMelodySequenceType::GetStartingNote() const
{
		return m_StartingNote;
}

// Element is optional
bool Mp7JrsMelodySequenceType::IsValidStartingNote() const
{
	return m_StartingNote_Exist;
}

Mp7JrsMelodySequenceType_NoteArray_CollectionPtr Mp7JrsMelodySequenceType::GetNoteArray() const
{
		return m_NoteArray;
}

void Mp7JrsMelodySequenceType::SetStartingNote(const Mp7JrsMelodySequenceType_StartingNote_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SetStartingNote().");
	}
	if (m_StartingNote != item || m_StartingNote_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_StartingNote);
		m_StartingNote = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StartingNote) m_StartingNote->SetParent(m_myself.getPointer());
		if (m_StartingNote && m_StartingNote->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StartingNote->UseTypeAttribute = true;
		}
		m_StartingNote_Exist = true;
		if(m_StartingNote != Mp7JrsMelodySequenceType_StartingNote_LocalPtr())
		{
			m_StartingNote->SetContentName(XMLString::transcode("StartingNote"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::InvalidateStartingNote()
{
	m_StartingNote_Exist = false;
}
void Mp7JrsMelodySequenceType::SetNoteArray(const Mp7JrsMelodySequenceType_NoteArray_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SetNoteArray().");
	}
	if (m_NoteArray != item)
	{
		// Dc1Factory::DeleteObject(m_NoteArray);
		m_NoteArray = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_NoteArray) m_NoteArray->SetParent(m_myself.getPointer());
		if(m_NoteArray != Mp7JrsMelodySequenceType_NoteArray_CollectionPtr())
		{
			m_NoteArray->SetContentName(XMLString::transcode("NoteArray"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsMelodySequenceType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsMelodySequenceType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsMelodySequenceType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsMelodySequenceType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsMelodySequenceType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsMelodySequenceType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMelodySequenceType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMelodySequenceType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMelodySequenceType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMelodySequenceType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMelodySequenceType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMelodySequenceType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMelodySequenceType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMelodySequenceType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMelodySequenceType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMelodySequenceType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMelodySequenceType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMelodySequenceType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMelodySequenceType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMelodySequenceType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMelodySequenceType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMelodySequenceType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMelodySequenceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("StartingNote")) == 0)
	{
		// StartingNote is simple element MelodySequenceType_StartingNote_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStartingNote()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_StartingNote_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMelodySequenceType_StartingNote_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStartingNote(p, client);
					if((p = GetStartingNote()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NoteArray")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:NoteArray is item of type MelodySequenceType_NoteArray_LocalType
		// in element collection MelodySequenceType_NoteArray_CollectionType
		
		context->Found = true;
		Mp7JrsMelodySequenceType_NoteArray_CollectionPtr coll = GetNoteArray();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMelodySequenceType_NoteArray_CollectionType; // FTT, check this
				SetNoteArray(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MelodySequenceType_NoteArray_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMelodySequenceType_NoteArray_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MelodySequenceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MelodySequenceType");
	}
	return result;
}

XMLCh * Mp7JrsMelodySequenceType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMelodySequenceType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMelodySequenceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MelodySequenceType"));
	// Element serialization:
	// Class
	
	if (m_StartingNote != Mp7JrsMelodySequenceType_StartingNote_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StartingNote->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StartingNote"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StartingNote->Serialize(doc, element, &elem);
	}
	if (m_NoteArray != Mp7JrsMelodySequenceType_NoteArray_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_NoteArray->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMelodySequenceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StartingNote"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StartingNote")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMelodySequenceType_StartingNote_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStartingNote(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("NoteArray"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("NoteArray")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMelodySequenceType_NoteArray_CollectionPtr tmp = CreateMelodySequenceType_NoteArray_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNoteArray(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMelodySequenceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("StartingNote")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMelodySequenceType_StartingNote_LocalType; // FTT, check this
	}
	this->SetStartingNote(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("NoteArray")) == 0))
  {
	Mp7JrsMelodySequenceType_NoteArray_CollectionPtr tmp = CreateMelodySequenceType_NoteArray_CollectionType; // FTT, check this
	this->SetNoteArray(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMelodySequenceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStartingNote() != Dc1NodePtr())
		result.Insert(GetStartingNote());
	if (GetNoteArray() != Dc1NodePtr())
		result.Insert(GetNoteArray());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMelodySequenceType_ExtMethodImpl.h


