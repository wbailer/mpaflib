
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsPersonType.h"
#include "Mp7JrsSpeakerInfoType_WordIndex_LocalType.h"
#include "Mp7JrsSpeakerInfoType_PhoneIndex_LocalType.h"
#include "Mp7JrsSpeakerInfoType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSpeakerInfoType::IMp7JrsSpeakerInfoType()
{

// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtPropInit.h

}

IMp7JrsSpeakerInfoType::~IMp7JrsSpeakerInfoType()
{
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtPropCleanup.h

}

Mp7JrsSpeakerInfoType::Mp7JrsSpeakerInfoType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpeakerInfoType::~Mp7JrsSpeakerInfoType()
{
	Cleanup();
}

void Mp7JrsSpeakerInfoType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_phoneLexiconRef = NULL; // String
	m_phoneLexiconRef_Exist = false;
	m_wordLexiconRef = NULL; // String
	m_wordLexiconRef_Exist = false;
	m_confusionInfoRef = NULL; // String
	m_confusionInfoRef_Exist = false;
	m_descriptionMetadataRef = NULL; // String
	m_descriptionMetadataRef_Exist = false;
	m_provenance = Mp7JrsSpeakerInfoType_provenance_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_SpokenLanguage = XMLString::transcode(""); //  Mandatory String
	m_Person = Mp7JrsPersonPtr(); // Class
	m_Person_Exist = false;
	m_WordIndex = Mp7JrsSpeakerInfoType_WordIndex_LocalPtr(); // Class
	m_WordIndex_Exist = false;
	m_PhoneIndex = Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr(); // Class
	m_PhoneIndex_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMyPropInit.h

}

void Mp7JrsSpeakerInfoType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_phoneLexiconRef); // String
	XMLString::release(&m_wordLexiconRef); // String
	XMLString::release(&m_confusionInfoRef); // String
	XMLString::release(&m_descriptionMetadataRef); // String
	XMLString::release(&this->m_SpokenLanguage);
	this->m_SpokenLanguage = NULL;
	// Dc1Factory::DeleteObject(m_Person);
	// Dc1Factory::DeleteObject(m_WordIndex);
	// Dc1Factory::DeleteObject(m_PhoneIndex);
}

void Mp7JrsSpeakerInfoType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpeakerInfoTypePtr, since we
	// might need GetBase(), which isn't defined in ISpeakerInfoType
	const Dc1Ptr< Mp7JrsSpeakerInfoType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSpeakerInfoType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_phoneLexiconRef); // String
	if (tmp->ExistphoneLexiconRef())
	{
		this->SetphoneLexiconRef(XMLString::replicate(tmp->GetphoneLexiconRef()));
	}
	else
	{
		InvalidatephoneLexiconRef();
	}
	}
	{
	XMLString::release(&m_wordLexiconRef); // String
	if (tmp->ExistwordLexiconRef())
	{
		this->SetwordLexiconRef(XMLString::replicate(tmp->GetwordLexiconRef()));
	}
	else
	{
		InvalidatewordLexiconRef();
	}
	}
	{
	XMLString::release(&m_confusionInfoRef); // String
	if (tmp->ExistconfusionInfoRef())
	{
		this->SetconfusionInfoRef(XMLString::replicate(tmp->GetconfusionInfoRef()));
	}
	else
	{
		InvalidateconfusionInfoRef();
	}
	}
	{
	XMLString::release(&m_descriptionMetadataRef); // String
	if (tmp->ExistdescriptionMetadataRef())
	{
		this->SetdescriptionMetadataRef(XMLString::replicate(tmp->GetdescriptionMetadataRef()));
	}
	else
	{
		InvalidatedescriptionMetadataRef();
	}
	}
	{
		this->Setprovenance(tmp->Getprovenance());
	}
		this->SetSpokenLanguage(XMLString::replicate(tmp->GetSpokenLanguage()));
	if (tmp->IsValidPerson())
	{
		// Dc1Factory::DeleteObject(m_Person);
		this->SetPerson(Dc1Factory::CloneObject(tmp->GetPerson()));
	}
	else
	{
		InvalidatePerson();
	}
	if (tmp->IsValidWordIndex())
	{
		// Dc1Factory::DeleteObject(m_WordIndex);
		this->SetWordIndex(Dc1Factory::CloneObject(tmp->GetWordIndex()));
	}
	else
	{
		InvalidateWordIndex();
	}
	if (tmp->IsValidPhoneIndex())
	{
		// Dc1Factory::DeleteObject(m_PhoneIndex);
		this->SetPhoneIndex(Dc1Factory::CloneObject(tmp->GetPhoneIndex()));
	}
	else
	{
		InvalidatePhoneIndex();
	}
}

Dc1NodePtr Mp7JrsSpeakerInfoType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSpeakerInfoType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsSpeakerInfoType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsSpeakerInfoType::GetphoneLexiconRef() const
{
	return m_phoneLexiconRef;
}

bool Mp7JrsSpeakerInfoType::ExistphoneLexiconRef() const
{
	return m_phoneLexiconRef_Exist;
}
void Mp7JrsSpeakerInfoType::SetphoneLexiconRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetphoneLexiconRef().");
	}
	m_phoneLexiconRef = item;
	m_phoneLexiconRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidatephoneLexiconRef()
{
	m_phoneLexiconRef_Exist = false;
}
XMLCh * Mp7JrsSpeakerInfoType::GetwordLexiconRef() const
{
	return m_wordLexiconRef;
}

bool Mp7JrsSpeakerInfoType::ExistwordLexiconRef() const
{
	return m_wordLexiconRef_Exist;
}
void Mp7JrsSpeakerInfoType::SetwordLexiconRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetwordLexiconRef().");
	}
	m_wordLexiconRef = item;
	m_wordLexiconRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidatewordLexiconRef()
{
	m_wordLexiconRef_Exist = false;
}
XMLCh * Mp7JrsSpeakerInfoType::GetconfusionInfoRef() const
{
	return m_confusionInfoRef;
}

bool Mp7JrsSpeakerInfoType::ExistconfusionInfoRef() const
{
	return m_confusionInfoRef_Exist;
}
void Mp7JrsSpeakerInfoType::SetconfusionInfoRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetconfusionInfoRef().");
	}
	m_confusionInfoRef = item;
	m_confusionInfoRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidateconfusionInfoRef()
{
	m_confusionInfoRef_Exist = false;
}
XMLCh * Mp7JrsSpeakerInfoType::GetdescriptionMetadataRef() const
{
	return m_descriptionMetadataRef;
}

bool Mp7JrsSpeakerInfoType::ExistdescriptionMetadataRef() const
{
	return m_descriptionMetadataRef_Exist;
}
void Mp7JrsSpeakerInfoType::SetdescriptionMetadataRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetdescriptionMetadataRef().");
	}
	m_descriptionMetadataRef = item;
	m_descriptionMetadataRef_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidatedescriptionMetadataRef()
{
	m_descriptionMetadataRef_Exist = false;
}
Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration Mp7JrsSpeakerInfoType::Getprovenance() const
{
	return m_provenance;
}

void Mp7JrsSpeakerInfoType::Setprovenance(Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::Setprovenance().");
	}
	m_provenance = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSpeakerInfoType::GetSpokenLanguage() const
{
		return m_SpokenLanguage;
}

Mp7JrsPersonPtr Mp7JrsSpeakerInfoType::GetPerson() const
{
		return m_Person;
}

// Element is optional
bool Mp7JrsSpeakerInfoType::IsValidPerson() const
{
	return m_Person_Exist;
}

Mp7JrsSpeakerInfoType_WordIndex_LocalPtr Mp7JrsSpeakerInfoType::GetWordIndex() const
{
		return m_WordIndex;
}

// Element is optional
bool Mp7JrsSpeakerInfoType::IsValidWordIndex() const
{
	return m_WordIndex_Exist;
}

Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr Mp7JrsSpeakerInfoType::GetPhoneIndex() const
{
		return m_PhoneIndex;
}

// Element is optional
bool Mp7JrsSpeakerInfoType::IsValidPhoneIndex() const
{
	return m_PhoneIndex_Exist;
}

void Mp7JrsSpeakerInfoType::SetSpokenLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetSpokenLanguage().");
	}
	if (m_SpokenLanguage != item)
	{
		XMLString::release(&m_SpokenLanguage);
		m_SpokenLanguage = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::SetPerson(const Mp7JrsPersonPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetPerson().");
	}
	if (m_Person != item || m_Person_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Person);
		m_Person = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Person) m_Person->SetParent(m_myself.getPointer());
		if (m_Person && m_Person->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Person->UseTypeAttribute = true;
		}
		m_Person_Exist = true;
		if(m_Person != Mp7JrsPersonPtr())
		{
			m_Person->SetContentName(XMLString::transcode("Person"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidatePerson()
{
	m_Person_Exist = false;
}
void Mp7JrsSpeakerInfoType::SetWordIndex(const Mp7JrsSpeakerInfoType_WordIndex_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetWordIndex().");
	}
	if (m_WordIndex != item || m_WordIndex_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_WordIndex);
		m_WordIndex = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WordIndex) m_WordIndex->SetParent(m_myself.getPointer());
		if (m_WordIndex && m_WordIndex->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_WordIndex->UseTypeAttribute = true;
		}
		m_WordIndex_Exist = true;
		if(m_WordIndex != Mp7JrsSpeakerInfoType_WordIndex_LocalPtr())
		{
			m_WordIndex->SetContentName(XMLString::transcode("WordIndex"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidateWordIndex()
{
	m_WordIndex_Exist = false;
}
void Mp7JrsSpeakerInfoType::SetPhoneIndex(const Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::SetPhoneIndex().");
	}
	if (m_PhoneIndex != item || m_PhoneIndex_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PhoneIndex);
		m_PhoneIndex = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PhoneIndex) m_PhoneIndex->SetParent(m_myself.getPointer());
		if (m_PhoneIndex && m_PhoneIndex->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PhoneIndex->UseTypeAttribute = true;
		}
		m_PhoneIndex_Exist = true;
		if(m_PhoneIndex != Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr())
		{
			m_PhoneIndex->SetContentName(XMLString::transcode("PhoneIndex"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::InvalidatePhoneIndex()
{
	m_PhoneIndex_Exist = false;
}
XMLCh * Mp7JrsSpeakerInfoType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSpeakerInfoType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSpeakerInfoType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpeakerInfoType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpeakerInfoType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsSpeakerInfoType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneLexiconRef")) == 0)
	{
		// phoneLexiconRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("wordLexiconRef")) == 0)
	{
		// wordLexiconRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confusionInfoRef")) == 0)
	{
		// confusionInfoRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("descriptionMetadataRef")) == 0)
	{
		// descriptionMetadataRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("provenance")) == 0)
	{
		// provenance is simple attribute SpeakerInfoType_provenance_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSpeakerInfoType_provenance_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Person")) == 0)
	{
		// Person is simple element PersonType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerson()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerson(p, client);
					if((p = GetPerson()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("WordIndex")) == 0)
	{
		// WordIndex is simple element SpeakerInfoType_WordIndex_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWordIndex()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_WordIndex_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpeakerInfoType_WordIndex_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWordIndex(p, client);
					if((p = GetWordIndex()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PhoneIndex")) == 0)
	{
		// PhoneIndex is simple element SpeakerInfoType_PhoneIndex_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPhoneIndex()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpeakerInfoType_PhoneIndex_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPhoneIndex(p, client);
					if((p = GetPhoneIndex()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpeakerInfoType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpeakerInfoType");
	}
	return result;
}

XMLCh * Mp7JrsSpeakerInfoType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpeakerInfoType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpeakerInfoType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpeakerInfoType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneLexiconRef_Exist)
	{
	// String
	if(m_phoneLexiconRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("phoneLexiconRef"), m_phoneLexiconRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_wordLexiconRef_Exist)
	{
	// String
	if(m_wordLexiconRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("wordLexiconRef"), m_wordLexiconRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confusionInfoRef_Exist)
	{
	// String
	if(m_confusionInfoRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("confusionInfoRef"), m_confusionInfoRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_descriptionMetadataRef_Exist)
	{
	// String
	if(m_descriptionMetadataRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("descriptionMetadataRef"), m_descriptionMetadataRef);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
	// Enumeration
	if(m_provenance != Mp7JrsSpeakerInfoType_provenance_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSpeakerInfoType_provenance_LocalType::ToText(m_provenance);
		element->setAttributeNS(X(""), X("provenance"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	 // String
	if(m_SpokenLanguage != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_SpokenLanguage, X("urn:mpeg:mpeg7:schema:2004"), X("SpokenLanguage"), false);
	// Class
	
	if (m_Person != Mp7JrsPersonPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Person->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Person"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Person->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_WordIndex != Mp7JrsSpeakerInfoType_WordIndex_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_WordIndex->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("WordIndex"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_WordIndex->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PhoneIndex != Mp7JrsSpeakerInfoType_PhoneIndex_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PhoneIndex->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PhoneIndex"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PhoneIndex->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpeakerInfoType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneLexiconRef")))
	{
		// Deserialize string type
		this->SetphoneLexiconRef(Dc1Convert::TextToString(parent->getAttribute(X("phoneLexiconRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("wordLexiconRef")))
	{
		// Deserialize string type
		this->SetwordLexiconRef(Dc1Convert::TextToString(parent->getAttribute(X("wordLexiconRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confusionInfoRef")))
	{
		// Deserialize string type
		this->SetconfusionInfoRef(Dc1Convert::TextToString(parent->getAttribute(X("confusionInfoRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("descriptionMetadataRef")))
	{
		// Deserialize string type
		this->SetdescriptionMetadataRef(Dc1Convert::TextToString(parent->getAttribute(X("descriptionMetadataRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("provenance")))
	{
		this->Setprovenance(Mp7JrsSpeakerInfoType_provenance_LocalType::Parse(parent->getAttribute(X("provenance"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("SpokenLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetSpokenLanguage(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetSpokenLanguage(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Person"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Person")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePersonType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerson(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("WordIndex"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("WordIndex")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpeakerInfoType_WordIndex_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetWordIndex(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PhoneIndex"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PhoneIndex")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpeakerInfoType_PhoneIndex_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPhoneIndex(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpeakerInfoType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Person")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePersonType; // FTT, check this
	}
	this->SetPerson(child);
  }
  if (XMLString::compareString(elementname, X("WordIndex")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpeakerInfoType_WordIndex_LocalType; // FTT, check this
	}
	this->SetWordIndex(child);
  }
  if (XMLString::compareString(elementname, X("PhoneIndex")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpeakerInfoType_PhoneIndex_LocalType; // FTT, check this
	}
	this->SetPhoneIndex(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpeakerInfoType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPerson() != Dc1NodePtr())
		result.Insert(GetPerson());
	if (GetWordIndex() != Dc1NodePtr())
		result.Insert(GetWordIndex());
	if (GetPhoneIndex() != Dc1NodePtr())
		result.Insert(GetPhoneIndex());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpeakerInfoType_ExtMethodImpl.h


