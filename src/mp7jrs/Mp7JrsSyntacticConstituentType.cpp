
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtImplInclude.h


#include "Mp7JrsLinguisticEntityType.h"
#include "Mp7JrsSyntacticConstituentType_CollectionType.h"
#include "Mp7JrsLinguisticEntityType_start_LocalType.h"
#include "Mp7JrsLinguisticEntityType_length_LocalType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrstermReferenceListType.h"
#include "Mp7JrsLinguisticEntityType_edit_LocalType.h"
#include "Mp7JrsLinguisticEntityType_MediaLocator_CollectionType.h"
#include "Mp7JrsLinguisticEntityType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSyntacticConstituentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaLocator
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsSyntacticConstituentType_LocalType.h" // Choice collection Head
#include "Mp7JrsSyntacticConstituentType.h" // Choice collection element Head
#include "Mp7JrsLinguisticDocumentType.h" // Choice collection element Quotation

#include <assert.h>
IMp7JrsSyntacticConstituentType::IMp7JrsSyntacticConstituentType()
{

// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtPropInit.h

}

IMp7JrsSyntacticConstituentType::~IMp7JrsSyntacticConstituentType()
{
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtPropCleanup.h

}

Mp7JrsSyntacticConstituentType::Mp7JrsSyntacticConstituentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSyntacticConstituentType::~Mp7JrsSyntacticConstituentType()
{
	Cleanup();
}

void Mp7JrsSyntacticConstituentType::Init()
{
	// Init base
	m_Base = CreateLinguisticEntityType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_baseForm = NULL; // String
	m_baseForm_Exist = false;
	m_synthesis = Mp7JrssynthesisType::UninitializedEnumeration;
	m_synthesis_Default = Mp7JrssynthesisType::dependency; // Default enumeration
	m_synthesis_Exist = false;
	m_functionWord = NULL; // String
	m_functionWord_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SyntacticConstituentType_LocalType = Mp7JrsSyntacticConstituentType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMyPropInit.h

}

void Mp7JrsSyntacticConstituentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_baseForm); // String
	XMLString::release(&m_functionWord); // String
	// Dc1Factory::DeleteObject(m_SyntacticConstituentType_LocalType);
}

void Mp7JrsSyntacticConstituentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SyntacticConstituentTypePtr, since we
	// might need GetBase(), which isn't defined in ISyntacticConstituentType
	const Dc1Ptr< Mp7JrsSyntacticConstituentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsLinguisticEntityPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSyntacticConstituentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_baseForm); // String
	if (tmp->ExistbaseForm())
	{
		this->SetbaseForm(XMLString::replicate(tmp->GetbaseForm()));
	}
	else
	{
		InvalidatebaseForm();
	}
	}
	{
	if (tmp->Existsynthesis())
	{
		this->Setsynthesis(tmp->Getsynthesis());
	}
	else
	{
		Invalidatesynthesis();
	}
	}
	{
	XMLString::release(&m_functionWord); // String
	if (tmp->ExistfunctionWord())
	{
		this->SetfunctionWord(XMLString::replicate(tmp->GetfunctionWord()));
	}
	else
	{
		InvalidatefunctionWord();
	}
	}
		// Dc1Factory::DeleteObject(m_SyntacticConstituentType_LocalType);
		this->SetSyntacticConstituentType_LocalType(Dc1Factory::CloneObject(tmp->GetSyntacticConstituentType_LocalType()));
}

Dc1NodePtr Mp7JrsSyntacticConstituentType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSyntacticConstituentType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsLinguisticEntityType > Mp7JrsSyntacticConstituentType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsSyntacticConstituentType::GetbaseForm() const
{
	return m_baseForm;
}

bool Mp7JrsSyntacticConstituentType::ExistbaseForm() const
{
	return m_baseForm_Exist;
}
void Mp7JrsSyntacticConstituentType::SetbaseForm(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetbaseForm().");
	}
	m_baseForm = item;
	m_baseForm_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatebaseForm()
{
	m_baseForm_Exist = false;
}
Mp7JrssynthesisType::Enumeration Mp7JrsSyntacticConstituentType::Getsynthesis() const
{
	if (this->Existsynthesis()) {
		return m_synthesis;
	} else {
		return m_synthesis_Default;
	}
}

bool Mp7JrsSyntacticConstituentType::Existsynthesis() const
{
	return m_synthesis_Exist;
}
void Mp7JrsSyntacticConstituentType::Setsynthesis(Mp7JrssynthesisType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setsynthesis().");
	}
	m_synthesis = item;
	m_synthesis_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatesynthesis()
{
	m_synthesis_Exist = false;
}
XMLCh * Mp7JrsSyntacticConstituentType::GetfunctionWord() const
{
	return m_functionWord;
}

bool Mp7JrsSyntacticConstituentType::ExistfunctionWord() const
{
	return m_functionWord_Exist;
}
void Mp7JrsSyntacticConstituentType::SetfunctionWord(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetfunctionWord().");
	}
	m_functionWord = item;
	m_functionWord_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatefunctionWord()
{
	m_functionWord_Exist = false;
}
Mp7JrsSyntacticConstituentType_CollectionPtr Mp7JrsSyntacticConstituentType::GetSyntacticConstituentType_LocalType() const
{
		return m_SyntacticConstituentType_LocalType;
}

void Mp7JrsSyntacticConstituentType::SetSyntacticConstituentType_LocalType(const Mp7JrsSyntacticConstituentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetSyntacticConstituentType_LocalType().");
	}
	if (m_SyntacticConstituentType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SyntacticConstituentType_LocalType);
		m_SyntacticConstituentType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SyntacticConstituentType_LocalType) m_SyntacticConstituentType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSyntacticConstituentType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsSyntacticConstituentType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsSyntacticConstituentType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsLinguisticEntityType_start_LocalPtr Mp7JrsSyntacticConstituentType::Getstart() const
{
	return GetBase()->Getstart();
}

bool Mp7JrsSyntacticConstituentType::Existstart() const
{
	return GetBase()->Existstart();
}
void Mp7JrsSyntacticConstituentType::Setstart(const Mp7JrsLinguisticEntityType_start_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setstart().");
	}
	GetBase()->Setstart(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatestart()
{
	GetBase()->Invalidatestart();
}
Mp7JrsLinguisticEntityType_length_LocalPtr Mp7JrsSyntacticConstituentType::Getlength() const
{
	return GetBase()->Getlength();
}

bool Mp7JrsSyntacticConstituentType::Existlength() const
{
	return GetBase()->Existlength();
}
void Mp7JrsSyntacticConstituentType::Setlength(const Mp7JrsLinguisticEntityType_length_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setlength().");
	}
	GetBase()->Setlength(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatelength()
{
	GetBase()->Invalidatelength();
}
Mp7JrstermReferencePtr Mp7JrsSyntacticConstituentType::Gettype() const
{
	return GetBase()->Gettype();
}

bool Mp7JrsSyntacticConstituentType::Existtype() const
{
	return GetBase()->Existtype();
}
void Mp7JrsSyntacticConstituentType::Settype(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Settype().");
	}
	GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatetype()
{
	GetBase()->Invalidatetype();
}
Mp7JrstermReferencePtr Mp7JrsSyntacticConstituentType::Getdepend() const
{
	return GetBase()->Getdepend();
}

bool Mp7JrsSyntacticConstituentType::Existdepend() const
{
	return GetBase()->Existdepend();
}
void Mp7JrsSyntacticConstituentType::Setdepend(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setdepend().");
	}
	GetBase()->Setdepend(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatedepend()
{
	GetBase()->Invalidatedepend();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::Getequal() const
{
	return GetBase()->Getequal();
}

bool Mp7JrsSyntacticConstituentType::Existequal() const
{
	return GetBase()->Existequal();
}
void Mp7JrsSyntacticConstituentType::Setequal(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setequal().");
	}
	GetBase()->Setequal(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidateequal()
{
	GetBase()->Invalidateequal();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::Getsemantics() const
{
	return GetBase()->Getsemantics();
}

bool Mp7JrsSyntacticConstituentType::Existsemantics() const
{
	return GetBase()->Existsemantics();
}
void Mp7JrsSyntacticConstituentType::Setsemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setsemantics().");
	}
	GetBase()->Setsemantics(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatesemantics()
{
	GetBase()->Invalidatesemantics();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::GetcompoundSemantics() const
{
	return GetBase()->GetcompoundSemantics();
}

bool Mp7JrsSyntacticConstituentType::ExistcompoundSemantics() const
{
	return GetBase()->ExistcompoundSemantics();
}
void Mp7JrsSyntacticConstituentType::SetcompoundSemantics(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetcompoundSemantics().");
	}
	GetBase()->SetcompoundSemantics(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatecompoundSemantics()
{
	GetBase()->InvalidatecompoundSemantics();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::Getoperator() const
{
	return GetBase()->Getoperator();
}

bool Mp7JrsSyntacticConstituentType::Existoperator() const
{
	return GetBase()->Existoperator();
}
void Mp7JrsSyntacticConstituentType::Setoperator(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setoperator().");
	}
	GetBase()->Setoperator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidateoperator()
{
	GetBase()->Invalidateoperator();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::Getcopy() const
{
	return GetBase()->Getcopy();
}

bool Mp7JrsSyntacticConstituentType::Existcopy() const
{
	return GetBase()->Existcopy();
}
void Mp7JrsSyntacticConstituentType::Setcopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setcopy().");
	}
	GetBase()->Setcopy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatecopy()
{
	GetBase()->Invalidatecopy();
}
Mp7JrstermReferenceListPtr Mp7JrsSyntacticConstituentType::GetnoCopy() const
{
	return GetBase()->GetnoCopy();
}

bool Mp7JrsSyntacticConstituentType::ExistnoCopy() const
{
	return GetBase()->ExistnoCopy();
}
void Mp7JrsSyntacticConstituentType::SetnoCopy(const Mp7JrstermReferenceListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetnoCopy().");
	}
	GetBase()->SetnoCopy(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatenoCopy()
{
	GetBase()->InvalidatenoCopy();
}
Mp7JrstermReferencePtr Mp7JrsSyntacticConstituentType::Getsubstitute() const
{
	return GetBase()->Getsubstitute();
}

bool Mp7JrsSyntacticConstituentType::Existsubstitute() const
{
	return GetBase()->Existsubstitute();
}
void Mp7JrsSyntacticConstituentType::Setsubstitute(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setsubstitute().");
	}
	GetBase()->Setsubstitute(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidatesubstitute()
{
	GetBase()->Invalidatesubstitute();
}
Mp7JrstermReferencePtr Mp7JrsSyntacticConstituentType::GetinScope() const
{
	return GetBase()->GetinScope();
}

bool Mp7JrsSyntacticConstituentType::ExistinScope() const
{
	return GetBase()->ExistinScope();
}
void Mp7JrsSyntacticConstituentType::SetinScope(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetinScope().");
	}
	GetBase()->SetinScope(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidateinScope()
{
	GetBase()->InvalidateinScope();
}
Mp7JrsLinguisticEntityType_edit_LocalPtr Mp7JrsSyntacticConstituentType::Getedit() const
{
	return GetBase()->Getedit();
}

bool Mp7JrsSyntacticConstituentType::Existedit() const
{
	return GetBase()->Existedit();
}
void Mp7JrsSyntacticConstituentType::Setedit(const Mp7JrsLinguisticEntityType_edit_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setedit().");
	}
	GetBase()->Setedit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidateedit()
{
	GetBase()->Invalidateedit();
}
Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr Mp7JrsSyntacticConstituentType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsLinguisticEntityType_Relation_CollectionPtr Mp7JrsSyntacticConstituentType::GetRelation() const
{
	return GetBase()->GetRelation();
}

void Mp7JrsSyntacticConstituentType::SetMediaLocator(const Mp7JrsLinguisticEntityType_MediaLocator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::SetRelation(const Mp7JrsLinguisticEntityType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSyntacticConstituentType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSyntacticConstituentType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSyntacticConstituentType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSyntacticConstituentType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSyntacticConstituentType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSyntacticConstituentType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSyntacticConstituentType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSyntacticConstituentType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSyntacticConstituentType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSyntacticConstituentType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSyntacticConstituentType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSyntacticConstituentType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSyntacticConstituentType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSyntacticConstituentType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSyntacticConstituentType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSyntacticConstituentType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSyntacticConstituentType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSyntacticConstituentType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSyntacticConstituentType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSyntacticConstituentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 22 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("baseForm")) == 0)
	{
		// baseForm is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("synthesis")) == 0)
	{
		// synthesis is simple attribute synthesisType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrssynthesisType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("functionWord")) == 0)
	{
		// functionWord is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Head")) == 0)
	{
		// Head is contained in itemtype SyntacticConstituentType_LocalType
		// in choice collection SyntacticConstituentType_CollectionType

		context->Found = true;
		Mp7JrsSyntacticConstituentType_CollectionPtr coll = GetSyntacticConstituentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSyntacticConstituentType_CollectionType; // FTT, check this
				SetSyntacticConstituentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetHead()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSyntacticConstituentType_LocalPtr item = CreateSyntacticConstituentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSyntacticConstituentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->SetHead(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetHead()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Phrase")) == 0)
	{
		// Phrase is contained in itemtype SyntacticConstituentType_LocalType
		// in choice collection SyntacticConstituentType_CollectionType

		context->Found = true;
		Mp7JrsSyntacticConstituentType_CollectionPtr coll = GetSyntacticConstituentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSyntacticConstituentType_CollectionType; // FTT, check this
				SetSyntacticConstituentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSyntacticConstituentType_LocalPtr item = CreateSyntacticConstituentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SyntacticConstituentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSyntacticConstituentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->SetPhrase(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quotation")) == 0)
	{
		// Quotation is contained in itemtype SyntacticConstituentType_LocalType
		// in choice collection SyntacticConstituentType_CollectionType

		context->Found = true;
		Mp7JrsSyntacticConstituentType_CollectionPtr coll = GetSyntacticConstituentType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSyntacticConstituentType_CollectionType; // FTT, check this
				SetSyntacticConstituentType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSyntacticConstituentType_LocalPtr item = CreateSyntacticConstituentType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:LinguisticDocumentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsLinguisticDocumentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->SetQuotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSyntacticConstituentType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SyntacticConstituentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SyntacticConstituentType");
	}
	return result;
}

XMLCh * Mp7JrsSyntacticConstituentType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSyntacticConstituentType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSyntacticConstituentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SyntacticConstituentType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_baseForm_Exist)
	{
	// String
	if(m_baseForm != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("baseForm"), m_baseForm);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_synthesis_Exist)
	{
	// Enumeration
	if(m_synthesis != Mp7JrssynthesisType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrssynthesisType::ToText(m_synthesis);
		element->setAttributeNS(X(""), X("synthesis"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_functionWord_Exist)
	{
	// String
	if(m_functionWord != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("functionWord"), m_functionWord);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_SyntacticConstituentType_LocalType != Mp7JrsSyntacticConstituentType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SyntacticConstituentType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSyntacticConstituentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("baseForm")))
	{
		// Deserialize string type
		this->SetbaseForm(Dc1Convert::TextToString(parent->getAttribute(X("baseForm"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("synthesis")))
	{
		this->Setsynthesis(Mp7JrssynthesisType::Parse(parent->getAttribute(X("synthesis"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("functionWord")))
	{
		// Deserialize string type
		this->SetfunctionWord(Dc1Convert::TextToString(parent->getAttribute(X("functionWord"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsLinguisticEntityType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Head
			Dc1Util::HasNodeName(parent, X("Head"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Head")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Phrase
			Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Phrase")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Quotation
			Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Quotation")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSyntacticConstituentType_CollectionPtr tmp = CreateSyntacticConstituentType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSyntacticConstituentType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSyntacticConstituentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Head"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Head")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Phrase")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Quotation")) == 0)
	)
  {
	Mp7JrsSyntacticConstituentType_CollectionPtr tmp = CreateSyntacticConstituentType_CollectionType; // FTT, check this
	this->SetSyntacticConstituentType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSyntacticConstituentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSyntacticConstituentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSyntacticConstituentType_LocalType());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSyntacticConstituentType_ExtMethodImpl.h


