
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtImplInclude.h


#include "Mp7JrsPersonType.h"
#include "Mp7JrsUserProfileType_PublicIdentifier_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsTimeZoneType.h"
#include "Mp7JrsConnectionsType.h"
#include "Mp7JrsPersonalInterestsType.h"
#include "Mp7JrsPersonType_CollectionType.h"
#include "Mp7JrsPersonType_Affiliation_CollectionType.h"
#include "Mp7JrsPersonType_Citizenship_CollectionType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsPersonType_ElectronicAddress_CollectionType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrscountryCode.h"
#include "Mp7JrsAgentType_Icon_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUserProfileType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Icon
#include "Mp7JrsPersonType_LocalType.h" // Choice collection Name
#include "Mp7JrsPersonNameType.h" // Choice collection element Name
#include "Mp7JrsControlledTermUseType.h" // Choice collection element NameTerm
#include "Mp7JrsPersonType_Affiliation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affiliation
#include "Mp7JrsElectronicAddressType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ElectronicAddress
#include "Mp7JrsUniqueIDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PublicIdentifier

#include <assert.h>
IMp7JrsUserProfileType::IMp7JrsUserProfileType()
{

// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtPropInit.h

}

IMp7JrsUserProfileType::~IMp7JrsUserProfileType()
{
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtPropCleanup.h

}

Mp7JrsUserProfileType::Mp7JrsUserProfileType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserProfileType::~Mp7JrsUserProfileType()
{
	Cleanup();
}

void Mp7JrsUserProfileType::Init()
{
	// Init base
	m_Base = CreatePersonType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_sex = Mp7JrsUserProfileType_sex_LocalType::UninitializedEnumeration;
	m_sex_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_PublicIdentifier = Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr(); // Collection
	m_Description = Mp7JrsTextAnnotationPtr(); // Class
	m_Description_Exist = false;
	m_TimeZone = Mp7JrsTimeZonePtr(); // Class
	m_TimeZone_Exist = false;
	m_PrimaryLanguage = NULL; // Optional String
	m_PrimaryLanguage_Exist = false;
	m_Connections = Mp7JrsConnectionsPtr(); // Class
	m_Connections_Exist = false;
	m_PersonalInterests = Mp7JrsPersonalInterestsPtr(); // Class
	m_PersonalInterests_Exist = false;


// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMyPropInit.h

}

void Mp7JrsUserProfileType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PublicIdentifier);
	// Dc1Factory::DeleteObject(m_Description);
	// Dc1Factory::DeleteObject(m_TimeZone);
	XMLString::release(&this->m_PrimaryLanguage);
	this->m_PrimaryLanguage = NULL;
	// Dc1Factory::DeleteObject(m_Connections);
	// Dc1Factory::DeleteObject(m_PersonalInterests);
}

void Mp7JrsUserProfileType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserProfileTypePtr, since we
	// might need GetBase(), which isn't defined in IUserProfileType
	const Dc1Ptr< Mp7JrsUserProfileType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsPersonPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUserProfileType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existsex())
	{
		this->Setsex(tmp->Getsex());
	}
	else
	{
		Invalidatesex();
	}
	}
		// Dc1Factory::DeleteObject(m_PublicIdentifier);
		this->SetPublicIdentifier(Dc1Factory::CloneObject(tmp->GetPublicIdentifier()));
	if (tmp->IsValidDescription())
	{
		// Dc1Factory::DeleteObject(m_Description);
		this->SetDescription(Dc1Factory::CloneObject(tmp->GetDescription()));
	}
	else
	{
		InvalidateDescription();
	}
	if (tmp->IsValidTimeZone())
	{
		// Dc1Factory::DeleteObject(m_TimeZone);
		this->SetTimeZone(Dc1Factory::CloneObject(tmp->GetTimeZone()));
	}
	else
	{
		InvalidateTimeZone();
	}
	if (tmp->IsValidPrimaryLanguage())
	{
		this->SetPrimaryLanguage(XMLString::replicate(tmp->GetPrimaryLanguage()));
	}
	else
	{
		InvalidatePrimaryLanguage();
	}
	if (tmp->IsValidConnections())
	{
		// Dc1Factory::DeleteObject(m_Connections);
		this->SetConnections(Dc1Factory::CloneObject(tmp->GetConnections()));
	}
	else
	{
		InvalidateConnections();
	}
	if (tmp->IsValidPersonalInterests())
	{
		// Dc1Factory::DeleteObject(m_PersonalInterests);
		this->SetPersonalInterests(Dc1Factory::CloneObject(tmp->GetPersonalInterests()));
	}
	else
	{
		InvalidatePersonalInterests();
	}
}

Dc1NodePtr Mp7JrsUserProfileType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUserProfileType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsPersonType > Mp7JrsUserProfileType::GetBase() const
{
	return m_Base;
}

Mp7JrsUserProfileType_sex_LocalType::Enumeration Mp7JrsUserProfileType::Getsex() const
{
	return m_sex;
}

bool Mp7JrsUserProfileType::Existsex() const
{
	return m_sex_Exist;
}
void Mp7JrsUserProfileType::Setsex(Mp7JrsUserProfileType_sex_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::Setsex().");
	}
	m_sex = item;
	m_sex_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::Invalidatesex()
{
	m_sex_Exist = false;
}
Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr Mp7JrsUserProfileType::GetPublicIdentifier() const
{
		return m_PublicIdentifier;
}

Mp7JrsTextAnnotationPtr Mp7JrsUserProfileType::GetDescription() const
{
		return m_Description;
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidDescription() const
{
	return m_Description_Exist;
}

Mp7JrsTimeZonePtr Mp7JrsUserProfileType::GetTimeZone() const
{
		return m_TimeZone;
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidTimeZone() const
{
	return m_TimeZone_Exist;
}

XMLCh * Mp7JrsUserProfileType::GetPrimaryLanguage() const
{
		return m_PrimaryLanguage;
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidPrimaryLanguage() const
{
	return m_PrimaryLanguage_Exist;
}

Mp7JrsConnectionsPtr Mp7JrsUserProfileType::GetConnections() const
{
		return m_Connections;
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidConnections() const
{
	return m_Connections_Exist;
}

Mp7JrsPersonalInterestsPtr Mp7JrsUserProfileType::GetPersonalInterests() const
{
		return m_PersonalInterests;
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidPersonalInterests() const
{
	return m_PersonalInterests_Exist;
}

void Mp7JrsUserProfileType::SetPublicIdentifier(const Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetPublicIdentifier().");
	}
	if (m_PublicIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_PublicIdentifier);
		m_PublicIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PublicIdentifier) m_PublicIdentifier->SetParent(m_myself.getPointer());
		if(m_PublicIdentifier != Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr())
		{
			m_PublicIdentifier->SetContentName(XMLString::transcode("PublicIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::SetDescription(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetDescription().");
	}
	if (m_Description != item || m_Description_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Description);
		m_Description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Description) m_Description->SetParent(m_myself.getPointer());
		if (m_Description && m_Description->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Description->UseTypeAttribute = true;
		}
		m_Description_Exist = true;
		if(m_Description != Mp7JrsTextAnnotationPtr())
		{
			m_Description->SetContentName(XMLString::transcode("Description"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidateDescription()
{
	m_Description_Exist = false;
}
void Mp7JrsUserProfileType::SetTimeZone(const Mp7JrsTimeZonePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetTimeZone().");
	}
	if (m_TimeZone != item || m_TimeZone_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TimeZone);
		m_TimeZone = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimeZone) m_TimeZone->SetParent(m_myself.getPointer());
		if (m_TimeZone && m_TimeZone->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeZoneType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimeZone->UseTypeAttribute = true;
		}
		m_TimeZone_Exist = true;
		if(m_TimeZone != Mp7JrsTimeZonePtr())
		{
			m_TimeZone->SetContentName(XMLString::transcode("TimeZone"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidateTimeZone()
{
	m_TimeZone_Exist = false;
}
void Mp7JrsUserProfileType::SetPrimaryLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetPrimaryLanguage().");
	}
	if (m_PrimaryLanguage != item || m_PrimaryLanguage_Exist == false)
	{
		XMLString::release(&m_PrimaryLanguage);
		m_PrimaryLanguage = item;
		m_PrimaryLanguage_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatePrimaryLanguage()
{
	m_PrimaryLanguage_Exist = false;
}
void Mp7JrsUserProfileType::SetConnections(const Mp7JrsConnectionsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetConnections().");
	}
	if (m_Connections != item || m_Connections_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Connections);
		m_Connections = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Connections) m_Connections->SetParent(m_myself.getPointer());
		if (m_Connections && m_Connections->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Connections->UseTypeAttribute = true;
		}
		m_Connections_Exist = true;
		if(m_Connections != Mp7JrsConnectionsPtr())
		{
			m_Connections->SetContentName(XMLString::transcode("Connections"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidateConnections()
{
	m_Connections_Exist = false;
}
void Mp7JrsUserProfileType::SetPersonalInterests(const Mp7JrsPersonalInterestsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetPersonalInterests().");
	}
	if (m_PersonalInterests != item || m_PersonalInterests_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PersonalInterests);
		m_PersonalInterests = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PersonalInterests) m_PersonalInterests->SetParent(m_myself.getPointer());
		if (m_PersonalInterests && m_PersonalInterests->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PersonalInterests->UseTypeAttribute = true;
		}
		m_PersonalInterests_Exist = true;
		if(m_PersonalInterests != Mp7JrsPersonalInterestsPtr())
		{
			m_PersonalInterests->SetContentName(XMLString::transcode("PersonalInterests"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatePersonalInterests()
{
	m_PersonalInterests_Exist = false;
}
Mp7JrsPersonType_CollectionPtr Mp7JrsUserProfileType::GetPersonType_LocalType() const
{
	return GetBase()->GetPersonType_LocalType();
}

Mp7JrsPersonType_Affiliation_CollectionPtr Mp7JrsUserProfileType::GetAffiliation() const
{
	return GetBase()->GetAffiliation();
}

Mp7JrsPersonType_Citizenship_CollectionPtr Mp7JrsUserProfileType::GetCitizenship() const
{
	return GetBase()->GetCitizenship();
}

Mp7JrsPlacePtr Mp7JrsUserProfileType::GetAddress() const
{
	return GetBase()->GetAddress();
}

Mp7JrsReferencePtr Mp7JrsUserProfileType::GetAddressRef() const
{
	return GetBase()->GetAddressRef();
}

Mp7JrsPersonType_ElectronicAddress_CollectionPtr Mp7JrsUserProfileType::GetElectronicAddress() const
{
	return GetBase()->GetElectronicAddress();
}

Mp7JrsTextualPtr Mp7JrsUserProfileType::GetPersonDescription() const
{
	return GetBase()->GetPersonDescription();
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidPersonDescription() const
{
	return GetBase()->IsValidPersonDescription();
}

Mp7JrscountryCodePtr Mp7JrsUserProfileType::GetNationality() const
{
	return GetBase()->GetNationality();
}

// Element is optional
bool Mp7JrsUserProfileType::IsValidNationality() const
{
	return GetBase()->IsValidNationality();
}

void Mp7JrsUserProfileType::SetPersonType_LocalType(const Mp7JrsPersonType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetPersonType_LocalType().");
	}
	GetBase()->SetPersonType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::SetAffiliation(const Mp7JrsPersonType_Affiliation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetAffiliation().");
	}
	GetBase()->SetAffiliation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::SetCitizenship(const Mp7JrsPersonType_Citizenship_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetCitizenship().");
	}
	GetBase()->SetCitizenship(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsUserProfileType::SetAddress(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetAddress().");
	}
	GetBase()->SetAddress(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsUserProfileType::SetAddressRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetAddressRef().");
	}
	GetBase()->SetAddressRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::SetElectronicAddress(const Mp7JrsPersonType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetElectronicAddress().");
	}
	GetBase()->SetElectronicAddress(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::SetPersonDescription(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetPersonDescription().");
	}
	GetBase()->SetPersonDescription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatePersonDescription()
{
	GetBase()->InvalidatePersonDescription();
}
void Mp7JrsUserProfileType::SetNationality(const Mp7JrscountryCodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetNationality().");
	}
	GetBase()->SetNationality(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidateNationality()
{
	GetBase()->InvalidateNationality();
}
Mp7JrsAgentType_Icon_CollectionPtr Mp7JrsUserProfileType::GetIcon() const
{
	return GetBase()->GetBase()->GetIcon();
}

void Mp7JrsUserProfileType::SetIcon(const Mp7JrsAgentType_Icon_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetIcon().");
	}
	GetBase()->GetBase()->SetIcon(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUserProfileType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsUserProfileType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsUserProfileType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUserProfileType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsUserProfileType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsUserProfileType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUserProfileType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsUserProfileType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsUserProfileType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUserProfileType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUserProfileType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUserProfileType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUserProfileType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUserProfileType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUserProfileType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserProfileType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUserProfileType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsUserProfileType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserProfileType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUserProfileType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("sex")) == 0)
	{
		// sex is simple attribute UserProfileType_sex_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsUserProfileType_sex_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PublicIdentifier")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PublicIdentifier is item of type UniqueIDType
		// in element collection UserProfileType_PublicIdentifier_CollectionType
		
		context->Found = true;
		Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr coll = GetPublicIdentifier();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserProfileType_PublicIdentifier_CollectionType; // FTT, check this
				SetPublicIdentifier(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Description")) == 0)
	{
		// Description is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescription()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescription(p, client);
					if((p = GetDescription()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TimeZone")) == 0)
	{
		// TimeZone is simple element TimeZoneType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTimeZone()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeZoneType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimeZonePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTimeZone(p, client);
					if((p = GetTimeZone()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Connections")) == 0)
	{
		// Connections is simple element ConnectionsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetConnections()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ConnectionsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsConnectionsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetConnections(p, client);
					if((p = GetConnections()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PersonalInterests")) == 0)
	{
		// PersonalInterests is simple element PersonalInterestsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPersonalInterests()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonalInterestsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonalInterestsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPersonalInterests(p, client);
					if((p = GetPersonalInterests()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserProfileType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserProfileType");
	}
	return result;
}

XMLCh * Mp7JrsUserProfileType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUserProfileType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUserProfileType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserProfileType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_sex_Exist)
	{
	// Enumeration
	if(m_sex != Mp7JrsUserProfileType_sex_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsUserProfileType_sex_LocalType::ToText(m_sex);
		element->setAttributeNS(X(""), X("sex"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_PublicIdentifier != Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PublicIdentifier->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Description != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Description->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Description"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Description->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TimeZone != Mp7JrsTimeZonePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TimeZone->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TimeZone"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TimeZone->Serialize(doc, element, &elem);
	}
	 // String
	if(m_PrimaryLanguage != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_PrimaryLanguage, X("urn:mpeg:mpeg7:schema:2004"), X("PrimaryLanguage"), true);
	// Class
	
	if (m_Connections != Mp7JrsConnectionsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Connections->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Connections"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Connections->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PersonalInterests != Mp7JrsPersonalInterestsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PersonalInterests->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PersonalInterests"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PersonalInterests->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsUserProfileType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("sex")))
	{
		this->Setsex(Mp7JrsUserProfileType_sex_LocalType::Parse(parent->getAttribute(X("sex"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsPersonType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PublicIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PublicIdentifier")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr tmp = CreateUserProfileType_PublicIdentifier_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPublicIdentifier(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Description")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TimeZone"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimeZone")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTimeZoneType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTimeZone(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("PrimaryLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetPrimaryLanguage(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetPrimaryLanguage(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Connections"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Connections")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateConnectionsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetConnections(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PersonalInterests"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PersonalInterests")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePersonalInterestsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPersonalInterests(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserProfileType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PublicIdentifier")) == 0))
  {
	Mp7JrsUserProfileType_PublicIdentifier_CollectionPtr tmp = CreateUserProfileType_PublicIdentifier_CollectionType; // FTT, check this
	this->SetPublicIdentifier(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Description")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetDescription(child);
  }
  if (XMLString::compareString(elementname, X("TimeZone")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTimeZoneType; // FTT, check this
	}
	this->SetTimeZone(child);
  }
  if (XMLString::compareString(elementname, X("Connections")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateConnectionsType; // FTT, check this
	}
	this->SetConnections(child);
  }
  if (XMLString::compareString(elementname, X("PersonalInterests")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePersonalInterestsType; // FTT, check this
	}
	this->SetPersonalInterests(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserProfileType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPublicIdentifier() != Dc1NodePtr())
		result.Insert(GetPublicIdentifier());
	if (GetDescription() != Dc1NodePtr())
		result.Insert(GetDescription());
	if (GetTimeZone() != Dc1NodePtr())
		result.Insert(GetTimeZone());
	if (GetConnections() != Dc1NodePtr())
		result.Insert(GetConnections());
	if (GetPersonalInterests() != Dc1NodePtr())
		result.Insert(GetPersonalInterests());
	if (GetPersonType_LocalType() != Dc1NodePtr())
		result.Insert(GetPersonType_LocalType());
	if (GetAffiliation() != Dc1NodePtr())
		result.Insert(GetAffiliation());
	if (GetCitizenship() != Dc1NodePtr())
		result.Insert(GetCitizenship());
	if (GetElectronicAddress() != Dc1NodePtr())
		result.Insert(GetElectronicAddress());
	if (GetPersonDescription() != Dc1NodePtr())
		result.Insert(GetPersonDescription());
	if (GetNationality() != Dc1NodePtr())
		result.Insert(GetNationality());
	if (GetIcon() != Dc1NodePtr())
		result.Insert(GetIcon());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetAddress() != Dc1NodePtr())
		result.Insert(GetAddress());
	if (GetAddressRef() != Dc1NodePtr())
		result.Insert(GetAddressRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserProfileType_ExtMethodImpl.h


