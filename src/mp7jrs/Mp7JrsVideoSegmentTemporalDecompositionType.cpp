
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtImplInclude.h


#include "Mp7JrsTemporalSegmentDecompositionType.h"
#include "Mp7JrsVideoSegmentTemporalDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsVideoSegmentTemporalDecompositionType_LocalType.h" // Choice collection VideoSegment
#include "Mp7JrsVideoSegmentType.h" // Choice collection element VideoSegment
#include "Mp7JrsReferenceType.h" // Choice collection element VideoSegmentRef
#include "Mp7JrsStillRegionType.h" // Choice collection element StillRegion

#include <assert.h>
IMp7JrsVideoSegmentTemporalDecompositionType::IMp7JrsVideoSegmentTemporalDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtPropInit.h

}

IMp7JrsVideoSegmentTemporalDecompositionType::~IMp7JrsVideoSegmentTemporalDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtPropCleanup.h

}

Mp7JrsVideoSegmentTemporalDecompositionType::Mp7JrsVideoSegmentTemporalDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSegmentTemporalDecompositionType::~Mp7JrsVideoSegmentTemporalDecompositionType()
{
	Cleanup();
}

void Mp7JrsVideoSegmentTemporalDecompositionType::Init()
{
	// Init base
	m_Base = CreateTemporalSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_VideoSegmentTemporalDecompositionType_LocalType = Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtMyPropInit.h

}

void Mp7JrsVideoSegmentTemporalDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_VideoSegmentTemporalDecompositionType_LocalType);
}

void Mp7JrsVideoSegmentTemporalDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSegmentTemporalDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSegmentTemporalDecompositionType
	const Dc1Ptr< Mp7JrsVideoSegmentTemporalDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTemporalSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsVideoSegmentTemporalDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_VideoSegmentTemporalDecompositionType_LocalType);
		this->SetVideoSegmentTemporalDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetVideoSegmentTemporalDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsVideoSegmentTemporalDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsVideoSegmentTemporalDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTemporalSegmentDecompositionType > Mp7JrsVideoSegmentTemporalDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr Mp7JrsVideoSegmentTemporalDecompositionType::GetVideoSegmentTemporalDecompositionType_LocalType() const
{
		return m_VideoSegmentTemporalDecompositionType_LocalType;
}

void Mp7JrsVideoSegmentTemporalDecompositionType::SetVideoSegmentTemporalDecompositionType_LocalType(const Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SetVideoSegmentTemporalDecompositionType_LocalType().");
	}
	if (m_VideoSegmentTemporalDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_VideoSegmentTemporalDecompositionType_LocalType);
		m_VideoSegmentTemporalDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSegmentTemporalDecompositionType_LocalType) m_VideoSegmentTemporalDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsVideoSegmentTemporalDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsVideoSegmentTemporalDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsVideoSegmentTemporalDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsVideoSegmentTemporalDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsVideoSegmentTemporalDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsVideoSegmentTemporalDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsVideoSegmentTemporalDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsVideoSegmentTemporalDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsVideoSegmentTemporalDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsVideoSegmentTemporalDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSegmentTemporalDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsVideoSegmentTemporalDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsVideoSegmentTemporalDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentTemporalDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSegmentTemporalDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VideoSegment")) == 0)
	{
		// VideoSegment is contained in itemtype VideoSegmentTemporalDecompositionType_LocalType
		// in choice collection VideoSegmentTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr coll = GetVideoSegmentTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
				SetVideoSegmentTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetVideoSegment()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr item = CreateVideoSegmentTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetVideoSegment(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetVideoSegment()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VideoSegmentRef")) == 0)
	{
		// VideoSegmentRef is contained in itemtype VideoSegmentTemporalDecompositionType_LocalType
		// in choice collection VideoSegmentTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr coll = GetVideoSegmentTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
				SetVideoSegmentTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetVideoSegmentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr item = CreateVideoSegmentTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetVideoSegmentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetVideoSegmentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StillRegion")) == 0)
	{
		// StillRegion is contained in itemtype VideoSegmentTemporalDecompositionType_LocalType
		// in choice collection VideoSegmentTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr coll = GetVideoSegmentTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
				SetVideoSegmentTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetStillRegion()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr item = CreateVideoSegmentTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStillRegionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetStillRegion(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetStillRegion()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StillRegionRef")) == 0)
	{
		// StillRegionRef is contained in itemtype VideoSegmentTemporalDecompositionType_LocalType
		// in choice collection VideoSegmentTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr coll = GetVideoSegmentTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
				SetVideoSegmentTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetStillRegionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr item = CreateVideoSegmentTemporalDecompositionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetStillRegionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoSegmentTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetStillRegionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSegmentTemporalDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSegmentTemporalDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSegmentTemporalDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsVideoSegmentTemporalDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsVideoSegmentTemporalDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSegmentTemporalDecompositionType"));
	// Element serialization:
	if (m_VideoSegmentTemporalDecompositionType_LocalType != Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VideoSegmentTemporalDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsVideoSegmentTemporalDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTemporalSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:VideoSegment
			Dc1Util::HasNodeName(parent, X("VideoSegment"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VideoSegment")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:VideoSegmentRef
			Dc1Util::HasNodeName(parent, X("VideoSegmentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VideoSegmentRef")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:StillRegion
			Dc1Util::HasNodeName(parent, X("StillRegion"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StillRegion")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:StillRegionRef
			Dc1Util::HasNodeName(parent, X("StillRegionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StillRegionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr tmp = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVideoSegmentTemporalDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSegmentTemporalDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VideoSegment"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VideoSegment")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VideoSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VideoSegmentRef")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("StillRegion"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("StillRegion")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("StillRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("StillRegionRef")) == 0)
	)
  {
	Mp7JrsVideoSegmentTemporalDecompositionType_CollectionPtr tmp = CreateVideoSegmentTemporalDecompositionType_CollectionType; // FTT, check this
	this->SetVideoSegmentTemporalDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSegmentTemporalDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVideoSegmentTemporalDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetVideoSegmentTemporalDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSegmentTemporalDecompositionType_ExtMethodImpl.h


