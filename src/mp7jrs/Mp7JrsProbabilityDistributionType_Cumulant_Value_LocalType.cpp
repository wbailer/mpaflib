
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtImplInclude.h


#include "Mp7JrsDoubleMatrixType.h"
#include "Mp7Jrsdim_LocalType.h"
#include "Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtPropInit.h

}

IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::~IMp7JrsProbabilityDistributionType_Cumulant_Value_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtPropCleanup.h

}

Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::~Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType()
{
	Cleanup();
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Init()
{
	// Init base
	m_Base = CreateDoubleMatrixType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_order = 0; // Value



// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtMyPropInit.h

}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ProbabilityDistributionType_Cumulant_Value_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IProbabilityDistributionType_Cumulant_Value_LocalType
	const Dc1Ptr< Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDoubleMatrixPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->Setorder(tmp->Getorder());
	}
}

Dc1NodePtr Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDoubleMatrixType > Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Mp7JrsdoubleVector > Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetBaseCollection() const
{
	return GetBase()->GetBase();
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Initialize(unsigned int maxElems)
{
	GetBase()->GetBase()->Initialize(maxElems);
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::addElement(const double &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::addElement().");
	}
	GetBase()->GetBase()->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::setElementAt(const double &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::setElementAt().");
	}
	GetBase()->GetBase()->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
	m_myself.getPointer(), DC1_NA_NODE_UPDATED,
	client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::insertElementAt(const double &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::insertElementAt().");
	}
	GetBase()->GetBase()->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::removeAllElements().");
	}
	GetBase()->GetBase()->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::removeElementAt().");
	}
	GetBase()->GetBase()->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::containsElement(const double &toCheck)
{
	return GetBase()->GetBase()->containsElement(toCheck);
}

unsigned int Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::curCapacity() const
{
	return GetBase()->GetBase()->curCapacity();
}

const double Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::elementAt(unsigned int getAt) const
{
	return GetBase()->GetBase()->elementAt(getAt);
}

double Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::elementAt(unsigned int getAt)
{
	return GetBase()->GetBase()->elementAt(getAt);
}


unsigned int Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::size() const
{
	return GetBase()->GetBase()->size();
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::ensureExtraCapacity(unsigned int length)
{
	GetBase()->GetBase()->ensureExtraCapacity(length);
}

int Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::elementIndexOf(const double &toCheck) const
{
	return GetBase()->GetBase()->elementIndexOf(toCheck);
}

unsigned Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Getorder() const
{
	return m_order;
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Setorder(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Setorder().");
	}
	m_order = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7Jrsdim_LocalPtr Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Getdim() const
{
	return GetBase()->Getdim();
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Setdim(const Mp7Jrsdim_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Setdim().");
	}
	GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("order")) == 0)
	{
		// order is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ProbabilityDistributionType_Cumulant_Value_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ProbabilityDistributionType_Cumulant_Value_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ProbabilityDistributionType_Cumulant_Value_LocalType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_order);
		element->setAttributeNS(X(""), X("order"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("order")))
	{
		// deserialize value type
		this->Setorder(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("order"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDoubleMatrixType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsProbabilityDistributionType_Cumulant_Value_LocalType_ExtMethodImpl.h


