
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtImplInclude.h


#include "Mp7JrsdoubleVector.h"
#include "Mp7Jrsdim_LocalType.h"
#include "Mp7JrsDoubleDiagonalMatrixType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsDoubleDiagonalMatrixType::IMp7JrsDoubleDiagonalMatrixType()
{

// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtPropInit.h

}

IMp7JrsDoubleDiagonalMatrixType::~IMp7JrsDoubleDiagonalMatrixType()
{
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtPropCleanup.h

}

Mp7JrsDoubleDiagonalMatrixType::Mp7JrsDoubleDiagonalMatrixType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDoubleDiagonalMatrixType::~Mp7JrsDoubleDiagonalMatrixType()
{
	Cleanup();
}

void Mp7JrsDoubleDiagonalMatrixType::Init()
{
	// Init base
	m_Base = CreatedoubleVector; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_dim = Mp7Jrsdim_LocalPtr(); // Create emtpy class ptr



// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMyPropInit.h

}

void Mp7JrsDoubleDiagonalMatrixType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_dim);
}

void Mp7JrsDoubleDiagonalMatrixType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DoubleDiagonalMatrixTypePtr, since we
	// might need GetBase(), which isn't defined in IDoubleDiagonalMatrixType
	const Dc1Ptr< Mp7JrsDoubleDiagonalMatrixType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsdoubleVectorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDoubleDiagonalMatrixType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_dim);
		this->Setdim(Dc1Factory::CloneObject(tmp->Getdim()));
	}
}

Dc1NodePtr Mp7JrsDoubleDiagonalMatrixType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsDoubleDiagonalMatrixType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsdoubleVector > Mp7JrsDoubleDiagonalMatrixType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Mp7JrsdoubleVector > Mp7JrsDoubleDiagonalMatrixType::GetBaseCollection() const
{
	return GetBase();
}

void Mp7JrsDoubleDiagonalMatrixType::Initialize(unsigned int maxElems)
{
	GetBase()->Initialize(maxElems);
}

void Mp7JrsDoubleDiagonalMatrixType::addElement(const double &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::addElement().");
	}
	GetBase()->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoubleDiagonalMatrixType::setElementAt(const double &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::setElementAt().");
	}
	GetBase()->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
	m_myself.getPointer(), DC1_NA_NODE_UPDATED,
	client, m_myself.getPointer());
}

void Mp7JrsDoubleDiagonalMatrixType::insertElementAt(const double &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::insertElementAt().");
	}
	GetBase()->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoubleDiagonalMatrixType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::removeAllElements().");
	}
	GetBase()->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDoubleDiagonalMatrixType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::removeElementAt().");
	}
	GetBase()->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsDoubleDiagonalMatrixType::containsElement(const double &toCheck)
{
	return GetBase()->containsElement(toCheck);
}

unsigned int Mp7JrsDoubleDiagonalMatrixType::curCapacity() const
{
	return GetBase()->curCapacity();
}

const double Mp7JrsDoubleDiagonalMatrixType::elementAt(unsigned int getAt) const
{
	return GetBase()->elementAt(getAt);
}

double Mp7JrsDoubleDiagonalMatrixType::elementAt(unsigned int getAt)
{
	return GetBase()->elementAt(getAt);
}


unsigned int Mp7JrsDoubleDiagonalMatrixType::size() const
{
	return GetBase()->size();
}

void Mp7JrsDoubleDiagonalMatrixType::ensureExtraCapacity(unsigned int length)
{
	GetBase()->ensureExtraCapacity(length);
}

int Mp7JrsDoubleDiagonalMatrixType::elementIndexOf(const double &toCheck) const
{
	return GetBase()->elementIndexOf(toCheck);
}

Mp7Jrsdim_LocalPtr Mp7JrsDoubleDiagonalMatrixType::Getdim() const
{
	return m_dim;
}

void Mp7JrsDoubleDiagonalMatrixType::Setdim(const Mp7Jrsdim_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDoubleDiagonalMatrixType::Setdim().");
	}
	m_dim = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dim) m_dim->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDoubleDiagonalMatrixType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dim")) == 0)
	{
		// dim is simple attribute dim_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsdim_LocalPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DoubleDiagonalMatrixType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DoubleDiagonalMatrixType");
	}
	return result;
}

XMLCh * Mp7JrsDoubleDiagonalMatrixType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsDoubleDiagonalMatrixType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsDoubleDiagonalMatrixType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsDoubleDiagonalMatrixType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsDoubleDiagonalMatrixType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DoubleDiagonalMatrixType"));
	// Attribute Serialization:
	// Class
	if (m_dim != Mp7Jrsdim_LocalPtr())
	{
		XMLCh * tmp = m_dim->ToText();
		element->setAttributeNS(X("urn:mpeg:mpeg7:schema:2004"), X("dim"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsDoubleDiagonalMatrixType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("urn:mpeg:mpeg7:schema:2004"), X("dim")))
	{
		// Deserialize class type
		Mp7Jrsdim_LocalPtr tmp = Createdim_LocalType; // FTT, check this
		tmp->Parse(parent->getAttributeNS(X("urn:mpeg:mpeg7:schema:2004"), X("dim")));
		this->Setdim(tmp);
		* current = parent;
	}
  // Extensionbase is Mp7JrsdoubleVector
  // Deserialize cce factory type
  if(m_Base->Deserialize(doc, parent, current))
  {
	found = true;
	parent = * current;
	parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
  }
// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDoubleDiagonalMatrixType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDoubleDiagonalMatrixType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDoubleDiagonalMatrixType_ExtMethodImpl.h


