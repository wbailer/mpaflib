
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtImplInclude.h


#include "Mp7JrsCollectionType.h"
#include "Mp7JrsContentCollectionType_CollectionType.h"
#include "Mp7JrsContentCollectionType_CollectionType0.h"
#include "Mp7JrsContentCollectionType_CollectionType1.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsCollectionType_TextAnnotation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsContentCollectionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextAnnotationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsContentCollectionType_LocalType.h" // Choice collection VisualFeature
#include "Mp7JrsVisualDType.h" // Choice collection element VisualFeature
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsAudioDType.h" // Choice collection element AudioFeature
#include "Mp7JrsContentCollectionType_LocalType0.h" // Choice collection Content
#include "Mp7JrsMultimediaContentType.h" // Choice collection element Content
#include "Mp7JrsContentCollectionType_LocalType1.h" // Choice collection ContentCollection
#include "Mp7JrsContentCollectionType.h" // Choice collection element ContentCollection

#include <assert.h>
IMp7JrsContentCollectionType::IMp7JrsContentCollectionType()
{

// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtPropInit.h

}

IMp7JrsContentCollectionType::~IMp7JrsContentCollectionType()
{
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtPropCleanup.h

}

Mp7JrsContentCollectionType::Mp7JrsContentCollectionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsContentCollectionType::~Mp7JrsContentCollectionType()
{
	Cleanup();
}

void Mp7JrsContentCollectionType::Init()
{
	// Init base
	m_Base = CreateCollectionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ContentCollectionType_LocalType = Mp7JrsContentCollectionType_CollectionPtr(); // Collection
	m_ContentCollectionType_LocalType0 = Mp7JrsContentCollectionType_Collection0Ptr(); // Collection
	m_ContentCollectionType_LocalType1 = Mp7JrsContentCollectionType_Collection1Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtMyPropInit.h

}

void Mp7JrsContentCollectionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType);
	// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType0);
	// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType1);
}

void Mp7JrsContentCollectionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContentCollectionTypePtr, since we
	// might need GetBase(), which isn't defined in IContentCollectionType
	const Dc1Ptr< Mp7JrsContentCollectionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsCollectionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsContentCollectionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType);
		this->SetContentCollectionType_LocalType(Dc1Factory::CloneObject(tmp->GetContentCollectionType_LocalType()));
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType0);
		this->SetContentCollectionType_LocalType0(Dc1Factory::CloneObject(tmp->GetContentCollectionType_LocalType0()));
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType1);
		this->SetContentCollectionType_LocalType1(Dc1Factory::CloneObject(tmp->GetContentCollectionType_LocalType1()));
}

Dc1NodePtr Mp7JrsContentCollectionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsContentCollectionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsCollectionType > Mp7JrsContentCollectionType::GetBase() const
{
	return m_Base;
}

Mp7JrsContentCollectionType_CollectionPtr Mp7JrsContentCollectionType::GetContentCollectionType_LocalType() const
{
		return m_ContentCollectionType_LocalType;
}

Mp7JrsContentCollectionType_Collection0Ptr Mp7JrsContentCollectionType::GetContentCollectionType_LocalType0() const
{
		return m_ContentCollectionType_LocalType0;
}

Mp7JrsContentCollectionType_Collection1Ptr Mp7JrsContentCollectionType::GetContentCollectionType_LocalType1() const
{
		return m_ContentCollectionType_LocalType1;
}

void Mp7JrsContentCollectionType::SetContentCollectionType_LocalType(const Mp7JrsContentCollectionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetContentCollectionType_LocalType().");
	}
	if (m_ContentCollectionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType);
		m_ContentCollectionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContentCollectionType_LocalType) m_ContentCollectionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::SetContentCollectionType_LocalType0(const Mp7JrsContentCollectionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetContentCollectionType_LocalType0().");
	}
	if (m_ContentCollectionType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType0);
		m_ContentCollectionType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContentCollectionType_LocalType0) m_ContentCollectionType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::SetContentCollectionType_LocalType1(const Mp7JrsContentCollectionType_Collection1Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetContentCollectionType_LocalType1().");
	}
	if (m_ContentCollectionType_LocalType1 != item)
	{
		// Dc1Factory::DeleteObject(m_ContentCollectionType_LocalType1);
		m_ContentCollectionType_LocalType1 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ContentCollectionType_LocalType1) m_ContentCollectionType_LocalType1->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsContentCollectionType::Getname() const
{
	return GetBase()->Getname();
}

bool Mp7JrsContentCollectionType::Existname() const
{
	return GetBase()->Existname();
}
void Mp7JrsContentCollectionType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::Setname().");
	}
	GetBase()->Setname(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::Invalidatename()
{
	GetBase()->Invalidatename();
}
Mp7JrsCreationInformationPtr Mp7JrsContentCollectionType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsContentCollectionType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsContentCollectionType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsContentCollectionType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsCollectionType_TextAnnotation_CollectionPtr Mp7JrsContentCollectionType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsContentCollectionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsContentCollectionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsContentCollectionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsContentCollectionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::SetTextAnnotation(const Mp7JrsCollectionType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsContentCollectionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsContentCollectionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsContentCollectionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsContentCollectionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsContentCollectionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsContentCollectionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsContentCollectionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsContentCollectionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsContentCollectionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsContentCollectionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsContentCollectionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsContentCollectionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsContentCollectionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsContentCollectionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsContentCollectionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentCollectionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsContentCollectionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsContentCollectionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsContentCollectionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VisualFeature")) == 0)
	{
		// VisualFeature is contained in itemtype ContentCollectionType_LocalType
		// in choice collection ContentCollectionType_CollectionType

		context->Found = true;
		Mp7JrsContentCollectionType_CollectionPtr coll = GetContentCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType; // FTT, check this
				SetContentCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetVisualFeature()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_LocalPtr item = CreateContentCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->SetVisualFeature(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetVisualFeature()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GofGopFeature")) == 0)
	{
		// GofGopFeature is contained in itemtype ContentCollectionType_LocalType
		// in choice collection ContentCollectionType_CollectionType

		context->Found = true;
		Mp7JrsContentCollectionType_CollectionPtr coll = GetContentCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType; // FTT, check this
				SetContentCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetGofGopFeature()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_LocalPtr item = CreateContentCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGofGopFeaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->SetGofGopFeature(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetGofGopFeature()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioFeature")) == 0)
	{
		// AudioFeature is contained in itemtype ContentCollectionType_LocalType
		// in choice collection ContentCollectionType_CollectionType

		context->Found = true;
		Mp7JrsContentCollectionType_CollectionPtr coll = GetContentCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType; // FTT, check this
				SetContentCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetAudioFeature()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_LocalPtr item = CreateContentCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAudioDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->SetAudioFeature(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_LocalPtr)coll->elementAt(i))->GetAudioFeature()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Content")) == 0)
	{
		// Content is contained in itemtype ContentCollectionType_LocalType0
		// in choice collection ContentCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsContentCollectionType_Collection0Ptr coll = GetContentCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType0; // FTT, check this
				SetContentCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->GetContent()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_Local0Ptr item = CreateContentCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMultimediaContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->SetContent(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->GetContent()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContentRef")) == 0)
	{
		// ContentRef is contained in itemtype ContentCollectionType_LocalType0
		// in choice collection ContentCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsContentCollectionType_Collection0Ptr coll = GetContentCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType0; // FTT, check this
				SetContentCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->GetContentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_Local0Ptr item = CreateContentCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->SetContentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_Local0Ptr)coll->elementAt(i))->GetContentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContentCollection")) == 0)
	{
		// ContentCollection is contained in itemtype ContentCollectionType_LocalType1
		// in choice collection ContentCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsContentCollectionType_Collection1Ptr coll = GetContentCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType1; // FTT, check this
				SetContentCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->GetContentCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_Local1Ptr item = CreateContentCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContentCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsContentCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->SetContentCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->GetContentCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContentCollectionRef")) == 0)
	{
		// ContentCollectionRef is contained in itemtype ContentCollectionType_LocalType1
		// in choice collection ContentCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsContentCollectionType_Collection1Ptr coll = GetContentCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentCollectionType_CollectionType1; // FTT, check this
				SetContentCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->GetContentCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsContentCollectionType_Local1Ptr item = CreateContentCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->SetContentCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsContentCollectionType_Local1Ptr)coll->elementAt(i))->GetContentCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ContentCollectionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ContentCollectionType");
	}
	return result;
}

XMLCh * Mp7JrsContentCollectionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsContentCollectionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsContentCollectionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ContentCollectionType"));
	// Element serialization:
	if (m_ContentCollectionType_LocalType != Mp7JrsContentCollectionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ContentCollectionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ContentCollectionType_LocalType0 != Mp7JrsContentCollectionType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ContentCollectionType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ContentCollectionType_LocalType1 != Mp7JrsContentCollectionType_Collection1Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ContentCollectionType_LocalType1->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsContentCollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsCollectionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:VisualFeature
			Dc1Util::HasNodeName(parent, X("VisualFeature"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualFeature")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:GofGopFeature
			Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GofGopFeature")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:AudioFeature
			Dc1Util::HasNodeName(parent, X("AudioFeature"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:AudioFeature")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsContentCollectionType_CollectionPtr tmp = CreateContentCollectionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetContentCollectionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Content
			Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Content")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ContentRef
			Dc1Util::HasNodeName(parent, X("ContentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ContentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsContentCollectionType_Collection0Ptr tmp = CreateContentCollectionType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetContentCollectionType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:ContentCollection
			Dc1Util::HasNodeName(parent, X("ContentCollection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ContentCollection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ContentCollectionRef
			Dc1Util::HasNodeName(parent, X("ContentCollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ContentCollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsContentCollectionType_Collection1Ptr tmp = CreateContentCollectionType_CollectionType1; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetContentCollectionType_LocalType1(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsContentCollectionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualFeature"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualFeature")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("GofGopFeature")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("AudioFeature"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("AudioFeature")) == 0)
	)
  {
	Mp7JrsContentCollectionType_CollectionPtr tmp = CreateContentCollectionType_CollectionType; // FTT, check this
	this->SetContentCollectionType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Content")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ContentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ContentRef")) == 0)
	)
  {
	Mp7JrsContentCollectionType_Collection0Ptr tmp = CreateContentCollectionType_CollectionType0; // FTT, check this
	this->SetContentCollectionType_LocalType0(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ContentCollection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ContentCollection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ContentCollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ContentCollectionRef")) == 0)
	)
  {
	Mp7JrsContentCollectionType_Collection1Ptr tmp = CreateContentCollectionType_CollectionType1; // FTT, check this
	this->SetContentCollectionType_LocalType1(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsContentCollectionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContentCollectionType_LocalType() != Dc1NodePtr())
		result.Insert(GetContentCollectionType_LocalType());
	if (GetContentCollectionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetContentCollectionType_LocalType0());
	if (GetContentCollectionType_LocalType1() != Dc1NodePtr())
		result.Insert(GetContentCollectionType_LocalType1());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsContentCollectionType_ExtMethodImpl.h


