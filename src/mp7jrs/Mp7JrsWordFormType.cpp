
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtImplInclude.h


#include "Mp7JrsWordFormType_terms_CollectionType.h"
#include "Mp7JrsWordFormType_type_CollectionType.h"
#include "Mp7JrsWordFormType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsWordFormType::IMp7JrsWordFormType()
{

// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtPropInit.h

}

IMp7JrsWordFormType::~IMp7JrsWordFormType()
{
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtPropCleanup.h

}

Mp7JrsWordFormType::Mp7JrsWordFormType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsWordFormType::~Mp7JrsWordFormType()
{
	Cleanup();
}

void Mp7JrsWordFormType::Init()
{
	// Init base
	m_Base = XMLString::transcode("");

	// Init attributes
	m_terms = Mp7JrsWordFormType_terms_CollectionPtr(); // Collection
	m_terms_Exist = false;
	m_id = NULL; // String
	m_id_Exist = false;
	m_equal = NULL; // String
	m_equal_Exist = false;
	m_type = Mp7JrsWordFormType_type_CollectionPtr(); // Collection
	m_type_Exist = false;
	m_baseForm = NULL; // String
	m_baseForm_Exist = false;



// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMyPropInit.h

}

void Mp7JrsWordFormType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMyPropCleanup.h


	XMLString::release(&m_Base);
	// Dc1Factory::DeleteObject(m_terms);
	XMLString::release(&m_id); // String
	XMLString::release(&m_equal); // String
	// Dc1Factory::DeleteObject(m_type);
	XMLString::release(&m_baseForm); // String
}

void Mp7JrsWordFormType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use WordFormTypePtr, since we
	// might need GetBase(), which isn't defined in IWordFormType
	const Dc1Ptr< Mp7JrsWordFormType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	XMLString::release(&m_Base);
	m_Base = XMLString::replicate(tmp->GetContent());
	{
	// Dc1Factory::DeleteObject(m_terms);
	if (tmp->Existterms())
	{
		this->Setterms(Dc1Factory::CloneObject(tmp->Getterms()));
	}
	else
	{
		Invalidateterms();
	}
	}
	{
	XMLString::release(&m_id); // String
	if (tmp->Existid())
	{
		this->Setid(XMLString::replicate(tmp->Getid()));
	}
	else
	{
		Invalidateid();
	}
	}
	{
	XMLString::release(&m_equal); // String
	if (tmp->Existequal())
	{
		this->Setequal(XMLString::replicate(tmp->Getequal()));
	}
	else
	{
		Invalidateequal();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
	{
	XMLString::release(&m_baseForm); // String
	if (tmp->ExistbaseForm())
	{
		this->SetbaseForm(XMLString::replicate(tmp->GetbaseForm()));
	}
	else
	{
		InvalidatebaseForm();
	}
	}
}

Dc1NodePtr Mp7JrsWordFormType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsWordFormType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsWordFormType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::SetContent().");
	}
	if (m_Base != item)
	{
		XMLString::release(&m_Base);
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsWordFormType::GetContent() const
{
	return m_Base;
}
Mp7JrsWordFormType_terms_CollectionPtr Mp7JrsWordFormType::Getterms() const
{
	return m_terms;
}

bool Mp7JrsWordFormType::Existterms() const
{
	return m_terms_Exist;
}
void Mp7JrsWordFormType::Setterms(const Mp7JrsWordFormType_terms_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::Setterms().");
	}
	m_terms = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_terms) m_terms->SetParent(m_myself.getPointer());
	m_terms_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordFormType::Invalidateterms()
{
	m_terms_Exist = false;
}
XMLCh * Mp7JrsWordFormType::Getid() const
{
	return m_id;
}

bool Mp7JrsWordFormType::Existid() const
{
	return m_id_Exist;
}
void Mp7JrsWordFormType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::Setid().");
	}
	m_id = item;
	m_id_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordFormType::Invalidateid()
{
	m_id_Exist = false;
}
XMLCh * Mp7JrsWordFormType::Getequal() const
{
	return m_equal;
}

bool Mp7JrsWordFormType::Existequal() const
{
	return m_equal_Exist;
}
void Mp7JrsWordFormType::Setequal(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::Setequal().");
	}
	m_equal = item;
	m_equal_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordFormType::Invalidateequal()
{
	m_equal_Exist = false;
}
Mp7JrsWordFormType_type_CollectionPtr Mp7JrsWordFormType::Gettype() const
{
	return m_type;
}

bool Mp7JrsWordFormType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsWordFormType::Settype(const Mp7JrsWordFormType_type_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordFormType::Invalidatetype()
{
	m_type_Exist = false;
}
XMLCh * Mp7JrsWordFormType::GetbaseForm() const
{
	return m_baseForm;
}

bool Mp7JrsWordFormType::ExistbaseForm() const
{
	return m_baseForm_Exist;
}
void Mp7JrsWordFormType::SetbaseForm(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsWordFormType::SetbaseForm().");
	}
	m_baseForm = item;
	m_baseForm_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsWordFormType::InvalidatebaseForm()
{
	m_baseForm_Exist = false;
}

Dc1NodeEnum Mp7JrsWordFormType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("terms")) == 0)
	{
		// terms is simple attribute WordFormType_terms_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsWordFormType_terms_CollectionPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("id")) == 0)
	{
		// id is simple attribute ID
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("equal")) == 0)
	{
		// equal is simple attribute IDREF
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute WordFormType_type_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsWordFormType_type_CollectionPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("baseForm")) == 0)
	{
		// baseForm is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for WordFormType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "WordFormType");
	}
	return result;
}

XMLCh * Mp7JrsWordFormType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsWordFormType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsWordFormType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsWordFormType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsWordFormType::ContentToString() const
{
	// Mp7JrsWordFormType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsWordFormType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	element->appendChild(doc->createTextNode(m_Base));

	assert(element);
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("WordFormType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_terms_Exist)
	{
	// Collection
	if(m_terms != Mp7JrsWordFormType_terms_CollectionPtr())
	{
		XMLCh * tmp = m_terms->ToText();
		element->setAttributeNS(X(""), X("terms"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_id_Exist)
	{
	// String
	if(m_id != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("id"), m_id);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_equal_Exist)
	{
	// String
	if(m_equal != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("equal"), m_equal);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Collection
	if(m_type != Mp7JrsWordFormType_type_CollectionPtr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_baseForm_Exist)
	{
	// String
	if(m_baseForm != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("baseForm"), m_baseForm);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsWordFormType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("terms")))
	{
		// Deserialize collection type
		Mp7JrsWordFormType_terms_CollectionPtr tmp = CreateWordFormType_terms_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("terms")));
		this->Setterms(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("id")))
	{
		// Deserialize string type
		this->Setid(Dc1Convert::TextToString(parent->getAttribute(X("id"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("equal")))
	{
		// Deserialize string type
		this->Setequal(Dc1Convert::TextToString(parent->getAttribute(X("equal"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize collection type
		Mp7JrsWordFormType_type_CollectionPtr tmp = CreateWordFormType_type_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("baseForm")))
	{
		// Deserialize string type
		this->SetbaseForm(Dc1Convert::TextToString(parent->getAttribute(X("baseForm"))));
		* current = parent;
	}

	XMLCh * tmp = Dc1Convert::TextToString(Dc1Util::GetElementText(parent));
	if(tmp != NULL)
	{
		XMLString::release(&m_Base);
		m_Base = tmp;
		found = true;
	}
// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsWordFormType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsWordFormType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsWordFormType_ExtMethodImpl.h


