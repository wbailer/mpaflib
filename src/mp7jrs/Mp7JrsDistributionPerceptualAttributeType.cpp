
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsPerceptualGenreDistributionType.h"
#include "Mp7JrsPerceptualMoodDistributionType.h"
#include "Mp7JrsPerceptualLyricsDistributionType.h"
#include "Mp7JrsPerceptualInstrumentationDistributionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsDistributionPerceptualAttributeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsDistributionPerceptualAttributeType::IMp7JrsDistributionPerceptualAttributeType()
{

// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtPropInit.h

}

IMp7JrsDistributionPerceptualAttributeType::~IMp7JrsDistributionPerceptualAttributeType()
{
// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtPropCleanup.h

}

Mp7JrsDistributionPerceptualAttributeType::Mp7JrsDistributionPerceptualAttributeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDistributionPerceptualAttributeType::~Mp7JrsDistributionPerceptualAttributeType()
{
	Cleanup();
}

void Mp7JrsDistributionPerceptualAttributeType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_PerceptualGenreDistribution = Mp7JrsPerceptualGenreDistributionPtr(); // Class
	m_PerceptualGenreDistribution_Exist = false;
	m_PerceptualMoodDistribution = Mp7JrsPerceptualMoodDistributionPtr(); // Class
	m_PerceptualMoodDistribution_Exist = false;
	m_PerceptualLyricsDistribution = Mp7JrsPerceptualLyricsDistributionPtr(); // Class
	m_PerceptualLyricsDistribution_Exist = false;
	m_PerceptualInstrumentationDistribution = Mp7JrsPerceptualInstrumentationDistributionPtr(); // Class
	m_PerceptualInstrumentationDistribution_Exist = false;


// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtMyPropInit.h

}

void Mp7JrsDistributionPerceptualAttributeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PerceptualGenreDistribution);
	// Dc1Factory::DeleteObject(m_PerceptualMoodDistribution);
	// Dc1Factory::DeleteObject(m_PerceptualLyricsDistribution);
	// Dc1Factory::DeleteObject(m_PerceptualInstrumentationDistribution);
}

void Mp7JrsDistributionPerceptualAttributeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DistributionPerceptualAttributeTypePtr, since we
	// might need GetBase(), which isn't defined in IDistributionPerceptualAttributeType
	const Dc1Ptr< Mp7JrsDistributionPerceptualAttributeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDistributionPerceptualAttributeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidPerceptualGenreDistribution())
	{
		// Dc1Factory::DeleteObject(m_PerceptualGenreDistribution);
		this->SetPerceptualGenreDistribution(Dc1Factory::CloneObject(tmp->GetPerceptualGenreDistribution()));
	}
	else
	{
		InvalidatePerceptualGenreDistribution();
	}
	if (tmp->IsValidPerceptualMoodDistribution())
	{
		// Dc1Factory::DeleteObject(m_PerceptualMoodDistribution);
		this->SetPerceptualMoodDistribution(Dc1Factory::CloneObject(tmp->GetPerceptualMoodDistribution()));
	}
	else
	{
		InvalidatePerceptualMoodDistribution();
	}
	if (tmp->IsValidPerceptualLyricsDistribution())
	{
		// Dc1Factory::DeleteObject(m_PerceptualLyricsDistribution);
		this->SetPerceptualLyricsDistribution(Dc1Factory::CloneObject(tmp->GetPerceptualLyricsDistribution()));
	}
	else
	{
		InvalidatePerceptualLyricsDistribution();
	}
	if (tmp->IsValidPerceptualInstrumentationDistribution())
	{
		// Dc1Factory::DeleteObject(m_PerceptualInstrumentationDistribution);
		this->SetPerceptualInstrumentationDistribution(Dc1Factory::CloneObject(tmp->GetPerceptualInstrumentationDistribution()));
	}
	else
	{
		InvalidatePerceptualInstrumentationDistribution();
	}
}

Dc1NodePtr Mp7JrsDistributionPerceptualAttributeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDistributionPerceptualAttributeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsDistributionPerceptualAttributeType::GetBase() const
{
	return m_Base;
}

Mp7JrsPerceptualGenreDistributionPtr Mp7JrsDistributionPerceptualAttributeType::GetPerceptualGenreDistribution() const
{
		return m_PerceptualGenreDistribution;
}

// Element is optional
bool Mp7JrsDistributionPerceptualAttributeType::IsValidPerceptualGenreDistribution() const
{
	return m_PerceptualGenreDistribution_Exist;
}

Mp7JrsPerceptualMoodDistributionPtr Mp7JrsDistributionPerceptualAttributeType::GetPerceptualMoodDistribution() const
{
		return m_PerceptualMoodDistribution;
}

// Element is optional
bool Mp7JrsDistributionPerceptualAttributeType::IsValidPerceptualMoodDistribution() const
{
	return m_PerceptualMoodDistribution_Exist;
}

Mp7JrsPerceptualLyricsDistributionPtr Mp7JrsDistributionPerceptualAttributeType::GetPerceptualLyricsDistribution() const
{
		return m_PerceptualLyricsDistribution;
}

// Element is optional
bool Mp7JrsDistributionPerceptualAttributeType::IsValidPerceptualLyricsDistribution() const
{
	return m_PerceptualLyricsDistribution_Exist;
}

Mp7JrsPerceptualInstrumentationDistributionPtr Mp7JrsDistributionPerceptualAttributeType::GetPerceptualInstrumentationDistribution() const
{
		return m_PerceptualInstrumentationDistribution;
}

// Element is optional
bool Mp7JrsDistributionPerceptualAttributeType::IsValidPerceptualInstrumentationDistribution() const
{
	return m_PerceptualInstrumentationDistribution_Exist;
}

void Mp7JrsDistributionPerceptualAttributeType::SetPerceptualGenreDistribution(const Mp7JrsPerceptualGenreDistributionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDistributionPerceptualAttributeType::SetPerceptualGenreDistribution().");
	}
	if (m_PerceptualGenreDistribution != item || m_PerceptualGenreDistribution_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualGenreDistribution);
		m_PerceptualGenreDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualGenreDistribution) m_PerceptualGenreDistribution->SetParent(m_myself.getPointer());
		if (m_PerceptualGenreDistribution && m_PerceptualGenreDistribution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualGenreDistribution->UseTypeAttribute = true;
		}
		m_PerceptualGenreDistribution_Exist = true;
		if(m_PerceptualGenreDistribution != Mp7JrsPerceptualGenreDistributionPtr())
		{
			m_PerceptualGenreDistribution->SetContentName(XMLString::transcode("PerceptualGenreDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDistributionPerceptualAttributeType::InvalidatePerceptualGenreDistribution()
{
	m_PerceptualGenreDistribution_Exist = false;
}
void Mp7JrsDistributionPerceptualAttributeType::SetPerceptualMoodDistribution(const Mp7JrsPerceptualMoodDistributionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDistributionPerceptualAttributeType::SetPerceptualMoodDistribution().");
	}
	if (m_PerceptualMoodDistribution != item || m_PerceptualMoodDistribution_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualMoodDistribution);
		m_PerceptualMoodDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualMoodDistribution) m_PerceptualMoodDistribution->SetParent(m_myself.getPointer());
		if (m_PerceptualMoodDistribution && m_PerceptualMoodDistribution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualMoodDistribution->UseTypeAttribute = true;
		}
		m_PerceptualMoodDistribution_Exist = true;
		if(m_PerceptualMoodDistribution != Mp7JrsPerceptualMoodDistributionPtr())
		{
			m_PerceptualMoodDistribution->SetContentName(XMLString::transcode("PerceptualMoodDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDistributionPerceptualAttributeType::InvalidatePerceptualMoodDistribution()
{
	m_PerceptualMoodDistribution_Exist = false;
}
void Mp7JrsDistributionPerceptualAttributeType::SetPerceptualLyricsDistribution(const Mp7JrsPerceptualLyricsDistributionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDistributionPerceptualAttributeType::SetPerceptualLyricsDistribution().");
	}
	if (m_PerceptualLyricsDistribution != item || m_PerceptualLyricsDistribution_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualLyricsDistribution);
		m_PerceptualLyricsDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualLyricsDistribution) m_PerceptualLyricsDistribution->SetParent(m_myself.getPointer());
		if (m_PerceptualLyricsDistribution && m_PerceptualLyricsDistribution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualLyricsDistribution->UseTypeAttribute = true;
		}
		m_PerceptualLyricsDistribution_Exist = true;
		if(m_PerceptualLyricsDistribution != Mp7JrsPerceptualLyricsDistributionPtr())
		{
			m_PerceptualLyricsDistribution->SetContentName(XMLString::transcode("PerceptualLyricsDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDistributionPerceptualAttributeType::InvalidatePerceptualLyricsDistribution()
{
	m_PerceptualLyricsDistribution_Exist = false;
}
void Mp7JrsDistributionPerceptualAttributeType::SetPerceptualInstrumentationDistribution(const Mp7JrsPerceptualInstrumentationDistributionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDistributionPerceptualAttributeType::SetPerceptualInstrumentationDistribution().");
	}
	if (m_PerceptualInstrumentationDistribution != item || m_PerceptualInstrumentationDistribution_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptualInstrumentationDistribution);
		m_PerceptualInstrumentationDistribution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptualInstrumentationDistribution) m_PerceptualInstrumentationDistribution->SetParent(m_myself.getPointer());
		if (m_PerceptualInstrumentationDistribution && m_PerceptualInstrumentationDistribution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptualInstrumentationDistribution->UseTypeAttribute = true;
		}
		m_PerceptualInstrumentationDistribution_Exist = true;
		if(m_PerceptualInstrumentationDistribution != Mp7JrsPerceptualInstrumentationDistributionPtr())
		{
			m_PerceptualInstrumentationDistribution->SetContentName(XMLString::transcode("PerceptualInstrumentationDistribution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDistributionPerceptualAttributeType::InvalidatePerceptualInstrumentationDistribution()
{
	m_PerceptualInstrumentationDistribution_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsDistributionPerceptualAttributeType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsDistributionPerceptualAttributeType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsDistributionPerceptualAttributeType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDistributionPerceptualAttributeType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDistributionPerceptualAttributeType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsDistributionPerceptualAttributeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("PerceptualGenreDistribution")) == 0)
	{
		// PerceptualGenreDistribution is simple element PerceptualGenreDistributionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualGenreDistribution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualGenreDistributionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualGenreDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualGenreDistribution(p, client);
					if((p = GetPerceptualGenreDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualMoodDistribution")) == 0)
	{
		// PerceptualMoodDistribution is simple element PerceptualMoodDistributionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualMoodDistribution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualMoodDistributionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualMoodDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualMoodDistribution(p, client);
					if((p = GetPerceptualMoodDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualLyricsDistribution")) == 0)
	{
		// PerceptualLyricsDistribution is simple element PerceptualLyricsDistributionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualLyricsDistribution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualLyricsDistributionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualLyricsDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualLyricsDistribution(p, client);
					if((p = GetPerceptualLyricsDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptualInstrumentationDistribution")) == 0)
	{
		// PerceptualInstrumentationDistribution is simple element PerceptualInstrumentationDistributionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptualInstrumentationDistribution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PerceptualInstrumentationDistributionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptualInstrumentationDistributionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptualInstrumentationDistribution(p, client);
					if((p = GetPerceptualInstrumentationDistribution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DistributionPerceptualAttributeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DistributionPerceptualAttributeType");
	}
	return result;
}

XMLCh * Mp7JrsDistributionPerceptualAttributeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDistributionPerceptualAttributeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDistributionPerceptualAttributeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DistributionPerceptualAttributeType"));
	// Element serialization:
	// Class
	
	if (m_PerceptualGenreDistribution != Mp7JrsPerceptualGenreDistributionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualGenreDistribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualGenreDistribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualGenreDistribution->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualMoodDistribution != Mp7JrsPerceptualMoodDistributionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualMoodDistribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualMoodDistribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualMoodDistribution->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualLyricsDistribution != Mp7JrsPerceptualLyricsDistributionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualLyricsDistribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualLyricsDistribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualLyricsDistribution->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PerceptualInstrumentationDistribution != Mp7JrsPerceptualInstrumentationDistributionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptualInstrumentationDistribution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualInstrumentationDistribution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptualInstrumentationDistribution->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsDistributionPerceptualAttributeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualGenreDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualGenreDistribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualGenreDistributionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualGenreDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualMoodDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualMoodDistribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualMoodDistributionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualMoodDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualLyricsDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualLyricsDistribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualLyricsDistributionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualLyricsDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptualInstrumentationDistribution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptualInstrumentationDistribution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptualInstrumentationDistributionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptualInstrumentationDistribution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDistributionPerceptualAttributeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("PerceptualGenreDistribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualGenreDistributionType; // FTT, check this
	}
	this->SetPerceptualGenreDistribution(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualMoodDistribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualMoodDistributionType; // FTT, check this
	}
	this->SetPerceptualMoodDistribution(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualLyricsDistribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualLyricsDistributionType; // FTT, check this
	}
	this->SetPerceptualLyricsDistribution(child);
  }
  if (XMLString::compareString(elementname, X("PerceptualInstrumentationDistribution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptualInstrumentationDistributionType; // FTT, check this
	}
	this->SetPerceptualInstrumentationDistribution(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDistributionPerceptualAttributeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPerceptualGenreDistribution() != Dc1NodePtr())
		result.Insert(GetPerceptualGenreDistribution());
	if (GetPerceptualMoodDistribution() != Dc1NodePtr())
		result.Insert(GetPerceptualMoodDistribution());
	if (GetPerceptualLyricsDistribution() != Dc1NodePtr())
		result.Insert(GetPerceptualLyricsDistribution());
	if (GetPerceptualInstrumentationDistribution() != Dc1NodePtr())
		result.Insert(GetPerceptualInstrumentationDistribution());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDistributionPerceptualAttributeType_ExtMethodImpl.h


