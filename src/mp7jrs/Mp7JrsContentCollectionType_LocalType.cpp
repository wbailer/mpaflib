
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsGofGopFeatureType.h"
#include "Mp7JrsAudioDType.h"
#include "Mp7JrsContentCollectionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsContentCollectionType_LocalType::IMp7JrsContentCollectionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtPropInit.h

}

IMp7JrsContentCollectionType_LocalType::~IMp7JrsContentCollectionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtPropCleanup.h

}

Mp7JrsContentCollectionType_LocalType::Mp7JrsContentCollectionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsContentCollectionType_LocalType::~Mp7JrsContentCollectionType_LocalType()
{
	Cleanup();
}

void Mp7JrsContentCollectionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_VisualFeature = Dc1Ptr< Dc1Node >(); // Class
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr(); // Class
	m_AudioFeature = Dc1Ptr< Dc1Node >(); // Class


// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsContentCollectionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_VisualFeature);
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	// Dc1Factory::DeleteObject(m_AudioFeature);
}

void Mp7JrsContentCollectionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContentCollectionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IContentCollectionType_LocalType
	const Dc1Ptr< Mp7JrsContentCollectionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_VisualFeature);
		this->SetVisualFeature(Dc1Factory::CloneObject(tmp->GetVisualFeature()));
		// Dc1Factory::DeleteObject(m_GofGopFeature);
		this->SetGofGopFeature(Dc1Factory::CloneObject(tmp->GetGofGopFeature()));
		// Dc1Factory::DeleteObject(m_AudioFeature);
		this->SetAudioFeature(Dc1Factory::CloneObject(tmp->GetAudioFeature()));
}

Dc1NodePtr Mp7JrsContentCollectionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsContentCollectionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1Ptr< Dc1Node > Mp7JrsContentCollectionType_LocalType::GetVisualFeature() const
{
		return m_VisualFeature;
}

Mp7JrsGofGopFeaturePtr Mp7JrsContentCollectionType_LocalType::GetGofGopFeature() const
{
		return m_GofGopFeature;
}

Dc1Ptr< Dc1Node > Mp7JrsContentCollectionType_LocalType::GetAudioFeature() const
{
		return m_AudioFeature;
}

// implementing setter for choice 
/* element */
void Mp7JrsContentCollectionType_LocalType::SetVisualFeature(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType_LocalType::SetVisualFeature().");
	}
	if (m_VisualFeature != item)
	{
		// Dc1Factory::DeleteObject(m_VisualFeature);
		m_VisualFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualFeature) m_VisualFeature->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualFeature) m_VisualFeature->UseTypeAttribute = true;
		if(m_VisualFeature != Dc1Ptr< Dc1Node >())
		{
			m_VisualFeature->SetContentName(XMLString::transcode("VisualFeature"));
		}
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr();
	// Dc1Factory::DeleteObject(m_AudioFeature);
	m_AudioFeature = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsContentCollectionType_LocalType::SetGofGopFeature(const Mp7JrsGofGopFeaturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType_LocalType::SetGofGopFeature().");
	}
	if (m_GofGopFeature != item)
	{
		// Dc1Factory::DeleteObject(m_GofGopFeature);
		m_GofGopFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GofGopFeature) m_GofGopFeature->SetParent(m_myself.getPointer());
		if (m_GofGopFeature && m_GofGopFeature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GofGopFeatureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GofGopFeature->UseTypeAttribute = true;
		}
		if(m_GofGopFeature != Mp7JrsGofGopFeaturePtr())
		{
			m_GofGopFeature->SetContentName(XMLString::transcode("GofGopFeature"));
		}
	// Dc1Factory::DeleteObject(m_VisualFeature);
	m_VisualFeature = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_AudioFeature);
	m_AudioFeature = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsContentCollectionType_LocalType::SetAudioFeature(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentCollectionType_LocalType::SetAudioFeature().");
	}
	if (m_AudioFeature != item)
	{
		// Dc1Factory::DeleteObject(m_AudioFeature);
		m_AudioFeature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioFeature) m_AudioFeature->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_AudioFeature) m_AudioFeature->UseTypeAttribute = true;
		if(m_AudioFeature != Dc1Ptr< Dc1Node >())
		{
			m_AudioFeature->SetContentName(XMLString::transcode("AudioFeature"));
		}
	// Dc1Factory::DeleteObject(m_VisualFeature);
	m_VisualFeature = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GofGopFeature);
	m_GofGopFeature = Mp7JrsGofGopFeaturePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: ContentCollectionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsContentCollectionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsContentCollectionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsContentCollectionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsContentCollectionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_VisualFeature != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualFeature->UseTypeAttribute = true;
		}
		m_VisualFeature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GofGopFeature != Mp7JrsGofGopFeaturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GofGopFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GofGopFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GofGopFeature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioFeature != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioFeature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioFeature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_AudioFeature->UseTypeAttribute = true;
		}
		m_AudioFeature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsContentCollectionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GofGopFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GofGopFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGofGopFeatureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGofGopFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioFeature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioFeature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioFeature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsContentCollectionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("VisualFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDType; // FTT, check this
	}
	this->SetVisualFeature(child);
  }
  if (XMLString::compareString(elementname, X("GofGopFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGofGopFeatureType; // FTT, check this
	}
	this->SetGofGopFeature(child);
  }
  if (XMLString::compareString(elementname, X("AudioFeature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioDType; // FTT, check this
	}
	this->SetAudioFeature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsContentCollectionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVisualFeature() != Dc1NodePtr())
		result.Insert(GetVisualFeature());
	if (GetGofGopFeature() != Dc1NodePtr())
		result.Insert(GetGofGopFeature());
	if (GetAudioFeature() != Dc1NodePtr())
		result.Insert(GetAudioFeature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsContentCollectionType_LocalType_ExtMethodImpl.h


