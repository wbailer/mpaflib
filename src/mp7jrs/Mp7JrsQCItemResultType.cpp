
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsQCItemResultType_CollectionType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsQCItemResultType_VerificationMedia_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsQCItemResultType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsQCItemResultType_LocalType.h" // Choice collection Output
#include "Mp7JrsOutputQCValueType.h" // Choice collection element Output
#include "Mp7JrsSegmentOutputQCValueType.h" // Choice collection element SegmentOutput
#include "Mp7JrsTitleMediaType.h" // Element collection urn:mpeg:mpeg7:schema:2004:VerificationMedia

#include <assert.h>
IMp7JrsQCItemResultType::IMp7JrsQCItemResultType()
{

// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtPropInit.h

}

IMp7JrsQCItemResultType::~IMp7JrsQCItemResultType()
{
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtPropCleanup.h

}

Mp7JrsQCItemResultType::Mp7JrsQCItemResultType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsQCItemResultType::~Mp7JrsQCItemResultType()
{
	Cleanup();
}

void Mp7JrsQCItemResultType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsControlledTermUsePtr(); // Class
	m_CheckResult = (false); // Value

	m_CheckResult_Exist = false;
	m_QCItemResultType_LocalType = Mp7JrsQCItemResultType_CollectionPtr(); // Collection
	m_Relevance = (0); // Value

	m_Relevance_Exist = false;
	m_ExecutionStatus = Mp7JrsQCItemResultType_ExecutionStatus_LocalType::UninitializedEnumeration; // Enumeration
	m_ErrorDescription = NULL; // Optional String
	m_ErrorDescription_Exist = false;
	m_ResultDescription = Mp7JrsTextAnnotationPtr(); // Class
	m_ResultDescription_Exist = false;
	m_Annotation = Mp7JrsTextAnnotationPtr(); // Class
	m_Annotation_Exist = false;
	m_VerificationMedia = Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr(); // Collection
	m_MaxSeverity = (0); // Value

	m_MaxSeverity_Exist = false;
	m_DetectionMethod = Mp7JrsQCItemResultType_DetectionMethod_LocalType::UninitializedEnumeration; // Enumeration
	m_DetectionMethod_Exist = false;


// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtMyPropInit.h

}

void Mp7JrsQCItemResultType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_QCItemResultType_LocalType);
	XMLString::release(&this->m_ErrorDescription);
	this->m_ErrorDescription = NULL;
	// Dc1Factory::DeleteObject(m_ResultDescription);
	// Dc1Factory::DeleteObject(m_Annotation);
	// Dc1Factory::DeleteObject(m_VerificationMedia);
}

void Mp7JrsQCItemResultType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QCItemResultTypePtr, since we
	// might need GetBase(), which isn't defined in IQCItemResultType
	const Dc1Ptr< Mp7JrsQCItemResultType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsQCItemResultType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	if (tmp->IsValidCheckResult())
	{
		this->SetCheckResult(tmp->GetCheckResult());
	}
	else
	{
		InvalidateCheckResult();
	}
		// Dc1Factory::DeleteObject(m_QCItemResultType_LocalType);
		this->SetQCItemResultType_LocalType(Dc1Factory::CloneObject(tmp->GetQCItemResultType_LocalType()));
	if (tmp->IsValidRelevance())
	{
		this->SetRelevance(tmp->GetRelevance());
	}
	else
	{
		InvalidateRelevance();
	}
		this->SetExecutionStatus(tmp->GetExecutionStatus());
	if (tmp->IsValidErrorDescription())
	{
		this->SetErrorDescription(XMLString::replicate(tmp->GetErrorDescription()));
	}
	else
	{
		InvalidateErrorDescription();
	}
	if (tmp->IsValidResultDescription())
	{
		// Dc1Factory::DeleteObject(m_ResultDescription);
		this->SetResultDescription(Dc1Factory::CloneObject(tmp->GetResultDescription()));
	}
	else
	{
		InvalidateResultDescription();
	}
	if (tmp->IsValidAnnotation())
	{
		// Dc1Factory::DeleteObject(m_Annotation);
		this->SetAnnotation(Dc1Factory::CloneObject(tmp->GetAnnotation()));
	}
	else
	{
		InvalidateAnnotation();
	}
		// Dc1Factory::DeleteObject(m_VerificationMedia);
		this->SetVerificationMedia(Dc1Factory::CloneObject(tmp->GetVerificationMedia()));
	if (tmp->IsValidMaxSeverity())
	{
		this->SetMaxSeverity(tmp->GetMaxSeverity());
	}
	else
	{
		InvalidateMaxSeverity();
	}
	if (tmp->IsValidDetectionMethod())
	{
		this->SetDetectionMethod(tmp->GetDetectionMethod());
	}
	else
	{
		InvalidateDetectionMethod();
	}
}

Dc1NodePtr Mp7JrsQCItemResultType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsQCItemResultType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsQCItemResultType::GetBase() const
{
	return m_Base;
}

Mp7JrsControlledTermUsePtr Mp7JrsQCItemResultType::GetName() const
{
		return m_Name;
}

bool Mp7JrsQCItemResultType::GetCheckResult() const
{
		return m_CheckResult;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidCheckResult() const
{
	return m_CheckResult_Exist;
}

Mp7JrsQCItemResultType_CollectionPtr Mp7JrsQCItemResultType::GetQCItemResultType_LocalType() const
{
		return m_QCItemResultType_LocalType;
}

unsigned Mp7JrsQCItemResultType::GetRelevance() const
{
		return m_Relevance;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidRelevance() const
{
	return m_Relevance_Exist;
}

Mp7JrsQCItemResultType_ExecutionStatus_LocalType::Enumeration Mp7JrsQCItemResultType::GetExecutionStatus() const
{
		return m_ExecutionStatus;
}

XMLCh * Mp7JrsQCItemResultType::GetErrorDescription() const
{
		return m_ErrorDescription;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidErrorDescription() const
{
	return m_ErrorDescription_Exist;
}

Mp7JrsTextAnnotationPtr Mp7JrsQCItemResultType::GetResultDescription() const
{
		return m_ResultDescription;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidResultDescription() const
{
	return m_ResultDescription_Exist;
}

Mp7JrsTextAnnotationPtr Mp7JrsQCItemResultType::GetAnnotation() const
{
		return m_Annotation;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidAnnotation() const
{
	return m_Annotation_Exist;
}

Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr Mp7JrsQCItemResultType::GetVerificationMedia() const
{
		return m_VerificationMedia;
}

unsigned Mp7JrsQCItemResultType::GetMaxSeverity() const
{
		return m_MaxSeverity;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidMaxSeverity() const
{
	return m_MaxSeverity_Exist;
}

Mp7JrsQCItemResultType_DetectionMethod_LocalType::Enumeration Mp7JrsQCItemResultType::GetDetectionMethod() const
{
		return m_DetectionMethod;
}

// Element is optional
bool Mp7JrsQCItemResultType::IsValidDetectionMethod() const
{
	return m_DetectionMethod_Exist;
}

void Mp7JrsQCItemResultType::SetName(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if (m_Name && m_Name->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Name->UseTypeAttribute = true;
		}
		if(m_Name != Mp7JrsControlledTermUsePtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::SetCheckResult(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetCheckResult().");
	}
	if (m_CheckResult != item || m_CheckResult_Exist == false)
	{
		m_CheckResult = item;
		m_CheckResult_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateCheckResult()
{
	m_CheckResult_Exist = false;
}
void Mp7JrsQCItemResultType::SetQCItemResultType_LocalType(const Mp7JrsQCItemResultType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetQCItemResultType_LocalType().");
	}
	if (m_QCItemResultType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_QCItemResultType_LocalType);
		m_QCItemResultType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QCItemResultType_LocalType) m_QCItemResultType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::SetRelevance(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetRelevance().");
	}
	if (m_Relevance != item || m_Relevance_Exist == false)
	{
		m_Relevance = item;
		m_Relevance_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateRelevance()
{
	m_Relevance_Exist = false;
}
void Mp7JrsQCItemResultType::SetExecutionStatus(Mp7JrsQCItemResultType_ExecutionStatus_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetExecutionStatus().");
	}
	if (m_ExecutionStatus != item)
	{
		// nothing to free here, hopefully
		m_ExecutionStatus = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::SetErrorDescription(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetErrorDescription().");
	}
	if (m_ErrorDescription != item || m_ErrorDescription_Exist == false)
	{
		XMLString::release(&m_ErrorDescription);
		m_ErrorDescription = item;
		m_ErrorDescription_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateErrorDescription()
{
	m_ErrorDescription_Exist = false;
}
void Mp7JrsQCItemResultType::SetResultDescription(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetResultDescription().");
	}
	if (m_ResultDescription != item || m_ResultDescription_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ResultDescription);
		m_ResultDescription = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ResultDescription) m_ResultDescription->SetParent(m_myself.getPointer());
		if (m_ResultDescription && m_ResultDescription->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ResultDescription->UseTypeAttribute = true;
		}
		m_ResultDescription_Exist = true;
		if(m_ResultDescription != Mp7JrsTextAnnotationPtr())
		{
			m_ResultDescription->SetContentName(XMLString::transcode("ResultDescription"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateResultDescription()
{
	m_ResultDescription_Exist = false;
}
void Mp7JrsQCItemResultType::SetAnnotation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetAnnotation().");
	}
	if (m_Annotation != item || m_Annotation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Annotation);
		m_Annotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Annotation) m_Annotation->SetParent(m_myself.getPointer());
		if (m_Annotation && m_Annotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Annotation->UseTypeAttribute = true;
		}
		m_Annotation_Exist = true;
		if(m_Annotation != Mp7JrsTextAnnotationPtr())
		{
			m_Annotation->SetContentName(XMLString::transcode("Annotation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateAnnotation()
{
	m_Annotation_Exist = false;
}
void Mp7JrsQCItemResultType::SetVerificationMedia(const Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetVerificationMedia().");
	}
	if (m_VerificationMedia != item)
	{
		// Dc1Factory::DeleteObject(m_VerificationMedia);
		m_VerificationMedia = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VerificationMedia) m_VerificationMedia->SetParent(m_myself.getPointer());
		if(m_VerificationMedia != Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr())
		{
			m_VerificationMedia->SetContentName(XMLString::transcode("VerificationMedia"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::SetMaxSeverity(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetMaxSeverity().");
	}
	if (m_MaxSeverity != item || m_MaxSeverity_Exist == false)
	{
		m_MaxSeverity = item;
		m_MaxSeverity_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateMaxSeverity()
{
	m_MaxSeverity_Exist = false;
}
void Mp7JrsQCItemResultType::SetDetectionMethod(Mp7JrsQCItemResultType_DetectionMethod_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetDetectionMethod().");
	}
	if (m_DetectionMethod != item || m_DetectionMethod_Exist == false)
	{
		// nothing to free here, hopefully
		m_DetectionMethod = item;
		m_DetectionMethod_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidateDetectionMethod()
{
	m_DetectionMethod_Exist = false;
}
XMLCh * Mp7JrsQCItemResultType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsQCItemResultType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsQCItemResultType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsQCItemResultType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsQCItemResultType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsQCItemResultType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsQCItemResultType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsQCItemResultType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsQCItemResultType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsQCItemResultType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsQCItemResultType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsQCItemResultType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsQCItemResultType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsQCItemResultType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsQCItemResultType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCItemResultType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsQCItemResultType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsQCItemResultType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCItemResultType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsQCItemResultType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// Name is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetName(p, client);
					if((p = GetName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Output")) == 0)
	{
		// Output is contained in itemtype QCItemResultType_LocalType
		// in choice collection QCItemResultType_CollectionType

		context->Found = true;
		Mp7JrsQCItemResultType_CollectionPtr coll = GetQCItemResultType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCItemResultType_CollectionType; // FTT, check this
				SetQCItemResultType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->GetOutput()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsQCItemResultType_LocalPtr item = CreateQCItemResultType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OutputQCValueType")))) != empty)
			{
				// Is type allowed
				Mp7JrsOutputQCValuePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->SetOutput(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->GetOutput()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SegmentOutput")) == 0)
	{
		// SegmentOutput is contained in itemtype QCItemResultType_LocalType
		// in choice collection QCItemResultType_CollectionType

		context->Found = true;
		Mp7JrsQCItemResultType_CollectionPtr coll = GetQCItemResultType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCItemResultType_CollectionType; // FTT, check this
				SetQCItemResultType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->GetSegmentOutput()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsQCItemResultType_LocalPtr item = CreateQCItemResultType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentOutputQCValueType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSegmentOutputQCValuePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->SetSegmentOutput(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsQCItemResultType_LocalPtr)coll->elementAt(i))->GetSegmentOutput()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ResultDescription")) == 0)
	{
		// ResultDescription is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetResultDescription()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetResultDescription(p, client);
					if((p = GetResultDescription()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Annotation")) == 0)
	{
		// Annotation is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAnnotation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAnnotation(p, client);
					if((p = GetAnnotation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VerificationMedia")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VerificationMedia is item of type TitleMediaType
		// in element collection QCItemResultType_VerificationMedia_CollectionType
		
		context->Found = true;
		Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr coll = GetVerificationMedia();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCItemResultType_VerificationMedia_CollectionType; // FTT, check this
				SetVerificationMedia(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TitleMediaType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTitleMediaPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for QCItemResultType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "QCItemResultType");
	}
	return result;
}

XMLCh * Mp7JrsQCItemResultType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsQCItemResultType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsQCItemResultType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("QCItemResultType"));
	// Element serialization:
	// Class
	
	if (m_Name != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Name->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Name"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Name->Serialize(doc, element, &elem);
	}
	if(m_CheckResult_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_CheckResult);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("CheckResult"), true);
		XMLString::release(&tmp);
	}
	if (m_QCItemResultType_LocalType != Mp7JrsQCItemResultType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QCItemResultType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_Relevance_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Relevance);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Relevance"), true);
		XMLString::release(&tmp);
	}
	if(m_ExecutionStatus != Mp7JrsQCItemResultType_ExecutionStatus_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsQCItemResultType_ExecutionStatus_LocalType::ToText(m_ExecutionStatus);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("ExecutionStatus"), false);
		XMLString::release(&tmp);
	}
	 // String
	if(m_ErrorDescription != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_ErrorDescription, X("urn:mpeg:mpeg7:schema:2004"), X("ErrorDescription"), true);
	// Class
	
	if (m_ResultDescription != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ResultDescription->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ResultDescription"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ResultDescription->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Annotation != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Annotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Annotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Annotation->Serialize(doc, element, &elem);
	}
	if (m_VerificationMedia != Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VerificationMedia->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_MaxSeverity_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_MaxSeverity);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("MaxSeverity"), true);
		XMLString::release(&tmp);
	}
	if(m_DetectionMethod != Mp7JrsQCItemResultType_DetectionMethod_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsQCItemResultType_DetectionMethod_LocalType::ToText(m_DetectionMethod);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("DetectionMethod"), true);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsQCItemResultType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("CheckResult"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetCheckResult(Dc1Convert::TextToBool(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Output
			Dc1Util::HasNodeName(parent, X("Output"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Output")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SegmentOutput
			Dc1Util::HasNodeName(parent, X("SegmentOutput"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SegmentOutput")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsQCItemResultType_CollectionPtr tmp = CreateQCItemResultType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQCItemResultType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("Relevance"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetRelevance(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("ExecutionStatus"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ExecutionStatus")) == 0))
		{
			Mp7JrsQCItemResultType_ExecutionStatus_LocalType::Enumeration tmp = Mp7JrsQCItemResultType_ExecutionStatus_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsQCItemResultType_ExecutionStatus_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetExecutionStatus(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("ErrorDescription"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetErrorDescription(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetErrorDescription(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ResultDescription"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ResultDescription")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetResultDescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Annotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Annotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VerificationMedia"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VerificationMedia")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr tmp = CreateQCItemResultType_VerificationMedia_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVerificationMedia(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("MaxSeverity"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetMaxSeverity(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("DetectionMethod"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DetectionMethod")) == 0))
		{
			Mp7JrsQCItemResultType_DetectionMethod_LocalType::Enumeration tmp = Mp7JrsQCItemResultType_DetectionMethod_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsQCItemResultType_DetectionMethod_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetDetectionMethod(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsQCItemResultType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Name")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetName(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Output"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Output")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SegmentOutput"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SegmentOutput")) == 0)
	)
  {
	Mp7JrsQCItemResultType_CollectionPtr tmp = CreateQCItemResultType_CollectionType; // FTT, check this
	this->SetQCItemResultType_LocalType(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("ResultDescription")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetResultDescription(child);
  }
  if (XMLString::compareString(elementname, X("Annotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetAnnotation(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VerificationMedia")) == 0))
  {
	Mp7JrsQCItemResultType_VerificationMedia_CollectionPtr tmp = CreateQCItemResultType_VerificationMedia_CollectionType; // FTT, check this
	this->SetVerificationMedia(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsQCItemResultType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetQCItemResultType_LocalType() != Dc1NodePtr())
		result.Insert(GetQCItemResultType_LocalType());
	if (GetResultDescription() != Dc1NodePtr())
		result.Insert(GetResultDescription());
	if (GetAnnotation() != Dc1NodePtr())
		result.Insert(GetAnnotation());
	if (GetVerificationMedia() != Dc1NodePtr())
		result.Insert(GetVerificationMedia());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsQCItemResultType_ExtMethodImpl.h


