
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtImplInclude.h


#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalType.h"
#include "Mp7JrsInlineMediaType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTemporalSegmentLocatorType::IMp7JrsTemporalSegmentLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtPropInit.h

}

IMp7JrsTemporalSegmentLocatorType::~IMp7JrsTemporalSegmentLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtPropCleanup.h

}

Mp7JrsTemporalSegmentLocatorType::Mp7JrsTemporalSegmentLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTemporalSegmentLocatorType::~Mp7JrsTemporalSegmentLocatorType()
{
	Cleanup();
}

void Mp7JrsTemporalSegmentLocatorType::Init()
{
	// Init base
	m_Base = CreateMediaLocatorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class
	m_BytePosition = Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtMyPropInit.h

}

void Mp7JrsTemporalSegmentLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MediaTime);
	// Dc1Factory::DeleteObject(m_BytePosition);
}

void Mp7JrsTemporalSegmentLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TemporalSegmentLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in ITemporalSegmentLocatorType
	const Dc1Ptr< Mp7JrsTemporalSegmentLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaLocatorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsTemporalSegmentLocatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
		// Dc1Factory::DeleteObject(m_BytePosition);
		this->SetBytePosition(Dc1Factory::CloneObject(tmp->GetBytePosition()));
}

Dc1NodePtr Mp7JrsTemporalSegmentLocatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsTemporalSegmentLocatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMediaLocatorType > Mp7JrsTemporalSegmentLocatorType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaTimePtr Mp7JrsTemporalSegmentLocatorType::GetMediaTime() const
{
		return m_MediaTime;
}

Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr Mp7JrsTemporalSegmentLocatorType::GetBytePosition() const
{
		return m_BytePosition;
}

// implementing setter for choice 
/* element */
void Mp7JrsTemporalSegmentLocatorType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalSegmentLocatorType::SetMediaTime().");
	}
	if (m_MediaTime != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	// Dc1Factory::DeleteObject(m_BytePosition);
	m_BytePosition = Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTemporalSegmentLocatorType::SetBytePosition(const Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalSegmentLocatorType::SetBytePosition().");
	}
	if (m_BytePosition != item)
	{
		// Dc1Factory::DeleteObject(m_BytePosition);
		m_BytePosition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BytePosition) m_BytePosition->SetParent(m_myself.getPointer());
		if (m_BytePosition && m_BytePosition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType_BytePosition_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BytePosition->UseTypeAttribute = true;
		}
		if(m_BytePosition != Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr())
		{
			m_BytePosition->SetContentName(XMLString::transcode("BytePosition"));
		}
	// Dc1Factory::DeleteObject(m_MediaTime);
	m_MediaTime = Mp7JrsMediaTimePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsTemporalSegmentLocatorType::GetMediaUri() const
{
	return GetBase()->GetMediaUri();
}

Mp7JrsInlineMediaPtr Mp7JrsTemporalSegmentLocatorType::GetInlineMedia() const
{
	return GetBase()->GetInlineMedia();
}

unsigned Mp7JrsTemporalSegmentLocatorType::GetStreamID() const
{
	return GetBase()->GetStreamID();
}

// Element is optional
bool Mp7JrsTemporalSegmentLocatorType::IsValidStreamID() const
{
	return GetBase()->IsValidStreamID();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsTemporalSegmentLocatorType::SetMediaUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalSegmentLocatorType::SetMediaUri().");
	}
	GetBase()->SetMediaUri(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsTemporalSegmentLocatorType::SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalSegmentLocatorType::SetInlineMedia().");
	}
	GetBase()->SetInlineMedia(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTemporalSegmentLocatorType::SetStreamID(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTemporalSegmentLocatorType::SetStreamID().");
	}
	GetBase()->SetStreamID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTemporalSegmentLocatorType::InvalidateStreamID()
{
	GetBase()->InvalidateStreamID();
}

Dc1NodeEnum Mp7JrsTemporalSegmentLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTime")) == 0)
	{
		// MediaTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTime(p, client);
					if((p = GetMediaTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BytePosition")) == 0)
	{
		// BytePosition is simple element TemporalSegmentLocatorType_BytePosition_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBytePosition()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType_BytePosition_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBytePosition(p, client);
					if((p = GetBytePosition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TemporalSegmentLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TemporalSegmentLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsTemporalSegmentLocatorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsTemporalSegmentLocatorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsTemporalSegmentLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TemporalSegmentLocatorType"));
	// Element serialization:
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_BytePosition != Mp7JrsTemporalSegmentLocatorType_BytePosition_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BytePosition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BytePosition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BytePosition->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTemporalSegmentLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaLocatorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BytePosition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BytePosition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType_BytePosition_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBytePosition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsTemporalSegmentLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTemporalSegmentLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  if (XMLString::compareString(elementname, X("BytePosition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType_BytePosition_LocalType; // FTT, check this
	}
	this->SetBytePosition(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTemporalSegmentLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetBytePosition() != Dc1NodePtr())
		result.Insert(GetBytePosition());
	if (GetInlineMedia() != Dc1NodePtr())
		result.Insert(GetInlineMedia());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsTemporalSegmentLocatorType_ExtMethodImpl.h
Mp7JrsmediaTimePointPtr Mp7JrsTemporalSegmentLocatorType::GetMediaTimeBase(void) const
{
	return this->GetBase()->GetMediaTimeBase();
}
void Mp7JrsTemporalSegmentLocatorType::SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client)
{
	return this->GetBase()->SetMediaTimeBase(timePoint, client);
}
// end extension included


