
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtImplInclude.h


#include "Mp7JrsClusterModelType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsStructuredCollectionType_LocalType1.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsStructuredCollectionType_LocalType1::IMp7JrsStructuredCollectionType_LocalType1()
{

// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtPropInit.h

}

IMp7JrsStructuredCollectionType_LocalType1::~IMp7JrsStructuredCollectionType_LocalType1()
{
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtPropCleanup.h

}

Mp7JrsStructuredCollectionType_LocalType1::Mp7JrsStructuredCollectionType_LocalType1()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStructuredCollectionType_LocalType1::~Mp7JrsStructuredCollectionType_LocalType1()
{
	Cleanup();
}

void Mp7JrsStructuredCollectionType_LocalType1::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_ClusterModel = Mp7JrsClusterModelPtr(); // Class
	m_ClusterModelRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtMyPropInit.h

}

void Mp7JrsStructuredCollectionType_LocalType1::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_ClusterModel);
	// Dc1Factory::DeleteObject(m_ClusterModelRef);
}

void Mp7JrsStructuredCollectionType_LocalType1::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StructuredCollectionType_LocalType1Ptr, since we
	// might need GetBase(), which isn't defined in IStructuredCollectionType_LocalType1
	const Dc1Ptr< Mp7JrsStructuredCollectionType_LocalType1 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_ClusterModel);
		this->SetClusterModel(Dc1Factory::CloneObject(tmp->GetClusterModel()));
		// Dc1Factory::DeleteObject(m_ClusterModelRef);
		this->SetClusterModelRef(Dc1Factory::CloneObject(tmp->GetClusterModelRef()));
}

Dc1NodePtr Mp7JrsStructuredCollectionType_LocalType1::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsStructuredCollectionType_LocalType1::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsClusterModelPtr Mp7JrsStructuredCollectionType_LocalType1::GetClusterModel() const
{
		return m_ClusterModel;
}

Mp7JrsReferencePtr Mp7JrsStructuredCollectionType_LocalType1::GetClusterModelRef() const
{
		return m_ClusterModelRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsStructuredCollectionType_LocalType1::SetClusterModel(const Mp7JrsClusterModelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType_LocalType1::SetClusterModel().");
	}
	if (m_ClusterModel != item)
	{
		// Dc1Factory::DeleteObject(m_ClusterModel);
		m_ClusterModel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClusterModel) m_ClusterModel->SetParent(m_myself.getPointer());
		if (m_ClusterModel && m_ClusterModel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ClusterModel->UseTypeAttribute = true;
		}
		if(m_ClusterModel != Mp7JrsClusterModelPtr())
		{
			m_ClusterModel->SetContentName(XMLString::transcode("ClusterModel"));
		}
	// Dc1Factory::DeleteObject(m_ClusterModelRef);
	m_ClusterModelRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStructuredCollectionType_LocalType1::SetClusterModelRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType_LocalType1::SetClusterModelRef().");
	}
	if (m_ClusterModelRef != item)
	{
		// Dc1Factory::DeleteObject(m_ClusterModelRef);
		m_ClusterModelRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClusterModelRef) m_ClusterModelRef->SetParent(m_myself.getPointer());
		if (m_ClusterModelRef && m_ClusterModelRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ClusterModelRef->UseTypeAttribute = true;
		}
		if(m_ClusterModelRef != Mp7JrsReferencePtr())
		{
			m_ClusterModelRef->SetContentName(XMLString::transcode("ClusterModelRef"));
		}
	// Dc1Factory::DeleteObject(m_ClusterModel);
	m_ClusterModel = Mp7JrsClusterModelPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: StructuredCollectionType_LocalType1: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsStructuredCollectionType_LocalType1::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsStructuredCollectionType_LocalType1::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsStructuredCollectionType_LocalType1::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsStructuredCollectionType_LocalType1::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_ClusterModel != Mp7JrsClusterModelPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ClusterModel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ClusterModel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ClusterModel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ClusterModelRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ClusterModelRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ClusterModelRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ClusterModelRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsStructuredCollectionType_LocalType1::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ClusterModel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateClusterModelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetClusterModel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ClusterModelRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetClusterModelRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStructuredCollectionType_LocalType1::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("ClusterModel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateClusterModelType; // FTT, check this
	}
	this->SetClusterModel(child);
  }
  if (XMLString::compareString(elementname, X("ClusterModelRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetClusterModelRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStructuredCollectionType_LocalType1::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetClusterModel() != Dc1NodePtr())
		result.Insert(GetClusterModel());
	if (GetClusterModelRef() != Dc1NodePtr())
		result.Insert(GetClusterModelRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_LocalType1_ExtMethodImpl.h


