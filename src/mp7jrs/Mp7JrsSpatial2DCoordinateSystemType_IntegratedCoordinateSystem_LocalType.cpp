
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaIncrDurationType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtPropInit.h

}

IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::~IMp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtPropCleanup.h

}

Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::~Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType()
{
	Cleanup();
}

void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TimeIncr = Mp7JrsMediaIncrDurationPtr(); // Class with content 
	m_MotionParams = Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TimeIncr);
	// Dc1Factory::DeleteObject(m_MotionParams);
}

void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType
	const Dc1Ptr< Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_TimeIncr);
		this->SetTimeIncr(Dc1Factory::CloneObject(tmp->GetTimeIncr()));
		// Dc1Factory::DeleteObject(m_MotionParams);
		this->SetMotionParams(Dc1Factory::CloneObject(tmp->GetMotionParams()));
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsMediaIncrDurationPtr Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetTimeIncr() const
{
		return m_TimeIncr;
}

Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetMotionParams() const
{
		return m_MotionParams;
}

void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::SetTimeIncr(const Mp7JrsMediaIncrDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::SetTimeIncr().");
	}
	if (m_TimeIncr != item)
	{
		// Dc1Factory::DeleteObject(m_TimeIncr);
		m_TimeIncr = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimeIncr) m_TimeIncr->SetParent(m_myself.getPointer());
		if (m_TimeIncr && m_TimeIncr->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimeIncr->UseTypeAttribute = true;
		}
		if(m_TimeIncr != Mp7JrsMediaIncrDurationPtr())
		{
			m_TimeIncr->SetContentName(XMLString::transcode("TimeIncr"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::SetMotionParams(const Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::SetMotionParams().");
	}
	if (m_MotionParams != item)
	{
		// Dc1Factory::DeleteObject(m_MotionParams);
		m_MotionParams = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionParams) m_MotionParams->SetParent(m_myself.getPointer());
		if(m_MotionParams != Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr())
		{
			m_MotionParams->SetContentName(XMLString::transcode("MotionParams"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Spatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_TimeIncr != Mp7JrsMediaIncrDurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TimeIncr->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TimeIncr"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TimeIncr->Serialize(doc, element, &elem);
	}
	if (m_MotionParams != Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MotionParams->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TimeIncr"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimeIncr")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaIncrDurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTimeIncr(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MotionParams"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MotionParams")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMotionParams(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TimeIncr")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaIncrDurationType; // FTT, check this
	}
	this->SetTimeIncr(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MotionParams")) == 0))
  {
	Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_MotionParams_CollectionType; // FTT, check this
	this->SetMotionParams(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimeIncr() != Dc1NodePtr())
		result.Insert(GetTimeIncr());
	if (GetMotionParams() != Dc1NodePtr())
		result.Insert(GetMotionParams());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_IntegratedCoordinateSystem_LocalType_ExtMethodImpl.h


