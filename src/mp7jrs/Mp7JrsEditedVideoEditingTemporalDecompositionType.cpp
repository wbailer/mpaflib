
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtImplInclude.h


#include "Mp7JrsTemporalSegmentDecompositionType.h"
#include "Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsEditedVideoEditingTemporalDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalType.h" // Sequence collection Shot
#include "Mp7JrsShotType.h" // Sequence collection element Shot
#include "Mp7JrsReferenceType.h" // Sequence collection element ShotRef
#include "Mp7JrsGlobalTransitionType.h" // Sequence collection element GlobalTransition

#include <assert.h>
IMp7JrsEditedVideoEditingTemporalDecompositionType::IMp7JrsEditedVideoEditingTemporalDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtPropInit.h

}

IMp7JrsEditedVideoEditingTemporalDecompositionType::~IMp7JrsEditedVideoEditingTemporalDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtPropCleanup.h

}

Mp7JrsEditedVideoEditingTemporalDecompositionType::Mp7JrsEditedVideoEditingTemporalDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsEditedVideoEditingTemporalDecompositionType::~Mp7JrsEditedVideoEditingTemporalDecompositionType()
{
	Cleanup();
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Init()
{
	// Init base
	m_Base = CreateTemporalSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_EditedVideoEditingTemporalDecompositionType_LocalType = Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtMyPropInit.h

}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_EditedVideoEditingTemporalDecompositionType_LocalType);
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use EditedVideoEditingTemporalDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in IEditedVideoEditingTemporalDecompositionType
	const Dc1Ptr< Mp7JrsEditedVideoEditingTemporalDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTemporalSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsEditedVideoEditingTemporalDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_EditedVideoEditingTemporalDecompositionType_LocalType);
		this->SetEditedVideoEditingTemporalDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetEditedVideoEditingTemporalDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTemporalSegmentDecompositionType > Mp7JrsEditedVideoEditingTemporalDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetEditedVideoEditingTemporalDecompositionType_LocalType() const
{
		return m_EditedVideoEditingTemporalDecompositionType_LocalType;
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::SetEditedVideoEditingTemporalDecompositionType_LocalType(const Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SetEditedVideoEditingTemporalDecompositionType_LocalType().");
	}
	if (m_EditedVideoEditingTemporalDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_EditedVideoEditingTemporalDecompositionType_LocalType);
		m_EditedVideoEditingTemporalDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EditedVideoEditingTemporalDecompositionType_LocalType) m_EditedVideoEditingTemporalDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsEditedVideoEditingTemporalDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsEditedVideoEditingTemporalDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsEditedVideoEditingTemporalDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsEditedVideoEditingTemporalDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsEditedVideoEditingTemporalDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsEditedVideoEditingTemporalDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsEditedVideoEditingTemporalDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Shot")) == 0)
	{
		// Shot is contained in itemtype EditedVideoEditingTemporalDecompositionType_LocalType
		// in sequence collection EditedVideoEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr coll = GetEditedVideoEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetEditedVideoEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetShot()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr item = CreateEditedVideoEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetShot(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetShot());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetShot(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetShot());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShotType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShotPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetShot(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetShot()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ShotRef")) == 0)
	{
		// ShotRef is contained in itemtype EditedVideoEditingTemporalDecompositionType_LocalType
		// in sequence collection EditedVideoEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr coll = GetEditedVideoEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetEditedVideoEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetShotRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr item = CreateEditedVideoEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetShotRef(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetShotRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetShotRef(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetShotRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetShotRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetShotRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GlobalTransition")) == 0)
	{
		// GlobalTransition is contained in itemtype EditedVideoEditingTemporalDecompositionType_LocalType
		// in sequence collection EditedVideoEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr coll = GetEditedVideoEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetEditedVideoEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetGlobalTransition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr item = CreateEditedVideoEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetGlobalTransition(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetGlobalTransition());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetGlobalTransition(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetGlobalTransition());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GlobalTransitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGlobalTransitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetGlobalTransition(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetGlobalTransition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GlobalTransitionRef")) == 0)
	{
		// GlobalTransitionRef is contained in itemtype EditedVideoEditingTemporalDecompositionType_LocalType
		// in sequence collection EditedVideoEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr coll = GetEditedVideoEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetEditedVideoEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetGlobalTransitionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr item = CreateEditedVideoEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetGlobalTransitionRef(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetGlobalTransitionRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetGlobalTransitionRef(
						((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetGlobalTransitionRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetGlobalTransitionRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsEditedVideoEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetGlobalTransitionRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for EditedVideoEditingTemporalDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "EditedVideoEditingTemporalDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsEditedVideoEditingTemporalDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsEditedVideoEditingTemporalDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("EditedVideoEditingTemporalDecompositionType"));
	// Element serialization:
	if (m_EditedVideoEditingTemporalDecompositionType_LocalType != Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_EditedVideoEditingTemporalDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsEditedVideoEditingTemporalDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTemporalSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Shot")) == 0)
			(Dc1Util::HasNodeName(parent, X("Shot"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ShotRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("ShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GlobalTransition")) == 0)
			(Dc1Util::HasNodeName(parent, X("GlobalTransition"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GlobalTransitionRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("GlobalTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr tmp = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetEditedVideoEditingTemporalDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsEditedVideoEditingTemporalDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsEditedVideoEditingTemporalDecompositionType_CollectionPtr tmp = CreateEditedVideoEditingTemporalDecompositionType_CollectionType; // FTT, check this
	this->SetEditedVideoEditingTemporalDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsEditedVideoEditingTemporalDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEditedVideoEditingTemporalDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetEditedVideoEditingTemporalDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsEditedVideoEditingTemporalDecompositionType_ExtMethodImpl.h


