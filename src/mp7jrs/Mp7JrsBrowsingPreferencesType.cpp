
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionType.h"
#include "Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionType.h"
#include "Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsBrowsingPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSummaryPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SummaryPreferences
#include "Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PreferenceCondition

#include <assert.h>
IMp7JrsBrowsingPreferencesType::IMp7JrsBrowsingPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtPropInit.h

}

IMp7JrsBrowsingPreferencesType::~IMp7JrsBrowsingPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtPropCleanup.h

}

Mp7JrsBrowsingPreferencesType::Mp7JrsBrowsingPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsBrowsingPreferencesType::~Mp7JrsBrowsingPreferencesType()
{
	Cleanup();
}

void Mp7JrsBrowsingPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_protected = Mp7JrsuserChoiceType::UninitializedEnumeration;
	m_protected_Default = Mp7JrsuserChoiceType::_true; // Default enumeration
	m_protected_Exist = false;
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SummaryPreferences = Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr(); // Collection
	m_BrowsingLocation = Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr(); // Collection
	m_PreferenceCondition = Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtMyPropInit.h

}

void Mp7JrsBrowsingPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_SummaryPreferences);
	// Dc1Factory::DeleteObject(m_BrowsingLocation);
	// Dc1Factory::DeleteObject(m_PreferenceCondition);
}

void Mp7JrsBrowsingPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use BrowsingPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in IBrowsingPreferencesType
	const Dc1Ptr< Mp7JrsBrowsingPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsBrowsingPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existprotected())
	{
		this->Setprotected(tmp->Getprotected());
	}
	else
	{
		Invalidateprotected();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_SummaryPreferences);
		this->SetSummaryPreferences(Dc1Factory::CloneObject(tmp->GetSummaryPreferences()));
		// Dc1Factory::DeleteObject(m_BrowsingLocation);
		this->SetBrowsingLocation(Dc1Factory::CloneObject(tmp->GetBrowsingLocation()));
		// Dc1Factory::DeleteObject(m_PreferenceCondition);
		this->SetPreferenceCondition(Dc1Factory::CloneObject(tmp->GetPreferenceCondition()));
}

Dc1NodePtr Mp7JrsBrowsingPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsBrowsingPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsBrowsingPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrsuserChoiceType::Enumeration Mp7JrsBrowsingPreferencesType::Getprotected() const
{
	if (this->Existprotected()) {
		return m_protected;
	} else {
		return m_protected_Default;
	}
}

bool Mp7JrsBrowsingPreferencesType::Existprotected() const
{
	return m_protected_Exist;
}
void Mp7JrsBrowsingPreferencesType::Setprotected(Mp7JrsuserChoiceType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::Setprotected().");
	}
	m_protected = item;
	m_protected_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::Invalidateprotected()
{
	m_protected_Exist = false;
}
Mp7JrspreferenceValuePtr Mp7JrsBrowsingPreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsBrowsingPreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsBrowsingPreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr Mp7JrsBrowsingPreferencesType::GetSummaryPreferences() const
{
		return m_SummaryPreferences;
}

Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr Mp7JrsBrowsingPreferencesType::GetBrowsingLocation() const
{
		return m_BrowsingLocation;
}

Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr Mp7JrsBrowsingPreferencesType::GetPreferenceCondition() const
{
		return m_PreferenceCondition;
}

void Mp7JrsBrowsingPreferencesType::SetSummaryPreferences(const Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetSummaryPreferences().");
	}
	if (m_SummaryPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_SummaryPreferences);
		m_SummaryPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummaryPreferences) m_SummaryPreferences->SetParent(m_myself.getPointer());
		if(m_SummaryPreferences != Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr())
		{
			m_SummaryPreferences->SetContentName(XMLString::transcode("SummaryPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::SetBrowsingLocation(const Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetBrowsingLocation().");
	}
	if (m_BrowsingLocation != item)
	{
		// Dc1Factory::DeleteObject(m_BrowsingLocation);
		m_BrowsingLocation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BrowsingLocation) m_BrowsingLocation->SetParent(m_myself.getPointer());
		if(m_BrowsingLocation != Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr())
		{
			m_BrowsingLocation->SetContentName(XMLString::transcode("BrowsingLocation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::SetPreferenceCondition(const Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetPreferenceCondition().");
	}
	if (m_PreferenceCondition != item)
	{
		// Dc1Factory::DeleteObject(m_PreferenceCondition);
		m_PreferenceCondition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PreferenceCondition) m_PreferenceCondition->SetParent(m_myself.getPointer());
		if(m_PreferenceCondition != Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr())
		{
			m_PreferenceCondition->SetContentName(XMLString::transcode("PreferenceCondition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsBrowsingPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsBrowsingPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsBrowsingPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsBrowsingPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsBrowsingPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsBrowsingPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsBrowsingPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsBrowsingPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsBrowsingPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsBrowsingPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsBrowsingPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsBrowsingPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsBrowsingPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsBrowsingPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsBrowsingPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsBrowsingPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsBrowsingPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsBrowsingPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("protected")) == 0)
	{
		// protected is simple attribute userChoiceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsuserChoiceType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummaryPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SummaryPreferences is item of type SummaryPreferencesType
		// in element collection BrowsingPreferencesType_SummaryPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr coll = GetSummaryPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateBrowsingPreferencesType_SummaryPreferences_CollectionType; // FTT, check this
				SetSummaryPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummaryPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BrowsingLocation")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PreferenceCondition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PreferenceCondition is item of type BrowsingPreferencesType_PreferenceCondition_LocalType
		// in element collection BrowsingPreferencesType_PreferenceCondition_CollectionType
		
		context->Found = true;
		Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr coll = GetPreferenceCondition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateBrowsingPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
				SetPreferenceCondition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BrowsingPreferencesType_PreferenceCondition_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for BrowsingPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "BrowsingPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsBrowsingPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsBrowsingPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsBrowsingPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("BrowsingPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_protected_Exist)
	{
	// Enumeration
	if(m_protected != Mp7JrsuserChoiceType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsuserChoiceType::ToText(m_protected);
		element->setAttributeNS(X(""), X("protected"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_SummaryPreferences != Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SummaryPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_BrowsingLocation != Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_BrowsingLocation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PreferenceCondition != Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PreferenceCondition->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsBrowsingPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("protected")))
	{
		this->Setprotected(Mp7JrsuserChoiceType::Parse(parent->getAttribute(X("protected"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SummaryPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SummaryPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr tmp = CreateBrowsingPreferencesType_SummaryPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummaryPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("BrowsingLocation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("BrowsingLocation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr tmp = CreateBrowsingPreferencesType_BrowsingLocation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetBrowsingLocation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PreferenceCondition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PreferenceCondition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr tmp = CreateBrowsingPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPreferenceCondition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsBrowsingPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SummaryPreferences")) == 0))
  {
	Mp7JrsBrowsingPreferencesType_SummaryPreferences_CollectionPtr tmp = CreateBrowsingPreferencesType_SummaryPreferences_CollectionType; // FTT, check this
	this->SetSummaryPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("BrowsingLocation")) == 0))
  {
	Mp7JrsBrowsingPreferencesType_BrowsingLocation_CollectionPtr tmp = CreateBrowsingPreferencesType_BrowsingLocation_CollectionType; // FTT, check this
	this->SetBrowsingLocation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PreferenceCondition")) == 0))
  {
	Mp7JrsBrowsingPreferencesType_PreferenceCondition_CollectionPtr tmp = CreateBrowsingPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
	this->SetPreferenceCondition(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsBrowsingPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSummaryPreferences() != Dc1NodePtr())
		result.Insert(GetSummaryPreferences());
	if (GetBrowsingLocation() != Dc1NodePtr())
		result.Insert(GetBrowsingLocation());
	if (GetPreferenceCondition() != Dc1NodePtr())
		result.Insert(GetPreferenceCondition());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_ExtMethodImpl.h


