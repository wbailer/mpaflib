
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPositionType_ExtImplInclude.h


#include "Mp7JrsExtentType.h"
#include "Mp7JrsPositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPositionType::IMp7JrsPositionType()
{

// no includefile for extension defined 
// file Mp7JrsPositionType_ExtPropInit.h

}

IMp7JrsPositionType::~IMp7JrsPositionType()
{
// no includefile for extension defined 
// file Mp7JrsPositionType_ExtPropCleanup.h

}

Mp7JrsPositionType::Mp7JrsPositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPositionType::~Mp7JrsPositionType()
{
	Cleanup();
}

void Mp7JrsPositionType::Init()
{

	// Init attributes
	m_origin = NULL; // String
	m_origin_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Displacement = Mp7JrsExtentPtr(); // Class
	m_Displacement_Exist = false;
	m_Direction = Mp7JrsExtentPtr(); // Class
	m_Direction_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPositionType_ExtMyPropInit.h

}

void Mp7JrsPositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPositionType_ExtMyPropCleanup.h


	XMLString::release(&m_origin); // String
	// Dc1Factory::DeleteObject(m_Displacement);
	// Dc1Factory::DeleteObject(m_Direction);
}

void Mp7JrsPositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PositionTypePtr, since we
	// might need GetBase(), which isn't defined in IPositionType
	const Dc1Ptr< Mp7JrsPositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_origin); // String
	if (tmp->Existorigin())
	{
		this->Setorigin(XMLString::replicate(tmp->Getorigin()));
	}
	else
	{
		Invalidateorigin();
	}
	}
	if (tmp->IsValidDisplacement())
	{
		// Dc1Factory::DeleteObject(m_Displacement);
		this->SetDisplacement(Dc1Factory::CloneObject(tmp->GetDisplacement()));
	}
	else
	{
		InvalidateDisplacement();
	}
	if (tmp->IsValidDirection())
	{
		// Dc1Factory::DeleteObject(m_Direction);
		this->SetDirection(Dc1Factory::CloneObject(tmp->GetDirection()));
	}
	else
	{
		InvalidateDirection();
	}
}

Dc1NodePtr Mp7JrsPositionType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPositionType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsPositionType::Getorigin() const
{
	return m_origin;
}

bool Mp7JrsPositionType::Existorigin() const
{
	return m_origin_Exist;
}
void Mp7JrsPositionType::Setorigin(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPositionType::Setorigin().");
	}
	m_origin = item;
	m_origin_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPositionType::Invalidateorigin()
{
	m_origin_Exist = false;
}
Mp7JrsExtentPtr Mp7JrsPositionType::GetDisplacement() const
{
		return m_Displacement;
}

// Element is optional
bool Mp7JrsPositionType::IsValidDisplacement() const
{
	return m_Displacement_Exist;
}

Mp7JrsExtentPtr Mp7JrsPositionType::GetDirection() const
{
		return m_Direction;
}

// Element is optional
bool Mp7JrsPositionType::IsValidDirection() const
{
	return m_Direction_Exist;
}

void Mp7JrsPositionType::SetDisplacement(const Mp7JrsExtentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPositionType::SetDisplacement().");
	}
	if (m_Displacement != item || m_Displacement_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Displacement);
		m_Displacement = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Displacement) m_Displacement->SetParent(m_myself.getPointer());
		if (m_Displacement && m_Displacement->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Displacement->UseTypeAttribute = true;
		}
		m_Displacement_Exist = true;
		if(m_Displacement != Mp7JrsExtentPtr())
		{
			m_Displacement->SetContentName(XMLString::transcode("Displacement"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPositionType::InvalidateDisplacement()
{
	m_Displacement_Exist = false;
}
void Mp7JrsPositionType::SetDirection(const Mp7JrsExtentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPositionType::SetDirection().");
	}
	if (m_Direction != item || m_Direction_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Direction);
		m_Direction = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Direction) m_Direction->SetParent(m_myself.getPointer());
		if (m_Direction && m_Direction->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Direction->UseTypeAttribute = true;
		}
		m_Direction_Exist = true;
		if(m_Direction != Mp7JrsExtentPtr())
		{
			m_Direction->SetContentName(XMLString::transcode("Direction"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPositionType::InvalidateDirection()
{
	m_Direction_Exist = false;
}

Dc1NodeEnum Mp7JrsPositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("origin")) == 0)
	{
		// origin is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Displacement")) == 0)
	{
		// Displacement is simple element ExtentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDisplacement()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsExtentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDisplacement(p, client);
					if((p = GetDisplacement()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Direction")) == 0)
	{
		// Direction is simple element ExtentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDirection()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsExtentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDirection(p, client);
					if((p = GetDirection()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PositionType");
	}
	return result;
}

XMLCh * Mp7JrsPositionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPositionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PositionType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_origin_Exist)
	{
	// String
	if(m_origin != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("origin"), m_origin);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Displacement != Mp7JrsExtentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Displacement->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Displacement"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Displacement->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Direction != Mp7JrsExtentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Direction->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Direction"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Direction->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("origin")))
	{
		// Deserialize string type
		this->Setorigin(Dc1Convert::TextToString(parent->getAttribute(X("origin"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Displacement"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Displacement")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateExtentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDisplacement(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Direction"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Direction")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateExtentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDirection(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Displacement")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateExtentType; // FTT, check this
	}
	this->SetDisplacement(child);
  }
  if (XMLString::compareString(elementname, X("Direction")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateExtentType; // FTT, check this
	}
	this->SetDirection(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDisplacement() != Dc1NodePtr())
		result.Insert(GetDisplacement());
	if (GetDirection() != Dc1NodePtr())
		result.Insert(GetDirection());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPositionType_ExtMethodImpl.h


