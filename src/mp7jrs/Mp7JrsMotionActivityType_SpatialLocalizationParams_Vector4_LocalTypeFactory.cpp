/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

// Based on Factory_CPP.template

#include <stdio.h>
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory.h"
#include "Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType.h"

Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory::Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory()
	: Dc1Factory("urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType", "urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType")
{
}

Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory::~Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory()
{
}

const char * Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory::GetTypeName() const
{
	// Note: This is slow
	// return Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType().GetTypeName();
	
	// FTT Easier but needs more memory in debug mode (no string pooling)
	return "urn:mpeg:mpeg7:schema:2004:MotionActivityType_SpatialLocalizationParams_Vector4_LocalType";
}

Dc1NodePtr Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalTypeFactory::CreateObject()
{
	return Dc1NodePtr(new Mp7JrsMotionActivityType_SpatialLocalizationParams_Vector4_LocalType);
}
