
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtImplInclude.h


#include "Mp7JrsFiniteStateModelType.h"
#include "Mp7JrsProbabilityMatrixType.h"
#include "Mp7JrsStateTransitionModelType_State_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsStateTransitionModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsModelStateType.h" // Element collection urn:mpeg:mpeg7:schema:2004:State

#include <assert.h>
IMp7JrsStateTransitionModelType::IMp7JrsStateTransitionModelType()
{

// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtPropInit.h

}

IMp7JrsStateTransitionModelType::~IMp7JrsStateTransitionModelType()
{
// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtPropCleanup.h

}

Mp7JrsStateTransitionModelType::Mp7JrsStateTransitionModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStateTransitionModelType::~Mp7JrsStateTransitionModelType()
{
	Cleanup();
}

void Mp7JrsStateTransitionModelType::Init()
{
	// Init base
	m_Base = CreateFiniteStateModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Initial = Mp7JrsProbabilityMatrixPtr(); // Class
	m_Initial_Exist = false;
	m_Transitions = Mp7JrsProbabilityMatrixPtr(); // Class
	m_State = Mp7JrsStateTransitionModelType_State_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtMyPropInit.h

}

void Mp7JrsStateTransitionModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Initial);
	// Dc1Factory::DeleteObject(m_Transitions);
	// Dc1Factory::DeleteObject(m_State);
}

void Mp7JrsStateTransitionModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StateTransitionModelTypePtr, since we
	// might need GetBase(), which isn't defined in IStateTransitionModelType
	const Dc1Ptr< Mp7JrsStateTransitionModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsFiniteStateModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStateTransitionModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidInitial())
	{
		// Dc1Factory::DeleteObject(m_Initial);
		this->SetInitial(Dc1Factory::CloneObject(tmp->GetInitial()));
	}
	else
	{
		InvalidateInitial();
	}
		// Dc1Factory::DeleteObject(m_Transitions);
		this->SetTransitions(Dc1Factory::CloneObject(tmp->GetTransitions()));
		// Dc1Factory::DeleteObject(m_State);
		this->SetState(Dc1Factory::CloneObject(tmp->GetState()));
}

Dc1NodePtr Mp7JrsStateTransitionModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsStateTransitionModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsFiniteStateModelType > Mp7JrsStateTransitionModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsProbabilityMatrixPtr Mp7JrsStateTransitionModelType::GetInitial() const
{
		return m_Initial;
}

// Element is optional
bool Mp7JrsStateTransitionModelType::IsValidInitial() const
{
	return m_Initial_Exist;
}

Mp7JrsProbabilityMatrixPtr Mp7JrsStateTransitionModelType::GetTransitions() const
{
		return m_Transitions;
}

Mp7JrsStateTransitionModelType_State_CollectionPtr Mp7JrsStateTransitionModelType::GetState() const
{
		return m_State;
}

void Mp7JrsStateTransitionModelType::SetInitial(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetInitial().");
	}
	if (m_Initial != item || m_Initial_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Initial);
		m_Initial = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Initial) m_Initial->SetParent(m_myself.getPointer());
		if (m_Initial && m_Initial->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Initial->UseTypeAttribute = true;
		}
		m_Initial_Exist = true;
		if(m_Initial != Mp7JrsProbabilityMatrixPtr())
		{
			m_Initial->SetContentName(XMLString::transcode("Initial"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::InvalidateInitial()
{
	m_Initial_Exist = false;
}
void Mp7JrsStateTransitionModelType::SetTransitions(const Mp7JrsProbabilityMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetTransitions().");
	}
	if (m_Transitions != item)
	{
		// Dc1Factory::DeleteObject(m_Transitions);
		m_Transitions = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Transitions) m_Transitions->SetParent(m_myself.getPointer());
		if (m_Transitions && m_Transitions->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Transitions->UseTypeAttribute = true;
		}
		if(m_Transitions != Mp7JrsProbabilityMatrixPtr())
		{
			m_Transitions->SetContentName(XMLString::transcode("Transitions"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::SetState(const Mp7JrsStateTransitionModelType_State_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetState().");
	}
	if (m_State != item)
	{
		// Dc1Factory::DeleteObject(m_State);
		m_State = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_State) m_State->SetParent(m_myself.getPointer());
		if(m_State != Mp7JrsStateTransitionModelType_State_CollectionPtr())
		{
			m_State->SetContentName(XMLString::transcode("State"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsStateTransitionModelType::GetnumOfStates() const
{
	return GetBase()->GetnumOfStates();
}

void Mp7JrsStateTransitionModelType::SetnumOfStates(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetnumOfStates().");
	}
	GetBase()->SetnumOfStates(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsStateTransitionModelType::Getconfidence() const
{
	return GetBase()->GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsStateTransitionModelType::Existconfidence() const
{
	return GetBase()->GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsStateTransitionModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::Setconfidence().");
	}
	GetBase()->GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsStateTransitionModelType::Getreliability() const
{
	return GetBase()->GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsStateTransitionModelType::Existreliability() const
{
	return GetBase()->GetBase()->GetBase()->Existreliability();
}
void Mp7JrsStateTransitionModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::Setreliability().");
	}
	GetBase()->GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::Invalidatereliability()
{
	GetBase()->GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsStateTransitionModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsStateTransitionModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsStateTransitionModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsStateTransitionModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsStateTransitionModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsStateTransitionModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsStateTransitionModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsStateTransitionModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsStateTransitionModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsStateTransitionModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsStateTransitionModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsStateTransitionModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsStateTransitionModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsStateTransitionModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsStateTransitionModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStateTransitionModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsStateTransitionModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsStateTransitionModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStateTransitionModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStateTransitionModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Initial")) == 0)
	{
		// Initial is simple element ProbabilityMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInitial()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInitial(p, client);
					if((p = GetInitial()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Transitions")) == 0)
	{
		// Transitions is simple element ProbabilityMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTransitions()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ProbabilityMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsProbabilityMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTransitions(p, client);
					if((p = GetTransitions()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("State")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:State is item of type ModelStateType
		// in element collection StateTransitionModelType_State_CollectionType
		
		context->Found = true;
		Mp7JrsStateTransitionModelType_State_CollectionPtr coll = GetState();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStateTransitionModelType_State_CollectionType; // FTT, check this
				SetState(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ModelStateType")))) != empty)
			{
				// Is type allowed
				Mp7JrsModelStatePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StateTransitionModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StateTransitionModelType");
	}
	return result;
}

XMLCh * Mp7JrsStateTransitionModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStateTransitionModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStateTransitionModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StateTransitionModelType"));
	// Element serialization:
	// Class
	
	if (m_Initial != Mp7JrsProbabilityMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Initial->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Initial"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Initial->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Transitions != Mp7JrsProbabilityMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Transitions->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Transitions"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Transitions->Serialize(doc, element, &elem);
	}
	if (m_State != Mp7JrsStateTransitionModelType_State_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_State->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsStateTransitionModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsFiniteStateModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Initial"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Initial")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateProbabilityMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInitial(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Transitions"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Transitions")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateProbabilityMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTransitions(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("State"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("State")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStateTransitionModelType_State_CollectionPtr tmp = CreateStateTransitionModelType_State_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetState(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStateTransitionModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Initial")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateProbabilityMatrixType; // FTT, check this
	}
	this->SetInitial(child);
  }
  if (XMLString::compareString(elementname, X("Transitions")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateProbabilityMatrixType; // FTT, check this
	}
	this->SetTransitions(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("State")) == 0))
  {
	Mp7JrsStateTransitionModelType_State_CollectionPtr tmp = CreateStateTransitionModelType_State_CollectionType; // FTT, check this
	this->SetState(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStateTransitionModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInitial() != Dc1NodePtr())
		result.Insert(GetInitial());
	if (GetTransitions() != Dc1NodePtr())
		result.Insert(GetTransitions());
	if (GetState() != Dc1NodePtr())
		result.Insert(GetState());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStateTransitionModelType_ExtMethodImpl.h


