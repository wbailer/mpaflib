
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtImplInclude.h


#include "Mp7JrsCollectionType.h"
#include "Mp7JrsMixedCollectionType_CollectionType.h"
#include "Mp7JrsMixedCollectionType_CollectionType0.h"
#include "Mp7JrsMixedCollectionType_Descriptor_CollectionType.h"
#include "Mp7JrsMixedCollectionType_CollectionType1.h"
#include "Mp7JrsMixedCollectionType_CollectionType2.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsCollectionType_TextAnnotation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMixedCollectionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextAnnotationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsMixedCollectionType_LocalType.h" // Choice collection Content
#include "Mp7JrsMultimediaContentType.h" // Choice collection element Content
#include "Mp7JrsMixedCollectionType_LocalType0.h" // Choice collection Segment
#include "Mp7JrsSegmentType.h" // Choice collection element Segment
#include "Mp7JrsDType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Descriptor
#include "Mp7JrsMixedCollectionType_LocalType1.h" // Choice collection Concept
#include "Mp7JrsSemanticBaseType.h" // Choice collection element Concept
#include "Mp7JrsMixedCollectionType_LocalType2.h" // Choice collection MixedCollection
#include "Mp7JrsMixedCollectionType.h" // Choice collection element MixedCollection

#include <assert.h>
IMp7JrsMixedCollectionType::IMp7JrsMixedCollectionType()
{

// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtPropInit.h

}

IMp7JrsMixedCollectionType::~IMp7JrsMixedCollectionType()
{
// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtPropCleanup.h

}

Mp7JrsMixedCollectionType::Mp7JrsMixedCollectionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMixedCollectionType::~Mp7JrsMixedCollectionType()
{
	Cleanup();
}

void Mp7JrsMixedCollectionType::Init()
{
	// Init base
	m_Base = CreateCollectionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MixedCollectionType_LocalType = Mp7JrsMixedCollectionType_CollectionPtr(); // Collection
	m_MixedCollectionType_LocalType0 = Mp7JrsMixedCollectionType_Collection0Ptr(); // Collection
	m_Descriptor = Mp7JrsMixedCollectionType_Descriptor_CollectionPtr(); // Collection
	m_MixedCollectionType_LocalType1 = Mp7JrsMixedCollectionType_Collection1Ptr(); // Collection
	m_MixedCollectionType_LocalType2 = Mp7JrsMixedCollectionType_Collection2Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtMyPropInit.h

}

void Mp7JrsMixedCollectionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType);
	// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType0);
	// Dc1Factory::DeleteObject(m_Descriptor);
	// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType1);
	// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType2);
}

void Mp7JrsMixedCollectionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MixedCollectionTypePtr, since we
	// might need GetBase(), which isn't defined in IMixedCollectionType
	const Dc1Ptr< Mp7JrsMixedCollectionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsCollectionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMixedCollectionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType);
		this->SetMixedCollectionType_LocalType(Dc1Factory::CloneObject(tmp->GetMixedCollectionType_LocalType()));
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType0);
		this->SetMixedCollectionType_LocalType0(Dc1Factory::CloneObject(tmp->GetMixedCollectionType_LocalType0()));
		// Dc1Factory::DeleteObject(m_Descriptor);
		this->SetDescriptor(Dc1Factory::CloneObject(tmp->GetDescriptor()));
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType1);
		this->SetMixedCollectionType_LocalType1(Dc1Factory::CloneObject(tmp->GetMixedCollectionType_LocalType1()));
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType2);
		this->SetMixedCollectionType_LocalType2(Dc1Factory::CloneObject(tmp->GetMixedCollectionType_LocalType2()));
}

Dc1NodePtr Mp7JrsMixedCollectionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMixedCollectionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsCollectionType > Mp7JrsMixedCollectionType::GetBase() const
{
	return m_Base;
}

Mp7JrsMixedCollectionType_CollectionPtr Mp7JrsMixedCollectionType::GetMixedCollectionType_LocalType() const
{
		return m_MixedCollectionType_LocalType;
}

Mp7JrsMixedCollectionType_Collection0Ptr Mp7JrsMixedCollectionType::GetMixedCollectionType_LocalType0() const
{
		return m_MixedCollectionType_LocalType0;
}

Mp7JrsMixedCollectionType_Descriptor_CollectionPtr Mp7JrsMixedCollectionType::GetDescriptor() const
{
		return m_Descriptor;
}

Mp7JrsMixedCollectionType_Collection1Ptr Mp7JrsMixedCollectionType::GetMixedCollectionType_LocalType1() const
{
		return m_MixedCollectionType_LocalType1;
}

Mp7JrsMixedCollectionType_Collection2Ptr Mp7JrsMixedCollectionType::GetMixedCollectionType_LocalType2() const
{
		return m_MixedCollectionType_LocalType2;
}

void Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType(const Mp7JrsMixedCollectionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType().");
	}
	if (m_MixedCollectionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType);
		m_MixedCollectionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MixedCollectionType_LocalType) m_MixedCollectionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType0(const Mp7JrsMixedCollectionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType0().");
	}
	if (m_MixedCollectionType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType0);
		m_MixedCollectionType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MixedCollectionType_LocalType0) m_MixedCollectionType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::SetDescriptor(const Mp7JrsMixedCollectionType_Descriptor_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetDescriptor().");
	}
	if (m_Descriptor != item)
	{
		// Dc1Factory::DeleteObject(m_Descriptor);
		m_Descriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Descriptor) m_Descriptor->SetParent(m_myself.getPointer());
		if(m_Descriptor != Mp7JrsMixedCollectionType_Descriptor_CollectionPtr())
		{
			m_Descriptor->SetContentName(XMLString::transcode("Descriptor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType1(const Mp7JrsMixedCollectionType_Collection1Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType1().");
	}
	if (m_MixedCollectionType_LocalType1 != item)
	{
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType1);
		m_MixedCollectionType_LocalType1 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MixedCollectionType_LocalType1) m_MixedCollectionType_LocalType1->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType2(const Mp7JrsMixedCollectionType_Collection2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetMixedCollectionType_LocalType2().");
	}
	if (m_MixedCollectionType_LocalType2 != item)
	{
		// Dc1Factory::DeleteObject(m_MixedCollectionType_LocalType2);
		m_MixedCollectionType_LocalType2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MixedCollectionType_LocalType2) m_MixedCollectionType_LocalType2->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMixedCollectionType::Getname() const
{
	return GetBase()->Getname();
}

bool Mp7JrsMixedCollectionType::Existname() const
{
	return GetBase()->Existname();
}
void Mp7JrsMixedCollectionType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::Setname().");
	}
	GetBase()->Setname(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::Invalidatename()
{
	GetBase()->Invalidatename();
}
Mp7JrsCreationInformationPtr Mp7JrsMixedCollectionType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsMixedCollectionType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsMixedCollectionType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsMixedCollectionType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsCollectionType_TextAnnotation_CollectionPtr Mp7JrsMixedCollectionType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMixedCollectionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMixedCollectionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMixedCollectionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMixedCollectionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::SetTextAnnotation(const Mp7JrsCollectionType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMixedCollectionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsMixedCollectionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsMixedCollectionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMixedCollectionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMixedCollectionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMixedCollectionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMixedCollectionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMixedCollectionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMixedCollectionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMixedCollectionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMixedCollectionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMixedCollectionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMixedCollectionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMixedCollectionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMixedCollectionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMixedCollectionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMixedCollectionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMixedCollectionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMixedCollectionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMixedCollectionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Content")) == 0)
	{
		// Content is contained in itemtype MixedCollectionType_LocalType
		// in choice collection MixedCollectionType_CollectionType

		context->Found = true;
		Mp7JrsMixedCollectionType_CollectionPtr coll = GetMixedCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType; // FTT, check this
				SetMixedCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->GetContent()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_LocalPtr item = CreateMixedCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMultimediaContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->SetContent(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->GetContent()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ContentRef")) == 0)
	{
		// ContentRef is contained in itemtype MixedCollectionType_LocalType
		// in choice collection MixedCollectionType_CollectionType

		context->Found = true;
		Mp7JrsMixedCollectionType_CollectionPtr coll = GetMixedCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType; // FTT, check this
				SetMixedCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->GetContentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_LocalPtr item = CreateMixedCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->SetContentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_LocalPtr)coll->elementAt(i))->GetContentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Segment")) == 0)
	{
		// Segment is contained in itemtype MixedCollectionType_LocalType0
		// in choice collection MixedCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection0Ptr coll = GetMixedCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType0; // FTT, check this
				SetMixedCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->GetSegment()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local0Ptr item = CreateMixedCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->SetSegment(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->GetSegment()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SegmentRef")) == 0)
	{
		// SegmentRef is contained in itemtype MixedCollectionType_LocalType0
		// in choice collection MixedCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection0Ptr coll = GetMixedCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType0; // FTT, check this
				SetMixedCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local0Ptr item = CreateMixedCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->SetSegmentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Descriptor")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Descriptor is item of abstract type DType
		// in element collection MixedCollectionType_Descriptor_CollectionType
		
		context->Found = true;
		Mp7JrsMixedCollectionType_Descriptor_CollectionPtr coll = GetDescriptor();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_Descriptor_CollectionType; // FTT, check this
				SetDescriptor(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Concept")) == 0)
	{
		// Concept is contained in itemtype MixedCollectionType_LocalType1
		// in choice collection MixedCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection1Ptr coll = GetMixedCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType1; // FTT, check this
				SetMixedCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->GetConcept()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local1Ptr item = CreateMixedCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSemanticBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->SetConcept(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->GetConcept()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ConceptRef")) == 0)
	{
		// ConceptRef is contained in itemtype MixedCollectionType_LocalType1
		// in choice collection MixedCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection1Ptr coll = GetMixedCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType1; // FTT, check this
				SetMixedCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->GetConceptRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local1Ptr item = CreateMixedCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->SetConceptRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local1Ptr)coll->elementAt(i))->GetConceptRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MixedCollection")) == 0)
	{
		// MixedCollection is contained in itemtype MixedCollectionType_LocalType2
		// in choice collection MixedCollectionType_CollectionType2

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection2Ptr coll = GetMixedCollectionType_LocalType2();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType2; // FTT, check this
				SetMixedCollectionType_LocalType2(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->GetMixedCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local2Ptr item = CreateMixedCollectionType_LocalType2; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MixedCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMixedCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->SetMixedCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->GetMixedCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MixedCollectionRef")) == 0)
	{
		// MixedCollectionRef is contained in itemtype MixedCollectionType_LocalType2
		// in choice collection MixedCollectionType_CollectionType2

		context->Found = true;
		Mp7JrsMixedCollectionType_Collection2Ptr coll = GetMixedCollectionType_LocalType2();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMixedCollectionType_CollectionType2; // FTT, check this
				SetMixedCollectionType_LocalType2(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->GetMixedCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsMixedCollectionType_Local2Ptr item = CreateMixedCollectionType_LocalType2; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->SetMixedCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsMixedCollectionType_Local2Ptr)coll->elementAt(i))->GetMixedCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MixedCollectionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MixedCollectionType");
	}
	return result;
}

XMLCh * Mp7JrsMixedCollectionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMixedCollectionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMixedCollectionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MixedCollectionType"));
	// Element serialization:
	if (m_MixedCollectionType_LocalType != Mp7JrsMixedCollectionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MixedCollectionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MixedCollectionType_LocalType0 != Mp7JrsMixedCollectionType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MixedCollectionType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Descriptor != Mp7JrsMixedCollectionType_Descriptor_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Descriptor->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MixedCollectionType_LocalType1 != Mp7JrsMixedCollectionType_Collection1Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MixedCollectionType_LocalType1->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MixedCollectionType_LocalType2 != Mp7JrsMixedCollectionType_Collection2Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MixedCollectionType_LocalType2->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMixedCollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsCollectionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Content
			Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Content")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ContentRef
			Dc1Util::HasNodeName(parent, X("ContentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ContentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMixedCollectionType_CollectionPtr tmp = CreateMixedCollectionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMixedCollectionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Segment
			Dc1Util::HasNodeName(parent, X("Segment"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Segment")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SegmentRef
			Dc1Util::HasNodeName(parent, X("SegmentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SegmentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMixedCollectionType_Collection0Ptr tmp = CreateMixedCollectionType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMixedCollectionType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Descriptor"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Descriptor")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMixedCollectionType_Descriptor_CollectionPtr tmp = CreateMixedCollectionType_Descriptor_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescriptor(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Concept
			Dc1Util::HasNodeName(parent, X("Concept"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Concept")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ConceptRef
			Dc1Util::HasNodeName(parent, X("ConceptRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ConceptRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMixedCollectionType_Collection1Ptr tmp = CreateMixedCollectionType_CollectionType1; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMixedCollectionType_LocalType1(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:MixedCollection
			Dc1Util::HasNodeName(parent, X("MixedCollection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MixedCollection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:MixedCollectionRef
			Dc1Util::HasNodeName(parent, X("MixedCollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:MixedCollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsMixedCollectionType_Collection2Ptr tmp = CreateMixedCollectionType_CollectionType2; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMixedCollectionType_LocalType2(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMixedCollectionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Content")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ContentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ContentRef")) == 0)
	)
  {
	Mp7JrsMixedCollectionType_CollectionPtr tmp = CreateMixedCollectionType_CollectionType; // FTT, check this
	this->SetMixedCollectionType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Segment"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Segment")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SegmentRef")) == 0)
	)
  {
	Mp7JrsMixedCollectionType_Collection0Ptr tmp = CreateMixedCollectionType_CollectionType0; // FTT, check this
	this->SetMixedCollectionType_LocalType0(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Descriptor")) == 0))
  {
	Mp7JrsMixedCollectionType_Descriptor_CollectionPtr tmp = CreateMixedCollectionType_Descriptor_CollectionType; // FTT, check this
	this->SetDescriptor(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Concept"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Concept")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ConceptRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ConceptRef")) == 0)
	)
  {
	Mp7JrsMixedCollectionType_Collection1Ptr tmp = CreateMixedCollectionType_CollectionType1; // FTT, check this
	this->SetMixedCollectionType_LocalType1(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("MixedCollection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("MixedCollection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("MixedCollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("MixedCollectionRef")) == 0)
	)
  {
	Mp7JrsMixedCollectionType_Collection2Ptr tmp = CreateMixedCollectionType_CollectionType2; // FTT, check this
	this->SetMixedCollectionType_LocalType2(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMixedCollectionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMixedCollectionType_LocalType() != Dc1NodePtr())
		result.Insert(GetMixedCollectionType_LocalType());
	if (GetMixedCollectionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetMixedCollectionType_LocalType0());
	if (GetDescriptor() != Dc1NodePtr())
		result.Insert(GetDescriptor());
	if (GetMixedCollectionType_LocalType1() != Dc1NodePtr())
		result.Insert(GetMixedCollectionType_LocalType1());
	if (GetMixedCollectionType_LocalType2() != Dc1NodePtr())
		result.Insert(GetMixedCollectionType_LocalType2());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMixedCollectionType_ExtMethodImpl.h


