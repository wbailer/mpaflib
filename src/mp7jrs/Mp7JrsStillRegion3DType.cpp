
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtImplInclude.h


#include "Mp7JrsSegmentType.h"
#include "Mp7JrsStillRegion3DType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionType.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsStillRegion3DType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsStillRegion3DType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsStillRegion3DSpatialDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SpatialDecomposition

#include <assert.h>
IMp7JrsStillRegion3DType::IMp7JrsStillRegion3DType()
{

// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtPropInit.h

}

IMp7JrsStillRegion3DType::~IMp7JrsStillRegion3DType()
{
// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtPropCleanup.h

}

Mp7JrsStillRegion3DType::Mp7JrsStillRegion3DType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStillRegion3DType::~Mp7JrsStillRegion3DType()
{
	Cleanup();
}

void Mp7JrsStillRegion3DType::Init()
{
	// Init base
	m_Base = CreateSegmentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_StillRegion3DType_LocalType = Mp7JrsStillRegion3DType_CollectionPtr(); // Collection
	m_MultipleView = Mp7JrsMultipleViewPtr(); // Class
	m_MultipleView_Exist = false;
	m_SpatialDecomposition = Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtMyPropInit.h

}

void Mp7JrsStillRegion3DType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_StillRegion3DType_LocalType);
	// Dc1Factory::DeleteObject(m_MultipleView);
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
}

void Mp7JrsStillRegion3DType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StillRegion3DTypePtr, since we
	// might need GetBase(), which isn't defined in IStillRegion3DType
	const Dc1Ptr< Mp7JrsStillRegion3DType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSegmentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStillRegion3DType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_StillRegion3DType_LocalType);
		this->SetStillRegion3DType_LocalType(Dc1Factory::CloneObject(tmp->GetStillRegion3DType_LocalType()));
	if (tmp->IsValidMultipleView())
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		this->SetMultipleView(Dc1Factory::CloneObject(tmp->GetMultipleView()));
	}
	else
	{
		InvalidateMultipleView();
	}
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		this->SetSpatialDecomposition(Dc1Factory::CloneObject(tmp->GetSpatialDecomposition()));
}

Dc1NodePtr Mp7JrsStillRegion3DType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsStillRegion3DType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSegmentType > Mp7JrsStillRegion3DType::GetBase() const
{
	return m_Base;
}

Mp7JrsStillRegion3DType_CollectionPtr Mp7JrsStillRegion3DType::GetStillRegion3DType_LocalType() const
{
		return m_StillRegion3DType_LocalType;
}

Mp7JrsMultipleViewPtr Mp7JrsStillRegion3DType::GetMultipleView() const
{
		return m_MultipleView;
}

// Element is optional
bool Mp7JrsStillRegion3DType::IsValidMultipleView() const
{
	return m_MultipleView_Exist;
}

Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr Mp7JrsStillRegion3DType::GetSpatialDecomposition() const
{
		return m_SpatialDecomposition;
}

void Mp7JrsStillRegion3DType::SetStillRegion3DType_LocalType(const Mp7JrsStillRegion3DType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetStillRegion3DType_LocalType().");
	}
	if (m_StillRegion3DType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegion3DType_LocalType);
		m_StillRegion3DType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegion3DType_LocalType) m_StillRegion3DType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetMultipleView().");
	}
	if (m_MultipleView != item || m_MultipleView_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MultipleView);
		m_MultipleView = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MultipleView) m_MultipleView->SetParent(m_myself.getPointer());
		if (m_MultipleView && m_MultipleView->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MultipleView->UseTypeAttribute = true;
		}
		m_MultipleView_Exist = true;
		if(m_MultipleView != Mp7JrsMultipleViewPtr())
		{
			m_MultipleView->SetContentName(XMLString::transcode("MultipleView"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidateMultipleView()
{
	m_MultipleView_Exist = false;
}
void Mp7JrsStillRegion3DType::SetSpatialDecomposition(const Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetSpatialDecomposition().");
	}
	if (m_SpatialDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		m_SpatialDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialDecomposition) m_SpatialDecomposition->SetParent(m_myself.getPointer());
		if(m_SpatialDecomposition != Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr())
		{
			m_SpatialDecomposition->SetContentName(XMLString::transcode("SpatialDecomposition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsStillRegion3DType::GetMediaInformation() const
{
	return GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegion3DType::GetMediaInformationRef() const
{
	return GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsStillRegion3DType::GetMediaLocator() const
{
	return GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsStillRegion3DType::GetStructuralUnit() const
{
	return GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsStillRegion3DType::IsValidStructuralUnit() const
{
	return GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsStillRegion3DType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegion3DType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsStillRegion3DType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsStillRegion3DType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsStillRegion3DType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsStillRegion3DType::GetSegmentType_LocalType() const
{
	return GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsStillRegion3DType::GetMatchingHint() const
{
	return GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsStillRegion3DType::GetPointOfView() const
{
	return GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsStillRegion3DType::GetRelation() const
{
	return GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegion3DType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetMediaInformation().");
	}
	GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetMediaInformationRef().");
	}
	GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetMediaLocator().");
	}
	GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetStructuralUnit().");
	}
	GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidateStructuralUnit()
{
	GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegion3DType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStillRegion3DType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegion3DType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetSegmentType_LocalType().");
	}
	GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetMatchingHint().");
	}
	GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetPointOfView().");
	}
	GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetRelation().");
	}
	GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsStillRegion3DType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsStillRegion3DType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsStillRegion3DType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegion3DType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsStillRegion3DType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsStillRegion3DType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsStillRegion3DType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsStillRegion3DType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsStillRegion3DType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegion3DType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsStillRegion3DType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsStillRegion3DType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsStillRegion3DType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsStillRegion3DType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsStillRegion3DType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegion3DType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsStillRegion3DType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsStillRegion3DType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegion3DType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStillRegion3DType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VisualDescriptor")) == 0)
	{
		// VisualDescriptor is contained in itemtype StillRegion3DType_LocalType
		// in choice collection StillRegion3DType_CollectionType

		context->Found = true;
		Mp7JrsStillRegion3DType_CollectionPtr coll = GetStillRegion3DType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegion3DType_CollectionType; // FTT, check this
				SetStillRegion3DType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegion3DType_LocalPtr item = CreateStillRegion3DType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->SetVisualDescriptor(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->GetVisualDescriptor()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualDescriptionScheme")) == 0)
	{
		// VisualDescriptionScheme is contained in itemtype StillRegion3DType_LocalType
		// in choice collection StillRegion3DType_CollectionType

		context->Found = true;
		Mp7JrsStillRegion3DType_CollectionPtr coll = GetStillRegion3DType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegion3DType_CollectionType; // FTT, check this
				SetStillRegion3DType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStillRegion3DType_LocalPtr item = CreateStillRegion3DType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsVisualDSPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->SetVisualDescriptionScheme(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStillRegion3DType_LocalPtr)coll->elementAt(i))->GetVisualDescriptionScheme()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MultipleView")) == 0)
	{
		// MultipleView is simple element MultipleViewType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMultipleView()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MultipleViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMultipleViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMultipleView(p, client);
					if((p = GetMultipleView()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialDecomposition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SpatialDecomposition is item of type StillRegion3DSpatialDecompositionType
		// in element collection StillRegion3DType_SpatialDecomposition_CollectionType
		
		context->Found = true;
		Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr coll = GetSpatialDecomposition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStillRegion3DType_SpatialDecomposition_CollectionType; // FTT, check this
				SetSpatialDecomposition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegion3DSpatialDecompositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStillRegion3DSpatialDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StillRegion3DType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StillRegion3DType");
	}
	return result;
}

XMLCh * Mp7JrsStillRegion3DType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStillRegion3DType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStillRegion3DType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StillRegion3DType"));
	// Element serialization:
	if (m_StillRegion3DType_LocalType != Mp7JrsStillRegion3DType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StillRegion3DType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MultipleView != Mp7JrsMultipleViewPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MultipleView->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MultipleView"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MultipleView->Serialize(doc, element, &elem);
	}
	if (m_SpatialDecomposition != Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpatialDecomposition->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsStillRegion3DType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsSegmentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptor
			Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptor")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme
			Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:VisualDescriptionScheme")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStillRegion3DType_CollectionPtr tmp = CreateStillRegion3DType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStillRegion3DType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MultipleView"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MultipleView")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMultipleViewType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMultipleView(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SpatialDecomposition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr tmp = CreateStillRegion3DType_SpatialDecomposition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpatialDecomposition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStillRegion3DType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptor")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0)
	)
  {
	Mp7JrsStillRegion3DType_CollectionPtr tmp = CreateStillRegion3DType_CollectionType; // FTT, check this
	this->SetStillRegion3DType_LocalType(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MultipleView")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMultipleViewType; // FTT, check this
	}
	this->SetMultipleView(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SpatialDecomposition")) == 0))
  {
	Mp7JrsStillRegion3DType_SpatialDecomposition_CollectionPtr tmp = CreateStillRegion3DType_SpatialDecomposition_CollectionType; // FTT, check this
	this->SetSpatialDecomposition(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStillRegion3DType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStillRegion3DType_LocalType() != Dc1NodePtr())
		result.Insert(GetStillRegion3DType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetSpatialDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatialDecomposition());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStillRegion3DType_ExtMethodImpl.h


