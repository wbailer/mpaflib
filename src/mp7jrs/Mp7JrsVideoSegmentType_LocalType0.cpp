
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtImplInclude.h


#include "Mp7JrsVideoSegmentSpatialDecompositionType.h"
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h"
#include "Mp7JrsVideoSegmentSpatioTemporalDecompositionType.h"
#include "Mp7JrsVideoSegmentMediaSourceDecompositionType.h"
#include "Mp7JrsVideoSegmentType_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsVideoSegmentType_LocalType0::IMp7JrsVideoSegmentType_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtPropInit.h

}

IMp7JrsVideoSegmentType_LocalType0::~IMp7JrsVideoSegmentType_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtPropCleanup.h

}

Mp7JrsVideoSegmentType_LocalType0::Mp7JrsVideoSegmentType_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSegmentType_LocalType0::~Mp7JrsVideoSegmentType_LocalType0()
{
	Cleanup();
}

void Mp7JrsVideoSegmentType_LocalType0::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_SpatialDecomposition = Mp7JrsVideoSegmentSpatialDecompositionPtr(); // Class
	m_TemporalDecomposition = Mp7JrsVideoSegmentTemporalDecompositionPtr(); // Class
	m_SpatioTemporalDecomposition = Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr(); // Class
	m_MediaSourceDecomposition = Mp7JrsVideoSegmentMediaSourceDecompositionPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtMyPropInit.h

}

void Mp7JrsVideoSegmentType_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
}

void Mp7JrsVideoSegmentType_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSegmentType_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in IVideoSegmentType_LocalType0
	const Dc1Ptr< Mp7JrsVideoSegmentType_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		this->SetSpatialDecomposition(Dc1Factory::CloneObject(tmp->GetSpatialDecomposition()));
		// Dc1Factory::DeleteObject(m_TemporalDecomposition);
		this->SetTemporalDecomposition(Dc1Factory::CloneObject(tmp->GetTemporalDecomposition()));
		// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
		this->SetSpatioTemporalDecomposition(Dc1Factory::CloneObject(tmp->GetSpatioTemporalDecomposition()));
		// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
		this->SetMediaSourceDecomposition(Dc1Factory::CloneObject(tmp->GetMediaSourceDecomposition()));
}

Dc1NodePtr Mp7JrsVideoSegmentType_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoSegmentType_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsVideoSegmentSpatialDecompositionPtr Mp7JrsVideoSegmentType_LocalType0::GetSpatialDecomposition() const
{
		return m_SpatialDecomposition;
}

Mp7JrsVideoSegmentTemporalDecompositionPtr Mp7JrsVideoSegmentType_LocalType0::GetTemporalDecomposition() const
{
		return m_TemporalDecomposition;
}

Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr Mp7JrsVideoSegmentType_LocalType0::GetSpatioTemporalDecomposition() const
{
		return m_SpatioTemporalDecomposition;
}

Mp7JrsVideoSegmentMediaSourceDecompositionPtr Mp7JrsVideoSegmentType_LocalType0::GetMediaSourceDecomposition() const
{
		return m_MediaSourceDecomposition;
}

// implementing setter for choice 
/* element */
void Mp7JrsVideoSegmentType_LocalType0::SetSpatialDecomposition(const Mp7JrsVideoSegmentSpatialDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentType_LocalType0::SetSpatialDecomposition().");
	}
	if (m_SpatialDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialDecomposition);
		m_SpatialDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialDecomposition) m_SpatialDecomposition->SetParent(m_myself.getPointer());
		if (m_SpatialDecomposition && m_SpatialDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatialDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialDecomposition->UseTypeAttribute = true;
		}
		if(m_SpatialDecomposition != Mp7JrsVideoSegmentSpatialDecompositionPtr())
		{
			m_SpatialDecomposition->SetContentName(XMLString::transcode("SpatialDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsVideoSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsVideoSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsVideoSegmentType_LocalType0::SetTemporalDecomposition(const Mp7JrsVideoSegmentTemporalDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentType_LocalType0::SetTemporalDecomposition().");
	}
	if (m_TemporalDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_TemporalDecomposition);
		m_TemporalDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TemporalDecomposition) m_TemporalDecomposition->SetParent(m_myself.getPointer());
		if (m_TemporalDecomposition && m_TemporalDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentTemporalDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TemporalDecomposition->UseTypeAttribute = true;
		}
		if(m_TemporalDecomposition != Mp7JrsVideoSegmentTemporalDecompositionPtr())
		{
			m_TemporalDecomposition->SetContentName(XMLString::transcode("TemporalDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsVideoSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsVideoSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsVideoSegmentType_LocalType0::SetSpatioTemporalDecomposition(const Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentType_LocalType0::SetSpatioTemporalDecomposition().");
	}
	if (m_SpatioTemporalDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
		m_SpatioTemporalDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatioTemporalDecomposition) m_SpatioTemporalDecomposition->SetParent(m_myself.getPointer());
		if (m_SpatioTemporalDecomposition && m_SpatioTemporalDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentSpatioTemporalDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatioTemporalDecomposition->UseTypeAttribute = true;
		}
		if(m_SpatioTemporalDecomposition != Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr())
		{
			m_SpatioTemporalDecomposition->SetContentName(XMLString::transcode("SpatioTemporalDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsVideoSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsVideoSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
	m_MediaSourceDecomposition = Mp7JrsVideoSegmentMediaSourceDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsVideoSegmentType_LocalType0::SetMediaSourceDecomposition(const Mp7JrsVideoSegmentMediaSourceDecompositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSegmentType_LocalType0::SetMediaSourceDecomposition().");
	}
	if (m_MediaSourceDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_MediaSourceDecomposition);
		m_MediaSourceDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaSourceDecomposition) m_MediaSourceDecomposition->SetParent(m_myself.getPointer());
		if (m_MediaSourceDecomposition && m_MediaSourceDecomposition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentMediaSourceDecompositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaSourceDecomposition->UseTypeAttribute = true;
		}
		if(m_MediaSourceDecomposition != Mp7JrsVideoSegmentMediaSourceDecompositionPtr())
		{
			m_MediaSourceDecomposition->SetContentName(XMLString::transcode("MediaSourceDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_SpatialDecomposition);
	m_SpatialDecomposition = Mp7JrsVideoSegmentSpatialDecompositionPtr();
	// Dc1Factory::DeleteObject(m_TemporalDecomposition);
	m_TemporalDecomposition = Mp7JrsVideoSegmentTemporalDecompositionPtr();
	// Dc1Factory::DeleteObject(m_SpatioTemporalDecomposition);
	m_SpatioTemporalDecomposition = Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: VideoSegmentType_LocalType0: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsVideoSegmentType_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsVideoSegmentType_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoSegmentType_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoSegmentType_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_SpatialDecomposition != Mp7JrsVideoSegmentSpatialDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TemporalDecomposition != Mp7JrsVideoSegmentTemporalDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TemporalDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TemporalDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TemporalDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SpatioTemporalDecomposition != Mp7JrsVideoSegmentSpatioTemporalDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatioTemporalDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatioTemporalDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatioTemporalDecomposition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaSourceDecomposition != Mp7JrsVideoSegmentMediaSourceDecompositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaSourceDecomposition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaSourceDecomposition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaSourceDecomposition->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVideoSegmentType_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSegmentSpatialDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TemporalDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSegmentTemporalDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTemporalDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatioTemporalDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatioTemporalDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSegmentSpatioTemporalDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatioTemporalDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaSourceDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaSourceDecomposition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSegmentMediaSourceDecompositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaSourceDecomposition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSegmentType_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("SpatialDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSegmentSpatialDecompositionType; // FTT, check this
	}
	this->SetSpatialDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("TemporalDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSegmentTemporalDecompositionType; // FTT, check this
	}
	this->SetTemporalDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("SpatioTemporalDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSegmentSpatioTemporalDecompositionType; // FTT, check this
	}
	this->SetSpatioTemporalDecomposition(child);
  }
  if (XMLString::compareString(elementname, X("MediaSourceDecomposition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSegmentMediaSourceDecompositionType; // FTT, check this
	}
	this->SetMediaSourceDecomposition(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSegmentType_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSpatialDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatialDecomposition());
	if (GetTemporalDecomposition() != Dc1NodePtr())
		result.Insert(GetTemporalDecomposition());
	if (GetSpatioTemporalDecomposition() != Dc1NodePtr())
		result.Insert(GetSpatioTemporalDecomposition());
	if (GetMediaSourceDecomposition() != Dc1NodePtr())
		result.Insert(GetMediaSourceDecomposition());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSegmentType_LocalType0_ExtMethodImpl.h


