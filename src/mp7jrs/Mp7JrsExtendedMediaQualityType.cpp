
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtImplInclude.h


#include "Mp7JrsMediaQualityType.h"
#include "Mp7JrsQCProfileType.h"
#include "Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionType.h"
#include "Mp7JrsPersonType.h"
#include "Mp7JrsCreationToolType.h"
#include "Mp7JrsTextAnnotationType.h"
#include "Mp7JrsMediaQualityType_QualityRating_CollectionType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsMediaQualityType_RatingInformationLocator_CollectionType.h"
#include "Mp7JrsMediaQualityType_PerceptibleDefects_LocalType.h"
#include "Mp7JrsExtendedMediaQualityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMediaQualityType_QualityRating_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QualityRating
#include "Mp7JrsReferenceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RatingInformationLocator
#include "Mp7JrsQCItemResultType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QCItemResult

#include <assert.h>
IMp7JrsExtendedMediaQualityType::IMp7JrsExtendedMediaQualityType()
{

// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtPropInit.h

}

IMp7JrsExtendedMediaQualityType::~IMp7JrsExtendedMediaQualityType()
{
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtPropCleanup.h

}

Mp7JrsExtendedMediaQualityType::Mp7JrsExtendedMediaQualityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsExtendedMediaQualityType::~Mp7JrsExtendedMediaQualityType()
{
	Cleanup();
}

void Mp7JrsExtendedMediaQualityType::Init()
{
	// Init base
	m_Base = CreateMediaQualityType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_QCProfile = Mp7JrsQCProfilePtr(); // Class
	m_QCProfile_Exist = false;
	m_QCItemResult = Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr(); // Collection
	m_Operator = Mp7JrsPersonPtr(); // Class
	m_Operator_Exist = false;
	m_UsedTool = Mp7JrsCreationToolPtr(); // Class
	m_UsedTool_Exist = false;
	m_Annotation = Mp7JrsTextAnnotationPtr(); // Class
	m_Annotation_Exist = false;


// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMyPropInit.h

}

void Mp7JrsExtendedMediaQualityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_QCProfile);
	// Dc1Factory::DeleteObject(m_QCItemResult);
	// Dc1Factory::DeleteObject(m_Operator);
	// Dc1Factory::DeleteObject(m_UsedTool);
	// Dc1Factory::DeleteObject(m_Annotation);
}

void Mp7JrsExtendedMediaQualityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ExtendedMediaQualityTypePtr, since we
	// might need GetBase(), which isn't defined in IExtendedMediaQualityType
	const Dc1Ptr< Mp7JrsExtendedMediaQualityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaQualityPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsExtendedMediaQualityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidQCProfile())
	{
		// Dc1Factory::DeleteObject(m_QCProfile);
		this->SetQCProfile(Dc1Factory::CloneObject(tmp->GetQCProfile()));
	}
	else
	{
		InvalidateQCProfile();
	}
		// Dc1Factory::DeleteObject(m_QCItemResult);
		this->SetQCItemResult(Dc1Factory::CloneObject(tmp->GetQCItemResult()));
	if (tmp->IsValidOperator())
	{
		// Dc1Factory::DeleteObject(m_Operator);
		this->SetOperator(Dc1Factory::CloneObject(tmp->GetOperator()));
	}
	else
	{
		InvalidateOperator();
	}
	if (tmp->IsValidUsedTool())
	{
		// Dc1Factory::DeleteObject(m_UsedTool);
		this->SetUsedTool(Dc1Factory::CloneObject(tmp->GetUsedTool()));
	}
	else
	{
		InvalidateUsedTool();
	}
	if (tmp->IsValidAnnotation())
	{
		// Dc1Factory::DeleteObject(m_Annotation);
		this->SetAnnotation(Dc1Factory::CloneObject(tmp->GetAnnotation()));
	}
	else
	{
		InvalidateAnnotation();
	}
}

Dc1NodePtr Mp7JrsExtendedMediaQualityType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsExtendedMediaQualityType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMediaQualityType > Mp7JrsExtendedMediaQualityType::GetBase() const
{
	return m_Base;
}

Mp7JrsQCProfilePtr Mp7JrsExtendedMediaQualityType::GetQCProfile() const
{
		return m_QCProfile;
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidQCProfile() const
{
	return m_QCProfile_Exist;
}

Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr Mp7JrsExtendedMediaQualityType::GetQCItemResult() const
{
		return m_QCItemResult;
}

Mp7JrsPersonPtr Mp7JrsExtendedMediaQualityType::GetOperator() const
{
		return m_Operator;
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidOperator() const
{
	return m_Operator_Exist;
}

Mp7JrsCreationToolPtr Mp7JrsExtendedMediaQualityType::GetUsedTool() const
{
		return m_UsedTool;
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidUsedTool() const
{
	return m_UsedTool_Exist;
}

Mp7JrsTextAnnotationPtr Mp7JrsExtendedMediaQualityType::GetAnnotation() const
{
		return m_Annotation;
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidAnnotation() const
{
	return m_Annotation_Exist;
}

void Mp7JrsExtendedMediaQualityType::SetQCProfile(const Mp7JrsQCProfilePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetQCProfile().");
	}
	if (m_QCProfile != item || m_QCProfile_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_QCProfile);
		m_QCProfile = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QCProfile) m_QCProfile->SetParent(m_myself.getPointer());
		if (m_QCProfile && m_QCProfile->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_QCProfile->UseTypeAttribute = true;
		}
		m_QCProfile_Exist = true;
		if(m_QCProfile != Mp7JrsQCProfilePtr())
		{
			m_QCProfile->SetContentName(XMLString::transcode("QCProfile"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidateQCProfile()
{
	m_QCProfile_Exist = false;
}
void Mp7JrsExtendedMediaQualityType::SetQCItemResult(const Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetQCItemResult().");
	}
	if (m_QCItemResult != item)
	{
		// Dc1Factory::DeleteObject(m_QCItemResult);
		m_QCItemResult = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QCItemResult) m_QCItemResult->SetParent(m_myself.getPointer());
		if(m_QCItemResult != Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr())
		{
			m_QCItemResult->SetContentName(XMLString::transcode("QCItemResult"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::SetOperator(const Mp7JrsPersonPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetOperator().");
	}
	if (m_Operator != item || m_Operator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Operator);
		m_Operator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Operator) m_Operator->SetParent(m_myself.getPointer());
		if (m_Operator && m_Operator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Operator->UseTypeAttribute = true;
		}
		m_Operator_Exist = true;
		if(m_Operator != Mp7JrsPersonPtr())
		{
			m_Operator->SetContentName(XMLString::transcode("Operator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidateOperator()
{
	m_Operator_Exist = false;
}
void Mp7JrsExtendedMediaQualityType::SetUsedTool(const Mp7JrsCreationToolPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetUsedTool().");
	}
	if (m_UsedTool != item || m_UsedTool_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_UsedTool);
		m_UsedTool = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsedTool) m_UsedTool->SetParent(m_myself.getPointer());
		if (m_UsedTool && m_UsedTool->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UsedTool->UseTypeAttribute = true;
		}
		m_UsedTool_Exist = true;
		if(m_UsedTool != Mp7JrsCreationToolPtr())
		{
			m_UsedTool->SetContentName(XMLString::transcode("UsedTool"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidateUsedTool()
{
	m_UsedTool_Exist = false;
}
void Mp7JrsExtendedMediaQualityType::SetAnnotation(const Mp7JrsTextAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetAnnotation().");
	}
	if (m_Annotation != item || m_Annotation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Annotation);
		m_Annotation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Annotation) m_Annotation->SetParent(m_myself.getPointer());
		if (m_Annotation && m_Annotation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Annotation->UseTypeAttribute = true;
		}
		m_Annotation_Exist = true;
		if(m_Annotation != Mp7JrsTextAnnotationPtr())
		{
			m_Annotation->SetContentName(XMLString::transcode("Annotation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidateAnnotation()
{
	m_Annotation_Exist = false;
}
Mp7JrsMediaQualityType_QualityRating_CollectionPtr Mp7JrsExtendedMediaQualityType::GetQualityRating() const
{
	return GetBase()->GetQualityRating();
}

Dc1Ptr< Dc1Node > Mp7JrsExtendedMediaQualityType::GetRatingSource() const
{
	return GetBase()->GetRatingSource();
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidRatingSource() const
{
	return GetBase()->IsValidRatingSource();
}

Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr Mp7JrsExtendedMediaQualityType::GetRatingInformationLocator() const
{
	return GetBase()->GetRatingInformationLocator();
}

Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr Mp7JrsExtendedMediaQualityType::GetPerceptibleDefects() const
{
	return GetBase()->GetPerceptibleDefects();
}

// Element is optional
bool Mp7JrsExtendedMediaQualityType::IsValidPerceptibleDefects() const
{
	return GetBase()->IsValidPerceptibleDefects();
}

void Mp7JrsExtendedMediaQualityType::SetQualityRating(const Mp7JrsMediaQualityType_QualityRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetQualityRating().");
	}
	GetBase()->SetQualityRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::SetRatingSource(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetRatingSource().");
	}
	GetBase()->SetRatingSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidateRatingSource()
{
	GetBase()->InvalidateRatingSource();
}
void Mp7JrsExtendedMediaQualityType::SetRatingInformationLocator(const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetRatingInformationLocator().");
	}
	GetBase()->SetRatingInformationLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::SetPerceptibleDefects(const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedMediaQualityType::SetPerceptibleDefects().");
	}
	GetBase()->SetPerceptibleDefects(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedMediaQualityType::InvalidatePerceptibleDefects()
{
	GetBase()->InvalidatePerceptibleDefects();
}

Dc1NodeEnum Mp7JrsExtendedMediaQualityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("QCProfile")) == 0)
	{
		// QCProfile is simple element QCProfileType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetQCProfile()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCProfilePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetQCProfile(p, client);
					if((p = GetQCProfile()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("QCItemResult")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:QCItemResult is item of type QCItemResultType
		// in element collection ExtendedMediaQualityType_QCItemResult_CollectionType
		
		context->Found = true;
		Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr coll = GetQCItemResult();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateExtendedMediaQualityType_QCItemResult_CollectionType; // FTT, check this
				SetQCItemResult(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCItemResultType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCItemResultPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Operator")) == 0)
	{
		// Operator is simple element PersonType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOperator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOperator(p, client);
					if((p = GetOperator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsedTool")) == 0)
	{
		// UsedTool is simple element CreationToolType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUsedTool()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUsedTool(p, client);
					if((p = GetUsedTool()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Annotation")) == 0)
	{
		// Annotation is simple element TextAnnotationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAnnotation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextAnnotationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextAnnotationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAnnotation(p, client);
					if((p = GetAnnotation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ExtendedMediaQualityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ExtendedMediaQualityType");
	}
	return result;
}

XMLCh * Mp7JrsExtendedMediaQualityType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsExtendedMediaQualityType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsExtendedMediaQualityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ExtendedMediaQualityType"));
	// Element serialization:
	// Class
	
	if (m_QCProfile != Mp7JrsQCProfilePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_QCProfile->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("QCProfile"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_QCProfile->Serialize(doc, element, &elem);
	}
	if (m_QCItemResult != Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QCItemResult->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Operator != Mp7JrsPersonPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Operator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Operator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Operator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_UsedTool != Mp7JrsCreationToolPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UsedTool->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UsedTool"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UsedTool->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Annotation != Mp7JrsTextAnnotationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Annotation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Annotation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Annotation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsExtendedMediaQualityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaQualityType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("QCProfile"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("QCProfile")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateQCProfileType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetQCProfile(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("QCItemResult"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("QCItemResult")) == 0))
		{
			// Deserialize factory type
			Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr tmp = CreateExtendedMediaQualityType_QCItemResult_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQCItemResult(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Operator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Operator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePersonType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOperator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UsedTool"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UsedTool")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCreationToolType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUsedTool(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Annotation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Annotation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextAnnotationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAnnotation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsExtendedMediaQualityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("QCProfile")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateQCProfileType; // FTT, check this
	}
	this->SetQCProfile(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("QCItemResult")) == 0))
  {
	Mp7JrsExtendedMediaQualityType_QCItemResult_CollectionPtr tmp = CreateExtendedMediaQualityType_QCItemResult_CollectionType; // FTT, check this
	this->SetQCItemResult(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Operator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePersonType; // FTT, check this
	}
	this->SetOperator(child);
  }
  if (XMLString::compareString(elementname, X("UsedTool")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCreationToolType; // FTT, check this
	}
	this->SetUsedTool(child);
  }
  if (XMLString::compareString(elementname, X("Annotation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextAnnotationType; // FTT, check this
	}
	this->SetAnnotation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsExtendedMediaQualityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetQCProfile() != Dc1NodePtr())
		result.Insert(GetQCProfile());
	if (GetQCItemResult() != Dc1NodePtr())
		result.Insert(GetQCItemResult());
	if (GetOperator() != Dc1NodePtr())
		result.Insert(GetOperator());
	if (GetUsedTool() != Dc1NodePtr())
		result.Insert(GetUsedTool());
	if (GetAnnotation() != Dc1NodePtr())
		result.Insert(GetAnnotation());
	if (GetQualityRating() != Dc1NodePtr())
		result.Insert(GetQualityRating());
	if (GetRatingSource() != Dc1NodePtr())
		result.Insert(GetRatingSource());
	if (GetRatingInformationLocator() != Dc1NodePtr())
		result.Insert(GetRatingInformationLocator());
	if (GetPerceptibleDefects() != Dc1NodePtr())
		result.Insert(GetPerceptibleDefects());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsExtendedMediaQualityType_ExtMethodImpl.h


