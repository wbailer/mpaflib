
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7Jrsunsigned6.h"
#include "Mp7JrsColorLayoutType_YACCoeff2_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff5_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff9_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff14_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff20_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff27_LocalType.h"
#include "Mp7JrsColorLayoutType_YACCoeff63_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff2_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff2_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff5_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff5_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff9_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff9_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff14_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff14_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff20_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff20_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff27_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff27_LocalType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff63_LocalType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff63_LocalType.h"
#include "Mp7JrsColorLayoutType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsColorLayoutType::IMp7JrsColorLayoutType()
{

// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtPropInit.h

}

IMp7JrsColorLayoutType::~IMp7JrsColorLayoutType()
{
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtPropCleanup.h

}

Mp7JrsColorLayoutType::Mp7JrsColorLayoutType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsColorLayoutType::~Mp7JrsColorLayoutType()
{
	Cleanup();
}

void Mp7JrsColorLayoutType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_YDCCoeff = Mp7Jrsunsigned6Ptr(); // Class with content 
	m_CbDCCoeff = Mp7Jrsunsigned6Ptr(); // Class with content 
	m_CrDCCoeff = Mp7Jrsunsigned6Ptr(); // Class with content 
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr(); // Class
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr(); // Class
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr(); // Class
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr(); // Class
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr(); // Class
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr(); // Class
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr(); // Class
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr(); // Class
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr(); // Class
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr(); // Class
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr(); // Class
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr(); // Class
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr(); // Class
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr(); // Class
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr(); // Class
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr(); // Class
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr(); // Class
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr(); // Class
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr(); // Class
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr(); // Class
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr(); // Class


// begin extension included
// file Mp7JrsColorLayoutType_ExtMyPropInit.h


	// create DC coefficients
	m_YDCCoeff = Createunsigned6;
	m_CbDCCoeff = Createunsigned3;
	m_CrDCCoeff = Createunsigned3;

	// create default set of AC coefficients
	unsigned y=6;
	unsigned c=3;
	
// end extension included

}

void Mp7JrsColorLayoutType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_YDCCoeff);
	// Dc1Factory::DeleteObject(m_CbDCCoeff);
	// Dc1Factory::DeleteObject(m_CrDCCoeff);
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
}

void Mp7JrsColorLayoutType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ColorLayoutTypePtr, since we
	// might need GetBase(), which isn't defined in IColorLayoutType
	const Dc1Ptr< Mp7JrsColorLayoutType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsColorLayoutType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_YDCCoeff);
		this->SetYDCCoeff(Dc1Factory::CloneObject(tmp->GetYDCCoeff()));
		// Dc1Factory::DeleteObject(m_CbDCCoeff);
		this->SetCbDCCoeff(Dc1Factory::CloneObject(tmp->GetCbDCCoeff()));
		// Dc1Factory::DeleteObject(m_CrDCCoeff);
		this->SetCrDCCoeff(Dc1Factory::CloneObject(tmp->GetCrDCCoeff()));
		// Dc1Factory::DeleteObject(m_YACCoeff2);
		this->SetYACCoeff2(Dc1Factory::CloneObject(tmp->GetYACCoeff2()));
		// Dc1Factory::DeleteObject(m_YACCoeff5);
		this->SetYACCoeff5(Dc1Factory::CloneObject(tmp->GetYACCoeff5()));
		// Dc1Factory::DeleteObject(m_YACCoeff9);
		this->SetYACCoeff9(Dc1Factory::CloneObject(tmp->GetYACCoeff9()));
		// Dc1Factory::DeleteObject(m_YACCoeff14);
		this->SetYACCoeff14(Dc1Factory::CloneObject(tmp->GetYACCoeff14()));
		// Dc1Factory::DeleteObject(m_YACCoeff20);
		this->SetYACCoeff20(Dc1Factory::CloneObject(tmp->GetYACCoeff20()));
		// Dc1Factory::DeleteObject(m_YACCoeff27);
		this->SetYACCoeff27(Dc1Factory::CloneObject(tmp->GetYACCoeff27()));
		// Dc1Factory::DeleteObject(m_YACCoeff63);
		this->SetYACCoeff63(Dc1Factory::CloneObject(tmp->GetYACCoeff63()));
		// Dc1Factory::DeleteObject(m_CbACCoeff2);
		this->SetCbACCoeff2(Dc1Factory::CloneObject(tmp->GetCbACCoeff2()));
		// Dc1Factory::DeleteObject(m_CrACCoeff2);
		this->SetCrACCoeff2(Dc1Factory::CloneObject(tmp->GetCrACCoeff2()));
		// Dc1Factory::DeleteObject(m_CbACCoeff5);
		this->SetCbACCoeff5(Dc1Factory::CloneObject(tmp->GetCbACCoeff5()));
		// Dc1Factory::DeleteObject(m_CrACCoeff5);
		this->SetCrACCoeff5(Dc1Factory::CloneObject(tmp->GetCrACCoeff5()));
		// Dc1Factory::DeleteObject(m_CbACCoeff9);
		this->SetCbACCoeff9(Dc1Factory::CloneObject(tmp->GetCbACCoeff9()));
		// Dc1Factory::DeleteObject(m_CrACCoeff9);
		this->SetCrACCoeff9(Dc1Factory::CloneObject(tmp->GetCrACCoeff9()));
		// Dc1Factory::DeleteObject(m_CbACCoeff14);
		this->SetCbACCoeff14(Dc1Factory::CloneObject(tmp->GetCbACCoeff14()));
		// Dc1Factory::DeleteObject(m_CrACCoeff14);
		this->SetCrACCoeff14(Dc1Factory::CloneObject(tmp->GetCrACCoeff14()));
		// Dc1Factory::DeleteObject(m_CbACCoeff20);
		this->SetCbACCoeff20(Dc1Factory::CloneObject(tmp->GetCbACCoeff20()));
		// Dc1Factory::DeleteObject(m_CrACCoeff20);
		this->SetCrACCoeff20(Dc1Factory::CloneObject(tmp->GetCrACCoeff20()));
		// Dc1Factory::DeleteObject(m_CbACCoeff27);
		this->SetCbACCoeff27(Dc1Factory::CloneObject(tmp->GetCbACCoeff27()));
		// Dc1Factory::DeleteObject(m_CrACCoeff27);
		this->SetCrACCoeff27(Dc1Factory::CloneObject(tmp->GetCrACCoeff27()));
		// Dc1Factory::DeleteObject(m_CbACCoeff63);
		this->SetCbACCoeff63(Dc1Factory::CloneObject(tmp->GetCbACCoeff63()));
		// Dc1Factory::DeleteObject(m_CrACCoeff63);
		this->SetCrACCoeff63(Dc1Factory::CloneObject(tmp->GetCrACCoeff63()));
}

Dc1NodePtr Mp7JrsColorLayoutType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsColorLayoutType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsColorLayoutType::GetBase() const
{
	return m_Base;
}

Mp7Jrsunsigned6Ptr Mp7JrsColorLayoutType::GetYDCCoeff() const
{
		return m_YDCCoeff;
}

Mp7Jrsunsigned6Ptr Mp7JrsColorLayoutType::GetCbDCCoeff() const
{
		return m_CbDCCoeff;
}

Mp7Jrsunsigned6Ptr Mp7JrsColorLayoutType::GetCrDCCoeff() const
{
		return m_CrDCCoeff;
}

Mp7JrsColorLayoutType_YACCoeff2_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff2() const
{
		return m_YACCoeff2;
}

Mp7JrsColorLayoutType_YACCoeff5_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff5() const
{
		return m_YACCoeff5;
}

Mp7JrsColorLayoutType_YACCoeff9_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff9() const
{
		return m_YACCoeff9;
}

Mp7JrsColorLayoutType_YACCoeff14_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff14() const
{
		return m_YACCoeff14;
}

Mp7JrsColorLayoutType_YACCoeff20_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff20() const
{
		return m_YACCoeff20;
}

Mp7JrsColorLayoutType_YACCoeff27_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff27() const
{
		return m_YACCoeff27;
}

Mp7JrsColorLayoutType_YACCoeff63_LocalPtr Mp7JrsColorLayoutType::GetYACCoeff63() const
{
		return m_YACCoeff63;
}

Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff2() const
{
		return m_CbACCoeff2;
}

Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff2() const
{
		return m_CrACCoeff2;
}

Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff5() const
{
		return m_CbACCoeff5;
}

Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff5() const
{
		return m_CrACCoeff5;
}

Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff9() const
{
		return m_CbACCoeff9;
}

Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff9() const
{
		return m_CrACCoeff9;
}

Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff14() const
{
		return m_CbACCoeff14;
}

Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff14() const
{
		return m_CrACCoeff14;
}

Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff20() const
{
		return m_CbACCoeff20;
}

Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff20() const
{
		return m_CrACCoeff20;
}

Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff27() const
{
		return m_CbACCoeff27;
}

Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff27() const
{
		return m_CrACCoeff27;
}

Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr Mp7JrsColorLayoutType::GetCbACCoeff63() const
{
		return m_CbACCoeff63;
}

Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr Mp7JrsColorLayoutType::GetCrACCoeff63() const
{
		return m_CrACCoeff63;
}

void Mp7JrsColorLayoutType::SetYDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYDCCoeff().");
	}
	if (m_YDCCoeff != item)
	{
		// Dc1Factory::DeleteObject(m_YDCCoeff);
		m_YDCCoeff = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YDCCoeff) m_YDCCoeff->SetParent(m_myself.getPointer());
		if (m_YDCCoeff && m_YDCCoeff->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YDCCoeff->UseTypeAttribute = true;
		}
		if(m_YDCCoeff != Mp7Jrsunsigned6Ptr())
		{
			m_YDCCoeff->SetContentName(XMLString::transcode("YDCCoeff"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCbDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbDCCoeff().");
	}
	if (m_CbDCCoeff != item)
	{
		// Dc1Factory::DeleteObject(m_CbDCCoeff);
		m_CbDCCoeff = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbDCCoeff) m_CbDCCoeff->SetParent(m_myself.getPointer());
		if (m_CbDCCoeff && m_CbDCCoeff->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbDCCoeff->UseTypeAttribute = true;
		}
		if(m_CbDCCoeff != Mp7Jrsunsigned6Ptr())
		{
			m_CbDCCoeff->SetContentName(XMLString::transcode("CbDCCoeff"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrDCCoeff(const Mp7Jrsunsigned6Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrDCCoeff().");
	}
	if (m_CrDCCoeff != item)
	{
		// Dc1Factory::DeleteObject(m_CrDCCoeff);
		m_CrDCCoeff = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrDCCoeff) m_CrDCCoeff->SetParent(m_myself.getPointer());
		if (m_CrDCCoeff && m_CrDCCoeff->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrDCCoeff->UseTypeAttribute = true;
		}
		if(m_CrDCCoeff != Mp7Jrsunsigned6Ptr())
		{
			m_CrDCCoeff->SetContentName(XMLString::transcode("CrDCCoeff"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsColorLayoutType::SetYACCoeff2(const Mp7JrsColorLayoutType_YACCoeff2_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff2().");
	}
	if (m_YACCoeff2 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff2);
		m_YACCoeff2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff2) m_YACCoeff2->SetParent(m_myself.getPointer());
		if (m_YACCoeff2 && m_YACCoeff2->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff2_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff2->UseTypeAttribute = true;
		}
		if(m_YACCoeff2 != Mp7JrsColorLayoutType_YACCoeff2_LocalPtr())
		{
			m_YACCoeff2->SetContentName(XMLString::transcode("YACCoeff2"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff5(const Mp7JrsColorLayoutType_YACCoeff5_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff5().");
	}
	if (m_YACCoeff5 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff5);
		m_YACCoeff5 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff5) m_YACCoeff5->SetParent(m_myself.getPointer());
		if (m_YACCoeff5 && m_YACCoeff5->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff5_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff5->UseTypeAttribute = true;
		}
		if(m_YACCoeff5 != Mp7JrsColorLayoutType_YACCoeff5_LocalPtr())
		{
			m_YACCoeff5->SetContentName(XMLString::transcode("YACCoeff5"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff9(const Mp7JrsColorLayoutType_YACCoeff9_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff9().");
	}
	if (m_YACCoeff9 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff9);
		m_YACCoeff9 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff9) m_YACCoeff9->SetParent(m_myself.getPointer());
		if (m_YACCoeff9 && m_YACCoeff9->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff9_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff9->UseTypeAttribute = true;
		}
		if(m_YACCoeff9 != Mp7JrsColorLayoutType_YACCoeff9_LocalPtr())
		{
			m_YACCoeff9->SetContentName(XMLString::transcode("YACCoeff9"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff14(const Mp7JrsColorLayoutType_YACCoeff14_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff14().");
	}
	if (m_YACCoeff14 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff14);
		m_YACCoeff14 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff14) m_YACCoeff14->SetParent(m_myself.getPointer());
		if (m_YACCoeff14 && m_YACCoeff14->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff14_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff14->UseTypeAttribute = true;
		}
		if(m_YACCoeff14 != Mp7JrsColorLayoutType_YACCoeff14_LocalPtr())
		{
			m_YACCoeff14->SetContentName(XMLString::transcode("YACCoeff14"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff20(const Mp7JrsColorLayoutType_YACCoeff20_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff20().");
	}
	if (m_YACCoeff20 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff20);
		m_YACCoeff20 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff20) m_YACCoeff20->SetParent(m_myself.getPointer());
		if (m_YACCoeff20 && m_YACCoeff20->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff20_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff20->UseTypeAttribute = true;
		}
		if(m_YACCoeff20 != Mp7JrsColorLayoutType_YACCoeff20_LocalPtr())
		{
			m_YACCoeff20->SetContentName(XMLString::transcode("YACCoeff20"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff27(const Mp7JrsColorLayoutType_YACCoeff27_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff27().");
	}
	if (m_YACCoeff27 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff27);
		m_YACCoeff27 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff27) m_YACCoeff27->SetParent(m_myself.getPointer());
		if (m_YACCoeff27 && m_YACCoeff27->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff27_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff27->UseTypeAttribute = true;
		}
		if(m_YACCoeff27 != Mp7JrsColorLayoutType_YACCoeff27_LocalPtr())
		{
			m_YACCoeff27->SetContentName(XMLString::transcode("YACCoeff27"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff63);
	m_YACCoeff63 = Mp7JrsColorLayoutType_YACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsColorLayoutType::SetYACCoeff63(const Mp7JrsColorLayoutType_YACCoeff63_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetYACCoeff63().");
	}
	if (m_YACCoeff63 != item)
	{
		// Dc1Factory::DeleteObject(m_YACCoeff63);
		m_YACCoeff63 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_YACCoeff63) m_YACCoeff63->SetParent(m_myself.getPointer());
		if (m_YACCoeff63 && m_YACCoeff63->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff63_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_YACCoeff63->UseTypeAttribute = true;
		}
		if(m_YACCoeff63 != Mp7JrsColorLayoutType_YACCoeff63_LocalPtr())
		{
			m_YACCoeff63->SetContentName(XMLString::transcode("YACCoeff63"));
		}
	// Dc1Factory::DeleteObject(m_YACCoeff2);
	m_YACCoeff2 = Mp7JrsColorLayoutType_YACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff5);
	m_YACCoeff5 = Mp7JrsColorLayoutType_YACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff9);
	m_YACCoeff9 = Mp7JrsColorLayoutType_YACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff14);
	m_YACCoeff14 = Mp7JrsColorLayoutType_YACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff20);
	m_YACCoeff20 = Mp7JrsColorLayoutType_YACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_YACCoeff27);
	m_YACCoeff27 = Mp7JrsColorLayoutType_YACCoeff27_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff2(const Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff2().");
	}
	if (m_CbACCoeff2 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff2);
		m_CbACCoeff2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff2) m_CbACCoeff2->SetParent(m_myself.getPointer());
		if (m_CbACCoeff2 && m_CbACCoeff2->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff2_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff2->UseTypeAttribute = true;
		}
		if(m_CbACCoeff2 != Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr())
		{
			m_CbACCoeff2->SetContentName(XMLString::transcode("CbACCoeff2"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff2(const Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff2().");
	}
	if (m_CrACCoeff2 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff2);
		m_CrACCoeff2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff2) m_CrACCoeff2->SetParent(m_myself.getPointer());
		if (m_CrACCoeff2 && m_CrACCoeff2->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff2_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff2->UseTypeAttribute = true;
		}
		if(m_CrACCoeff2 != Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr())
		{
			m_CrACCoeff2->SetContentName(XMLString::transcode("CrACCoeff2"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff5(const Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff5().");
	}
	if (m_CbACCoeff5 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff5);
		m_CbACCoeff5 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff5) m_CbACCoeff5->SetParent(m_myself.getPointer());
		if (m_CbACCoeff5 && m_CbACCoeff5->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff5_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff5->UseTypeAttribute = true;
		}
		if(m_CbACCoeff5 != Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr())
		{
			m_CbACCoeff5->SetContentName(XMLString::transcode("CbACCoeff5"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff5(const Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff5().");
	}
	if (m_CrACCoeff5 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff5);
		m_CrACCoeff5 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff5) m_CrACCoeff5->SetParent(m_myself.getPointer());
		if (m_CrACCoeff5 && m_CrACCoeff5->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff5_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff5->UseTypeAttribute = true;
		}
		if(m_CrACCoeff5 != Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr())
		{
			m_CrACCoeff5->SetContentName(XMLString::transcode("CrACCoeff5"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff9(const Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff9().");
	}
	if (m_CbACCoeff9 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff9);
		m_CbACCoeff9 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff9) m_CbACCoeff9->SetParent(m_myself.getPointer());
		if (m_CbACCoeff9 && m_CbACCoeff9->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff9_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff9->UseTypeAttribute = true;
		}
		if(m_CbACCoeff9 != Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr())
		{
			m_CbACCoeff9->SetContentName(XMLString::transcode("CbACCoeff9"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff9(const Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff9().");
	}
	if (m_CrACCoeff9 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff9);
		m_CrACCoeff9 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff9) m_CrACCoeff9->SetParent(m_myself.getPointer());
		if (m_CrACCoeff9 && m_CrACCoeff9->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff9_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff9->UseTypeAttribute = true;
		}
		if(m_CrACCoeff9 != Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr())
		{
			m_CrACCoeff9->SetContentName(XMLString::transcode("CrACCoeff9"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff14(const Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff14().");
	}
	if (m_CbACCoeff14 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff14);
		m_CbACCoeff14 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff14) m_CbACCoeff14->SetParent(m_myself.getPointer());
		if (m_CbACCoeff14 && m_CbACCoeff14->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff14_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff14->UseTypeAttribute = true;
		}
		if(m_CbACCoeff14 != Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr())
		{
			m_CbACCoeff14->SetContentName(XMLString::transcode("CbACCoeff14"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff14(const Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff14().");
	}
	if (m_CrACCoeff14 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff14);
		m_CrACCoeff14 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff14) m_CrACCoeff14->SetParent(m_myself.getPointer());
		if (m_CrACCoeff14 && m_CrACCoeff14->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff14_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff14->UseTypeAttribute = true;
		}
		if(m_CrACCoeff14 != Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr())
		{
			m_CrACCoeff14->SetContentName(XMLString::transcode("CrACCoeff14"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff20(const Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff20().");
	}
	if (m_CbACCoeff20 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff20);
		m_CbACCoeff20 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff20) m_CbACCoeff20->SetParent(m_myself.getPointer());
		if (m_CbACCoeff20 && m_CbACCoeff20->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff20_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff20->UseTypeAttribute = true;
		}
		if(m_CbACCoeff20 != Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr())
		{
			m_CbACCoeff20->SetContentName(XMLString::transcode("CbACCoeff20"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff20(const Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff20().");
	}
	if (m_CrACCoeff20 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff20);
		m_CrACCoeff20 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff20) m_CrACCoeff20->SetParent(m_myself.getPointer());
		if (m_CrACCoeff20 && m_CrACCoeff20->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff20_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff20->UseTypeAttribute = true;
		}
		if(m_CrACCoeff20 != Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr())
		{
			m_CrACCoeff20->SetContentName(XMLString::transcode("CrACCoeff20"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff27(const Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff27().");
	}
	if (m_CbACCoeff27 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff27);
		m_CbACCoeff27 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff27) m_CbACCoeff27->SetParent(m_myself.getPointer());
		if (m_CbACCoeff27 && m_CbACCoeff27->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff27_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff27->UseTypeAttribute = true;
		}
		if(m_CbACCoeff27 != Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr())
		{
			m_CbACCoeff27->SetContentName(XMLString::transcode("CbACCoeff27"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff27(const Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff27().");
	}
	if (m_CrACCoeff27 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff27);
		m_CrACCoeff27 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff27) m_CrACCoeff27->SetParent(m_myself.getPointer());
		if (m_CrACCoeff27 && m_CrACCoeff27->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff27_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff27->UseTypeAttribute = true;
		}
		if(m_CrACCoeff27 != Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr())
		{
			m_CrACCoeff27->SetContentName(XMLString::transcode("CrACCoeff27"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff63);
	m_CbACCoeff63 = Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff63);
	m_CrACCoeff63 = Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* sequence */
void Mp7JrsColorLayoutType::SetCbACCoeff63(const Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCbACCoeff63().");
	}
	if (m_CbACCoeff63 != item)
	{
		// Dc1Factory::DeleteObject(m_CbACCoeff63);
		m_CbACCoeff63 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CbACCoeff63) m_CbACCoeff63->SetParent(m_myself.getPointer());
		if (m_CbACCoeff63 && m_CbACCoeff63->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff63_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CbACCoeff63->UseTypeAttribute = true;
		}
		if(m_CbACCoeff63 != Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr())
		{
			m_CbACCoeff63->SetContentName(XMLString::transcode("CbACCoeff63"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorLayoutType::SetCrACCoeff63(const Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorLayoutType::SetCrACCoeff63().");
	}
	if (m_CrACCoeff63 != item)
	{
		// Dc1Factory::DeleteObject(m_CrACCoeff63);
		m_CrACCoeff63 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrACCoeff63) m_CrACCoeff63->SetParent(m_myself.getPointer());
		if (m_CrACCoeff63 && m_CrACCoeff63->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff63_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrACCoeff63->UseTypeAttribute = true;
		}
		if(m_CrACCoeff63 != Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr())
		{
			m_CrACCoeff63->SetContentName(XMLString::transcode("CrACCoeff63"));
		}
	// Dc1Factory::DeleteObject(m_CbACCoeff2);
	m_CbACCoeff2 = Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff2);
	m_CrACCoeff2 = Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff5);
	m_CbACCoeff5 = Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff5);
	m_CrACCoeff5 = Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff9);
	m_CbACCoeff9 = Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff9);
	m_CrACCoeff9 = Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff14);
	m_CbACCoeff14 = Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff14);
	m_CrACCoeff14 = Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff20);
	m_CbACCoeff20 = Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff20);
	m_CrACCoeff20 = Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr();
	// Dc1Factory::DeleteObject(m_CbACCoeff27);
	m_CbACCoeff27 = Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr();
	// Dc1Factory::DeleteObject(m_CrACCoeff27);
	m_CrACCoeff27 = Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsColorLayoutType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("YDCCoeff")) == 0)
	{
		// YDCCoeff is simple element unsigned6
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYDCCoeff()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned6Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYDCCoeff(p, client);
					if((p = GetYDCCoeff()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbDCCoeff")) == 0)
	{
		// CbDCCoeff is simple element unsigned6
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbDCCoeff()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned6Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbDCCoeff(p, client);
					if((p = GetCbDCCoeff()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrDCCoeff")) == 0)
	{
		// CrDCCoeff is simple element unsigned6
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrDCCoeff()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned6")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned6Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrDCCoeff(p, client);
					if((p = GetCrDCCoeff()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff2")) == 0)
	{
		// YACCoeff2 is simple element ColorLayoutType_YACCoeff2_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff2()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff2_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff2_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff2(p, client);
					if((p = GetYACCoeff2()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff5")) == 0)
	{
		// YACCoeff5 is simple element ColorLayoutType_YACCoeff5_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff5()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff5_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff5_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff5(p, client);
					if((p = GetYACCoeff5()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff9")) == 0)
	{
		// YACCoeff9 is simple element ColorLayoutType_YACCoeff9_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff9()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff9_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff9_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff9(p, client);
					if((p = GetYACCoeff9()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff14")) == 0)
	{
		// YACCoeff14 is simple element ColorLayoutType_YACCoeff14_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff14()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff14_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff14_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff14(p, client);
					if((p = GetYACCoeff14()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff20")) == 0)
	{
		// YACCoeff20 is simple element ColorLayoutType_YACCoeff20_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff20()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff20_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff20_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff20(p, client);
					if((p = GetYACCoeff20()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff27")) == 0)
	{
		// YACCoeff27 is simple element ColorLayoutType_YACCoeff27_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff27()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff27_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff27_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff27(p, client);
					if((p = GetYACCoeff27()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("YACCoeff63")) == 0)
	{
		// YACCoeff63 is simple element ColorLayoutType_YACCoeff63_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetYACCoeff63()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_YACCoeff63_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_YACCoeff63_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetYACCoeff63(p, client);
					if((p = GetYACCoeff63()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff2")) == 0)
	{
		// CbACCoeff2 is simple element ColorLayoutType_CbACCoeff2_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff2()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff2_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff2(p, client);
					if((p = GetCbACCoeff2()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff2")) == 0)
	{
		// CrACCoeff2 is simple element ColorLayoutType_CrACCoeff2_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff2()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff2_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff2(p, client);
					if((p = GetCrACCoeff2()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff5")) == 0)
	{
		// CbACCoeff5 is simple element ColorLayoutType_CbACCoeff5_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff5()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff5_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff5(p, client);
					if((p = GetCbACCoeff5()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff5")) == 0)
	{
		// CrACCoeff5 is simple element ColorLayoutType_CrACCoeff5_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff5()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff5_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff5(p, client);
					if((p = GetCrACCoeff5()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff9")) == 0)
	{
		// CbACCoeff9 is simple element ColorLayoutType_CbACCoeff9_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff9()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff9_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff9(p, client);
					if((p = GetCbACCoeff9()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff9")) == 0)
	{
		// CrACCoeff9 is simple element ColorLayoutType_CrACCoeff9_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff9()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff9_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff9(p, client);
					if((p = GetCrACCoeff9()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff14")) == 0)
	{
		// CbACCoeff14 is simple element ColorLayoutType_CbACCoeff14_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff14()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff14_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff14(p, client);
					if((p = GetCbACCoeff14()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff14")) == 0)
	{
		// CrACCoeff14 is simple element ColorLayoutType_CrACCoeff14_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff14()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff14_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff14(p, client);
					if((p = GetCrACCoeff14()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff20")) == 0)
	{
		// CbACCoeff20 is simple element ColorLayoutType_CbACCoeff20_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff20()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff20_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff20(p, client);
					if((p = GetCbACCoeff20()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff20")) == 0)
	{
		// CrACCoeff20 is simple element ColorLayoutType_CrACCoeff20_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff20()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff20_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff20(p, client);
					if((p = GetCrACCoeff20()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff27")) == 0)
	{
		// CbACCoeff27 is simple element ColorLayoutType_CbACCoeff27_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff27()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff27_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff27(p, client);
					if((p = GetCbACCoeff27()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff27")) == 0)
	{
		// CrACCoeff27 is simple element ColorLayoutType_CrACCoeff27_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff27()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff27_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff27(p, client);
					if((p = GetCrACCoeff27()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CbACCoeff63")) == 0)
	{
		// CbACCoeff63 is simple element ColorLayoutType_CbACCoeff63_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCbACCoeff63()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CbACCoeff63_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCbACCoeff63(p, client);
					if((p = GetCbACCoeff63()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrACCoeff63")) == 0)
	{
		// CrACCoeff63 is simple element ColorLayoutType_CrACCoeff63_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrACCoeff63()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType_CrACCoeff63_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrACCoeff63(p, client);
					if((p = GetCrACCoeff63()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ColorLayoutType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ColorLayoutType");
	}
	return result;
}

XMLCh * Mp7JrsColorLayoutType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsColorLayoutType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsColorLayoutType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ColorLayoutType"));
	// Element serialization:
	// Class with content		
	
	if (m_YDCCoeff != Mp7Jrsunsigned6Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YDCCoeff->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YDCCoeff"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YDCCoeff->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_CbDCCoeff != Mp7Jrsunsigned6Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbDCCoeff->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbDCCoeff"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbDCCoeff->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_CrDCCoeff != Mp7Jrsunsigned6Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrDCCoeff->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrDCCoeff"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrDCCoeff->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff2 != Mp7JrsColorLayoutType_YACCoeff2_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff2->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff2"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff2->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff5 != Mp7JrsColorLayoutType_YACCoeff5_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff5->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff5"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff5->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff9 != Mp7JrsColorLayoutType_YACCoeff9_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff9->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff9"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff9->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff14 != Mp7JrsColorLayoutType_YACCoeff14_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff14->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff14"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff14->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff20 != Mp7JrsColorLayoutType_YACCoeff20_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff20->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff20"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff20->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff27 != Mp7JrsColorLayoutType_YACCoeff27_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff27->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff27"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff27->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_YACCoeff63 != Mp7JrsColorLayoutType_YACCoeff63_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_YACCoeff63->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("YACCoeff63"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_YACCoeff63->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff2 != Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff2->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff2"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff2->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff2 != Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff2->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff2"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff2->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff5 != Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff5->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff5"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff5->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff5 != Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff5->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff5"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff5->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff9 != Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff9->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff9"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff9->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff9 != Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff9->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff9"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff9->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff14 != Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff14->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff14"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff14->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff14 != Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff14->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff14"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff14->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff20 != Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff20->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff20"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff20->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff20 != Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff20->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff20"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff20->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff27 != Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff27->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff27"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff27->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff27 != Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff27->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff27"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff27->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CbACCoeff63 != Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CbACCoeff63->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CbACCoeff63"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CbACCoeff63->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrACCoeff63 != Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrACCoeff63->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrACCoeff63"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrACCoeff63->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsColorLayoutType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YDCCoeff"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YDCCoeff")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned6; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYDCCoeff(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbDCCoeff"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbDCCoeff")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned6; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbDCCoeff(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrDCCoeff"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrDCCoeff")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned6; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrDCCoeff(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff2"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff2")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff2_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff2(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff5"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff5")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff5_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff5(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff9"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff9")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff9_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff9(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff14"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff14")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff14_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff14(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff20"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff20")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff20_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff20(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff27"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff27")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff27_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff27(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("YACCoeff63"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("YACCoeff63")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_YACCoeff63_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetYACCoeff63(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff2"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff2")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff2_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff2(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff2"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff2")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff2_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff2(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff5"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff5")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff5_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff5(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff5"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff5")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff5_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff5(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff9"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff9")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff9_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff9(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff9"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff9")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff9_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff9(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff14"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff14")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff14_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff14(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff14"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff14")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff14_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff14(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff20"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff20")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff20_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff20(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff20"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff20")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff20_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff20(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff27"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff27")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff27_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff27(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff27"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff27")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff27_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff27(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CbACCoeff63"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CbACCoeff63")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CbACCoeff63_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCbACCoeff63(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrACCoeff63"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrACCoeff63")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType_CrACCoeff63_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrACCoeff63(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}

				// FIXME: sat 2004-04-15: `continue' would be good here; we
				//finished deserializing the sequence but don't know if it
				//succeeded, so we can't.

				// continue;
	} while(false);
// no includefile for extension defined 
// file Mp7JrsColorLayoutType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsColorLayoutType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("YDCCoeff")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned6; // FTT, check this
	}
	this->SetYDCCoeff(child);
  }
  if (XMLString::compareString(elementname, X("CbDCCoeff")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned6; // FTT, check this
	}
	this->SetCbDCCoeff(child);
  }
  if (XMLString::compareString(elementname, X("CrDCCoeff")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned6; // FTT, check this
	}
	this->SetCrDCCoeff(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff2")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff2_LocalType; // FTT, check this
	}
	this->SetYACCoeff2(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff5")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff5_LocalType; // FTT, check this
	}
	this->SetYACCoeff5(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff9")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff9_LocalType; // FTT, check this
	}
	this->SetYACCoeff9(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff14")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff14_LocalType; // FTT, check this
	}
	this->SetYACCoeff14(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff20")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff20_LocalType; // FTT, check this
	}
	this->SetYACCoeff20(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff27")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff27_LocalType; // FTT, check this
	}
	this->SetYACCoeff27(child);
  }
  if (XMLString::compareString(elementname, X("YACCoeff63")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_YACCoeff63_LocalType; // FTT, check this
	}
	this->SetYACCoeff63(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff2")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff2_LocalType; // FTT, check this
	}
	this->SetCbACCoeff2(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff2")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff2_LocalType; // FTT, check this
	}
	this->SetCrACCoeff2(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff5")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff5_LocalType; // FTT, check this
	}
	this->SetCbACCoeff5(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff5")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff5_LocalType; // FTT, check this
	}
	this->SetCrACCoeff5(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff9")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff9_LocalType; // FTT, check this
	}
	this->SetCbACCoeff9(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff9")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff9_LocalType; // FTT, check this
	}
	this->SetCrACCoeff9(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff14")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff14_LocalType; // FTT, check this
	}
	this->SetCbACCoeff14(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff14")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff14_LocalType; // FTT, check this
	}
	this->SetCrACCoeff14(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff20")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff20_LocalType; // FTT, check this
	}
	this->SetCbACCoeff20(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff20")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff20_LocalType; // FTT, check this
	}
	this->SetCrACCoeff20(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff27")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff27_LocalType; // FTT, check this
	}
	this->SetCbACCoeff27(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff27")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff27_LocalType; // FTT, check this
	}
	this->SetCrACCoeff27(child);
  }
  if (XMLString::compareString(elementname, X("CbACCoeff63")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CbACCoeff63_LocalType; // FTT, check this
	}
	this->SetCbACCoeff63(child);
  }
  if (XMLString::compareString(elementname, X("CrACCoeff63")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType_CrACCoeff63_LocalType; // FTT, check this
	}
	this->SetCrACCoeff63(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsColorLayoutType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetYDCCoeff() != Dc1NodePtr())
		result.Insert(GetYDCCoeff());
	if (GetCbDCCoeff() != Dc1NodePtr())
		result.Insert(GetCbDCCoeff());
	if (GetCrDCCoeff() != Dc1NodePtr())
		result.Insert(GetCrDCCoeff());
	if (GetYACCoeff2() != Dc1NodePtr())
		result.Insert(GetYACCoeff2());
	if (GetYACCoeff5() != Dc1NodePtr())
		result.Insert(GetYACCoeff5());
	if (GetYACCoeff9() != Dc1NodePtr())
		result.Insert(GetYACCoeff9());
	if (GetYACCoeff14() != Dc1NodePtr())
		result.Insert(GetYACCoeff14());
	if (GetYACCoeff20() != Dc1NodePtr())
		result.Insert(GetYACCoeff20());
	if (GetYACCoeff27() != Dc1NodePtr())
		result.Insert(GetYACCoeff27());
	if (GetYACCoeff63() != Dc1NodePtr())
		result.Insert(GetYACCoeff63());
	if (GetCbACCoeff2() != Dc1NodePtr())
		result.Insert(GetCbACCoeff2());
	if (GetCrACCoeff2() != Dc1NodePtr())
		result.Insert(GetCrACCoeff2());
	if (GetCbACCoeff5() != Dc1NodePtr())
		result.Insert(GetCbACCoeff5());
	if (GetCrACCoeff5() != Dc1NodePtr())
		result.Insert(GetCrACCoeff5());
	if (GetCbACCoeff9() != Dc1NodePtr())
		result.Insert(GetCbACCoeff9());
	if (GetCrACCoeff9() != Dc1NodePtr())
		result.Insert(GetCrACCoeff9());
	if (GetCbACCoeff14() != Dc1NodePtr())
		result.Insert(GetCbACCoeff14());
	if (GetCrACCoeff14() != Dc1NodePtr())
		result.Insert(GetCrACCoeff14());
	if (GetCbACCoeff20() != Dc1NodePtr())
		result.Insert(GetCbACCoeff20());
	if (GetCrACCoeff20() != Dc1NodePtr())
		result.Insert(GetCrACCoeff20());
	if (GetCbACCoeff27() != Dc1NodePtr())
		result.Insert(GetCbACCoeff27());
	if (GetCrACCoeff27() != Dc1NodePtr())
		result.Insert(GetCrACCoeff27());
	if (GetCbACCoeff63() != Dc1NodePtr())
		result.Insert(GetCbACCoeff63());
	if (GetCrACCoeff63() != Dc1NodePtr())
		result.Insert(GetCrACCoeff63());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsColorLayoutType_ExtMethodImpl.h
#include "Mp7JrsColorLayoutType_YACCoeff2_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff5_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff9_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff14_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff20_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff27_CollectionType.h"
#include "Mp7JrsColorLayoutType_YACCoeff63_CollectionType.h"

#include "Mp7JrsColorLayoutType_CbACCoeff2_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff5_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff9_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff14_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff20_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff27_CollectionType.h"
#include "Mp7JrsColorLayoutType_CbACCoeff63_CollectionType.h"

#include "Mp7JrsColorLayoutType_CrACCoeff2_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff5_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff9_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff14_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff20_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff27_CollectionType.h"
#include "Mp7JrsColorLayoutType_CrACCoeff63_CollectionType.h"

#include "Mp7Jrsunsigned5.h"
#include "Dc1Factory.h"

int _clNCoeffs[] = { 2, 5, 9, 14, 20, 27, 63 };
int _clNCoeffOptions = 7;

void IMp7JrsColorLayoutType::SetNrCoefficients(unsigned* y, unsigned* c, Dc1ClientID client)
{
	// subtract DC
	(*y)--; (*c)--;
	
	// change to valid numbers
	*y = _NrCoeffsToUse(*y);
	*c = _NrCoeffsToUse(*c);

	_nrYCoeff = *y;
	_nrCCoeff = *c;
	

	_CreateCoeffListsY(*y,client);

	_CreateCoeffListsC(*c,client);
	
}

void IMp7JrsColorLayoutType::SetY(unsigned* coeffs, Dc1ClientID client) {
	int i;
	GetYDCCoeff()->SetContent(coeffs[0],client);
	if (GetYACCoeff2()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff2_LocalPtr coll = GetYACCoeff2();
		for (i=1;i<=2;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff5()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff5_LocalPtr coll = GetYACCoeff5();
		for (i=1;i<=5;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff9()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff9_LocalPtr coll = GetYACCoeff9();
		for (i=1;i<=9;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff14()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff14_LocalPtr coll = GetYACCoeff14();
		for (i=1;i<=14;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff20()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff20_LocalPtr coll = GetYACCoeff20();
		for (i=1;i<=20;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff27()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff27_LocalPtr coll = GetYACCoeff27();
		for (i=1;i<=27;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	else if (GetYACCoeff63()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff63_LocalPtr coll = GetYACCoeff63();
		for (i=1;i<=63;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
}

void IMp7JrsColorLayoutType::GetY(unsigned* coeffs) {
	int i;
	coeffs[0] = GetYDCCoeff()->GetContent();

	if (GetYACCoeff2()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff2_LocalPtr coll = GetYACCoeff2();
		for (i=1;i<=2;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff5()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff5_LocalPtr coll = GetYACCoeff5();
		for (i=1;i<=5;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff9()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff9_LocalPtr coll = GetYACCoeff9();
		for (i=1;i<=9;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff14()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff14_LocalPtr coll = GetYACCoeff14();
		for (i=1;i<=14;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff20()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff20_LocalPtr coll = GetYACCoeff20();
		for (i=1;i<=20;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff27()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff27_LocalPtr coll = GetYACCoeff27();
		for (i=1;i<=2;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	else if (GetYACCoeff63()!=NULL) {
		Mp7JrsColorLayoutType_YACCoeff63_LocalPtr coll = GetYACCoeff63();
		for (i=1;i<=63;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
}

void IMp7JrsColorLayoutType::SetCb(unsigned* coeffs, Dc1ClientID client) {
	int i;
	GetCbDCCoeff()->SetContent(coeffs[0],client);
	if (GetCbACCoeff2() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr coll = GetCbACCoeff2();
		for (i=1;i<=2;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff5() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr coll = GetCbACCoeff5();
		for (i=1;i<=5;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff9() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr coll = GetCbACCoeff9();
		for (i=1;i<=9;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff14() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr coll = GetCbACCoeff14();
		for (i=1;i<=14;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff20() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr coll = GetCbACCoeff20();
		for (i=1;i<=20;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff27() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr coll = GetCbACCoeff27();
		for (i=1;i<=27;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCbACCoeff63() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr coll = GetCbACCoeff63();
		for (i=1;i<=63;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
}

void IMp7JrsColorLayoutType::GetCb(unsigned* coeffs) {
	int i;
	coeffs[0] = GetCbDCCoeff()->GetContent();
	if (GetCbACCoeff2() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr coll = GetCbACCoeff2();
		for (i=1;i<=2;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff5() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr coll = GetCbACCoeff5();
		for (i=1;i<=5;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff9() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr coll = GetCbACCoeff9();
		for (i=1;i<=9;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff14() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr coll = GetCbACCoeff14();
		for (i=1;i<=14;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff20() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr coll = GetCbACCoeff20();
		for (i=1;i<=20;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff27() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr coll = GetCbACCoeff27();
		for (i=1;i<=27;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCbACCoeff63() != NULL) {
		Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr coll = GetCbACCoeff63();
		for (i=1;i<=63;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
}

void IMp7JrsColorLayoutType::SetCr(unsigned* coeffs, Dc1ClientID client) {
	int i;
	GetCrDCCoeff()->SetContent(coeffs[0],client);
	if (GetCrACCoeff2() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr coll = GetCrACCoeff2();
		for (i=1;i<=2;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff5() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr coll = GetCrACCoeff5();
		for (i=1;i<=5;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff9() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr coll = GetCrACCoeff9();
		for (i=1;i<=9;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff14() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr coll = GetCrACCoeff14();
		for (i=1;i<=14;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff20() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr coll = GetCrACCoeff20();
		for (i=1;i<=20;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff27() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr coll = GetCrACCoeff27();
		for (i=1;i<=27;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
	if (GetCrACCoeff63() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr coll = GetCrACCoeff63();
		for (i=1;i<=63;i++) (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->SetContent(coeffs[i],client);
	}
}

void IMp7JrsColorLayoutType::GetCr(unsigned* coeffs) {
	int i;
	coeffs[0] = GetCrDCCoeff()->GetContent();
	if (GetCrACCoeff2() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr coll = GetCrACCoeff2();
		for (i=1;i<=2;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff5() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr coll = GetCrACCoeff5();
		for (i=1;i<=5;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff9() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr coll = GetCrACCoeff9();
		for (i=1;i<=9;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff14() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr coll = GetCrACCoeff14();
		for (i=1;i<=14;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff20() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr coll = GetCrACCoeff20();
		for (i=1;i<=20;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff27() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr coll = GetCrACCoeff27();
		for (i=1;i<=27;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
	if (GetCrACCoeff63() != NULL) {
		Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr coll = GetCrACCoeff63();
		for (i=1;i<=63;i++) coeffs[i] = (Mp7Jrsunsigned5Ptr(coll->elementAt(i-1)))->GetContent();
	}
}


unsigned IMp7JrsColorLayoutType::_NrCoeffsToUse(unsigned nrCoeffs) {
	unsigned i = 0;
	while (((int)i<=_clNCoeffOptions) && ((int)nrCoeffs>=_clNCoeffs[i])) i++;
	return _clNCoeffs[i-1];	
}


void IMp7JrsColorLayoutType::_CreateCoeffListsY(unsigned y, Dc1ClientID client)
{
	// create DC member
	Mp7Jrsunsigned6Ptr ydc = Createunsigned6;
	SetYDCCoeff(ydc);
	
	

	switch(y) {
	case 2: {
			Mp7JrsColorLayoutType_YACCoeff2_LocalPtr yLocal = CreateColorLayoutType_YACCoeff2_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff2(yLocal,client);
			break;
		}
	case 5: {
			Mp7JrsColorLayoutType_YACCoeff5_LocalPtr yLocal = CreateColorLayoutType_YACCoeff5_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff5(yLocal,client);
			break;
		}
	case 9: {
			Mp7JrsColorLayoutType_YACCoeff9_LocalPtr yLocal = CreateColorLayoutType_YACCoeff9_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff9(yLocal,client);
			break;
		}
	case 14: {
			Mp7JrsColorLayoutType_YACCoeff14_LocalPtr yLocal = CreateColorLayoutType_YACCoeff14_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff14(yLocal,client);
			break;
		}
	case 20: {
			Mp7JrsColorLayoutType_YACCoeff20_LocalPtr yLocal = CreateColorLayoutType_YACCoeff20_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff20(yLocal,client);
			break;
		}
	case 27: {
			Mp7JrsColorLayoutType_YACCoeff27_LocalPtr yLocal = CreateColorLayoutType_YACCoeff27_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff27(yLocal,client);
			break;
		}
	case 63: {
			Mp7JrsColorLayoutType_YACCoeff63_LocalPtr yLocal = CreateColorLayoutType_YACCoeff63_LocalType;
			for (int i=0; i<(int)y; i++) yLocal->addElement(Createunsigned5,client);
			SetYACCoeff63(yLocal,client);
			break;
		}
	}
}

void IMp7JrsColorLayoutType::_CreateCoeffListsC(unsigned c, Dc1ClientID client) {
	// create DC members
	Mp7Jrsunsigned6Ptr cbdc = Createunsigned6;
	SetCbDCCoeff(cbdc);
	Mp7Jrsunsigned6Ptr crdc = Createunsigned6;
	SetCrDCCoeff(crdc);


	int i;

	switch(c) {
	case 2: {
			Mp7JrsColorLayoutType_CbACCoeff2_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff2_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff2(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff2_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff2_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff2(crLocal,client);
			break;
		}
	case 5: {
			Mp7JrsColorLayoutType_CbACCoeff5_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff5_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff5(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff5_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff5_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff5(crLocal,client);
			break;
		}
	case 9: {
			Mp7JrsColorLayoutType_CbACCoeff9_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff9_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff9(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff9_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff9_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff9(crLocal,client);
			break;
		}
	case 14: {
			Mp7JrsColorLayoutType_CbACCoeff14_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff14_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff14(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff14_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff14_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff14(crLocal,client);
			break;
		}
	case 20: {
			Mp7JrsColorLayoutType_CbACCoeff20_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff20_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff20(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff20_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff20_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff20(crLocal,client);
			break;
		}
	case 27: {
			Mp7JrsColorLayoutType_CbACCoeff27_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff27_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff27(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff27_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff27_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff27(crLocal,client);
			break;
			 }
	case 63: {
			Mp7JrsColorLayoutType_CbACCoeff63_LocalPtr cbLocal = CreateColorLayoutType_CbACCoeff63_LocalType;
			for (i=0; i<(int)c; i++) cbLocal->addElement(Createunsigned5,client);
			SetCbACCoeff63(cbLocal,client);

			Mp7JrsColorLayoutType_CrACCoeff63_LocalPtr crLocal = CreateColorLayoutType_CrACCoeff63_LocalType;
			for (i=0; i<(int)c; i++) crLocal->addElement(Createunsigned5,client);
			SetCrACCoeff63(crLocal,client);
			break;
			 }
	}
}

unsigned IMp7JrsColorLayoutType::GetNrYCoeffs() {
	if (GetYACCoeff2() != NULL) return 3;
	else if (GetYACCoeff5() != NULL) return 6;
	else if (GetYACCoeff9() != NULL) return 10;
	else if (GetYACCoeff14() != NULL) return 15;
	else if (GetYACCoeff20() != NULL) return 21;
	else if (GetYACCoeff27() != NULL) return 28;
	else if (GetYACCoeff63() != NULL) return 64;
	else return 0;
}


unsigned IMp7JrsColorLayoutType::GetNrCCoeffs() {
	if (GetCbACCoeff2() != NULL && GetCrACCoeff2() != NULL) return 3;
	else if (GetCbACCoeff5() != NULL && GetCrACCoeff5() != NULL) return 6;
	else if (GetCbACCoeff9() != NULL && GetCrACCoeff9() != NULL) return 10;
	else if (GetCbACCoeff14() != NULL && GetCrACCoeff14() != NULL) return 15;
	else if (GetCbACCoeff20() != NULL && GetCrACCoeff20() != NULL) return 21;
	else if (GetCbACCoeff27() != NULL && GetCrACCoeff27() != NULL) return 28;
	else if (GetCbACCoeff63() != NULL && GetCrACCoeff63() != NULL) return 64;
	else return 0;
}
// end extension included


