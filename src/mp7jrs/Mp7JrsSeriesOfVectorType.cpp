
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtImplInclude.h


#include "Mp7JrsScalableSeriesType.h"
#include "Mp7JrsFloatMatrixType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsScalableSeriesType_Scaling_CollectionType.h"
#include "Mp7JrsSeriesOfVectorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsScalableSeriesType_Scaling_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Scaling

#include <assert.h>
IMp7JrsSeriesOfVectorType::IMp7JrsSeriesOfVectorType()
{

// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtPropInit.h

}

IMp7JrsSeriesOfVectorType::~IMp7JrsSeriesOfVectorType()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtPropCleanup.h

}

Mp7JrsSeriesOfVectorType::Mp7JrsSeriesOfVectorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSeriesOfVectorType::~Mp7JrsSeriesOfVectorType()
{
	Cleanup();
}

void Mp7JrsSeriesOfVectorType::Init()
{
	// Init base
	m_Base = CreateScalableSeriesType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_vectorSize = 1; // Value
	m_vectorSize_Default = 1; // Default value
	m_vectorSize_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Raw = Mp7JrsFloatMatrixPtr(); // Class
	m_Raw_Exist = false;
	m_Min = Mp7JrsFloatMatrixPtr(); // Class
	m_Min_Exist = false;
	m_Max = Mp7JrsFloatMatrixPtr(); // Class
	m_Max_Exist = false;
	m_Mean = Mp7JrsFloatMatrixPtr(); // Class
	m_Mean_Exist = false;
	m_Random = Mp7JrsFloatMatrixPtr(); // Class
	m_Random_Exist = false;
	m_First = Mp7JrsFloatMatrixPtr(); // Class
	m_First_Exist = false;
	m_Last = Mp7JrsFloatMatrixPtr(); // Class
	m_Last_Exist = false;
	m_Variance = Mp7JrsFloatMatrixPtr(); // Class
	m_Variance_Exist = false;
	m_LogBase = (0.0f); // Value

	m_LogBase_Exist = false;
	m_Covariance = Mp7JrsFloatMatrixPtr(); // Class
	m_Covariance_Exist = false;
	m_VarianceSummed = Mp7JrsfloatVectorPtr(); // Collection
	m_VarianceSummed_Exist = false;
	m_MaxSqDist = Mp7JrsfloatVectorPtr(); // Collection
	m_MaxSqDist_Exist = false;
	m_Weight = Mp7JrsfloatVectorPtr(); // Collection
	m_Weight_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtMyPropInit.h

}

void Mp7JrsSeriesOfVectorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Raw);
	// Dc1Factory::DeleteObject(m_Min);
	// Dc1Factory::DeleteObject(m_Max);
	// Dc1Factory::DeleteObject(m_Mean);
	// Dc1Factory::DeleteObject(m_Random);
	// Dc1Factory::DeleteObject(m_First);
	// Dc1Factory::DeleteObject(m_Last);
	// Dc1Factory::DeleteObject(m_Variance);
	// Dc1Factory::DeleteObject(m_Covariance);
	// Dc1Factory::DeleteObject(m_VarianceSummed);
	// Dc1Factory::DeleteObject(m_MaxSqDist);
	// Dc1Factory::DeleteObject(m_Weight);
}

void Mp7JrsSeriesOfVectorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SeriesOfVectorTypePtr, since we
	// might need GetBase(), which isn't defined in ISeriesOfVectorType
	const Dc1Ptr< Mp7JrsSeriesOfVectorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsScalableSeriesPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSeriesOfVectorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistvectorSize())
	{
		this->SetvectorSize(tmp->GetvectorSize());
	}
	else
	{
		InvalidatevectorSize();
	}
	}
	if (tmp->IsValidRaw())
	{
		// Dc1Factory::DeleteObject(m_Raw);
		this->SetRaw(Dc1Factory::CloneObject(tmp->GetRaw()));
	}
	else
	{
		InvalidateRaw();
	}
	if (tmp->IsValidMin())
	{
		// Dc1Factory::DeleteObject(m_Min);
		this->SetMin(Dc1Factory::CloneObject(tmp->GetMin()));
	}
	else
	{
		InvalidateMin();
	}
	if (tmp->IsValidMax())
	{
		// Dc1Factory::DeleteObject(m_Max);
		this->SetMax(Dc1Factory::CloneObject(tmp->GetMax()));
	}
	else
	{
		InvalidateMax();
	}
	if (tmp->IsValidMean())
	{
		// Dc1Factory::DeleteObject(m_Mean);
		this->SetMean(Dc1Factory::CloneObject(tmp->GetMean()));
	}
	else
	{
		InvalidateMean();
	}
	if (tmp->IsValidRandom())
	{
		// Dc1Factory::DeleteObject(m_Random);
		this->SetRandom(Dc1Factory::CloneObject(tmp->GetRandom()));
	}
	else
	{
		InvalidateRandom();
	}
	if (tmp->IsValidFirst())
	{
		// Dc1Factory::DeleteObject(m_First);
		this->SetFirst(Dc1Factory::CloneObject(tmp->GetFirst()));
	}
	else
	{
		InvalidateFirst();
	}
	if (tmp->IsValidLast())
	{
		// Dc1Factory::DeleteObject(m_Last);
		this->SetLast(Dc1Factory::CloneObject(tmp->GetLast()));
	}
	else
	{
		InvalidateLast();
	}
	if (tmp->IsValidVariance())
	{
		// Dc1Factory::DeleteObject(m_Variance);
		this->SetVariance(Dc1Factory::CloneObject(tmp->GetVariance()));
	}
	else
	{
		InvalidateVariance();
	}
	if (tmp->IsValidLogBase())
	{
		this->SetLogBase(tmp->GetLogBase());
	}
	else
	{
		InvalidateLogBase();
	}
	if (tmp->IsValidCovariance())
	{
		// Dc1Factory::DeleteObject(m_Covariance);
		this->SetCovariance(Dc1Factory::CloneObject(tmp->GetCovariance()));
	}
	else
	{
		InvalidateCovariance();
	}
	if (tmp->IsValidVarianceSummed())
	{
		// Dc1Factory::DeleteObject(m_VarianceSummed);
		this->SetVarianceSummed(Dc1Factory::CloneObject(tmp->GetVarianceSummed()));
	}
	else
	{
		InvalidateVarianceSummed();
	}
	if (tmp->IsValidMaxSqDist())
	{
		// Dc1Factory::DeleteObject(m_MaxSqDist);
		this->SetMaxSqDist(Dc1Factory::CloneObject(tmp->GetMaxSqDist()));
	}
	else
	{
		InvalidateMaxSqDist();
	}
	if (tmp->IsValidWeight())
	{
		// Dc1Factory::DeleteObject(m_Weight);
		this->SetWeight(Dc1Factory::CloneObject(tmp->GetWeight()));
	}
	else
	{
		InvalidateWeight();
	}
}

Dc1NodePtr Mp7JrsSeriesOfVectorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsSeriesOfVectorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsScalableSeriesType > Mp7JrsSeriesOfVectorType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsSeriesOfVectorType::GetvectorSize() const
{
	if (this->ExistvectorSize()) {
		return m_vectorSize;
	} else {
		return m_vectorSize_Default;
	}
}

bool Mp7JrsSeriesOfVectorType::ExistvectorSize() const
{
	return m_vectorSize_Exist;
}
void Mp7JrsSeriesOfVectorType::SetvectorSize(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetvectorSize().");
	}
	m_vectorSize = item;
	m_vectorSize_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidatevectorSize()
{
	m_vectorSize_Exist = false;
}
Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetRaw() const
{
		return m_Raw;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidRaw() const
{
	return m_Raw_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetMin() const
{
		return m_Min;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidMin() const
{
	return m_Min_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetMax() const
{
		return m_Max;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidMax() const
{
	return m_Max_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetMean() const
{
		return m_Mean;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidMean() const
{
	return m_Mean_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetRandom() const
{
		return m_Random;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidRandom() const
{
	return m_Random_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetFirst() const
{
		return m_First;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidFirst() const
{
	return m_First_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetLast() const
{
		return m_Last;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidLast() const
{
	return m_Last_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetVariance() const
{
		return m_Variance;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidVariance() const
{
	return m_Variance_Exist;
}

float Mp7JrsSeriesOfVectorType::GetLogBase() const
{
		return m_LogBase;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidLogBase() const
{
	return m_LogBase_Exist;
}

Mp7JrsFloatMatrixPtr Mp7JrsSeriesOfVectorType::GetCovariance() const
{
		return m_Covariance;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidCovariance() const
{
	return m_Covariance_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorType::GetVarianceSummed() const
{
		return m_VarianceSummed;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidVarianceSummed() const
{
	return m_VarianceSummed_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorType::GetMaxSqDist() const
{
		return m_MaxSqDist;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidMaxSqDist() const
{
	return m_MaxSqDist_Exist;
}

Mp7JrsfloatVectorPtr Mp7JrsSeriesOfVectorType::GetWeight() const
{
		return m_Weight;
}

// Element is optional
bool Mp7JrsSeriesOfVectorType::IsValidWeight() const
{
	return m_Weight_Exist;
}

void Mp7JrsSeriesOfVectorType::SetRaw(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetRaw().");
	}
	if (m_Raw != item || m_Raw_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Raw);
		m_Raw = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Raw) m_Raw->SetParent(m_myself.getPointer());
		if (m_Raw && m_Raw->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Raw->UseTypeAttribute = true;
		}
		m_Raw_Exist = true;
		if(m_Raw != Mp7JrsFloatMatrixPtr())
		{
			m_Raw->SetContentName(XMLString::transcode("Raw"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateRaw()
{
	m_Raw_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetMin(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetMin().");
	}
	if (m_Min != item || m_Min_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Min);
		m_Min = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Min) m_Min->SetParent(m_myself.getPointer());
		if (m_Min && m_Min->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Min->UseTypeAttribute = true;
		}
		m_Min_Exist = true;
		if(m_Min != Mp7JrsFloatMatrixPtr())
		{
			m_Min->SetContentName(XMLString::transcode("Min"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateMin()
{
	m_Min_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetMax(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetMax().");
	}
	if (m_Max != item || m_Max_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Max);
		m_Max = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Max) m_Max->SetParent(m_myself.getPointer());
		if (m_Max && m_Max->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Max->UseTypeAttribute = true;
		}
		m_Max_Exist = true;
		if(m_Max != Mp7JrsFloatMatrixPtr())
		{
			m_Max->SetContentName(XMLString::transcode("Max"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateMax()
{
	m_Max_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetMean(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetMean().");
	}
	if (m_Mean != item || m_Mean_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Mean);
		m_Mean = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Mean) m_Mean->SetParent(m_myself.getPointer());
		if (m_Mean && m_Mean->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Mean->UseTypeAttribute = true;
		}
		m_Mean_Exist = true;
		if(m_Mean != Mp7JrsFloatMatrixPtr())
		{
			m_Mean->SetContentName(XMLString::transcode("Mean"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateMean()
{
	m_Mean_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetRandom(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetRandom().");
	}
	if (m_Random != item || m_Random_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Random);
		m_Random = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Random) m_Random->SetParent(m_myself.getPointer());
		if (m_Random && m_Random->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Random->UseTypeAttribute = true;
		}
		m_Random_Exist = true;
		if(m_Random != Mp7JrsFloatMatrixPtr())
		{
			m_Random->SetContentName(XMLString::transcode("Random"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateRandom()
{
	m_Random_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetFirst(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetFirst().");
	}
	if (m_First != item || m_First_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_First);
		m_First = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_First) m_First->SetParent(m_myself.getPointer());
		if (m_First && m_First->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_First->UseTypeAttribute = true;
		}
		m_First_Exist = true;
		if(m_First != Mp7JrsFloatMatrixPtr())
		{
			m_First->SetContentName(XMLString::transcode("First"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateFirst()
{
	m_First_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetLast(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetLast().");
	}
	if (m_Last != item || m_Last_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Last);
		m_Last = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Last) m_Last->SetParent(m_myself.getPointer());
		if (m_Last && m_Last->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Last->UseTypeAttribute = true;
		}
		m_Last_Exist = true;
		if(m_Last != Mp7JrsFloatMatrixPtr())
		{
			m_Last->SetContentName(XMLString::transcode("Last"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateLast()
{
	m_Last_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetVariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetVariance().");
	}
	if (m_Variance != item || m_Variance_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Variance);
		m_Variance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Variance) m_Variance->SetParent(m_myself.getPointer());
		if (m_Variance && m_Variance->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Variance->UseTypeAttribute = true;
		}
		m_Variance_Exist = true;
		if(m_Variance != Mp7JrsFloatMatrixPtr())
		{
			m_Variance->SetContentName(XMLString::transcode("Variance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateVariance()
{
	m_Variance_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetLogBase(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetLogBase().");
	}
	if (m_LogBase != item || m_LogBase_Exist == false)
	{
		m_LogBase = item;
		m_LogBase_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateLogBase()
{
	m_LogBase_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetCovariance(const Mp7JrsFloatMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetCovariance().");
	}
	if (m_Covariance != item || m_Covariance_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Covariance);
		m_Covariance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Covariance) m_Covariance->SetParent(m_myself.getPointer());
		if (m_Covariance && m_Covariance->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Covariance->UseTypeAttribute = true;
		}
		m_Covariance_Exist = true;
		if(m_Covariance != Mp7JrsFloatMatrixPtr())
		{
			m_Covariance->SetContentName(XMLString::transcode("Covariance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateCovariance()
{
	m_Covariance_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetVarianceSummed(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetVarianceSummed().");
	}
	if (m_VarianceSummed != item || m_VarianceSummed_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VarianceSummed);
		m_VarianceSummed = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VarianceSummed) m_VarianceSummed->SetParent(m_myself.getPointer());
		m_VarianceSummed_Exist = true;
		if(m_VarianceSummed != Mp7JrsfloatVectorPtr())
		{
			m_VarianceSummed->SetContentName(XMLString::transcode("VarianceSummed"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateVarianceSummed()
{
	m_VarianceSummed_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetMaxSqDist(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetMaxSqDist().");
	}
	if (m_MaxSqDist != item || m_MaxSqDist_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MaxSqDist);
		m_MaxSqDist = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MaxSqDist) m_MaxSqDist->SetParent(m_myself.getPointer());
		m_MaxSqDist_Exist = true;
		if(m_MaxSqDist != Mp7JrsfloatVectorPtr())
		{
			m_MaxSqDist->SetContentName(XMLString::transcode("MaxSqDist"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateMaxSqDist()
{
	m_MaxSqDist_Exist = false;
}
void Mp7JrsSeriesOfVectorType::SetWeight(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetWeight().");
	}
	if (m_Weight != item || m_Weight_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Weight);
		m_Weight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Weight) m_Weight->SetParent(m_myself.getPointer());
		m_Weight_Exist = true;
		if(m_Weight != Mp7JrsfloatVectorPtr())
		{
			m_Weight->SetContentName(XMLString::transcode("Weight"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSeriesOfVectorType::InvalidateWeight()
{
	m_Weight_Exist = false;
}
unsigned Mp7JrsSeriesOfVectorType::GettotalNumOfSamples() const
{
	return GetBase()->GettotalNumOfSamples();
}

void Mp7JrsSeriesOfVectorType::SettotalNumOfSamples(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SettotalNumOfSamples().");
	}
	GetBase()->SettotalNumOfSamples(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsScalableSeriesType_Scaling_CollectionPtr Mp7JrsSeriesOfVectorType::GetScaling() const
{
	return GetBase()->GetScaling();
}

void Mp7JrsSeriesOfVectorType::SetScaling(const Mp7JrsScalableSeriesType_Scaling_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSeriesOfVectorType::SetScaling().");
	}
	GetBase()->SetScaling(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSeriesOfVectorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("vectorSize")) == 0)
	{
		// vectorSize is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Raw")) == 0)
	{
		// Raw is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRaw()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRaw(p, client);
					if((p = GetRaw()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Min")) == 0)
	{
		// Min is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMin()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMin(p, client);
					if((p = GetMin()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max")) == 0)
	{
		// Max is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMax()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMax(p, client);
					if((p = GetMax()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Mean")) == 0)
	{
		// Mean is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMean()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMean(p, client);
					if((p = GetMean()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Random")) == 0)
	{
		// Random is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRandom()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRandom(p, client);
					if((p = GetRandom()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("First")) == 0)
	{
		// First is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFirst()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFirst(p, client);
					if((p = GetFirst()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Last")) == 0)
	{
		// Last is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLast()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLast(p, client);
					if((p = GetLast()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Variance")) == 0)
	{
		// Variance is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVariance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVariance(p, client);
					if((p = GetVariance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Covariance")) == 0)
	{
		// Covariance is simple element FloatMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCovariance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FloatMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFloatMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCovariance(p, client);
					if((p = GetCovariance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VarianceSummed")) == 0)
	{
		// VarianceSummed is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVarianceSummed()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVarianceSummed(p, client);
					if((p = GetVarianceSummed()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MaxSqDist")) == 0)
	{
		// MaxSqDist is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMaxSqDist()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMaxSqDist(p, client);
					if((p = GetMaxSqDist()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Weight")) == 0)
	{
		// Weight is simple element floatVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWeight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:floatVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsfloatVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWeight(p, client);
					if((p = GetWeight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SeriesOfVectorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SeriesOfVectorType");
	}
	return result;
}

XMLCh * Mp7JrsSeriesOfVectorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSeriesOfVectorType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSeriesOfVectorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SeriesOfVectorType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_vectorSize_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_vectorSize);
		element->setAttributeNS(X(""), X("vectorSize"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Raw != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Raw->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Raw"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Raw->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Min != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Min->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Min"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Min->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Max != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Max->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Max"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Max->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Mean != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Mean->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Mean"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Mean->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Random != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Random->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Random"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Random->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_First != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_First->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("First"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_First->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Last != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Last->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Last"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Last->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Variance != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Variance->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Variance"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Variance->Serialize(doc, element, &elem);
	}
	if(m_LogBase_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_LogBase);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("LogBase"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_Covariance != Mp7JrsFloatMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Covariance->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Covariance"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Covariance->Serialize(doc, element, &elem);
	}
	if (m_VarianceSummed != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VarianceSummed->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MaxSqDist != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MaxSqDist->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Weight != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Weight->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSeriesOfVectorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("vectorSize")))
	{
		// deserialize value type
		this->SetvectorSize(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("vectorSize"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsScalableSeriesType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Raw"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Raw")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRaw(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Min"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Min")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMin(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Max"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Max")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMax(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Mean"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Mean")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMean(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Random"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Random")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRandom(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("First"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("First")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFirst(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Last"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Last")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLast(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Variance"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Variance")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVariance(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("LogBase"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetLogBase(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Covariance"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Covariance")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFloatMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCovariance(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("VarianceSummed")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVarianceSummed(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("MaxSqDist")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMaxSqDist(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Weight")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWeight(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSeriesOfVectorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Raw")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetRaw(child);
  }
  if (XMLString::compareString(elementname, X("Min")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetMin(child);
  }
  if (XMLString::compareString(elementname, X("Max")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetMax(child);
  }
  if (XMLString::compareString(elementname, X("Mean")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetMean(child);
  }
  if (XMLString::compareString(elementname, X("Random")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetRandom(child);
  }
  if (XMLString::compareString(elementname, X("First")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetFirst(child);
  }
  if (XMLString::compareString(elementname, X("Last")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetLast(child);
  }
  if (XMLString::compareString(elementname, X("Variance")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetVariance(child);
  }
  if (XMLString::compareString(elementname, X("Covariance")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFloatMatrixType; // FTT, check this
	}
	this->SetCovariance(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VarianceSummed")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetVarianceSummed(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MaxSqDist")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetMaxSqDist(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Weight")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetWeight(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSeriesOfVectorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRaw() != Dc1NodePtr())
		result.Insert(GetRaw());
	if (GetMin() != Dc1NodePtr())
		result.Insert(GetMin());
	if (GetMax() != Dc1NodePtr())
		result.Insert(GetMax());
	if (GetMean() != Dc1NodePtr())
		result.Insert(GetMean());
	if (GetRandom() != Dc1NodePtr())
		result.Insert(GetRandom());
	if (GetFirst() != Dc1NodePtr())
		result.Insert(GetFirst());
	if (GetLast() != Dc1NodePtr())
		result.Insert(GetLast());
	if (GetVariance() != Dc1NodePtr())
		result.Insert(GetVariance());
	if (GetCovariance() != Dc1NodePtr())
		result.Insert(GetCovariance());
	if (GetVarianceSummed() != Dc1NodePtr())
		result.Insert(GetVarianceSummed());
	if (GetMaxSqDist() != Dc1NodePtr())
		result.Insert(GetMaxSqDist());
	if (GetWeight() != Dc1NodePtr())
		result.Insert(GetWeight());
	if (GetScaling() != Dc1NodePtr())
		result.Insert(GetScaling());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSeriesOfVectorType_ExtMethodImpl.h


