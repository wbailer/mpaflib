
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsClassificationPreferencesType_Country_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_DatePeriod_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_Language_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_Form_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_Genre_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_Subject_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_Review_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType_AssociatedData_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsClassificationPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsClassificationPreferencesType_Country_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Country
#include "Mp7JrsClassificationPreferencesType_DatePeriod_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:DatePeriod
#include "Mp7JrsClassificationPreferencesType_LanguageFormat_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:LanguageFormat
#include "Mp7JrsClassificationPreferencesType_Language_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Language
#include "Mp7JrsClassificationPreferencesType_CaptionLanguage_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CaptionLanguage
#include "Mp7JrsClassificationPreferencesType_Form_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Form
#include "Mp7JrsClassificationPreferencesType_Genre_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Genre
#include "Mp7JrsClassificationPreferencesType_Subject_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Subject
#include "Mp7JrsClassificationPreferencesType_Review_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Review
#include "Mp7JrsClassificationPreferencesType_ParentalGuidance_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ParentalGuidance
#include "Mp7JrsClassificationPreferencesType_AssociatedData_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AssociatedData

#include <assert.h>
IMp7JrsClassificationPreferencesType::IMp7JrsClassificationPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtPropInit.h

}

IMp7JrsClassificationPreferencesType::~IMp7JrsClassificationPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtPropCleanup.h

}

Mp7JrsClassificationPreferencesType::Mp7JrsClassificationPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationPreferencesType::~Mp7JrsClassificationPreferencesType()
{
	Cleanup();
}

void Mp7JrsClassificationPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Country = Mp7JrsClassificationPreferencesType_Country_CollectionPtr(); // Collection
	m_DatePeriod = Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr(); // Collection
	m_LanguageFormat = Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr(); // Collection
	m_Language = Mp7JrsClassificationPreferencesType_Language_CollectionPtr(); // Collection
	m_CaptionLanguage = Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr(); // Collection
	m_Form = Mp7JrsClassificationPreferencesType_Form_CollectionPtr(); // Collection
	m_Genre = Mp7JrsClassificationPreferencesType_Genre_CollectionPtr(); // Collection
	m_Subject = Mp7JrsClassificationPreferencesType_Subject_CollectionPtr(); // Collection
	m_Review = Mp7JrsClassificationPreferencesType_Review_CollectionPtr(); // Collection
	m_ParentalGuidance = Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr(); // Collection
	m_AssociatedData = Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtMyPropInit.h

}

void Mp7JrsClassificationPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_Country);
	// Dc1Factory::DeleteObject(m_DatePeriod);
	// Dc1Factory::DeleteObject(m_LanguageFormat);
	// Dc1Factory::DeleteObject(m_Language);
	// Dc1Factory::DeleteObject(m_CaptionLanguage);
	// Dc1Factory::DeleteObject(m_Form);
	// Dc1Factory::DeleteObject(m_Genre);
	// Dc1Factory::DeleteObject(m_Subject);
	// Dc1Factory::DeleteObject(m_Review);
	// Dc1Factory::DeleteObject(m_ParentalGuidance);
	// Dc1Factory::DeleteObject(m_AssociatedData);
}

void Mp7JrsClassificationPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationPreferencesType
	const Dc1Ptr< Mp7JrsClassificationPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_Country);
		this->SetCountry(Dc1Factory::CloneObject(tmp->GetCountry()));
		// Dc1Factory::DeleteObject(m_DatePeriod);
		this->SetDatePeriod(Dc1Factory::CloneObject(tmp->GetDatePeriod()));
		// Dc1Factory::DeleteObject(m_LanguageFormat);
		this->SetLanguageFormat(Dc1Factory::CloneObject(tmp->GetLanguageFormat()));
		// Dc1Factory::DeleteObject(m_Language);
		this->SetLanguage(Dc1Factory::CloneObject(tmp->GetLanguage()));
		// Dc1Factory::DeleteObject(m_CaptionLanguage);
		this->SetCaptionLanguage(Dc1Factory::CloneObject(tmp->GetCaptionLanguage()));
		// Dc1Factory::DeleteObject(m_Form);
		this->SetForm(Dc1Factory::CloneObject(tmp->GetForm()));
		// Dc1Factory::DeleteObject(m_Genre);
		this->SetGenre(Dc1Factory::CloneObject(tmp->GetGenre()));
		// Dc1Factory::DeleteObject(m_Subject);
		this->SetSubject(Dc1Factory::CloneObject(tmp->GetSubject()));
		// Dc1Factory::DeleteObject(m_Review);
		this->SetReview(Dc1Factory::CloneObject(tmp->GetReview()));
		// Dc1Factory::DeleteObject(m_ParentalGuidance);
		this->SetParentalGuidance(Dc1Factory::CloneObject(tmp->GetParentalGuidance()));
		// Dc1Factory::DeleteObject(m_AssociatedData);
		this->SetAssociatedData(Dc1Factory::CloneObject(tmp->GetAssociatedData()));
}

Dc1NodePtr Mp7JrsClassificationPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsClassificationPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrspreferenceValuePtr Mp7JrsClassificationPreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsClassificationPreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsClassificationPreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsClassificationPreferencesType_Country_CollectionPtr Mp7JrsClassificationPreferencesType::GetCountry() const
{
		return m_Country;
}

Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr Mp7JrsClassificationPreferencesType::GetDatePeriod() const
{
		return m_DatePeriod;
}

Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr Mp7JrsClassificationPreferencesType::GetLanguageFormat() const
{
		return m_LanguageFormat;
}

Mp7JrsClassificationPreferencesType_Language_CollectionPtr Mp7JrsClassificationPreferencesType::GetLanguage() const
{
		return m_Language;
}

Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr Mp7JrsClassificationPreferencesType::GetCaptionLanguage() const
{
		return m_CaptionLanguage;
}

Mp7JrsClassificationPreferencesType_Form_CollectionPtr Mp7JrsClassificationPreferencesType::GetForm() const
{
		return m_Form;
}

Mp7JrsClassificationPreferencesType_Genre_CollectionPtr Mp7JrsClassificationPreferencesType::GetGenre() const
{
		return m_Genre;
}

Mp7JrsClassificationPreferencesType_Subject_CollectionPtr Mp7JrsClassificationPreferencesType::GetSubject() const
{
		return m_Subject;
}

Mp7JrsClassificationPreferencesType_Review_CollectionPtr Mp7JrsClassificationPreferencesType::GetReview() const
{
		return m_Review;
}

Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr Mp7JrsClassificationPreferencesType::GetParentalGuidance() const
{
		return m_ParentalGuidance;
}

Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr Mp7JrsClassificationPreferencesType::GetAssociatedData() const
{
		return m_AssociatedData;
}

void Mp7JrsClassificationPreferencesType::SetCountry(const Mp7JrsClassificationPreferencesType_Country_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetCountry().");
	}
	if (m_Country != item)
	{
		// Dc1Factory::DeleteObject(m_Country);
		m_Country = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Country) m_Country->SetParent(m_myself.getPointer());
		if(m_Country != Mp7JrsClassificationPreferencesType_Country_CollectionPtr())
		{
			m_Country->SetContentName(XMLString::transcode("Country"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetDatePeriod(const Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetDatePeriod().");
	}
	if (m_DatePeriod != item)
	{
		// Dc1Factory::DeleteObject(m_DatePeriod);
		m_DatePeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DatePeriod) m_DatePeriod->SetParent(m_myself.getPointer());
		if(m_DatePeriod != Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr())
		{
			m_DatePeriod->SetContentName(XMLString::transcode("DatePeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetLanguageFormat(const Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetLanguageFormat().");
	}
	if (m_LanguageFormat != item)
	{
		// Dc1Factory::DeleteObject(m_LanguageFormat);
		m_LanguageFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LanguageFormat) m_LanguageFormat->SetParent(m_myself.getPointer());
		if(m_LanguageFormat != Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr())
		{
			m_LanguageFormat->SetContentName(XMLString::transcode("LanguageFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetLanguage(const Mp7JrsClassificationPreferencesType_Language_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetLanguage().");
	}
	if (m_Language != item)
	{
		// Dc1Factory::DeleteObject(m_Language);
		m_Language = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Language) m_Language->SetParent(m_myself.getPointer());
		if(m_Language != Mp7JrsClassificationPreferencesType_Language_CollectionPtr())
		{
			m_Language->SetContentName(XMLString::transcode("Language"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetCaptionLanguage(const Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetCaptionLanguage().");
	}
	if (m_CaptionLanguage != item)
	{
		// Dc1Factory::DeleteObject(m_CaptionLanguage);
		m_CaptionLanguage = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CaptionLanguage) m_CaptionLanguage->SetParent(m_myself.getPointer());
		if(m_CaptionLanguage != Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr())
		{
			m_CaptionLanguage->SetContentName(XMLString::transcode("CaptionLanguage"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetForm(const Mp7JrsClassificationPreferencesType_Form_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetForm().");
	}
	if (m_Form != item)
	{
		// Dc1Factory::DeleteObject(m_Form);
		m_Form = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Form) m_Form->SetParent(m_myself.getPointer());
		if(m_Form != Mp7JrsClassificationPreferencesType_Form_CollectionPtr())
		{
			m_Form->SetContentName(XMLString::transcode("Form"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetGenre(const Mp7JrsClassificationPreferencesType_Genre_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetGenre().");
	}
	if (m_Genre != item)
	{
		// Dc1Factory::DeleteObject(m_Genre);
		m_Genre = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Genre) m_Genre->SetParent(m_myself.getPointer());
		if(m_Genre != Mp7JrsClassificationPreferencesType_Genre_CollectionPtr())
		{
			m_Genre->SetContentName(XMLString::transcode("Genre"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetSubject(const Mp7JrsClassificationPreferencesType_Subject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetSubject().");
	}
	if (m_Subject != item)
	{
		// Dc1Factory::DeleteObject(m_Subject);
		m_Subject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Subject) m_Subject->SetParent(m_myself.getPointer());
		if(m_Subject != Mp7JrsClassificationPreferencesType_Subject_CollectionPtr())
		{
			m_Subject->SetContentName(XMLString::transcode("Subject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetReview(const Mp7JrsClassificationPreferencesType_Review_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetReview().");
	}
	if (m_Review != item)
	{
		// Dc1Factory::DeleteObject(m_Review);
		m_Review = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Review) m_Review->SetParent(m_myself.getPointer());
		if(m_Review != Mp7JrsClassificationPreferencesType_Review_CollectionPtr())
		{
			m_Review->SetContentName(XMLString::transcode("Review"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetParentalGuidance(const Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetParentalGuidance().");
	}
	if (m_ParentalGuidance != item)
	{
		// Dc1Factory::DeleteObject(m_ParentalGuidance);
		m_ParentalGuidance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ParentalGuidance) m_ParentalGuidance->SetParent(m_myself.getPointer());
		if(m_ParentalGuidance != Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr())
		{
			m_ParentalGuidance->SetContentName(XMLString::transcode("ParentalGuidance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::SetAssociatedData(const Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetAssociatedData().");
	}
	if (m_AssociatedData != item)
	{
		// Dc1Factory::DeleteObject(m_AssociatedData);
		m_AssociatedData = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AssociatedData) m_AssociatedData->SetParent(m_myself.getPointer());
		if(m_AssociatedData != Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr())
		{
			m_AssociatedData->SetContentName(XMLString::transcode("AssociatedData"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsClassificationPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsClassificationPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsClassificationPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsClassificationPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsClassificationPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsClassificationPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsClassificationPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsClassificationPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsClassificationPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsClassificationPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsClassificationPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsClassificationPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsClassificationPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsClassificationPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsClassificationPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsClassificationPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Country")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Country is item of type ClassificationPreferencesType_Country_LocalType
		// in element collection ClassificationPreferencesType_Country_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Country_CollectionPtr coll = GetCountry();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Country_CollectionType; // FTT, check this
				SetCountry(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Country_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Country_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DatePeriod")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:DatePeriod is item of type ClassificationPreferencesType_DatePeriod_LocalType
		// in element collection ClassificationPreferencesType_DatePeriod_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr coll = GetDatePeriod();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_DatePeriod_CollectionType; // FTT, check this
				SetDatePeriod(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_DatePeriod_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_DatePeriod_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LanguageFormat")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:LanguageFormat is item of type ClassificationPreferencesType_LanguageFormat_LocalType
		// in element collection ClassificationPreferencesType_LanguageFormat_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr coll = GetLanguageFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_LanguageFormat_CollectionType; // FTT, check this
				SetLanguageFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_LanguageFormat_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_LanguageFormat_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Language")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Language is item of type ClassificationPreferencesType_Language_LocalType
		// in element collection ClassificationPreferencesType_Language_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Language_CollectionPtr coll = GetLanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Language_CollectionType; // FTT, check this
				SetLanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Language_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Language_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CaptionLanguage")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CaptionLanguage is item of type ClassificationPreferencesType_CaptionLanguage_LocalType
		// in element collection ClassificationPreferencesType_CaptionLanguage_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr coll = GetCaptionLanguage();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_CaptionLanguage_CollectionType; // FTT, check this
				SetCaptionLanguage(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_CaptionLanguage_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_CaptionLanguage_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Form")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Form is item of type ClassificationPreferencesType_Form_LocalType
		// in element collection ClassificationPreferencesType_Form_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Form_CollectionPtr coll = GetForm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Form_CollectionType; // FTT, check this
				SetForm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Form_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Form_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Genre")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Genre is item of type ClassificationPreferencesType_Genre_LocalType
		// in element collection ClassificationPreferencesType_Genre_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Genre_CollectionPtr coll = GetGenre();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Genre_CollectionType; // FTT, check this
				SetGenre(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Genre_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Genre_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Subject")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Subject is item of type ClassificationPreferencesType_Subject_LocalType
		// in element collection ClassificationPreferencesType_Subject_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Subject_CollectionPtr coll = GetSubject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Subject_CollectionType; // FTT, check this
				SetSubject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Subject_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Subject_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Review")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Review is item of type ClassificationPreferencesType_Review_LocalType
		// in element collection ClassificationPreferencesType_Review_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_Review_CollectionPtr coll = GetReview();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_Review_CollectionType; // FTT, check this
				SetReview(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_Review_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_Review_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ParentalGuidance")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ParentalGuidance is item of type ClassificationPreferencesType_ParentalGuidance_LocalType
		// in element collection ClassificationPreferencesType_ParentalGuidance_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr coll = GetParentalGuidance();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_ParentalGuidance_CollectionType; // FTT, check this
				SetParentalGuidance(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_ParentalGuidance_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_ParentalGuidance_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AssociatedData")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AssociatedData is item of type ClassificationPreferencesType_AssociatedData_LocalType
		// in element collection ClassificationPreferencesType_AssociatedData_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr coll = GetAssociatedData();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationPreferencesType_AssociatedData_CollectionType; // FTT, check this
				SetAssociatedData(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType_AssociatedData_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesType_AssociatedData_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsClassificationPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsClassificationPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Country != Mp7JrsClassificationPreferencesType_Country_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Country->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_DatePeriod != Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DatePeriod->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_LanguageFormat != Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_LanguageFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Language != Mp7JrsClassificationPreferencesType_Language_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Language->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_CaptionLanguage != Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CaptionLanguage->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Form != Mp7JrsClassificationPreferencesType_Form_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Form->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Genre != Mp7JrsClassificationPreferencesType_Genre_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Genre->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Subject != Mp7JrsClassificationPreferencesType_Subject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Subject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Review != Mp7JrsClassificationPreferencesType_Review_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Review->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ParentalGuidance != Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ParentalGuidance->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_AssociatedData != Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AssociatedData->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsClassificationPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Country"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Country")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Country_CollectionPtr tmp = CreateClassificationPreferencesType_Country_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCountry(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("DatePeriod"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("DatePeriod")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr tmp = CreateClassificationPreferencesType_DatePeriod_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDatePeriod(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("LanguageFormat"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("LanguageFormat")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr tmp = CreateClassificationPreferencesType_LanguageFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLanguageFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Language"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Language")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Language_CollectionPtr tmp = CreateClassificationPreferencesType_Language_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetLanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CaptionLanguage"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CaptionLanguage")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr tmp = CreateClassificationPreferencesType_CaptionLanguage_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCaptionLanguage(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Form"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Form")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Form_CollectionPtr tmp = CreateClassificationPreferencesType_Form_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetForm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Genre"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Genre")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Genre_CollectionPtr tmp = CreateClassificationPreferencesType_Genre_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetGenre(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Subject"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Subject")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Subject_CollectionPtr tmp = CreateClassificationPreferencesType_Subject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSubject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Review"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Review")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_Review_CollectionPtr tmp = CreateClassificationPreferencesType_Review_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetReview(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ParentalGuidance"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ParentalGuidance")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr tmp = CreateClassificationPreferencesType_ParentalGuidance_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetParentalGuidance(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AssociatedData"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AssociatedData")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr tmp = CreateClassificationPreferencesType_AssociatedData_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAssociatedData(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Country")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Country_CollectionPtr tmp = CreateClassificationPreferencesType_Country_CollectionType; // FTT, check this
	this->SetCountry(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("DatePeriod")) == 0))
  {
	Mp7JrsClassificationPreferencesType_DatePeriod_CollectionPtr tmp = CreateClassificationPreferencesType_DatePeriod_CollectionType; // FTT, check this
	this->SetDatePeriod(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("LanguageFormat")) == 0))
  {
	Mp7JrsClassificationPreferencesType_LanguageFormat_CollectionPtr tmp = CreateClassificationPreferencesType_LanguageFormat_CollectionType; // FTT, check this
	this->SetLanguageFormat(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Language")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Language_CollectionPtr tmp = CreateClassificationPreferencesType_Language_CollectionType; // FTT, check this
	this->SetLanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CaptionLanguage")) == 0))
  {
	Mp7JrsClassificationPreferencesType_CaptionLanguage_CollectionPtr tmp = CreateClassificationPreferencesType_CaptionLanguage_CollectionType; // FTT, check this
	this->SetCaptionLanguage(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Form")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Form_CollectionPtr tmp = CreateClassificationPreferencesType_Form_CollectionType; // FTT, check this
	this->SetForm(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Genre")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Genre_CollectionPtr tmp = CreateClassificationPreferencesType_Genre_CollectionType; // FTT, check this
	this->SetGenre(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Subject")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Subject_CollectionPtr tmp = CreateClassificationPreferencesType_Subject_CollectionType; // FTT, check this
	this->SetSubject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Review")) == 0))
  {
	Mp7JrsClassificationPreferencesType_Review_CollectionPtr tmp = CreateClassificationPreferencesType_Review_CollectionType; // FTT, check this
	this->SetReview(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ParentalGuidance")) == 0))
  {
	Mp7JrsClassificationPreferencesType_ParentalGuidance_CollectionPtr tmp = CreateClassificationPreferencesType_ParentalGuidance_CollectionType; // FTT, check this
	this->SetParentalGuidance(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AssociatedData")) == 0))
  {
	Mp7JrsClassificationPreferencesType_AssociatedData_CollectionPtr tmp = CreateClassificationPreferencesType_AssociatedData_CollectionType; // FTT, check this
	this->SetAssociatedData(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCountry() != Dc1NodePtr())
		result.Insert(GetCountry());
	if (GetDatePeriod() != Dc1NodePtr())
		result.Insert(GetDatePeriod());
	if (GetLanguageFormat() != Dc1NodePtr())
		result.Insert(GetLanguageFormat());
	if (GetLanguage() != Dc1NodePtr())
		result.Insert(GetLanguage());
	if (GetCaptionLanguage() != Dc1NodePtr())
		result.Insert(GetCaptionLanguage());
	if (GetForm() != Dc1NodePtr())
		result.Insert(GetForm());
	if (GetGenre() != Dc1NodePtr())
		result.Insert(GetGenre());
	if (GetSubject() != Dc1NodePtr())
		result.Insert(GetSubject());
	if (GetReview() != Dc1NodePtr())
		result.Insert(GetReview());
	if (GetParentalGuidance() != Dc1NodePtr())
		result.Insert(GetParentalGuidance());
	if (GetAssociatedData() != Dc1NodePtr())
		result.Insert(GetAssociatedData());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationPreferencesType_ExtMethodImpl.h


