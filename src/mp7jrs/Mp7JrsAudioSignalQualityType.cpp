
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtImplInclude.h


#include "Mp7JrsAudioDSType.h"
#include "Mp7JrsPersonType.h"
#include "Mp7JrsCreationToolType.h"
#include "Mp7JrsBackgroundNoiseLevelType.h"
#include "Mp7JrsRelativeDelayType.h"
#include "Mp7JrsBalanceType.h"
#include "Mp7JrsDcOffsetType.h"
#include "Mp7JrsCrossChannelCorrelationType.h"
#include "Mp7JrsBandwidthType.h"
#include "Mp7JrsTransmissionTechnologyType.h"
#include "Mp7JrsAudioSignalQualityType_ErrorEventList_LocalType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAudioSignalQualityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsAudioSignalQualityType::IMp7JrsAudioSignalQualityType()
{

// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtPropInit.h

}

IMp7JrsAudioSignalQualityType::~IMp7JrsAudioSignalQualityType()
{
// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtPropCleanup.h

}

Mp7JrsAudioSignalQualityType::Mp7JrsAudioSignalQualityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioSignalQualityType::~Mp7JrsAudioSignalQualityType()
{
	Cleanup();
}

void Mp7JrsAudioSignalQualityType::Init()
{
	// Init base
	m_Base = CreateAudioDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_IsOriginalMono = false; // Value
	m_IsOriginalMono_Exist = false;
	m_BroadcastReady = false; // Value
	m_BroadcastReady_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Operator = Mp7JrsPersonPtr(); // Class
	m_Operator_Exist = false;
	m_UsedTool = Mp7JrsCreationToolPtr(); // Class
	m_UsedTool_Exist = false;
	m_BackgroundNoiseLevel = Mp7JrsBackgroundNoiseLevelPtr(); // Class
	m_RelativeDelay = Mp7JrsRelativeDelayPtr(); // Class
	m_Balance = Mp7JrsBalancePtr(); // Class
	m_DcOffset = Mp7JrsDcOffsetPtr(); // Class
	m_CrossChannelCorrelation = Mp7JrsCrossChannelCorrelationPtr(); // Class
	m_Bandwidth = Mp7JrsBandwidthPtr(); // Class
	m_TransmissionTechnology = Mp7JrsTransmissionTechnologyPtr(); // Class
	m_TransmissionTechnology_Exist = false;
	m_ErrorEventList = Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr(); // Class
	m_ErrorEventList_Exist = false;


// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtMyPropInit.h

}

void Mp7JrsAudioSignalQualityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Operator);
	// Dc1Factory::DeleteObject(m_UsedTool);
	// Dc1Factory::DeleteObject(m_BackgroundNoiseLevel);
	// Dc1Factory::DeleteObject(m_RelativeDelay);
	// Dc1Factory::DeleteObject(m_Balance);
	// Dc1Factory::DeleteObject(m_DcOffset);
	// Dc1Factory::DeleteObject(m_CrossChannelCorrelation);
	// Dc1Factory::DeleteObject(m_Bandwidth);
	// Dc1Factory::DeleteObject(m_TransmissionTechnology);
	// Dc1Factory::DeleteObject(m_ErrorEventList);
}

void Mp7JrsAudioSignalQualityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioSignalQualityTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioSignalQualityType
	const Dc1Ptr< Mp7JrsAudioSignalQualityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioSignalQualityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistIsOriginalMono())
	{
		this->SetIsOriginalMono(tmp->GetIsOriginalMono());
	}
	else
	{
		InvalidateIsOriginalMono();
	}
	}
	{
	if (tmp->ExistBroadcastReady())
	{
		this->SetBroadcastReady(tmp->GetBroadcastReady());
	}
	else
	{
		InvalidateBroadcastReady();
	}
	}
	if (tmp->IsValidOperator())
	{
		// Dc1Factory::DeleteObject(m_Operator);
		this->SetOperator(Dc1Factory::CloneObject(tmp->GetOperator()));
	}
	else
	{
		InvalidateOperator();
	}
	if (tmp->IsValidUsedTool())
	{
		// Dc1Factory::DeleteObject(m_UsedTool);
		this->SetUsedTool(Dc1Factory::CloneObject(tmp->GetUsedTool()));
	}
	else
	{
		InvalidateUsedTool();
	}
		// Dc1Factory::DeleteObject(m_BackgroundNoiseLevel);
		this->SetBackgroundNoiseLevel(Dc1Factory::CloneObject(tmp->GetBackgroundNoiseLevel()));
		// Dc1Factory::DeleteObject(m_RelativeDelay);
		this->SetRelativeDelay(Dc1Factory::CloneObject(tmp->GetRelativeDelay()));
		// Dc1Factory::DeleteObject(m_Balance);
		this->SetBalance(Dc1Factory::CloneObject(tmp->GetBalance()));
		// Dc1Factory::DeleteObject(m_DcOffset);
		this->SetDcOffset(Dc1Factory::CloneObject(tmp->GetDcOffset()));
		// Dc1Factory::DeleteObject(m_CrossChannelCorrelation);
		this->SetCrossChannelCorrelation(Dc1Factory::CloneObject(tmp->GetCrossChannelCorrelation()));
		// Dc1Factory::DeleteObject(m_Bandwidth);
		this->SetBandwidth(Dc1Factory::CloneObject(tmp->GetBandwidth()));
	if (tmp->IsValidTransmissionTechnology())
	{
		// Dc1Factory::DeleteObject(m_TransmissionTechnology);
		this->SetTransmissionTechnology(Dc1Factory::CloneObject(tmp->GetTransmissionTechnology()));
	}
	else
	{
		InvalidateTransmissionTechnology();
	}
	if (tmp->IsValidErrorEventList())
	{
		// Dc1Factory::DeleteObject(m_ErrorEventList);
		this->SetErrorEventList(Dc1Factory::CloneObject(tmp->GetErrorEventList()));
	}
	else
	{
		InvalidateErrorEventList();
	}
}

Dc1NodePtr Mp7JrsAudioSignalQualityType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioSignalQualityType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDSType > Mp7JrsAudioSignalQualityType::GetBase() const
{
	return m_Base;
}

bool Mp7JrsAudioSignalQualityType::GetIsOriginalMono() const
{
	return m_IsOriginalMono;
}

bool Mp7JrsAudioSignalQualityType::ExistIsOriginalMono() const
{
	return m_IsOriginalMono_Exist;
}
void Mp7JrsAudioSignalQualityType::SetIsOriginalMono(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetIsOriginalMono().");
	}
	m_IsOriginalMono = item;
	m_IsOriginalMono_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateIsOriginalMono()
{
	m_IsOriginalMono_Exist = false;
}
bool Mp7JrsAudioSignalQualityType::GetBroadcastReady() const
{
	return m_BroadcastReady;
}

bool Mp7JrsAudioSignalQualityType::ExistBroadcastReady() const
{
	return m_BroadcastReady_Exist;
}
void Mp7JrsAudioSignalQualityType::SetBroadcastReady(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetBroadcastReady().");
	}
	m_BroadcastReady = item;
	m_BroadcastReady_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateBroadcastReady()
{
	m_BroadcastReady_Exist = false;
}
Mp7JrsPersonPtr Mp7JrsAudioSignalQualityType::GetOperator() const
{
		return m_Operator;
}

// Element is optional
bool Mp7JrsAudioSignalQualityType::IsValidOperator() const
{
	return m_Operator_Exist;
}

Mp7JrsCreationToolPtr Mp7JrsAudioSignalQualityType::GetUsedTool() const
{
		return m_UsedTool;
}

// Element is optional
bool Mp7JrsAudioSignalQualityType::IsValidUsedTool() const
{
	return m_UsedTool_Exist;
}

Mp7JrsBackgroundNoiseLevelPtr Mp7JrsAudioSignalQualityType::GetBackgroundNoiseLevel() const
{
		return m_BackgroundNoiseLevel;
}

Mp7JrsRelativeDelayPtr Mp7JrsAudioSignalQualityType::GetRelativeDelay() const
{
		return m_RelativeDelay;
}

Mp7JrsBalancePtr Mp7JrsAudioSignalQualityType::GetBalance() const
{
		return m_Balance;
}

Mp7JrsDcOffsetPtr Mp7JrsAudioSignalQualityType::GetDcOffset() const
{
		return m_DcOffset;
}

Mp7JrsCrossChannelCorrelationPtr Mp7JrsAudioSignalQualityType::GetCrossChannelCorrelation() const
{
		return m_CrossChannelCorrelation;
}

Mp7JrsBandwidthPtr Mp7JrsAudioSignalQualityType::GetBandwidth() const
{
		return m_Bandwidth;
}

Mp7JrsTransmissionTechnologyPtr Mp7JrsAudioSignalQualityType::GetTransmissionTechnology() const
{
		return m_TransmissionTechnology;
}

// Element is optional
bool Mp7JrsAudioSignalQualityType::IsValidTransmissionTechnology() const
{
	return m_TransmissionTechnology_Exist;
}

Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr Mp7JrsAudioSignalQualityType::GetErrorEventList() const
{
		return m_ErrorEventList;
}

// Element is optional
bool Mp7JrsAudioSignalQualityType::IsValidErrorEventList() const
{
	return m_ErrorEventList_Exist;
}

void Mp7JrsAudioSignalQualityType::SetOperator(const Mp7JrsPersonPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetOperator().");
	}
	if (m_Operator != item || m_Operator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Operator);
		m_Operator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Operator) m_Operator->SetParent(m_myself.getPointer());
		if (m_Operator && m_Operator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Operator->UseTypeAttribute = true;
		}
		m_Operator_Exist = true;
		if(m_Operator != Mp7JrsPersonPtr())
		{
			m_Operator->SetContentName(XMLString::transcode("Operator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateOperator()
{
	m_Operator_Exist = false;
}
void Mp7JrsAudioSignalQualityType::SetUsedTool(const Mp7JrsCreationToolPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetUsedTool().");
	}
	if (m_UsedTool != item || m_UsedTool_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_UsedTool);
		m_UsedTool = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsedTool) m_UsedTool->SetParent(m_myself.getPointer());
		if (m_UsedTool && m_UsedTool->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_UsedTool->UseTypeAttribute = true;
		}
		m_UsedTool_Exist = true;
		if(m_UsedTool != Mp7JrsCreationToolPtr())
		{
			m_UsedTool->SetContentName(XMLString::transcode("UsedTool"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateUsedTool()
{
	m_UsedTool_Exist = false;
}
void Mp7JrsAudioSignalQualityType::SetBackgroundNoiseLevel(const Mp7JrsBackgroundNoiseLevelPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetBackgroundNoiseLevel().");
	}
	if (m_BackgroundNoiseLevel != item)
	{
		// Dc1Factory::DeleteObject(m_BackgroundNoiseLevel);
		m_BackgroundNoiseLevel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BackgroundNoiseLevel) m_BackgroundNoiseLevel->SetParent(m_myself.getPointer());
		if (m_BackgroundNoiseLevel && m_BackgroundNoiseLevel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BackgroundNoiseLevelType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BackgroundNoiseLevel->UseTypeAttribute = true;
		}
		if(m_BackgroundNoiseLevel != Mp7JrsBackgroundNoiseLevelPtr())
		{
			m_BackgroundNoiseLevel->SetContentName(XMLString::transcode("BackgroundNoiseLevel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetRelativeDelay(const Mp7JrsRelativeDelayPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetRelativeDelay().");
	}
	if (m_RelativeDelay != item)
	{
		// Dc1Factory::DeleteObject(m_RelativeDelay);
		m_RelativeDelay = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RelativeDelay) m_RelativeDelay->SetParent(m_myself.getPointer());
		if (m_RelativeDelay && m_RelativeDelay->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelativeDelayType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RelativeDelay->UseTypeAttribute = true;
		}
		if(m_RelativeDelay != Mp7JrsRelativeDelayPtr())
		{
			m_RelativeDelay->SetContentName(XMLString::transcode("RelativeDelay"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetBalance(const Mp7JrsBalancePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetBalance().");
	}
	if (m_Balance != item)
	{
		// Dc1Factory::DeleteObject(m_Balance);
		m_Balance = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Balance) m_Balance->SetParent(m_myself.getPointer());
		if (m_Balance && m_Balance->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BalanceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Balance->UseTypeAttribute = true;
		}
		if(m_Balance != Mp7JrsBalancePtr())
		{
			m_Balance->SetContentName(XMLString::transcode("Balance"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetDcOffset(const Mp7JrsDcOffsetPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetDcOffset().");
	}
	if (m_DcOffset != item)
	{
		// Dc1Factory::DeleteObject(m_DcOffset);
		m_DcOffset = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DcOffset) m_DcOffset->SetParent(m_myself.getPointer());
		if (m_DcOffset && m_DcOffset->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DcOffsetType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DcOffset->UseTypeAttribute = true;
		}
		if(m_DcOffset != Mp7JrsDcOffsetPtr())
		{
			m_DcOffset->SetContentName(XMLString::transcode("DcOffset"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetCrossChannelCorrelation(const Mp7JrsCrossChannelCorrelationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetCrossChannelCorrelation().");
	}
	if (m_CrossChannelCorrelation != item)
	{
		// Dc1Factory::DeleteObject(m_CrossChannelCorrelation);
		m_CrossChannelCorrelation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CrossChannelCorrelation) m_CrossChannelCorrelation->SetParent(m_myself.getPointer());
		if (m_CrossChannelCorrelation && m_CrossChannelCorrelation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CrossChannelCorrelationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CrossChannelCorrelation->UseTypeAttribute = true;
		}
		if(m_CrossChannelCorrelation != Mp7JrsCrossChannelCorrelationPtr())
		{
			m_CrossChannelCorrelation->SetContentName(XMLString::transcode("CrossChannelCorrelation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetBandwidth(const Mp7JrsBandwidthPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetBandwidth().");
	}
	if (m_Bandwidth != item)
	{
		// Dc1Factory::DeleteObject(m_Bandwidth);
		m_Bandwidth = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Bandwidth) m_Bandwidth->SetParent(m_myself.getPointer());
		if (m_Bandwidth && m_Bandwidth->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BandwidthType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Bandwidth->UseTypeAttribute = true;
		}
		if(m_Bandwidth != Mp7JrsBandwidthPtr())
		{
			m_Bandwidth->SetContentName(XMLString::transcode("Bandwidth"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::SetTransmissionTechnology(const Mp7JrsTransmissionTechnologyPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetTransmissionTechnology().");
	}
	if (m_TransmissionTechnology != item || m_TransmissionTechnology_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TransmissionTechnology);
		m_TransmissionTechnology = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TransmissionTechnology) m_TransmissionTechnology->SetParent(m_myself.getPointer());
		if (m_TransmissionTechnology && m_TransmissionTechnology->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TransmissionTechnologyType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TransmissionTechnology->UseTypeAttribute = true;
		}
		m_TransmissionTechnology_Exist = true;
		if(m_TransmissionTechnology != Mp7JrsTransmissionTechnologyPtr())
		{
			m_TransmissionTechnology->SetContentName(XMLString::transcode("TransmissionTechnology"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateTransmissionTechnology()
{
	m_TransmissionTechnology_Exist = false;
}
void Mp7JrsAudioSignalQualityType::SetErrorEventList(const Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetErrorEventList().");
	}
	if (m_ErrorEventList != item || m_ErrorEventList_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ErrorEventList);
		m_ErrorEventList = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ErrorEventList) m_ErrorEventList->SetParent(m_myself.getPointer());
		if (m_ErrorEventList && m_ErrorEventList->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignalQualityType_ErrorEventList_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ErrorEventList->UseTypeAttribute = true;
		}
		m_ErrorEventList_Exist = true;
		if(m_ErrorEventList != Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr())
		{
			m_ErrorEventList->SetContentName(XMLString::transcode("ErrorEventList"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidateErrorEventList()
{
	m_ErrorEventList_Exist = false;
}
Mp7JrsintegerVectorPtr Mp7JrsAudioSignalQualityType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsAudioSignalQualityType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsAudioSignalQualityType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}
XMLCh * Mp7JrsAudioSignalQualityType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsAudioSignalQualityType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsAudioSignalQualityType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAudioSignalQualityType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsAudioSignalQualityType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsAudioSignalQualityType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAudioSignalQualityType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsAudioSignalQualityType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsAudioSignalQualityType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAudioSignalQualityType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAudioSignalQualityType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAudioSignalQualityType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAudioSignalQualityType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAudioSignalQualityType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAudioSignalQualityType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSignalQualityType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAudioSignalQualityType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsAudioSignalQualityType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSignalQualityType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAudioSignalQualityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("IsOriginalMono")) == 0)
	{
		// IsOriginalMono is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("BroadcastReady")) == 0)
	{
		// BroadcastReady is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Operator")) == 0)
	{
		// Operator is simple element PersonType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOperator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOperator(p, client);
					if((p = GetOperator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsedTool")) == 0)
	{
		// UsedTool is simple element CreationToolType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUsedTool()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUsedTool(p, client);
					if((p = GetUsedTool()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BackgroundNoiseLevel")) == 0)
	{
		// BackgroundNoiseLevel is simple element BackgroundNoiseLevelType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBackgroundNoiseLevel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BackgroundNoiseLevelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsBackgroundNoiseLevelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBackgroundNoiseLevel(p, client);
					if((p = GetBackgroundNoiseLevel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RelativeDelay")) == 0)
	{
		// RelativeDelay is simple element RelativeDelayType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRelativeDelay()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelativeDelayType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelativeDelayPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRelativeDelay(p, client);
					if((p = GetRelativeDelay()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Balance")) == 0)
	{
		// Balance is simple element BalanceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBalance()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BalanceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsBalancePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBalance(p, client);
					if((p = GetBalance()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DcOffset")) == 0)
	{
		// DcOffset is simple element DcOffsetType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDcOffset()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DcOffsetType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDcOffsetPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDcOffset(p, client);
					if((p = GetDcOffset()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CrossChannelCorrelation")) == 0)
	{
		// CrossChannelCorrelation is simple element CrossChannelCorrelationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCrossChannelCorrelation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CrossChannelCorrelationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCrossChannelCorrelationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCrossChannelCorrelation(p, client);
					if((p = GetCrossChannelCorrelation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Bandwidth")) == 0)
	{
		// Bandwidth is simple element BandwidthType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBandwidth()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:BandwidthType")))) != empty)
			{
				// Is type allowed
				Mp7JrsBandwidthPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBandwidth(p, client);
					if((p = GetBandwidth()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TransmissionTechnology")) == 0)
	{
		// TransmissionTechnology is simple element TransmissionTechnologyType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTransmissionTechnology()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TransmissionTechnologyType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTransmissionTechnologyPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTransmissionTechnology(p, client);
					if((p = GetTransmissionTechnology()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ErrorEventList")) == 0)
	{
		// ErrorEventList is simple element AudioSignalQualityType_ErrorEventList_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetErrorEventList()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSignalQualityType_ErrorEventList_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetErrorEventList(p, client);
					if((p = GetErrorEventList()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioSignalQualityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioSignalQualityType");
	}
	return result;
}

XMLCh * Mp7JrsAudioSignalQualityType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioSignalQualityType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioSignalQualityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioSignalQualityType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_IsOriginalMono_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_IsOriginalMono);
		element->setAttributeNS(X(""), X("IsOriginalMono"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_BroadcastReady_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_BroadcastReady);
		element->setAttributeNS(X(""), X("BroadcastReady"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Operator != Mp7JrsPersonPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Operator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Operator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Operator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_UsedTool != Mp7JrsCreationToolPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_UsedTool->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("UsedTool"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_UsedTool->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_BackgroundNoiseLevel != Mp7JrsBackgroundNoiseLevelPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BackgroundNoiseLevel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BackgroundNoiseLevel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BackgroundNoiseLevel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_RelativeDelay != Mp7JrsRelativeDelayPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RelativeDelay->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RelativeDelay"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RelativeDelay->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Balance != Mp7JrsBalancePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Balance->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Balance"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Balance->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_DcOffset != Mp7JrsDcOffsetPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DcOffset->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DcOffset"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DcOffset->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CrossChannelCorrelation != Mp7JrsCrossChannelCorrelationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CrossChannelCorrelation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CrossChannelCorrelation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CrossChannelCorrelation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Bandwidth != Mp7JrsBandwidthPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Bandwidth->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Bandwidth"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Bandwidth->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TransmissionTechnology != Mp7JrsTransmissionTechnologyPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TransmissionTechnology->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TransmissionTechnology"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TransmissionTechnology->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ErrorEventList != Mp7JrsAudioSignalQualityType_ErrorEventList_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ErrorEventList->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ErrorEventList"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ErrorEventList->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioSignalQualityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("IsOriginalMono")))
	{
		// deserialize value type
		this->SetIsOriginalMono(Dc1Convert::TextToBool(parent->getAttribute(X("IsOriginalMono"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("BroadcastReady")))
	{
		// deserialize value type
		this->SetBroadcastReady(Dc1Convert::TextToBool(parent->getAttribute(X("BroadcastReady"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Operator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Operator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePersonType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOperator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("UsedTool"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("UsedTool")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCreationToolType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUsedTool(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BackgroundNoiseLevel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BackgroundNoiseLevel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateBackgroundNoiseLevelType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBackgroundNoiseLevel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RelativeDelay"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RelativeDelay")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRelativeDelayType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelativeDelay(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Balance"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Balance")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateBalanceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBalance(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DcOffset"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DcOffset")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDcOffsetType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDcOffset(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CrossChannelCorrelation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CrossChannelCorrelation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCrossChannelCorrelationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCrossChannelCorrelation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Bandwidth"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Bandwidth")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateBandwidthType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBandwidth(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TransmissionTechnology"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TransmissionTechnology")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTransmissionTechnologyType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTransmissionTechnology(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ErrorEventList"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ErrorEventList")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioSignalQualityType_ErrorEventList_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetErrorEventList(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioSignalQualityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Operator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePersonType; // FTT, check this
	}
	this->SetOperator(child);
  }
  if (XMLString::compareString(elementname, X("UsedTool")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCreationToolType; // FTT, check this
	}
	this->SetUsedTool(child);
  }
  if (XMLString::compareString(elementname, X("BackgroundNoiseLevel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateBackgroundNoiseLevelType; // FTT, check this
	}
	this->SetBackgroundNoiseLevel(child);
  }
  if (XMLString::compareString(elementname, X("RelativeDelay")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRelativeDelayType; // FTT, check this
	}
	this->SetRelativeDelay(child);
  }
  if (XMLString::compareString(elementname, X("Balance")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateBalanceType; // FTT, check this
	}
	this->SetBalance(child);
  }
  if (XMLString::compareString(elementname, X("DcOffset")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDcOffsetType; // FTT, check this
	}
	this->SetDcOffset(child);
  }
  if (XMLString::compareString(elementname, X("CrossChannelCorrelation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCrossChannelCorrelationType; // FTT, check this
	}
	this->SetCrossChannelCorrelation(child);
  }
  if (XMLString::compareString(elementname, X("Bandwidth")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateBandwidthType; // FTT, check this
	}
	this->SetBandwidth(child);
  }
  if (XMLString::compareString(elementname, X("TransmissionTechnology")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTransmissionTechnologyType; // FTT, check this
	}
	this->SetTransmissionTechnology(child);
  }
  if (XMLString::compareString(elementname, X("ErrorEventList")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioSignalQualityType_ErrorEventList_LocalType; // FTT, check this
	}
	this->SetErrorEventList(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioSignalQualityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOperator() != Dc1NodePtr())
		result.Insert(GetOperator());
	if (GetUsedTool() != Dc1NodePtr())
		result.Insert(GetUsedTool());
	if (GetBackgroundNoiseLevel() != Dc1NodePtr())
		result.Insert(GetBackgroundNoiseLevel());
	if (GetRelativeDelay() != Dc1NodePtr())
		result.Insert(GetRelativeDelay());
	if (GetBalance() != Dc1NodePtr())
		result.Insert(GetBalance());
	if (GetDcOffset() != Dc1NodePtr())
		result.Insert(GetDcOffset());
	if (GetCrossChannelCorrelation() != Dc1NodePtr())
		result.Insert(GetCrossChannelCorrelation());
	if (GetBandwidth() != Dc1NodePtr())
		result.Insert(GetBandwidth());
	if (GetTransmissionTechnology() != Dc1NodePtr())
		result.Insert(GetTransmissionTechnology());
	if (GetErrorEventList() != Dc1NodePtr())
		result.Insert(GetErrorEventList());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioSignalQualityType_ExtMethodImpl.h


