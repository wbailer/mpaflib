
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned32.h"
#include "Mp7Jrsunsigned8.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtPropInit.h

}

IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::~IMp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtPropCleanup.h

}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::~Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType()
{
	Cleanup();
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaTimeOfFrame = Mp7Jrsunsigned32Ptr(); // Class with content 
	m_MediaTimeOfFrame_Exist = false;
	m_FrameConfidence = Mp7Jrsunsigned8Ptr(); // Class with content 
	m_Word = Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr(); // Class
	m_FrameSignature = Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtMyPropInit.h

}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MediaTimeOfFrame);
	// Dc1Factory::DeleteObject(m_FrameConfidence);
	// Dc1Factory::DeleteObject(m_Word);
	// Dc1Factory::DeleteObject(m_FrameSignature);
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType
	const Dc1Ptr< Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidMediaTimeOfFrame())
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfFrame);
		this->SetMediaTimeOfFrame(Dc1Factory::CloneObject(tmp->GetMediaTimeOfFrame()));
	}
	else
	{
		InvalidateMediaTimeOfFrame();
	}
		// Dc1Factory::DeleteObject(m_FrameConfidence);
		this->SetFrameConfidence(Dc1Factory::CloneObject(tmp->GetFrameConfidence()));
		// Dc1Factory::DeleteObject(m_Word);
		this->SetWord(Dc1Factory::CloneObject(tmp->GetWord()));
		// Dc1Factory::DeleteObject(m_FrameSignature);
		this->SetFrameSignature(Dc1Factory::CloneObject(tmp->GetFrameSignature()));
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetMediaTimeOfFrame() const
{
		return m_MediaTimeOfFrame;
}

// Element is optional
bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::IsValidMediaTimeOfFrame() const
{
	return m_MediaTimeOfFrame_Exist;
}

Mp7Jrsunsigned8Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetFrameConfidence() const
{
		return m_FrameConfidence;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetWord() const
{
		return m_Word;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetFrameSignature() const
{
		return m_FrameSignature;
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetMediaTimeOfFrame(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetMediaTimeOfFrame().");
	}
	if (m_MediaTimeOfFrame != item || m_MediaTimeOfFrame_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfFrame);
		m_MediaTimeOfFrame = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimeOfFrame) m_MediaTimeOfFrame->SetParent(m_myself.getPointer());
		if (m_MediaTimeOfFrame && m_MediaTimeOfFrame->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimeOfFrame->UseTypeAttribute = true;
		}
		m_MediaTimeOfFrame_Exist = true;
		if(m_MediaTimeOfFrame != Mp7Jrsunsigned32Ptr())
		{
			m_MediaTimeOfFrame->SetContentName(XMLString::transcode("MediaTimeOfFrame"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::InvalidateMediaTimeOfFrame()
{
	m_MediaTimeOfFrame_Exist = false;
}
void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetFrameConfidence(const Mp7Jrsunsigned8Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetFrameConfidence().");
	}
	if (m_FrameConfidence != item)
	{
		// Dc1Factory::DeleteObject(m_FrameConfidence);
		m_FrameConfidence = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrameConfidence) m_FrameConfidence->SetParent(m_myself.getPointer());
		if (m_FrameConfidence && m_FrameConfidence->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrameConfidence->UseTypeAttribute = true;
		}
		if(m_FrameConfidence != Mp7Jrsunsigned8Ptr())
		{
			m_FrameConfidence->SetContentName(XMLString::transcode("FrameConfidence"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetWord(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetWord().");
	}
	if (m_Word != item)
	{
		// Dc1Factory::DeleteObject(m_Word);
		m_Word = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Word) m_Word->SetParent(m_myself.getPointer());
		if (m_Word && m_Word->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Word->UseTypeAttribute = true;
		}
		if(m_Word != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr())
		{
			m_Word->SetContentName(XMLString::transcode("Word"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetFrameSignature(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::SetFrameSignature().");
	}
	if (m_FrameSignature != item)
	{
		// Dc1Factory::DeleteObject(m_FrameSignature);
		m_FrameSignature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrameSignature) m_FrameSignature->SetParent(m_myself.getPointer());
		if (m_FrameSignature && m_FrameSignature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrameSignature->UseTypeAttribute = true;
		}
		if(m_FrameSignature != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr())
		{
			m_FrameSignature->SetContentName(XMLString::transcode("FrameSignature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaTimeOfFrame")) == 0)
	{
		// MediaTimeOfFrame is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimeOfFrame()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimeOfFrame(p, client);
					if((p = GetMediaTimeOfFrame()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrameConfidence")) == 0)
	{
		// FrameConfidence is simple element unsigned8
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFrameConfidence()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned8")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned8Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFrameConfidence(p, client);
					if((p = GetFrameConfidence()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Word")) == 0)
	{
		// Word is simple element VideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetWord()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetWord(p, client);
					if((p = GetWord()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrameSignature")) == 0)
	{
		// FrameSignature is simple element VideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFrameSignature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFrameSignature(p, client);
					if((p = GetFrameSignature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_MediaTimeOfFrame != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTimeOfFrame->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTimeOfFrame"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTimeOfFrame->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_FrameConfidence != Mp7Jrsunsigned8Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrameConfidence->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrameConfidence"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrameConfidence->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Word != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Word->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Word"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Word->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FrameSignature != Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrameSignature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrameSignature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrameSignature->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTimeOfFrame"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimeOfFrame")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTimeOfFrame(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrameConfidence"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrameConfidence")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned8; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrameConfidence(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Word"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Word")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetWord(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrameSignature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrameSignature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrameSignature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaTimeOfFrame")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetMediaTimeOfFrame(child);
  }
  if (XMLString::compareString(elementname, X("FrameConfidence")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned8; // FTT, check this
	}
	this->SetFrameConfidence(child);
  }
  if (XMLString::compareString(elementname, X("Word")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_Word_LocalType; // FTT, check this
	}
	this->SetWord(child);
  }
  if (XMLString::compareString(elementname, X("FrameSignature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSignatureType_VideoSignatureRegion_VideoFrame_FrameSignature_LocalType; // FTT, check this
	}
	this->SetFrameSignature(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTimeOfFrame() != Dc1NodePtr())
		result.Insert(GetMediaTimeOfFrame());
	if (GetFrameConfidence() != Dc1NodePtr())
		result.Insert(GetFrameConfidence());
	if (GetWord() != Dc1NodePtr())
		result.Insert(GetWord());
	if (GetFrameSignature() != Dc1NodePtr())
		result.Insert(GetFrameSignature());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VideoFrame_LocalType_ExtMethodImpl.h


