
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionType.h"
#include "Mp7JrsPhoneticTranscriptionLexiconType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Token

#include <assert.h>
IMp7JrsPhoneticTranscriptionLexiconType::IMp7JrsPhoneticTranscriptionLexiconType()
{

// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtPropInit.h

}

IMp7JrsPhoneticTranscriptionLexiconType::~IMp7JrsPhoneticTranscriptionLexiconType()
{
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtPropCleanup.h

}

Mp7JrsPhoneticTranscriptionLexiconType::Mp7JrsPhoneticTranscriptionLexiconType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPhoneticTranscriptionLexiconType::~Mp7JrsPhoneticTranscriptionLexiconType()
{
	Cleanup();
}

void Mp7JrsPhoneticTranscriptionLexiconType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;
	m_phoneticAlphabet = Mp7JrsphoneticAlphabetType::UninitializedEnumeration;
	m_phoneticAlphabet_Default = Mp7JrsphoneticAlphabetType::sampa; // Default enumeration
	m_phoneticAlphabet_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Token = Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtMyPropInit.h

}

void Mp7JrsPhoneticTranscriptionLexiconType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_Token);
}

void Mp7JrsPhoneticTranscriptionLexiconType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PhoneticTranscriptionLexiconTypePtr, since we
	// might need GetBase(), which isn't defined in IPhoneticTranscriptionLexiconType
	const Dc1Ptr< Mp7JrsPhoneticTranscriptionLexiconType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPhoneticTranscriptionLexiconType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
	{
	if (tmp->ExistphoneticAlphabet())
	{
		this->SetphoneticAlphabet(tmp->GetphoneticAlphabet());
	}
	else
	{
		InvalidatephoneticAlphabet();
	}
	}
		// Dc1Factory::DeleteObject(m_Token);
		this->SetToken(Dc1Factory::CloneObject(tmp->GetToken()));
}

Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsPhoneticTranscriptionLexiconType::GetBase() const
{
	return m_Base;
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsPhoneticTranscriptionLexiconType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsPhoneticTranscriptionLexiconType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneticTranscriptionLexiconType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsPhoneticTranscriptionLexiconType::GetphoneticAlphabet() const
{
	if (this->ExistphoneticAlphabet()) {
		return m_phoneticAlphabet;
	} else {
		return m_phoneticAlphabet_Default;
	}
}

bool Mp7JrsPhoneticTranscriptionLexiconType::ExistphoneticAlphabet() const
{
	return m_phoneticAlphabet_Exist;
}
void Mp7JrsPhoneticTranscriptionLexiconType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType::SetphoneticAlphabet().");
	}
	m_phoneticAlphabet = item;
	m_phoneticAlphabet_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneticTranscriptionLexiconType::InvalidatephoneticAlphabet()
{
	m_phoneticAlphabet_Exist = false;
}
Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr Mp7JrsPhoneticTranscriptionLexiconType::GetToken() const
{
		return m_Token;
}

void Mp7JrsPhoneticTranscriptionLexiconType::SetToken(const Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType::SetToken().");
	}
	if (m_Token != item)
	{
		// Dc1Factory::DeleteObject(m_Token);
		m_Token = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Token) m_Token->SetParent(m_myself.getPointer());
		if(m_Token != Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr())
		{
			m_Token->SetContentName(XMLString::transcode("Token"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsPhoneticTranscriptionLexiconType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsPhoneticTranscriptionLexiconType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneticTranscriptionLexiconType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneticTranscriptionLexiconType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsPhoneticTranscriptionLexiconType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticAlphabet")) == 0)
	{
		// phoneticAlphabet is simple attribute phoneticAlphabetType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsphoneticAlphabetType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Token")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Token is item of type PhoneticTranscriptionLexiconType_Token_LocalType
		// in element collection PhoneticTranscriptionLexiconType_Token_CollectionType
		
		context->Found = true;
		Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr coll = GetToken();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePhoneticTranscriptionLexiconType_Token_CollectionType; // FTT, check this
				SetToken(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneticTranscriptionLexiconType_Token_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPhoneticTranscriptionLexiconType_Token_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PhoneticTranscriptionLexiconType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PhoneticTranscriptionLexiconType");
	}
	return result;
}

XMLCh * Mp7JrsPhoneticTranscriptionLexiconType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPhoneticTranscriptionLexiconType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPhoneticTranscriptionLexiconType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PhoneticTranscriptionLexiconType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneticAlphabet_Exist)
	{
	// Enumeration
	if(m_phoneticAlphabet != Mp7JrsphoneticAlphabetType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsphoneticAlphabetType::ToText(m_phoneticAlphabet);
		element->setAttributeNS(X(""), X("phoneticAlphabet"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Token != Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Token->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPhoneticTranscriptionLexiconType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticAlphabet")))
	{
		this->SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Parse(parent->getAttribute(X("phoneticAlphabet"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Token"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Token")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr tmp = CreatePhoneticTranscriptionLexiconType_Token_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetToken(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPhoneticTranscriptionLexiconType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Token")) == 0))
  {
	Mp7JrsPhoneticTranscriptionLexiconType_Token_CollectionPtr tmp = CreatePhoneticTranscriptionLexiconType_Token_CollectionType; // FTT, check this
	this->SetToken(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPhoneticTranscriptionLexiconType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetToken() != Dc1NodePtr())
		result.Insert(GetToken());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPhoneticTranscriptionLexiconType_ExtMethodImpl.h


