
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtImplInclude.h


#include "Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsPlaceType_PostalAddress_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPlaceType_PostalAddress_LocalType::IMp7JrsPlaceType_PostalAddress_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtPropInit.h

}

IMp7JrsPlaceType_PostalAddress_LocalType::~IMp7JrsPlaceType_PostalAddress_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtPropCleanup.h

}

Mp7JrsPlaceType_PostalAddress_LocalType::Mp7JrsPlaceType_PostalAddress_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPlaceType_PostalAddress_LocalType::~Mp7JrsPlaceType_PostalAddress_LocalType()
{
	Cleanup();
}

void Mp7JrsPlaceType_PostalAddress_LocalType::Init()
{

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_AddressLine = Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr(); // Collection
	m_PostingIdentifier = Mp7JrsTextualPtr(); // Class
	m_PostingIdentifier_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtMyPropInit.h

}

void Mp7JrsPlaceType_PostalAddress_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_AddressLine);
	// Dc1Factory::DeleteObject(m_PostingIdentifier);
}

void Mp7JrsPlaceType_PostalAddress_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PlaceType_PostalAddress_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPlaceType_PostalAddress_LocalType
	const Dc1Ptr< Mp7JrsPlaceType_PostalAddress_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_AddressLine);
		this->SetAddressLine(Dc1Factory::CloneObject(tmp->GetAddressLine()));
	if (tmp->IsValidPostingIdentifier())
	{
		// Dc1Factory::DeleteObject(m_PostingIdentifier);
		this->SetPostingIdentifier(Dc1Factory::CloneObject(tmp->GetPostingIdentifier()));
	}
	else
	{
		InvalidatePostingIdentifier();
	}
}

Dc1NodePtr Mp7JrsPlaceType_PostalAddress_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPlaceType_PostalAddress_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsPlaceType_PostalAddress_LocalType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsPlaceType_PostalAddress_LocalType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsPlaceType_PostalAddress_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_PostalAddress_LocalType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_PostalAddress_LocalType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr Mp7JrsPlaceType_PostalAddress_LocalType::GetAddressLine() const
{
		return m_AddressLine;
}

Mp7JrsTextualPtr Mp7JrsPlaceType_PostalAddress_LocalType::GetPostingIdentifier() const
{
		return m_PostingIdentifier;
}

// Element is optional
bool Mp7JrsPlaceType_PostalAddress_LocalType::IsValidPostingIdentifier() const
{
	return m_PostingIdentifier_Exist;
}

void Mp7JrsPlaceType_PostalAddress_LocalType::SetAddressLine(const Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_PostalAddress_LocalType::SetAddressLine().");
	}
	if (m_AddressLine != item)
	{
		// Dc1Factory::DeleteObject(m_AddressLine);
		m_AddressLine = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AddressLine) m_AddressLine->SetParent(m_myself.getPointer());
		if(m_AddressLine != Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr())
		{
			m_AddressLine->SetContentName(XMLString::transcode("AddressLine"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_PostalAddress_LocalType::SetPostingIdentifier(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_PostalAddress_LocalType::SetPostingIdentifier().");
	}
	if (m_PostingIdentifier != item || m_PostingIdentifier_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PostingIdentifier);
		m_PostingIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PostingIdentifier) m_PostingIdentifier->SetParent(m_myself.getPointer());
		if (m_PostingIdentifier && m_PostingIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PostingIdentifier->UseTypeAttribute = true;
		}
		m_PostingIdentifier_Exist = true;
		if(m_PostingIdentifier != Mp7JrsTextualPtr())
		{
			m_PostingIdentifier->SetContentName(XMLString::transcode("PostingIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_PostalAddress_LocalType::InvalidatePostingIdentifier()
{
	m_PostingIdentifier_Exist = false;
}

Dc1NodeEnum Mp7JrsPlaceType_PostalAddress_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AddressLine")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AddressLine is item of type TextualType
		// in element collection PlaceType_PostalAddress_AddressLine_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr coll = GetAddressLine();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_PostalAddress_AddressLine_CollectionType; // FTT, check this
				SetAddressLine(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PostingIdentifier")) == 0)
	{
		// PostingIdentifier is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPostingIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPostingIdentifier(p, client);
					if((p = GetPostingIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PlaceType_PostalAddress_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PlaceType_PostalAddress_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPlaceType_PostalAddress_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPlaceType_PostalAddress_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPlaceType_PostalAddress_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PlaceType_PostalAddress_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_AddressLine != Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AddressLine->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_PostingIdentifier != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PostingIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PostingIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PostingIdentifier->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPlaceType_PostalAddress_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AddressLine"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AddressLine")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr tmp = CreatePlaceType_PostalAddress_AddressLine_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAddressLine(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PostingIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PostingIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPostingIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPlaceType_PostalAddress_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AddressLine")) == 0))
  {
	Mp7JrsPlaceType_PostalAddress_AddressLine_CollectionPtr tmp = CreatePlaceType_PostalAddress_AddressLine_CollectionType; // FTT, check this
	this->SetAddressLine(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("PostingIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetPostingIdentifier(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPlaceType_PostalAddress_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAddressLine() != Dc1NodePtr())
		result.Insert(GetAddressLine());
	if (GetPostingIdentifier() != Dc1NodePtr())
		result.Insert(GetPostingIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPlaceType_PostalAddress_LocalType_ExtMethodImpl.h


