
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtImplInclude.h


#include "Mp7JrsMediaAgentType.h"
#include "Mp7JrsCreatorType_Character_CollectionType.h"
#include "Mp7JrsCreatorType_Instrument_CollectionType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsCreatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPersonNameType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Character
#include "Mp7JrsCreationToolType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Instrument

#include <assert.h>
IMp7JrsCreatorType::IMp7JrsCreatorType()
{

// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtPropInit.h

}

IMp7JrsCreatorType::~IMp7JrsCreatorType()
{
// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtPropCleanup.h

}

Mp7JrsCreatorType::Mp7JrsCreatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreatorType::~Mp7JrsCreatorType()
{
	Cleanup();
}

void Mp7JrsCreatorType::Init()
{
	// Init base
	m_Base = CreateMediaAgentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Character = Mp7JrsCreatorType_Character_CollectionPtr(); // Collection
	m_Instrument = Mp7JrsCreatorType_Instrument_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtMyPropInit.h

}

void Mp7JrsCreatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Character);
	// Dc1Factory::DeleteObject(m_Instrument);
}

void Mp7JrsCreatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreatorTypePtr, since we
	// might need GetBase(), which isn't defined in ICreatorType
	const Dc1Ptr< Mp7JrsCreatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaAgentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCreatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Character);
		this->SetCharacter(Dc1Factory::CloneObject(tmp->GetCharacter()));
		// Dc1Factory::DeleteObject(m_Instrument);
		this->SetInstrument(Dc1Factory::CloneObject(tmp->GetInstrument()));
}

Dc1NodePtr Mp7JrsCreatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsCreatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMediaAgentType > Mp7JrsCreatorType::GetBase() const
{
	return m_Base;
}

Mp7JrsCreatorType_Character_CollectionPtr Mp7JrsCreatorType::GetCharacter() const
{
		return m_Character;
}

Mp7JrsCreatorType_Instrument_CollectionPtr Mp7JrsCreatorType::GetInstrument() const
{
		return m_Instrument;
}

void Mp7JrsCreatorType::SetCharacter(const Mp7JrsCreatorType_Character_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreatorType::SetCharacter().");
	}
	if (m_Character != item)
	{
		// Dc1Factory::DeleteObject(m_Character);
		m_Character = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Character) m_Character->SetParent(m_myself.getPointer());
		if(m_Character != Mp7JrsCreatorType_Character_CollectionPtr())
		{
			m_Character->SetContentName(XMLString::transcode("Character"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreatorType::SetInstrument(const Mp7JrsCreatorType_Instrument_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreatorType::SetInstrument().");
	}
	if (m_Instrument != item)
	{
		// Dc1Factory::DeleteObject(m_Instrument);
		m_Instrument = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Instrument) m_Instrument->SetParent(m_myself.getPointer());
		if(m_Instrument != Mp7JrsCreatorType_Instrument_CollectionPtr())
		{
			m_Instrument->SetContentName(XMLString::transcode("Instrument"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsControlledTermUsePtr Mp7JrsCreatorType::GetRole() const
{
	return GetBase()->GetRole();
}

Dc1Ptr< Dc1Node > Mp7JrsCreatorType::GetAgent() const
{
	return GetBase()->GetAgent();
}

Mp7JrsReferencePtr Mp7JrsCreatorType::GetAgentRef() const
{
	return GetBase()->GetAgentRef();
}

void Mp7JrsCreatorType::SetRole(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreatorType::SetRole().");
	}
	GetBase()->SetRole(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsCreatorType::SetAgent(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreatorType::SetAgent().");
	}
	GetBase()->SetAgent(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsCreatorType::SetAgentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreatorType::SetAgentRef().");
	}
	GetBase()->SetAgentRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCreatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Character")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Character is item of type PersonNameType
		// in element collection CreatorType_Character_CollectionType
		
		context->Found = true;
		Mp7JrsCreatorType_Character_CollectionPtr coll = GetCharacter();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreatorType_Character_CollectionType; // FTT, check this
				SetCharacter(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonNamePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Instrument")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Instrument is item of type CreationToolType
		// in element collection CreatorType_Instrument_CollectionType
		
		context->Found = true;
		Mp7JrsCreatorType_Instrument_CollectionPtr coll = GetInstrument();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCreatorType_Instrument_CollectionType; // FTT, check this
				SetInstrument(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreatorType");
	}
	return result;
}

XMLCh * Mp7JrsCreatorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsCreatorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsCreatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreatorType"));
	// Element serialization:
	if (m_Character != Mp7JrsCreatorType_Character_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Character->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Instrument != Mp7JrsCreatorType_Instrument_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Instrument->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCreatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaAgentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Character"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Character")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreatorType_Character_CollectionPtr tmp = CreateCreatorType_Character_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCharacter(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Instrument"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Instrument")) == 0))
		{
			// Deserialize factory type
			Mp7JrsCreatorType_Instrument_CollectionPtr tmp = CreateCreatorType_Instrument_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInstrument(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Character")) == 0))
  {
	Mp7JrsCreatorType_Character_CollectionPtr tmp = CreateCreatorType_Character_CollectionType; // FTT, check this
	this->SetCharacter(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Instrument")) == 0))
  {
	Mp7JrsCreatorType_Instrument_CollectionPtr tmp = CreateCreatorType_Instrument_CollectionType; // FTT, check this
	this->SetInstrument(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCharacter() != Dc1NodePtr())
		result.Insert(GetCharacter());
	if (GetInstrument() != Dc1NodePtr())
		result.Insert(GetInstrument());
	if (GetRole() != Dc1NodePtr())
		result.Insert(GetRole());
	if (GetAgent() != Dc1NodePtr())
		result.Insert(GetAgent());
	if (GetAgentRef() != Dc1NodePtr())
		result.Insert(GetAgentRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreatorType_ExtMethodImpl.h


