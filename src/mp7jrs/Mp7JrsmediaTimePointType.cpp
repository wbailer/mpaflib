
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsmediaTimePointType_ExtImplInclude.h


#include "Mp7JrsmediaTimePointType.h"

Mp7JrsmediaTimePointType::Mp7JrsmediaTimePointType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrsmediaTimePointType::~Mp7JrsmediaTimePointType()
{
		Cleanup();
}

void Mp7JrsmediaTimePointType::Init()
{	   
		m_TimepointSign = +1;
		m_Year = -1;
		m_Month = -1;
		m_Day = -1;
		m_Date_Exist = false;
		m_Hours = -1;
		m_Minutes = -1;
		m_Seconds = -1;
		m_NumberOfFractions = -1;
		m_Time_Exist = false;
		m_FractionsPerSecond = -1;

// no includefile for extension defined 
// file Mp7JrsmediaTimePointType_ExtPropInit.h

}

void Mp7JrsmediaTimePointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsmediaTimePointType_ExtPropCleanup.h


}

void Mp7JrsmediaTimePointType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrsmediaTimePointType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrsmediaTimePointType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsmediaTimePointType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrsmediaTimePointType::GetTimepointSign() const
{
		return m_TimepointSign;
}

void Mp7JrsmediaTimePointType::SetTimepointSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetTimepointSign().");
		}
		m_TimepointSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetYear() const
{
		return m_Year;
}

void Mp7JrsmediaTimePointType::SetYear(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetYear().");
		}
		m_Year = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetMonth() const
{
		return m_Month;
}

void Mp7JrsmediaTimePointType::SetMonth(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetMonth().");
		}
		m_Month = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetDay() const
{
		return m_Day;
}

void Mp7JrsmediaTimePointType::SetDay(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetDay().");
		}
		m_Day = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsmediaTimePointType::GetDate_Exist() const
{
		return m_Date_Exist;
}

void Mp7JrsmediaTimePointType::SetDate_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetDate_Exist().");
		}
		m_Date_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetHours() const
{
		return m_Hours;
}

void Mp7JrsmediaTimePointType::SetHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetHours().");
		}
		m_Hours = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetMinutes() const
{
		return m_Minutes;
}

void Mp7JrsmediaTimePointType::SetMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetMinutes().");
		}
		m_Minutes = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetSeconds() const
{
		return m_Seconds;
}

void Mp7JrsmediaTimePointType::SetSeconds(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetSeconds().");
		}
		m_Seconds = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetNumberOfFractions() const
{
		return m_NumberOfFractions;
}

void Mp7JrsmediaTimePointType::SetNumberOfFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetNumberOfFractions().");
		}
		m_NumberOfFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsmediaTimePointType::GetTime_Exist() const
{
		return m_Time_Exist;
}

void Mp7JrsmediaTimePointType::SetTime_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetTime_Exist().");
		}
		m_Time_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaTimePointType::GetFractionsPerSecond() const
{
		return m_FractionsPerSecond;
}

void Mp7JrsmediaTimePointType::SetFractionsPerSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaTimePointType::SetFractionsPerSecond().");
		}
		m_FractionsPerSecond = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrsmediaTimePointType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_TimepointSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}

		if(m_Date_Exist)
		{
			if(m_Year >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Year);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Month >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Month);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Day >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Day);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_Time_Exist)
		{
			if(m_Hours >= 0)
			{
				Dc1Util::CatString(&buf, X("T"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Hours);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Minutes >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Minutes);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Seconds >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Seconds);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_NumberOfFractions >= 0)
			{
				tmp = Dc1Convert::BinToText(m_NumberOfFractions);
				Dc1Util::CatString(&buf, X(":"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_FractionsPerSecond >= 0)
		{
			tmp = Dc1Convert::BinToText(m_FractionsPerSecond);
			Dc1Util::CatString(&buf, X("F"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrsmediaTimePointType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^(\\-?\\d+(\\-\\d{2}(\\-\\d{2})?)?)?(T\\d{2}(:\\d{2}(:\\d{2}(:\\d+)?)?)?)?(F\\d+)?$");
		// This is for Xerces 3.1.1+
		if(r.matches(txt, &m))
		{
				for(int i = 1; i < m.getNoGroups(); i++)
				{
						int i1 = m.getStartPos(i);
						int i2 = Dc1Util::GetNextEndPos(m, i);
						if(i1 == i2) continue; // ignore missing optional or wrong group
						switch(i)
						{
								case 0:
								//\\-?
												// Nothing to do here...
								break;

								case 1:
										if(i1 >= 0)
										{
												// (\\d+...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2);
												m_TimepointSign = Dc1Convert::TextToSign(substring);
												if(m_TimepointSign < 0)
												{
														XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												}
												m_Year = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 2:
										if(i1 >= 0)
										{
												// (\\-\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Month = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 3:
										if(i1 >= 0)
										{
												// (\\-\\d{2})?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Day = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 4:
										if(i1 >= 0)
										{
												// (T\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Hours = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;


								case 5:
										if(i1 >= 0)
										{
												// (:\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Minutes = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 6:
										if(i1 >= 0)
										{
												// (:\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Seconds = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 7:
										if(i1 >= 0)
										{
												// (:\\d+...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_NumberOfFractions = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 8:
										if(i1 >= 0)
										{
												// (F\\d+)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_FractionsPerSecond = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								default: return true;
								break;
						}
				}
		}
		else return false;
		return true;
}

bool Mp7JrsmediaTimePointType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsmediaTimePointType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsmediaTimePointType::ContentToString() const
{
	// Mp7JrsmediaTimePointType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsmediaTimePointType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType")))));
		}

// no includefile for extension defined 
// file Mp7JrsmediaTimePointType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool Mp7JrsmediaTimePointType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrsmediaTimePointType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// begin extension included
// file Mp7JrsmediaTimePointType_ExtMethodImpl.h
#include "Mp7JrsmediaDurationType.h"

void IMp7JrsmediaTimePointType::Add(Mp7JrsmediaDurationPtr duration, Dc1ClientID client) {
	long long nFractions = (long long)(GetTotalNrFractions()+(duration->GetTotalNrFractions()*1.0/duration->GetFractionsPerSecond())*GetFractionsPerSecond());
	SetTotalNrFractions(nFractions,client);
}

Mp7JrsmediaDurationPtr IMp7JrsmediaTimePointType::Subtract(Mp7JrsmediaTimePointPtr timePoint, Dc1ClientID client) {
	long long nFractions = (long long)(GetTotalNrFractions()-(timePoint->GetTotalNrFractions()*1.0/timePoint->GetFractionsPerSecond())*GetFractionsPerSecond());
	Mp7JrsmediaDurationPtr duration = CreatemediaDurationType;
	duration->SetFractionsPerSecond(GetFractionsPerSecond(),client);
	duration->SetTotalNrFractions(nFractions,client);
	return duration;
}

long long IMp7JrsmediaTimePointType::GetTotalNrFractions() {
	long long sum = 0LL;
	// Days and months missing!
	long long val = GetHours() > 0 ? GetHours() : 0LL;
	sum = 60LL * (val + sum);
	val = GetMinutes() > 0 ? GetMinutes() : 0LL;
	sum = 60LL * (val + sum);
	val = GetSeconds() > 0 ? GetSeconds() : 0LL;
	long long val1 = GetFractionsPerSecond() > 0 ? GetFractionsPerSecond() : 0LL;
	sum = val1 * (val + sum);
	val = GetNumberOfFractions() > 0 ? GetNumberOfFractions() : 0LL;
	sum += val; 
	return sum;
}

void IMp7JrsmediaTimePointType::SetTotalNrFractions(long long fractions, Dc1ClientID client) {
	long long remainder = fractions;
	SetNumberOfFractions((int)(remainder % GetFractionsPerSecond()),client);
	remainder /= GetFractionsPerSecond();
	SetSeconds ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetMinutes ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetHours ((int)remainder,client);
	// Days and months ignored!!!!
}

// end extension included








