
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtImplInclude.h


#include "Mp7JrsIntegerMatrixType.h"
#include "Mp7JrsRegionLocatorType_Polygon_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsRegionLocatorType_Polygon_LocalType::IMp7JrsRegionLocatorType_Polygon_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtPropInit.h

}

IMp7JrsRegionLocatorType_Polygon_LocalType::~IMp7JrsRegionLocatorType_Polygon_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtPropCleanup.h

}

Mp7JrsRegionLocatorType_Polygon_LocalType::Mp7JrsRegionLocatorType_Polygon_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRegionLocatorType_Polygon_LocalType::~Mp7JrsRegionLocatorType_Polygon_LocalType()
{
	Cleanup();
}

void Mp7JrsRegionLocatorType_Polygon_LocalType::Init()
{

	// Init attributes
	m_unlocatedRegion = false; // Value
	m_unlocatedRegion_Default = false; // Default value
	m_unlocatedRegion_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Coords = Mp7JrsIntegerMatrixPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtMyPropInit.h

}

void Mp7JrsRegionLocatorType_Polygon_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Coords);
}

void Mp7JrsRegionLocatorType_Polygon_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RegionLocatorType_Polygon_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IRegionLocatorType_Polygon_LocalType
	const Dc1Ptr< Mp7JrsRegionLocatorType_Polygon_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExistunlocatedRegion())
	{
		this->SetunlocatedRegion(tmp->GetunlocatedRegion());
	}
	else
	{
		InvalidateunlocatedRegion();
	}
	}
		// Dc1Factory::DeleteObject(m_Coords);
		this->SetCoords(Dc1Factory::CloneObject(tmp->GetCoords()));
}

Dc1NodePtr Mp7JrsRegionLocatorType_Polygon_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsRegionLocatorType_Polygon_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsRegionLocatorType_Polygon_LocalType::GetunlocatedRegion() const
{
	if (this->ExistunlocatedRegion()) {
		return m_unlocatedRegion;
	} else {
		return m_unlocatedRegion_Default;
	}
}

bool Mp7JrsRegionLocatorType_Polygon_LocalType::ExistunlocatedRegion() const
{
	return m_unlocatedRegion_Exist;
}
void Mp7JrsRegionLocatorType_Polygon_LocalType::SetunlocatedRegion(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRegionLocatorType_Polygon_LocalType::SetunlocatedRegion().");
	}
	m_unlocatedRegion = item;
	m_unlocatedRegion_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRegionLocatorType_Polygon_LocalType::InvalidateunlocatedRegion()
{
	m_unlocatedRegion_Exist = false;
}
Mp7JrsIntegerMatrixPtr Mp7JrsRegionLocatorType_Polygon_LocalType::GetCoords() const
{
		return m_Coords;
}

void Mp7JrsRegionLocatorType_Polygon_LocalType::SetCoords(const Mp7JrsIntegerMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRegionLocatorType_Polygon_LocalType::SetCoords().");
	}
	if (m_Coords != item)
	{
		// Dc1Factory::DeleteObject(m_Coords);
		m_Coords = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Coords) m_Coords->SetParent(m_myself.getPointer());
		if (m_Coords && m_Coords->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Coords->UseTypeAttribute = true;
		}
		if(m_Coords != Mp7JrsIntegerMatrixPtr())
		{
			m_Coords->SetContentName(XMLString::transcode("Coords"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRegionLocatorType_Polygon_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("unlocatedRegion")) == 0)
	{
		// unlocatedRegion is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Coords")) == 0)
	{
		// Coords is simple element IntegerMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoords()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIntegerMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoords(p, client);
					if((p = GetCoords()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RegionLocatorType_Polygon_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RegionLocatorType_Polygon_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsRegionLocatorType_Polygon_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRegionLocatorType_Polygon_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsRegionLocatorType_Polygon_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RegionLocatorType_Polygon_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_unlocatedRegion_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_unlocatedRegion);
		element->setAttributeNS(X(""), X("unlocatedRegion"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Coords != Mp7JrsIntegerMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Coords->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Coords"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Coords->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsRegionLocatorType_Polygon_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("unlocatedRegion")))
	{
		// deserialize value type
		this->SetunlocatedRegion(Dc1Convert::TextToBool(parent->getAttribute(X("unlocatedRegion"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Coords"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Coords")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIntegerMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoords(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRegionLocatorType_Polygon_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Coords")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIntegerMatrixType; // FTT, check this
	}
	this->SetCoords(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRegionLocatorType_Polygon_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCoords() != Dc1NodePtr())
		result.Insert(GetCoords());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_Polygon_LocalType_ExtMethodImpl.h


