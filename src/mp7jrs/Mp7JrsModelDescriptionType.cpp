
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentAbstractionType.h"
#include "Mp7JrsModelDescriptionType_Model_CollectionType.h"
#include "Mp7JrsContentDescriptionType_Affective_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsModelDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsAffectiveType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affective
#include "Mp7JrsModelType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Model

#include <assert.h>
IMp7JrsModelDescriptionType::IMp7JrsModelDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtPropInit.h

}

IMp7JrsModelDescriptionType::~IMp7JrsModelDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtPropCleanup.h

}

Mp7JrsModelDescriptionType::Mp7JrsModelDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsModelDescriptionType::~Mp7JrsModelDescriptionType()
{
	Cleanup();
}

void Mp7JrsModelDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentAbstractionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Model = Mp7JrsModelDescriptionType_Model_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtMyPropInit.h

}

void Mp7JrsModelDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Model);
}

void Mp7JrsModelDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ModelDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IModelDescriptionType
	const Dc1Ptr< Mp7JrsModelDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentAbstractionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsModelDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Model);
		this->SetModel(Dc1Factory::CloneObject(tmp->GetModel()));
}

Dc1NodePtr Mp7JrsModelDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsModelDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentAbstractionType > Mp7JrsModelDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsModelDescriptionType_Model_CollectionPtr Mp7JrsModelDescriptionType::GetModel() const
{
		return m_Model;
}

void Mp7JrsModelDescriptionType::SetModel(const Mp7JrsModelDescriptionType_Model_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelDescriptionType::SetModel().");
	}
	if (m_Model != item)
	{
		// Dc1Factory::DeleteObject(m_Model);
		m_Model = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Model) m_Model->SetParent(m_myself.getPointer());
		if(m_Model != Mp7JrsModelDescriptionType_Model_CollectionPtr())
		{
			m_Model->SetContentName(XMLString::transcode("Model"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsContentDescriptionType_Affective_CollectionPtr Mp7JrsModelDescriptionType::GetAffective() const
{
	return GetBase()->GetBase()->GetAffective();
}

void Mp7JrsModelDescriptionType::SetAffective(const Mp7JrsContentDescriptionType_Affective_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelDescriptionType::SetAffective().");
	}
	GetBase()->GetBase()->SetAffective(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsModelDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsModelDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsModelDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsModelDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsModelDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsModelDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsModelDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Model")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Model is item of abstract type ModelType
		// in element collection ModelDescriptionType_Model_CollectionType
		
		context->Found = true;
		Mp7JrsModelDescriptionType_Model_CollectionPtr coll = GetModel();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateModelDescriptionType_Model_CollectionType; // FTT, check this
				SetModel(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ModelDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ModelDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsModelDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsModelDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsModelDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ModelDescriptionType"));
	// Element serialization:
	if (m_Model != Mp7JrsModelDescriptionType_Model_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Model->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsModelDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentAbstractionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Model"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Model")) == 0))
		{
			// Deserialize factory type
			Mp7JrsModelDescriptionType_Model_CollectionPtr tmp = CreateModelDescriptionType_Model_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetModel(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsModelDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Model")) == 0))
  {
	Mp7JrsModelDescriptionType_Model_CollectionPtr tmp = CreateModelDescriptionType_Model_CollectionType; // FTT, check this
	this->SetModel(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsModelDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetModel() != Dc1NodePtr())
		result.Insert(GetModel());
	if (GetAffective() != Dc1NodePtr())
		result.Insert(GetAffective());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsModelDescriptionType_ExtMethodImpl.h


