
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTitleType_ExtImplInclude.h


#include "Mp7JrsTextualBaseType.h"
#include "Mp7JrsTitleType_type_LocalType2.h"
#include "Mp7JrsTextualBaseType_phoneticTranscription_CollectionType.h"
#include "Mp7JrsTitleType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsTitleType::IMp7JrsTitleType()
{

// no includefile for extension defined 
// file Mp7JrsTitleType_ExtPropInit.h

}

IMp7JrsTitleType::~IMp7JrsTitleType()
{
// no includefile for extension defined 
// file Mp7JrsTitleType_ExtPropCleanup.h

}

Mp7JrsTitleType::Mp7JrsTitleType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTitleType::~Mp7JrsTitleType()
{
	Cleanup();
}

void Mp7JrsTitleType::Init()
{
	// Init base
	m_Base = CreateTextualBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_type = Mp7JrsTitleType_type_Local2Ptr(); // Create emtpy class ptr
	m_type_Exist = false;



// no includefile for extension defined 
// file Mp7JrsTitleType_ExtMyPropInit.h

}

void Mp7JrsTitleType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTitleType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_type);
}

void Mp7JrsTitleType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TitleTypePtr, since we
	// might need GetBase(), which isn't defined in ITitleType
	const Dc1Ptr< Mp7JrsTitleType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTextualBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsTitleType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_type);
	if (tmp->Existtype())
	{
		this->Settype(Dc1Factory::CloneObject(tmp->Gettype()));
	}
	else
	{
		Invalidatetype();
	}
	}
}

Dc1NodePtr Mp7JrsTitleType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsTitleType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsTextualBaseType > Mp7JrsTitleType::GetBase() const
{
	return m_Base;
}

void Mp7JrsTitleType::SetContent(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::SetContent().");
	}
	GetBase()->SetContent(item);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsTitleType::GetContent() const
{
	return GetBase()->GetContent();
}
Mp7JrsTitleType_type_Local2Ptr Mp7JrsTitleType::Gettype() const
{
	if (this->Existtype()) {
		return m_type;
	} else {
		return m_type_Default;
	}
}

bool Mp7JrsTitleType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsTitleType::Settype(const Mp7JrsTitleType_type_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::Settype().");
	}
	m_type = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_type) m_type->SetParent(m_myself.getPointer());
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::Invalidatetype()
{
	m_type_Exist = false;
}
XMLCh * Mp7JrsTitleType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsTitleType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsTitleType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr Mp7JrsTitleType::GetphoneticTranscription() const
{
	return GetBase()->GetphoneticTranscription();
}

bool Mp7JrsTitleType::ExistphoneticTranscription() const
{
	return GetBase()->ExistphoneticTranscription();
}
void Mp7JrsTitleType::SetphoneticTranscription(const Mp7JrsTextualBaseType_phoneticTranscription_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::SetphoneticTranscription().");
	}
	GetBase()->SetphoneticTranscription(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::InvalidatephoneticTranscription()
{
	GetBase()->InvalidatephoneticTranscription();
}
Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsTitleType::GetphoneticAlphabet() const
{
	return GetBase()->GetphoneticAlphabet();
}

bool Mp7JrsTitleType::ExistphoneticAlphabet() const
{
	return GetBase()->ExistphoneticAlphabet();
}
void Mp7JrsTitleType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::SetphoneticAlphabet().");
	}
	GetBase()->SetphoneticAlphabet(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::InvalidatephoneticAlphabet()
{
	GetBase()->InvalidatephoneticAlphabet();
}
XMLCh * Mp7JrsTitleType::Getcharset() const
{
	return GetBase()->Getcharset();
}

bool Mp7JrsTitleType::Existcharset() const
{
	return GetBase()->Existcharset();
}
void Mp7JrsTitleType::Setcharset(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::Setcharset().");
	}
	GetBase()->Setcharset(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::Invalidatecharset()
{
	GetBase()->Invalidatecharset();
}
XMLCh * Mp7JrsTitleType::Getencoding() const
{
	return GetBase()->Getencoding();
}

bool Mp7JrsTitleType::Existencoding() const
{
	return GetBase()->Existencoding();
}
void Mp7JrsTitleType::Setencoding(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::Setencoding().");
	}
	GetBase()->Setencoding(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::Invalidateencoding()
{
	GetBase()->Invalidateencoding();
}
XMLCh * Mp7JrsTitleType::Getscript() const
{
	return GetBase()->Getscript();
}

bool Mp7JrsTitleType::Existscript() const
{
	return GetBase()->Existscript();
}
void Mp7JrsTitleType::Setscript(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTitleType::Setscript().");
	}
	GetBase()->Setscript(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTitleType::Invalidatescript()
{
	GetBase()->Invalidatescript();
}

Dc1NodeEnum Mp7JrsTitleType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute TitleType_type_LocalType2
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsTitleType_type_Local2Ptr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TitleType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TitleType");
	}
	return result;
}

XMLCh * Mp7JrsTitleType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTitleType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsTitleType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsTitleType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsTitleType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTitleType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TitleType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Class
	if (m_type != Mp7JrsTitleType_type_Local2Ptr())
	{
		XMLCh * tmp = m_type->ToText();
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsTitleType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		// Deserialize class type
		Mp7JrsTitleType_type_Local2Ptr tmp = CreateTitleType_type_LocalType2; // FTT, check this
		tmp->Parse(parent->getAttribute(X("type")));
		this->Settype(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsTextualBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsTitleType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTitleType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTitleType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTitleType_ExtMethodImpl.h


