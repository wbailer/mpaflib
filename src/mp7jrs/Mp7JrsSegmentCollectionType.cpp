
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtImplInclude.h


#include "Mp7JrsCollectionType.h"
#include "Mp7JrsSegmentCollectionType_CollectionType.h"
#include "Mp7JrsSegmentCollectionType_CollectionType0.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsCollectionType_TextAnnotation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSegmentCollectionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextAnnotationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentCollectionType_LocalType.h" // Choice collection Segment
#include "Mp7JrsSegmentType.h" // Choice collection element Segment
#include "Mp7JrsSegmentCollectionType_LocalType0.h" // Choice collection SegmentCollection
#include "Mp7JrsSegmentCollectionType.h" // Choice collection element SegmentCollection

#include <assert.h>
IMp7JrsSegmentCollectionType::IMp7JrsSegmentCollectionType()
{

// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtPropInit.h

}

IMp7JrsSegmentCollectionType::~IMp7JrsSegmentCollectionType()
{
// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtPropCleanup.h

}

Mp7JrsSegmentCollectionType::Mp7JrsSegmentCollectionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSegmentCollectionType::~Mp7JrsSegmentCollectionType()
{
	Cleanup();
}

void Mp7JrsSegmentCollectionType::Init()
{
	// Init base
	m_Base = CreateCollectionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SegmentCollectionType_LocalType = Mp7JrsSegmentCollectionType_CollectionPtr(); // Collection
	m_SegmentCollectionType_LocalType0 = Mp7JrsSegmentCollectionType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtMyPropInit.h

}

void Mp7JrsSegmentCollectionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType);
	// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType0);
}

void Mp7JrsSegmentCollectionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SegmentCollectionTypePtr, since we
	// might need GetBase(), which isn't defined in ISegmentCollectionType
	const Dc1Ptr< Mp7JrsSegmentCollectionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsCollectionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSegmentCollectionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType);
		this->SetSegmentCollectionType_LocalType(Dc1Factory::CloneObject(tmp->GetSegmentCollectionType_LocalType()));
		// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType0);
		this->SetSegmentCollectionType_LocalType0(Dc1Factory::CloneObject(tmp->GetSegmentCollectionType_LocalType0()));
}

Dc1NodePtr Mp7JrsSegmentCollectionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSegmentCollectionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsCollectionType > Mp7JrsSegmentCollectionType::GetBase() const
{
	return m_Base;
}

Mp7JrsSegmentCollectionType_CollectionPtr Mp7JrsSegmentCollectionType::GetSegmentCollectionType_LocalType() const
{
		return m_SegmentCollectionType_LocalType;
}

Mp7JrsSegmentCollectionType_Collection0Ptr Mp7JrsSegmentCollectionType::GetSegmentCollectionType_LocalType0() const
{
		return m_SegmentCollectionType_LocalType0;
}

void Mp7JrsSegmentCollectionType::SetSegmentCollectionType_LocalType(const Mp7JrsSegmentCollectionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetSegmentCollectionType_LocalType().");
	}
	if (m_SegmentCollectionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType);
		m_SegmentCollectionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SegmentCollectionType_LocalType) m_SegmentCollectionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::SetSegmentCollectionType_LocalType0(const Mp7JrsSegmentCollectionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetSegmentCollectionType_LocalType0().");
	}
	if (m_SegmentCollectionType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_SegmentCollectionType_LocalType0);
		m_SegmentCollectionType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SegmentCollectionType_LocalType0) m_SegmentCollectionType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSegmentCollectionType::Getname() const
{
	return GetBase()->Getname();
}

bool Mp7JrsSegmentCollectionType::Existname() const
{
	return GetBase()->Existname();
}
void Mp7JrsSegmentCollectionType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::Setname().");
	}
	GetBase()->Setname(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::Invalidatename()
{
	GetBase()->Invalidatename();
}
Mp7JrsCreationInformationPtr Mp7JrsSegmentCollectionType::GetCreationInformation() const
{
	return GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsSegmentCollectionType::GetCreationInformationRef() const
{
	return GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsSegmentCollectionType::GetUsageInformation() const
{
	return GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsSegmentCollectionType::GetUsageInformationRef() const
{
	return GetBase()->GetUsageInformationRef();
}

Mp7JrsCollectionType_TextAnnotation_CollectionPtr Mp7JrsSegmentCollectionType::GetTextAnnotation() const
{
	return GetBase()->GetTextAnnotation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentCollectionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetCreationInformation().");
	}
	GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentCollectionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetCreationInformationRef().");
	}
	GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentCollectionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetUsageInformation().");
	}
	GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentCollectionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetUsageInformationRef().");
	}
	GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::SetTextAnnotation(const Mp7JrsCollectionType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetTextAnnotation().");
	}
	GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSegmentCollectionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSegmentCollectionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSegmentCollectionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSegmentCollectionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSegmentCollectionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSegmentCollectionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSegmentCollectionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSegmentCollectionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSegmentCollectionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSegmentCollectionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSegmentCollectionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSegmentCollectionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSegmentCollectionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSegmentCollectionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSegmentCollectionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentCollectionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSegmentCollectionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSegmentCollectionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentCollectionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSegmentCollectionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Segment")) == 0)
	{
		// Segment is contained in itemtype SegmentCollectionType_LocalType
		// in choice collection SegmentCollectionType_CollectionType

		context->Found = true;
		Mp7JrsSegmentCollectionType_CollectionPtr coll = GetSegmentCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentCollectionType_CollectionType; // FTT, check this
				SetSegmentCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->GetSegment()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentCollectionType_LocalPtr item = CreateSegmentCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->SetSegment(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->GetSegment()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SegmentRef")) == 0)
	{
		// SegmentRef is contained in itemtype SegmentCollectionType_LocalType
		// in choice collection SegmentCollectionType_CollectionType

		context->Found = true;
		Mp7JrsSegmentCollectionType_CollectionPtr coll = GetSegmentCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentCollectionType_CollectionType; // FTT, check this
				SetSegmentCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->GetSegmentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentCollectionType_LocalPtr item = CreateSegmentCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->SetSegmentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentCollectionType_LocalPtr)coll->elementAt(i))->GetSegmentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SegmentCollection")) == 0)
	{
		// SegmentCollection is contained in itemtype SegmentCollectionType_LocalType0
		// in choice collection SegmentCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsSegmentCollectionType_Collection0Ptr coll = GetSegmentCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentCollectionType_CollectionType0; // FTT, check this
				SetSegmentCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentCollectionType_Local0Ptr item = CreateSegmentCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSegmentCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->SetSegmentCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SegmentCollectionRef")) == 0)
	{
		// SegmentCollectionRef is contained in itemtype SegmentCollectionType_LocalType0
		// in choice collection SegmentCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsSegmentCollectionType_Collection0Ptr coll = GetSegmentCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentCollectionType_CollectionType0; // FTT, check this
				SetSegmentCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSegmentCollectionType_Local0Ptr item = CreateSegmentCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->SetSegmentCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSegmentCollectionType_Local0Ptr)coll->elementAt(i))->GetSegmentCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SegmentCollectionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SegmentCollectionType");
	}
	return result;
}

XMLCh * Mp7JrsSegmentCollectionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSegmentCollectionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSegmentCollectionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SegmentCollectionType"));
	// Element serialization:
	if (m_SegmentCollectionType_LocalType != Mp7JrsSegmentCollectionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SegmentCollectionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SegmentCollectionType_LocalType0 != Mp7JrsSegmentCollectionType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SegmentCollectionType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSegmentCollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsCollectionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Segment
			Dc1Util::HasNodeName(parent, X("Segment"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Segment")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SegmentRef
			Dc1Util::HasNodeName(parent, X("SegmentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SegmentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSegmentCollectionType_CollectionPtr tmp = CreateSegmentCollectionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSegmentCollectionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SegmentCollection
			Dc1Util::HasNodeName(parent, X("SegmentCollection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SegmentCollection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SegmentCollectionRef
			Dc1Util::HasNodeName(parent, X("SegmentCollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SegmentCollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSegmentCollectionType_Collection0Ptr tmp = CreateSegmentCollectionType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSegmentCollectionType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSegmentCollectionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Segment"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Segment")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SegmentRef")) == 0)
	)
  {
	Mp7JrsSegmentCollectionType_CollectionPtr tmp = CreateSegmentCollectionType_CollectionType; // FTT, check this
	this->SetSegmentCollectionType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SegmentCollection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SegmentCollection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SegmentCollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SegmentCollectionRef")) == 0)
	)
  {
	Mp7JrsSegmentCollectionType_Collection0Ptr tmp = CreateSegmentCollectionType_CollectionType0; // FTT, check this
	this->SetSegmentCollectionType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSegmentCollectionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSegmentCollectionType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentCollectionType_LocalType());
	if (GetSegmentCollectionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetSegmentCollectionType_LocalType0());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSegmentCollectionType_ExtMethodImpl.h


