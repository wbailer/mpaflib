
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrsVariationType_VariationRelationship_LocalType_ExtImplInclude.h


#include "Mp7JrsVariationType_VariationRelationship_LocalType.h"

// no includefile for extension defined 
// file Mp7JrsVariationType_VariationRelationship_LocalType_ExtMethodImpl.h


const XMLCh * Mp7JrsVariationType_VariationRelationship_LocalType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrsVariationType_VariationRelationship_LocalType::ToText(Mp7JrsVariationType_VariationRelationship_LocalType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/summarization:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("summarization");
			break;
		case /*Enumeration::*/abstraction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("abstraction");
			break;
		case /*Enumeration::*/extraction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("extraction");
			break;
		case /*Enumeration::*/modalityTranslation:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("modalityTranslation");
			break;
		case /*Enumeration::*/languageTranslation:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("languageTranslation");
			break;
		case /*Enumeration::*/colorReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("colorReduction");
			break;
		case /*Enumeration::*/spatialReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("spatialReduction");
			break;
		case /*Enumeration::*/temporalReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("temporalReduction");
			break;
		case /*Enumeration::*/samplingReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("samplingReduction");
			break;
		case /*Enumeration::*/rateReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("rateReduction");
			break;
		case /*Enumeration::*/qualityReduction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("qualityReduction");
			break;
		case /*Enumeration::*/compression:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("compression");
			break;
		case /*Enumeration::*/scaling:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("scaling");
			break;
		case /*Enumeration::*/revision:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("revision");
			break;
		case /*Enumeration::*/substitution:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("substitution");
			break;
		case /*Enumeration::*/replay:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("replay");
			break;
		case /*Enumeration::*/alternativeView:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("alternativeView");
			break;
		case /*Enumeration::*/alternativeMediaProfile:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("alternativeMediaProfile");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrsVariationType_VariationRelationship_LocalType::Enumeration Mp7JrsVariationType_VariationRelationship_LocalType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrsVariationType_VariationRelationship_LocalType::Enumeration result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "summarization") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/summarization;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "abstraction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/abstraction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "extraction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/extraction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "modalityTranslation") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/modalityTranslation;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "languageTranslation") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/languageTranslation;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "colorReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/colorReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "spatialReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/spatialReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "temporalReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/temporalReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "samplingReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/samplingReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "rateReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/rateReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "qualityReduction") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/qualityReduction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "compression") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/compression;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "scaling") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/scaling;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "revision") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/revision;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "substitution") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/substitution;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "replay") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/replay;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "alternativeView") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/alternativeView;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "alternativeMediaProfile") == 0)
	{
		result = Mp7JrsVariationType_VariationRelationship_LocalType::/*Enumeration::*/alternativeMediaProfile;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
