
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtImplInclude.h


#include "Mp7JrsSignalPlaneType.h"
#include "Mp7JrsSignalPlaneType_dim_LocalType.h"
#include "Mp7JrsSignalPlaneOriginType.h"
#include "Mp7JrsSignalPlaneFractionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSignalPlaneFractionType::IMp7JrsSignalPlaneFractionType()
{

// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtPropInit.h

}

IMp7JrsSignalPlaneFractionType::~IMp7JrsSignalPlaneFractionType()
{
// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtPropCleanup.h

}

Mp7JrsSignalPlaneFractionType::Mp7JrsSignalPlaneFractionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSignalPlaneFractionType::~Mp7JrsSignalPlaneFractionType()
{
	Cleanup();
}

void Mp7JrsSignalPlaneFractionType::Init()
{
	// Init base
	m_Base = CreateSignalPlaneType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_x = 0.0; // Value
	m_x_Exist = false;
	m_y = 0.0; // Value
	m_y_Exist = false;
	m_z = 0.0; // Value
	m_z_Exist = false;
	m_t = 0.0; // Value
	m_t_Exist = false;



// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtMyPropInit.h

}

void Mp7JrsSignalPlaneFractionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsSignalPlaneFractionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SignalPlaneFractionTypePtr, since we
	// might need GetBase(), which isn't defined in ISignalPlaneFractionType
	const Dc1Ptr< Mp7JrsSignalPlaneFractionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSignalPlanePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSignalPlaneFractionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existx())
	{
		this->Setx(tmp->Getx());
	}
	else
	{
		Invalidatex();
	}
	}
	{
	if (tmp->Existy())
	{
		this->Sety(tmp->Gety());
	}
	else
	{
		Invalidatey();
	}
	}
	{
	if (tmp->Existz())
	{
		this->Setz(tmp->Getz());
	}
	else
	{
		Invalidatez();
	}
	}
	{
	if (tmp->Existt())
	{
		this->Sett(tmp->Gett());
	}
	else
	{
		Invalidatet();
	}
	}
}

Dc1NodePtr Mp7JrsSignalPlaneFractionType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsSignalPlaneFractionType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsSignalPlaneType > Mp7JrsSignalPlaneFractionType::GetBase() const
{
	return m_Base;
}

double Mp7JrsSignalPlaneFractionType::Getx() const
{
	return m_x;
}

bool Mp7JrsSignalPlaneFractionType::Existx() const
{
	return m_x_Exist;
}
void Mp7JrsSignalPlaneFractionType::Setx(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::Setx().");
	}
	m_x = item;
	m_x_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::Invalidatex()
{
	m_x_Exist = false;
}
double Mp7JrsSignalPlaneFractionType::Gety() const
{
	return m_y;
}

bool Mp7JrsSignalPlaneFractionType::Existy() const
{
	return m_y_Exist;
}
void Mp7JrsSignalPlaneFractionType::Sety(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::Sety().");
	}
	m_y = item;
	m_y_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::Invalidatey()
{
	m_y_Exist = false;
}
double Mp7JrsSignalPlaneFractionType::Getz() const
{
	return m_z;
}

bool Mp7JrsSignalPlaneFractionType::Existz() const
{
	return m_z_Exist;
}
void Mp7JrsSignalPlaneFractionType::Setz(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::Setz().");
	}
	m_z = item;
	m_z_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::Invalidatez()
{
	m_z_Exist = false;
}
double Mp7JrsSignalPlaneFractionType::Gett() const
{
	return m_t;
}

bool Mp7JrsSignalPlaneFractionType::Existt() const
{
	return m_t_Exist;
}
void Mp7JrsSignalPlaneFractionType::Sett(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::Sett().");
	}
	m_t = item;
	m_t_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::Invalidatet()
{
	m_t_Exist = false;
}
Mp7JrsSignalPlaneType_dim_LocalPtr Mp7JrsSignalPlaneFractionType::Getdim() const
{
	return GetBase()->Getdim();
}

bool Mp7JrsSignalPlaneFractionType::Existdim() const
{
	return GetBase()->Existdim();
}
void Mp7JrsSignalPlaneFractionType::Setdim(const Mp7JrsSignalPlaneType_dim_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::Setdim().");
	}
	GetBase()->Setdim(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::Invalidatedim()
{
	GetBase()->Invalidatedim();
}
Mp7JrsSignalPlaneOriginPtr Mp7JrsSignalPlaneFractionType::GetOrigin() const
{
	return GetBase()->GetOrigin();
}

// Element is optional
bool Mp7JrsSignalPlaneFractionType::IsValidOrigin() const
{
	return GetBase()->IsValidOrigin();
}

void Mp7JrsSignalPlaneFractionType::SetOrigin(const Mp7JrsSignalPlaneOriginPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSignalPlaneFractionType::SetOrigin().");
	}
	GetBase()->SetOrigin(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSignalPlaneFractionType::InvalidateOrigin()
{
	GetBase()->InvalidateOrigin();
}

Dc1NodeEnum Mp7JrsSignalPlaneFractionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("x")) == 0)
	{
		// x is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("y")) == 0)
	{
		// y is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("z")) == 0)
	{
		// z is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("t")) == 0)
	{
		// t is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SignalPlaneFractionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SignalPlaneFractionType");
	}
	return result;
}

XMLCh * Mp7JrsSignalPlaneFractionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSignalPlaneFractionType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsSignalPlaneFractionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SignalPlaneFractionType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_x_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_x);
		element->setAttributeNS(X(""), X("x"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_y_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_y);
		element->setAttributeNS(X(""), X("y"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_z_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_z);
		element->setAttributeNS(X(""), X("z"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_t_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_t);
		element->setAttributeNS(X(""), X("t"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsSignalPlaneFractionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("x")))
	{
		// deserialize value type
		this->Setx(Dc1Convert::TextToDouble(parent->getAttribute(X("x"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("y")))
	{
		// deserialize value type
		this->Sety(Dc1Convert::TextToDouble(parent->getAttribute(X("y"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("z")))
	{
		// deserialize value type
		this->Setz(Dc1Convert::TextToDouble(parent->getAttribute(X("z"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("t")))
	{
		// deserialize value type
		this->Sett(Dc1Convert::TextToDouble(parent->getAttribute(X("t"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsSignalPlaneType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSignalPlaneFractionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSignalPlaneFractionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOrigin() != Dc1NodePtr())
		result.Insert(GetOrigin());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSignalPlaneFractionType_ExtMethodImpl.h


