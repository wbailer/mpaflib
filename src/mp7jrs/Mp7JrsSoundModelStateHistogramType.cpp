
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtImplInclude.h


#include "Mp7JrsAudioDType.h"
#include "Mp7JrsSoundModelStateHistogramType_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsSoundModelStateHistogramType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSoundModelStateHistogramType_LocalType.h" // Sequence collection RelativeFrequency
#include "Mp7JrsnonNegativeReal.h" // Sequence collection element RelativeFrequency

#include <assert.h>
IMp7JrsSoundModelStateHistogramType::IMp7JrsSoundModelStateHistogramType()
{

// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtPropInit.h

}

IMp7JrsSoundModelStateHistogramType::~IMp7JrsSoundModelStateHistogramType()
{
// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtPropCleanup.h

}

Mp7JrsSoundModelStateHistogramType::Mp7JrsSoundModelStateHistogramType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSoundModelStateHistogramType::~Mp7JrsSoundModelStateHistogramType()
{
	Cleanup();
}

void Mp7JrsSoundModelStateHistogramType::Init()
{
	// Init base
	m_Base = CreateAudioDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SoundModelStateHistogramType_LocalType = Mp7JrsSoundModelStateHistogramType_CollectionPtr(); // Collection
	m_SoundModelRef = XMLString::transcode(""); //  Mandatory String


// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtMyPropInit.h

}

void Mp7JrsSoundModelStateHistogramType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SoundModelStateHistogramType_LocalType);
	XMLString::release(&this->m_SoundModelRef);
	this->m_SoundModelRef = NULL;
}

void Mp7JrsSoundModelStateHistogramType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SoundModelStateHistogramTypePtr, since we
	// might need GetBase(), which isn't defined in ISoundModelStateHistogramType
	const Dc1Ptr< Mp7JrsSoundModelStateHistogramType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSoundModelStateHistogramType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SoundModelStateHistogramType_LocalType);
		this->SetSoundModelStateHistogramType_LocalType(Dc1Factory::CloneObject(tmp->GetSoundModelStateHistogramType_LocalType()));
		this->SetSoundModelRef(XMLString::replicate(tmp->GetSoundModelRef()));
}

Dc1NodePtr Mp7JrsSoundModelStateHistogramType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSoundModelStateHistogramType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioDType > Mp7JrsSoundModelStateHistogramType::GetBase() const
{
	return m_Base;
}

Mp7JrsSoundModelStateHistogramType_CollectionPtr Mp7JrsSoundModelStateHistogramType::GetSoundModelStateHistogramType_LocalType() const
{
		return m_SoundModelStateHistogramType_LocalType;
}

XMLCh * Mp7JrsSoundModelStateHistogramType::GetSoundModelRef() const
{
		return m_SoundModelRef;
}

void Mp7JrsSoundModelStateHistogramType::SetSoundModelStateHistogramType_LocalType(const Mp7JrsSoundModelStateHistogramType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelStateHistogramType::SetSoundModelStateHistogramType_LocalType().");
	}
	if (m_SoundModelStateHistogramType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SoundModelStateHistogramType_LocalType);
		m_SoundModelStateHistogramType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SoundModelStateHistogramType_LocalType) m_SoundModelStateHistogramType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelStateHistogramType::SetSoundModelRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelStateHistogramType::SetSoundModelRef().");
	}
	if (m_SoundModelRef != item)
	{
		XMLString::release(&m_SoundModelRef);
		m_SoundModelRef = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsSoundModelStateHistogramType::Getchannels() const
{
	return GetBase()->Getchannels();
}

bool Mp7JrsSoundModelStateHistogramType::Existchannels() const
{
	return GetBase()->Existchannels();
}
void Mp7JrsSoundModelStateHistogramType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSoundModelStateHistogramType::Setchannels().");
	}
	GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSoundModelStateHistogramType::Invalidatechannels()
{
	GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsSoundModelStateHistogramType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("RelativeFrequency")) == 0)
	{
		// RelativeFrequency is contained in itemtype SoundModelStateHistogramType_LocalType
		// in sequence collection SoundModelStateHistogramType_CollectionType

		context->Found = true;
		Mp7JrsSoundModelStateHistogramType_CollectionPtr coll = GetSoundModelStateHistogramType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSoundModelStateHistogramType_CollectionType; // FTT, check this
				SetSoundModelStateHistogramType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(i))->GetRelativeFrequency()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsSoundModelStateHistogramType_LocalPtr item = CreateSoundModelStateHistogramType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(j))->SetRelativeFrequency(
						((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(j+1))->GetRelativeFrequency());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(j))->SetRelativeFrequency(
						((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(j-1))->GetRelativeFrequency());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:nonNegativeReal")))) != empty)
			{
				// Is type allowed
				Mp7JrsnonNegativeRealPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(i))->SetRelativeFrequency(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsSoundModelStateHistogramType_LocalPtr)coll->elementAt(i))->GetRelativeFrequency()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SoundModelStateHistogramType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SoundModelStateHistogramType");
	}
	return result;
}

XMLCh * Mp7JrsSoundModelStateHistogramType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSoundModelStateHistogramType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSoundModelStateHistogramType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SoundModelStateHistogramType"));
	// Element serialization:
	if (m_SoundModelStateHistogramType_LocalType != Mp7JrsSoundModelStateHistogramType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SoundModelStateHistogramType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	 // String
	if(m_SoundModelRef != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_SoundModelRef, X("urn:mpeg:mpeg7:schema:2004"), X("SoundModelRef"), false);

}

bool Mp7JrsSoundModelStateHistogramType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAudioDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StateRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("StateRef"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:RelativeFrequency")) == 0)
			(Dc1Util::HasNodeName(parent, X("RelativeFrequency"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsSoundModelStateHistogramType_CollectionPtr tmp = CreateSoundModelStateHistogramType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSoundModelStateHistogramType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("SoundModelRef"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetSoundModelRef(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetSoundModelRef(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSoundModelStateHistogramType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsSoundModelStateHistogramType_CollectionPtr tmp = CreateSoundModelStateHistogramType_CollectionType; // FTT, check this
	this->SetSoundModelStateHistogramType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSoundModelStateHistogramType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSoundModelStateHistogramType_LocalType() != Dc1NodePtr())
		result.Insert(GetSoundModelStateHistogramType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSoundModelStateHistogramType_ExtMethodImpl.h


