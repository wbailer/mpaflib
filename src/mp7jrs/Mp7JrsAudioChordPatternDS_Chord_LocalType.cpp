
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtImplInclude.h


#include "Mp7JrsChordBaseType.h"
#include "Mp7JrsAudioSegmentType.h"
#include "Mp7JrsAudioChordPatternDS_Chord_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsAudioChordPatternDS_Chord_LocalType::IMp7JrsAudioChordPatternDS_Chord_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtPropInit.h

}

IMp7JrsAudioChordPatternDS_Chord_LocalType::~IMp7JrsAudioChordPatternDS_Chord_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioChordPatternDS_Chord_LocalType::Mp7JrsAudioChordPatternDS_Chord_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioChordPatternDS_Chord_LocalType::~Mp7JrsAudioChordPatternDS_Chord_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioChordPatternDS_Chord_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_ChordBase = Mp7JrsChordBasePtr(); // Class
	m_AudioSegment = Mp7JrsAudioSegmentPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioChordPatternDS_Chord_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_ChordBase);
	// Dc1Factory::DeleteObject(m_AudioSegment);
}

void Mp7JrsAudioChordPatternDS_Chord_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioChordPatternDS_Chord_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioChordPatternDS_Chord_LocalType
	const Dc1Ptr< Mp7JrsAudioChordPatternDS_Chord_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_ChordBase);
		this->SetChordBase(Dc1Factory::CloneObject(tmp->GetChordBase()));
		// Dc1Factory::DeleteObject(m_AudioSegment);
		this->SetAudioSegment(Dc1Factory::CloneObject(tmp->GetAudioSegment()));
}

Dc1NodePtr Mp7JrsAudioChordPatternDS_Chord_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAudioChordPatternDS_Chord_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsChordBasePtr Mp7JrsAudioChordPatternDS_Chord_LocalType::GetChordBase() const
{
		return m_ChordBase;
}

Mp7JrsAudioSegmentPtr Mp7JrsAudioChordPatternDS_Chord_LocalType::GetAudioSegment() const
{
		return m_AudioSegment;
}

void Mp7JrsAudioChordPatternDS_Chord_LocalType::SetChordBase(const Mp7JrsChordBasePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioChordPatternDS_Chord_LocalType::SetChordBase().");
	}
	if (m_ChordBase != item)
	{
		// Dc1Factory::DeleteObject(m_ChordBase);
		m_ChordBase = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ChordBase) m_ChordBase->SetParent(m_myself.getPointer());
		if (m_ChordBase && m_ChordBase->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ChordBase->UseTypeAttribute = true;
		}
		if(m_ChordBase != Mp7JrsChordBasePtr())
		{
			m_ChordBase->SetContentName(XMLString::transcode("ChordBase"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioChordPatternDS_Chord_LocalType::SetAudioSegment(const Mp7JrsAudioSegmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioChordPatternDS_Chord_LocalType::SetAudioSegment().");
	}
	if (m_AudioSegment != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegment);
		m_AudioSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegment) m_AudioSegment->SetParent(m_myself.getPointer());
		if (m_AudioSegment && m_AudioSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSegment->UseTypeAttribute = true;
		}
		if(m_AudioSegment != Mp7JrsAudioSegmentPtr())
		{
			m_AudioSegment->SetContentName(XMLString::transcode("AudioSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAudioChordPatternDS_Chord_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ChordBase")) == 0)
	{
		// ChordBase is simple element ChordBaseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetChordBase()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ChordBaseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsChordBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetChordBase(p, client);
					if((p = GetChordBase()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSegment")) == 0)
	{
		// AudioSegment is simple element AudioSegmentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioSegment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioSegment(p, client);
					if((p = GetAudioSegment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioChordPatternDS_Chord_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioChordPatternDS_Chord_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsAudioChordPatternDS_Chord_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioChordPatternDS_Chord_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioChordPatternDS_Chord_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioChordPatternDS_Chord_LocalType"));
	// Element serialization:
	// Class
	
	if (m_ChordBase != Mp7JrsChordBasePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ChordBase->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ChordBase"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ChordBase->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSegment != Mp7JrsAudioSegmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSegment->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioChordPatternDS_Chord_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ChordBase"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ChordBase")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateChordBaseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetChordBase(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioSegmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioChordPatternDS_Chord_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("ChordBase")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateChordBaseType; // FTT, check this
	}
	this->SetChordBase(child);
  }
  if (XMLString::compareString(elementname, X("AudioSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioSegmentType; // FTT, check this
	}
	this->SetAudioSegment(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioChordPatternDS_Chord_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetChordBase() != Dc1NodePtr())
		result.Insert(GetChordBase());
	if (GetAudioSegment() != Dc1NodePtr())
		result.Insert(GetAudioSegment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioChordPatternDS_Chord_LocalType_ExtMethodImpl.h


