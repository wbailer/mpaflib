
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrstimePointType_ExtImplInclude.h


#include "Mp7JrstimePointType.h"

Mp7JrstimePointType::Mp7JrstimePointType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrstimePointType::~Mp7JrstimePointType()
{
		Cleanup();
}

void Mp7JrstimePointType::Init()
{	   
		m_TimepointSign = 1;
		m_Year = -1;
		m_Month = -1;
		m_Day = -1;
		m_Date_Exist = false;
		m_Hours = -1;
		m_Minutes = -1;
		m_Seconds = -1;
		m_NumberOfFractions = -1;
		m_Time_Exist = false;
		m_FractionsPerSecond = -1;
		m_TimezoneSign = 1;
		m_TimezoneHours = -1;
		m_TimezoneMinutes = -1;
		m_Timezone_Exist = false;

// no includefile for extension defined 
// file Mp7JrstimePointType_ExtPropInit.h

}

void Mp7JrstimePointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrstimePointType_ExtPropCleanup.h


}

void Mp7JrstimePointType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrstimePointType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrstimePointType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrstimePointType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrstimePointType::GetTimepointSign() const
{
		return m_TimepointSign;
}

void Mp7JrstimePointType::SetTimepointSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTimepointSign().");
		}
		m_TimepointSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetYear() const
{
		return m_Year;
}

void Mp7JrstimePointType::SetYear(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetYear().");
		}
		m_Year = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetMonth() const
{
		return m_Month;
}

void Mp7JrstimePointType::SetMonth(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetMonth().");
		}
		m_Month = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetDay() const
{
		return m_Day;
}

void Mp7JrstimePointType::SetDay(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetDay().");
		}
		m_Day = val;
		m_Date_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrstimePointType::GetDate_Exist() const
{
		return m_Date_Exist;
}

void Mp7JrstimePointType::SetDate_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetDate_Exist().");
		}
		m_Date_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetHours() const
{
		return m_Hours;
}

void Mp7JrstimePointType::SetHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetHours().");
		}
		m_Hours = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetMinutes() const
{
		return m_Minutes;
}

void Mp7JrstimePointType::SetMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetMinutes().");
		}
		m_Minutes = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetSeconds() const
{
		return m_Seconds;
}

void Mp7JrstimePointType::SetSeconds(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetSeconds().");
		}
		m_Seconds = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetNumberOfFractions() const
{
		return m_NumberOfFractions;
}

void Mp7JrstimePointType::SetNumberOfFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetNumberOfFractions().");
		}
		m_NumberOfFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrstimePointType::GetTime_Exist() const
{
		return m_Time_Exist;
}

void Mp7JrstimePointType::SetTime_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTime_Exist().");
		}
		m_Time_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetFractionsPerSecond() const
{
		return m_FractionsPerSecond;
}

void Mp7JrstimePointType::SetFractionsPerSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetFractionsPerSecond().");
		}
		m_FractionsPerSecond = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetTimezoneSign() const
{
		return m_TimezoneSign;
}

void Mp7JrstimePointType::SetTimezoneSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTimezoneSign().");
		}
		m_TimezoneSign = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetTimezoneHours() const
{
		return m_TimezoneHours;
}

void Mp7JrstimePointType::SetTimezoneHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTimezoneHours().");
		}
		m_TimezoneHours = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrstimePointType::GetTimezoneMinutes() const
{
		return m_TimezoneMinutes;
}

void Mp7JrstimePointType::SetTimezoneMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTimezoneMinutes().");
		}
		m_TimezoneMinutes = val;
		m_Timezone_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrstimePointType::GetTimezone_Exist() const
{
		return m_Timezone_Exist;
}

void Mp7JrstimePointType::SetTimezone_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrstimePointType::SetTimezone_Exist().");
		}
		m_Timezone_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrstimePointType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if(m_TimepointSign < 0)
		{
			Dc1Util::CatString(&buf, X("-"));
		}

		if(m_Date_Exist)
		{
			if(m_Year >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Year);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Month >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Month);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Day >= 0)
			{
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Day);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, X("-"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_Time_Exist)
		{
			if(m_Hours >= 0)
			{
				Dc1Util::CatString(&buf, X("T"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Hours);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Minutes >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Minutes);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_Seconds >= 0)
			{
				Dc1Util::CatString(&buf, X(":"));
				XMLCh* tmp1 = Dc1Convert::BinToText(m_Seconds);
				tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}

			if(m_NumberOfFractions >= 0)
			{
				tmp = Dc1Convert::BinToText(m_NumberOfFractions);
				Dc1Util::CatString(&buf, X(":"));
				Dc1Util::CatString(&buf, tmp);
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			}
		}

		if(m_FractionsPerSecond >= 0)
		{
			tmp = Dc1Convert::BinToText(m_FractionsPerSecond);
			Dc1Util::CatString(&buf, X("F"));
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}

		if(m_Timezone_Exist)
		{
			if(m_TimezoneSign > 0)
			{
				Dc1Util::CatString(&buf, X("+"));
			}
			else if(m_TimezoneSign < 0)
			{
				Dc1Util::CatString(&buf, X("-"));
			}

			XMLCh* tmp1 = Dc1Convert::BinToText(m_TimezoneHours);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			Dc1Util::CatString(&buf, X(":"));
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
			tmp1 = Dc1Convert::BinToText(m_TimezoneMinutes);
			tmp = Dc1Util::PadLeft(tmp1, X("0"), 2);
			Dc1Util::CatString(&buf, tmp);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp1);
			XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrstimePointType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^(\\-?\\d+(\\-\\d{2}(\\-\\d{2})?)?)?(T\\d{2}(:\\d{2}(:\\d{2}(:\\d+)?)?)?)?(F\\d+)?((\\-|\\+)\\d{2}:\\d{2})?$");
		// This is for Xerces 3.1.1+
		if(r.matches(txt, &m))
		{
				for(int i = 1; i < m.getNoGroups(); i++)
				{
						int i1 = m.getStartPos(i);
						int i2 = Dc1Util::GetNextEndPos(m, i);
						if(i1 == i2) continue; // ignore missing optional or wrong group
						switch(i)
						{
								case 0:
												// Nothing to do here...
								break;

								case 1:
										if(i1 >= 0)
										{
												// (\\d+...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2);
												m_TimepointSign = Dc1Convert::TextToSign(substring);
												if(m_TimepointSign < 0)
												{
														XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												}
												m_Year = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 2:
										if(i1 >= 0)
										{
												// (\\-\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Month = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 3:
										if(i1 >= 0)
										{
												// (\\-\\d{2})?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Day = Dc1Convert::TextToUnsignedInt(substring);
												m_Date_Exist = true;
										}
								break;

								case 4:
										if(i1 >= 0)
										{
												// (T\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Hours = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 5:
										if(i1 >= 0)
										{
												// (:\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Minutes = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 6:
										if(i1 >= 0)
										{
												// (:\\d{2}...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_Seconds = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 7:
										if(i1 >= 0)
										{
												// (:\\d+...)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_NumberOfFractions = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 8:
										if(i1 >= 0)
										{
												// (F\\d+)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i2);
												m_FractionsPerSecond = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								case 9:
										if(i1 >= 0)
										{
												// ((\\-|\\+)\\d{2}:\\d{2})?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i1 + 1);
												m_TimezoneSign = Dc1Convert::TextToSign(substring);

												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 1, i1 + 3);
												m_TimezoneHours = Dc1Convert::TextToUnsignedInt(substring);

												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1 + 4, i2);
												m_TimezoneMinutes = Dc1Convert::TextToUnsignedInt(substring);

												m_Timezone_Exist = true;
										}
								break;

								default: return true;
								break;
						}
				}
		}
		else return false;
		return true;
}

bool Mp7JrstimePointType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrstimePointType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrstimePointType::ContentToString() const
{
	// Mp7JrstimePointType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrstimePointType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:timePointType")))));
		}

// no includefile for extension defined 
// file Mp7JrstimePointType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool Mp7JrstimePointType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrstimePointType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// begin extension included
// file Mp7JrstimePointType_ExtMethodImpl.h
#include "Mp7JrsdurationType.h"

void IMp7JrstimePointType::Add(Mp7JrsdurationPtr duration, Dc1ClientID client) {
	long long nFractions = (long long)(GetTotalNrFractions()+(duration->GetTotalNrFractions()*1.0/duration->GetFractionsPerSecond())*GetFractionsPerSecond());
	SetTotalNrFractions(nFractions,client);
}

Mp7JrsdurationPtr IMp7JrstimePointType::Subtract(Mp7JrstimePointPtr timePoint, Dc1ClientID client) {
	long long nFractions = (long long)(GetTotalNrFractions() - (timePoint->GetTotalNrFractions()*1.0 / timePoint->GetFractionsPerSecond())*GetFractionsPerSecond());
	Mp7JrsdurationPtr duration = CreatedurationType;
	duration->SetFractionsPerSecond(GetFractionsPerSecond(),client);
	duration->SetTotalNrFractions(nFractions,client);
	return duration;
}

long long IMp7JrstimePointType::GetTotalNrFractions() {
	long long  sum = 0LL;
	// days and months missing!!!!!
	sum = 60LL * (GetHours() + sum);
	sum = 60LL * (GetMinutes() + sum);
	sum = GetFractionsPerSecond() * (GetSeconds() + sum);
	sum += GetNumberOfFractions(); 
	return sum;
}

void IMp7JrstimePointType::SetTotalNrFractions(long long fractions, Dc1ClientID client) {
	long long remainder = fractions;
	SetNumberOfFractions((int)(remainder % GetFractionsPerSecond()),client);
	remainder /= GetFractionsPerSecond();
	SetSeconds ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetMinutes ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetHours ((int)remainder,client);
	// days and months ignored!!!!
}

// end extension included








