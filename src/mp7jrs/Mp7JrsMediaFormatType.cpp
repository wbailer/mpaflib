
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsMediaFormatType_BitRate_LocalType.h"
#include "Mp7JrsMediaFormatType_ScalableCoding_LocalType2.h"
#include "Mp7JrsMediaFormatType_VisualCoding_LocalType.h"
#include "Mp7JrsMediaFormatType_AudioCoding_LocalType.h"
#include "Mp7JrsMediaFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaFormatType::IMp7JrsMediaFormatType()
{

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtPropInit.h

}

IMp7JrsMediaFormatType::~IMp7JrsMediaFormatType()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtPropCleanup.h

}

Mp7JrsMediaFormatType::Mp7JrsMediaFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaFormatType::~Mp7JrsMediaFormatType()
{
	Cleanup();
}

void Mp7JrsMediaFormatType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Content = Mp7JrsControlledTermUsePtr(); // Class
	m_Medium = Mp7JrsControlledTermUsePtr(); // Class
	m_Medium_Exist = false;
	m_FileFormat = Mp7JrsControlledTermUsePtr(); // Class
	m_FileFormat_Exist = false;
	m_FileSize = (0); // Value

	m_FileSize_Exist = false;
	m_System = Mp7JrsControlledTermUsePtr(); // Class
	m_System_Exist = false;
	m_Bandwidth = (0.0f); // Value

	m_Bandwidth_Exist = false;
	m_BitRate = Mp7JrsMediaFormatType_BitRate_LocalPtr(); // Class with content 
	m_BitRate_Exist = false;
	m_TargetChannelBitRate = (0); // Value

	m_TargetChannelBitRate_Exist = false;
	m_ScalableCoding = Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr(); // Class
	m_ScalableCoding_Exist = false;
	m_VisualCoding = Mp7JrsMediaFormatType_VisualCoding_LocalPtr(); // Class
	m_VisualCoding_Exist = false;
	m_AudioCoding = Mp7JrsMediaFormatType_AudioCoding_LocalPtr(); // Class
	m_AudioCoding_Exist = false;
	m_SceneCodingFormat = Mp7JrsControlledTermUsePtr(); // Class
	m_SceneCodingFormat_Exist = false;
	m_GraphicsCodingFormat = Mp7JrsControlledTermUsePtr(); // Class
	m_GraphicsCodingFormat_Exist = false;
	m_OtherCodingFormat = Mp7JrsControlledTermUsePtr(); // Class
	m_OtherCodingFormat_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMyPropInit.h

}

void Mp7JrsMediaFormatType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Content);
	// Dc1Factory::DeleteObject(m_Medium);
	// Dc1Factory::DeleteObject(m_FileFormat);
	// Dc1Factory::DeleteObject(m_System);
	// Dc1Factory::DeleteObject(m_BitRate);
	// Dc1Factory::DeleteObject(m_ScalableCoding);
	// Dc1Factory::DeleteObject(m_VisualCoding);
	// Dc1Factory::DeleteObject(m_AudioCoding);
	// Dc1Factory::DeleteObject(m_SceneCodingFormat);
	// Dc1Factory::DeleteObject(m_GraphicsCodingFormat);
	// Dc1Factory::DeleteObject(m_OtherCodingFormat);
}

void Mp7JrsMediaFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaFormatType
	const Dc1Ptr< Mp7JrsMediaFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaFormatType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Content);
		this->SetContent(Dc1Factory::CloneObject(tmp->GetContent()));
	if (tmp->IsValidMedium())
	{
		// Dc1Factory::DeleteObject(m_Medium);
		this->SetMedium(Dc1Factory::CloneObject(tmp->GetMedium()));
	}
	else
	{
		InvalidateMedium();
	}
	if (tmp->IsValidFileFormat())
	{
		// Dc1Factory::DeleteObject(m_FileFormat);
		this->SetFileFormat(Dc1Factory::CloneObject(tmp->GetFileFormat()));
	}
	else
	{
		InvalidateFileFormat();
	}
	if (tmp->IsValidFileSize())
	{
		this->SetFileSize(tmp->GetFileSize());
	}
	else
	{
		InvalidateFileSize();
	}
	if (tmp->IsValidSystem())
	{
		// Dc1Factory::DeleteObject(m_System);
		this->SetSystem(Dc1Factory::CloneObject(tmp->GetSystem()));
	}
	else
	{
		InvalidateSystem();
	}
	if (tmp->IsValidBandwidth())
	{
		this->SetBandwidth(tmp->GetBandwidth());
	}
	else
	{
		InvalidateBandwidth();
	}
	if (tmp->IsValidBitRate())
	{
		// Dc1Factory::DeleteObject(m_BitRate);
		this->SetBitRate(Dc1Factory::CloneObject(tmp->GetBitRate()));
	}
	else
	{
		InvalidateBitRate();
	}
	if (tmp->IsValidTargetChannelBitRate())
	{
		this->SetTargetChannelBitRate(tmp->GetTargetChannelBitRate());
	}
	else
	{
		InvalidateTargetChannelBitRate();
	}
	if (tmp->IsValidScalableCoding())
	{
		// Dc1Factory::DeleteObject(m_ScalableCoding);
		this->SetScalableCoding(Dc1Factory::CloneObject(tmp->GetScalableCoding()));
	}
	else
	{
		InvalidateScalableCoding();
	}
	if (tmp->IsValidVisualCoding())
	{
		// Dc1Factory::DeleteObject(m_VisualCoding);
		this->SetVisualCoding(Dc1Factory::CloneObject(tmp->GetVisualCoding()));
	}
	else
	{
		InvalidateVisualCoding();
	}
	if (tmp->IsValidAudioCoding())
	{
		// Dc1Factory::DeleteObject(m_AudioCoding);
		this->SetAudioCoding(Dc1Factory::CloneObject(tmp->GetAudioCoding()));
	}
	else
	{
		InvalidateAudioCoding();
	}
	if (tmp->IsValidSceneCodingFormat())
	{
		// Dc1Factory::DeleteObject(m_SceneCodingFormat);
		this->SetSceneCodingFormat(Dc1Factory::CloneObject(tmp->GetSceneCodingFormat()));
	}
	else
	{
		InvalidateSceneCodingFormat();
	}
	if (tmp->IsValidGraphicsCodingFormat())
	{
		// Dc1Factory::DeleteObject(m_GraphicsCodingFormat);
		this->SetGraphicsCodingFormat(Dc1Factory::CloneObject(tmp->GetGraphicsCodingFormat()));
	}
	else
	{
		InvalidateGraphicsCodingFormat();
	}
	if (tmp->IsValidOtherCodingFormat())
	{
		// Dc1Factory::DeleteObject(m_OtherCodingFormat);
		this->SetOtherCodingFormat(Dc1Factory::CloneObject(tmp->GetOtherCodingFormat()));
	}
	else
	{
		InvalidateOtherCodingFormat();
	}
}

Dc1NodePtr Mp7JrsMediaFormatType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaFormatType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsMediaFormatType::GetBase() const
{
	return m_Base;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetContent() const
{
		return m_Content;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetMedium() const
{
		return m_Medium;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidMedium() const
{
	return m_Medium_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetFileFormat() const
{
		return m_FileFormat;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidFileFormat() const
{
	return m_FileFormat_Exist;
}

unsigned Mp7JrsMediaFormatType::GetFileSize() const
{
		return m_FileSize;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidFileSize() const
{
	return m_FileSize_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetSystem() const
{
		return m_System;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidSystem() const
{
	return m_System_Exist;
}

float Mp7JrsMediaFormatType::GetBandwidth() const
{
		return m_Bandwidth;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidBandwidth() const
{
	return m_Bandwidth_Exist;
}

Mp7JrsMediaFormatType_BitRate_LocalPtr Mp7JrsMediaFormatType::GetBitRate() const
{
		return m_BitRate;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidBitRate() const
{
	return m_BitRate_Exist;
}

unsigned Mp7JrsMediaFormatType::GetTargetChannelBitRate() const
{
		return m_TargetChannelBitRate;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidTargetChannelBitRate() const
{
	return m_TargetChannelBitRate_Exist;
}

Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr Mp7JrsMediaFormatType::GetScalableCoding() const
{
		return m_ScalableCoding;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidScalableCoding() const
{
	return m_ScalableCoding_Exist;
}

Mp7JrsMediaFormatType_VisualCoding_LocalPtr Mp7JrsMediaFormatType::GetVisualCoding() const
{
		return m_VisualCoding;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidVisualCoding() const
{
	return m_VisualCoding_Exist;
}

Mp7JrsMediaFormatType_AudioCoding_LocalPtr Mp7JrsMediaFormatType::GetAudioCoding() const
{
		return m_AudioCoding;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidAudioCoding() const
{
	return m_AudioCoding_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetSceneCodingFormat() const
{
		return m_SceneCodingFormat;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidSceneCodingFormat() const
{
	return m_SceneCodingFormat_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetGraphicsCodingFormat() const
{
		return m_GraphicsCodingFormat;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidGraphicsCodingFormat() const
{
	return m_GraphicsCodingFormat_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType::GetOtherCodingFormat() const
{
		return m_OtherCodingFormat;
}

// Element is optional
bool Mp7JrsMediaFormatType::IsValidOtherCodingFormat() const
{
	return m_OtherCodingFormat_Exist;
}

void Mp7JrsMediaFormatType::SetContent(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetContent().");
	}
	if (m_Content != item)
	{
		// Dc1Factory::DeleteObject(m_Content);
		m_Content = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Content) m_Content->SetParent(m_myself.getPointer());
		if (m_Content && m_Content->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Content->UseTypeAttribute = true;
		}
		if(m_Content != Mp7JrsControlledTermUsePtr())
		{
			m_Content->SetContentName(XMLString::transcode("Content"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::SetMedium(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetMedium().");
	}
	if (m_Medium != item || m_Medium_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Medium);
		m_Medium = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Medium) m_Medium->SetParent(m_myself.getPointer());
		if (m_Medium && m_Medium->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Medium->UseTypeAttribute = true;
		}
		m_Medium_Exist = true;
		if(m_Medium != Mp7JrsControlledTermUsePtr())
		{
			m_Medium->SetContentName(XMLString::transcode("Medium"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateMedium()
{
	m_Medium_Exist = false;
}
void Mp7JrsMediaFormatType::SetFileFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetFileFormat().");
	}
	if (m_FileFormat != item || m_FileFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FileFormat);
		m_FileFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FileFormat) m_FileFormat->SetParent(m_myself.getPointer());
		if (m_FileFormat && m_FileFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FileFormat->UseTypeAttribute = true;
		}
		m_FileFormat_Exist = true;
		if(m_FileFormat != Mp7JrsControlledTermUsePtr())
		{
			m_FileFormat->SetContentName(XMLString::transcode("FileFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateFileFormat()
{
	m_FileFormat_Exist = false;
}
void Mp7JrsMediaFormatType::SetFileSize(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetFileSize().");
	}
	if (m_FileSize != item || m_FileSize_Exist == false)
	{
		m_FileSize = item;
		m_FileSize_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateFileSize()
{
	m_FileSize_Exist = false;
}
void Mp7JrsMediaFormatType::SetSystem(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetSystem().");
	}
	if (m_System != item || m_System_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_System);
		m_System = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_System) m_System->SetParent(m_myself.getPointer());
		if (m_System && m_System->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_System->UseTypeAttribute = true;
		}
		m_System_Exist = true;
		if(m_System != Mp7JrsControlledTermUsePtr())
		{
			m_System->SetContentName(XMLString::transcode("System"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateSystem()
{
	m_System_Exist = false;
}
void Mp7JrsMediaFormatType::SetBandwidth(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetBandwidth().");
	}
	if (m_Bandwidth != item || m_Bandwidth_Exist == false)
	{
		m_Bandwidth = item;
		m_Bandwidth_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateBandwidth()
{
	m_Bandwidth_Exist = false;
}
void Mp7JrsMediaFormatType::SetBitRate(const Mp7JrsMediaFormatType_BitRate_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetBitRate().");
	}
	if (m_BitRate != item || m_BitRate_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BitRate);
		m_BitRate = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BitRate) m_BitRate->SetParent(m_myself.getPointer());
		if (m_BitRate && m_BitRate->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_BitRate_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BitRate->UseTypeAttribute = true;
		}
		m_BitRate_Exist = true;
		if(m_BitRate != Mp7JrsMediaFormatType_BitRate_LocalPtr())
		{
			m_BitRate->SetContentName(XMLString::transcode("BitRate"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateBitRate()
{
	m_BitRate_Exist = false;
}
void Mp7JrsMediaFormatType::SetTargetChannelBitRate(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetTargetChannelBitRate().");
	}
	if (m_TargetChannelBitRate != item || m_TargetChannelBitRate_Exist == false)
	{
		m_TargetChannelBitRate = item;
		m_TargetChannelBitRate_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateTargetChannelBitRate()
{
	m_TargetChannelBitRate_Exist = false;
}
void Mp7JrsMediaFormatType::SetScalableCoding(const Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetScalableCoding().");
	}
	if (m_ScalableCoding != item || m_ScalableCoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ScalableCoding);
		m_ScalableCoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ScalableCoding) m_ScalableCoding->SetParent(m_myself.getPointer());
		if (m_ScalableCoding && m_ScalableCoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType2"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ScalableCoding->UseTypeAttribute = true;
		}
		m_ScalableCoding_Exist = true;
		if(m_ScalableCoding != Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr())
		{
			m_ScalableCoding->SetContentName(XMLString::transcode("ScalableCoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateScalableCoding()
{
	m_ScalableCoding_Exist = false;
}
void Mp7JrsMediaFormatType::SetVisualCoding(const Mp7JrsMediaFormatType_VisualCoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetVisualCoding().");
	}
	if (m_VisualCoding != item || m_VisualCoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VisualCoding);
		m_VisualCoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualCoding) m_VisualCoding->SetParent(m_myself.getPointer());
		if (m_VisualCoding && m_VisualCoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VisualCoding->UseTypeAttribute = true;
		}
		m_VisualCoding_Exist = true;
		if(m_VisualCoding != Mp7JrsMediaFormatType_VisualCoding_LocalPtr())
		{
			m_VisualCoding->SetContentName(XMLString::transcode("VisualCoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateVisualCoding()
{
	m_VisualCoding_Exist = false;
}
void Mp7JrsMediaFormatType::SetAudioCoding(const Mp7JrsMediaFormatType_AudioCoding_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetAudioCoding().");
	}
	if (m_AudioCoding != item || m_AudioCoding_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioCoding);
		m_AudioCoding = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioCoding) m_AudioCoding->SetParent(m_myself.getPointer());
		if (m_AudioCoding && m_AudioCoding->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioCoding->UseTypeAttribute = true;
		}
		m_AudioCoding_Exist = true;
		if(m_AudioCoding != Mp7JrsMediaFormatType_AudioCoding_LocalPtr())
		{
			m_AudioCoding->SetContentName(XMLString::transcode("AudioCoding"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateAudioCoding()
{
	m_AudioCoding_Exist = false;
}
void Mp7JrsMediaFormatType::SetSceneCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetSceneCodingFormat().");
	}
	if (m_SceneCodingFormat != item || m_SceneCodingFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SceneCodingFormat);
		m_SceneCodingFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SceneCodingFormat) m_SceneCodingFormat->SetParent(m_myself.getPointer());
		if (m_SceneCodingFormat && m_SceneCodingFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SceneCodingFormat->UseTypeAttribute = true;
		}
		m_SceneCodingFormat_Exist = true;
		if(m_SceneCodingFormat != Mp7JrsControlledTermUsePtr())
		{
			m_SceneCodingFormat->SetContentName(XMLString::transcode("SceneCodingFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateSceneCodingFormat()
{
	m_SceneCodingFormat_Exist = false;
}
void Mp7JrsMediaFormatType::SetGraphicsCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetGraphicsCodingFormat().");
	}
	if (m_GraphicsCodingFormat != item || m_GraphicsCodingFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_GraphicsCodingFormat);
		m_GraphicsCodingFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GraphicsCodingFormat) m_GraphicsCodingFormat->SetParent(m_myself.getPointer());
		if (m_GraphicsCodingFormat && m_GraphicsCodingFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GraphicsCodingFormat->UseTypeAttribute = true;
		}
		m_GraphicsCodingFormat_Exist = true;
		if(m_GraphicsCodingFormat != Mp7JrsControlledTermUsePtr())
		{
			m_GraphicsCodingFormat->SetContentName(XMLString::transcode("GraphicsCodingFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateGraphicsCodingFormat()
{
	m_GraphicsCodingFormat_Exist = false;
}
void Mp7JrsMediaFormatType::SetOtherCodingFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType::SetOtherCodingFormat().");
	}
	if (m_OtherCodingFormat != item || m_OtherCodingFormat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_OtherCodingFormat);
		m_OtherCodingFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_OtherCodingFormat) m_OtherCodingFormat->SetParent(m_myself.getPointer());
		if (m_OtherCodingFormat && m_OtherCodingFormat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_OtherCodingFormat->UseTypeAttribute = true;
		}
		m_OtherCodingFormat_Exist = true;
		if(m_OtherCodingFormat != Mp7JrsControlledTermUsePtr())
		{
			m_OtherCodingFormat->SetContentName(XMLString::transcode("OtherCodingFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType::InvalidateOtherCodingFormat()
{
	m_OtherCodingFormat_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Content")) == 0)
	{
		// Content is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContent()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContent(p, client);
					if((p = GetContent()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Medium")) == 0)
	{
		// Medium is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMedium()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMedium(p, client);
					if((p = GetMedium()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FileFormat")) == 0)
	{
		// FileFormat is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFileFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFileFormat(p, client);
					if((p = GetFileFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("System")) == 0)
	{
		// System is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSystem()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSystem(p, client);
					if((p = GetSystem()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BitRate")) == 0)
	{
		// BitRate is simple element MediaFormatType_BitRate_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBitRate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_BitRate_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_BitRate_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBitRate(p, client);
					if((p = GetBitRate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ScalableCoding")) == 0)
	{
		// ScalableCoding is simple element MediaFormatType_ScalableCoding_LocalType2
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetScalableCoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_ScalableCoding_LocalType2")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetScalableCoding(p, client);
					if((p = GetScalableCoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VisualCoding")) == 0)
	{
		// VisualCoding is simple element MediaFormatType_VisualCoding_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVisualCoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_VisualCoding_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_VisualCoding_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVisualCoding(p, client);
					if((p = GetVisualCoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioCoding")) == 0)
	{
		// AudioCoding is simple element MediaFormatType_AudioCoding_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioCoding()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_AudioCoding_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioCoding(p, client);
					if((p = GetAudioCoding()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SceneCodingFormat")) == 0)
	{
		// SceneCodingFormat is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSceneCodingFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSceneCodingFormat(p, client);
					if((p = GetSceneCodingFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GraphicsCodingFormat")) == 0)
	{
		// GraphicsCodingFormat is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGraphicsCodingFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGraphicsCodingFormat(p, client);
					if((p = GetGraphicsCodingFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("OtherCodingFormat")) == 0)
	{
		// OtherCodingFormat is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetOtherCodingFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetOtherCodingFormat(p, client);
					if((p = GetOtherCodingFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaFormatType");
	}
	return result;
}

XMLCh * Mp7JrsMediaFormatType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMediaFormatType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMediaFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormatType"));
	// Element serialization:
	// Class
	
	if (m_Content != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Content->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Content"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Content->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Medium != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Medium->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Medium"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Medium->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_FileFormat != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FileFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FileFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FileFormat->Serialize(doc, element, &elem);
	}
	if(m_FileSize_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_FileSize);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("FileSize"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_System != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_System->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("System"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_System->Serialize(doc, element, &elem);
	}
	if(m_Bandwidth_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Bandwidth);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Bandwidth"), true);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_BitRate != Mp7JrsMediaFormatType_BitRate_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BitRate->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BitRate"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BitRate->Serialize(doc, element, &elem);
	}
	if(m_TargetChannelBitRate_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_TargetChannelBitRate);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("TargetChannelBitRate"), true);
		XMLString::release(&tmp);
	}
	// Class
	
	if (m_ScalableCoding != Mp7JrsMediaFormatType_ScalableCoding_Local2Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ScalableCoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ScalableCoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ScalableCoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VisualCoding != Mp7JrsMediaFormatType_VisualCoding_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualCoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualCoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VisualCoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioCoding != Mp7JrsMediaFormatType_AudioCoding_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioCoding->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioCoding"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioCoding->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SceneCodingFormat != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SceneCodingFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SceneCodingFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SceneCodingFormat->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GraphicsCodingFormat != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GraphicsCodingFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GraphicsCodingFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GraphicsCodingFormat->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_OtherCodingFormat != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_OtherCodingFormat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("OtherCodingFormat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_OtherCodingFormat->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Content"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Content")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContent(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Medium"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Medium")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMedium(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FileFormat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FileFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFileFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("FileSize"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetFileSize(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("System"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("System")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSystem(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("Bandwidth"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetBandwidth(Dc1Convert::TextToFloat(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BitRate"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BitRate")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_BitRate_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBitRate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("TargetChannelBitRate"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetTargetChannelBitRate(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ScalableCoding"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ScalableCoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_ScalableCoding_LocalType2; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetScalableCoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualCoding"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualCoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_VisualCoding_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualCoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioCoding"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioCoding")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_AudioCoding_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioCoding(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SceneCodingFormat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SceneCodingFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSceneCodingFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GraphicsCodingFormat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GraphicsCodingFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGraphicsCodingFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("OtherCodingFormat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("OtherCodingFormat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetOtherCodingFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Content")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetContent(child);
  }
  if (XMLString::compareString(elementname, X("Medium")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetMedium(child);
  }
  if (XMLString::compareString(elementname, X("FileFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetFileFormat(child);
  }
  if (XMLString::compareString(elementname, X("System")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetSystem(child);
  }
  if (XMLString::compareString(elementname, X("BitRate")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_BitRate_LocalType; // FTT, check this
	}
	this->SetBitRate(child);
  }
  if (XMLString::compareString(elementname, X("ScalableCoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_ScalableCoding_LocalType2; // FTT, check this
	}
	this->SetScalableCoding(child);
  }
  if (XMLString::compareString(elementname, X("VisualCoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_VisualCoding_LocalType; // FTT, check this
	}
	this->SetVisualCoding(child);
  }
  if (XMLString::compareString(elementname, X("AudioCoding")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_AudioCoding_LocalType; // FTT, check this
	}
	this->SetAudioCoding(child);
  }
  if (XMLString::compareString(elementname, X("SceneCodingFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetSceneCodingFormat(child);
  }
  if (XMLString::compareString(elementname, X("GraphicsCodingFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetGraphicsCodingFormat(child);
  }
  if (XMLString::compareString(elementname, X("OtherCodingFormat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetOtherCodingFormat(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetContent() != Dc1NodePtr())
		result.Insert(GetContent());
	if (GetMedium() != Dc1NodePtr())
		result.Insert(GetMedium());
	if (GetFileFormat() != Dc1NodePtr())
		result.Insert(GetFileFormat());
	if (GetSystem() != Dc1NodePtr())
		result.Insert(GetSystem());
	if (GetBitRate() != Dc1NodePtr())
		result.Insert(GetBitRate());
	if (GetScalableCoding() != Dc1NodePtr())
		result.Insert(GetScalableCoding());
	if (GetVisualCoding() != Dc1NodePtr())
		result.Insert(GetVisualCoding());
	if (GetAudioCoding() != Dc1NodePtr())
		result.Insert(GetAudioCoding());
	if (GetSceneCodingFormat() != Dc1NodePtr())
		result.Insert(GetSceneCodingFormat());
	if (GetGraphicsCodingFormat() != Dc1NodePtr())
		result.Insert(GetGraphicsCodingFormat());
	if (GetOtherCodingFormat() != Dc1NodePtr())
		result.Insert(GetOtherCodingFormat());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_ExtMethodImpl.h


