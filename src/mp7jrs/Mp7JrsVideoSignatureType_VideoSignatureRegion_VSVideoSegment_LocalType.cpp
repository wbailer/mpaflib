
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned32.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0.h"
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:BagOfWords

#include <assert.h>
IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtPropInit.h

}

IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::~IMp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtPropCleanup.h

}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::~Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType()
{
	Cleanup();
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_StartFrameOfSegment = Mp7Jrsunsigned32Ptr(); // Class with content 
	m_EndFrameOfSegment = Mp7Jrsunsigned32Ptr(); // Class with content 
	m_MediaTimeOfSegment = Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr(); // Class
	m_MediaTimeOfSegment_Exist = false;
	m_BagOfWords = Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtMyPropInit.h

}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_StartFrameOfSegment);
	// Dc1Factory::DeleteObject(m_EndFrameOfSegment);
	// Dc1Factory::DeleteObject(m_MediaTimeOfSegment);
	// Dc1Factory::DeleteObject(m_BagOfWords);
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType
	const Dc1Ptr< Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_StartFrameOfSegment);
		this->SetStartFrameOfSegment(Dc1Factory::CloneObject(tmp->GetStartFrameOfSegment()));
		// Dc1Factory::DeleteObject(m_EndFrameOfSegment);
		this->SetEndFrameOfSegment(Dc1Factory::CloneObject(tmp->GetEndFrameOfSegment()));
	if (tmp->IsValidMediaTimeOfSegment())
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfSegment);
		this->SetMediaTimeOfSegment(Dc1Factory::CloneObject(tmp->GetMediaTimeOfSegment()));
	}
	else
	{
		InvalidateMediaTimeOfSegment();
	}
		// Dc1Factory::DeleteObject(m_BagOfWords);
		this->SetBagOfWords(Dc1Factory::CloneObject(tmp->GetBagOfWords()));
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetStartFrameOfSegment() const
{
		return m_StartFrameOfSegment;
}

Mp7Jrsunsigned32Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetEndFrameOfSegment() const
{
		return m_EndFrameOfSegment;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetMediaTimeOfSegment() const
{
		return m_MediaTimeOfSegment;
}

// Element is optional
bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::IsValidMediaTimeOfSegment() const
{
	return m_MediaTimeOfSegment_Exist;
}

Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetBagOfWords() const
{
		return m_BagOfWords;
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetStartFrameOfSegment(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetStartFrameOfSegment().");
	}
	if (m_StartFrameOfSegment != item)
	{
		// Dc1Factory::DeleteObject(m_StartFrameOfSegment);
		m_StartFrameOfSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StartFrameOfSegment) m_StartFrameOfSegment->SetParent(m_myself.getPointer());
		if (m_StartFrameOfSegment && m_StartFrameOfSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StartFrameOfSegment->UseTypeAttribute = true;
		}
		if(m_StartFrameOfSegment != Mp7Jrsunsigned32Ptr())
		{
			m_StartFrameOfSegment->SetContentName(XMLString::transcode("StartFrameOfSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetEndFrameOfSegment(const Mp7Jrsunsigned32Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetEndFrameOfSegment().");
	}
	if (m_EndFrameOfSegment != item)
	{
		// Dc1Factory::DeleteObject(m_EndFrameOfSegment);
		m_EndFrameOfSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EndFrameOfSegment) m_EndFrameOfSegment->SetParent(m_myself.getPointer());
		if (m_EndFrameOfSegment && m_EndFrameOfSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_EndFrameOfSegment->UseTypeAttribute = true;
		}
		if(m_EndFrameOfSegment != Mp7Jrsunsigned32Ptr())
		{
			m_EndFrameOfSegment->SetContentName(XMLString::transcode("EndFrameOfSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetMediaTimeOfSegment(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetMediaTimeOfSegment().");
	}
	if (m_MediaTimeOfSegment != item || m_MediaTimeOfSegment_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MediaTimeOfSegment);
		m_MediaTimeOfSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimeOfSegment) m_MediaTimeOfSegment->SetParent(m_myself.getPointer());
		if (m_MediaTimeOfSegment && m_MediaTimeOfSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimeOfSegment->UseTypeAttribute = true;
		}
		m_MediaTimeOfSegment_Exist = true;
		if(m_MediaTimeOfSegment != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr())
		{
			m_MediaTimeOfSegment->SetContentName(XMLString::transcode("MediaTimeOfSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::InvalidateMediaTimeOfSegment()
{
	m_MediaTimeOfSegment_Exist = false;
}
void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetBagOfWords(const Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::SetBagOfWords().");
	}
	if (m_BagOfWords != item)
	{
		// Dc1Factory::DeleteObject(m_BagOfWords);
		m_BagOfWords = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BagOfWords) m_BagOfWords->SetParent(m_myself.getPointer());
		if(m_BagOfWords != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr())
		{
			m_BagOfWords->SetContentName(XMLString::transcode("BagOfWords"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("StartFrameOfSegment")) == 0)
	{
		// StartFrameOfSegment is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStartFrameOfSegment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStartFrameOfSegment(p, client);
					if((p = GetStartFrameOfSegment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("EndFrameOfSegment")) == 0)
	{
		// EndFrameOfSegment is simple element unsigned32
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEndFrameOfSegment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned32")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned32Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEndFrameOfSegment(p, client);
					if((p = GetEndFrameOfSegment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTimeOfSegment")) == 0)
	{
		// MediaTimeOfSegment is simple element VideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTimeOfSegment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTimeOfSegment(p, client);
					if((p = GetMediaTimeOfSegment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BagOfWords")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:BagOfWords is item of type VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType
		// in element collection VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0
		
		context->Found = true;
		Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr coll = GetBagOfWords();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0; // FTT, check this
				SetBagOfWords(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 5))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType"));
	// Element serialization:
	// Class with content		
	
	if (m_StartFrameOfSegment != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StartFrameOfSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StartFrameOfSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StartFrameOfSegment->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_EndFrameOfSegment != Mp7Jrsunsigned32Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_EndFrameOfSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("EndFrameOfSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_EndFrameOfSegment->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MediaTimeOfSegment != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTimeOfSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTimeOfSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTimeOfSegment->Serialize(doc, element, &elem);
	}
	if (m_BagOfWords != Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_BagOfWords->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StartFrameOfSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StartFrameOfSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStartFrameOfSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("EndFrameOfSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("EndFrameOfSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned32; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetEndFrameOfSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTimeOfSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimeOfSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTimeOfSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("BagOfWords"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("BagOfWords")) == 0))
		{
			// Deserialize factory type
			Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr tmp = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetBagOfWords(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("StartFrameOfSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetStartFrameOfSegment(child);
  }
  if (XMLString::compareString(elementname, X("EndFrameOfSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned32; // FTT, check this
	}
	this->SetEndFrameOfSegment(child);
  }
  if (XMLString::compareString(elementname, X("MediaTimeOfSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_MediaTimeOfSegment_LocalType; // FTT, check this
	}
	this->SetMediaTimeOfSegment(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("BagOfWords")) == 0))
  {
	Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_Collection0Ptr tmp = CreateVideoSignatureType_VideoSignatureRegion_VSVideoSegment_BagOfWords_CollectionType0; // FTT, check this
	this->SetBagOfWords(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStartFrameOfSegment() != Dc1NodePtr())
		result.Insert(GetStartFrameOfSegment());
	if (GetEndFrameOfSegment() != Dc1NodePtr())
		result.Insert(GetEndFrameOfSegment());
	if (GetMediaTimeOfSegment() != Dc1NodePtr())
		result.Insert(GetMediaTimeOfSegment());
	if (GetBagOfWords() != Dc1NodePtr())
		result.Insert(GetBagOfWords());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoSignatureType_VideoSignatureRegion_VSVideoSegment_LocalType_ExtMethodImpl.h


