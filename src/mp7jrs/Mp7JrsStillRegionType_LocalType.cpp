
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsVisualDSType.h"
#include "Mp7JrsGridLayoutType.h"
#include "Mp7JrsIlluminationInvariantColorType.h"
#include "Mp7JrsStillRegionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsStillRegionType_LocalType::IMp7JrsStillRegionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtPropInit.h

}

IMp7JrsStillRegionType_LocalType::~IMp7JrsStillRegionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtPropCleanup.h

}

Mp7JrsStillRegionType_LocalType::Mp7JrsStillRegionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStillRegionType_LocalType::~Mp7JrsStillRegionType_LocalType()
{
	Cleanup();
}

void Mp7JrsStillRegionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_VisualDescriptor = Dc1Ptr< Dc1Node >(); // Class
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >(); // Class
	m_GridLayoutDescriptors = Mp7JrsGridLayoutPtr(); // Class
	m_IlluminationInvariantColor = Mp7JrsIlluminationInvariantColorPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsStillRegionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
	// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
}

void Mp7JrsStillRegionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StillRegionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IStillRegionType_LocalType
	const Dc1Ptr< Mp7JrsStillRegionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		this->SetVisualDescriptor(Dc1Factory::CloneObject(tmp->GetVisualDescriptor()));
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		this->SetVisualDescriptionScheme(Dc1Factory::CloneObject(tmp->GetVisualDescriptionScheme()));
		// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
		this->SetGridLayoutDescriptors(Dc1Factory::CloneObject(tmp->GetGridLayoutDescriptors()));
		// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
		this->SetIlluminationInvariantColor(Dc1Factory::CloneObject(tmp->GetIlluminationInvariantColor()));
}

Dc1NodePtr Mp7JrsStillRegionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsStillRegionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Dc1Ptr< Dc1Node > Mp7JrsStillRegionType_LocalType::GetVisualDescriptor() const
{
		return m_VisualDescriptor;
}

Dc1Ptr< Dc1Node > Mp7JrsStillRegionType_LocalType::GetVisualDescriptionScheme() const
{
		return m_VisualDescriptionScheme;
}

Mp7JrsGridLayoutPtr Mp7JrsStillRegionType_LocalType::GetGridLayoutDescriptors() const
{
		return m_GridLayoutDescriptors;
}

Mp7JrsIlluminationInvariantColorPtr Mp7JrsStillRegionType_LocalType::GetIlluminationInvariantColor() const
{
		return m_IlluminationInvariantColor;
}

// implementing setter for choice 
/* element */
void Mp7JrsStillRegionType_LocalType::SetVisualDescriptor(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType_LocalType::SetVisualDescriptor().");
	}
	if (m_VisualDescriptor != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptor);
		m_VisualDescriptor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptor) m_VisualDescriptor->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualDescriptor) m_VisualDescriptor->UseTypeAttribute = true;
		if(m_VisualDescriptor != Dc1Ptr< Dc1Node >())
		{
			m_VisualDescriptor->SetContentName(XMLString::transcode("VisualDescriptor"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
	m_GridLayoutDescriptors = Mp7JrsGridLayoutPtr();
	// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
	m_IlluminationInvariantColor = Mp7JrsIlluminationInvariantColorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType_LocalType::SetVisualDescriptionScheme(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType_LocalType::SetVisualDescriptionScheme().");
	}
	if (m_VisualDescriptionScheme != item)
	{
		// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
		m_VisualDescriptionScheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VisualDescriptionScheme) m_VisualDescriptionScheme->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_VisualDescriptionScheme) m_VisualDescriptionScheme->UseTypeAttribute = true;
		if(m_VisualDescriptionScheme != Dc1Ptr< Dc1Node >())
		{
			m_VisualDescriptionScheme->SetContentName(XMLString::transcode("VisualDescriptionScheme"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
	m_GridLayoutDescriptors = Mp7JrsGridLayoutPtr();
	// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
	m_IlluminationInvariantColor = Mp7JrsIlluminationInvariantColorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType_LocalType::SetGridLayoutDescriptors(const Mp7JrsGridLayoutPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType_LocalType::SetGridLayoutDescriptors().");
	}
	if (m_GridLayoutDescriptors != item)
	{
		// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
		m_GridLayoutDescriptors = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GridLayoutDescriptors) m_GridLayoutDescriptors->SetParent(m_myself.getPointer());
		if (m_GridLayoutDescriptors && m_GridLayoutDescriptors->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GridLayoutType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GridLayoutDescriptors->UseTypeAttribute = true;
		}
		if(m_GridLayoutDescriptors != Mp7JrsGridLayoutPtr())
		{
			m_GridLayoutDescriptors->SetContentName(XMLString::transcode("GridLayoutDescriptors"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
	m_IlluminationInvariantColor = Mp7JrsIlluminationInvariantColorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStillRegionType_LocalType::SetIlluminationInvariantColor(const Mp7JrsIlluminationInvariantColorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionType_LocalType::SetIlluminationInvariantColor().");
	}
	if (m_IlluminationInvariantColor != item)
	{
		// Dc1Factory::DeleteObject(m_IlluminationInvariantColor);
		m_IlluminationInvariantColor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IlluminationInvariantColor) m_IlluminationInvariantColor->SetParent(m_myself.getPointer());
		if (m_IlluminationInvariantColor && m_IlluminationInvariantColor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IlluminationInvariantColor->UseTypeAttribute = true;
		}
		if(m_IlluminationInvariantColor != Mp7JrsIlluminationInvariantColorPtr())
		{
			m_IlluminationInvariantColor->SetContentName(XMLString::transcode("IlluminationInvariantColor"));
		}
	// Dc1Factory::DeleteObject(m_VisualDescriptor);
	m_VisualDescriptor = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_VisualDescriptionScheme);
	m_VisualDescriptionScheme = Dc1Ptr< Dc1Node >();
	// Dc1Factory::DeleteObject(m_GridLayoutDescriptors);
	m_GridLayoutDescriptors = Mp7JrsGridLayoutPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: StillRegionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsStillRegionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsStillRegionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsStillRegionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsStillRegionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_VisualDescriptor != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualDescriptor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualDescriptor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualDescriptor->UseTypeAttribute = true;
		}
		m_VisualDescriptor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VisualDescriptionScheme != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VisualDescriptionScheme->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VisualDescriptionScheme"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_VisualDescriptionScheme->UseTypeAttribute = true;
		}
		m_VisualDescriptionScheme->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_GridLayoutDescriptors != Mp7JrsGridLayoutPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GridLayoutDescriptors->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GridLayoutDescriptors"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GridLayoutDescriptors->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_IlluminationInvariantColor != Mp7JrsIlluminationInvariantColorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IlluminationInvariantColor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IlluminationInvariantColor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IlluminationInvariantColor->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsStillRegionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualDescriptor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualDescriptor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualDescriptor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VisualDescriptionScheme"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VisualDescriptionScheme")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVisualDSType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVisualDescriptionScheme(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GridLayoutDescriptors"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GridLayoutDescriptors")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGridLayoutType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGridLayoutDescriptors(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IlluminationInvariantColor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IlluminationInvariantColor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIlluminationInvariantColorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIlluminationInvariantColor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStillRegionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("VisualDescriptor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDType; // FTT, check this
	}
	this->SetVisualDescriptor(child);
  }
  if (XMLString::compareString(elementname, X("VisualDescriptionScheme")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVisualDSType; // FTT, check this
	}
	this->SetVisualDescriptionScheme(child);
  }
  if (XMLString::compareString(elementname, X("GridLayoutDescriptors")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGridLayoutType; // FTT, check this
	}
	this->SetGridLayoutDescriptors(child);
  }
  if (XMLString::compareString(elementname, X("IlluminationInvariantColor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIlluminationInvariantColorType; // FTT, check this
	}
	this->SetIlluminationInvariantColor(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStillRegionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVisualDescriptor() != Dc1NodePtr())
		result.Insert(GetVisualDescriptor());
	if (GetVisualDescriptionScheme() != Dc1NodePtr())
		result.Insert(GetVisualDescriptionScheme());
	if (GetGridLayoutDescriptors() != Dc1NodePtr())
		result.Insert(GetGridLayoutDescriptors());
	if (GetIlluminationInvariantColor() != Dc1NodePtr())
		result.Insert(GetIlluminationInvariantColor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStillRegionType_LocalType_ExtMethodImpl.h


