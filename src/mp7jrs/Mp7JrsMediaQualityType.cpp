
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsMediaQualityType_QualityRating_CollectionType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsMediaQualityType_RatingInformationLocator_CollectionType.h"
#include "Mp7JrsMediaQualityType_PerceptibleDefects_LocalType.h"
#include "Mp7JrsMediaQualityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsMediaQualityType_QualityRating_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QualityRating
#include "Mp7JrsReferenceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RatingInformationLocator

#include <assert.h>
IMp7JrsMediaQualityType::IMp7JrsMediaQualityType()
{

// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtPropInit.h

}

IMp7JrsMediaQualityType::~IMp7JrsMediaQualityType()
{
// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtPropCleanup.h

}

Mp7JrsMediaQualityType::Mp7JrsMediaQualityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaQualityType::~Mp7JrsMediaQualityType()
{
	Cleanup();
}

void Mp7JrsMediaQualityType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_QualityRating = Mp7JrsMediaQualityType_QualityRating_CollectionPtr(); // Collection
	m_RatingSource = Dc1Ptr< Dc1Node >(); // Class
	m_RatingSource_Exist = false;
	m_RatingInformationLocator = Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr(); // Collection
	m_PerceptibleDefects = Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr(); // Class
	m_PerceptibleDefects_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtMyPropInit.h

}

void Mp7JrsMediaQualityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_QualityRating);
	// Dc1Factory::DeleteObject(m_RatingSource);
	// Dc1Factory::DeleteObject(m_RatingInformationLocator);
	// Dc1Factory::DeleteObject(m_PerceptibleDefects);
}

void Mp7JrsMediaQualityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaQualityTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaQualityType
	const Dc1Ptr< Mp7JrsMediaQualityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaQualityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_QualityRating);
		this->SetQualityRating(Dc1Factory::CloneObject(tmp->GetQualityRating()));
	if (tmp->IsValidRatingSource())
	{
		// Dc1Factory::DeleteObject(m_RatingSource);
		this->SetRatingSource(Dc1Factory::CloneObject(tmp->GetRatingSource()));
	}
	else
	{
		InvalidateRatingSource();
	}
		// Dc1Factory::DeleteObject(m_RatingInformationLocator);
		this->SetRatingInformationLocator(Dc1Factory::CloneObject(tmp->GetRatingInformationLocator()));
	if (tmp->IsValidPerceptibleDefects())
	{
		// Dc1Factory::DeleteObject(m_PerceptibleDefects);
		this->SetPerceptibleDefects(Dc1Factory::CloneObject(tmp->GetPerceptibleDefects()));
	}
	else
	{
		InvalidatePerceptibleDefects();
	}
}

Dc1NodePtr Mp7JrsMediaQualityType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaQualityType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsMediaQualityType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaQualityType_QualityRating_CollectionPtr Mp7JrsMediaQualityType::GetQualityRating() const
{
		return m_QualityRating;
}

Dc1Ptr< Dc1Node > Mp7JrsMediaQualityType::GetRatingSource() const
{
		return m_RatingSource;
}

// Element is optional
bool Mp7JrsMediaQualityType::IsValidRatingSource() const
{
	return m_RatingSource_Exist;
}

Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr Mp7JrsMediaQualityType::GetRatingInformationLocator() const
{
		return m_RatingInformationLocator;
}

Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr Mp7JrsMediaQualityType::GetPerceptibleDefects() const
{
		return m_PerceptibleDefects;
}

// Element is optional
bool Mp7JrsMediaQualityType::IsValidPerceptibleDefects() const
{
	return m_PerceptibleDefects_Exist;
}

void Mp7JrsMediaQualityType::SetQualityRating(const Mp7JrsMediaQualityType_QualityRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaQualityType::SetQualityRating().");
	}
	if (m_QualityRating != item)
	{
		// Dc1Factory::DeleteObject(m_QualityRating);
		m_QualityRating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QualityRating) m_QualityRating->SetParent(m_myself.getPointer());
		if(m_QualityRating != Mp7JrsMediaQualityType_QualityRating_CollectionPtr())
		{
			m_QualityRating->SetContentName(XMLString::transcode("QualityRating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaQualityType::SetRatingSource(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaQualityType::SetRatingSource().");
	}
	if (m_RatingSource != item || m_RatingSource_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RatingSource);
		m_RatingSource = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RatingSource) m_RatingSource->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_RatingSource) m_RatingSource->UseTypeAttribute = true;
		m_RatingSource_Exist = true;
		if(m_RatingSource != Dc1Ptr< Dc1Node >())
		{
			m_RatingSource->SetContentName(XMLString::transcode("RatingSource"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaQualityType::InvalidateRatingSource()
{
	m_RatingSource_Exist = false;
}
void Mp7JrsMediaQualityType::SetRatingInformationLocator(const Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaQualityType::SetRatingInformationLocator().");
	}
	if (m_RatingInformationLocator != item)
	{
		// Dc1Factory::DeleteObject(m_RatingInformationLocator);
		m_RatingInformationLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RatingInformationLocator) m_RatingInformationLocator->SetParent(m_myself.getPointer());
		if(m_RatingInformationLocator != Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr())
		{
			m_RatingInformationLocator->SetContentName(XMLString::transcode("RatingInformationLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaQualityType::SetPerceptibleDefects(const Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaQualityType::SetPerceptibleDefects().");
	}
	if (m_PerceptibleDefects != item || m_PerceptibleDefects_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PerceptibleDefects);
		m_PerceptibleDefects = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PerceptibleDefects) m_PerceptibleDefects->SetParent(m_myself.getPointer());
		if (m_PerceptibleDefects && m_PerceptibleDefects->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_PerceptibleDefects_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PerceptibleDefects->UseTypeAttribute = true;
		}
		m_PerceptibleDefects_Exist = true;
		if(m_PerceptibleDefects != Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr())
		{
			m_PerceptibleDefects->SetContentName(XMLString::transcode("PerceptibleDefects"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaQualityType::InvalidatePerceptibleDefects()
{
	m_PerceptibleDefects_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaQualityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("QualityRating")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:QualityRating is item of type MediaQualityType_QualityRating_LocalType
		// in element collection MediaQualityType_QualityRating_CollectionType
		
		context->Found = true;
		Mp7JrsMediaQualityType_QualityRating_CollectionPtr coll = GetQualityRating();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaQualityType_QualityRating_CollectionType; // FTT, check this
				SetQualityRating(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_QualityRating_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaQualityType_QualityRating_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RatingSource")) == 0)
	{
		// RatingSource is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRatingSource()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRatingSource(p, client);
					if((p = GetRatingSource()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RatingInformationLocator")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:RatingInformationLocator is item of type ReferenceType
		// in element collection MediaQualityType_RatingInformationLocator_CollectionType
		
		context->Found = true;
		Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr coll = GetRatingInformationLocator();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaQualityType_RatingInformationLocator_CollectionType; // FTT, check this
				SetRatingInformationLocator(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PerceptibleDefects")) == 0)
	{
		// PerceptibleDefects is simple element MediaQualityType_PerceptibleDefects_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPerceptibleDefects()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaQualityType_PerceptibleDefects_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPerceptibleDefects(p, client);
					if((p = GetPerceptibleDefects()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaQualityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaQualityType");
	}
	return result;
}

XMLCh * Mp7JrsMediaQualityType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMediaQualityType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMediaQualityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaQualityType"));
	// Element serialization:
	if (m_QualityRating != Mp7JrsMediaQualityType_QualityRating_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QualityRating->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_RatingSource != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RatingSource->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RatingSource"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_RatingSource->UseTypeAttribute = true;
		}
		m_RatingSource->Serialize(doc, element, &elem);
	}
	if (m_RatingInformationLocator != Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_RatingInformationLocator->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_PerceptibleDefects != Mp7JrsMediaQualityType_PerceptibleDefects_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PerceptibleDefects->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PerceptibleDefects"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PerceptibleDefects->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaQualityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("QualityRating"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("QualityRating")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaQualityType_QualityRating_CollectionPtr tmp = CreateMediaQualityType_QualityRating_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQualityRating(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RatingSource"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RatingSource")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRatingSource(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("RatingInformationLocator"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("RatingInformationLocator")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr tmp = CreateMediaQualityType_RatingInformationLocator_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRatingInformationLocator(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PerceptibleDefects"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PerceptibleDefects")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaQualityType_PerceptibleDefects_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPerceptibleDefects(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaQualityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("QualityRating")) == 0))
  {
	Mp7JrsMediaQualityType_QualityRating_CollectionPtr tmp = CreateMediaQualityType_QualityRating_CollectionType; // FTT, check this
	this->SetQualityRating(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("RatingSource")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetRatingSource(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("RatingInformationLocator")) == 0))
  {
	Mp7JrsMediaQualityType_RatingInformationLocator_CollectionPtr tmp = CreateMediaQualityType_RatingInformationLocator_CollectionType; // FTT, check this
	this->SetRatingInformationLocator(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("PerceptibleDefects")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaQualityType_PerceptibleDefects_LocalType; // FTT, check this
	}
	this->SetPerceptibleDefects(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaQualityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetQualityRating() != Dc1NodePtr())
		result.Insert(GetQualityRating());
	if (GetRatingSource() != Dc1NodePtr())
		result.Insert(GetRatingSource());
	if (GetRatingInformationLocator() != Dc1NodePtr())
		result.Insert(GetRatingInformationLocator());
	if (GetPerceptibleDefects() != Dc1NodePtr())
		result.Insert(GetPerceptibleDefects());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaQualityType_ExtMethodImpl.h


