
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtImplInclude.h


#include "Mp7JrsTextureBrowsingType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsTextureBrowsingType_LocalType::IMp7JrsTextureBrowsingType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtPropInit.h

}

IMp7JrsTextureBrowsingType_LocalType::~IMp7JrsTextureBrowsingType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtPropCleanup.h

}

Mp7JrsTextureBrowsingType_LocalType::Mp7JrsTextureBrowsingType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextureBrowsingType_LocalType::~Mp7JrsTextureBrowsingType_LocalType()
{
	Cleanup();
}

void Mp7JrsTextureBrowsingType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Direction = Mp7JrsTextureBrowsingType_Direction_LocalType::UninitializedEnumeration; // Enumeration
	m_Scale = Mp7JrsTextureBrowsingType_Scale_LocalType::UninitializedEnumeration; // Enumeration


// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtMyPropInit.h

}

void Mp7JrsTextureBrowsingType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsTextureBrowsingType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextureBrowsingType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ITextureBrowsingType_LocalType
	const Dc1Ptr< Mp7JrsTextureBrowsingType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetDirection(tmp->GetDirection());
		this->SetScale(tmp->GetScale());
}

Dc1NodePtr Mp7JrsTextureBrowsingType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsTextureBrowsingType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsTextureBrowsingType_Direction_LocalType::Enumeration Mp7JrsTextureBrowsingType_LocalType::GetDirection() const
{
		return m_Direction;
}

Mp7JrsTextureBrowsingType_Scale_LocalType::Enumeration Mp7JrsTextureBrowsingType_LocalType::GetScale() const
{
		return m_Scale;
}

void Mp7JrsTextureBrowsingType_LocalType::SetDirection(Mp7JrsTextureBrowsingType_Direction_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextureBrowsingType_LocalType::SetDirection().");
	}
	if (m_Direction != item)
	{
		// nothing to free here, hopefully
		m_Direction = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextureBrowsingType_LocalType::SetScale(Mp7JrsTextureBrowsingType_Scale_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextureBrowsingType_LocalType::SetScale().");
	}
	if (m_Scale != item)
	{
		// nothing to free here, hopefully
		m_Scale = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: TextureBrowsingType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsTextureBrowsingType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsTextureBrowsingType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsTextureBrowsingType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsTextureBrowsingType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextureBrowsingType_LocalType"));
	// Element serialization:
	if(m_Direction != Mp7JrsTextureBrowsingType_Direction_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsTextureBrowsingType_Direction_LocalType::ToText(m_Direction);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Direction"), false);
		XMLString::release(&tmp);
	}
	if(m_Scale != Mp7JrsTextureBrowsingType_Scale_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsTextureBrowsingType_Scale_LocalType::ToText(m_Scale);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("Scale"), false);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsTextureBrowsingType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Direction"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Direction")) == 0))
		{
			Mp7JrsTextureBrowsingType_Direction_LocalType::Enumeration tmp = Mp7JrsTextureBrowsingType_Direction_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsTextureBrowsingType_Direction_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetDirection(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("Scale"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Scale")) == 0))
		{
			Mp7JrsTextureBrowsingType_Scale_LocalType::Enumeration tmp = Mp7JrsTextureBrowsingType_Scale_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsTextureBrowsingType_Scale_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetScale(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextureBrowsingType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsTextureBrowsingType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextureBrowsingType_LocalType_ExtMethodImpl.h


