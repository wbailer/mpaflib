
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrscurvatureType.h"
#include "Mp7Jrsunsigned7.h"
#include "Mp7JrsContourShapeType_Peak_CollectionType.h"
#include "Mp7JrsContourShapeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsContourShapeType_Peak_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Peak

#include <assert.h>
IMp7JrsContourShapeType::IMp7JrsContourShapeType()
{

// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtPropInit.h

}

IMp7JrsContourShapeType::~IMp7JrsContourShapeType()
{
// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtPropCleanup.h

}

Mp7JrsContourShapeType::Mp7JrsContourShapeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsContourShapeType::~Mp7JrsContourShapeType()
{
	Cleanup();
}

void Mp7JrsContourShapeType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_GlobalCurvature = Mp7JrscurvaturePtr(); // Class
	m_PrototypeCurvature = Mp7JrscurvaturePtr(); // Class
	m_PrototypeCurvature_Exist = false;
	m_HighestPeakY = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_Peak = Mp7JrsContourShapeType_Peak_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtMyPropInit.h

}

void Mp7JrsContourShapeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_GlobalCurvature);
	// Dc1Factory::DeleteObject(m_PrototypeCurvature);
	// Dc1Factory::DeleteObject(m_HighestPeakY);
	// Dc1Factory::DeleteObject(m_Peak);
}

void Mp7JrsContourShapeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContourShapeTypePtr, since we
	// might need GetBase(), which isn't defined in IContourShapeType
	const Dc1Ptr< Mp7JrsContourShapeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsContourShapeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_GlobalCurvature);
		this->SetGlobalCurvature(Dc1Factory::CloneObject(tmp->GetGlobalCurvature()));
	if (tmp->IsValidPrototypeCurvature())
	{
		// Dc1Factory::DeleteObject(m_PrototypeCurvature);
		this->SetPrototypeCurvature(Dc1Factory::CloneObject(tmp->GetPrototypeCurvature()));
	}
	else
	{
		InvalidatePrototypeCurvature();
	}
		// Dc1Factory::DeleteObject(m_HighestPeakY);
		this->SetHighestPeakY(Dc1Factory::CloneObject(tmp->GetHighestPeakY()));
		// Dc1Factory::DeleteObject(m_Peak);
		this->SetPeak(Dc1Factory::CloneObject(tmp->GetPeak()));
}

Dc1NodePtr Mp7JrsContourShapeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsContourShapeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsContourShapeType::GetBase() const
{
	return m_Base;
}

Mp7JrscurvaturePtr Mp7JrsContourShapeType::GetGlobalCurvature() const
{
		return m_GlobalCurvature;
}

Mp7JrscurvaturePtr Mp7JrsContourShapeType::GetPrototypeCurvature() const
{
		return m_PrototypeCurvature;
}

// Element is optional
bool Mp7JrsContourShapeType::IsValidPrototypeCurvature() const
{
	return m_PrototypeCurvature_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsContourShapeType::GetHighestPeakY() const
{
		return m_HighestPeakY;
}

Mp7JrsContourShapeType_Peak_CollectionPtr Mp7JrsContourShapeType::GetPeak() const
{
		return m_Peak;
}

void Mp7JrsContourShapeType::SetGlobalCurvature(const Mp7JrscurvaturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContourShapeType::SetGlobalCurvature().");
	}
	if (m_GlobalCurvature != item)
	{
		// Dc1Factory::DeleteObject(m_GlobalCurvature);
		m_GlobalCurvature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GlobalCurvature) m_GlobalCurvature->SetParent(m_myself.getPointer());
		if (m_GlobalCurvature && m_GlobalCurvature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_GlobalCurvature->UseTypeAttribute = true;
		}
		if(m_GlobalCurvature != Mp7JrscurvaturePtr())
		{
			m_GlobalCurvature->SetContentName(XMLString::transcode("GlobalCurvature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContourShapeType::SetPrototypeCurvature(const Mp7JrscurvaturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContourShapeType::SetPrototypeCurvature().");
	}
	if (m_PrototypeCurvature != item || m_PrototypeCurvature_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PrototypeCurvature);
		m_PrototypeCurvature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PrototypeCurvature) m_PrototypeCurvature->SetParent(m_myself.getPointer());
		if (m_PrototypeCurvature && m_PrototypeCurvature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PrototypeCurvature->UseTypeAttribute = true;
		}
		m_PrototypeCurvature_Exist = true;
		if(m_PrototypeCurvature != Mp7JrscurvaturePtr())
		{
			m_PrototypeCurvature->SetContentName(XMLString::transcode("PrototypeCurvature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContourShapeType::InvalidatePrototypeCurvature()
{
	m_PrototypeCurvature_Exist = false;
}
void Mp7JrsContourShapeType::SetHighestPeakY(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContourShapeType::SetHighestPeakY().");
	}
	if (m_HighestPeakY != item)
	{
		// Dc1Factory::DeleteObject(m_HighestPeakY);
		m_HighestPeakY = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HighestPeakY) m_HighestPeakY->SetParent(m_myself.getPointer());
		if (m_HighestPeakY && m_HighestPeakY->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HighestPeakY->UseTypeAttribute = true;
		}
		if(m_HighestPeakY != Mp7Jrsunsigned7Ptr())
		{
			m_HighestPeakY->SetContentName(XMLString::transcode("HighestPeakY"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContourShapeType::SetPeak(const Mp7JrsContourShapeType_Peak_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContourShapeType::SetPeak().");
	}
	if (m_Peak != item)
	{
		// Dc1Factory::DeleteObject(m_Peak);
		m_Peak = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Peak) m_Peak->SetParent(m_myself.getPointer());
		if(m_Peak != Mp7JrsContourShapeType_Peak_CollectionPtr())
		{
			m_Peak->SetContentName(XMLString::transcode("Peak"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsContourShapeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("GlobalCurvature")) == 0)
	{
		// GlobalCurvature is simple element curvatureType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGlobalCurvature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrscurvaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGlobalCurvature(p, client);
					if((p = GetGlobalCurvature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PrototypeCurvature")) == 0)
	{
		// PrototypeCurvature is simple element curvatureType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPrototypeCurvature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:curvatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrscurvaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPrototypeCurvature(p, client);
					if((p = GetPrototypeCurvature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HighestPeakY")) == 0)
	{
		// HighestPeakY is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHighestPeakY()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHighestPeakY(p, client);
					if((p = GetHighestPeakY()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Peak")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Peak is item of type ContourShapeType_Peak_LocalType
		// in element collection ContourShapeType_Peak_CollectionType
		
		context->Found = true;
		Mp7JrsContourShapeType_Peak_CollectionPtr coll = GetPeak();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContourShapeType_Peak_CollectionType; // FTT, check this
				SetPeak(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 62))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType_Peak_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsContourShapeType_Peak_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ContourShapeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ContourShapeType");
	}
	return result;
}

XMLCh * Mp7JrsContourShapeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsContourShapeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsContourShapeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ContourShapeType"));
	// Element serialization:
	// Class
	
	if (m_GlobalCurvature != Mp7JrscurvaturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_GlobalCurvature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("GlobalCurvature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_GlobalCurvature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PrototypeCurvature != Mp7JrscurvaturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PrototypeCurvature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PrototypeCurvature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PrototypeCurvature->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_HighestPeakY != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HighestPeakY->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HighestPeakY"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HighestPeakY->Serialize(doc, element, &elem);
	}
	if (m_Peak != Mp7JrsContourShapeType_Peak_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Peak->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsContourShapeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("GlobalCurvature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("GlobalCurvature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecurvatureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGlobalCurvature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PrototypeCurvature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PrototypeCurvature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatecurvatureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPrototypeCurvature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HighestPeakY"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HighestPeakY")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHighestPeakY(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Peak"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Peak")) == 0))
		{
			// Deserialize factory type
			Mp7JrsContourShapeType_Peak_CollectionPtr tmp = CreateContourShapeType_Peak_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPeak(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsContourShapeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("GlobalCurvature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecurvatureType; // FTT, check this
	}
	this->SetGlobalCurvature(child);
  }
  if (XMLString::compareString(elementname, X("PrototypeCurvature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecurvatureType; // FTT, check this
	}
	this->SetPrototypeCurvature(child);
  }
  if (XMLString::compareString(elementname, X("HighestPeakY")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetHighestPeakY(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Peak")) == 0))
  {
	Mp7JrsContourShapeType_Peak_CollectionPtr tmp = CreateContourShapeType_Peak_CollectionType; // FTT, check this
	this->SetPeak(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsContourShapeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetGlobalCurvature() != Dc1NodePtr())
		result.Insert(GetGlobalCurvature());
	if (GetPrototypeCurvature() != Dc1NodePtr())
		result.Insert(GetPrototypeCurvature());
	if (GetHighestPeakY() != Dc1NodePtr())
		result.Insert(GetHighestPeakY());
	if (GetPeak() != Dc1NodePtr())
		result.Insert(GetPeak());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsContourShapeType_ExtMethodImpl.h


