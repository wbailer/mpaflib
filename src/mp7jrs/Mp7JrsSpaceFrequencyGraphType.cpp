
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtImplInclude.h


#include "Mp7JrsViewDecompositionType.h"
#include "Mp7JrsSpaceFrequencyViewType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsSpaceFrequencyGraphType_CollectionType.h"
#include "Mp7JrsSpaceFrequencyGraphType_CollectionType0.h"
#include "Mp7JrsSignalType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSpaceFrequencyGraphType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSpaceFrequencyGraphType_LocalType.h" // Choice collection SpaceChild
#include "Mp7JrsSpaceFrequencyGraphType.h" // Choice collection element SpaceChild
#include "Mp7JrsSpaceFrequencyGraphType_LocalType0.h" // Choice collection FrequencyChild

#include <assert.h>
IMp7JrsSpaceFrequencyGraphType::IMp7JrsSpaceFrequencyGraphType()
{

// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtPropInit.h

}

IMp7JrsSpaceFrequencyGraphType::~IMp7JrsSpaceFrequencyGraphType()
{
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtPropCleanup.h

}

Mp7JrsSpaceFrequencyGraphType::Mp7JrsSpaceFrequencyGraphType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpaceFrequencyGraphType::~Mp7JrsSpaceFrequencyGraphType()
{
	Cleanup();
}

void Mp7JrsSpaceFrequencyGraphType::Init()
{
	// Init base
	m_Base = CreateViewDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_branching = 0; // Value

	// Init elements (element, union sequence choice all any)
	
	m_View = Mp7JrsSpaceFrequencyViewPtr(); // Class
	m_ViewRef = Mp7JrsReferencePtr(); // Class
	m_SpaceFrequencyGraphType_LocalType = Mp7JrsSpaceFrequencyGraphType_CollectionPtr(); // Collection
	m_SpaceFrequencyGraphType_LocalType0 = Mp7JrsSpaceFrequencyGraphType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtMyPropInit.h

}

void Mp7JrsSpaceFrequencyGraphType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_View);
	// Dc1Factory::DeleteObject(m_ViewRef);
	// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType);
	// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType0);
}

void Mp7JrsSpaceFrequencyGraphType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpaceFrequencyGraphTypePtr, since we
	// might need GetBase(), which isn't defined in ISpaceFrequencyGraphType
	const Dc1Ptr< Mp7JrsSpaceFrequencyGraphType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsViewDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSpaceFrequencyGraphType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->Setbranching(tmp->Getbranching());
	}
		// Dc1Factory::DeleteObject(m_View);
		this->SetView(Dc1Factory::CloneObject(tmp->GetView()));
		// Dc1Factory::DeleteObject(m_ViewRef);
		this->SetViewRef(Dc1Factory::CloneObject(tmp->GetViewRef()));
		// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType);
		this->SetSpaceFrequencyGraphType_LocalType(Dc1Factory::CloneObject(tmp->GetSpaceFrequencyGraphType_LocalType()));
		// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType0);
		this->SetSpaceFrequencyGraphType_LocalType0(Dc1Factory::CloneObject(tmp->GetSpaceFrequencyGraphType_LocalType0()));
}

Dc1NodePtr Mp7JrsSpaceFrequencyGraphType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSpaceFrequencyGraphType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsViewDecompositionType > Mp7JrsSpaceFrequencyGraphType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsSpaceFrequencyGraphType::Getbranching() const
{
	return m_branching;
}

void Mp7JrsSpaceFrequencyGraphType::Setbranching(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::Setbranching().");
	}
	m_branching = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSpaceFrequencyViewPtr Mp7JrsSpaceFrequencyGraphType::GetView() const
{
		return m_View;
}

Mp7JrsReferencePtr Mp7JrsSpaceFrequencyGraphType::GetViewRef() const
{
		return m_ViewRef;
}

Mp7JrsSpaceFrequencyGraphType_CollectionPtr Mp7JrsSpaceFrequencyGraphType::GetSpaceFrequencyGraphType_LocalType() const
{
		return m_SpaceFrequencyGraphType_LocalType;
}

Mp7JrsSpaceFrequencyGraphType_Collection0Ptr Mp7JrsSpaceFrequencyGraphType::GetSpaceFrequencyGraphType_LocalType0() const
{
		return m_SpaceFrequencyGraphType_LocalType0;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSpaceFrequencyGraphType::SetView(const Mp7JrsSpaceFrequencyViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetView().");
	}
	if (m_View != item)
	{
		// Dc1Factory::DeleteObject(m_View);
		m_View = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_View) m_View->SetParent(m_myself.getPointer());
		if (m_View && m_View->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyViewType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_View->UseTypeAttribute = true;
		}
		if(m_View != Mp7JrsSpaceFrequencyViewPtr())
		{
			m_View->SetContentName(XMLString::transcode("View"));
		}
	// Dc1Factory::DeleteObject(m_ViewRef);
	m_ViewRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpaceFrequencyGraphType::SetViewRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetViewRef().");
	}
	if (m_ViewRef != item)
	{
		// Dc1Factory::DeleteObject(m_ViewRef);
		m_ViewRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ViewRef) m_ViewRef->SetParent(m_myself.getPointer());
		if (m_ViewRef && m_ViewRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ViewRef->UseTypeAttribute = true;
		}
		if(m_ViewRef != Mp7JrsReferencePtr())
		{
			m_ViewRef->SetContentName(XMLString::transcode("ViewRef"));
		}
	// Dc1Factory::DeleteObject(m_View);
	m_View = Mp7JrsSpaceFrequencyViewPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::SetSpaceFrequencyGraphType_LocalType(const Mp7JrsSpaceFrequencyGraphType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetSpaceFrequencyGraphType_LocalType().");
	}
	if (m_SpaceFrequencyGraphType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType);
		m_SpaceFrequencyGraphType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpaceFrequencyGraphType_LocalType) m_SpaceFrequencyGraphType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::SetSpaceFrequencyGraphType_LocalType0(const Mp7JrsSpaceFrequencyGraphType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetSpaceFrequencyGraphType_LocalType0().");
	}
	if (m_SpaceFrequencyGraphType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_SpaceFrequencyGraphType_LocalType0);
		m_SpaceFrequencyGraphType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpaceFrequencyGraphType_LocalType0) m_SpaceFrequencyGraphType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsSpaceFrequencyGraphType::Getcomplete() const
{
	return GetBase()->Getcomplete();
}

bool Mp7JrsSpaceFrequencyGraphType::Existcomplete() const
{
	return GetBase()->Existcomplete();
}
void Mp7JrsSpaceFrequencyGraphType::Setcomplete(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::Setcomplete().");
	}
	GetBase()->Setcomplete(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::Invalidatecomplete()
{
	GetBase()->Invalidatecomplete();
}
bool Mp7JrsSpaceFrequencyGraphType::GetnonRedundant() const
{
	return GetBase()->GetnonRedundant();
}

bool Mp7JrsSpaceFrequencyGraphType::ExistnonRedundant() const
{
	return GetBase()->ExistnonRedundant();
}
void Mp7JrsSpaceFrequencyGraphType::SetnonRedundant(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetnonRedundant().");
	}
	GetBase()->SetnonRedundant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidatenonRedundant()
{
	GetBase()->InvalidatenonRedundant();
}
Mp7JrsSignalPtr Mp7JrsSpaceFrequencyGraphType::GetSource() const
{
	return GetBase()->GetSource();
}

// Element is optional
bool Mp7JrsSpaceFrequencyGraphType::IsValidSource() const
{
	return GetBase()->IsValidSource();
}

void Mp7JrsSpaceFrequencyGraphType::SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetSource().");
	}
	GetBase()->SetSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidateSource()
{
	GetBase()->InvalidateSource();
}
XMLCh * Mp7JrsSpaceFrequencyGraphType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsSpaceFrequencyGraphType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsSpaceFrequencyGraphType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSpaceFrequencyGraphType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsSpaceFrequencyGraphType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsSpaceFrequencyGraphType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSpaceFrequencyGraphType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsSpaceFrequencyGraphType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsSpaceFrequencyGraphType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSpaceFrequencyGraphType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSpaceFrequencyGraphType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSpaceFrequencyGraphType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSpaceFrequencyGraphType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSpaceFrequencyGraphType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSpaceFrequencyGraphType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpaceFrequencyGraphType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSpaceFrequencyGraphType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsSpaceFrequencyGraphType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceFrequencyGraphType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpaceFrequencyGraphType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("branching")) == 0)
	{
		// branching is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("View")) == 0)
	{
		// View is simple element SpaceFrequencyViewType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetView()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpaceFrequencyViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetView(p, client);
					if((p = GetView()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ViewRef")) == 0)
	{
		// ViewRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetViewRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetViewRef(p, client);
					if((p = GetViewRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpaceChild")) == 0)
	{
		// SpaceChild is contained in itemtype SpaceFrequencyGraphType_LocalType
		// in choice collection SpaceFrequencyGraphType_CollectionType

		context->Found = true;
		Mp7JrsSpaceFrequencyGraphType_CollectionPtr coll = GetSpaceFrequencyGraphType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpaceFrequencyGraphType_CollectionType; // FTT, check this
				SetSpaceFrequencyGraphType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->GetSpaceChild()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpaceFrequencyGraphType_LocalPtr item = CreateSpaceFrequencyGraphType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpaceFrequencyGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->SetSpaceChild(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->GetSpaceChild()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpaceChildRef")) == 0)
	{
		// SpaceChildRef is contained in itemtype SpaceFrequencyGraphType_LocalType
		// in choice collection SpaceFrequencyGraphType_CollectionType

		context->Found = true;
		Mp7JrsSpaceFrequencyGraphType_CollectionPtr coll = GetSpaceFrequencyGraphType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpaceFrequencyGraphType_CollectionType; // FTT, check this
				SetSpaceFrequencyGraphType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->GetSpaceChildRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpaceFrequencyGraphType_LocalPtr item = CreateSpaceFrequencyGraphType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->SetSpaceChildRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpaceFrequencyGraphType_LocalPtr)coll->elementAt(i))->GetSpaceChildRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrequencyChild")) == 0)
	{
		// FrequencyChild is contained in itemtype SpaceFrequencyGraphType_LocalType0
		// in choice collection SpaceFrequencyGraphType_CollectionType0

		context->Found = true;
		Mp7JrsSpaceFrequencyGraphType_Collection0Ptr coll = GetSpaceFrequencyGraphType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpaceFrequencyGraphType_CollectionType0; // FTT, check this
				SetSpaceFrequencyGraphType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->GetFrequencyChild()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpaceFrequencyGraphType_Local0Ptr item = CreateSpaceFrequencyGraphType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpaceFrequencyGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->SetFrequencyChild(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->GetFrequencyChild()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrequencyChildRef")) == 0)
	{
		// FrequencyChildRef is contained in itemtype SpaceFrequencyGraphType_LocalType0
		// in choice collection SpaceFrequencyGraphType_CollectionType0

		context->Found = true;
		Mp7JrsSpaceFrequencyGraphType_Collection0Ptr coll = GetSpaceFrequencyGraphType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpaceFrequencyGraphType_CollectionType0; // FTT, check this
				SetSpaceFrequencyGraphType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->GetFrequencyChildRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSpaceFrequencyGraphType_Local0Ptr item = CreateSpaceFrequencyGraphType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->SetFrequencyChildRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSpaceFrequencyGraphType_Local0Ptr)coll->elementAt(i))->GetFrequencyChildRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpaceFrequencyGraphType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpaceFrequencyGraphType");
	}
	return result;
}

XMLCh * Mp7JrsSpaceFrequencyGraphType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpaceFrequencyGraphType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpaceFrequencyGraphType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpaceFrequencyGraphType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_branching);
		element->setAttributeNS(X(""), X("branching"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_View != Mp7JrsSpaceFrequencyViewPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_View->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("View"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_View->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ViewRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ViewRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ViewRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ViewRef->Serialize(doc, element, &elem);
	}
	if (m_SpaceFrequencyGraphType_LocalType != Mp7JrsSpaceFrequencyGraphType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpaceFrequencyGraphType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SpaceFrequencyGraphType_LocalType0 != Mp7JrsSpaceFrequencyGraphType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SpaceFrequencyGraphType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpaceFrequencyGraphType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("branching")))
	{
		// deserialize value type
		this->Setbranching(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("branching"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsViewDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("View"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("View")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpaceFrequencyViewType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetView(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ViewRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ViewRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetViewRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SpaceChild
			Dc1Util::HasNodeName(parent, X("SpaceChild"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceChild")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SpaceChildRef
			Dc1Util::HasNodeName(parent, X("SpaceChildRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceChildRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSpaceFrequencyGraphType_CollectionPtr tmp = CreateSpaceFrequencyGraphType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpaceFrequencyGraphType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:FrequencyChild
			Dc1Util::HasNodeName(parent, X("FrequencyChild"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FrequencyChild")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:FrequencyChildRef
			Dc1Util::HasNodeName(parent, X("FrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FrequencyChildRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSpaceFrequencyGraphType_Collection0Ptr tmp = CreateSpaceFrequencyGraphType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpaceFrequencyGraphType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpaceFrequencyGraphType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("View")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpaceFrequencyViewType; // FTT, check this
	}
	this->SetView(child);
  }
  if (XMLString::compareString(elementname, X("ViewRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetViewRef(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpaceChild"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpaceChild")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpaceChildRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpaceChildRef")) == 0)
	)
  {
	Mp7JrsSpaceFrequencyGraphType_CollectionPtr tmp = CreateSpaceFrequencyGraphType_CollectionType; // FTT, check this
	this->SetSpaceFrequencyGraphType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FrequencyChild"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FrequencyChild")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FrequencyChildRef")) == 0)
	)
  {
	Mp7JrsSpaceFrequencyGraphType_Collection0Ptr tmp = CreateSpaceFrequencyGraphType_CollectionType0; // FTT, check this
	this->SetSpaceFrequencyGraphType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpaceFrequencyGraphType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSpaceFrequencyGraphType_LocalType() != Dc1NodePtr())
		result.Insert(GetSpaceFrequencyGraphType_LocalType());
	if (GetSpaceFrequencyGraphType_LocalType0() != Dc1NodePtr())
		result.Insert(GetSpaceFrequencyGraphType_LocalType0());
	if (GetSource() != Dc1NodePtr())
		result.Insert(GetSource());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetView() != Dc1NodePtr())
		result.Insert(GetView());
	if (GetViewRef() != Dc1NodePtr())
		result.Insert(GetViewRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpaceFrequencyGraphType_ExtMethodImpl.h


