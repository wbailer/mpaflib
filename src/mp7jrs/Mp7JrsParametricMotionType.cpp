
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsParametricMotionType_CoordRef_LocalType.h"
#include "Mp7JrsParametricMotionType_CoordDef_LocalType.h"
#include "Mp7JrsMediaIncrDurationType.h"
#include "Mp7JrsParametricMotionType_Parameters_LocalType.h"
#include "Mp7JrsParametricMotionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsParametricMotionType::IMp7JrsParametricMotionType()
{

// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtPropInit.h

}

IMp7JrsParametricMotionType::~IMp7JrsParametricMotionType()
{
// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtPropCleanup.h

}

Mp7JrsParametricMotionType::Mp7JrsParametricMotionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsParametricMotionType::~Mp7JrsParametricMotionType()
{
	Cleanup();
}

void Mp7JrsParametricMotionType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_motionModel = Mp7JrsParametricMotionType_motionModel_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_CoordRef = Mp7JrsParametricMotionType_CoordRef_LocalPtr(); // Class
	m_CoordDef = Mp7JrsParametricMotionType_CoordDef_LocalPtr(); // Class
	m_MediaDuration = Mp7JrsMediaIncrDurationPtr(); // Class with content 
	m_Parameters = Mp7JrsParametricMotionType_Parameters_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtMyPropInit.h

}

void Mp7JrsParametricMotionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_CoordRef);
	// Dc1Factory::DeleteObject(m_CoordDef);
	// Dc1Factory::DeleteObject(m_MediaDuration);
	// Dc1Factory::DeleteObject(m_Parameters);
}

void Mp7JrsParametricMotionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ParametricMotionTypePtr, since we
	// might need GetBase(), which isn't defined in IParametricMotionType
	const Dc1Ptr< Mp7JrsParametricMotionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsParametricMotionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->SetmotionModel(tmp->GetmotionModel());
	}
		// Dc1Factory::DeleteObject(m_CoordRef);
		this->SetCoordRef(Dc1Factory::CloneObject(tmp->GetCoordRef()));
		// Dc1Factory::DeleteObject(m_CoordDef);
		this->SetCoordDef(Dc1Factory::CloneObject(tmp->GetCoordDef()));
		// Dc1Factory::DeleteObject(m_MediaDuration);
		this->SetMediaDuration(Dc1Factory::CloneObject(tmp->GetMediaDuration()));
		// Dc1Factory::DeleteObject(m_Parameters);
		this->SetParameters(Dc1Factory::CloneObject(tmp->GetParameters()));
}

Dc1NodePtr Mp7JrsParametricMotionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsParametricMotionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsParametricMotionType::GetBase() const
{
	return m_Base;
}

Mp7JrsParametricMotionType_motionModel_LocalType::Enumeration Mp7JrsParametricMotionType::GetmotionModel() const
{
	return m_motionModel;
}

void Mp7JrsParametricMotionType::SetmotionModel(Mp7JrsParametricMotionType_motionModel_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsParametricMotionType::SetmotionModel().");
	}
	m_motionModel = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsParametricMotionType_CoordRef_LocalPtr Mp7JrsParametricMotionType::GetCoordRef() const
{
		return m_CoordRef;
}

Mp7JrsParametricMotionType_CoordDef_LocalPtr Mp7JrsParametricMotionType::GetCoordDef() const
{
		return m_CoordDef;
}

Mp7JrsMediaIncrDurationPtr Mp7JrsParametricMotionType::GetMediaDuration() const
{
		return m_MediaDuration;
}

Mp7JrsParametricMotionType_Parameters_LocalPtr Mp7JrsParametricMotionType::GetParameters() const
{
		return m_Parameters;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsParametricMotionType::SetCoordRef(const Mp7JrsParametricMotionType_CoordRef_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsParametricMotionType::SetCoordRef().");
	}
	if (m_CoordRef != item)
	{
		// Dc1Factory::DeleteObject(m_CoordRef);
		m_CoordRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CoordRef) m_CoordRef->SetParent(m_myself.getPointer());
		if (m_CoordRef && m_CoordRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordRef_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CoordRef->UseTypeAttribute = true;
		}
		if(m_CoordRef != Mp7JrsParametricMotionType_CoordRef_LocalPtr())
		{
			m_CoordRef->SetContentName(XMLString::transcode("CoordRef"));
		}
	// Dc1Factory::DeleteObject(m_CoordDef);
	m_CoordDef = Mp7JrsParametricMotionType_CoordDef_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsParametricMotionType::SetCoordDef(const Mp7JrsParametricMotionType_CoordDef_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsParametricMotionType::SetCoordDef().");
	}
	if (m_CoordDef != item)
	{
		// Dc1Factory::DeleteObject(m_CoordDef);
		m_CoordDef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CoordDef) m_CoordDef->SetParent(m_myself.getPointer());
		if (m_CoordDef && m_CoordDef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordDef_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CoordDef->UseTypeAttribute = true;
		}
		if(m_CoordDef != Mp7JrsParametricMotionType_CoordDef_LocalPtr())
		{
			m_CoordDef->SetContentName(XMLString::transcode("CoordDef"));
		}
	// Dc1Factory::DeleteObject(m_CoordRef);
	m_CoordRef = Mp7JrsParametricMotionType_CoordRef_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsParametricMotionType::SetMediaDuration(const Mp7JrsMediaIncrDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsParametricMotionType::SetMediaDuration().");
	}
	if (m_MediaDuration != item)
	{
		// Dc1Factory::DeleteObject(m_MediaDuration);
		m_MediaDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaDuration) m_MediaDuration->SetParent(m_myself.getPointer());
		if (m_MediaDuration && m_MediaDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaDuration->UseTypeAttribute = true;
		}
		if(m_MediaDuration != Mp7JrsMediaIncrDurationPtr())
		{
			m_MediaDuration->SetContentName(XMLString::transcode("MediaDuration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsParametricMotionType::SetParameters(const Mp7JrsParametricMotionType_Parameters_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsParametricMotionType::SetParameters().");
	}
	if (m_Parameters != item)
	{
		// Dc1Factory::DeleteObject(m_Parameters);
		m_Parameters = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Parameters) m_Parameters->SetParent(m_myself.getPointer());
		if (m_Parameters && m_Parameters->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_Parameters_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Parameters->UseTypeAttribute = true;
		}
		if(m_Parameters != Mp7JrsParametricMotionType_Parameters_LocalPtr())
		{
			m_Parameters->SetContentName(XMLString::transcode("Parameters"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsParametricMotionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("motionModel")) == 0)
	{
		// motionModel is simple attribute ParametricMotionType_motionModel_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsParametricMotionType_motionModel_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CoordRef")) == 0)
	{
		// CoordRef is simple element ParametricMotionType_CoordRef_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoordRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordRef_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsParametricMotionType_CoordRef_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoordRef(p, client);
					if((p = GetCoordRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CoordDef")) == 0)
	{
		// CoordDef is simple element ParametricMotionType_CoordDef_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoordDef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_CoordDef_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsParametricMotionType_CoordDef_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoordDef(p, client);
					if((p = GetCoordDef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaDuration")) == 0)
	{
		// MediaDuration is simple element MediaIncrDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaIncrDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaIncrDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaDuration(p, client);
					if((p = GetMediaDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Parameters")) == 0)
	{
		// Parameters is simple element ParametricMotionType_Parameters_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetParameters()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType_Parameters_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsParametricMotionType_Parameters_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetParameters(p, client);
					if((p = GetParameters()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ParametricMotionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ParametricMotionType");
	}
	return result;
}

XMLCh * Mp7JrsParametricMotionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsParametricMotionType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsParametricMotionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ParametricMotionType"));
	// Attribute Serialization:
	// Enumeration
	if(m_motionModel != Mp7JrsParametricMotionType_motionModel_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsParametricMotionType_motionModel_LocalType::ToText(m_motionModel);
		element->setAttributeNS(X(""), X("motionModel"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_CoordRef != Mp7JrsParametricMotionType_CoordRef_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CoordRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CoordRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CoordRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CoordDef != Mp7JrsParametricMotionType_CoordDef_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CoordDef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CoordDef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CoordDef->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaDuration != Mp7JrsMediaIncrDurationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaDuration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaDuration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaDuration->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Parameters != Mp7JrsParametricMotionType_Parameters_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Parameters->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Parameters"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Parameters->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsParametricMotionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("motionModel")))
	{
		this->SetmotionModel(Mp7JrsParametricMotionType_motionModel_LocalType::Parse(parent->getAttribute(X("motionModel"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CoordRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CoordRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateParametricMotionType_CoordRef_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoordRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CoordDef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CoordDef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateParametricMotionType_CoordDef_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoordDef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaDuration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaIncrDurationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Parameters"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Parameters")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateParametricMotionType_Parameters_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParameters(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsParametricMotionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("CoordRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateParametricMotionType_CoordRef_LocalType; // FTT, check this
	}
	this->SetCoordRef(child);
  }
  if (XMLString::compareString(elementname, X("CoordDef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateParametricMotionType_CoordDef_LocalType; // FTT, check this
	}
	this->SetCoordDef(child);
  }
  if (XMLString::compareString(elementname, X("MediaDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaIncrDurationType; // FTT, check this
	}
	this->SetMediaDuration(child);
  }
  if (XMLString::compareString(elementname, X("Parameters")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateParametricMotionType_Parameters_LocalType; // FTT, check this
	}
	this->SetParameters(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsParametricMotionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaDuration() != Dc1NodePtr())
		result.Insert(GetMediaDuration());
	if (GetParameters() != Dc1NodePtr())
		result.Insert(GetParameters());
	if (GetCoordRef() != Dc1NodePtr())
		result.Insert(GetCoordRef());
	if (GetCoordDef() != Dc1NodePtr())
		result.Insert(GetCoordDef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsParametricMotionType_ExtMethodImpl.h


