
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtImplInclude.h


#include "Mp7Jrsunsigned7.h"
#include "Mp7JrsFractionalPresenceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFractionalPresenceType::IMp7JrsFractionalPresenceType()
{

// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtPropInit.h

}

IMp7JrsFractionalPresenceType::~IMp7JrsFractionalPresenceType()
{
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtPropCleanup.h

}

Mp7JrsFractionalPresenceType::Mp7JrsFractionalPresenceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFractionalPresenceType::~Mp7JrsFractionalPresenceType()
{
	Cleanup();
}

void Mp7JrsFractionalPresenceType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TrackLeft = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_TrackLeft_Exist = false;
	m_TrackRight = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_TrackRight_Exist = false;
	m_BoomDown = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_BoomDown_Exist = false;
	m_BoomUp = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_BoomUp_Exist = false;
	m_DollyForward = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_DollyForward_Exist = false;
	m_DollyBackward = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_DollyBackward_Exist = false;
	m_PanLeft = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_PanLeft_Exist = false;
	m_PanRight = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_PanRight_Exist = false;
	m_TiltDown = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_TiltDown_Exist = false;
	m_TiltUp = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_TiltUp_Exist = false;
	m_RollClockwise = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_RollClockwise_Exist = false;
	m_RollAnticlockwise = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_RollAnticlockwise_Exist = false;
	m_ZoomIn = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_ZoomIn_Exist = false;
	m_ZoomOut = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_ZoomOut_Exist = false;
	m_Fixed = Mp7Jrsunsigned7Ptr(); // Class with content 
	m_Fixed_Exist = false;


// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMyPropInit.h

}

void Mp7JrsFractionalPresenceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TrackLeft);
	// Dc1Factory::DeleteObject(m_TrackRight);
	// Dc1Factory::DeleteObject(m_BoomDown);
	// Dc1Factory::DeleteObject(m_BoomUp);
	// Dc1Factory::DeleteObject(m_DollyForward);
	// Dc1Factory::DeleteObject(m_DollyBackward);
	// Dc1Factory::DeleteObject(m_PanLeft);
	// Dc1Factory::DeleteObject(m_PanRight);
	// Dc1Factory::DeleteObject(m_TiltDown);
	// Dc1Factory::DeleteObject(m_TiltUp);
	// Dc1Factory::DeleteObject(m_RollClockwise);
	// Dc1Factory::DeleteObject(m_RollAnticlockwise);
	// Dc1Factory::DeleteObject(m_ZoomIn);
	// Dc1Factory::DeleteObject(m_ZoomOut);
	// Dc1Factory::DeleteObject(m_Fixed);
}

void Mp7JrsFractionalPresenceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FractionalPresenceTypePtr, since we
	// might need GetBase(), which isn't defined in IFractionalPresenceType
	const Dc1Ptr< Mp7JrsFractionalPresenceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidTrackLeft())
	{
		// Dc1Factory::DeleteObject(m_TrackLeft);
		this->SetTrackLeft(Dc1Factory::CloneObject(tmp->GetTrackLeft()));
	}
	else
	{
		InvalidateTrackLeft();
	}
	if (tmp->IsValidTrackRight())
	{
		// Dc1Factory::DeleteObject(m_TrackRight);
		this->SetTrackRight(Dc1Factory::CloneObject(tmp->GetTrackRight()));
	}
	else
	{
		InvalidateTrackRight();
	}
	if (tmp->IsValidBoomDown())
	{
		// Dc1Factory::DeleteObject(m_BoomDown);
		this->SetBoomDown(Dc1Factory::CloneObject(tmp->GetBoomDown()));
	}
	else
	{
		InvalidateBoomDown();
	}
	if (tmp->IsValidBoomUp())
	{
		// Dc1Factory::DeleteObject(m_BoomUp);
		this->SetBoomUp(Dc1Factory::CloneObject(tmp->GetBoomUp()));
	}
	else
	{
		InvalidateBoomUp();
	}
	if (tmp->IsValidDollyForward())
	{
		// Dc1Factory::DeleteObject(m_DollyForward);
		this->SetDollyForward(Dc1Factory::CloneObject(tmp->GetDollyForward()));
	}
	else
	{
		InvalidateDollyForward();
	}
	if (tmp->IsValidDollyBackward())
	{
		// Dc1Factory::DeleteObject(m_DollyBackward);
		this->SetDollyBackward(Dc1Factory::CloneObject(tmp->GetDollyBackward()));
	}
	else
	{
		InvalidateDollyBackward();
	}
	if (tmp->IsValidPanLeft())
	{
		// Dc1Factory::DeleteObject(m_PanLeft);
		this->SetPanLeft(Dc1Factory::CloneObject(tmp->GetPanLeft()));
	}
	else
	{
		InvalidatePanLeft();
	}
	if (tmp->IsValidPanRight())
	{
		// Dc1Factory::DeleteObject(m_PanRight);
		this->SetPanRight(Dc1Factory::CloneObject(tmp->GetPanRight()));
	}
	else
	{
		InvalidatePanRight();
	}
	if (tmp->IsValidTiltDown())
	{
		// Dc1Factory::DeleteObject(m_TiltDown);
		this->SetTiltDown(Dc1Factory::CloneObject(tmp->GetTiltDown()));
	}
	else
	{
		InvalidateTiltDown();
	}
	if (tmp->IsValidTiltUp())
	{
		// Dc1Factory::DeleteObject(m_TiltUp);
		this->SetTiltUp(Dc1Factory::CloneObject(tmp->GetTiltUp()));
	}
	else
	{
		InvalidateTiltUp();
	}
	if (tmp->IsValidRollClockwise())
	{
		// Dc1Factory::DeleteObject(m_RollClockwise);
		this->SetRollClockwise(Dc1Factory::CloneObject(tmp->GetRollClockwise()));
	}
	else
	{
		InvalidateRollClockwise();
	}
	if (tmp->IsValidRollAnticlockwise())
	{
		// Dc1Factory::DeleteObject(m_RollAnticlockwise);
		this->SetRollAnticlockwise(Dc1Factory::CloneObject(tmp->GetRollAnticlockwise()));
	}
	else
	{
		InvalidateRollAnticlockwise();
	}
	if (tmp->IsValidZoomIn())
	{
		// Dc1Factory::DeleteObject(m_ZoomIn);
		this->SetZoomIn(Dc1Factory::CloneObject(tmp->GetZoomIn()));
	}
	else
	{
		InvalidateZoomIn();
	}
	if (tmp->IsValidZoomOut())
	{
		// Dc1Factory::DeleteObject(m_ZoomOut);
		this->SetZoomOut(Dc1Factory::CloneObject(tmp->GetZoomOut()));
	}
	else
	{
		InvalidateZoomOut();
	}
	if (tmp->IsValidFixed())
	{
		// Dc1Factory::DeleteObject(m_Fixed);
		this->SetFixed(Dc1Factory::CloneObject(tmp->GetFixed()));
	}
	else
	{
		InvalidateFixed();
	}
}

Dc1NodePtr Mp7JrsFractionalPresenceType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFractionalPresenceType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetTrackLeft() const
{
		return m_TrackLeft;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidTrackLeft() const
{
	return m_TrackLeft_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetTrackRight() const
{
		return m_TrackRight;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidTrackRight() const
{
	return m_TrackRight_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetBoomDown() const
{
		return m_BoomDown;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidBoomDown() const
{
	return m_BoomDown_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetBoomUp() const
{
		return m_BoomUp;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidBoomUp() const
{
	return m_BoomUp_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetDollyForward() const
{
		return m_DollyForward;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidDollyForward() const
{
	return m_DollyForward_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetDollyBackward() const
{
		return m_DollyBackward;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidDollyBackward() const
{
	return m_DollyBackward_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetPanLeft() const
{
		return m_PanLeft;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidPanLeft() const
{
	return m_PanLeft_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetPanRight() const
{
		return m_PanRight;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidPanRight() const
{
	return m_PanRight_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetTiltDown() const
{
		return m_TiltDown;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidTiltDown() const
{
	return m_TiltDown_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetTiltUp() const
{
		return m_TiltUp;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidTiltUp() const
{
	return m_TiltUp_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetRollClockwise() const
{
		return m_RollClockwise;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidRollClockwise() const
{
	return m_RollClockwise_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetRollAnticlockwise() const
{
		return m_RollAnticlockwise;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidRollAnticlockwise() const
{
	return m_RollAnticlockwise_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetZoomIn() const
{
		return m_ZoomIn;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidZoomIn() const
{
	return m_ZoomIn_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetZoomOut() const
{
		return m_ZoomOut;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidZoomOut() const
{
	return m_ZoomOut_Exist;
}

Mp7Jrsunsigned7Ptr Mp7JrsFractionalPresenceType::GetFixed() const
{
		return m_Fixed;
}

// Element is optional
bool Mp7JrsFractionalPresenceType::IsValidFixed() const
{
	return m_Fixed_Exist;
}

void Mp7JrsFractionalPresenceType::SetTrackLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetTrackLeft().");
	}
	if (m_TrackLeft != item || m_TrackLeft_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TrackLeft);
		m_TrackLeft = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TrackLeft) m_TrackLeft->SetParent(m_myself.getPointer());
		if (m_TrackLeft && m_TrackLeft->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TrackLeft->UseTypeAttribute = true;
		}
		m_TrackLeft_Exist = true;
		if(m_TrackLeft != Mp7Jrsunsigned7Ptr())
		{
			m_TrackLeft->SetContentName(XMLString::transcode("TrackLeft"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateTrackLeft()
{
	m_TrackLeft_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetTrackRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetTrackRight().");
	}
	if (m_TrackRight != item || m_TrackRight_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TrackRight);
		m_TrackRight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TrackRight) m_TrackRight->SetParent(m_myself.getPointer());
		if (m_TrackRight && m_TrackRight->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TrackRight->UseTypeAttribute = true;
		}
		m_TrackRight_Exist = true;
		if(m_TrackRight != Mp7Jrsunsigned7Ptr())
		{
			m_TrackRight->SetContentName(XMLString::transcode("TrackRight"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateTrackRight()
{
	m_TrackRight_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetBoomDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetBoomDown().");
	}
	if (m_BoomDown != item || m_BoomDown_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BoomDown);
		m_BoomDown = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BoomDown) m_BoomDown->SetParent(m_myself.getPointer());
		if (m_BoomDown && m_BoomDown->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BoomDown->UseTypeAttribute = true;
		}
		m_BoomDown_Exist = true;
		if(m_BoomDown != Mp7Jrsunsigned7Ptr())
		{
			m_BoomDown->SetContentName(XMLString::transcode("BoomDown"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateBoomDown()
{
	m_BoomDown_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetBoomUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetBoomUp().");
	}
	if (m_BoomUp != item || m_BoomUp_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_BoomUp);
		m_BoomUp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BoomUp) m_BoomUp->SetParent(m_myself.getPointer());
		if (m_BoomUp && m_BoomUp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BoomUp->UseTypeAttribute = true;
		}
		m_BoomUp_Exist = true;
		if(m_BoomUp != Mp7Jrsunsigned7Ptr())
		{
			m_BoomUp->SetContentName(XMLString::transcode("BoomUp"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateBoomUp()
{
	m_BoomUp_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetDollyForward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetDollyForward().");
	}
	if (m_DollyForward != item || m_DollyForward_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DollyForward);
		m_DollyForward = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DollyForward) m_DollyForward->SetParent(m_myself.getPointer());
		if (m_DollyForward && m_DollyForward->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DollyForward->UseTypeAttribute = true;
		}
		m_DollyForward_Exist = true;
		if(m_DollyForward != Mp7Jrsunsigned7Ptr())
		{
			m_DollyForward->SetContentName(XMLString::transcode("DollyForward"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateDollyForward()
{
	m_DollyForward_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetDollyBackward(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetDollyBackward().");
	}
	if (m_DollyBackward != item || m_DollyBackward_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DollyBackward);
		m_DollyBackward = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DollyBackward) m_DollyBackward->SetParent(m_myself.getPointer());
		if (m_DollyBackward && m_DollyBackward->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DollyBackward->UseTypeAttribute = true;
		}
		m_DollyBackward_Exist = true;
		if(m_DollyBackward != Mp7Jrsunsigned7Ptr())
		{
			m_DollyBackward->SetContentName(XMLString::transcode("DollyBackward"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateDollyBackward()
{
	m_DollyBackward_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetPanLeft(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetPanLeft().");
	}
	if (m_PanLeft != item || m_PanLeft_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PanLeft);
		m_PanLeft = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PanLeft) m_PanLeft->SetParent(m_myself.getPointer());
		if (m_PanLeft && m_PanLeft->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PanLeft->UseTypeAttribute = true;
		}
		m_PanLeft_Exist = true;
		if(m_PanLeft != Mp7Jrsunsigned7Ptr())
		{
			m_PanLeft->SetContentName(XMLString::transcode("PanLeft"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidatePanLeft()
{
	m_PanLeft_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetPanRight(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetPanRight().");
	}
	if (m_PanRight != item || m_PanRight_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PanRight);
		m_PanRight = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PanRight) m_PanRight->SetParent(m_myself.getPointer());
		if (m_PanRight && m_PanRight->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PanRight->UseTypeAttribute = true;
		}
		m_PanRight_Exist = true;
		if(m_PanRight != Mp7Jrsunsigned7Ptr())
		{
			m_PanRight->SetContentName(XMLString::transcode("PanRight"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidatePanRight()
{
	m_PanRight_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetTiltDown(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetTiltDown().");
	}
	if (m_TiltDown != item || m_TiltDown_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TiltDown);
		m_TiltDown = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TiltDown) m_TiltDown->SetParent(m_myself.getPointer());
		if (m_TiltDown && m_TiltDown->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TiltDown->UseTypeAttribute = true;
		}
		m_TiltDown_Exist = true;
		if(m_TiltDown != Mp7Jrsunsigned7Ptr())
		{
			m_TiltDown->SetContentName(XMLString::transcode("TiltDown"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateTiltDown()
{
	m_TiltDown_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetTiltUp(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetTiltUp().");
	}
	if (m_TiltUp != item || m_TiltUp_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TiltUp);
		m_TiltUp = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TiltUp) m_TiltUp->SetParent(m_myself.getPointer());
		if (m_TiltUp && m_TiltUp->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TiltUp->UseTypeAttribute = true;
		}
		m_TiltUp_Exist = true;
		if(m_TiltUp != Mp7Jrsunsigned7Ptr())
		{
			m_TiltUp->SetContentName(XMLString::transcode("TiltUp"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateTiltUp()
{
	m_TiltUp_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetRollClockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetRollClockwise().");
	}
	if (m_RollClockwise != item || m_RollClockwise_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RollClockwise);
		m_RollClockwise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RollClockwise) m_RollClockwise->SetParent(m_myself.getPointer());
		if (m_RollClockwise && m_RollClockwise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RollClockwise->UseTypeAttribute = true;
		}
		m_RollClockwise_Exist = true;
		if(m_RollClockwise != Mp7Jrsunsigned7Ptr())
		{
			m_RollClockwise->SetContentName(XMLString::transcode("RollClockwise"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateRollClockwise()
{
	m_RollClockwise_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetRollAnticlockwise(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetRollAnticlockwise().");
	}
	if (m_RollAnticlockwise != item || m_RollAnticlockwise_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RollAnticlockwise);
		m_RollAnticlockwise = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RollAnticlockwise) m_RollAnticlockwise->SetParent(m_myself.getPointer());
		if (m_RollAnticlockwise && m_RollAnticlockwise->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RollAnticlockwise->UseTypeAttribute = true;
		}
		m_RollAnticlockwise_Exist = true;
		if(m_RollAnticlockwise != Mp7Jrsunsigned7Ptr())
		{
			m_RollAnticlockwise->SetContentName(XMLString::transcode("RollAnticlockwise"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateRollAnticlockwise()
{
	m_RollAnticlockwise_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetZoomIn(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetZoomIn().");
	}
	if (m_ZoomIn != item || m_ZoomIn_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ZoomIn);
		m_ZoomIn = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ZoomIn) m_ZoomIn->SetParent(m_myself.getPointer());
		if (m_ZoomIn && m_ZoomIn->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ZoomIn->UseTypeAttribute = true;
		}
		m_ZoomIn_Exist = true;
		if(m_ZoomIn != Mp7Jrsunsigned7Ptr())
		{
			m_ZoomIn->SetContentName(XMLString::transcode("ZoomIn"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateZoomIn()
{
	m_ZoomIn_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetZoomOut(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetZoomOut().");
	}
	if (m_ZoomOut != item || m_ZoomOut_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ZoomOut);
		m_ZoomOut = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ZoomOut) m_ZoomOut->SetParent(m_myself.getPointer());
		if (m_ZoomOut && m_ZoomOut->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ZoomOut->UseTypeAttribute = true;
		}
		m_ZoomOut_Exist = true;
		if(m_ZoomOut != Mp7Jrsunsigned7Ptr())
		{
			m_ZoomOut->SetContentName(XMLString::transcode("ZoomOut"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateZoomOut()
{
	m_ZoomOut_Exist = false;
}
void Mp7JrsFractionalPresenceType::SetFixed(const Mp7Jrsunsigned7Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFractionalPresenceType::SetFixed().");
	}
	if (m_Fixed != item || m_Fixed_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Fixed);
		m_Fixed = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Fixed) m_Fixed->SetParent(m_myself.getPointer());
		if (m_Fixed && m_Fixed->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Fixed->UseTypeAttribute = true;
		}
		m_Fixed_Exist = true;
		if(m_Fixed != Mp7Jrsunsigned7Ptr())
		{
			m_Fixed->SetContentName(XMLString::transcode("Fixed"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFractionalPresenceType::InvalidateFixed()
{
	m_Fixed_Exist = false;
}

Dc1NodeEnum Mp7JrsFractionalPresenceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TrackLeft")) == 0)
	{
		// TrackLeft is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTrackLeft()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTrackLeft(p, client);
					if((p = GetTrackLeft()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TrackRight")) == 0)
	{
		// TrackRight is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTrackRight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTrackRight(p, client);
					if((p = GetTrackRight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BoomDown")) == 0)
	{
		// BoomDown is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBoomDown()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBoomDown(p, client);
					if((p = GetBoomDown()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("BoomUp")) == 0)
	{
		// BoomUp is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBoomUp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBoomUp(p, client);
					if((p = GetBoomUp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DollyForward")) == 0)
	{
		// DollyForward is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDollyForward()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDollyForward(p, client);
					if((p = GetDollyForward()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("DollyBackward")) == 0)
	{
		// DollyBackward is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDollyBackward()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDollyBackward(p, client);
					if((p = GetDollyBackward()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PanLeft")) == 0)
	{
		// PanLeft is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPanLeft()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPanLeft(p, client);
					if((p = GetPanLeft()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PanRight")) == 0)
	{
		// PanRight is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPanRight()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPanRight(p, client);
					if((p = GetPanRight()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TiltDown")) == 0)
	{
		// TiltDown is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTiltDown()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTiltDown(p, client);
					if((p = GetTiltDown()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TiltUp")) == 0)
	{
		// TiltUp is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTiltUp()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTiltUp(p, client);
					if((p = GetTiltUp()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RollClockwise")) == 0)
	{
		// RollClockwise is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRollClockwise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRollClockwise(p, client);
					if((p = GetRollClockwise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RollAnticlockwise")) == 0)
	{
		// RollAnticlockwise is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRollAnticlockwise()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRollAnticlockwise(p, client);
					if((p = GetRollAnticlockwise()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ZoomIn")) == 0)
	{
		// ZoomIn is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetZoomIn()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetZoomIn(p, client);
					if((p = GetZoomIn()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ZoomOut")) == 0)
	{
		// ZoomOut is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetZoomOut()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetZoomOut(p, client);
					if((p = GetZoomOut()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Fixed")) == 0)
	{
		// Fixed is simple element unsigned7
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFixed()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned7")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned7Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFixed(p, client);
					if((p = GetFixed()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FractionalPresenceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FractionalPresenceType");
	}
	return result;
}

XMLCh * Mp7JrsFractionalPresenceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFractionalPresenceType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFractionalPresenceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FractionalPresenceType"));
	// Element serialization:
	// Class with content		
	
	if (m_TrackLeft != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TrackLeft->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TrackLeft"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TrackLeft->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TrackRight != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TrackRight->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TrackRight"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TrackRight->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_BoomDown != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BoomDown->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BoomDown"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BoomDown->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_BoomUp != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BoomUp->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BoomUp"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BoomUp->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_DollyForward != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DollyForward->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DollyForward"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DollyForward->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_DollyBackward != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DollyBackward->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DollyBackward"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DollyBackward->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PanLeft != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PanLeft->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PanLeft"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PanLeft->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_PanRight != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PanRight->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PanRight"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PanRight->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TiltDown != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TiltDown->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TiltDown"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TiltDown->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_TiltUp != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TiltUp->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TiltUp"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TiltUp->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_RollClockwise != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RollClockwise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RollClockwise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RollClockwise->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_RollAnticlockwise != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RollAnticlockwise->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RollAnticlockwise"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RollAnticlockwise->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_ZoomIn != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ZoomIn->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ZoomIn"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ZoomIn->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_ZoomOut != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ZoomOut->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ZoomOut"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ZoomOut->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Fixed != Mp7Jrsunsigned7Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Fixed->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Fixed"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Fixed->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFractionalPresenceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TrackLeft"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TrackLeft")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTrackLeft(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TrackRight"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TrackRight")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTrackRight(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BoomDown"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BoomDown")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBoomDown(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BoomUp"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BoomUp")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBoomUp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DollyForward"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DollyForward")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDollyForward(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DollyBackward"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DollyBackward")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDollyBackward(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PanLeft"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PanLeft")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPanLeft(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PanRight"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PanRight")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPanRight(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TiltDown"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TiltDown")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTiltDown(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TiltUp"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TiltUp")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTiltUp(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RollClockwise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RollClockwise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRollClockwise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RollAnticlockwise"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RollAnticlockwise")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRollAnticlockwise(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ZoomIn"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ZoomIn")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetZoomIn(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ZoomOut"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ZoomOut")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetZoomOut(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Fixed"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Fixed")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned7; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFixed(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFractionalPresenceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TrackLeft")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetTrackLeft(child);
  }
  if (XMLString::compareString(elementname, X("TrackRight")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetTrackRight(child);
  }
  if (XMLString::compareString(elementname, X("BoomDown")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetBoomDown(child);
  }
  if (XMLString::compareString(elementname, X("BoomUp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetBoomUp(child);
  }
  if (XMLString::compareString(elementname, X("DollyForward")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetDollyForward(child);
  }
  if (XMLString::compareString(elementname, X("DollyBackward")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetDollyBackward(child);
  }
  if (XMLString::compareString(elementname, X("PanLeft")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetPanLeft(child);
  }
  if (XMLString::compareString(elementname, X("PanRight")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetPanRight(child);
  }
  if (XMLString::compareString(elementname, X("TiltDown")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetTiltDown(child);
  }
  if (XMLString::compareString(elementname, X("TiltUp")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetTiltUp(child);
  }
  if (XMLString::compareString(elementname, X("RollClockwise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetRollClockwise(child);
  }
  if (XMLString::compareString(elementname, X("RollAnticlockwise")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetRollAnticlockwise(child);
  }
  if (XMLString::compareString(elementname, X("ZoomIn")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetZoomIn(child);
  }
  if (XMLString::compareString(elementname, X("ZoomOut")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetZoomOut(child);
  }
  if (XMLString::compareString(elementname, X("Fixed")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned7; // FTT, check this
	}
	this->SetFixed(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFractionalPresenceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTrackLeft() != Dc1NodePtr())
		result.Insert(GetTrackLeft());
	if (GetTrackRight() != Dc1NodePtr())
		result.Insert(GetTrackRight());
	if (GetBoomDown() != Dc1NodePtr())
		result.Insert(GetBoomDown());
	if (GetBoomUp() != Dc1NodePtr())
		result.Insert(GetBoomUp());
	if (GetDollyForward() != Dc1NodePtr())
		result.Insert(GetDollyForward());
	if (GetDollyBackward() != Dc1NodePtr())
		result.Insert(GetDollyBackward());
	if (GetPanLeft() != Dc1NodePtr())
		result.Insert(GetPanLeft());
	if (GetPanRight() != Dc1NodePtr())
		result.Insert(GetPanRight());
	if (GetTiltDown() != Dc1NodePtr())
		result.Insert(GetTiltDown());
	if (GetTiltUp() != Dc1NodePtr())
		result.Insert(GetTiltUp());
	if (GetRollClockwise() != Dc1NodePtr())
		result.Insert(GetRollClockwise());
	if (GetRollAnticlockwise() != Dc1NodePtr())
		result.Insert(GetRollAnticlockwise());
	if (GetZoomIn() != Dc1NodePtr())
		result.Insert(GetZoomIn());
	if (GetZoomOut() != Dc1NodePtr())
		result.Insert(GetZoomOut());
	if (GetFixed() != Dc1NodePtr())
		result.Insert(GetFixed());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFractionalPresenceType_ExtMethodImpl.h


