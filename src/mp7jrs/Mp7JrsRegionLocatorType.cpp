
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtImplInclude.h


#include "Mp7JrsRegionLocatorType_CoordRef_LocalType.h"
#include "Mp7JrsRegionLocatorType_Box_CollectionType.h"
#include "Mp7JrsRegionLocatorType_Polygon_CollectionType.h"
#include "Mp7JrsRegionLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsRegionLocatorType_Box_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Box
#include "Mp7JrsRegionLocatorType_Polygon_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Polygon

#include <assert.h>
IMp7JrsRegionLocatorType::IMp7JrsRegionLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtPropInit.h

}

IMp7JrsRegionLocatorType::~IMp7JrsRegionLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtPropCleanup.h

}

Mp7JrsRegionLocatorType::Mp7JrsRegionLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsRegionLocatorType::~Mp7JrsRegionLocatorType()
{
	Cleanup();
}

void Mp7JrsRegionLocatorType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_CoordRef = Mp7JrsRegionLocatorType_CoordRef_LocalPtr(); // Class
	m_CoordRef_Exist = false;
	m_Box = Mp7JrsRegionLocatorType_Box_CollectionPtr(); // Collection
	m_Polygon = Mp7JrsRegionLocatorType_Polygon_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtMyPropInit.h

}

void Mp7JrsRegionLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_CoordRef);
	// Dc1Factory::DeleteObject(m_Box);
	// Dc1Factory::DeleteObject(m_Polygon);
}

void Mp7JrsRegionLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use RegionLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in IRegionLocatorType
	const Dc1Ptr< Mp7JrsRegionLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidCoordRef())
	{
		// Dc1Factory::DeleteObject(m_CoordRef);
		this->SetCoordRef(Dc1Factory::CloneObject(tmp->GetCoordRef()));
	}
	else
	{
		InvalidateCoordRef();
	}
		// Dc1Factory::DeleteObject(m_Box);
		this->SetBox(Dc1Factory::CloneObject(tmp->GetBox()));
		// Dc1Factory::DeleteObject(m_Polygon);
		this->SetPolygon(Dc1Factory::CloneObject(tmp->GetPolygon()));
}

Dc1NodePtr Mp7JrsRegionLocatorType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsRegionLocatorType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsRegionLocatorType_CoordRef_LocalPtr Mp7JrsRegionLocatorType::GetCoordRef() const
{
		return m_CoordRef;
}

// Element is optional
bool Mp7JrsRegionLocatorType::IsValidCoordRef() const
{
	return m_CoordRef_Exist;
}

Mp7JrsRegionLocatorType_Box_CollectionPtr Mp7JrsRegionLocatorType::GetBox() const
{
		return m_Box;
}

Mp7JrsRegionLocatorType_Polygon_CollectionPtr Mp7JrsRegionLocatorType::GetPolygon() const
{
		return m_Polygon;
}

void Mp7JrsRegionLocatorType::SetCoordRef(const Mp7JrsRegionLocatorType_CoordRef_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRegionLocatorType::SetCoordRef().");
	}
	if (m_CoordRef != item || m_CoordRef_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CoordRef);
		m_CoordRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CoordRef) m_CoordRef->SetParent(m_myself.getPointer());
		if (m_CoordRef && m_CoordRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_CoordRef_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CoordRef->UseTypeAttribute = true;
		}
		m_CoordRef_Exist = true;
		if(m_CoordRef != Mp7JrsRegionLocatorType_CoordRef_LocalPtr())
		{
			m_CoordRef->SetContentName(XMLString::transcode("CoordRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRegionLocatorType::InvalidateCoordRef()
{
	m_CoordRef_Exist = false;
}
void Mp7JrsRegionLocatorType::SetBox(const Mp7JrsRegionLocatorType_Box_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRegionLocatorType::SetBox().");
	}
	if (m_Box != item)
	{
		// Dc1Factory::DeleteObject(m_Box);
		m_Box = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Box) m_Box->SetParent(m_myself.getPointer());
		if(m_Box != Mp7JrsRegionLocatorType_Box_CollectionPtr())
		{
			m_Box->SetContentName(XMLString::transcode("Box"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsRegionLocatorType::SetPolygon(const Mp7JrsRegionLocatorType_Polygon_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsRegionLocatorType::SetPolygon().");
	}
	if (m_Polygon != item)
	{
		// Dc1Factory::DeleteObject(m_Polygon);
		m_Polygon = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Polygon) m_Polygon->SetParent(m_myself.getPointer());
		if(m_Polygon != Mp7JrsRegionLocatorType_Polygon_CollectionPtr())
		{
			m_Polygon->SetContentName(XMLString::transcode("Polygon"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsRegionLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("CoordRef")) == 0)
	{
		// CoordRef is simple element RegionLocatorType_CoordRef_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCoordRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_CoordRef_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorType_CoordRef_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCoordRef(p, client);
					if((p = GetCoordRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Box")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Box is item of type RegionLocatorType_Box_LocalType
		// in element collection RegionLocatorType_Box_CollectionType
		
		context->Found = true;
		Mp7JrsRegionLocatorType_Box_CollectionPtr coll = GetBox();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRegionLocatorType_Box_CollectionType; // FTT, check this
				SetBox(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Box_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorType_Box_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Polygon")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Polygon is item of type RegionLocatorType_Polygon_LocalType
		// in element collection RegionLocatorType_Polygon_CollectionType
		
		context->Found = true;
		Mp7JrsRegionLocatorType_Polygon_CollectionPtr coll = GetPolygon();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateRegionLocatorType_Polygon_CollectionType; // FTT, check this
				SetPolygon(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType_Polygon_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorType_Polygon_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for RegionLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "RegionLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsRegionLocatorType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsRegionLocatorType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsRegionLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("RegionLocatorType"));
	// Element serialization:
	// Class
	
	if (m_CoordRef != Mp7JrsRegionLocatorType_CoordRef_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CoordRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CoordRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CoordRef->Serialize(doc, element, &elem);
	}
	if (m_Box != Mp7JrsRegionLocatorType_Box_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Box->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Polygon != Mp7JrsRegionLocatorType_Polygon_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Polygon->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsRegionLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CoordRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CoordRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionLocatorType_CoordRef_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCoordRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Box"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Box")) == 0))
		{
			// Deserialize factory type
			Mp7JrsRegionLocatorType_Box_CollectionPtr tmp = CreateRegionLocatorType_Box_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetBox(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Polygon"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Polygon")) == 0))
		{
			// Deserialize factory type
			Mp7JrsRegionLocatorType_Polygon_CollectionPtr tmp = CreateRegionLocatorType_Polygon_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPolygon(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsRegionLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("CoordRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionLocatorType_CoordRef_LocalType; // FTT, check this
	}
	this->SetCoordRef(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Box")) == 0))
  {
	Mp7JrsRegionLocatorType_Box_CollectionPtr tmp = CreateRegionLocatorType_Box_CollectionType; // FTT, check this
	this->SetBox(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Polygon")) == 0))
  {
	Mp7JrsRegionLocatorType_Polygon_CollectionPtr tmp = CreateRegionLocatorType_Polygon_CollectionType; // FTT, check this
	this->SetPolygon(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsRegionLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCoordRef() != Dc1NodePtr())
		result.Insert(GetCoordRef());
	if (GetBox() != Dc1NodePtr())
		result.Insert(GetBox());
	if (GetPolygon() != Dc1NodePtr())
		result.Insert(GetPolygon());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsRegionLocatorType_ExtMethodImpl.h


