
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentManagementType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsUserDescriptionType_UserPreferences_CollectionType.h"
#include "Mp7JrsUserDescriptionType_UsageHistory_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsUserDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsUserPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UserPreferences
#include "Mp7JrsUsageHistoryType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UsageHistory

#include <assert.h>
IMp7JrsUserDescriptionType::IMp7JrsUserDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtPropInit.h

}

IMp7JrsUserDescriptionType::~IMp7JrsUserDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtPropCleanup.h

}

Mp7JrsUserDescriptionType::Mp7JrsUserDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserDescriptionType::~Mp7JrsUserDescriptionType()
{
	Cleanup();
}

void Mp7JrsUserDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentManagementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_User = Dc1Ptr< Dc1Node >(); // Class
	m_User_Exist = false;
	m_UserPreferences = Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr(); // Collection
	m_UsageHistory = Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtMyPropInit.h

}

void Mp7JrsUserDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_User);
	// Dc1Factory::DeleteObject(m_UserPreferences);
	// Dc1Factory::DeleteObject(m_UsageHistory);
}

void Mp7JrsUserDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IUserDescriptionType
	const Dc1Ptr< Mp7JrsUserDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentManagementPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUserDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidUser())
	{
		// Dc1Factory::DeleteObject(m_User);
		this->SetUser(Dc1Factory::CloneObject(tmp->GetUser()));
	}
	else
	{
		InvalidateUser();
	}
		// Dc1Factory::DeleteObject(m_UserPreferences);
		this->SetUserPreferences(Dc1Factory::CloneObject(tmp->GetUserPreferences()));
		// Dc1Factory::DeleteObject(m_UsageHistory);
		this->SetUsageHistory(Dc1Factory::CloneObject(tmp->GetUsageHistory()));
}

Dc1NodePtr Mp7JrsUserDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUserDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentManagementType > Mp7JrsUserDescriptionType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Dc1Node > Mp7JrsUserDescriptionType::GetUser() const
{
		return m_User;
}

// Element is optional
bool Mp7JrsUserDescriptionType::IsValidUser() const
{
	return m_User_Exist;
}

Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr Mp7JrsUserDescriptionType::GetUserPreferences() const
{
		return m_UserPreferences;
}

Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr Mp7JrsUserDescriptionType::GetUsageHistory() const
{
		return m_UsageHistory;
}

void Mp7JrsUserDescriptionType::SetUser(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetUser().");
	}
	if (m_User != item || m_User_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_User);
		m_User = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_User) m_User->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_User) m_User->UseTypeAttribute = true;
		m_User_Exist = true;
		if(m_User != Dc1Ptr< Dc1Node >())
		{
			m_User->SetContentName(XMLString::transcode("User"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserDescriptionType::InvalidateUser()
{
	m_User_Exist = false;
}
void Mp7JrsUserDescriptionType::SetUserPreferences(const Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetUserPreferences().");
	}
	if (m_UserPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_UserPreferences);
		m_UserPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserPreferences) m_UserPreferences->SetParent(m_myself.getPointer());
		if(m_UserPreferences != Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr())
		{
			m_UserPreferences->SetContentName(XMLString::transcode("UserPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserDescriptionType::SetUsageHistory(const Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetUsageHistory().");
	}
	if (m_UsageHistory != item)
	{
		// Dc1Factory::DeleteObject(m_UsageHistory);
		m_UsageHistory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UsageHistory) m_UsageHistory->SetParent(m_myself.getPointer());
		if(m_UsageHistory != Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr())
		{
			m_UsageHistory->SetContentName(XMLString::transcode("UsageHistory"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsUserDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsUserDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsUserDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsUserDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsUserDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsUserDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUserDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("User")) == 0)
	{
		// User is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetUser()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetUser(p, client);
					if((p = GetUser()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UserPreferences is item of type UserPreferencesType
		// in element collection UserDescriptionType_UserPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr coll = GetUserPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserDescriptionType_UserPreferences_CollectionType; // FTT, check this
				SetUserPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UsageHistory")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UsageHistory is item of type UsageHistoryType
		// in element collection UserDescriptionType_UsageHistory_CollectionType
		
		context->Found = true;
		Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr coll = GetUsageHistory();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserDescriptionType_UsageHistory_CollectionType; // FTT, check this
				SetUsageHistory(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UsageHistoryType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUsageHistoryPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsUserDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsUserDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsUserDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserDescriptionType"));
	// Element serialization:
	// Class
	
	if (m_User != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_User->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("User"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_User->UseTypeAttribute = true;
		}
		m_User->Serialize(doc, element, &elem);
	}
	if (m_UserPreferences != Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UserPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_UsageHistory != Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UsageHistory->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUserDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentManagementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("User"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("User")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetUser(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UserPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UserPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr tmp = CreateUserDescriptionType_UserPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUserPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UsageHistory"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UsageHistory")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr tmp = CreateUserDescriptionType_UsageHistory_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUsageHistory(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("User")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetUser(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UserPreferences")) == 0))
  {
	Mp7JrsUserDescriptionType_UserPreferences_CollectionPtr tmp = CreateUserDescriptionType_UserPreferences_CollectionType; // FTT, check this
	this->SetUserPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UsageHistory")) == 0))
  {
	Mp7JrsUserDescriptionType_UsageHistory_CollectionPtr tmp = CreateUserDescriptionType_UsageHistory_CollectionType; // FTT, check this
	this->SetUsageHistory(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetUser() != Dc1NodePtr())
		result.Insert(GetUser());
	if (GetUserPreferences() != Dc1NodePtr())
		result.Insert(GetUserPreferences());
	if (GetUsageHistory() != Dc1NodePtr())
		result.Insert(GetUsageHistory());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserDescriptionType_ExtMethodImpl.h


