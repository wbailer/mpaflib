
// Based on PatternType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/regx/RegularExpression.hpp>
#include <xercesc/util/regx/Match.hpp>
#include <assert.h>
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsmediaDurationType_ExtImplInclude.h


#include "Mp7JrsmediaDurationType.h"

Mp7JrsmediaDurationType::Mp7JrsmediaDurationType()
	: m_nsURI(X("urn:mpeg:mpeg7:schema:2004"))
{
		Init();
}

Mp7JrsmediaDurationType::~Mp7JrsmediaDurationType()
{
		Cleanup();
}

void Mp7JrsmediaDurationType::Init()
{	   
		m_DurationSign = 1;
		m_Days = -1;
		m_Hours = -1;
		m_Minutes = -1;
		m_Seconds = -1;
		m_NumberOfFractions = -1;
		m_Time_Exist = false;
		m_FractionsPerSecond = -1;

// no includefile for extension defined 
// file Mp7JrsmediaDurationType_ExtPropInit.h

}

void Mp7JrsmediaDurationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsmediaDurationType_ExtPropCleanup.h


}

void Mp7JrsmediaDurationType::DeepCopy(const Dc1NodePtr &original)
{
		Dc1Ptr< Mp7JrsmediaDurationType > tmp = original;
		if (tmp == NULL) return; // EXIT: wrong type passed
		Cleanup();
		// FIXME: this will fail for derived classes
  
		this->Parse(tmp->ToText());
		
		this->m_ContentName = NULL;
}

Dc1NodePtr Mp7JrsmediaDurationType::GetBaseRoot()
{
		return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsmediaDurationType::GetBaseRoot() const
{
		return m_myself.getPointer();
}

int Mp7JrsmediaDurationType::GetDurationSign() const
{
		return m_DurationSign;
}

void Mp7JrsmediaDurationType::SetDurationSign(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetDurationSign().");
		}
		m_DurationSign = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetDays() const
{
		return m_Days;
}

void Mp7JrsmediaDurationType::SetDays(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetDays().");
		}
		m_Days = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetHours() const
{
		return m_Hours;
}

void Mp7JrsmediaDurationType::SetHours(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetHours().");
		}
		m_Hours = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetMinutes() const
{
		return m_Minutes;
}

void Mp7JrsmediaDurationType::SetMinutes(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetMinutes().");
		}
		m_Minutes = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetSeconds() const
{
		return m_Seconds;
}

void Mp7JrsmediaDurationType::SetSeconds(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetSeconds().");
		}
		m_Seconds = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetNumberOfFractions() const
{
		return m_NumberOfFractions;
}

void Mp7JrsmediaDurationType::SetNumberOfFractions(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetNumberOfFractions().");
		}
		m_NumberOfFractions = val;
		m_Time_Exist = true;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
bool Mp7JrsmediaDurationType::GetTime_Exist() const
{
		return m_Time_Exist;
}

void Mp7JrsmediaDurationType::SetTime_Exist(bool val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetTime_Exist().");
		}
		m_Time_Exist = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}
int Mp7JrsmediaDurationType::GetFractionsPerSecond() const
{
		return m_FractionsPerSecond;
}

void Mp7JrsmediaDurationType::SetFractionsPerSecond(int val, Dc1ClientID client)
{
		if (!IsLocked(client)) {
				throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsmediaDurationType::SetFractionsPerSecond().");
		}
		m_FractionsPerSecond = val;
		Dc1ClientManager::SendUpdateNotification(
				m_myself.getPointer(), DC1_NA_NODE_UPDATED,
				client, m_myself.getPointer());
}


XMLCh * Mp7JrsmediaDurationType::ToText() const
{
		XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
		XMLCh * tmp = NULL;

		if ( m_Days >= 0 || m_Time_Exist || m_FractionsPerSecond >= 0 ) {

			if(m_DurationSign < 0)
			{
				Dc1Util::CatString(&buf, X("-"));
			}


			Dc1Util::CatString(&buf, X("P"));

			if(m_Days >= 0)
			{
				tmp = Dc1Convert::BinToText(m_Days);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("D"));
				XMLString::release(&tmp);
			}

			if(m_Time_Exist)
			{
				Dc1Util::CatString(&buf, X("T"));

				if(m_Hours >= 0)
				{
					tmp = Dc1Convert::BinToText(m_Hours);
					Dc1Util::CatString(&buf, tmp);
					Dc1Util::CatString(&buf, X("H"));
					XMLString::release(&tmp);
				}

				if(m_Minutes >= 0)
				{
					tmp = Dc1Convert::BinToText(m_Minutes);
					Dc1Util::CatString(&buf, tmp);
					Dc1Util::CatString(&buf, X("M"));
					XMLString::release(&tmp);
				}

				if(m_Seconds >= 0)
				{
					tmp = Dc1Convert::BinToText(m_Seconds);
					Dc1Util::CatString(&buf, tmp);
					Dc1Util::CatString(&buf, X("S"));
					XMLString::release(&tmp);
				}

				if(m_NumberOfFractions >= 0)
				{
					tmp = Dc1Convert::BinToText(m_NumberOfFractions);
					Dc1Util::CatString(&buf, tmp);
					Dc1Util::CatString(&buf, X("N"));
					XMLString::release(&tmp);
				}
			}

			if(m_FractionsPerSecond >= 0)
			{
				tmp = Dc1Convert::BinToText(m_FractionsPerSecond);
				Dc1Util::CatString(&buf, tmp);
				Dc1Util::CatString(&buf, X("F"));
				XMLString::release(&tmp);
			}
		}
		return buf;
}


/////////////////////////////////////////////////////////////////

bool Mp7JrsmediaDurationType::Parse(const XMLCh * const txt)
{
		Match m;
		XMLCh substring [128] = {0};
		RegularExpression r("^\\-?P(\\d+D)?(T(\\d+H)?(\\d+M)?(\\d+S)?(\\d+N)?)?(\\d+F)?$");
		// This is for Xerces 3.1.1+
		if(r.matches(txt, &m))
		{
				for(int i = 0; i < m.getNoGroups(); i++)
				{
						int i1 = m.getStartPos(i);
						int i2 = Dc1Util::GetNextEndPos(m, i);
						if(i1 == i2) continue; // ignore missing optional or wrong group
						switch(i)
						{
								case 0:
										if(i1 >= 0 && i2 - i1 > 1)
										{
												// \\-?P
												m_DurationSign = -1;
										}
								break;

								case 1:
										if(i1 >= 0)
										{
												// (\\d+D)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Days = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								case 2:
										if(i1 >= 0)
										{
												// T
												// Nothing to do
										}
								break;

								case 3:
										if(i1 >= 0)
										{
												// (\\d+H)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Hours = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 4:
										if(i1 >= 0)
										{
												// (\\d+M)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Minutes = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 5:
										if(i1 >= 0)
										{
												// (\\d+S)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_Seconds = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 6:
										if(i1 >= 0)
										{
												// (\\d+N)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_NumberOfFractions = Dc1Convert::TextToUnsignedInt(substring);
												m_Time_Exist = true;
										}
								break;

								case 7:
										if(i1 >= 0)
										{
												// (\\d+F)?
												XERCES_CPP_NAMESPACE_QUALIFIER XMLString::subString(substring, txt, i1, i2 - 1);
												m_FractionsPerSecond = Dc1Convert::TextToUnsignedInt(substring);
										}
								break;

								default: return true;
								break;
						}
				}
		}
		else return false;
		return true;
}

bool Mp7JrsmediaDurationType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsmediaDurationType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsmediaDurationType::ContentToString() const
{
	// Mp7JrsmediaDurationType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsmediaDurationType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
		// The element we serialize into
		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

		XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
		if (!newElem) newElem = &localNewElem;

		if (*newElem) {
				element = *newElem;
		} else if(parent == NULL) {
				element = doc->getDocumentElement();
				*newElem = element;
		} else {
				if(this->GetContentName() != (XMLCh*)NULL) {
//  OLD					  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
						DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
						parent->appendChild(elem);
						*newElem = elem;
						element = elem;
				} else
						assert(false);
		}
		assert(element);

		if(this->UseTypeAttribute && ! element->hasAttribute(X("xsi:type")))
		{
			element->setAttribute(X("xsi:type"), X(Dc1Factory::GetTypeName(Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))));
		}

// no includefile for extension defined 
// file Mp7JrsmediaDurationType_ExtPreImplementCleanup.h


		XMLCh * buf = this->ToText();
		if(buf != NULL)
		{
				element->appendChild(doc->createTextNode(buf));
				XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&buf);
		}
}

bool Mp7JrsmediaDurationType::Deserialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * parent,  XERCES_CPP_NAMESPACE_QUALIFIER DOMElement ** current)
{
// no includefile for extension defined 
// file Mp7JrsmediaDurationType_ExtPostDeserialize.h


		bool found = this->Parse(Dc1Util::GetElementText(parent));
		if(found)
		{
				* current = parent;
		}
		return found;   
}

// begin extension included
// file Mp7JrsmediaDurationType_ExtMethodImpl.h


long long IMp7JrsmediaDurationType::GetTotalNrFractions() {
	long long sum = 0LL; // In V 2.8. we have switched to 64 bit long long
	// Days and months missing!!!!!
	long long val = GetHours() > 0 ? GetHours() : 0LL; // FTT: -1 is used to determine hours not set. This is bogus!
	sum = 60LL * (val + sum);
	val = GetMinutes() > 0 ? GetMinutes() : 0LL; // FTT: -1 is used to determine minutes not set. This is bogus!
	sum = 60LL * (val + sum);
	val = GetFractionsPerSecond() > 0 ? GetFractionsPerSecond() : 0LL; // FTT: -1 is used to determine fractions per second not set. This is bogus!
	long long val1 = GetSeconds() > 0 ? GetSeconds() : 0LL; // FTT: -1 is used to determine seconds not set. This is bogus!
	sum = val * (val1 + sum);
	val = GetNumberOfFractions() > 0 ? GetNumberOfFractions() : 0LL; // FTT: -1 is used to determine fractions not set. This is bogus!
	sum += val; 
	return sum;
}

void IMp7JrsmediaDurationType::SetTotalNrFractions(long long fractions, Dc1ClientID client) {
	long long remainder = fractions;
	SetNumberOfFractions((int)(remainder % GetFractionsPerSecond()),client);
	remainder /= GetFractionsPerSecond();
	SetSeconds((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetMinutes((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetHours((int)remainder,client);
	// Days and months ignored!!!!
}

// end extension included








