
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtImplInclude.h


#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0()
{

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtPropInit.h

}

IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::~IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtPropCleanup.h

}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::~Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0()
{
	Cleanup();
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_CurrPixel = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr(); // Class
	m_SrcPixel = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtMyPropInit.h

}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_CurrPixel);
	// Dc1Factory::DeleteObject(m_SrcPixel);
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0Ptr, since we
	// might need GetBase(), which isn't defined in ISpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0
	const Dc1Ptr< Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_CurrPixel);
		this->SetCurrPixel(Dc1Factory::CloneObject(tmp->GetCurrPixel()));
		// Dc1Factory::DeleteObject(m_SrcPixel);
		this->SetSrcPixel(Dc1Factory::CloneObject(tmp->GetSrcPixel()));
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetCurrPixel() const
{
		return m_CurrPixel;
}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetSrcPixel() const
{
		return m_SrcPixel;
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::SetCurrPixel(const Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::SetCurrPixel().");
	}
	if (m_CurrPixel != item)
	{
		// Dc1Factory::DeleteObject(m_CurrPixel);
		m_CurrPixel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CurrPixel) m_CurrPixel->SetParent(m_myself.getPointer());
		if (m_CurrPixel && m_CurrPixel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CurrPixel->UseTypeAttribute = true;
		}
		if(m_CurrPixel != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr())
		{
			m_CurrPixel->SetContentName(XMLString::transcode("CurrPixel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::SetSrcPixel(const Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::SetSrcPixel().");
	}
	if (m_SrcPixel != item)
	{
		// Dc1Factory::DeleteObject(m_SrcPixel);
		m_SrcPixel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SrcPixel) m_SrcPixel->SetParent(m_myself.getPointer());
		if (m_SrcPixel && m_SrcPixel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SrcPixel->UseTypeAttribute = true;
		}
		if(m_SrcPixel != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr())
		{
			m_SrcPixel->SetContentName(XMLString::transcode("SrcPixel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0"));
	// Element serialization:
	// Class
	
	if (m_CurrPixel != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CurrPixel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CurrPixel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CurrPixel->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SrcPixel != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SrcPixel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SrcPixel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SrcPixel->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CurrPixel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CurrPixel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCurrPixel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SrcPixel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SrcPixel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSrcPixel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("CurrPixel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType; // FTT, check this
	}
	this->SetCurrPixel(child);
  }
  if (XMLString::compareString(elementname, X("SrcPixel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType; // FTT, check this
	}
	this->SetSrcPixel(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCurrPixel() != Dc1NodePtr())
		result.Insert(GetCurrPixel());
	if (GetSrcPixel() != Dc1NodePtr())
		result.Insert(GetSrcPixel());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0_ExtMethodImpl.h


