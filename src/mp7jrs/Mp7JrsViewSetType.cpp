
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtImplInclude.h


#include "Mp7JrsViewDecompositionType.h"
#include "Mp7JrsViewSetType_setProperty_LocalType2.h"
#include "Mp7JrsViewSetType_CollectionType.h"
#include "Mp7JrsSignalType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsViewSetType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsViewSetType_LocalType.h" // Choice collection View
#include "Mp7JrsViewType.h" // Choice collection element View
#include "Mp7JrsReferenceType.h" // Choice collection element ViewRef

#include <assert.h>
IMp7JrsViewSetType::IMp7JrsViewSetType()
{

// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtPropInit.h

}

IMp7JrsViewSetType::~IMp7JrsViewSetType()
{
// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtPropCleanup.h

}

Mp7JrsViewSetType::Mp7JrsViewSetType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsViewSetType::~Mp7JrsViewSetType()
{
	Cleanup();
}

void Mp7JrsViewSetType::Init()
{
	// Init base
	m_Base = CreateViewDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_setProperty = Mp7JrsViewSetType_setProperty_Local2Ptr(); // Create emtpy class ptr
	m_setProperty_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_ViewSetType_LocalType = Mp7JrsViewSetType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtMyPropInit.h

}

void Mp7JrsViewSetType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_setProperty);
	// Dc1Factory::DeleteObject(m_ViewSetType_LocalType);
}

void Mp7JrsViewSetType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ViewSetTypePtr, since we
	// might need GetBase(), which isn't defined in IViewSetType
	const Dc1Ptr< Mp7JrsViewSetType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsViewDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsViewSetType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_setProperty);
	if (tmp->ExistsetProperty())
	{
		this->SetsetProperty(Dc1Factory::CloneObject(tmp->GetsetProperty()));
	}
	else
	{
		InvalidatesetProperty();
	}
	}
		// Dc1Factory::DeleteObject(m_ViewSetType_LocalType);
		this->SetViewSetType_LocalType(Dc1Factory::CloneObject(tmp->GetViewSetType_LocalType()));
}

Dc1NodePtr Mp7JrsViewSetType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsViewSetType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsViewDecompositionType > Mp7JrsViewSetType::GetBase() const
{
	return m_Base;
}

Mp7JrsViewSetType_setProperty_Local2Ptr Mp7JrsViewSetType::GetsetProperty() const
{
	return m_setProperty;
}

bool Mp7JrsViewSetType::ExistsetProperty() const
{
	return m_setProperty_Exist;
}
void Mp7JrsViewSetType::SetsetProperty(const Mp7JrsViewSetType_setProperty_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetsetProperty().");
	}
	m_setProperty = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_setProperty) m_setProperty->SetParent(m_myself.getPointer());
	m_setProperty_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatesetProperty()
{
	m_setProperty_Exist = false;
}
Mp7JrsViewSetType_CollectionPtr Mp7JrsViewSetType::GetViewSetType_LocalType() const
{
		return m_ViewSetType_LocalType;
}

void Mp7JrsViewSetType::SetViewSetType_LocalType(const Mp7JrsViewSetType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetViewSetType_LocalType().");
	}
	if (m_ViewSetType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_ViewSetType_LocalType);
		m_ViewSetType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ViewSetType_LocalType) m_ViewSetType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsViewSetType::Getcomplete() const
{
	return GetBase()->Getcomplete();
}

bool Mp7JrsViewSetType::Existcomplete() const
{
	return GetBase()->Existcomplete();
}
void Mp7JrsViewSetType::Setcomplete(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::Setcomplete().");
	}
	GetBase()->Setcomplete(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::Invalidatecomplete()
{
	GetBase()->Invalidatecomplete();
}
bool Mp7JrsViewSetType::GetnonRedundant() const
{
	return GetBase()->GetnonRedundant();
}

bool Mp7JrsViewSetType::ExistnonRedundant() const
{
	return GetBase()->ExistnonRedundant();
}
void Mp7JrsViewSetType::SetnonRedundant(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetnonRedundant().");
	}
	GetBase()->SetnonRedundant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatenonRedundant()
{
	GetBase()->InvalidatenonRedundant();
}
Mp7JrsSignalPtr Mp7JrsViewSetType::GetSource() const
{
	return GetBase()->GetSource();
}

// Element is optional
bool Mp7JrsViewSetType::IsValidSource() const
{
	return GetBase()->IsValidSource();
}

void Mp7JrsViewSetType::SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetSource().");
	}
	GetBase()->SetSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidateSource()
{
	GetBase()->InvalidateSource();
}
XMLCh * Mp7JrsViewSetType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsViewSetType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsViewSetType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsViewSetType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsViewSetType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsViewSetType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsViewSetType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsViewSetType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsViewSetType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsViewSetType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsViewSetType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsViewSetType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsViewSetType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsViewSetType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsViewSetType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewSetType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsViewSetType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsViewSetType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewSetType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsViewSetType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("setProperty")) == 0)
	{
		// setProperty is simple attribute ViewSetType_setProperty_LocalType2
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsViewSetType_setProperty_Local2Ptr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("View")) == 0)
	{
		// View is contained in itemtype ViewSetType_LocalType
		// in choice collection ViewSetType_CollectionType

		context->Found = true;
		Mp7JrsViewSetType_CollectionPtr coll = GetViewSetType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateViewSetType_CollectionType; // FTT, check this
				SetViewSetType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->GetView()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsViewSetType_LocalPtr item = CreateViewSetType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->SetView(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->GetView()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ViewRef")) == 0)
	{
		// ViewRef is contained in itemtype ViewSetType_LocalType
		// in choice collection ViewSetType_CollectionType

		context->Found = true;
		Mp7JrsViewSetType_CollectionPtr coll = GetViewSetType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateViewSetType_CollectionType; // FTT, check this
				SetViewSetType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->GetViewRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsViewSetType_LocalPtr item = CreateViewSetType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->SetViewRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsViewSetType_LocalPtr)coll->elementAt(i))->GetViewRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ViewSetType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ViewSetType");
	}
	return result;
}

XMLCh * Mp7JrsViewSetType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsViewSetType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsViewSetType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ViewSetType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_setProperty_Exist)
	{
	// Class
	if (m_setProperty != Mp7JrsViewSetType_setProperty_Local2Ptr())
	{
		XMLCh * tmp = m_setProperty->ToText();
		element->setAttributeNS(X(""), X("setProperty"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_ViewSetType_LocalType != Mp7JrsViewSetType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ViewSetType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsViewSetType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("setProperty")))
	{
		// Deserialize class type
		Mp7JrsViewSetType_setProperty_Local2Ptr tmp = CreateViewSetType_setProperty_LocalType2; // FTT, check this
		tmp->Parse(parent->getAttribute(X("setProperty")));
		this->SetsetProperty(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsViewDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:View
			Dc1Util::HasNodeName(parent, X("View"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:View")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ViewRef
			Dc1Util::HasNodeName(parent, X("ViewRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ViewRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsViewSetType_CollectionPtr tmp = CreateViewSetType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetViewSetType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsViewSetType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("View"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("View")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ViewRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ViewRef")) == 0)
	)
  {
	Mp7JrsViewSetType_CollectionPtr tmp = CreateViewSetType_CollectionType; // FTT, check this
	this->SetViewSetType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsViewSetType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetViewSetType_LocalType() != Dc1NodePtr())
		result.Insert(GetViewSetType_LocalType());
	if (GetSource() != Dc1NodePtr())
		result.Insert(GetSource());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsViewSetType_ExtMethodImpl.h


