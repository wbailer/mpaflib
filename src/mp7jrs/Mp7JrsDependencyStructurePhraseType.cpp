
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtImplInclude.h


#include "Mp7JrsDependencyStructurePhraseType_operator_LocalType.h"
#include "Mp7JrsDependencyStructurePhraseType_CollectionType.h"
#include "Mp7JrsWordFormType.h"
#include "Mp7JrsNonDependencyStructurePhraseType.h"
#include "Mp7JrsDependencyStructurePhraseType_CollectionType0.h"
#include "Mp7JrsDependencyStructurePhraseType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsDependencyStructurePhraseType_LocalType.h" // Choice collection Quotation
#include "Mp7JrsDependencyStructurePhraseType.h" // Choice collection element Quotation
#include "Mp7JrsDependencyStructurePhraseType_LocalType0.h" // Choice collection Quotation

#include <assert.h>
IMp7JrsDependencyStructurePhraseType::IMp7JrsDependencyStructurePhraseType()
{

// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtPropInit.h

}

IMp7JrsDependencyStructurePhraseType::~IMp7JrsDependencyStructurePhraseType()
{
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtPropCleanup.h

}

Mp7JrsDependencyStructurePhraseType::Mp7JrsDependencyStructurePhraseType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDependencyStructurePhraseType::~Mp7JrsDependencyStructurePhraseType()
{
	Cleanup();
}

void Mp7JrsDependencyStructurePhraseType::Init()
{

	// Init attributes
	m_id = NULL; // String
	m_id_Exist = false;
	m_equal = NULL; // String
	m_equal_Exist = false;
	m_operator = Mp7JrsDependencyStructurePhraseType_operator_LocalPtr(); // Create emtpy class ptr
	m_operator_Exist = false;
	m_functionWord = NULL; // String
	m_functionWord_Exist = false;
	m_synthesis = NULL; // String
	m_synthesis_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_DependencyStructurePhraseType_LocalType = Mp7JrsDependencyStructurePhraseType_CollectionPtr(); // Collection
	m_Head = Mp7JrsWordFormPtr(); // Class
	m_CompoundHead = Mp7JrsNonDependencyStructurePhrasePtr(); // Class
	m_DependencyStructurePhraseType_LocalType0 = Mp7JrsDependencyStructurePhraseType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtMyPropInit.h

}

void Mp7JrsDependencyStructurePhraseType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtMyPropCleanup.h


	XMLString::release(&m_id); // String
	XMLString::release(&m_equal); // String
	// Dc1Factory::DeleteObject(m_operator);
	XMLString::release(&m_functionWord); // String
	XMLString::release(&m_synthesis); // String
	// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType);
	// Dc1Factory::DeleteObject(m_Head);
	// Dc1Factory::DeleteObject(m_CompoundHead);
	// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType0);
}

void Mp7JrsDependencyStructurePhraseType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DependencyStructurePhraseTypePtr, since we
	// might need GetBase(), which isn't defined in IDependencyStructurePhraseType
	const Dc1Ptr< Mp7JrsDependencyStructurePhraseType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_id); // String
	if (tmp->Existid())
	{
		this->Setid(XMLString::replicate(tmp->Getid()));
	}
	else
	{
		Invalidateid();
	}
	}
	{
	XMLString::release(&m_equal); // String
	if (tmp->Existequal())
	{
		this->Setequal(XMLString::replicate(tmp->Getequal()));
	}
	else
	{
		Invalidateequal();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_operator);
	if (tmp->Existoperator())
	{
		this->Setoperator(Dc1Factory::CloneObject(tmp->Getoperator()));
	}
	else
	{
		Invalidateoperator();
	}
	}
	{
	XMLString::release(&m_functionWord); // String
	if (tmp->ExistfunctionWord())
	{
		this->SetfunctionWord(XMLString::replicate(tmp->GetfunctionWord()));
	}
	else
	{
		InvalidatefunctionWord();
	}
	}
	{
	XMLString::release(&m_synthesis); // String
	if (tmp->Existsynthesis())
	{
		this->Setsynthesis(XMLString::replicate(tmp->Getsynthesis()));
	}
	else
	{
		Invalidatesynthesis();
	}
	}
		// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType);
		this->SetDependencyStructurePhraseType_LocalType(Dc1Factory::CloneObject(tmp->GetDependencyStructurePhraseType_LocalType()));
		// Dc1Factory::DeleteObject(m_Head);
		this->SetHead(Dc1Factory::CloneObject(tmp->GetHead()));
		// Dc1Factory::DeleteObject(m_CompoundHead);
		this->SetCompoundHead(Dc1Factory::CloneObject(tmp->GetCompoundHead()));
		// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType0);
		this->SetDependencyStructurePhraseType_LocalType0(Dc1Factory::CloneObject(tmp->GetDependencyStructurePhraseType_LocalType0()));
}

Dc1NodePtr Mp7JrsDependencyStructurePhraseType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsDependencyStructurePhraseType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsDependencyStructurePhraseType::Getid() const
{
	return m_id;
}

bool Mp7JrsDependencyStructurePhraseType::Existid() const
{
	return m_id_Exist;
}
void Mp7JrsDependencyStructurePhraseType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::Setid().");
	}
	m_id = item;
	m_id_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::Invalidateid()
{
	m_id_Exist = false;
}
XMLCh * Mp7JrsDependencyStructurePhraseType::Getequal() const
{
	return m_equal;
}

bool Mp7JrsDependencyStructurePhraseType::Existequal() const
{
	return m_equal_Exist;
}
void Mp7JrsDependencyStructurePhraseType::Setequal(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::Setequal().");
	}
	m_equal = item;
	m_equal_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::Invalidateequal()
{
	m_equal_Exist = false;
}
Mp7JrsDependencyStructurePhraseType_operator_LocalPtr Mp7JrsDependencyStructurePhraseType::Getoperator() const
{
	return m_operator;
}

bool Mp7JrsDependencyStructurePhraseType::Existoperator() const
{
	return m_operator_Exist;
}
void Mp7JrsDependencyStructurePhraseType::Setoperator(const Mp7JrsDependencyStructurePhraseType_operator_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::Setoperator().");
	}
	m_operator = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_operator) m_operator->SetParent(m_myself.getPointer());
	m_operator_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::Invalidateoperator()
{
	m_operator_Exist = false;
}
XMLCh * Mp7JrsDependencyStructurePhraseType::GetfunctionWord() const
{
	return m_functionWord;
}

bool Mp7JrsDependencyStructurePhraseType::ExistfunctionWord() const
{
	return m_functionWord_Exist;
}
void Mp7JrsDependencyStructurePhraseType::SetfunctionWord(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::SetfunctionWord().");
	}
	m_functionWord = item;
	m_functionWord_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::InvalidatefunctionWord()
{
	m_functionWord_Exist = false;
}
XMLCh * Mp7JrsDependencyStructurePhraseType::Getsynthesis() const
{
	return m_synthesis;
}

bool Mp7JrsDependencyStructurePhraseType::Existsynthesis() const
{
	return m_synthesis_Exist;
}
void Mp7JrsDependencyStructurePhraseType::Setsynthesis(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::Setsynthesis().");
	}
	// TODO: Attribute is fixed -- check the value
	m_synthesis = item;
	m_synthesis_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::Invalidatesynthesis()
{
	m_synthesis_Exist = false;
}
Mp7JrsDependencyStructurePhraseType_CollectionPtr Mp7JrsDependencyStructurePhraseType::GetDependencyStructurePhraseType_LocalType() const
{
		return m_DependencyStructurePhraseType_LocalType;
}

Mp7JrsWordFormPtr Mp7JrsDependencyStructurePhraseType::GetHead() const
{
		return m_Head;
}

Mp7JrsNonDependencyStructurePhrasePtr Mp7JrsDependencyStructurePhraseType::GetCompoundHead() const
{
		return m_CompoundHead;
}

Mp7JrsDependencyStructurePhraseType_Collection0Ptr Mp7JrsDependencyStructurePhraseType::GetDependencyStructurePhraseType_LocalType0() const
{
		return m_DependencyStructurePhraseType_LocalType0;
}

void Mp7JrsDependencyStructurePhraseType::SetDependencyStructurePhraseType_LocalType(const Mp7JrsDependencyStructurePhraseType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::SetDependencyStructurePhraseType_LocalType().");
	}
	if (m_DependencyStructurePhraseType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType);
		m_DependencyStructurePhraseType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DependencyStructurePhraseType_LocalType) m_DependencyStructurePhraseType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsDependencyStructurePhraseType::SetHead(const Mp7JrsWordFormPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::SetHead().");
	}
	if (m_Head != item)
	{
		// Dc1Factory::DeleteObject(m_Head);
		m_Head = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Head) m_Head->SetParent(m_myself.getPointer());
		if (m_Head && m_Head->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Head->UseTypeAttribute = true;
		}
		if(m_Head != Mp7JrsWordFormPtr())
		{
			m_Head->SetContentName(XMLString::transcode("Head"));
		}
	// Dc1Factory::DeleteObject(m_CompoundHead);
	m_CompoundHead = Mp7JrsNonDependencyStructurePhrasePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsDependencyStructurePhraseType::SetCompoundHead(const Mp7JrsNonDependencyStructurePhrasePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::SetCompoundHead().");
	}
	if (m_CompoundHead != item)
	{
		// Dc1Factory::DeleteObject(m_CompoundHead);
		m_CompoundHead = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompoundHead) m_CompoundHead->SetParent(m_myself.getPointer());
		if (m_CompoundHead && m_CompoundHead->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CompoundHead->UseTypeAttribute = true;
		}
		if(m_CompoundHead != Mp7JrsNonDependencyStructurePhrasePtr())
		{
			m_CompoundHead->SetContentName(XMLString::transcode("CompoundHead"));
		}
	// Dc1Factory::DeleteObject(m_Head);
	m_Head = Mp7JrsWordFormPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDependencyStructurePhraseType::SetDependencyStructurePhraseType_LocalType0(const Mp7JrsDependencyStructurePhraseType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDependencyStructurePhraseType::SetDependencyStructurePhraseType_LocalType0().");
	}
	if (m_DependencyStructurePhraseType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_DependencyStructurePhraseType_LocalType0);
		m_DependencyStructurePhraseType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DependencyStructurePhraseType_LocalType0) m_DependencyStructurePhraseType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDependencyStructurePhraseType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("id")) == 0)
	{
		// id is simple attribute ID
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("equal")) == 0)
	{
		// equal is simple attribute IDREF
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("operator")) == 0)
	{
		// operator is simple attribute DependencyStructurePhraseType_operator_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsDependencyStructurePhraseType_operator_LocalPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("functionWord")) == 0)
	{
		// functionWord is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("synthesis")) == 0)
	{
		// synthesis is simple attribute NMTOKEN
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quotation")) == 0)
	{
		// Quotation is contained in itemtype DependencyStructurePhraseType_LocalType
		// in choice collection DependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsDependencyStructurePhraseType_CollectionPtr coll = GetDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsDependencyStructurePhraseType_LocalPtr item = CreateDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetQuotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetQuotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Phrase")) == 0)
	{
		// Phrase is contained in itemtype DependencyStructurePhraseType_LocalType
		// in choice collection DependencyStructurePhraseType_CollectionType

		context->Found = true;
		Mp7JrsDependencyStructurePhraseType_CollectionPtr coll = GetDependencyStructurePhraseType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDependencyStructurePhraseType_CollectionType; // FTT, check this
				SetDependencyStructurePhraseType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsDependencyStructurePhraseType_LocalPtr item = CreateDependencyStructurePhraseType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->SetPhrase(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsDependencyStructurePhraseType_LocalPtr)coll->elementAt(i))->GetPhrase()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Head")) == 0)
	{
		// Head is simple element WordFormType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHead()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:WordFormType")))) != empty)
			{
				// Is type allowed
				Mp7JrsWordFormPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHead(p, client);
					if((p = GetHead()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CompoundHead")) == 0)
	{
		// CompoundHead is simple element NonDependencyStructurePhraseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCompoundHead()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NonDependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNonDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCompoundHead(p, client);
					if((p = GetCompoundHead()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quotation")) == 0)
	{
		// Quotation is contained in itemtype DependencyStructurePhraseType_LocalType0
		// in choice collection DependencyStructurePhraseType_CollectionType0

		context->Found = true;
		Mp7JrsDependencyStructurePhraseType_Collection0Ptr coll = GetDependencyStructurePhraseType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDependencyStructurePhraseType_CollectionType0; // FTT, check this
				SetDependencyStructurePhraseType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->GetQuotation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsDependencyStructurePhraseType_Local0Ptr item = CreateDependencyStructurePhraseType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->SetQuotation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->GetQuotation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Phrase")) == 0)
	{
		// Phrase is contained in itemtype DependencyStructurePhraseType_LocalType0
		// in choice collection DependencyStructurePhraseType_CollectionType0

		context->Found = true;
		Mp7JrsDependencyStructurePhraseType_Collection0Ptr coll = GetDependencyStructurePhraseType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDependencyStructurePhraseType_CollectionType0; // FTT, check this
				SetDependencyStructurePhraseType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->GetPhrase()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsDependencyStructurePhraseType_Local0Ptr item = CreateDependencyStructurePhraseType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DependencyStructurePhraseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDependencyStructurePhrasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->SetPhrase(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsDependencyStructurePhraseType_Local0Ptr)coll->elementAt(i))->GetPhrase()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DependencyStructurePhraseType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DependencyStructurePhraseType");
	}
	return result;
}

XMLCh * Mp7JrsDependencyStructurePhraseType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsDependencyStructurePhraseType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsDependencyStructurePhraseType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DependencyStructurePhraseType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_id_Exist)
	{
	// String
	if(m_id != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("id"), m_id);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_equal_Exist)
	{
	// String
	if(m_equal != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("equal"), m_equal);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_operator_Exist)
	{
	// Class
	if (m_operator != Mp7JrsDependencyStructurePhraseType_operator_LocalPtr())
	{
		XMLCh * tmp = m_operator->ToText();
		element->setAttributeNS(X(""), X("operator"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_functionWord_Exist)
	{
	// String
	if(m_functionWord != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("functionWord"), m_functionWord);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_synthesis_Exist)
	{
	// String
	if(m_synthesis != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("synthesis"), m_synthesis);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_DependencyStructurePhraseType_LocalType != Mp7JrsDependencyStructurePhraseType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DependencyStructurePhraseType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Head != Mp7JrsWordFormPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Head->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Head"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Head->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CompoundHead != Mp7JrsNonDependencyStructurePhrasePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CompoundHead->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CompoundHead"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CompoundHead->Serialize(doc, element, &elem);
	}
	if (m_DependencyStructurePhraseType_LocalType0 != Mp7JrsDependencyStructurePhraseType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_DependencyStructurePhraseType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDependencyStructurePhraseType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("id")))
	{
		// Deserialize string type
		this->Setid(Dc1Convert::TextToString(parent->getAttribute(X("id"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("equal")))
	{
		// Deserialize string type
		this->Setequal(Dc1Convert::TextToString(parent->getAttribute(X("equal"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("operator")))
	{
		// Deserialize class type
		Mp7JrsDependencyStructurePhraseType_operator_LocalPtr tmp = CreateDependencyStructurePhraseType_operator_LocalType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("operator")));
		this->Setoperator(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("functionWord")))
	{
		// Deserialize string type
		this->SetfunctionWord(Dc1Convert::TextToString(parent->getAttribute(X("functionWord"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("synthesis")))
	{
		// Deserialize string type
		this->Setsynthesis(Dc1Convert::TextToString(parent->getAttribute(X("synthesis"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Quotation
			Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Quotation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Phrase
			Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Phrase")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsDependencyStructurePhraseType_CollectionPtr tmp = CreateDependencyStructurePhraseType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDependencyStructurePhraseType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Head"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Head")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateWordFormType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHead(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CompoundHead"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CompoundHead")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateNonDependencyStructurePhraseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCompoundHead(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Quotation
			Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Quotation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Phrase
			Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Phrase")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsDependencyStructurePhraseType_Collection0Ptr tmp = CreateDependencyStructurePhraseType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDependencyStructurePhraseType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDependencyStructurePhraseType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Quotation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Phrase")) == 0)
	)
  {
	Mp7JrsDependencyStructurePhraseType_CollectionPtr tmp = CreateDependencyStructurePhraseType_CollectionType; // FTT, check this
	this->SetDependencyStructurePhraseType_LocalType(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Head")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateWordFormType; // FTT, check this
	}
	this->SetHead(child);
  }
  if (XMLString::compareString(elementname, X("CompoundHead")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateNonDependencyStructurePhraseType; // FTT, check this
	}
	this->SetCompoundHead(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Quotation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Quotation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Phrase"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Phrase")) == 0)
	)
  {
	Mp7JrsDependencyStructurePhraseType_Collection0Ptr tmp = CreateDependencyStructurePhraseType_CollectionType0; // FTT, check this
	this->SetDependencyStructurePhraseType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDependencyStructurePhraseType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDependencyStructurePhraseType_LocalType() != Dc1NodePtr())
		result.Insert(GetDependencyStructurePhraseType_LocalType());
	if (GetDependencyStructurePhraseType_LocalType0() != Dc1NodePtr())
		result.Insert(GetDependencyStructurePhraseType_LocalType0());
	if (GetHead() != Dc1NodePtr())
		result.Insert(GetHead());
	if (GetCompoundHead() != Dc1NodePtr())
		result.Insert(GetCompoundHead());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDependencyStructurePhraseType_ExtMethodImpl.h


