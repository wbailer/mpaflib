
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtImplInclude.h


#include "Mp7JrsMpeg7Type.h"
#include "Mp7JrsMpeg7BaseType.h"
#include "Mp7JrsMpeg7_Description_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDescriptionProfileType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsMpeg7_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsCompleteDescriptionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Description

#include <assert.h>
IMp7JrsMpeg7_LocalType::IMp7JrsMpeg7_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtPropInit.h

}

IMp7JrsMpeg7_LocalType::~IMp7JrsMpeg7_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtPropCleanup.h

}

Mp7JrsMpeg7_LocalType::Mp7JrsMpeg7_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMpeg7_LocalType::~Mp7JrsMpeg7_LocalType()
{
	Cleanup();
}

void Mp7JrsMpeg7_LocalType::Init()
{
	// Init base
	m_Base = CreateMpeg7Type; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_DescriptionUnit = Dc1Ptr< Dc1Node >(); // Class
	m_Description = Mp7JrsMpeg7_Description_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMyPropInit.h

}

void Mp7JrsMpeg7_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_DescriptionUnit);
	// Dc1Factory::DeleteObject(m_Description);
}

void Mp7JrsMpeg7_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Mpeg7_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMpeg7_LocalType
	const Dc1Ptr< Mp7JrsMpeg7_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMpeg7Ptr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMpeg7_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_DescriptionUnit);
		this->SetDescriptionUnit(Dc1Factory::CloneObject(tmp->GetDescriptionUnit()));
		// Dc1Factory::DeleteObject(m_Description);
		this->SetDescription(Dc1Factory::CloneObject(tmp->GetDescription()));
}

Dc1NodePtr Mp7JrsMpeg7_LocalType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsMpeg7_LocalType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMpeg7Type > Mp7JrsMpeg7_LocalType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Dc1Node > Mp7JrsMpeg7_LocalType::GetDescriptionUnit() const
{
		return m_DescriptionUnit;
}

Mp7JrsMpeg7_Description_CollectionPtr Mp7JrsMpeg7_LocalType::GetDescription() const
{
		return m_Description;
}

// implementing setter for choice 
/* element */
void Mp7JrsMpeg7_LocalType::SetDescriptionUnit(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetDescriptionUnit().");
	}
	if (m_DescriptionUnit != item)
	{
		// Dc1Factory::DeleteObject(m_DescriptionUnit);
		m_DescriptionUnit = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DescriptionUnit) m_DescriptionUnit->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_DescriptionUnit) m_DescriptionUnit->UseTypeAttribute = true;
		if(m_DescriptionUnit != Dc1Ptr< Dc1Node >())
		{
			m_DescriptionUnit->SetContentName(XMLString::transcode("DescriptionUnit"));
		}
	// Dc1Factory::DeleteObject(m_Description);
	m_Description = Mp7JrsMpeg7_Description_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMpeg7_LocalType::SetDescription(const Mp7JrsMpeg7_Description_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetDescription().");
	}
	if (m_Description != item)
	{
		// Dc1Factory::DeleteObject(m_Description);
		m_Description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Description) m_Description->SetParent(m_myself.getPointer());
		if(m_Description != Mp7JrsMpeg7_Description_CollectionPtr())
		{
			m_Description->SetContentName(XMLString::transcode("Description"));
		}
	// Dc1Factory::DeleteObject(m_DescriptionUnit);
	m_DescriptionUnit = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMpeg7_LocalType::Getlang() const
{
	return GetBase()->Getlang();
}

bool Mp7JrsMpeg7_LocalType::Existlang() const
{
	return GetBase()->Existlang();
}
void Mp7JrsMpeg7_LocalType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::Setlang().");
	}
	GetBase()->Setlang(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::Invalidatelang()
{
	GetBase()->Invalidatelang();
}
Mp7JrsxPathRefPtr Mp7JrsMpeg7_LocalType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsMpeg7_LocalType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsMpeg7_LocalType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMpeg7_LocalType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsMpeg7_LocalType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsMpeg7_LocalType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMpeg7_LocalType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMpeg7_LocalType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMpeg7_LocalType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMpeg7_LocalType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMpeg7_LocalType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMpeg7_LocalType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDescriptionProfilePtr Mp7JrsMpeg7_LocalType::GetDescriptionProfile() const
{
	return GetBase()->GetDescriptionProfile();
}

// Element is optional
bool Mp7JrsMpeg7_LocalType::IsValidDescriptionProfile() const
{
	return GetBase()->IsValidDescriptionProfile();
}

Mp7JrsDescriptionMetadataPtr Mp7JrsMpeg7_LocalType::GetDescriptionMetadata() const
{
	return GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsMpeg7_LocalType::IsValidDescriptionMetadata() const
{
	return GetBase()->IsValidDescriptionMetadata();
}

void Mp7JrsMpeg7_LocalType::SetDescriptionProfile(const Mp7JrsDescriptionProfilePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetDescriptionProfile().");
	}
	GetBase()->SetDescriptionProfile(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidateDescriptionProfile()
{
	GetBase()->InvalidateDescriptionProfile();
}
void Mp7JrsMpeg7_LocalType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMpeg7_LocalType::SetDescriptionMetadata().");
	}
	GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMpeg7_LocalType::InvalidateDescriptionMetadata()
{
	GetBase()->InvalidateDescriptionMetadata();
}

Dc1NodeEnum Mp7JrsMpeg7_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DescriptionUnit")) == 0)
	{
		// DescriptionUnit is simple abstract element Mpeg7BaseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescriptionUnit()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMpeg7BasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescriptionUnit(p, client);
					if((p = GetDescriptionUnit()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Description")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Description is item of abstract type CompleteDescriptionType
		// in element collection Mpeg7_Description_CollectionType
		
		context->Found = true;
		Mp7JrsMpeg7_Description_CollectionPtr coll = GetDescription();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMpeg7_Description_CollectionType; // FTT, check this
				SetDescription(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCompleteDescriptionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Mpeg7_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Mpeg7_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMpeg7_LocalType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMpeg7_LocalType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMpeg7_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Mpeg7_LocalType"));
	// Element serialization:
	// Class
	
	if (m_DescriptionUnit != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DescriptionUnit->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DescriptionUnit"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_DescriptionUnit->UseTypeAttribute = true;
		}
		m_DescriptionUnit->Serialize(doc, element, &elem);
	}
	if (m_Description != Mp7JrsMpeg7_Description_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Description->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMpeg7_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMpeg7Type
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DescriptionUnit"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DescriptionUnit")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMpeg7BaseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescriptionUnit(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Description")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMpeg7_Description_CollectionPtr tmp = CreateMpeg7_Description_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDescription(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMpeg7_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("DescriptionUnit")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMpeg7BaseType; // FTT, check this
	}
	this->SetDescriptionUnit(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Description")) == 0))
  {
	Mp7JrsMpeg7_Description_CollectionPtr tmp = CreateMpeg7_Description_CollectionType; // FTT, check this
	this->SetDescription(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMpeg7_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDescriptionUnit() != Dc1NodePtr())
		result.Insert(GetDescriptionUnit());
	if (GetDescription() != Dc1NodePtr())
		result.Insert(GetDescription());
	if (GetDescriptionProfile() != Dc1NodePtr())
		result.Insert(GetDescriptionProfile());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMpeg7_LocalType_ExtMethodImpl.h


