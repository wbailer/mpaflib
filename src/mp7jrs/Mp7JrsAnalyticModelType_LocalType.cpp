
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtImplInclude.h


#include "Mp7JrsAnalyticModelType_Label_LocalType.h"
#include "Mp7JrsAnalyticModelType_Semantics_LocalType.h"
#include "Mp7JrsAnalyticModelType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsAnalyticModelType_LocalType::IMp7JrsAnalyticModelType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtPropInit.h

}

IMp7JrsAnalyticModelType_LocalType::~IMp7JrsAnalyticModelType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtPropCleanup.h

}

Mp7JrsAnalyticModelType_LocalType::Mp7JrsAnalyticModelType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAnalyticModelType_LocalType::~Mp7JrsAnalyticModelType_LocalType()
{
	Cleanup();
}

void Mp7JrsAnalyticModelType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Label = Mp7JrsAnalyticModelType_Label_LocalPtr(); // Class
	m_Semantics = Mp7JrsAnalyticModelType_Semantics_LocalPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtMyPropInit.h

}

void Mp7JrsAnalyticModelType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Label);
	// Dc1Factory::DeleteObject(m_Semantics);
}

void Mp7JrsAnalyticModelType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AnalyticModelType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAnalyticModelType_LocalType
	const Dc1Ptr< Mp7JrsAnalyticModelType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Label);
		this->SetLabel(Dc1Factory::CloneObject(tmp->GetLabel()));
		// Dc1Factory::DeleteObject(m_Semantics);
		this->SetSemantics(Dc1Factory::CloneObject(tmp->GetSemantics()));
}

Dc1NodePtr Mp7JrsAnalyticModelType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAnalyticModelType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsAnalyticModelType_Label_LocalPtr Mp7JrsAnalyticModelType_LocalType::GetLabel() const
{
		return m_Label;
}

Mp7JrsAnalyticModelType_Semantics_LocalPtr Mp7JrsAnalyticModelType_LocalType::GetSemantics() const
{
		return m_Semantics;
}

// implementing setter for choice 
/* element */
void Mp7JrsAnalyticModelType_LocalType::SetLabel(const Mp7JrsAnalyticModelType_Label_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType_LocalType::SetLabel().");
	}
	if (m_Label != item)
	{
		// Dc1Factory::DeleteObject(m_Label);
		m_Label = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Label) m_Label->SetParent(m_myself.getPointer());
		if (m_Label && m_Label->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Label_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Label->UseTypeAttribute = true;
		}
		if(m_Label != Mp7JrsAnalyticModelType_Label_LocalPtr())
		{
			m_Label->SetContentName(XMLString::transcode("Label"));
		}
	// Dc1Factory::DeleteObject(m_Semantics);
	m_Semantics = Mp7JrsAnalyticModelType_Semantics_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAnalyticModelType_LocalType::SetSemantics(const Mp7JrsAnalyticModelType_Semantics_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAnalyticModelType_LocalType::SetSemantics().");
	}
	if (m_Semantics != item)
	{
		// Dc1Factory::DeleteObject(m_Semantics);
		m_Semantics = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Semantics) m_Semantics->SetParent(m_myself.getPointer());
		if (m_Semantics && m_Semantics->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AnalyticModelType_Semantics_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Semantics->UseTypeAttribute = true;
		}
		if(m_Semantics != Mp7JrsAnalyticModelType_Semantics_LocalPtr())
		{
			m_Semantics->SetContentName(XMLString::transcode("Semantics"));
		}
	// Dc1Factory::DeleteObject(m_Label);
	m_Label = Mp7JrsAnalyticModelType_Label_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: AnalyticModelType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsAnalyticModelType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsAnalyticModelType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAnalyticModelType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAnalyticModelType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Label != Mp7JrsAnalyticModelType_Label_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Label->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Label"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Label->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Semantics != Mp7JrsAnalyticModelType_Semantics_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Semantics->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Semantics"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Semantics->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAnalyticModelType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Label"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Label")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAnalyticModelType_Label_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLabel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Semantics"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Semantics")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAnalyticModelType_Semantics_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSemantics(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAnalyticModelType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Label")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAnalyticModelType_Label_LocalType; // FTT, check this
	}
	this->SetLabel(child);
  }
  if (XMLString::compareString(elementname, X("Semantics")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAnalyticModelType_Semantics_LocalType; // FTT, check this
	}
	this->SetSemantics(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAnalyticModelType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLabel() != Dc1NodePtr())
		result.Insert(GetLabel());
	if (GetSemantics() != Dc1NodePtr())
		result.Insert(GetSemantics());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAnalyticModelType_LocalType_ExtMethodImpl.h


