
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtImplInclude.h


#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsReferenceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsReferenceType::IMp7JrsReferenceType()
{

// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtPropInit.h

}

IMp7JrsReferenceType::~IMp7JrsReferenceType()
{
// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtPropCleanup.h

}

Mp7JrsReferenceType::Mp7JrsReferenceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsReferenceType::~Mp7JrsReferenceType()
{
	Cleanup();
}

void Mp7JrsReferenceType::Init()
{

	// Init attributes
	m_idref = NULL; // String
	m_idref_Exist = false;
	m_xpath = Mp7JrsxPathRefPtr(); // Pattern
	m_xpath_Exist = false;
	m_href = NULL; // String
	m_href_Exist = false;



// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtMyPropInit.h

}

void Mp7JrsReferenceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtMyPropCleanup.h


	XMLString::release(&m_idref); // String
	// Dc1Factory::DeleteObject(m_xpath); // Pattern
	XMLString::release(&m_href); // String
}

void Mp7JrsReferenceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ReferenceTypePtr, since we
	// might need GetBase(), which isn't defined in IReferenceType
	const Dc1Ptr< Mp7JrsReferenceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_idref); // String
	if (tmp->Existidref())
	{
		this->Setidref(XMLString::replicate(tmp->Getidref()));
	}
	else
	{
		Invalidateidref();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_xpath); // Pattern
	if (tmp->Existxpath())
	{
		this->Setxpath(Dc1Factory::CloneObject(tmp->Getxpath()));
	}
	else
	{
		Invalidatexpath();
	}
	}
	{
	XMLString::release(&m_href); // String
	if (tmp->Existhref())
	{
		this->Sethref(XMLString::replicate(tmp->Gethref()));
	}
	else
	{
		Invalidatehref();
	}
	}
}

Dc1NodePtr Mp7JrsReferenceType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsReferenceType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsReferenceType::Getidref() const
{
	return m_idref;
}

bool Mp7JrsReferenceType::Existidref() const
{
	return m_idref_Exist;
}
void Mp7JrsReferenceType::Setidref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferenceType::Setidref().");
	}
	m_idref = item;
	m_idref_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferenceType::Invalidateidref()
{
	m_idref_Exist = false;
}
Mp7JrsxPathRefPtr Mp7JrsReferenceType::Getxpath() const
{
	return m_xpath;
}

bool Mp7JrsReferenceType::Existxpath() const
{
	return m_xpath_Exist;
}
void Mp7JrsReferenceType::Setxpath(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferenceType::Setxpath().");
	}
	m_xpath = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_xpath) m_xpath->SetParent(m_myself.getPointer());
	m_xpath_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferenceType::Invalidatexpath()
{
	m_xpath_Exist = false;
}
XMLCh * Mp7JrsReferenceType::Gethref() const
{
	return m_href;
}

bool Mp7JrsReferenceType::Existhref() const
{
	return m_href_Exist;
}
void Mp7JrsReferenceType::Sethref(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsReferenceType::Sethref().");
	}
	m_href = item;
	m_href_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsReferenceType::Invalidatehref()
{
	m_href_Exist = false;
}

Dc1NodeEnum Mp7JrsReferenceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	// Only rudimentary expressions allowed for this type with no child elements
	return result;
}	

XMLCh * Mp7JrsReferenceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsReferenceType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsReferenceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ReferenceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_idref_Exist)
	{
	// String
	if(m_idref != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("idref"), m_idref);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_xpath_Exist)
	{
	// Pattern
	if (m_xpath != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_xpath->ToText();
		element->setAttributeNS(X(""), X("xpath"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_href_Exist)
	{
	// String
	if(m_href != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("href"), m_href);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsReferenceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("idref")))
	{
		// Deserialize string type
		this->Setidref(Dc1Convert::TextToString(parent->getAttribute(X("idref"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xpath")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("xpath")));
		this->Setxpath(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("href")))
	{
		// Deserialize string type
		this->Sethref(Dc1Convert::TextToString(parent->getAttribute(X("href"))));
		* current = parent;
	}

// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsReferenceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsReferenceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsReferenceType_ExtMethodImpl.h


