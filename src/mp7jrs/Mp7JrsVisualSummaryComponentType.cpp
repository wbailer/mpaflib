
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsImageLocatorType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsRegionLocatorType.h"
#include "Mp7JrsVisualSummaryComponentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsVisualSummaryComponentType::IMp7JrsVisualSummaryComponentType()
{

// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtPropInit.h

}

IMp7JrsVisualSummaryComponentType::~IMp7JrsVisualSummaryComponentType()
{
// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtPropCleanup.h

}

Mp7JrsVisualSummaryComponentType::Mp7JrsVisualSummaryComponentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVisualSummaryComponentType::~Mp7JrsVisualSummaryComponentType()
{
	Cleanup();
}

void Mp7JrsVisualSummaryComponentType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_VideoSourceID = Mp7JrsUniqueIDPtr(); // Class
	m_VideoSourceID_Exist = false;
	m_VideoSourceLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_VideoSourceLocator_Exist = false;
	m_ComponentSourceTime = Mp7JrsMediaTimePtr(); // Class
	m_ComponentSourceTime_Exist = false;
	m_ImageLocator = Mp7JrsImageLocatorPtr(); // Class
	m_ImageLocator_Exist = false;
	m_SyncTime = Mp7JrsMediaTimePtr(); // Class
	m_SyncTime_Exist = false;
	m_FrameActivity = Mp7JrszeroToOnePtr(); // Class with content 
	m_FrameActivity_Exist = false;
	m_Region = Mp7JrsRegionLocatorPtr(); // Class
	m_Region_Exist = false;


// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtMyPropInit.h

}

void Mp7JrsVisualSummaryComponentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_VideoSourceID);
	// Dc1Factory::DeleteObject(m_VideoSourceLocator);
	// Dc1Factory::DeleteObject(m_ComponentSourceTime);
	// Dc1Factory::DeleteObject(m_ImageLocator);
	// Dc1Factory::DeleteObject(m_SyncTime);
	// Dc1Factory::DeleteObject(m_FrameActivity);
	// Dc1Factory::DeleteObject(m_Region);
}

void Mp7JrsVisualSummaryComponentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VisualSummaryComponentTypePtr, since we
	// might need GetBase(), which isn't defined in IVisualSummaryComponentType
	const Dc1Ptr< Mp7JrsVisualSummaryComponentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsVisualSummaryComponentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidVideoSourceID())
	{
		// Dc1Factory::DeleteObject(m_VideoSourceID);
		this->SetVideoSourceID(Dc1Factory::CloneObject(tmp->GetVideoSourceID()));
	}
	else
	{
		InvalidateVideoSourceID();
	}
	if (tmp->IsValidVideoSourceLocator())
	{
		// Dc1Factory::DeleteObject(m_VideoSourceLocator);
		this->SetVideoSourceLocator(Dc1Factory::CloneObject(tmp->GetVideoSourceLocator()));
	}
	else
	{
		InvalidateVideoSourceLocator();
	}
	if (tmp->IsValidComponentSourceTime())
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		this->SetComponentSourceTime(Dc1Factory::CloneObject(tmp->GetComponentSourceTime()));
	}
	else
	{
		InvalidateComponentSourceTime();
	}
	if (tmp->IsValidImageLocator())
	{
		// Dc1Factory::DeleteObject(m_ImageLocator);
		this->SetImageLocator(Dc1Factory::CloneObject(tmp->GetImageLocator()));
	}
	else
	{
		InvalidateImageLocator();
	}
	if (tmp->IsValidSyncTime())
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		this->SetSyncTime(Dc1Factory::CloneObject(tmp->GetSyncTime()));
	}
	else
	{
		InvalidateSyncTime();
	}
	if (tmp->IsValidFrameActivity())
	{
		// Dc1Factory::DeleteObject(m_FrameActivity);
		this->SetFrameActivity(Dc1Factory::CloneObject(tmp->GetFrameActivity()));
	}
	else
	{
		InvalidateFrameActivity();
	}
	if (tmp->IsValidRegion())
	{
		// Dc1Factory::DeleteObject(m_Region);
		this->SetRegion(Dc1Factory::CloneObject(tmp->GetRegion()));
	}
	else
	{
		InvalidateRegion();
	}
}

Dc1NodePtr Mp7JrsVisualSummaryComponentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsVisualSummaryComponentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsVisualSummaryComponentType::GetBase() const
{
	return m_Base;
}

Mp7JrsUniqueIDPtr Mp7JrsVisualSummaryComponentType::GetVideoSourceID() const
{
		return m_VideoSourceID;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidVideoSourceID() const
{
	return m_VideoSourceID_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsVisualSummaryComponentType::GetVideoSourceLocator() const
{
		return m_VideoSourceLocator;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidVideoSourceLocator() const
{
	return m_VideoSourceLocator_Exist;
}

Mp7JrsMediaTimePtr Mp7JrsVisualSummaryComponentType::GetComponentSourceTime() const
{
		return m_ComponentSourceTime;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidComponentSourceTime() const
{
	return m_ComponentSourceTime_Exist;
}

Mp7JrsImageLocatorPtr Mp7JrsVisualSummaryComponentType::GetImageLocator() const
{
		return m_ImageLocator;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidImageLocator() const
{
	return m_ImageLocator_Exist;
}

Mp7JrsMediaTimePtr Mp7JrsVisualSummaryComponentType::GetSyncTime() const
{
		return m_SyncTime;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidSyncTime() const
{
	return m_SyncTime_Exist;
}

Mp7JrszeroToOnePtr Mp7JrsVisualSummaryComponentType::GetFrameActivity() const
{
		return m_FrameActivity;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidFrameActivity() const
{
	return m_FrameActivity_Exist;
}

Mp7JrsRegionLocatorPtr Mp7JrsVisualSummaryComponentType::GetRegion() const
{
		return m_Region;
}

// Element is optional
bool Mp7JrsVisualSummaryComponentType::IsValidRegion() const
{
	return m_Region_Exist;
}

void Mp7JrsVisualSummaryComponentType::SetVideoSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetVideoSourceID().");
	}
	if (m_VideoSourceID != item || m_VideoSourceID_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VideoSourceID);
		m_VideoSourceID = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSourceID) m_VideoSourceID->SetParent(m_myself.getPointer());
		if (m_VideoSourceID && m_VideoSourceID->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSourceID->UseTypeAttribute = true;
		}
		m_VideoSourceID_Exist = true;
		if(m_VideoSourceID != Mp7JrsUniqueIDPtr())
		{
			m_VideoSourceID->SetContentName(XMLString::transcode("VideoSourceID"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateVideoSourceID()
{
	m_VideoSourceID_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetVideoSourceLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetVideoSourceLocator().");
	}
	if (m_VideoSourceLocator != item || m_VideoSourceLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_VideoSourceLocator);
		m_VideoSourceLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSourceLocator) m_VideoSourceLocator->SetParent(m_myself.getPointer());
		if (m_VideoSourceLocator && m_VideoSourceLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSourceLocator->UseTypeAttribute = true;
		}
		m_VideoSourceLocator_Exist = true;
		if(m_VideoSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_VideoSourceLocator->SetContentName(XMLString::transcode("VideoSourceLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateVideoSourceLocator()
{
	m_VideoSourceLocator_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetComponentSourceTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetComponentSourceTime().");
	}
	if (m_ComponentSourceTime != item || m_ComponentSourceTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		m_ComponentSourceTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ComponentSourceTime) m_ComponentSourceTime->SetParent(m_myself.getPointer());
		if (m_ComponentSourceTime && m_ComponentSourceTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ComponentSourceTime->UseTypeAttribute = true;
		}
		m_ComponentSourceTime_Exist = true;
		if(m_ComponentSourceTime != Mp7JrsMediaTimePtr())
		{
			m_ComponentSourceTime->SetContentName(XMLString::transcode("ComponentSourceTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateComponentSourceTime()
{
	m_ComponentSourceTime_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetImageLocator(const Mp7JrsImageLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetImageLocator().");
	}
	if (m_ImageLocator != item || m_ImageLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ImageLocator);
		m_ImageLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ImageLocator) m_ImageLocator->SetParent(m_myself.getPointer());
		if (m_ImageLocator && m_ImageLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ImageLocator->UseTypeAttribute = true;
		}
		m_ImageLocator_Exist = true;
		if(m_ImageLocator != Mp7JrsImageLocatorPtr())
		{
			m_ImageLocator->SetContentName(XMLString::transcode("ImageLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateImageLocator()
{
	m_ImageLocator_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetSyncTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetSyncTime().");
	}
	if (m_SyncTime != item || m_SyncTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		m_SyncTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SyncTime) m_SyncTime->SetParent(m_myself.getPointer());
		if (m_SyncTime && m_SyncTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SyncTime->UseTypeAttribute = true;
		}
		m_SyncTime_Exist = true;
		if(m_SyncTime != Mp7JrsMediaTimePtr())
		{
			m_SyncTime->SetContentName(XMLString::transcode("SyncTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateSyncTime()
{
	m_SyncTime_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetFrameActivity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetFrameActivity().");
	}
	if (m_FrameActivity != item || m_FrameActivity_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_FrameActivity);
		m_FrameActivity = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FrameActivity) m_FrameActivity->SetParent(m_myself.getPointer());
		if (m_FrameActivity && m_FrameActivity->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_FrameActivity->UseTypeAttribute = true;
		}
		m_FrameActivity_Exist = true;
		if(m_FrameActivity != Mp7JrszeroToOnePtr())
		{
			m_FrameActivity->SetContentName(XMLString::transcode("FrameActivity"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateFrameActivity()
{
	m_FrameActivity_Exist = false;
}
void Mp7JrsVisualSummaryComponentType::SetRegion(const Mp7JrsRegionLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualSummaryComponentType::SetRegion().");
	}
	if (m_Region != item || m_Region_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Region);
		m_Region = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Region) m_Region->SetParent(m_myself.getPointer());
		if (m_Region && m_Region->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Region->UseTypeAttribute = true;
		}
		m_Region_Exist = true;
		if(m_Region != Mp7JrsRegionLocatorPtr())
		{
			m_Region->SetContentName(XMLString::transcode("Region"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualSummaryComponentType::InvalidateRegion()
{
	m_Region_Exist = false;
}

Dc1NodeEnum Mp7JrsVisualSummaryComponentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("VideoSourceID")) == 0)
	{
		// VideoSourceID is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVideoSourceID()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVideoSourceID(p, client);
					if((p = GetVideoSourceID()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VideoSourceLocator")) == 0)
	{
		// VideoSourceLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetVideoSourceLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetVideoSourceLocator(p, client);
					if((p = GetVideoSourceLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ComponentSourceTime")) == 0)
	{
		// ComponentSourceTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetComponentSourceTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetComponentSourceTime(p, client);
					if((p = GetComponentSourceTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ImageLocator")) == 0)
	{
		// ImageLocator is simple element ImageLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetImageLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ImageLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsImageLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetImageLocator(p, client);
					if((p = GetImageLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SyncTime")) == 0)
	{
		// SyncTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSyncTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSyncTime(p, client);
					if((p = GetSyncTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FrameActivity")) == 0)
	{
		// FrameActivity is simple element zeroToOneType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFrameActivity()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType")))) != empty)
			{
				// Is type allowed
				Mp7JrszeroToOnePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFrameActivity(p, client);
					if((p = GetFrameActivity()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Region")) == 0)
	{
		// Region is simple element RegionLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRegion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRegion(p, client);
					if((p = GetRegion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VisualSummaryComponentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VisualSummaryComponentType");
	}
	return result;
}

XMLCh * Mp7JrsVisualSummaryComponentType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsVisualSummaryComponentType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsVisualSummaryComponentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VisualSummaryComponentType"));
	// Element serialization:
	// Class
	
	if (m_VideoSourceID != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSourceID->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSourceID"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSourceID->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VideoSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSourceLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSourceLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSourceLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ComponentSourceTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ComponentSourceTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ComponentSourceTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ComponentSourceTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ImageLocator != Mp7JrsImageLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ImageLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ImageLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ImageLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_SyncTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SyncTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SyncTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SyncTime->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_FrameActivity != Mp7JrszeroToOnePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_FrameActivity->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("FrameActivity"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_FrameActivity->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Region != Mp7JrsRegionLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Region->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Region"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Region->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsVisualSummaryComponentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSourceID"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSourceID")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSourceID(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSourceLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSourceLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSourceLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ComponentSourceTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ComponentSourceTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetComponentSourceTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ImageLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ImageLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateImageLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetImageLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SyncTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SyncTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSyncTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("FrameActivity"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("FrameActivity")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatezeroToOneType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFrameActivity(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Region"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Region")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVisualSummaryComponentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("VideoSourceID")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetVideoSourceID(child);
  }
  if (XMLString::compareString(elementname, X("VideoSourceLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetVideoSourceLocator(child);
  }
  if (XMLString::compareString(elementname, X("ComponentSourceTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetComponentSourceTime(child);
  }
  if (XMLString::compareString(elementname, X("ImageLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateImageLocatorType; // FTT, check this
	}
	this->SetImageLocator(child);
  }
  if (XMLString::compareString(elementname, X("SyncTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetSyncTime(child);
  }
  if (XMLString::compareString(elementname, X("FrameActivity")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatezeroToOneType; // FTT, check this
	}
	this->SetFrameActivity(child);
  }
  if (XMLString::compareString(elementname, X("Region")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionLocatorType; // FTT, check this
	}
	this->SetRegion(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVisualSummaryComponentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVideoSourceID() != Dc1NodePtr())
		result.Insert(GetVideoSourceID());
	if (GetVideoSourceLocator() != Dc1NodePtr())
		result.Insert(GetVideoSourceLocator());
	if (GetComponentSourceTime() != Dc1NodePtr())
		result.Insert(GetComponentSourceTime());
	if (GetImageLocator() != Dc1NodePtr())
		result.Insert(GetImageLocator());
	if (GetSyncTime() != Dc1NodePtr())
		result.Insert(GetSyncTime());
	if (GetFrameActivity() != Dc1NodePtr())
		result.Insert(GetFrameActivity());
	if (GetRegion() != Dc1NodePtr())
		result.Insert(GetRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVisualSummaryComponentType_ExtMethodImpl.h


