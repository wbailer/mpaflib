
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPersonType_ExtImplInclude.h


#include "Mp7JrsAgentType.h"
#include "Mp7JrsPersonType_CollectionType.h"
#include "Mp7JrsPersonType_Affiliation_CollectionType.h"
#include "Mp7JrsPersonType_Citizenship_CollectionType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsPersonType_ElectronicAddress_CollectionType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrscountryCode.h"
#include "Mp7JrsAgentType_Icon_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsPersonType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsMediaLocatorType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Icon
#include "Mp7JrsPersonType_LocalType.h" // Choice collection Name
#include "Mp7JrsPersonNameType.h" // Choice collection element Name
#include "Mp7JrsControlledTermUseType.h" // Choice collection element NameTerm
#include "Mp7JrsPersonType_Affiliation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affiliation
#include "Mp7JrsElectronicAddressType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ElectronicAddress

#include <assert.h>
IMp7JrsPersonType::IMp7JrsPersonType()
{

// no includefile for extension defined 
// file Mp7JrsPersonType_ExtPropInit.h

}

IMp7JrsPersonType::~IMp7JrsPersonType()
{
// no includefile for extension defined 
// file Mp7JrsPersonType_ExtPropCleanup.h

}

Mp7JrsPersonType::Mp7JrsPersonType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPersonType::~Mp7JrsPersonType()
{
	Cleanup();
}

void Mp7JrsPersonType::Init()
{
	// Init base
	m_Base = CreateAgentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_PersonType_LocalType = Mp7JrsPersonType_CollectionPtr(); // Collection
	m_Affiliation = Mp7JrsPersonType_Affiliation_CollectionPtr(); // Collection
	m_Citizenship = Mp7JrsPersonType_Citizenship_CollectionPtr(); // Collection
	m_Address = Mp7JrsPlacePtr(); // Class
	m_AddressRef = Mp7JrsReferencePtr(); // Class
	m_ElectronicAddress = Mp7JrsPersonType_ElectronicAddress_CollectionPtr(); // Collection
	m_PersonDescription = Mp7JrsTextualPtr(); // Class
	m_PersonDescription_Exist = false;
	m_Nationality = Mp7JrscountryCodePtr(); // Pattern
	m_Nationality_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPersonType_ExtMyPropInit.h

}

void Mp7JrsPersonType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPersonType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_PersonType_LocalType);
	// Dc1Factory::DeleteObject(m_Affiliation);
	// Dc1Factory::DeleteObject(m_Citizenship);
	// Dc1Factory::DeleteObject(m_Address);
	// Dc1Factory::DeleteObject(m_AddressRef);
	// Dc1Factory::DeleteObject(m_ElectronicAddress);
	// Dc1Factory::DeleteObject(m_PersonDescription);
	// Dc1Factory::DeleteObject(m_Nationality);
}

void Mp7JrsPersonType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PersonTypePtr, since we
	// might need GetBase(), which isn't defined in IPersonType
	const Dc1Ptr< Mp7JrsPersonType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAgentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPersonType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_PersonType_LocalType);
		this->SetPersonType_LocalType(Dc1Factory::CloneObject(tmp->GetPersonType_LocalType()));
		// Dc1Factory::DeleteObject(m_Affiliation);
		this->SetAffiliation(Dc1Factory::CloneObject(tmp->GetAffiliation()));
		// Dc1Factory::DeleteObject(m_Citizenship);
		this->SetCitizenship(Dc1Factory::CloneObject(tmp->GetCitizenship()));
		// Dc1Factory::DeleteObject(m_Address);
		this->SetAddress(Dc1Factory::CloneObject(tmp->GetAddress()));
		// Dc1Factory::DeleteObject(m_AddressRef);
		this->SetAddressRef(Dc1Factory::CloneObject(tmp->GetAddressRef()));
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		this->SetElectronicAddress(Dc1Factory::CloneObject(tmp->GetElectronicAddress()));
	if (tmp->IsValidPersonDescription())
	{
		// Dc1Factory::DeleteObject(m_PersonDescription);
		this->SetPersonDescription(Dc1Factory::CloneObject(tmp->GetPersonDescription()));
	}
	else
	{
		InvalidatePersonDescription();
	}
	if (tmp->IsValidNationality())
	{
		// Dc1Factory::DeleteObject(m_Nationality);
		this->SetNationality(Dc1Factory::CloneObject(tmp->GetNationality()));
	}
	else
	{
		InvalidateNationality();
	}
}

Dc1NodePtr Mp7JrsPersonType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPersonType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAgentType > Mp7JrsPersonType::GetBase() const
{
	return m_Base;
}

Mp7JrsPersonType_CollectionPtr Mp7JrsPersonType::GetPersonType_LocalType() const
{
		return m_PersonType_LocalType;
}

Mp7JrsPersonType_Affiliation_CollectionPtr Mp7JrsPersonType::GetAffiliation() const
{
		return m_Affiliation;
}

Mp7JrsPersonType_Citizenship_CollectionPtr Mp7JrsPersonType::GetCitizenship() const
{
		return m_Citizenship;
}

Mp7JrsPlacePtr Mp7JrsPersonType::GetAddress() const
{
		return m_Address;
}

Mp7JrsReferencePtr Mp7JrsPersonType::GetAddressRef() const
{
		return m_AddressRef;
}

Mp7JrsPersonType_ElectronicAddress_CollectionPtr Mp7JrsPersonType::GetElectronicAddress() const
{
		return m_ElectronicAddress;
}

Mp7JrsTextualPtr Mp7JrsPersonType::GetPersonDescription() const
{
		return m_PersonDescription;
}

// Element is optional
bool Mp7JrsPersonType::IsValidPersonDescription() const
{
	return m_PersonDescription_Exist;
}

Mp7JrscountryCodePtr Mp7JrsPersonType::GetNationality() const
{
		return m_Nationality;
}

// Element is optional
bool Mp7JrsPersonType::IsValidNationality() const
{
	return m_Nationality_Exist;
}

void Mp7JrsPersonType::SetPersonType_LocalType(const Mp7JrsPersonType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetPersonType_LocalType().");
	}
	if (m_PersonType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_PersonType_LocalType);
		m_PersonType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PersonType_LocalType) m_PersonType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::SetAffiliation(const Mp7JrsPersonType_Affiliation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetAffiliation().");
	}
	if (m_Affiliation != item)
	{
		// Dc1Factory::DeleteObject(m_Affiliation);
		m_Affiliation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Affiliation) m_Affiliation->SetParent(m_myself.getPointer());
		if(m_Affiliation != Mp7JrsPersonType_Affiliation_CollectionPtr())
		{
			m_Affiliation->SetContentName(XMLString::transcode("Affiliation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::SetCitizenship(const Mp7JrsPersonType_Citizenship_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetCitizenship().");
	}
	if (m_Citizenship != item)
	{
		// Dc1Factory::DeleteObject(m_Citizenship);
		m_Citizenship = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Citizenship) m_Citizenship->SetParent(m_myself.getPointer());
		if(m_Citizenship != Mp7JrsPersonType_Citizenship_CollectionPtr())
		{
			m_Citizenship->SetContentName(XMLString::transcode("Citizenship"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsPersonType::SetAddress(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetAddress().");
	}
	if (m_Address != item)
	{
		// Dc1Factory::DeleteObject(m_Address);
		m_Address = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Address) m_Address->SetParent(m_myself.getPointer());
		if (m_Address && m_Address->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Address->UseTypeAttribute = true;
		}
		if(m_Address != Mp7JrsPlacePtr())
		{
			m_Address->SetContentName(XMLString::transcode("Address"));
		}
	// Dc1Factory::DeleteObject(m_AddressRef);
	m_AddressRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsPersonType::SetAddressRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetAddressRef().");
	}
	if (m_AddressRef != item)
	{
		// Dc1Factory::DeleteObject(m_AddressRef);
		m_AddressRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AddressRef) m_AddressRef->SetParent(m_myself.getPointer());
		if (m_AddressRef && m_AddressRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AddressRef->UseTypeAttribute = true;
		}
		if(m_AddressRef != Mp7JrsReferencePtr())
		{
			m_AddressRef->SetContentName(XMLString::transcode("AddressRef"));
		}
	// Dc1Factory::DeleteObject(m_Address);
	m_Address = Mp7JrsPlacePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::SetElectronicAddress(const Mp7JrsPersonType_ElectronicAddress_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetElectronicAddress().");
	}
	if (m_ElectronicAddress != item)
	{
		// Dc1Factory::DeleteObject(m_ElectronicAddress);
		m_ElectronicAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ElectronicAddress) m_ElectronicAddress->SetParent(m_myself.getPointer());
		if(m_ElectronicAddress != Mp7JrsPersonType_ElectronicAddress_CollectionPtr())
		{
			m_ElectronicAddress->SetContentName(XMLString::transcode("ElectronicAddress"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::SetPersonDescription(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetPersonDescription().");
	}
	if (m_PersonDescription != item || m_PersonDescription_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PersonDescription);
		m_PersonDescription = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PersonDescription) m_PersonDescription->SetParent(m_myself.getPointer());
		if (m_PersonDescription && m_PersonDescription->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PersonDescription->UseTypeAttribute = true;
		}
		m_PersonDescription_Exist = true;
		if(m_PersonDescription != Mp7JrsTextualPtr())
		{
			m_PersonDescription->SetContentName(XMLString::transcode("PersonDescription"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidatePersonDescription()
{
	m_PersonDescription_Exist = false;
}
void Mp7JrsPersonType::SetNationality(const Mp7JrscountryCodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetNationality().");
	}
	if (m_Nationality != item || m_Nationality_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Nationality);
		m_Nationality = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Nationality) m_Nationality->SetParent(m_myself.getPointer());
		if (m_Nationality && m_Nationality->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Nationality->UseTypeAttribute = true;
		}
		m_Nationality_Exist = true;
		if(m_Nationality != Mp7JrscountryCodePtr())
		{
			m_Nationality->SetContentName(XMLString::transcode("Nationality"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidateNationality()
{
	m_Nationality_Exist = false;
}
Mp7JrsAgentType_Icon_CollectionPtr Mp7JrsPersonType::GetIcon() const
{
	return GetBase()->GetIcon();
}

void Mp7JrsPersonType::SetIcon(const Mp7JrsAgentType_Icon_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetIcon().");
	}
	GetBase()->SetIcon(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsPersonType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsPersonType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsPersonType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsPersonType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsPersonType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsPersonType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsPersonType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsPersonType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsPersonType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsPersonType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsPersonType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsPersonType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsPersonType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsPersonType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsPersonType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsPersonType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsPersonType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPersonType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// Name is contained in itemtype PersonType_LocalType
		// in choice collection PersonType_CollectionType

		context->Found = true;
		Mp7JrsPersonType_CollectionPtr coll = GetPersonType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonType_CollectionType; // FTT, check this
				SetPersonType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->GetName()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonType_LocalPtr item = CreatePersonType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonNameType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonNamePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->SetName(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->GetName()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("NameTerm")) == 0)
	{
		// NameTerm is contained in itemtype PersonType_LocalType
		// in choice collection PersonType_CollectionType

		context->Found = true;
		Mp7JrsPersonType_CollectionPtr coll = GetPersonType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonType_CollectionType; // FTT, check this
				SetPersonType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->GetNameTerm()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonType_LocalPtr item = CreatePersonType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->SetNameTerm(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonType_LocalPtr)coll->elementAt(i))->GetNameTerm()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Affiliation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Affiliation is item of type PersonType_Affiliation_LocalType
		// in element collection PersonType_Affiliation_CollectionType
		
		context->Found = true;
		Mp7JrsPersonType_Affiliation_CollectionPtr coll = GetAffiliation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonType_Affiliation_CollectionType; // FTT, check this
				SetAffiliation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PersonType_Affiliation_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPersonType_Affiliation_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Citizenship")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Citizenship is item of type countryCode
		// in element collection PersonType_Citizenship_CollectionType
		
		context->Found = true;
		Mp7JrsPersonType_Citizenship_CollectionPtr coll = GetCitizenship();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonType_Citizenship_CollectionType; // FTT, check this
				SetCitizenship(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode")))) != empty)
			{
				// Is type allowed
				Mp7JrscountryCodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Address")) == 0)
	{
		// Address is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAddress(p, client);
					if((p = GetAddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AddressRef")) == 0)
	{
		// AddressRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAddressRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAddressRef(p, client);
					if((p = GetAddressRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ElectronicAddress")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ElectronicAddress is item of type ElectronicAddressType
		// in element collection PersonType_ElectronicAddress_CollectionType
		
		context->Found = true;
		Mp7JrsPersonType_ElectronicAddress_CollectionPtr coll = GetElectronicAddress();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonType_ElectronicAddress_CollectionType; // FTT, check this
				SetElectronicAddress(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ElectronicAddressType")))) != empty)
			{
				// Is type allowed
				Mp7JrsElectronicAddressPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PersonDescription")) == 0)
	{
		// PersonDescription is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPersonDescription()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPersonDescription(p, client);
					if((p = GetPersonDescription()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Nationality")) == 0)
	{
		// Nationality is simple element countryCode
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetNationality()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode")))) != empty)
			{
				// Is type allowed
				Mp7JrscountryCodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetNationality(p, client);
					if((p = GetNationality()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PersonType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PersonType");
	}
	return result;
}

XMLCh * Mp7JrsPersonType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPersonType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPersonType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPersonType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PersonType"));
	// Element serialization:
	if (m_PersonType_LocalType != Mp7JrsPersonType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PersonType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Affiliation != Mp7JrsPersonType_Affiliation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Affiliation->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Citizenship != Mp7JrsPersonType_Citizenship_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Citizenship->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Address != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Address->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Address"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Address->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AddressRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AddressRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AddressRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AddressRef->Serialize(doc, element, &elem);
	}
	if (m_ElectronicAddress != Mp7JrsPersonType_ElectronicAddress_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ElectronicAddress->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_PersonDescription != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PersonDescription->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PersonDescription"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PersonDescription->Serialize(doc, element, &elem);
	}
	if(m_Nationality != Mp7JrscountryCodePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("Nationality");
//		m_Nationality->SetContentName(contentname);
		m_Nationality->UseTypeAttribute = this->UseTypeAttribute;
		m_Nationality->Serialize(doc, element);
	}

}

bool Mp7JrsPersonType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAgentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Name
			Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Name")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:NameTerm
			Dc1Util::HasNodeName(parent, X("NameTerm"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:NameTerm")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsPersonType_CollectionPtr tmp = CreatePersonType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPersonType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Affiliation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Affiliation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonType_Affiliation_CollectionPtr tmp = CreatePersonType_Affiliation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAffiliation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Citizenship"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Citizenship")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonType_Citizenship_CollectionPtr tmp = CreatePersonType_Citizenship_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCitizenship(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Address"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Address")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AddressRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AddressRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAddressRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ElectronicAddress"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ElectronicAddress")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPersonType_ElectronicAddress_CollectionPtr tmp = CreatePersonType_ElectronicAddress_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetElectronicAddress(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PersonDescription"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PersonDescription")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPersonDescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("Nationality"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Nationality")) == 0))
		{
			Mp7JrscountryCodePtr tmp = CreatecountryCode; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNationality(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPersonType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPersonType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Name")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("NameTerm"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("NameTerm")) == 0)
	)
  {
	Mp7JrsPersonType_CollectionPtr tmp = CreatePersonType_CollectionType; // FTT, check this
	this->SetPersonType_LocalType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Affiliation")) == 0))
  {
	Mp7JrsPersonType_Affiliation_CollectionPtr tmp = CreatePersonType_Affiliation_CollectionType; // FTT, check this
	this->SetAffiliation(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Citizenship")) == 0))
  {
	Mp7JrsPersonType_Citizenship_CollectionPtr tmp = CreatePersonType_Citizenship_CollectionType; // FTT, check this
	this->SetCitizenship(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Address")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetAddress(child);
  }
  if (XMLString::compareString(elementname, X("AddressRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetAddressRef(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ElectronicAddress")) == 0))
  {
	Mp7JrsPersonType_ElectronicAddress_CollectionPtr tmp = CreatePersonType_ElectronicAddress_CollectionType; // FTT, check this
	this->SetElectronicAddress(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("PersonDescription")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetPersonDescription(child);
  }
  if (XMLString::compareString(elementname, X("Nationality")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecountryCode; // FTT, check this
	}
	this->SetNationality(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPersonType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPersonType_LocalType() != Dc1NodePtr())
		result.Insert(GetPersonType_LocalType());
	if (GetAffiliation() != Dc1NodePtr())
		result.Insert(GetAffiliation());
	if (GetCitizenship() != Dc1NodePtr())
		result.Insert(GetCitizenship());
	if (GetElectronicAddress() != Dc1NodePtr())
		result.Insert(GetElectronicAddress());
	if (GetPersonDescription() != Dc1NodePtr())
		result.Insert(GetPersonDescription());
	if (GetNationality() != Dc1NodePtr())
		result.Insert(GetNationality());
	if (GetIcon() != Dc1NodePtr())
		result.Insert(GetIcon());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetAddress() != Dc1NodePtr())
		result.Insert(GetAddress());
	if (GetAddressRef() != Dc1NodePtr())
		result.Insert(GetAddressRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPersonType_ExtMethodImpl.h


