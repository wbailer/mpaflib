
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsFilteringAndSearchPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsCreationPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:CreationPreferences
#include "Mp7JrsClassificationPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ClassificationPreferences
#include "Mp7JrsSourcePreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SourcePreferences
#include "Mp7JrsPreferenceConditionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PreferenceCondition
#include "Mp7JrsFilteringAndSearchPreferencesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferences

#include <assert.h>
IMp7JrsFilteringAndSearchPreferencesType::IMp7JrsFilteringAndSearchPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtPropInit.h

}

IMp7JrsFilteringAndSearchPreferencesType::~IMp7JrsFilteringAndSearchPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtPropCleanup.h

}

Mp7JrsFilteringAndSearchPreferencesType::Mp7JrsFilteringAndSearchPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFilteringAndSearchPreferencesType::~Mp7JrsFilteringAndSearchPreferencesType()
{
	Cleanup();
}

void Mp7JrsFilteringAndSearchPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_protected = Mp7JrsuserChoiceType::UninitializedEnumeration;
	m_protected_Default = Mp7JrsuserChoiceType::_true; // Default enumeration
	m_protected_Exist = false;
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_CreationPreferences = Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr(); // Collection
	m_ClassificationPreferences = Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr(); // Collection
	m_SourcePreferences = Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr(); // Collection
	m_PreferenceCondition = Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr(); // Collection
	m_FilteringAndSearchPreferences = Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtMyPropInit.h

}

void Mp7JrsFilteringAndSearchPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_CreationPreferences);
	// Dc1Factory::DeleteObject(m_ClassificationPreferences);
	// Dc1Factory::DeleteObject(m_SourcePreferences);
	// Dc1Factory::DeleteObject(m_PreferenceCondition);
	// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
}

void Mp7JrsFilteringAndSearchPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FilteringAndSearchPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in IFilteringAndSearchPreferencesType
	const Dc1Ptr< Mp7JrsFilteringAndSearchPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsFilteringAndSearchPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existprotected())
	{
		this->Setprotected(tmp->Getprotected());
	}
	else
	{
		Invalidateprotected();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_CreationPreferences);
		this->SetCreationPreferences(Dc1Factory::CloneObject(tmp->GetCreationPreferences()));
		// Dc1Factory::DeleteObject(m_ClassificationPreferences);
		this->SetClassificationPreferences(Dc1Factory::CloneObject(tmp->GetClassificationPreferences()));
		// Dc1Factory::DeleteObject(m_SourcePreferences);
		this->SetSourcePreferences(Dc1Factory::CloneObject(tmp->GetSourcePreferences()));
		// Dc1Factory::DeleteObject(m_PreferenceCondition);
		this->SetPreferenceCondition(Dc1Factory::CloneObject(tmp->GetPreferenceCondition()));
		// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
		this->SetFilteringAndSearchPreferences(Dc1Factory::CloneObject(tmp->GetFilteringAndSearchPreferences()));
}

Dc1NodePtr Mp7JrsFilteringAndSearchPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsFilteringAndSearchPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsFilteringAndSearchPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrsuserChoiceType::Enumeration Mp7JrsFilteringAndSearchPreferencesType::Getprotected() const
{
	if (this->Existprotected()) {
		return m_protected;
	} else {
		return m_protected_Default;
	}
}

bool Mp7JrsFilteringAndSearchPreferencesType::Existprotected() const
{
	return m_protected_Exist;
}
void Mp7JrsFilteringAndSearchPreferencesType::Setprotected(Mp7JrsuserChoiceType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::Setprotected().");
	}
	m_protected = item;
	m_protected_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::Invalidateprotected()
{
	m_protected_Exist = false;
}
Mp7JrspreferenceValuePtr Mp7JrsFilteringAndSearchPreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsFilteringAndSearchPreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsFilteringAndSearchPreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetCreationPreferences() const
{
		return m_CreationPreferences;
}

Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetClassificationPreferences() const
{
		return m_ClassificationPreferences;
}

Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetSourcePreferences() const
{
		return m_SourcePreferences;
}

Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetPreferenceCondition() const
{
		return m_PreferenceCondition;
}

Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetFilteringAndSearchPreferences() const
{
		return m_FilteringAndSearchPreferences;
}

void Mp7JrsFilteringAndSearchPreferencesType::SetCreationPreferences(const Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetCreationPreferences().");
	}
	if (m_CreationPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_CreationPreferences);
		m_CreationPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CreationPreferences) m_CreationPreferences->SetParent(m_myself.getPointer());
		if(m_CreationPreferences != Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr())
		{
			m_CreationPreferences->SetContentName(XMLString::transcode("CreationPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::SetClassificationPreferences(const Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetClassificationPreferences().");
	}
	if (m_ClassificationPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_ClassificationPreferences);
		m_ClassificationPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClassificationPreferences) m_ClassificationPreferences->SetParent(m_myself.getPointer());
		if(m_ClassificationPreferences != Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr())
		{
			m_ClassificationPreferences->SetContentName(XMLString::transcode("ClassificationPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::SetSourcePreferences(const Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetSourcePreferences().");
	}
	if (m_SourcePreferences != item)
	{
		// Dc1Factory::DeleteObject(m_SourcePreferences);
		m_SourcePreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SourcePreferences) m_SourcePreferences->SetParent(m_myself.getPointer());
		if(m_SourcePreferences != Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr())
		{
			m_SourcePreferences->SetContentName(XMLString::transcode("SourcePreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::SetPreferenceCondition(const Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetPreferenceCondition().");
	}
	if (m_PreferenceCondition != item)
	{
		// Dc1Factory::DeleteObject(m_PreferenceCondition);
		m_PreferenceCondition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PreferenceCondition) m_PreferenceCondition->SetParent(m_myself.getPointer());
		if(m_PreferenceCondition != Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr())
		{
			m_PreferenceCondition->SetContentName(XMLString::transcode("PreferenceCondition"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::SetFilteringAndSearchPreferences(const Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetFilteringAndSearchPreferences().");
	}
	if (m_FilteringAndSearchPreferences != item)
	{
		// Dc1Factory::DeleteObject(m_FilteringAndSearchPreferences);
		m_FilteringAndSearchPreferences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FilteringAndSearchPreferences) m_FilteringAndSearchPreferences->SetParent(m_myself.getPointer());
		if(m_FilteringAndSearchPreferences != Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr())
		{
			m_FilteringAndSearchPreferences->SetContentName(XMLString::transcode("FilteringAndSearchPreferences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsFilteringAndSearchPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsFilteringAndSearchPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsFilteringAndSearchPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsFilteringAndSearchPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsFilteringAndSearchPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsFilteringAndSearchPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsFilteringAndSearchPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsFilteringAndSearchPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsFilteringAndSearchPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsFilteringAndSearchPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsFilteringAndSearchPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsFilteringAndSearchPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsFilteringAndSearchPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsFilteringAndSearchPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsFilteringAndSearchPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringAndSearchPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsFilteringAndSearchPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsFilteringAndSearchPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringAndSearchPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFilteringAndSearchPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("protected")) == 0)
	{
		// protected is simple attribute userChoiceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsuserChoiceType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CreationPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:CreationPreferences is item of type CreationPreferencesType
		// in element collection FilteringAndSearchPreferencesType_CreationPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr coll = GetCreationPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFilteringAndSearchPreferencesType_CreationPreferences_CollectionType; // FTT, check this
				SetCreationPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClassificationPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ClassificationPreferences is item of type ClassificationPreferencesType
		// in element collection FilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr coll = GetClassificationPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType; // FTT, check this
				SetClassificationPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SourcePreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SourcePreferences is item of type SourcePreferencesType
		// in element collection FilteringAndSearchPreferencesType_SourcePreferences_CollectionType
		
		context->Found = true;
		Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr coll = GetSourcePreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFilteringAndSearchPreferencesType_SourcePreferences_CollectionType; // FTT, check this
				SetSourcePreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SourcePreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSourcePreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PreferenceCondition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:PreferenceCondition is item of type PreferenceConditionType
		// in element collection FilteringAndSearchPreferencesType_PreferenceCondition_CollectionType
		
		context->Found = true;
		Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr coll = GetPreferenceCondition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
				SetPreferenceCondition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PreferenceConditionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPreferenceConditionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FilteringAndSearchPreferences")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferences is item of type FilteringAndSearchPreferencesType
		// in element collection FilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType
		
		context->Found = true;
		Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr coll = GetFilteringAndSearchPreferences();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
				SetFilteringAndSearchPreferences(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FilteringAndSearchPreferencesType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFilteringAndSearchPreferencesPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FilteringAndSearchPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FilteringAndSearchPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsFilteringAndSearchPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFilteringAndSearchPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFilteringAndSearchPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FilteringAndSearchPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_protected_Exist)
	{
	// Enumeration
	if(m_protected != Mp7JrsuserChoiceType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsuserChoiceType::ToText(m_protected);
		element->setAttributeNS(X(""), X("protected"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_CreationPreferences != Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CreationPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ClassificationPreferences != Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClassificationPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SourcePreferences != Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SourcePreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_PreferenceCondition != Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PreferenceCondition->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_FilteringAndSearchPreferences != Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FilteringAndSearchPreferences->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsFilteringAndSearchPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("protected")))
	{
		this->Setprotected(Mp7JrsuserChoiceType::Parse(parent->getAttribute(X("protected"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("CreationPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("CreationPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_CreationPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCreationPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ClassificationPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ClassificationPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClassificationPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SourcePreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SourcePreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_SourcePreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSourcePreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("PreferenceCondition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("PreferenceCondition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPreferenceCondition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FilteringAndSearchPreferences"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FilteringAndSearchPreferences")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFilteringAndSearchPreferences(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFilteringAndSearchPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("CreationPreferences")) == 0))
  {
	Mp7JrsFilteringAndSearchPreferencesType_CreationPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_CreationPreferences_CollectionType; // FTT, check this
	this->SetCreationPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ClassificationPreferences")) == 0))
  {
	Mp7JrsFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_ClassificationPreferences_CollectionType; // FTT, check this
	this->SetClassificationPreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SourcePreferences")) == 0))
  {
	Mp7JrsFilteringAndSearchPreferencesType_SourcePreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_SourcePreferences_CollectionType; // FTT, check this
	this->SetSourcePreferences(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("PreferenceCondition")) == 0))
  {
	Mp7JrsFilteringAndSearchPreferencesType_PreferenceCondition_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_PreferenceCondition_CollectionType; // FTT, check this
	this->SetPreferenceCondition(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FilteringAndSearchPreferences")) == 0))
  {
	Mp7JrsFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionPtr tmp = CreateFilteringAndSearchPreferencesType_FilteringAndSearchPreferences_CollectionType; // FTT, check this
	this->SetFilteringAndSearchPreferences(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFilteringAndSearchPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCreationPreferences() != Dc1NodePtr())
		result.Insert(GetCreationPreferences());
	if (GetClassificationPreferences() != Dc1NodePtr())
		result.Insert(GetClassificationPreferences());
	if (GetSourcePreferences() != Dc1NodePtr())
		result.Insert(GetSourcePreferences());
	if (GetPreferenceCondition() != Dc1NodePtr())
		result.Insert(GetPreferenceCondition());
	if (GetFilteringAndSearchPreferences() != Dc1NodePtr())
		result.Insert(GetFilteringAndSearchPreferences());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFilteringAndSearchPreferencesType_ExtMethodImpl.h


