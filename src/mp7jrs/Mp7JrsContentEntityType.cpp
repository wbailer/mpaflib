
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtImplInclude.h


#include "Mp7JrsContentDescriptionType.h"
#include "Mp7JrsContentEntityType_MultimediaContent_CollectionType.h"
#include "Mp7JrsContentDescriptionType_Affective_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsContentEntityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsAffectiveType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affective
#include "Mp7JrsMultimediaContentType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MultimediaContent

#include <assert.h>
IMp7JrsContentEntityType::IMp7JrsContentEntityType()
{

// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtPropInit.h

}

IMp7JrsContentEntityType::~IMp7JrsContentEntityType()
{
// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtPropCleanup.h

}

Mp7JrsContentEntityType::Mp7JrsContentEntityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsContentEntityType::~Mp7JrsContentEntityType()
{
	Cleanup();
}

void Mp7JrsContentEntityType::Init()
{
	// Init base
	m_Base = CreateContentDescriptionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MultimediaContent = Mp7JrsContentEntityType_MultimediaContent_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtMyPropInit.h

}

void Mp7JrsContentEntityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MultimediaContent);
}

void Mp7JrsContentEntityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ContentEntityTypePtr, since we
	// might need GetBase(), which isn't defined in IContentEntityType
	const Dc1Ptr< Mp7JrsContentEntityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentDescriptionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsContentEntityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MultimediaContent);
		this->SetMultimediaContent(Dc1Factory::CloneObject(tmp->GetMultimediaContent()));
}

Dc1NodePtr Mp7JrsContentEntityType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsContentEntityType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentDescriptionType > Mp7JrsContentEntityType::GetBase() const
{
	return m_Base;
}

Mp7JrsContentEntityType_MultimediaContent_CollectionPtr Mp7JrsContentEntityType::GetMultimediaContent() const
{
		return m_MultimediaContent;
}

void Mp7JrsContentEntityType::SetMultimediaContent(const Mp7JrsContentEntityType_MultimediaContent_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentEntityType::SetMultimediaContent().");
	}
	if (m_MultimediaContent != item)
	{
		// Dc1Factory::DeleteObject(m_MultimediaContent);
		m_MultimediaContent = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MultimediaContent) m_MultimediaContent->SetParent(m_myself.getPointer());
		if(m_MultimediaContent != Mp7JrsContentEntityType_MultimediaContent_CollectionPtr())
		{
			m_MultimediaContent->SetContentName(XMLString::transcode("MultimediaContent"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsContentDescriptionType_Affective_CollectionPtr Mp7JrsContentEntityType::GetAffective() const
{
	return GetBase()->GetAffective();
}

void Mp7JrsContentEntityType::SetAffective(const Mp7JrsContentDescriptionType_Affective_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentEntityType::SetAffective().");
	}
	GetBase()->SetAffective(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsContentEntityType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsContentEntityType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsContentEntityType::GetRelationships() const
{
	return GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsContentEntityType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsContentEntityType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentEntityType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentEntityType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsContentEntityType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentEntityType::SetRelationships().");
	}
	GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsContentEntityType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsContentEntityType::SetOrderingKey().");
	}
	GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsContentEntityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MultimediaContent")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MultimediaContent is item of abstract type MultimediaContentType
		// in element collection ContentEntityType_MultimediaContent_CollectionType
		
		context->Found = true;
		Mp7JrsContentEntityType_MultimediaContent_CollectionPtr coll = GetMultimediaContent();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateContentEntityType_MultimediaContent_CollectionType; // FTT, check this
				SetMultimediaContent(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsMultimediaContentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ContentEntityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ContentEntityType");
	}
	return result;
}

XMLCh * Mp7JrsContentEntityType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsContentEntityType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsContentEntityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ContentEntityType"));
	// Element serialization:
	if (m_MultimediaContent != Mp7JrsContentEntityType_MultimediaContent_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MultimediaContent->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsContentEntityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentDescriptionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MultimediaContent"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MultimediaContent")) == 0))
		{
			// Deserialize factory type
			Mp7JrsContentEntityType_MultimediaContent_CollectionPtr tmp = CreateContentEntityType_MultimediaContent_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMultimediaContent(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsContentEntityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MultimediaContent")) == 0))
  {
	Mp7JrsContentEntityType_MultimediaContent_CollectionPtr tmp = CreateContentEntityType_MultimediaContent_CollectionType; // FTT, check this
	this->SetMultimediaContent(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsContentEntityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMultimediaContent() != Dc1NodePtr())
		result.Insert(GetMultimediaContent());
	if (GetAffective() != Dc1NodePtr())
		result.Insert(GetAffective());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsContentEntityType_ExtMethodImpl.h


