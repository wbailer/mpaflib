
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtImplInclude.h


#include "Mp7JrsTermUseType.h"
#include "Mp7JrsInkMediaInformationType_InputDevice_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsInkMediaInformationType_InputDevice_LocalType::IMp7JrsInkMediaInformationType_InputDevice_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtPropInit.h

}

IMp7JrsInkMediaInformationType_InputDevice_LocalType::~IMp7JrsInkMediaInformationType_InputDevice_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtPropCleanup.h

}

Mp7JrsInkMediaInformationType_InputDevice_LocalType::Mp7JrsInkMediaInformationType_InputDevice_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsInkMediaInformationType_InputDevice_LocalType::~Mp7JrsInkMediaInformationType_InputDevice_LocalType()
{
	Cleanup();
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::Init()
{

	// Init attributes
	m_xWidth = 0; // Value
	m_xWidth_Exist = false;
	m_yWidth = 0; // Value
	m_yWidth_Exist = false;
	m_resolutionX = 0; // Value
	m_resolutionX_Exist = false;
	m_resolutionY = 0; // Value
	m_resolutionY_Exist = false;
	m_resolutionZ = 0; // Value
	m_resolutionZ_Exist = false;
	m_resolutionP = 0; // Value
	m_resolutionP_Exist = false;
	m_resolutionS = 0; // Value
	m_resolutionS_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Device = Mp7JrsTermUsePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtMyPropInit.h

}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Device);
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use InkMediaInformationType_InputDevice_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IInkMediaInformationType_InputDevice_LocalType
	const Dc1Ptr< Mp7JrsInkMediaInformationType_InputDevice_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExistxWidth())
	{
		this->SetxWidth(tmp->GetxWidth());
	}
	else
	{
		InvalidatexWidth();
	}
	}
	{
	if (tmp->ExistyWidth())
	{
		this->SetyWidth(tmp->GetyWidth());
	}
	else
	{
		InvalidateyWidth();
	}
	}
	{
	if (tmp->ExistresolutionX())
	{
		this->SetresolutionX(tmp->GetresolutionX());
	}
	else
	{
		InvalidateresolutionX();
	}
	}
	{
	if (tmp->ExistresolutionY())
	{
		this->SetresolutionY(tmp->GetresolutionY());
	}
	else
	{
		InvalidateresolutionY();
	}
	}
	{
	if (tmp->ExistresolutionZ())
	{
		this->SetresolutionZ(tmp->GetresolutionZ());
	}
	else
	{
		InvalidateresolutionZ();
	}
	}
	{
	if (tmp->ExistresolutionP())
	{
		this->SetresolutionP(tmp->GetresolutionP());
	}
	else
	{
		InvalidateresolutionP();
	}
	}
	{
	if (tmp->ExistresolutionS())
	{
		this->SetresolutionS(tmp->GetresolutionS());
	}
	else
	{
		InvalidateresolutionS();
	}
	}
		// Dc1Factory::DeleteObject(m_Device);
		this->SetDevice(Dc1Factory::CloneObject(tmp->GetDevice()));
}

Dc1NodePtr Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


unsigned Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetxWidth() const
{
	return m_xWidth;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistxWidth() const
{
	return m_xWidth_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetxWidth(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetxWidth().");
	}
	m_xWidth = item;
	m_xWidth_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidatexWidth()
{
	m_xWidth_Exist = false;
}
unsigned Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetyWidth() const
{
	return m_yWidth;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistyWidth() const
{
	return m_yWidth_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetyWidth(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetyWidth().");
	}
	m_yWidth = item;
	m_yWidth_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateyWidth()
{
	m_yWidth_Exist = false;
}
unsigned Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetresolutionX() const
{
	return m_resolutionX;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistresolutionX() const
{
	return m_resolutionX_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionX(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionX().");
	}
	m_resolutionX = item;
	m_resolutionX_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateresolutionX()
{
	m_resolutionX_Exist = false;
}
unsigned Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetresolutionY() const
{
	return m_resolutionY;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistresolutionY() const
{
	return m_resolutionY_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionY(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionY().");
	}
	m_resolutionY = item;
	m_resolutionY_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateresolutionY()
{
	m_resolutionY_Exist = false;
}
unsigned Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetresolutionZ() const
{
	return m_resolutionZ;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistresolutionZ() const
{
	return m_resolutionZ_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionZ(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionZ().");
	}
	m_resolutionZ = item;
	m_resolutionZ_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateresolutionZ()
{
	m_resolutionZ_Exist = false;
}
int Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetresolutionP() const
{
	return m_resolutionP;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistresolutionP() const
{
	return m_resolutionP_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionP(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionP().");
	}
	m_resolutionP = item;
	m_resolutionP_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateresolutionP()
{
	m_resolutionP_Exist = false;
}
int Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetresolutionS() const
{
	return m_resolutionS;
}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::ExistresolutionS() const
{
	return m_resolutionS_Exist;
}
void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionS(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetresolutionS().");
	}
	m_resolutionS = item;
	m_resolutionS_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::InvalidateresolutionS()
{
	m_resolutionS_Exist = false;
}
Mp7JrsTermUsePtr Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetDevice() const
{
		return m_Device;
}

void Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetDevice(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsInkMediaInformationType_InputDevice_LocalType::SetDevice().");
	}
	if (m_Device != item)
	{
		// Dc1Factory::DeleteObject(m_Device);
		m_Device = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Device) m_Device->SetParent(m_myself.getPointer());
		if (m_Device && m_Device->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Device->UseTypeAttribute = true;
		}
		if(m_Device != Mp7JrsTermUsePtr())
		{
			m_Device->SetContentName(XMLString::transcode("Device"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("xWidth")) == 0)
	{
		// xWidth is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("yWidth")) == 0)
	{
		// yWidth is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("resolutionX")) == 0)
	{
		// resolutionX is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("resolutionY")) == 0)
	{
		// resolutionY is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("resolutionZ")) == 0)
	{
		// resolutionZ is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("resolutionP")) == 0)
	{
		// resolutionP is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("resolutionS")) == 0)
	{
		// resolutionS is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Device")) == 0)
	{
		// Device is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDevice()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDevice(p, client);
					if((p = GetDevice()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for InkMediaInformationType_InputDevice_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "InkMediaInformationType_InputDevice_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsInkMediaInformationType_InputDevice_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsInkMediaInformationType_InputDevice_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("InkMediaInformationType_InputDevice_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_xWidth_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_xWidth);
		element->setAttributeNS(X(""), X("xWidth"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_yWidth_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_yWidth);
		element->setAttributeNS(X(""), X("yWidth"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_resolutionX_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_resolutionX);
		element->setAttributeNS(X(""), X("resolutionX"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_resolutionY_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_resolutionY);
		element->setAttributeNS(X(""), X("resolutionY"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_resolutionZ_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_resolutionZ);
		element->setAttributeNS(X(""), X("resolutionZ"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_resolutionP_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_resolutionP);
		element->setAttributeNS(X(""), X("resolutionP"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_resolutionS_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_resolutionS);
		element->setAttributeNS(X(""), X("resolutionS"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Device != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Device->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Device"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Device->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsInkMediaInformationType_InputDevice_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xWidth")))
	{
		// deserialize value type
		this->SetxWidth(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("xWidth"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yWidth")))
	{
		// deserialize value type
		this->SetyWidth(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("yWidth"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("resolutionX")))
	{
		// deserialize value type
		this->SetresolutionX(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("resolutionX"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("resolutionY")))
	{
		// deserialize value type
		this->SetresolutionY(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("resolutionY"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("resolutionZ")))
	{
		// deserialize value type
		this->SetresolutionZ(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("resolutionZ"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("resolutionP")))
	{
		// deserialize value type
		this->SetresolutionP(Dc1Convert::TextToInt(parent->getAttribute(X("resolutionP"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("resolutionS")))
	{
		// deserialize value type
		this->SetresolutionS(Dc1Convert::TextToInt(parent->getAttribute(X("resolutionS"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Device"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Device")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDevice(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsInkMediaInformationType_InputDevice_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Device")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetDevice(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsInkMediaInformationType_InputDevice_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDevice() != Dc1NodePtr())
		result.Insert(GetDevice());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsInkMediaInformationType_InputDevice_LocalType_ExtMethodImpl.h


