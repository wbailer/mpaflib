
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrsWordFormType_type_LocalType_ExtImplInclude.h


#include "Mp7JrsWordFormType_type_LocalType.h"

// no includefile for extension defined 
// file Mp7JrsWordFormType_type_LocalType_ExtMethodImpl.h


const XMLCh * Mp7JrsWordFormType_type_LocalType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrsWordFormType_type_LocalType::ToText(Mp7JrsWordFormType_type_LocalType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/noun:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("noun");
			break;
		case /*Enumeration::*/pronoun:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("pronoun");
			break;
		case /*Enumeration::*/adjective:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("adjective");
			break;
		case /*Enumeration::*/verb:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("verb");
			break;
		case /*Enumeration::*/adverb:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("adverb");
			break;
		case /*Enumeration::*/conjunction:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("conjunction");
			break;
		case /*Enumeration::*/preposition:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("preposition");
			break;
		case /*Enumeration::*/postposition:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("postposition");
			break;
		case /*Enumeration::*/article:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("article");
			break;
		case /*Enumeration::*/interjection:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("interjection");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrsWordFormType_type_LocalType::Enumeration Mp7JrsWordFormType_type_LocalType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrsWordFormType_type_LocalType::Enumeration result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "noun") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/noun;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "pronoun") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/pronoun;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "adjective") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/adjective;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "verb") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/verb;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "adverb") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/adverb;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "conjunction") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/conjunction;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "preposition") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/preposition;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "postposition") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/postposition;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "article") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/article;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "interjection") == 0)
	{
		result = Mp7JrsWordFormType_type_LocalType::/*Enumeration::*/interjection;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
