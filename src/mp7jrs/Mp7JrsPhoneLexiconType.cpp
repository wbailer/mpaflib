
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtImplInclude.h


#include "Mp7JrsLexiconType.h"
#include "Mp7JrsPhoneLexiconType_Token_CollectionType.h"
#include "Mp7JrsPhoneLexiconType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPhoneType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Token

#include <assert.h>
IMp7JrsPhoneLexiconType::IMp7JrsPhoneLexiconType()
{

// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtPropInit.h

}

IMp7JrsPhoneLexiconType::~IMp7JrsPhoneLexiconType()
{
// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtPropCleanup.h

}

Mp7JrsPhoneLexiconType::Mp7JrsPhoneLexiconType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPhoneLexiconType::~Mp7JrsPhoneLexiconType()
{
	Cleanup();
}

void Mp7JrsPhoneLexiconType::Init()
{
	// Init base
	m_Base = CreateLexiconType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_phoneticAlphabet = Mp7JrsphoneticAlphabetType::UninitializedEnumeration;
	m_phoneticAlphabet_Default = Mp7JrsphoneticAlphabetType::sampa; // Default enumeration
	m_phoneticAlphabet_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Token = Mp7JrsPhoneLexiconType_Token_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtMyPropInit.h

}

void Mp7JrsPhoneLexiconType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Token);
}

void Mp7JrsPhoneLexiconType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PhoneLexiconTypePtr, since we
	// might need GetBase(), which isn't defined in IPhoneLexiconType
	const Dc1Ptr< Mp7JrsPhoneLexiconType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsLexiconPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPhoneLexiconType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistphoneticAlphabet())
	{
		this->SetphoneticAlphabet(tmp->GetphoneticAlphabet());
	}
	else
	{
		InvalidatephoneticAlphabet();
	}
	}
		// Dc1Factory::DeleteObject(m_Token);
		this->SetToken(Dc1Factory::CloneObject(tmp->GetToken()));
}

Dc1NodePtr Mp7JrsPhoneLexiconType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPhoneLexiconType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsLexiconType > Mp7JrsPhoneLexiconType::GetBase() const
{
	return m_Base;
}

Mp7JrsphoneticAlphabetType::Enumeration Mp7JrsPhoneLexiconType::GetphoneticAlphabet() const
{
	if (this->ExistphoneticAlphabet()) {
		return m_phoneticAlphabet;
	} else {
		return m_phoneticAlphabet_Default;
	}
}

bool Mp7JrsPhoneLexiconType::ExistphoneticAlphabet() const
{
	return m_phoneticAlphabet_Exist;
}
void Mp7JrsPhoneLexiconType::SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneLexiconType::SetphoneticAlphabet().");
	}
	m_phoneticAlphabet = item;
	m_phoneticAlphabet_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneLexiconType::InvalidatephoneticAlphabet()
{
	m_phoneticAlphabet_Exist = false;
}
Mp7JrsPhoneLexiconType_Token_CollectionPtr Mp7JrsPhoneLexiconType::GetToken() const
{
		return m_Token;
}

void Mp7JrsPhoneLexiconType::SetToken(const Mp7JrsPhoneLexiconType_Token_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneLexiconType::SetToken().");
	}
	if (m_Token != item)
	{
		// Dc1Factory::DeleteObject(m_Token);
		m_Token = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Token) m_Token->SetParent(m_myself.getPointer());
		if(m_Token != Mp7JrsPhoneLexiconType_Token_CollectionPtr())
		{
			m_Token->SetContentName(XMLString::transcode("Token"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsPhoneLexiconType::GetnumOfOriginalEntries() const
{
	return GetBase()->GetnumOfOriginalEntries();
}

bool Mp7JrsPhoneLexiconType::ExistnumOfOriginalEntries() const
{
	return GetBase()->ExistnumOfOriginalEntries();
}
void Mp7JrsPhoneLexiconType::SetnumOfOriginalEntries(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneLexiconType::SetnumOfOriginalEntries().");
	}
	GetBase()->SetnumOfOriginalEntries(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneLexiconType::InvalidatenumOfOriginalEntries()
{
	GetBase()->InvalidatenumOfOriginalEntries();
}
XMLCh * Mp7JrsPhoneLexiconType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsPhoneLexiconType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsPhoneLexiconType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPhoneLexiconType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPhoneLexiconType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsPhoneLexiconType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("phoneticAlphabet")) == 0)
	{
		// phoneticAlphabet is simple attribute phoneticAlphabetType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsphoneticAlphabetType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Token")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Token is item of type PhoneType
		// in element collection PhoneLexiconType_Token_CollectionType
		
		context->Found = true;
		Mp7JrsPhoneLexiconType_Token_CollectionPtr coll = GetToken();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePhoneLexiconType_Token_CollectionType; // FTT, check this
				SetToken(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65536))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PhoneType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPhonePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PhoneLexiconType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PhoneLexiconType");
	}
	return result;
}

XMLCh * Mp7JrsPhoneLexiconType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPhoneLexiconType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPhoneLexiconType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PhoneLexiconType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_phoneticAlphabet_Exist)
	{
	// Enumeration
	if(m_phoneticAlphabet != Mp7JrsphoneticAlphabetType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsphoneticAlphabetType::ToText(m_phoneticAlphabet);
		element->setAttributeNS(X(""), X("phoneticAlphabet"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Token != Mp7JrsPhoneLexiconType_Token_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Token->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPhoneLexiconType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("phoneticAlphabet")))
	{
		this->SetphoneticAlphabet(Mp7JrsphoneticAlphabetType::Parse(parent->getAttribute(X("phoneticAlphabet"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsLexiconType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Token"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Token")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPhoneLexiconType_Token_CollectionPtr tmp = CreatePhoneLexiconType_Token_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetToken(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPhoneLexiconType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Token")) == 0))
  {
	Mp7JrsPhoneLexiconType_Token_CollectionPtr tmp = CreatePhoneLexiconType_Token_CollectionType; // FTT, check this
	this->SetToken(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPhoneLexiconType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetToken() != Dc1NodePtr())
		result.Insert(GetToken());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPhoneLexiconType_ExtMethodImpl.h


