
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtImplInclude.h


#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalType.h"
#include "Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType::IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtPropInit.h

}

IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType::~IMp7JrsMediaTranscodingHintsType_MotionHint_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::~Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Init()
{

	// Init attributes
	m_uncompensability = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_uncompensability->SetContent(0.0); // Use Value initialiser (xxx2)
	m_uncompensability_Exist = false;
	m_intensity = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_intensity->SetContent(0.0); // Use Value initialiser (xxx2)
	m_intensity_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_MotionRange = Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr(); // Class
	m_MotionRange_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_uncompensability);
	// Dc1Factory::DeleteObject(m_intensity);
	// Dc1Factory::DeleteObject(m_MotionRange);
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaTranscodingHintsType_MotionHint_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaTranscodingHintsType_MotionHint_LocalType
	const Dc1Ptr< Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_uncompensability);
	if (tmp->Existuncompensability())
	{
		this->Setuncompensability(Dc1Factory::CloneObject(tmp->Getuncompensability()));
	}
	else
	{
		Invalidateuncompensability();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_intensity);
	if (tmp->Existintensity())
	{
		this->Setintensity(Dc1Factory::CloneObject(tmp->Getintensity()));
	}
	else
	{
		Invalidateintensity();
	}
	}
	if (tmp->IsValidMotionRange())
	{
		// Dc1Factory::DeleteObject(m_MotionRange);
		this->SetMotionRange(Dc1Factory::CloneObject(tmp->GetMotionRange()));
	}
	else
	{
		InvalidateMotionRange();
	}
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrszeroToOnePtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Getuncompensability() const
{
	return m_uncompensability;
}

bool Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Existuncompensability() const
{
	return m_uncompensability_Exist;
}
void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Setuncompensability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Setuncompensability().");
	}
	m_uncompensability = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_uncompensability) m_uncompensability->SetParent(m_myself.getPointer());
	m_uncompensability_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Invalidateuncompensability()
{
	m_uncompensability_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Getintensity() const
{
	return m_intensity;
}

bool Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Existintensity() const
{
	return m_intensity_Exist;
}
void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Setintensity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Setintensity().");
	}
	m_intensity = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_intensity) m_intensity->SetParent(m_myself.getPointer());
	m_intensity_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Invalidateintensity()
{
	m_intensity_Exist = false;
}
Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::GetMotionRange() const
{
		return m_MotionRange;
}

// Element is optional
bool Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::IsValidMotionRange() const
{
	return m_MotionRange_Exist;
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::SetMotionRange(const Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::SetMotionRange().");
	}
	if (m_MotionRange != item || m_MotionRange_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MotionRange);
		m_MotionRange = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionRange) m_MotionRange->SetParent(m_myself.getPointer());
		if (m_MotionRange && m_MotionRange->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_MotionRange_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MotionRange->UseTypeAttribute = true;
		}
		m_MotionRange_Exist = true;
		if(m_MotionRange != Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr())
		{
			m_MotionRange->SetContentName(XMLString::transcode("MotionRange"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::InvalidateMotionRange()
{
	m_MotionRange_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("uncompensability")) == 0)
	{
		// uncompensability is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("intensity")) == 0)
	{
		// intensity is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MotionRange")) == 0)
	{
		// MotionRange is simple element MediaTranscodingHintsType_MotionHint_MotionRange_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMotionRange()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTranscodingHintsType_MotionHint_MotionRange_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMotionRange(p, client);
					if((p = GetMotionRange()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaTranscodingHintsType_MotionHint_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaTranscodingHintsType_MotionHint_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaTranscodingHintsType_MotionHint_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_uncompensability_Exist)
	{
	// Class
	if (m_uncompensability != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_uncompensability->ToText();
		element->setAttributeNS(X(""), X("uncompensability"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_intensity_Exist)
	{
	// Class
	if (m_intensity != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_intensity->ToText();
		element->setAttributeNS(X(""), X("intensity"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_MotionRange != Mp7JrsMediaTranscodingHintsType_MotionHint_MotionRange_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MotionRange->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MotionRange"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MotionRange->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("uncompensability")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("uncompensability")));
		this->Setuncompensability(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("intensity")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("intensity")));
		this->Setintensity(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MotionRange"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MotionRange")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTranscodingHintsType_MotionHint_MotionRange_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMotionRange(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MotionRange")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTranscodingHintsType_MotionHint_MotionRange_LocalType; // FTT, check this
	}
	this->SetMotionRange(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMotionRange() != Dc1NodePtr())
		result.Insert(GetMotionRange());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaTranscodingHintsType_MotionHint_LocalType_ExtMethodImpl.h


