
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtImplInclude.h


#include "Mp7JrsPositionType.h"
#include "Mp7JrsExtentType.h"
#include "Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtPropInit.h

}

IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::~IMp7JrsSemanticTimeType_SemanticTimeInterval_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtPropCleanup.h

}

Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::~Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType()
{
	Cleanup();
}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_TimePoint = Mp7JrsPositionPtr(); // Class
	m_TimePoint_Exist = false;
	m_Duration = Mp7JrsExtentPtr(); // Class
	m_Duration_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtMyPropInit.h

}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_TimePoint);
	// Dc1Factory::DeleteObject(m_Duration);
}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SemanticTimeType_SemanticTimeInterval_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISemanticTimeType_SemanticTimeInterval_LocalType
	const Dc1Ptr< Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidTimePoint())
	{
		// Dc1Factory::DeleteObject(m_TimePoint);
		this->SetTimePoint(Dc1Factory::CloneObject(tmp->GetTimePoint()));
	}
	else
	{
		InvalidateTimePoint();
	}
	if (tmp->IsValidDuration())
	{
		// Dc1Factory::DeleteObject(m_Duration);
		this->SetDuration(Dc1Factory::CloneObject(tmp->GetDuration()));
	}
	else
	{
		InvalidateDuration();
	}
}

Dc1NodePtr Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsPositionPtr Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetTimePoint() const
{
		return m_TimePoint;
}

// Element is optional
bool Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::IsValidTimePoint() const
{
	return m_TimePoint_Exist;
}

Mp7JrsExtentPtr Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetDuration() const
{
		return m_Duration;
}

// Element is optional
bool Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::IsValidDuration() const
{
	return m_Duration_Exist;
}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::SetTimePoint(const Mp7JrsPositionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::SetTimePoint().");
	}
	if (m_TimePoint != item || m_TimePoint_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TimePoint);
		m_TimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimePoint) m_TimePoint->SetParent(m_myself.getPointer());
		if (m_TimePoint && m_TimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PositionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimePoint->UseTypeAttribute = true;
		}
		m_TimePoint_Exist = true;
		if(m_TimePoint != Mp7JrsPositionPtr())
		{
			m_TimePoint->SetContentName(XMLString::transcode("TimePoint"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::InvalidateTimePoint()
{
	m_TimePoint_Exist = false;
}
void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::SetDuration(const Mp7JrsExtentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::SetDuration().");
	}
	if (m_Duration != item || m_Duration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Duration);
		m_Duration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Duration) m_Duration->SetParent(m_myself.getPointer());
		if (m_Duration && m_Duration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Duration->UseTypeAttribute = true;
		}
		m_Duration_Exist = true;
		if(m_Duration != Mp7JrsExtentPtr())
		{
			m_Duration->SetContentName(XMLString::transcode("Duration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::InvalidateDuration()
{
	m_Duration_Exist = false;
}

Dc1NodeEnum Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TimePoint")) == 0)
	{
		// TimePoint is simple element PositionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTimePoint()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PositionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTimePoint(p, client);
					if((p = GetTimePoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Duration")) == 0)
	{
		// Duration is simple element ExtentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ExtentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsExtentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDuration(p, client);
					if((p = GetDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SemanticTimeType_SemanticTimeInterval_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SemanticTimeType_SemanticTimeInterval_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SemanticTimeType_SemanticTimeInterval_LocalType"));
	// Element serialization:
	// Class
	
	if (m_TimePoint != Mp7JrsPositionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TimePoint->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Duration != Mp7JrsExtentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Duration->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Duration"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Duration->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePositionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Duration"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Duration")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateExtentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePositionType; // FTT, check this
	}
	this->SetTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("Duration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateExtentType; // FTT, check this
	}
	this->SetDuration(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimePoint() != Dc1NodePtr())
		result.Insert(GetTimePoint());
	if (GetDuration() != Dc1NodePtr())
		result.Insert(GetDuration());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSemanticTimeType_SemanticTimeInterval_LocalType_ExtMethodImpl.h


