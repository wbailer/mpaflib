
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtImplInclude.h


#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsHandWritingRecogResultType_Result_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsHandWritingRecogResultType_Result_LocalType::IMp7JrsHandWritingRecogResultType_Result_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtPropInit.h

}

IMp7JrsHandWritingRecogResultType_Result_LocalType::~IMp7JrsHandWritingRecogResultType_Result_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtPropCleanup.h

}

Mp7JrsHandWritingRecogResultType_Result_LocalType::Mp7JrsHandWritingRecogResultType_Result_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHandWritingRecogResultType_Result_LocalType::~Mp7JrsHandWritingRecogResultType_Result_LocalType()
{
	Cleanup();
}

void Mp7JrsHandWritingRecogResultType_Result_LocalType::Init()
{

	// Init attributes
	m_score = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_score->SetContent(0.0); // Use Value initialiser (xxx2)
	m_score_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Text = XMLString::transcode(""); //  Mandatory String
	m_LexiconIndexRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtMyPropInit.h

}

void Mp7JrsHandWritingRecogResultType_Result_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_score);
	XMLString::release(&this->m_Text);
	this->m_Text = NULL;
	// Dc1Factory::DeleteObject(m_LexiconIndexRef);
}

void Mp7JrsHandWritingRecogResultType_Result_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HandWritingRecogResultType_Result_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IHandWritingRecogResultType_Result_LocalType
	const Dc1Ptr< Mp7JrsHandWritingRecogResultType_Result_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_score);
	if (tmp->Existscore())
	{
		this->Setscore(Dc1Factory::CloneObject(tmp->Getscore()));
	}
	else
	{
		Invalidatescore();
	}
	}
		this->SetText(XMLString::replicate(tmp->GetText()));
		// Dc1Factory::DeleteObject(m_LexiconIndexRef);
		this->SetLexiconIndexRef(Dc1Factory::CloneObject(tmp->GetLexiconIndexRef()));
}

Dc1NodePtr Mp7JrsHandWritingRecogResultType_Result_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsHandWritingRecogResultType_Result_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrszeroToOnePtr Mp7JrsHandWritingRecogResultType_Result_LocalType::Getscore() const
{
	return m_score;
}

bool Mp7JrsHandWritingRecogResultType_Result_LocalType::Existscore() const
{
	return m_score_Exist;
}
void Mp7JrsHandWritingRecogResultType_Result_LocalType::Setscore(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType_Result_LocalType::Setscore().");
	}
	m_score = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_score) m_score->SetParent(m_myself.getPointer());
	m_score_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType_Result_LocalType::Invalidatescore()
{
	m_score_Exist = false;
}
XMLCh * Mp7JrsHandWritingRecogResultType_Result_LocalType::GetText() const
{
		return m_Text;
}

Mp7JrsReferencePtr Mp7JrsHandWritingRecogResultType_Result_LocalType::GetLexiconIndexRef() const
{
		return m_LexiconIndexRef;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsHandWritingRecogResultType_Result_LocalType::SetText(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType_Result_LocalType::SetText().");
	}
	if (m_Text != item)
	{
		XMLString::release(&m_Text);
		m_Text = item;
	// Dc1Factory::DeleteObject(m_LexiconIndexRef);
	m_LexiconIndexRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsHandWritingRecogResultType_Result_LocalType::SetLexiconIndexRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType_Result_LocalType::SetLexiconIndexRef().");
	}
	if (m_LexiconIndexRef != item)
	{
		// Dc1Factory::DeleteObject(m_LexiconIndexRef);
		m_LexiconIndexRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_LexiconIndexRef) m_LexiconIndexRef->SetParent(m_myself.getPointer());
		if (m_LexiconIndexRef && m_LexiconIndexRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_LexiconIndexRef->UseTypeAttribute = true;
		}
		if(m_LexiconIndexRef != Mp7JrsReferencePtr())
		{
			m_LexiconIndexRef->SetContentName(XMLString::transcode("LexiconIndexRef"));
		}
	XMLString::release(&this->m_Text);
	m_Text = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsHandWritingRecogResultType_Result_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("score")) == 0)
	{
		// score is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("LexiconIndexRef")) == 0)
	{
		// LexiconIndexRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLexiconIndexRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLexiconIndexRef(p, client);
					if((p = GetLexiconIndexRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HandWritingRecogResultType_Result_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HandWritingRecogResultType_Result_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsHandWritingRecogResultType_Result_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsHandWritingRecogResultType_Result_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsHandWritingRecogResultType_Result_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogResultType_Result_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_score_Exist)
	{
	// Class
	if (m_score != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_score->ToText();
		element->setAttributeNS(X(""), X("score"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	 // String
	if(m_Text != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_Text, X("urn:mpeg:mpeg7:schema:2004"), X("Text"), false);
	// Class
	
	if (m_LexiconIndexRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_LexiconIndexRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("LexiconIndexRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_LexiconIndexRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsHandWritingRecogResultType_Result_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("score")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("score")));
		this->Setscore(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("Text"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetText(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetText(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("LexiconIndexRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("LexiconIndexRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLexiconIndexRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHandWritingRecogResultType_Result_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("LexiconIndexRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetLexiconIndexRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHandWritingRecogResultType_Result_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLexiconIndexRef() != Dc1NodePtr())
		result.Insert(GetLexiconIndexRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_Result_LocalType_ExtMethodImpl.h


