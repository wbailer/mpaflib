
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtImplInclude.h


#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType.h"
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType.h" // Sequence collection Pixel
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType.h" // Sequence collection element Pixel
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType.h" // Sequence collection element CoordPoint
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0.h" // Sequence collection CurrPixel
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType.h" // Sequence collection element CurrPixel
#include "Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType.h" // Sequence collection element SrcPixel

#include <assert.h>
IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1()
{

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtPropInit.h

}

IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::~IMp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtPropCleanup.h

}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::~Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1()
{
	Cleanup();
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Init()
{

	// Init attributes
	m_name = NULL; // String
	m_dataSet = NULL; // String
	m_dataSet_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr(); // Collection
	m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr(); // Collection
	m_MappingFunct = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtMyPropInit.h

}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtMyPropCleanup.h


	XMLString::release(&m_name); // String
	XMLString::release(&m_dataSet); // String
	// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType);
	// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0);
	// Dc1Factory::DeleteObject(m_MappingFunct);
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1Ptr, since we
	// might need GetBase(), which isn't defined in ISpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1
	const Dc1Ptr< Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1 > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_name); // String
		this->Setname(XMLString::replicate(tmp->Getname()));
	}
	{
	XMLString::release(&m_dataSet); // String
	if (tmp->ExistdataSet())
	{
		this->SetdataSet(XMLString::replicate(tmp->GetdataSet()));
	}
	else
	{
		InvalidatedataSet();
	}
	}
		// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType);
		this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(Dc1Factory::CloneObject(tmp->GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType()));
		// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0);
		this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(Dc1Factory::CloneObject(tmp->GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0()));
		// Dc1Factory::DeleteObject(m_MappingFunct);
		this->SetMappingFunct(Dc1Factory::CloneObject(tmp->GetMappingFunct()));
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Getname() const
{
	return m_name;
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Setname().");
	}
	m_name = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetdataSet() const
{
	return m_dataSet;
}

bool Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::ExistdataSet() const
{
	return m_dataSet_Exist;
}
void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetdataSet(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetdataSet().");
	}
	m_dataSet = item;
	m_dataSet_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::InvalidatedataSet()
{
	m_dataSet_Exist = false;
}
Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType() const
{
		return m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType;
}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0() const
{
		return m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0;
}

Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetMappingFunct() const
{
		return m_MappingFunct;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(const Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType().");
	}
	if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType);
		m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType) m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType->SetParent(m_myself.getPointer());
	// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0);
	m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(const Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0().");
	}
	if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0);
		m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0) m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0->SetParent(m_myself.getPointer());
	// Dc1Factory::DeleteObject(m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType);
	m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType = Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetMappingFunct(const Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::SetMappingFunct().");
	}
	if (m_MappingFunct != item)
	{
		// Dc1Factory::DeleteObject(m_MappingFunct);
		m_MappingFunct = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MappingFunct) m_MappingFunct->SetParent(m_myself.getPointer());
		if(m_MappingFunct != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr())
		{
			m_MappingFunct->SetContentName(XMLString::transcode("MappingFunct"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("name")) == 0)
	{
		// name is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dataSet")) == 0)
	{
		// dataSet is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Pixel")) == 0)
	{
		// Pixel is contained in itemtype Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType
		// in sequence collection Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType

		context->Found = true;
		Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr coll = GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType; // FTT, check this
				SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 3))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->GetPixel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr item = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j))->SetPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j+1))->GetPixel());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j))->SetPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j-1))->GetPixel());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Pixel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->SetPixel(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->GetPixel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CoordPoint")) == 0)
	{
		// CoordPoint is contained in itemtype Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType
		// in sequence collection Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType

		context->Found = true;
		Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr coll = GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType; // FTT, check this
				SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 3))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->GetCoordPoint()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr item = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j))->SetCoordPoint(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j+1))->GetCoordPoint());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j))->SetCoordPoint(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(j-1))->GetCoordPoint());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CoordPoint_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->SetCoordPoint(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalPtr)coll->elementAt(i))->GetCoordPoint()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CurrPixel")) == 0)
	{
		// CurrPixel is contained in itemtype Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0
		// in sequence collection Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0

		context->Found = true;
		Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr coll = GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0; // FTT, check this
				SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 3))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->GetCurrPixel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr item = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j))->SetCurrPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j+1))->GetCurrPixel());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j))->SetCurrPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j-1))->GetCurrPixel());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CurrPixel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->SetCurrPixel(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->GetCurrPixel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SrcPixel")) == 0)
	{
		// SrcPixel is contained in itemtype Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0
		// in sequence collection Spatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0

		context->Found = true;
		Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr coll = GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0; // FTT, check this
				SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 3))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->GetSrcPixel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr item = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j))->SetSrcPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j+1))->GetSrcPixel());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j))->SetSrcPixel(
						((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(j-1))->GetSrcPixel());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Spatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_SrcPixel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->SetSrcPixel(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Local0Ptr)coll->elementAt(i))->GetSrcPixel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MappingFunct")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1");
	}
	return result;
}

XMLCh * Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1"));
	// Attribute Serialization:
	// String
	if(m_name != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("name"), m_name);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dataSet_Exist)
	{
	// String
	if(m_dataSet != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("dataSet"), m_dataSet);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0 != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Spatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_MappingFunct != Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MappingFunct->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("name")))
	{
		// Deserialize string type
		this->Setname(Dc1Convert::TextToString(parent->getAttribute(X("name"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dataSet")))
	{
		// Deserialize string type
		this->SetdataSet(Dc1Convert::TextToString(parent->getAttribute(X("dataSet"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
	do // Deserialize choice just one time!
	{
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Pixel")) == 0)
			(Dc1Util::HasNodeName(parent, X("Pixel"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CoordPoint")) == 0)
			(Dc1Util::HasNodeName(parent, X("CoordPoint"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CurrPixel")) == 0)
			(Dc1Util::HasNodeName(parent, X("CurrPixel"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SrcPixel")) == 0)
			(Dc1Util::HasNodeName(parent, X("SrcPixel"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MappingFunct"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MappingFunct")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMappingFunct(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType; // FTT, check this
	this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_Collection0Ptr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_CollectionType0; // FTT, check this
	this->SetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MappingFunct")) == 0))
  {
	Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionPtr tmp = CreateSpatial2DCoordinateSystemType_LocalCoordinateSystem_MappingFunct_CollectionType; // FTT, check this
	this->SetMappingFunct(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMappingFunct() != Dc1NodePtr())
		result.Insert(GetMappingFunct());
	if (GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType() != Dc1NodePtr())
		result.Insert(GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType());
	if (GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0() != Dc1NodePtr())
		result.Insert(GetSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType0());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpatial2DCoordinateSystemType_LocalCoordinateSystem_LocalType1_ExtMethodImpl.h


