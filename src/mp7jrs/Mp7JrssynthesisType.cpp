
// Based on EnumerationType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/


#include <stdio.h>
#include <xercesc/util/Janitor.hpp>

// no includefile for extension defined 
// file Mp7JrssynthesisType_ExtImplInclude.h


#include "Mp7JrssynthesisType.h"

// no includefile for extension defined 
// file Mp7JrssynthesisType_ExtMethodImpl.h


const XMLCh * Mp7JrssynthesisType::nsURI(){ return X("urn:mpeg:mpeg7:schema:2004");}

XMLCh * Mp7JrssynthesisType::ToText(Mp7JrssynthesisType::Enumeration item)
{
	XMLCh * result = NULL;
	switch(item)
	{
		case /*Enumeration::*/none:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("none");
			break;
		case /*Enumeration::*/dependency:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("dependency");
			break;
		case /*Enumeration::*/forward:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("forward");
			break;
		case /*Enumeration::*/backward:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("backward");
			break;
		case /*Enumeration::*/coordination:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("coordination");
			break;
		case /*Enumeration::*/apposition:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("apposition");
			break;
		case /*Enumeration::*/repair:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("repair");
			break;
		case /*Enumeration::*/error:
			result = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("error");
			break;
		default:; break;
	}	
	
	// Note: You must release this result after using it
	return result;
}

Mp7JrssynthesisType::Enumeration Mp7JrssynthesisType::Parse(const XMLCh * const txt)
{
	char * item = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(txt);
	Mp7JrssynthesisType::Enumeration result = Mp7JrssynthesisType::/*Enumeration::*/UninitializedEnumeration;
	
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "none") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/none;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "dependency") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/dependency;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "forward") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/forward;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "backward") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/backward;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "coordination") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/coordination;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "apposition") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/apposition;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "repair") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/repair;
	}
	if(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::compareString(item, "error") == 0)
	{
		result = Mp7JrssynthesisType::/*Enumeration::*/error;
	}
	

	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&item);
	return result;
}
