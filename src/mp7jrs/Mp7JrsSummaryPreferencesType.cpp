
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrspreferenceValueType.h"
#include "Mp7JrsSummaryPreferencesType_SummaryType_CollectionType.h"
#include "Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSummaryPreferencesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSummaryPreferencesType_SummaryType_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SummaryType
#include "Mp7JrsSummaryPreferencesType_SummaryTheme_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SummaryTheme

#include <assert.h>
IMp7JrsSummaryPreferencesType::IMp7JrsSummaryPreferencesType()
{

// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtPropInit.h

}

IMp7JrsSummaryPreferencesType::~IMp7JrsSummaryPreferencesType()
{
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtPropCleanup.h

}

Mp7JrsSummaryPreferencesType::Mp7JrsSummaryPreferencesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSummaryPreferencesType::~Mp7JrsSummaryPreferencesType()
{
	Cleanup();
}

void Mp7JrsSummaryPreferencesType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_preferenceValue = CreatepreferenceValueType; // Create a valid class, FTT, check this
	m_preferenceValue->SetContent(-100); // Use Value initialiser (xxx2)
	m_preferenceValue_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_SummaryType = Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr(); // Collection
	m_SummaryTheme = Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr(); // Collection
	m_SummaryDuration = Mp7JrsmediaDurationPtr(); // Pattern
	m_SummaryDuration_Exist = false;
	m_MinSummaryDuration = Mp7JrsmediaDurationPtr(); // Pattern
	m_MinSummaryDuration_Exist = false;
	m_MaxSummaryDuration = Mp7JrsmediaDurationPtr(); // Pattern
	m_MaxSummaryDuration_Exist = false;
	m_NumOfKeyFrames = (1); // Value

	m_NumOfKeyFrames_Exist = false;
	m_MinNumOfKeyFrames = (1); // Value

	m_MinNumOfKeyFrames_Exist = false;
	m_MaxNumOfKeyFrames = (1); // Value

	m_MaxNumOfKeyFrames_Exist = false;
	m_NumOfChars = (1); // Value

	m_NumOfChars_Exist = false;
	m_MinNumOfChars = (1); // Value

	m_MinNumOfChars_Exist = false;
	m_MaxNumOfChars = (1); // Value

	m_MaxNumOfChars_Exist = false;


// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMyPropInit.h

}

void Mp7JrsSummaryPreferencesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_preferenceValue);
	// Dc1Factory::DeleteObject(m_SummaryType);
	// Dc1Factory::DeleteObject(m_SummaryTheme);
	// Dc1Factory::DeleteObject(m_SummaryDuration);
	// Dc1Factory::DeleteObject(m_MinSummaryDuration);
	// Dc1Factory::DeleteObject(m_MaxSummaryDuration);
}

void Mp7JrsSummaryPreferencesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SummaryPreferencesTypePtr, since we
	// might need GetBase(), which isn't defined in ISummaryPreferencesType
	const Dc1Ptr< Mp7JrsSummaryPreferencesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSummaryPreferencesType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_preferenceValue);
	if (tmp->ExistpreferenceValue())
	{
		this->SetpreferenceValue(Dc1Factory::CloneObject(tmp->GetpreferenceValue()));
	}
	else
	{
		InvalidatepreferenceValue();
	}
	}
		// Dc1Factory::DeleteObject(m_SummaryType);
		this->SetSummaryType(Dc1Factory::CloneObject(tmp->GetSummaryType()));
		// Dc1Factory::DeleteObject(m_SummaryTheme);
		this->SetSummaryTheme(Dc1Factory::CloneObject(tmp->GetSummaryTheme()));
	if (tmp->IsValidSummaryDuration())
	{
		// Dc1Factory::DeleteObject(m_SummaryDuration);
		this->SetSummaryDuration(Dc1Factory::CloneObject(tmp->GetSummaryDuration()));
	}
	else
	{
		InvalidateSummaryDuration();
	}
	if (tmp->IsValidMinSummaryDuration())
	{
		// Dc1Factory::DeleteObject(m_MinSummaryDuration);
		this->SetMinSummaryDuration(Dc1Factory::CloneObject(tmp->GetMinSummaryDuration()));
	}
	else
	{
		InvalidateMinSummaryDuration();
	}
	if (tmp->IsValidMaxSummaryDuration())
	{
		// Dc1Factory::DeleteObject(m_MaxSummaryDuration);
		this->SetMaxSummaryDuration(Dc1Factory::CloneObject(tmp->GetMaxSummaryDuration()));
	}
	else
	{
		InvalidateMaxSummaryDuration();
	}
	if (tmp->IsValidNumOfKeyFrames())
	{
		this->SetNumOfKeyFrames(tmp->GetNumOfKeyFrames());
	}
	else
	{
		InvalidateNumOfKeyFrames();
	}
	if (tmp->IsValidMinNumOfKeyFrames())
	{
		this->SetMinNumOfKeyFrames(tmp->GetMinNumOfKeyFrames());
	}
	else
	{
		InvalidateMinNumOfKeyFrames();
	}
	if (tmp->IsValidMaxNumOfKeyFrames())
	{
		this->SetMaxNumOfKeyFrames(tmp->GetMaxNumOfKeyFrames());
	}
	else
	{
		InvalidateMaxNumOfKeyFrames();
	}
	if (tmp->IsValidNumOfChars())
	{
		this->SetNumOfChars(tmp->GetNumOfChars());
	}
	else
	{
		InvalidateNumOfChars();
	}
	if (tmp->IsValidMinNumOfChars())
	{
		this->SetMinNumOfChars(tmp->GetMinNumOfChars());
	}
	else
	{
		InvalidateMinNumOfChars();
	}
	if (tmp->IsValidMaxNumOfChars())
	{
		this->SetMaxNumOfChars(tmp->GetMaxNumOfChars());
	}
	else
	{
		InvalidateMaxNumOfChars();
	}
}

Dc1NodePtr Mp7JrsSummaryPreferencesType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSummaryPreferencesType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSummaryPreferencesType::GetBase() const
{
	return m_Base;
}

Mp7JrspreferenceValuePtr Mp7JrsSummaryPreferencesType::GetpreferenceValue() const
{
	if (this->ExistpreferenceValue()) {
		return m_preferenceValue;
	} else {
		return m_preferenceValue_Default;
	}
}

bool Mp7JrsSummaryPreferencesType::ExistpreferenceValue() const
{
	return m_preferenceValue_Exist;
}
void Mp7JrsSummaryPreferencesType::SetpreferenceValue(const Mp7JrspreferenceValuePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetpreferenceValue().");
	}
	m_preferenceValue = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_preferenceValue) m_preferenceValue->SetParent(m_myself.getPointer());
	m_preferenceValue_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidatepreferenceValue()
{
	m_preferenceValue_Exist = false;
}
Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr Mp7JrsSummaryPreferencesType::GetSummaryType() const
{
		return m_SummaryType;
}

Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr Mp7JrsSummaryPreferencesType::GetSummaryTheme() const
{
		return m_SummaryTheme;
}

Mp7JrsmediaDurationPtr Mp7JrsSummaryPreferencesType::GetSummaryDuration() const
{
		return m_SummaryDuration;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidSummaryDuration() const
{
	return m_SummaryDuration_Exist;
}

Mp7JrsmediaDurationPtr Mp7JrsSummaryPreferencesType::GetMinSummaryDuration() const
{
		return m_MinSummaryDuration;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMinSummaryDuration() const
{
	return m_MinSummaryDuration_Exist;
}

Mp7JrsmediaDurationPtr Mp7JrsSummaryPreferencesType::GetMaxSummaryDuration() const
{
		return m_MaxSummaryDuration;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMaxSummaryDuration() const
{
	return m_MaxSummaryDuration_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetNumOfKeyFrames() const
{
		return m_NumOfKeyFrames;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidNumOfKeyFrames() const
{
	return m_NumOfKeyFrames_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetMinNumOfKeyFrames() const
{
		return m_MinNumOfKeyFrames;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMinNumOfKeyFrames() const
{
	return m_MinNumOfKeyFrames_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetMaxNumOfKeyFrames() const
{
		return m_MaxNumOfKeyFrames;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMaxNumOfKeyFrames() const
{
	return m_MaxNumOfKeyFrames_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetNumOfChars() const
{
		return m_NumOfChars;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidNumOfChars() const
{
	return m_NumOfChars_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetMinNumOfChars() const
{
		return m_MinNumOfChars;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMinNumOfChars() const
{
	return m_MinNumOfChars_Exist;
}

unsigned Mp7JrsSummaryPreferencesType::GetMaxNumOfChars() const
{
		return m_MaxNumOfChars;
}

// Element is optional
bool Mp7JrsSummaryPreferencesType::IsValidMaxNumOfChars() const
{
	return m_MaxNumOfChars_Exist;
}

void Mp7JrsSummaryPreferencesType::SetSummaryType(const Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetSummaryType().");
	}
	if (m_SummaryType != item)
	{
		// Dc1Factory::DeleteObject(m_SummaryType);
		m_SummaryType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummaryType) m_SummaryType->SetParent(m_myself.getPointer());
		if(m_SummaryType != Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr())
		{
			m_SummaryType->SetContentName(XMLString::transcode("SummaryType"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::SetSummaryTheme(const Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetSummaryTheme().");
	}
	if (m_SummaryTheme != item)
	{
		// Dc1Factory::DeleteObject(m_SummaryTheme);
		m_SummaryTheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummaryTheme) m_SummaryTheme->SetParent(m_myself.getPointer());
		if(m_SummaryTheme != Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr())
		{
			m_SummaryTheme->SetContentName(XMLString::transcode("SummaryTheme"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::SetSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetSummaryDuration().");
	}
	if (m_SummaryDuration != item || m_SummaryDuration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SummaryDuration);
		m_SummaryDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummaryDuration) m_SummaryDuration->SetParent(m_myself.getPointer());
		if (m_SummaryDuration && m_SummaryDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SummaryDuration->UseTypeAttribute = true;
		}
		m_SummaryDuration_Exist = true;
		if(m_SummaryDuration != Mp7JrsmediaDurationPtr())
		{
			m_SummaryDuration->SetContentName(XMLString::transcode("SummaryDuration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateSummaryDuration()
{
	m_SummaryDuration_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMinSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMinSummaryDuration().");
	}
	if (m_MinSummaryDuration != item || m_MinSummaryDuration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MinSummaryDuration);
		m_MinSummaryDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MinSummaryDuration) m_MinSummaryDuration->SetParent(m_myself.getPointer());
		if (m_MinSummaryDuration && m_MinSummaryDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MinSummaryDuration->UseTypeAttribute = true;
		}
		m_MinSummaryDuration_Exist = true;
		if(m_MinSummaryDuration != Mp7JrsmediaDurationPtr())
		{
			m_MinSummaryDuration->SetContentName(XMLString::transcode("MinSummaryDuration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMinSummaryDuration()
{
	m_MinSummaryDuration_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMaxSummaryDuration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMaxSummaryDuration().");
	}
	if (m_MaxSummaryDuration != item || m_MaxSummaryDuration_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_MaxSummaryDuration);
		m_MaxSummaryDuration = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MaxSummaryDuration) m_MaxSummaryDuration->SetParent(m_myself.getPointer());
		if (m_MaxSummaryDuration && m_MaxSummaryDuration->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MaxSummaryDuration->UseTypeAttribute = true;
		}
		m_MaxSummaryDuration_Exist = true;
		if(m_MaxSummaryDuration != Mp7JrsmediaDurationPtr())
		{
			m_MaxSummaryDuration->SetContentName(XMLString::transcode("MaxSummaryDuration"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMaxSummaryDuration()
{
	m_MaxSummaryDuration_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetNumOfKeyFrames(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetNumOfKeyFrames().");
	}
	if (m_NumOfKeyFrames != item || m_NumOfKeyFrames_Exist == false)
	{
		m_NumOfKeyFrames = item;
		m_NumOfKeyFrames_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateNumOfKeyFrames()
{
	m_NumOfKeyFrames_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMinNumOfKeyFrames(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMinNumOfKeyFrames().");
	}
	if (m_MinNumOfKeyFrames != item || m_MinNumOfKeyFrames_Exist == false)
	{
		m_MinNumOfKeyFrames = item;
		m_MinNumOfKeyFrames_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMinNumOfKeyFrames()
{
	m_MinNumOfKeyFrames_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMaxNumOfKeyFrames(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMaxNumOfKeyFrames().");
	}
	if (m_MaxNumOfKeyFrames != item || m_MaxNumOfKeyFrames_Exist == false)
	{
		m_MaxNumOfKeyFrames = item;
		m_MaxNumOfKeyFrames_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMaxNumOfKeyFrames()
{
	m_MaxNumOfKeyFrames_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetNumOfChars(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetNumOfChars().");
	}
	if (m_NumOfChars != item || m_NumOfChars_Exist == false)
	{
		m_NumOfChars = item;
		m_NumOfChars_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateNumOfChars()
{
	m_NumOfChars_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMinNumOfChars(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMinNumOfChars().");
	}
	if (m_MinNumOfChars != item || m_MinNumOfChars_Exist == false)
	{
		m_MinNumOfChars = item;
		m_MinNumOfChars_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMinNumOfChars()
{
	m_MinNumOfChars_Exist = false;
}
void Mp7JrsSummaryPreferencesType::SetMaxNumOfChars(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetMaxNumOfChars().");
	}
	if (m_MaxNumOfChars != item || m_MaxNumOfChars_Exist == false)
	{
		m_MaxNumOfChars = item;
		m_MaxNumOfChars_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidateMaxNumOfChars()
{
	m_MaxNumOfChars_Exist = false;
}
XMLCh * Mp7JrsSummaryPreferencesType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSummaryPreferencesType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSummaryPreferencesType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSummaryPreferencesType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSummaryPreferencesType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSummaryPreferencesType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSummaryPreferencesType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSummaryPreferencesType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSummaryPreferencesType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSummaryPreferencesType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSummaryPreferencesType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSummaryPreferencesType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSummaryPreferencesType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSummaryPreferencesType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSummaryPreferencesType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummaryPreferencesType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSummaryPreferencesType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSummaryPreferencesType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummaryPreferencesType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSummaryPreferencesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("preferenceValue")) == 0)
	{
		// preferenceValue is simple attribute preferenceValueType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspreferenceValuePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummaryType")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SummaryType is item of type SummaryPreferencesType_SummaryType_LocalType
		// in element collection SummaryPreferencesType_SummaryType_CollectionType
		
		context->Found = true;
		Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr coll = GetSummaryType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummaryPreferencesType_SummaryType_CollectionType; // FTT, check this
				SetSummaryType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryType_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummaryPreferencesType_SummaryType_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummaryTheme")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:SummaryTheme is item of type SummaryPreferencesType_SummaryTheme_LocalType
		// in element collection SummaryPreferencesType_SummaryTheme_CollectionType
		
		context->Found = true;
		Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr coll = GetSummaryTheme();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummaryPreferencesType_SummaryTheme_CollectionType; // FTT, check this
				SetSummaryTheme(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryPreferencesType_SummaryTheme_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummaryPreferencesType_SummaryTheme_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummaryDuration")) == 0)
	{
		// SummaryDuration is simple element mediaDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSummaryDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSummaryDuration(p, client);
					if((p = GetSummaryDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MinSummaryDuration")) == 0)
	{
		// MinSummaryDuration is simple element mediaDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMinSummaryDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMinSummaryDuration(p, client);
					if((p = GetMinSummaryDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MaxSummaryDuration")) == 0)
	{
		// MaxSummaryDuration is simple element mediaDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMaxSummaryDuration()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMaxSummaryDuration(p, client);
					if((p = GetMaxSummaryDuration()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SummaryPreferencesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SummaryPreferencesType");
	}
	return result;
}

XMLCh * Mp7JrsSummaryPreferencesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSummaryPreferencesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSummaryPreferencesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SummaryPreferencesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_preferenceValue_Exist)
	{
	// Class
	if (m_preferenceValue != Mp7JrspreferenceValuePtr())
	{
		XMLCh * tmp = m_preferenceValue->ToText();
		element->setAttributeNS(X(""), X("preferenceValue"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_SummaryType != Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SummaryType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SummaryTheme != Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SummaryTheme->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_SummaryDuration != Mp7JrsmediaDurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("SummaryDuration");
//		m_SummaryDuration->SetContentName(contentname);
		m_SummaryDuration->UseTypeAttribute = this->UseTypeAttribute;
		m_SummaryDuration->Serialize(doc, element);
	}
	if(m_MinSummaryDuration != Mp7JrsmediaDurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MinSummaryDuration");
//		m_MinSummaryDuration->SetContentName(contentname);
		m_MinSummaryDuration->UseTypeAttribute = this->UseTypeAttribute;
		m_MinSummaryDuration->Serialize(doc, element);
	}
	if(m_MaxSummaryDuration != Mp7JrsmediaDurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MaxSummaryDuration");
//		m_MaxSummaryDuration->SetContentName(contentname);
		m_MaxSummaryDuration->UseTypeAttribute = this->UseTypeAttribute;
		m_MaxSummaryDuration->Serialize(doc, element);
	}
	if(m_NumOfKeyFrames_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_NumOfKeyFrames);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("NumOfKeyFrames"), true);
		XMLString::release(&tmp);
	}
	if(m_MinNumOfKeyFrames_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_MinNumOfKeyFrames);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("MinNumOfKeyFrames"), true);
		XMLString::release(&tmp);
	}
	if(m_MaxNumOfKeyFrames_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_MaxNumOfKeyFrames);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("MaxNumOfKeyFrames"), true);
		XMLString::release(&tmp);
	}
	if(m_NumOfChars_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_NumOfChars);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("NumOfChars"), true);
		XMLString::release(&tmp);
	}
	if(m_MinNumOfChars_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_MinNumOfChars);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("MinNumOfChars"), true);
		XMLString::release(&tmp);
	}
	if(m_MaxNumOfChars_Exist)
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_MaxNumOfChars);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("MaxNumOfChars"), true);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsSummaryPreferencesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("preferenceValue")))
	{
		// Deserialize class type
		Mp7JrspreferenceValuePtr tmp = CreatepreferenceValueType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("preferenceValue")));
		this->SetpreferenceValue(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SummaryType"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SummaryType")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr tmp = CreateSummaryPreferencesType_SummaryType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummaryType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("SummaryTheme"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("SummaryTheme")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr tmp = CreateSummaryPreferencesType_SummaryTheme_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummaryTheme(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("SummaryDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SummaryDuration")) == 0))
		{
			Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummaryDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MinSummaryDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MinSummaryDuration")) == 0))
		{
			Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMinSummaryDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MaxSummaryDuration"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MaxSummaryDuration")) == 0))
		{
			Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMaxSummaryDuration(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("NumOfKeyFrames"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetNumOfKeyFrames(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("MinNumOfKeyFrames"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetMinNumOfKeyFrames(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("MaxNumOfKeyFrames"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetMaxNumOfKeyFrames(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("NumOfChars"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetNumOfChars(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("MinNumOfChars"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetMinNumOfChars(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("MaxNumOfChars"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetMaxNumOfChars(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSummaryPreferencesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SummaryType")) == 0))
  {
	Mp7JrsSummaryPreferencesType_SummaryType_CollectionPtr tmp = CreateSummaryPreferencesType_SummaryType_CollectionType; // FTT, check this
	this->SetSummaryType(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("SummaryTheme")) == 0))
  {
	Mp7JrsSummaryPreferencesType_SummaryTheme_CollectionPtr tmp = CreateSummaryPreferencesType_SummaryTheme_CollectionType; // FTT, check this
	this->SetSummaryTheme(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("SummaryDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaDurationType; // FTT, check this
	}
	this->SetSummaryDuration(child);
  }
  if (XMLString::compareString(elementname, X("MinSummaryDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaDurationType; // FTT, check this
	}
	this->SetMinSummaryDuration(child);
  }
  if (XMLString::compareString(elementname, X("MaxSummaryDuration")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaDurationType; // FTT, check this
	}
	this->SetMaxSummaryDuration(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSummaryPreferencesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSummaryType() != Dc1NodePtr())
		result.Insert(GetSummaryType());
	if (GetSummaryTheme() != Dc1NodePtr())
		result.Insert(GetSummaryTheme());
	if (GetSummaryDuration() != Dc1NodePtr())
		result.Insert(GetSummaryDuration());
	if (GetMinSummaryDuration() != Dc1NodePtr())
		result.Insert(GetMinSummaryDuration());
	if (GetMaxSummaryDuration() != Dc1NodePtr())
		result.Insert(GetMaxSummaryDuration());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSummaryPreferencesType_ExtMethodImpl.h


