
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtImplInclude.h


#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrstermReferenceType.h"
#include "Mp7JrsInlineTermDefinitionType_Name_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Definition_CollectionType.h"
#include "Mp7JrsInlineTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsClassificationType_Genre_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsInlineTermDefinitionType_Name_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Definition
#include "Mp7JrsInlineTermDefinitionType_Term_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Term

#include <assert.h>
IMp7JrsClassificationType_Genre_LocalType::IMp7JrsClassificationType_Genre_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtPropInit.h

}

IMp7JrsClassificationType_Genre_LocalType::~IMp7JrsClassificationType_Genre_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtPropCleanup.h

}

Mp7JrsClassificationType_Genre_LocalType::Mp7JrsClassificationType_Genre_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationType_Genre_LocalType::~Mp7JrsClassificationType_Genre_LocalType()
{
	Cleanup();
}

void Mp7JrsClassificationType_Genre_LocalType::Init()
{
	// Init base
	m_Base = CreateControlledTermUseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_type = Mp7JrsClassificationType_Genre_type_LocalType::UninitializedEnumeration;
	m_type_Default = Mp7JrsClassificationType_Genre_type_LocalType::main; // Default enumeration
	m_type_Exist = false;



// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtMyPropInit.h

}

void Mp7JrsClassificationType_Genre_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsClassificationType_Genre_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationType_Genre_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationType_Genre_LocalType
	const Dc1Ptr< Mp7JrsClassificationType_Genre_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsControlledTermUsePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationType_Genre_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existtype())
	{
		this->Settype(tmp->Gettype());
	}
	else
	{
		Invalidatetype();
	}
	}
}

Dc1NodePtr Mp7JrsClassificationType_Genre_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationType_Genre_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsControlledTermUseType > Mp7JrsClassificationType_Genre_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsClassificationType_Genre_type_LocalType::Enumeration Mp7JrsClassificationType_Genre_LocalType::Gettype() const
{
	if (this->Existtype()) {
		return m_type;
	} else {
		return m_type_Default;
	}
}

bool Mp7JrsClassificationType_Genre_LocalType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsClassificationType_Genre_LocalType::Settype(Mp7JrsClassificationType_Genre_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Genre_LocalType::Settype().");
	}
	m_type = item;
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType_Genre_LocalType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrstermReferencePtr Mp7JrsClassificationType_Genre_LocalType::Gethref() const
{
	return GetBase()->Gethref();
}

void Mp7JrsClassificationType_Genre_LocalType::Sethref(const Mp7JrstermReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Genre_LocalType::Sethref().");
	}
	GetBase()->Sethref(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsInlineTermDefinitionType_Name_CollectionPtr Mp7JrsClassificationType_Genre_LocalType::GetName() const
{
	return GetBase()->GetBase()->GetName();
}

Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr Mp7JrsClassificationType_Genre_LocalType::GetDefinition() const
{
	return GetBase()->GetBase()->GetDefinition();
}

Mp7JrsInlineTermDefinitionType_Term_CollectionPtr Mp7JrsClassificationType_Genre_LocalType::GetTerm() const
{
	return GetBase()->GetBase()->GetTerm();
}

void Mp7JrsClassificationType_Genre_LocalType::SetName(const Mp7JrsInlineTermDefinitionType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Genre_LocalType::SetName().");
	}
	GetBase()->GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType_Genre_LocalType::SetDefinition(const Mp7JrsInlineTermDefinitionType_Definition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Genre_LocalType::SetDefinition().");
	}
	GetBase()->GetBase()->SetDefinition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationType_Genre_LocalType::SetTerm(const Mp7JrsInlineTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationType_Genre_LocalType::SetTerm().");
	}
	GetBase()->GetBase()->SetTerm(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationType_Genre_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute ClassificationType_Genre_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsClassificationType_Genre_type_LocalType::Enumeration");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationType_Genre_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationType_Genre_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationType_Genre_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsClassificationType_Genre_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsClassificationType_Genre_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationType_Genre_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Enumeration
	if(m_type != Mp7JrsClassificationType_Genre_type_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsClassificationType_Genre_type_LocalType::ToText(m_type);
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsClassificationType_Genre_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		this->Settype(Mp7JrsClassificationType_Genre_type_LocalType::Parse(parent->getAttribute(X("type"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsControlledTermUseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationType_Genre_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationType_Genre_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDefinition() != Dc1NodePtr())
		result.Insert(GetDefinition());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationType_Genre_LocalType_ExtMethodImpl.h


