
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentManagementType.h"
#include "Mp7JrsMediaDescriptionType_MediaInformation_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsMediaDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsMediaInformationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MediaInformation

#include <assert.h>
IMp7JrsMediaDescriptionType::IMp7JrsMediaDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtPropInit.h

}

IMp7JrsMediaDescriptionType::~IMp7JrsMediaDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtPropCleanup.h

}

Mp7JrsMediaDescriptionType::Mp7JrsMediaDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaDescriptionType::~Mp7JrsMediaDescriptionType()
{
	Cleanup();
}

void Mp7JrsMediaDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentManagementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_MediaInformation = Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtMyPropInit.h

}

void Mp7JrsMediaDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_MediaInformation);
}

void Mp7JrsMediaDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaDescriptionType
	const Dc1Ptr< Mp7JrsMediaDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentManagementPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_MediaInformation);
		this->SetMediaInformation(Dc1Factory::CloneObject(tmp->GetMediaInformation()));
}

Dc1NodePtr Mp7JrsMediaDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentManagementType > Mp7JrsMediaDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr Mp7JrsMediaDescriptionType::GetMediaInformation() const
{
		return m_MediaInformation;
}

void Mp7JrsMediaDescriptionType::SetMediaInformation(const Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaDescriptionType::SetMediaInformation().");
	}
	if (m_MediaInformation != item)
	{
		// Dc1Factory::DeleteObject(m_MediaInformation);
		m_MediaInformation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaInformation) m_MediaInformation->SetParent(m_myself.getPointer());
		if(m_MediaInformation != Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr())
		{
			m_MediaInformation->SetContentName(XMLString::transcode("MediaInformation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsMediaDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsMediaDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsMediaDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsMediaDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsMediaDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsMediaDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("MediaInformation")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:MediaInformation is item of type MediaInformationType
		// in element collection MediaDescriptionType_MediaInformation_CollectionType
		
		context->Found = true;
		Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr coll = GetMediaInformation();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaDescriptionType_MediaInformation_CollectionType; // FTT, check this
				SetMediaInformation(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaInformationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaInformationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsMediaDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMediaDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMediaDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaDescriptionType"));
	// Element serialization:
	if (m_MediaInformation != Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MediaInformation->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMediaDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentManagementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("MediaInformation"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("MediaInformation")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr tmp = CreateMediaDescriptionType_MediaInformation_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaInformation(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MediaInformation")) == 0))
  {
	Mp7JrsMediaDescriptionType_MediaInformation_CollectionPtr tmp = CreateMediaDescriptionType_MediaInformation_CollectionType; // FTT, check this
	this->SetMediaInformation(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaDescriptionType_ExtMethodImpl.h


