
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtImplInclude.h


#include "Mp7JrsAnalyticTransitionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTemporalMaskType.h"
#include "Mp7JrsVideoSegmentType_CollectionType.h"
#include "Mp7JrsMultipleViewType.h"
#include "Mp7JrsVideoSegmentType_Mosaic_CollectionType.h"
#include "Mp7JrsVideoSegmentType_CollectionType0.h"
#include "Mp7JrsMediaInformationType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsCreationInformationType.h"
#include "Mp7JrsUsageInformationType.h"
#include "Mp7JrsSegmentType_TextAnnotation_CollectionType.h"
#include "Mp7JrsSegmentType_CollectionType.h"
#include "Mp7JrsSegmentType_MatchingHint_CollectionType.h"
#include "Mp7JrsSegmentType_PointOfView_CollectionType.h"
#include "Mp7JrsSegmentType_Relation_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsGlobalTransitionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsSegmentType_TextAnnotation_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TextAnnotation
#include "Mp7JrsSegmentType_LocalType.h" // Choice collection Semantic
#include "Mp7JrsSemanticType.h" // Choice collection element Semantic
#include "Mp7JrsMatchingHintType.h" // Element collection urn:mpeg:mpeg7:schema:2004:MatchingHint
#include "Mp7JrsPointOfViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:PointOfView
#include "Mp7JrsRelationType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relation
#include "Mp7JrsVideoSegmentType_LocalType.h" // Choice collection VisualDescriptor
#include "Mp7JrsVisualDType.h" // Choice collection element VisualDescriptor
#include "Mp7JrsVisualDSType.h" // Choice collection element VisualDescriptionScheme
#include "Mp7JrsVisualTimeSeriesType.h" // Choice collection element VisualTimeSeriesDescriptor
#include "Mp7JrsGofGopFeatureType.h" // Choice collection element GofGopFeature
#include "Mp7JrsMosaicType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Mosaic
#include "Mp7JrsVideoSegmentType_LocalType0.h" // Choice collection SpatialDecomposition
#include "Mp7JrsVideoSegmentSpatialDecompositionType.h" // Choice collection element SpatialDecomposition
#include "Mp7JrsVideoSegmentTemporalDecompositionType.h" // Choice collection element TemporalDecomposition
#include "Mp7JrsVideoSegmentSpatioTemporalDecompositionType.h" // Choice collection element SpatioTemporalDecomposition
#include "Mp7JrsVideoSegmentMediaSourceDecompositionType.h" // Choice collection element MediaSourceDecomposition
#include "Mp7JrsAnalyticEditedVideoSegmentSpatioTemporalDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AnalyticEditionAreaDecomposition

#include <assert.h>
IMp7JrsGlobalTransitionType::IMp7JrsGlobalTransitionType()
{

// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtPropInit.h

}

IMp7JrsGlobalTransitionType::~IMp7JrsGlobalTransitionType()
{
// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtPropCleanup.h

}

Mp7JrsGlobalTransitionType::Mp7JrsGlobalTransitionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGlobalTransitionType::~Mp7JrsGlobalTransitionType()
{
	Cleanup();
}

void Mp7JrsGlobalTransitionType::Init()
{
	// Init base
	m_Base = CreateAnalyticTransitionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtMyPropInit.h

}

void Mp7JrsGlobalTransitionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsGlobalTransitionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GlobalTransitionTypePtr, since we
	// might need GetBase(), which isn't defined in IGlobalTransitionType
	const Dc1Ptr< Mp7JrsGlobalTransitionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAnalyticTransitionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsGlobalTransitionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrsGlobalTransitionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsGlobalTransitionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAnalyticTransitionType > Mp7JrsGlobalTransitionType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsGlobalTransitionType::GetevolutionReliability() const
{
	return GetBase()->GetevolutionReliability();
}

bool Mp7JrsGlobalTransitionType::ExistevolutionReliability() const
{
	return GetBase()->ExistevolutionReliability();
}
void Mp7JrsGlobalTransitionType::SetevolutionReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetevolutionReliability().");
	}
	GetBase()->SetevolutionReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateevolutionReliability()
{
	GetBase()->InvalidateevolutionReliability();
}
Mp7JrsTermUsePtr Mp7JrsGlobalTransitionType::GetEvolutionType() const
{
	return GetBase()->GetEvolutionType();
}

// Element is optional
bool Mp7JrsGlobalTransitionType::IsValidEvolutionType() const
{
	return GetBase()->IsValidEvolutionType();
}

Mp7JrsReferencePtr Mp7JrsGlobalTransitionType::GetEditedMovingRegionRef() const
{
	return GetBase()->GetEditedMovingRegionRef();
}

// Element is optional
bool Mp7JrsGlobalTransitionType::IsValidEditedMovingRegionRef() const
{
	return GetBase()->IsValidEditedMovingRegionRef();
}

void Mp7JrsGlobalTransitionType::SetEvolutionType(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetEvolutionType().");
	}
	GetBase()->SetEvolutionType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateEvolutionType()
{
	GetBase()->InvalidateEvolutionType();
}
void Mp7JrsGlobalTransitionType::SetEditedMovingRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetEditedMovingRegionRef().");
	}
	GetBase()->SetEditedMovingRegionRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateEditedMovingRegionRef()
{
	GetBase()->InvalidateEditedMovingRegionRef();
}
Mp7JrszeroToOnePtr Mp7JrsGlobalTransitionType::GetlocationReliability() const
{
	return GetBase()->GetBase()->GetlocationReliability();
}

bool Mp7JrsGlobalTransitionType::ExistlocationReliability() const
{
	return GetBase()->GetBase()->ExistlocationReliability();
}
void Mp7JrsGlobalTransitionType::SetlocationReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetlocationReliability().");
	}
	GetBase()->GetBase()->SetlocationReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidatelocationReliability()
{
	GetBase()->GetBase()->InvalidatelocationReliability();
}
Mp7JrszeroToOnePtr Mp7JrsGlobalTransitionType::GeteditingLevelReliability() const
{
	return GetBase()->GetBase()->GeteditingLevelReliability();
}

bool Mp7JrsGlobalTransitionType::ExisteditingLevelReliability() const
{
	return GetBase()->GetBase()->ExisteditingLevelReliability();
}
void Mp7JrsGlobalTransitionType::SeteditingLevelReliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SeteditingLevelReliability().");
	}
	GetBase()->GetBase()->SeteditingLevelReliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateeditingLevelReliability()
{
	GetBase()->GetBase()->InvalidateeditingLevelReliability();
}
Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration Mp7JrsGlobalTransitionType::Gettype() const
{
	return GetBase()->GetBase()->Gettype();
}

bool Mp7JrsGlobalTransitionType::Existtype() const
{
	return GetBase()->GetBase()->Existtype();
}
void Mp7JrsGlobalTransitionType::Settype(Mp7JrsAnalyticEditedVideoSegmentType_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::Settype().");
	}
	GetBase()->GetBase()->Settype(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::Invalidatetype()
{
	GetBase()->GetBase()->Invalidatetype();
}
Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr Mp7JrsGlobalTransitionType::GetAnalyticEditionAreaDecomposition() const
{
	return GetBase()->GetBase()->GetAnalyticEditionAreaDecomposition();
}

void Mp7JrsGlobalTransitionType::SetAnalyticEditionAreaDecomposition(const Mp7JrsAnalyticEditedVideoSegmentType_AnalyticEditionAreaDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetAnalyticEditionAreaDecomposition().");
	}
	GetBase()->GetBase()->SetAnalyticEditionAreaDecomposition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaTimePtr Mp7JrsGlobalTransitionType::GetMediaTime() const
{
	return GetBase()->GetBase()->GetBase()->GetMediaTime();
}

Mp7JrsTemporalMaskPtr Mp7JrsGlobalTransitionType::GetTemporalMask() const
{
	return GetBase()->GetBase()->GetBase()->GetTemporalMask();
}

Mp7JrsVideoSegmentType_CollectionPtr Mp7JrsGlobalTransitionType::GetVideoSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetBase()->GetVideoSegmentType_LocalType();
}

Mp7JrsMultipleViewPtr Mp7JrsGlobalTransitionType::GetMultipleView() const
{
	return GetBase()->GetBase()->GetBase()->GetMultipleView();
}

// Element is optional
bool Mp7JrsGlobalTransitionType::IsValidMultipleView() const
{
	return GetBase()->GetBase()->GetBase()->IsValidMultipleView();
}

Mp7JrsVideoSegmentType_Mosaic_CollectionPtr Mp7JrsGlobalTransitionType::GetMosaic() const
{
	return GetBase()->GetBase()->GetBase()->GetMosaic();
}

Mp7JrsVideoSegmentType_Collection0Ptr Mp7JrsGlobalTransitionType::GetVideoSegmentType_LocalType0() const
{
	return GetBase()->GetBase()->GetBase()->GetVideoSegmentType_LocalType0();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsGlobalTransitionType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMediaTime().");
	}
	GetBase()->GetBase()->GetBase()->SetMediaTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGlobalTransitionType::SetTemporalMask(const Mp7JrsTemporalMaskPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetTemporalMask().");
	}
	GetBase()->GetBase()->GetBase()->SetTemporalMask(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetVideoSegmentType_LocalType(const Mp7JrsVideoSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetVideoSegmentType_LocalType().");
	}
	GetBase()->GetBase()->GetBase()->SetVideoSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetMultipleView(const Mp7JrsMultipleViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMultipleView().");
	}
	GetBase()->GetBase()->GetBase()->SetMultipleView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateMultipleView()
{
	GetBase()->GetBase()->GetBase()->InvalidateMultipleView();
}
void Mp7JrsGlobalTransitionType::SetMosaic(const Mp7JrsVideoSegmentType_Mosaic_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMosaic().");
	}
	GetBase()->GetBase()->GetBase()->SetMosaic(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetVideoSegmentType_LocalType0(const Mp7JrsVideoSegmentType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetVideoSegmentType_LocalType0().");
	}
	GetBase()->GetBase()->GetBase()->SetVideoSegmentType_LocalType0(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsMediaInformationPtr Mp7JrsGlobalTransitionType::GetMediaInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaInformation();
}

Mp7JrsReferencePtr Mp7JrsGlobalTransitionType::GetMediaInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaInformationRef();
}

Mp7JrsMediaLocatorPtr Mp7JrsGlobalTransitionType::GetMediaLocator() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMediaLocator();
}

Mp7JrsControlledTermUsePtr Mp7JrsGlobalTransitionType::GetStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetStructuralUnit();
}

// Element is optional
bool Mp7JrsGlobalTransitionType::IsValidStructuralUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->IsValidStructuralUnit();
}

Mp7JrsCreationInformationPtr Mp7JrsGlobalTransitionType::GetCreationInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetCreationInformation();
}

Mp7JrsReferencePtr Mp7JrsGlobalTransitionType::GetCreationInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetCreationInformationRef();
}

Mp7JrsUsageInformationPtr Mp7JrsGlobalTransitionType::GetUsageInformation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetUsageInformation();
}

Mp7JrsReferencePtr Mp7JrsGlobalTransitionType::GetUsageInformationRef() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetUsageInformationRef();
}

Mp7JrsSegmentType_TextAnnotation_CollectionPtr Mp7JrsGlobalTransitionType::GetTextAnnotation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetTextAnnotation();
}

Mp7JrsSegmentType_CollectionPtr Mp7JrsGlobalTransitionType::GetSegmentType_LocalType() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetSegmentType_LocalType();
}

Mp7JrsSegmentType_MatchingHint_CollectionPtr Mp7JrsGlobalTransitionType::GetMatchingHint() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetMatchingHint();
}

Mp7JrsSegmentType_PointOfView_CollectionPtr Mp7JrsGlobalTransitionType::GetPointOfView() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetPointOfView();
}

Mp7JrsSegmentType_Relation_CollectionPtr Mp7JrsGlobalTransitionType::GetRelation() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetRelation();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsGlobalTransitionType::SetMediaInformation(const Mp7JrsMediaInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMediaInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGlobalTransitionType::SetMediaInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMediaInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGlobalTransitionType::SetMediaLocator(const Mp7JrsMediaLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMediaLocator().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMediaLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetStructuralUnit(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetStructuralUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetStructuralUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidateStructuralUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->InvalidateStructuralUnit();
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsGlobalTransitionType::SetCreationInformation(const Mp7JrsCreationInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetCreationInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetCreationInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGlobalTransitionType::SetCreationInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetCreationInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetCreationInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsGlobalTransitionType::SetUsageInformation(const Mp7JrsUsageInformationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetUsageInformation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetUsageInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGlobalTransitionType::SetUsageInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetUsageInformationRef().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetUsageInformationRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetTextAnnotation(const Mp7JrsSegmentType_TextAnnotation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetTextAnnotation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetTextAnnotation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetSegmentType_LocalType(const Mp7JrsSegmentType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetSegmentType_LocalType().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetSegmentType_LocalType(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetMatchingHint(const Mp7JrsSegmentType_MatchingHint_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetMatchingHint().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetMatchingHint(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetPointOfView(const Mp7JrsSegmentType_PointOfView_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetPointOfView().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetPointOfView(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::SetRelation(const Mp7JrsSegmentType_Relation_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetRelation().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->SetRelation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsGlobalTransitionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsGlobalTransitionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsGlobalTransitionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsGlobalTransitionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsGlobalTransitionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsGlobalTransitionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsGlobalTransitionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsGlobalTransitionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsGlobalTransitionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsGlobalTransitionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsGlobalTransitionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsGlobalTransitionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsGlobalTransitionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsGlobalTransitionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsGlobalTransitionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGlobalTransitionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsGlobalTransitionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsGlobalTransitionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGlobalTransitionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGlobalTransitionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GlobalTransitionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GlobalTransitionType");
	}
	return result;
}

XMLCh * Mp7JrsGlobalTransitionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsGlobalTransitionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsGlobalTransitionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GlobalTransitionType"));
	// Element serialization:

}

bool Mp7JrsGlobalTransitionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsAnalyticTransitionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGlobalTransitionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGlobalTransitionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEvolutionType() != Dc1NodePtr())
		result.Insert(GetEvolutionType());
	if (GetEditedMovingRegionRef() != Dc1NodePtr())
		result.Insert(GetEditedMovingRegionRef());
	if (GetAnalyticEditionAreaDecomposition() != Dc1NodePtr())
		result.Insert(GetAnalyticEditionAreaDecomposition());
	if (GetVideoSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType());
	if (GetMultipleView() != Dc1NodePtr())
		result.Insert(GetMultipleView());
	if (GetMosaic() != Dc1NodePtr())
		result.Insert(GetMosaic());
	if (GetVideoSegmentType_LocalType0() != Dc1NodePtr())
		result.Insert(GetVideoSegmentType_LocalType0());
	if (GetStructuralUnit() != Dc1NodePtr())
		result.Insert(GetStructuralUnit());
	if (GetTextAnnotation() != Dc1NodePtr())
		result.Insert(GetTextAnnotation());
	if (GetSegmentType_LocalType() != Dc1NodePtr())
		result.Insert(GetSegmentType_LocalType());
	if (GetMatchingHint() != Dc1NodePtr())
		result.Insert(GetMatchingHint());
	if (GetPointOfView() != Dc1NodePtr())
		result.Insert(GetPointOfView());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetTemporalMask() != Dc1NodePtr())
		result.Insert(GetTemporalMask());
	if (GetMediaInformation() != Dc1NodePtr())
		result.Insert(GetMediaInformation());
	if (GetMediaInformationRef() != Dc1NodePtr())
		result.Insert(GetMediaInformationRef());
	if (GetMediaLocator() != Dc1NodePtr())
		result.Insert(GetMediaLocator());
	if (GetCreationInformation() != Dc1NodePtr())
		result.Insert(GetCreationInformation());
	if (GetCreationInformationRef() != Dc1NodePtr())
		result.Insert(GetCreationInformationRef());
	if (GetUsageInformation() != Dc1NodePtr())
		result.Insert(GetUsageInformation());
	if (GetUsageInformationRef() != Dc1NodePtr())
		result.Insert(GetUsageInformationRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGlobalTransitionType_ExtMethodImpl.h


