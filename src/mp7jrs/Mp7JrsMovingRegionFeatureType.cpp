
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtImplInclude.h


#include "Mp7JrsVideoSegmentFeatureType.h"
#include "Mp7JrsShapeVariationType.h"
#include "Mp7JrsParametricMotionType.h"
#include "Mp7JrsMotionTrajectoryType.h"
#include "Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionType.h"
#include "Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionType.h"
#include "Mp7JrsMotionActivityType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMovingRegionFeatureType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsVisualTimeSeriesType.h" // Element collection urn:mpeg:mpeg7:schema:2004:TemporalTransition
#include "Mp7JrsGofGopFeatureType.h" // Element collection urn:mpeg:mpeg7:schema:2004:RepresentativeFeature

#include <assert.h>
IMp7JrsMovingRegionFeatureType::IMp7JrsMovingRegionFeatureType()
{

// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtPropInit.h

}

IMp7JrsMovingRegionFeatureType::~IMp7JrsMovingRegionFeatureType()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtPropCleanup.h

}

Mp7JrsMovingRegionFeatureType::Mp7JrsMovingRegionFeatureType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMovingRegionFeatureType::~Mp7JrsMovingRegionFeatureType()
{
	Cleanup();
}

void Mp7JrsMovingRegionFeatureType::Init()
{
	// Init base
	m_Base = CreateVideoSegmentFeatureType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ShapeVariation = Mp7JrsShapeVariationPtr(); // Class
	m_ShapeVariation_Exist = false;
	m_ParametricMotion = Mp7JrsParametricMotionPtr(); // Class
	m_MotionTrajectory = Mp7JrsMotionTrajectoryPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtMyPropInit.h

}

void Mp7JrsMovingRegionFeatureType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ShapeVariation);
	// Dc1Factory::DeleteObject(m_ParametricMotion);
	// Dc1Factory::DeleteObject(m_MotionTrajectory);
}

void Mp7JrsMovingRegionFeatureType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MovingRegionFeatureTypePtr, since we
	// might need GetBase(), which isn't defined in IMovingRegionFeatureType
	const Dc1Ptr< Mp7JrsMovingRegionFeatureType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVideoSegmentFeaturePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMovingRegionFeatureType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidShapeVariation())
	{
		// Dc1Factory::DeleteObject(m_ShapeVariation);
		this->SetShapeVariation(Dc1Factory::CloneObject(tmp->GetShapeVariation()));
	}
	else
	{
		InvalidateShapeVariation();
	}
		// Dc1Factory::DeleteObject(m_ParametricMotion);
		this->SetParametricMotion(Dc1Factory::CloneObject(tmp->GetParametricMotion()));
		// Dc1Factory::DeleteObject(m_MotionTrajectory);
		this->SetMotionTrajectory(Dc1Factory::CloneObject(tmp->GetMotionTrajectory()));
}

Dc1NodePtr Mp7JrsMovingRegionFeatureType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMovingRegionFeatureType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVideoSegmentFeatureType > Mp7JrsMovingRegionFeatureType::GetBase() const
{
	return m_Base;
}

Mp7JrsShapeVariationPtr Mp7JrsMovingRegionFeatureType::GetShapeVariation() const
{
		return m_ShapeVariation;
}

// Element is optional
bool Mp7JrsMovingRegionFeatureType::IsValidShapeVariation() const
{
	return m_ShapeVariation_Exist;
}

Mp7JrsParametricMotionPtr Mp7JrsMovingRegionFeatureType::GetParametricMotion() const
{
		return m_ParametricMotion;
}

Mp7JrsMotionTrajectoryPtr Mp7JrsMovingRegionFeatureType::GetMotionTrajectory() const
{
		return m_MotionTrajectory;
}

void Mp7JrsMovingRegionFeatureType::SetShapeVariation(const Mp7JrsShapeVariationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetShapeVariation().");
	}
	if (m_ShapeVariation != item || m_ShapeVariation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ShapeVariation);
		m_ShapeVariation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ShapeVariation) m_ShapeVariation->SetParent(m_myself.getPointer());
		if (m_ShapeVariation && m_ShapeVariation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ShapeVariation->UseTypeAttribute = true;
		}
		m_ShapeVariation_Exist = true;
		if(m_ShapeVariation != Mp7JrsShapeVariationPtr())
		{
			m_ShapeVariation->SetContentName(XMLString::transcode("ShapeVariation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidateShapeVariation()
{
	m_ShapeVariation_Exist = false;
}
	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMovingRegionFeatureType::SetParametricMotion(const Mp7JrsParametricMotionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetParametricMotion().");
	}
	if (m_ParametricMotion != item)
	{
		// Dc1Factory::DeleteObject(m_ParametricMotion);
		m_ParametricMotion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ParametricMotion) m_ParametricMotion->SetParent(m_myself.getPointer());
		if (m_ParametricMotion && m_ParametricMotion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ParametricMotion->UseTypeAttribute = true;
		}
		if(m_ParametricMotion != Mp7JrsParametricMotionPtr())
		{
			m_ParametricMotion->SetContentName(XMLString::transcode("ParametricMotion"));
		}
	// Dc1Factory::DeleteObject(m_MotionTrajectory);
	m_MotionTrajectory = Mp7JrsMotionTrajectoryPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMovingRegionFeatureType::SetMotionTrajectory(const Mp7JrsMotionTrajectoryPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetMotionTrajectory().");
	}
	if (m_MotionTrajectory != item)
	{
		// Dc1Factory::DeleteObject(m_MotionTrajectory);
		m_MotionTrajectory = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionTrajectory) m_MotionTrajectory->SetParent(m_myself.getPointer());
		if (m_MotionTrajectory && m_MotionTrajectory->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MotionTrajectory->UseTypeAttribute = true;
		}
		if(m_MotionTrajectory != Mp7JrsMotionTrajectoryPtr())
		{
			m_MotionTrajectory->SetContentName(XMLString::transcode("MotionTrajectory"));
		}
	// Dc1Factory::DeleteObject(m_ParametricMotion);
	m_ParametricMotion = Mp7JrsParametricMotionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr Mp7JrsMovingRegionFeatureType::GetTemporalTransition() const
{
	return GetBase()->GetTemporalTransition();
}

Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr Mp7JrsMovingRegionFeatureType::GetRepresentativeFeature() const
{
	return GetBase()->GetRepresentativeFeature();
}

Mp7JrsMotionActivityPtr Mp7JrsMovingRegionFeatureType::GetMotionActivity() const
{
	return GetBase()->GetMotionActivity();
}

// Element is optional
bool Mp7JrsMovingRegionFeatureType::IsValidMotionActivity() const
{
	return GetBase()->IsValidMotionActivity();
}

void Mp7JrsMovingRegionFeatureType::SetTemporalTransition(const Mp7JrsVideoSegmentFeatureType_TemporalTransition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetTemporalTransition().");
	}
	GetBase()->SetTemporalTransition(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::SetRepresentativeFeature(const Mp7JrsVideoSegmentFeatureType_RepresentativeFeature_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetRepresentativeFeature().");
	}
	GetBase()->SetRepresentativeFeature(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::SetMotionActivity(const Mp7JrsMotionActivityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetMotionActivity().");
	}
	GetBase()->SetMotionActivity(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidateMotionActivity()
{
	GetBase()->InvalidateMotionActivity();
}
XMLCh * Mp7JrsMovingRegionFeatureType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsMovingRegionFeatureType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsMovingRegionFeatureType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMovingRegionFeatureType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMovingRegionFeatureType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMovingRegionFeatureType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMovingRegionFeatureType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMovingRegionFeatureType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMovingRegionFeatureType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMovingRegionFeatureType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMovingRegionFeatureType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMovingRegionFeatureType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMovingRegionFeatureType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMovingRegionFeatureType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMovingRegionFeatureType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMovingRegionFeatureType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMovingRegionFeatureType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMovingRegionFeatureType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMovingRegionFeatureType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMovingRegionFeatureType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ShapeVariation")) == 0)
	{
		// ShapeVariation is simple element ShapeVariationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShapeVariation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ShapeVariationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsShapeVariationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShapeVariation(p, client);
					if((p = GetShapeVariation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ParametricMotion")) == 0)
	{
		// ParametricMotion is simple element ParametricMotionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetParametricMotion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ParametricMotionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsParametricMotionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetParametricMotion(p, client);
					if((p = GetParametricMotion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MotionTrajectory")) == 0)
	{
		// MotionTrajectory is simple element MotionTrajectoryType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMotionTrajectory()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MotionTrajectoryType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMotionTrajectoryPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMotionTrajectory(p, client);
					if((p = GetMotionTrajectory()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MovingRegionFeatureType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MovingRegionFeatureType");
	}
	return result;
}

XMLCh * Mp7JrsMovingRegionFeatureType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMovingRegionFeatureType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMovingRegionFeatureType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MovingRegionFeatureType"));
	// Element serialization:
	// Class
	
	if (m_ShapeVariation != Mp7JrsShapeVariationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ShapeVariation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ShapeVariation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ShapeVariation->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ParametricMotion != Mp7JrsParametricMotionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ParametricMotion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ParametricMotion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ParametricMotion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MotionTrajectory != Mp7JrsMotionTrajectoryPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MotionTrajectory->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MotionTrajectory"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MotionTrajectory->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMovingRegionFeatureType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVideoSegmentFeatureType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ShapeVariation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ShapeVariation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateShapeVariationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShapeVariation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ParametricMotion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ParametricMotion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateParametricMotionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetParametricMotion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MotionTrajectory"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MotionTrajectory")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMotionTrajectoryType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMotionTrajectory(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMovingRegionFeatureType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ShapeVariation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateShapeVariationType; // FTT, check this
	}
	this->SetShapeVariation(child);
  }
  if (XMLString::compareString(elementname, X("ParametricMotion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateParametricMotionType; // FTT, check this
	}
	this->SetParametricMotion(child);
  }
  if (XMLString::compareString(elementname, X("MotionTrajectory")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMotionTrajectoryType; // FTT, check this
	}
	this->SetMotionTrajectory(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMovingRegionFeatureType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetShapeVariation() != Dc1NodePtr())
		result.Insert(GetShapeVariation());
	if (GetTemporalTransition() != Dc1NodePtr())
		result.Insert(GetTemporalTransition());
	if (GetRepresentativeFeature() != Dc1NodePtr())
		result.Insert(GetRepresentativeFeature());
	if (GetMotionActivity() != Dc1NodePtr())
		result.Insert(GetMotionActivity());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetParametricMotion() != Dc1NodePtr())
		result.Insert(GetParametricMotion());
	if (GetMotionTrajectory() != Dc1NodePtr())
		result.Insert(GetMotionTrajectory());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMovingRegionFeatureType_ExtMethodImpl.h


