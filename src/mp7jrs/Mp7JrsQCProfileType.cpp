
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsQCProfileType_QCItem_CollectionType.h"
#include "Mp7JrsQCProfileType_RelevanceLevel_LocalType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsQCProfileType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsQCProfileType_QCItem_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QCItem

#include <assert.h>
IMp7JrsQCProfileType::IMp7JrsQCProfileType()
{

// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtPropInit.h

}

IMp7JrsQCProfileType::~IMp7JrsQCProfileType()
{
// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtPropCleanup.h

}

Mp7JrsQCProfileType::Mp7JrsQCProfileType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsQCProfileType::~Mp7JrsQCProfileType()
{
	Cleanup();
}

void Mp7JrsQCProfileType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsControlledTermUsePtr(); // Class
	m_Description = Mp7JrsTextualPtr(); // Class
	m_Description_Exist = false;
	m_QCItem = Mp7JrsQCProfileType_QCItem_CollectionPtr(); // Collection
	m_CheckResultRule = Mp7JrsQCProfileType_CheckResultRule_LocalType::UninitializedEnumeration; // Enumeration
	m_CheckResultRule_Exist = false;
	m_RelevanceLevel = Mp7JrsQCProfileType_RelevanceLevel_LocalPtr(); // Class with content 
	m_RelevanceLevel_Exist = false;


// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtMyPropInit.h

}

void Mp7JrsQCProfileType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Description);
	// Dc1Factory::DeleteObject(m_QCItem);
	// Dc1Factory::DeleteObject(m_RelevanceLevel);
}

void Mp7JrsQCProfileType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use QCProfileTypePtr, since we
	// might need GetBase(), which isn't defined in IQCProfileType
	const Dc1Ptr< Mp7JrsQCProfileType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsQCProfileType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
	if (tmp->IsValidDescription())
	{
		// Dc1Factory::DeleteObject(m_Description);
		this->SetDescription(Dc1Factory::CloneObject(tmp->GetDescription()));
	}
	else
	{
		InvalidateDescription();
	}
		// Dc1Factory::DeleteObject(m_QCItem);
		this->SetQCItem(Dc1Factory::CloneObject(tmp->GetQCItem()));
	if (tmp->IsValidCheckResultRule())
	{
		this->SetCheckResultRule(tmp->GetCheckResultRule());
	}
	else
	{
		InvalidateCheckResultRule();
	}
	if (tmp->IsValidRelevanceLevel())
	{
		// Dc1Factory::DeleteObject(m_RelevanceLevel);
		this->SetRelevanceLevel(Dc1Factory::CloneObject(tmp->GetRelevanceLevel()));
	}
	else
	{
		InvalidateRelevanceLevel();
	}
}

Dc1NodePtr Mp7JrsQCProfileType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsQCProfileType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsQCProfileType::GetBase() const
{
	return m_Base;
}

Mp7JrsControlledTermUsePtr Mp7JrsQCProfileType::GetName() const
{
		return m_Name;
}

Mp7JrsTextualPtr Mp7JrsQCProfileType::GetDescription() const
{
		return m_Description;
}

// Element is optional
bool Mp7JrsQCProfileType::IsValidDescription() const
{
	return m_Description_Exist;
}

Mp7JrsQCProfileType_QCItem_CollectionPtr Mp7JrsQCProfileType::GetQCItem() const
{
		return m_QCItem;
}

Mp7JrsQCProfileType_CheckResultRule_LocalType::Enumeration Mp7JrsQCProfileType::GetCheckResultRule() const
{
		return m_CheckResultRule;
}

// Element is optional
bool Mp7JrsQCProfileType::IsValidCheckResultRule() const
{
	return m_CheckResultRule_Exist;
}

Mp7JrsQCProfileType_RelevanceLevel_LocalPtr Mp7JrsQCProfileType::GetRelevanceLevel() const
{
		return m_RelevanceLevel;
}

// Element is optional
bool Mp7JrsQCProfileType::IsValidRelevanceLevel() const
{
	return m_RelevanceLevel_Exist;
}

void Mp7JrsQCProfileType::SetName(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if (m_Name && m_Name->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Name->UseTypeAttribute = true;
		}
		if(m_Name != Mp7JrsControlledTermUsePtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::SetDescription(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetDescription().");
	}
	if (m_Description != item || m_Description_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Description);
		m_Description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Description) m_Description->SetParent(m_myself.getPointer());
		if (m_Description && m_Description->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Description->UseTypeAttribute = true;
		}
		m_Description_Exist = true;
		if(m_Description != Mp7JrsTextualPtr())
		{
			m_Description->SetContentName(XMLString::transcode("Description"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidateDescription()
{
	m_Description_Exist = false;
}
void Mp7JrsQCProfileType::SetQCItem(const Mp7JrsQCProfileType_QCItem_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetQCItem().");
	}
	if (m_QCItem != item)
	{
		// Dc1Factory::DeleteObject(m_QCItem);
		m_QCItem = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_QCItem) m_QCItem->SetParent(m_myself.getPointer());
		if(m_QCItem != Mp7JrsQCProfileType_QCItem_CollectionPtr())
		{
			m_QCItem->SetContentName(XMLString::transcode("QCItem"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::SetCheckResultRule(Mp7JrsQCProfileType_CheckResultRule_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetCheckResultRule().");
	}
	if (m_CheckResultRule != item || m_CheckResultRule_Exist == false)
	{
		// nothing to free here, hopefully
		m_CheckResultRule = item;
		m_CheckResultRule_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidateCheckResultRule()
{
	m_CheckResultRule_Exist = false;
}
void Mp7JrsQCProfileType::SetRelevanceLevel(const Mp7JrsQCProfileType_RelevanceLevel_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetRelevanceLevel().");
	}
	if (m_RelevanceLevel != item || m_RelevanceLevel_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_RelevanceLevel);
		m_RelevanceLevel = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RelevanceLevel) m_RelevanceLevel->SetParent(m_myself.getPointer());
		if (m_RelevanceLevel && m_RelevanceLevel->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_RelevanceLevel_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RelevanceLevel->UseTypeAttribute = true;
		}
		m_RelevanceLevel_Exist = true;
		if(m_RelevanceLevel != Mp7JrsQCProfileType_RelevanceLevel_LocalPtr())
		{
			m_RelevanceLevel->SetContentName(XMLString::transcode("RelevanceLevel"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidateRelevanceLevel()
{
	m_RelevanceLevel_Exist = false;
}
XMLCh * Mp7JrsQCProfileType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsQCProfileType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsQCProfileType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsQCProfileType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsQCProfileType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsQCProfileType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsQCProfileType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsQCProfileType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsQCProfileType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsQCProfileType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsQCProfileType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsQCProfileType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsQCProfileType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsQCProfileType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsQCProfileType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsQCProfileType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsQCProfileType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsQCProfileType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsQCProfileType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsQCProfileType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// Name is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetName()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetName(p, client);
					if((p = GetName()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Description")) == 0)
	{
		// Description is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDescription()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDescription(p, client);
					if((p = GetDescription()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("QCItem")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:QCItem is item of type QCProfileType_QCItem_LocalType
		// in element collection QCProfileType_QCItem_CollectionType
		
		context->Found = true;
		Mp7JrsQCProfileType_QCItem_CollectionPtr coll = GetQCItem();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateQCProfileType_QCItem_CollectionType; // FTT, check this
				SetQCItem(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_QCItem_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCProfileType_QCItem_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RelevanceLevel")) == 0)
	{
		// RelevanceLevel is simple element QCProfileType_RelevanceLevel_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRelevanceLevel()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:QCProfileType_RelevanceLevel_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsQCProfileType_RelevanceLevel_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRelevanceLevel(p, client);
					if((p = GetRelevanceLevel()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for QCProfileType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "QCProfileType");
	}
	return result;
}

XMLCh * Mp7JrsQCProfileType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsQCProfileType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsQCProfileType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("QCProfileType"));
	// Element serialization:
	// Class
	
	if (m_Name != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Name->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Name"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Name->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Description != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Description->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Description"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Description->Serialize(doc, element, &elem);
	}
	if (m_QCItem != Mp7JrsQCProfileType_QCItem_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_QCItem->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if(m_CheckResultRule != Mp7JrsQCProfileType_CheckResultRule_LocalType::UninitializedEnumeration) // Enumeration
	{
		XMLCh * tmp = Mp7JrsQCProfileType_CheckResultRule_LocalType::ToText(m_CheckResultRule);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("CheckResultRule"), true);
		XMLString::release(&tmp);
	}
	// Class with content		
	
	if (m_RelevanceLevel != Mp7JrsQCProfileType_RelevanceLevel_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RelevanceLevel->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RelevanceLevel"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RelevanceLevel->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsQCProfileType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetName(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Description"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Description")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDescription(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("QCItem"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("QCItem")) == 0))
		{
			// Deserialize factory type
			Mp7JrsQCProfileType_QCItem_CollectionPtr tmp = CreateQCProfileType_QCItem_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetQCItem(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize enumeration element
	if(Dc1Util::HasNodeName(parent, X("CheckResultRule"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CheckResultRule")) == 0))
		{
			Mp7JrsQCProfileType_CheckResultRule_LocalType::Enumeration tmp = Mp7JrsQCProfileType_CheckResultRule_LocalType::Parse(Dc1Util::GetElementText(parent));
			if(tmp == Mp7JrsQCProfileType_CheckResultRule_LocalType::UninitializedEnumeration)
			{
					return false;
			}
			this->SetCheckResultRule(tmp);
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RelevanceLevel"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RelevanceLevel")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateQCProfileType_RelevanceLevel_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelevanceLevel(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsQCProfileType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Name")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetName(child);
  }
  if (XMLString::compareString(elementname, X("Description")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetDescription(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("QCItem")) == 0))
  {
	Mp7JrsQCProfileType_QCItem_CollectionPtr tmp = CreateQCProfileType_QCItem_CollectionType; // FTT, check this
	this->SetQCItem(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("RelevanceLevel")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateQCProfileType_RelevanceLevel_LocalType; // FTT, check this
	}
	this->SetRelevanceLevel(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsQCProfileType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetDescription() != Dc1NodePtr())
		result.Insert(GetDescription());
	if (GetQCItem() != Dc1NodePtr())
		result.Insert(GetQCItem());
	if (GetRelevanceLevel() != Dc1NodePtr())
		result.Insert(GetRelevanceLevel());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsQCProfileType_ExtMethodImpl.h


