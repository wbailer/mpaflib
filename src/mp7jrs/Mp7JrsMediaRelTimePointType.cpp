
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtImplInclude.h


#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsMediaRelTimePointType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaRelTimePointType::IMp7JrsMediaRelTimePointType()
{

// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtPropInit.h

}

IMp7JrsMediaRelTimePointType::~IMp7JrsMediaRelTimePointType()
{
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtPropCleanup.h

}

Mp7JrsMediaRelTimePointType::Mp7JrsMediaRelTimePointType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaRelTimePointType::~Mp7JrsMediaRelTimePointType()
{
	Cleanup();
}

void Mp7JrsMediaRelTimePointType::Init()
{
	// Init base
	m_Base = CreatemediaTimeOffsetType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_mediaTimeBase = Mp7JrsxPathRefPtr(); // Pattern
	m_mediaTimeBase_Exist = false;



// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtMyPropInit.h

}

void Mp7JrsMediaRelTimePointType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
}

void Mp7JrsMediaRelTimePointType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaRelTimePointTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaRelTimePointType
	const Dc1Ptr< Mp7JrsMediaRelTimePointType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsmediaTimeOffsetPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaRelTimePointType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_mediaTimeBase); // Pattern
	if (tmp->ExistmediaTimeBase())
	{
		this->SetmediaTimeBase(Dc1Factory::CloneObject(tmp->GetmediaTimeBase()));
	}
	else
	{
		InvalidatemediaTimeBase();
	}
	}
}

Dc1NodePtr Mp7JrsMediaRelTimePointType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsMediaRelTimePointType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsmediaTimeOffsetType > Mp7JrsMediaRelTimePointType::GetBase() const
{
	return m_Base;
}

Mp7JrsxPathRefPtr Mp7JrsMediaRelTimePointType::GetmediaTimeBase() const
{
	return m_mediaTimeBase;
}

bool Mp7JrsMediaRelTimePointType::ExistmediaTimeBase() const
{
	return m_mediaTimeBase_Exist;
}
void Mp7JrsMediaRelTimePointType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetmediaTimeBase().");
	}
	m_mediaTimeBase = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_mediaTimeBase) m_mediaTimeBase->SetParent(m_myself.getPointer());
	m_mediaTimeBase_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaRelTimePointType::InvalidatemediaTimeBase()
{
	m_mediaTimeBase_Exist = false;
}
int Mp7JrsMediaRelTimePointType::GetOffsetSign() const
{
	return GetBase()->GetOffsetSign();
}

void Mp7JrsMediaRelTimePointType::SetOffsetSign(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetOffsetSign().");
	}
	GetBase()->SetOffsetSign(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetDays() const
{
	return GetBase()->GetDays();
}

void Mp7JrsMediaRelTimePointType::SetDays(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetDays().");
	}
	GetBase()->SetDays(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetHours() const
{
	return GetBase()->GetHours();
}

void Mp7JrsMediaRelTimePointType::SetHours(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetHours().");
	}
	GetBase()->SetHours(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetMinutes() const
{
	return GetBase()->GetMinutes();
}

void Mp7JrsMediaRelTimePointType::SetMinutes(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetMinutes().");
	}
	GetBase()->SetMinutes(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetSeconds() const
{
	return GetBase()->GetSeconds();
}

void Mp7JrsMediaRelTimePointType::SetSeconds(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetSeconds().");
	}
	GetBase()->SetSeconds(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetNumberOfFractions() const
{
	return GetBase()->GetNumberOfFractions();
}

void Mp7JrsMediaRelTimePointType::SetNumberOfFractions(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetNumberOfFractions().");
	}
	GetBase()->SetNumberOfFractions(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsMediaRelTimePointType::GetTime_Exist() const
{
	return GetBase()->GetTime_Exist();
}

void Mp7JrsMediaRelTimePointType::SetTime_Exist(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetTime_Exist().");
	}
	GetBase()->SetTime_Exist(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsMediaRelTimePointType::GetFractionsPerSecond() const
{
	return GetBase()->GetFractionsPerSecond();
}

void Mp7JrsMediaRelTimePointType::SetFractionsPerSecond(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaRelTimePointType::SetFractionsPerSecond().");
	}
	GetBase()->SetFractionsPerSecond(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaRelTimePointType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("mediaTimeBase")) == 0)
	{
		// mediaTimeBase is simple attribute xPathRefType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsxPathRefPtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaRelTimePointType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaRelTimePointType");
	}
	return result;
}

XMLCh * Mp7JrsMediaRelTimePointType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaRelTimePointType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsMediaRelTimePointType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrsMediaRelTimePointType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrsMediaRelTimePointType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	m_Base->Serialize(doc, element, newElem);

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelTimePointType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_mediaTimeBase_Exist)
	{
	// Pattern
	if (m_mediaTimeBase != Mp7JrsxPathRefPtr())
	{
		XMLCh * tmp = m_mediaTimeBase->ToText();
		element->setAttributeNS(X(""), X("mediaTimeBase"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsMediaRelTimePointType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("mediaTimeBase")))
	{
		Mp7JrsxPathRefPtr tmp = CreatexPathRefType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("mediaTimeBase")));
		this->SetmediaTimeBase(tmp);
		* current = parent;
	}

  // Extensionbase is Mp7JrsmediaTimeOffsetType
  // Deserialize cce factory type
  if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
  }
// no includefile for extension defined 
// file Mp7JrsMediaRelTimePointType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaRelTimePointType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaRelTimePointType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsMediaRelTimePointType_ExtMethodImpl.h
#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsDSType.h"

long long IMp7JrsMediaRelTimePointType::GetTotalNrFractions() {
	long long sum = 0LL; // In V2.8. we switched to 64 bit long long.
	// days missing!!!!!
	sum = 60LL * (GetHours() + sum);
	sum = 60LL * (GetMinutes() + sum);
	sum = GetFractionsPerSecond() * (GetSeconds() + sum);
	sum += GetNumberOfFractions(); 
	return sum;
}

void IMp7JrsMediaRelTimePointType::SetTotalNrFractions(long long fractions, Dc1ClientID client) {
	long long remainder = fractions; // In V2.8. we switched to 64 bit long long.
	SetNumberOfFractions((int)(remainder % GetFractionsPerSecond()),client);
	remainder /= GetFractionsPerSecond();
	SetSeconds ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetMinutes ((int)(remainder % 60LL),client);
	remainder /= 60LL;
	SetHours ((int)remainder,client);
	// days ignored
}



Mp7JrsmediaTimePointPtr IMp7JrsMediaRelTimePointType::GetAbsMediaTimePoint() {
	Mp7JrsmediaTimePointPtr absTp = CreatemediaTimePointType;
	Mp7JrsmediaTimePointPtr relTp = _GetMediaTimeBase();
	absTp->SetFractionsPerSecond(GetFractionsPerSecond());
	long long nFractions = (long long)(GetTotalNrFractions()+(relTp->GetTotalNrFractions()*1.0/relTp->GetFractionsPerSecond())*GetFractionsPerSecond());
	absTp->SetTotalNrFractions(nFractions);
	return absTp;
}

void IMp7JrsMediaRelTimePointType::SetAbsMediaTimePoint(Mp7JrsmediaTimePointPtr tp, Dc1ClientID client) {
	Mp7JrsmediaTimePointPtr relTp = _GetMediaTimeBase();
	SetFractionsPerSecond(tp->GetFractionsPerSecond(),client);
	long long nFractions = (long long)(tp->GetTotalNrFractions()+(relTp->GetTotalNrFractions()*1.0/relTp->GetFractionsPerSecond())*tp->GetFractionsPerSecond());
	SetTotalNrFractions(nFractions,client);
}

Mp7JrsmediaTimePointPtr IMp7JrsMediaRelTimePointType::_GetMediaTimeBase() {
	Mp7JrsxPathRefPtr mtbPath;
	Dc1NodePtr parent;
	if (ExistmediaTimeBase()) mtbPath = GetmediaTimeBase();
	else {
		parent = GetParent();
		while (parent) {
			Mp7JrsDSPtr dstype = parent;
			if (dstype) {
				if (dstype->ExistmediaTimeBase()) {
					mtbPath = dstype->GetmediaTimeBase();
					break;
				}
			}
			parent = parent->GetParent();
		}
	}

	
	Mp7JrsmediaTimePointPtr nullTp;
	// if no path found return empty time point
	if (mtbPath==NULL) return nullTp;

	// try to resolve XPath
	Dc1NodePtr target;
	if (parent) target = parent->NodeFromXPath(mtbPath->GetContent());
	else target = NodeFromXPath(mtbPath->GetContent());

	// no target found
	if (target==NULL) return nullTp;

	// check if target is a MediaTimeType
	Mp7JrsMediaTimePtr mediaTime = target;
	if (mediaTime) {
		// try to get time point
		Mp7JrsmediaTimePointPtr mtp = mediaTime->GetMediaTimePoint();
		// if not, try to get absolute value of relative tp
		if (mtp!=NULL) return mtp;

		Mp7JrsMediaRelTimePointPtr mrtp = mediaTime->GetMediaRelTimePoint();
		if (mrtp==NULL) return nullTp;

		mtp = mrtp->GetAbsMediaTimePoint();
		return mtp;
	}
	// check if target is a MediaLocator
	Mp7JrsMediaLocatorPtr mediaLoc = target;
	if (mediaLoc) {
		// not well defined in schema, has to be done in own extension
		return nullTp;
	}

	return nullTp;
}
// end extension included


