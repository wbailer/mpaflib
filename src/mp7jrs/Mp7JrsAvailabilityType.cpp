
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsDisseminationType.h"
#include "Mp7JrsFinancialType.h"
#include "Mp7JrsRightsType.h"
#include "Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsAvailabilityType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsAvailabilityType_AvailabilityPeriod_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AvailabilityPeriod

#include <assert.h>
IMp7JrsAvailabilityType::IMp7JrsAvailabilityType()
{

// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtPropInit.h

}

IMp7JrsAvailabilityType::~IMp7JrsAvailabilityType()
{
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtPropCleanup.h

}

Mp7JrsAvailabilityType::Mp7JrsAvailabilityType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAvailabilityType::~Mp7JrsAvailabilityType()
{
	Cleanup();
}

void Mp7JrsAvailabilityType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_InstanceRef = Mp7JrsReferencePtr(); // Class
	m_Dissemination = Mp7JrsDisseminationPtr(); // Class
	m_Dissemination_Exist = false;
	m_Financial = Mp7JrsFinancialPtr(); // Class
	m_Financial_Exist = false;
	m_Rights = Mp7JrsRightsPtr(); // Class
	m_Rights_Exist = false;
	m_AvailabilityPeriod = Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtMyPropInit.h

}

void Mp7JrsAvailabilityType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_InstanceRef);
	// Dc1Factory::DeleteObject(m_Dissemination);
	// Dc1Factory::DeleteObject(m_Financial);
	// Dc1Factory::DeleteObject(m_Rights);
	// Dc1Factory::DeleteObject(m_AvailabilityPeriod);
}

void Mp7JrsAvailabilityType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AvailabilityTypePtr, since we
	// might need GetBase(), which isn't defined in IAvailabilityType
	const Dc1Ptr< Mp7JrsAvailabilityType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAvailabilityType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_InstanceRef);
		this->SetInstanceRef(Dc1Factory::CloneObject(tmp->GetInstanceRef()));
	if (tmp->IsValidDissemination())
	{
		// Dc1Factory::DeleteObject(m_Dissemination);
		this->SetDissemination(Dc1Factory::CloneObject(tmp->GetDissemination()));
	}
	else
	{
		InvalidateDissemination();
	}
	if (tmp->IsValidFinancial())
	{
		// Dc1Factory::DeleteObject(m_Financial);
		this->SetFinancial(Dc1Factory::CloneObject(tmp->GetFinancial()));
	}
	else
	{
		InvalidateFinancial();
	}
	if (tmp->IsValidRights())
	{
		// Dc1Factory::DeleteObject(m_Rights);
		this->SetRights(Dc1Factory::CloneObject(tmp->GetRights()));
	}
	else
	{
		InvalidateRights();
	}
		// Dc1Factory::DeleteObject(m_AvailabilityPeriod);
		this->SetAvailabilityPeriod(Dc1Factory::CloneObject(tmp->GetAvailabilityPeriod()));
}

Dc1NodePtr Mp7JrsAvailabilityType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAvailabilityType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsAvailabilityType::GetBase() const
{
	return m_Base;
}

Mp7JrsReferencePtr Mp7JrsAvailabilityType::GetInstanceRef() const
{
		return m_InstanceRef;
}

Mp7JrsDisseminationPtr Mp7JrsAvailabilityType::GetDissemination() const
{
		return m_Dissemination;
}

// Element is optional
bool Mp7JrsAvailabilityType::IsValidDissemination() const
{
	return m_Dissemination_Exist;
}

Mp7JrsFinancialPtr Mp7JrsAvailabilityType::GetFinancial() const
{
		return m_Financial;
}

// Element is optional
bool Mp7JrsAvailabilityType::IsValidFinancial() const
{
	return m_Financial_Exist;
}

Mp7JrsRightsPtr Mp7JrsAvailabilityType::GetRights() const
{
		return m_Rights;
}

// Element is optional
bool Mp7JrsAvailabilityType::IsValidRights() const
{
	return m_Rights_Exist;
}

Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr Mp7JrsAvailabilityType::GetAvailabilityPeriod() const
{
		return m_AvailabilityPeriod;
}

void Mp7JrsAvailabilityType::SetInstanceRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetInstanceRef().");
	}
	if (m_InstanceRef != item)
	{
		// Dc1Factory::DeleteObject(m_InstanceRef);
		m_InstanceRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InstanceRef) m_InstanceRef->SetParent(m_myself.getPointer());
		if (m_InstanceRef && m_InstanceRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InstanceRef->UseTypeAttribute = true;
		}
		if(m_InstanceRef != Mp7JrsReferencePtr())
		{
			m_InstanceRef->SetContentName(XMLString::transcode("InstanceRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::SetDissemination(const Mp7JrsDisseminationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetDissemination().");
	}
	if (m_Dissemination != item || m_Dissemination_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Dissemination);
		m_Dissemination = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Dissemination) m_Dissemination->SetParent(m_myself.getPointer());
		if (m_Dissemination && m_Dissemination->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DisseminationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Dissemination->UseTypeAttribute = true;
		}
		m_Dissemination_Exist = true;
		if(m_Dissemination != Mp7JrsDisseminationPtr())
		{
			m_Dissemination->SetContentName(XMLString::transcode("Dissemination"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidateDissemination()
{
	m_Dissemination_Exist = false;
}
void Mp7JrsAvailabilityType::SetFinancial(const Mp7JrsFinancialPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetFinancial().");
	}
	if (m_Financial != item || m_Financial_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Financial);
		m_Financial = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Financial) m_Financial->SetParent(m_myself.getPointer());
		if (m_Financial && m_Financial->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Financial->UseTypeAttribute = true;
		}
		m_Financial_Exist = true;
		if(m_Financial != Mp7JrsFinancialPtr())
		{
			m_Financial->SetContentName(XMLString::transcode("Financial"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidateFinancial()
{
	m_Financial_Exist = false;
}
void Mp7JrsAvailabilityType::SetRights(const Mp7JrsRightsPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetRights().");
	}
	if (m_Rights != item || m_Rights_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Rights);
		m_Rights = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Rights) m_Rights->SetParent(m_myself.getPointer());
		if (m_Rights && m_Rights->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Rights->UseTypeAttribute = true;
		}
		m_Rights_Exist = true;
		if(m_Rights != Mp7JrsRightsPtr())
		{
			m_Rights->SetContentName(XMLString::transcode("Rights"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidateRights()
{
	m_Rights_Exist = false;
}
void Mp7JrsAvailabilityType::SetAvailabilityPeriod(const Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetAvailabilityPeriod().");
	}
	if (m_AvailabilityPeriod != item)
	{
		// Dc1Factory::DeleteObject(m_AvailabilityPeriod);
		m_AvailabilityPeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AvailabilityPeriod) m_AvailabilityPeriod->SetParent(m_myself.getPointer());
		if(m_AvailabilityPeriod != Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr())
		{
			m_AvailabilityPeriod->SetContentName(XMLString::transcode("AvailabilityPeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsAvailabilityType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsAvailabilityType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsAvailabilityType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsAvailabilityType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsAvailabilityType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsAvailabilityType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsAvailabilityType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsAvailabilityType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsAvailabilityType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsAvailabilityType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsAvailabilityType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsAvailabilityType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsAvailabilityType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsAvailabilityType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsAvailabilityType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAvailabilityType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsAvailabilityType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsAvailabilityType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAvailabilityType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAvailabilityType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("InstanceRef")) == 0)
	{
		// InstanceRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInstanceRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInstanceRef(p, client);
					if((p = GetInstanceRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Dissemination")) == 0)
	{
		// Dissemination is simple element DisseminationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDissemination()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DisseminationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDisseminationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDissemination(p, client);
					if((p = GetDissemination()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Financial")) == 0)
	{
		// Financial is simple element FinancialType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFinancial()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFinancialPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFinancial(p, client);
					if((p = GetFinancial()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Rights")) == 0)
	{
		// Rights is simple element RightsType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRights()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RightsType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRightsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRights(p, client);
					if((p = GetRights()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AvailabilityPeriod")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AvailabilityPeriod is item of type AvailabilityType_AvailabilityPeriod_LocalType
		// in element collection AvailabilityType_AvailabilityPeriod_CollectionType
		
		context->Found = true;
		Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr coll = GetAvailabilityPeriod();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAvailabilityType_AvailabilityPeriod_CollectionType; // FTT, check this
				SetAvailabilityPeriod(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AvailabilityType_AvailabilityPeriod_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAvailabilityType_AvailabilityPeriod_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AvailabilityType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AvailabilityType");
	}
	return result;
}

XMLCh * Mp7JrsAvailabilityType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAvailabilityType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAvailabilityType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AvailabilityType"));
	// Element serialization:
	// Class
	
	if (m_InstanceRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InstanceRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InstanceRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InstanceRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Dissemination != Mp7JrsDisseminationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Dissemination->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Dissemination"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Dissemination->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Financial != Mp7JrsFinancialPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Financial->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Financial"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Financial->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Rights != Mp7JrsRightsPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Rights->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Rights"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Rights->Serialize(doc, element, &elem);
	}
	if (m_AvailabilityPeriod != Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AvailabilityPeriod->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsAvailabilityType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InstanceRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InstanceRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInstanceRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Dissemination"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Dissemination")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDisseminationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDissemination(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Financial"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Financial")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFinancialType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFinancial(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Rights"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Rights")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRightsType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRights(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AvailabilityPeriod"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AvailabilityPeriod")) == 0))
		{
			// Deserialize factory type
			Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr tmp = CreateAvailabilityType_AvailabilityPeriod_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAvailabilityPeriod(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAvailabilityType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("InstanceRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetInstanceRef(child);
  }
  if (XMLString::compareString(elementname, X("Dissemination")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDisseminationType; // FTT, check this
	}
	this->SetDissemination(child);
  }
  if (XMLString::compareString(elementname, X("Financial")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFinancialType; // FTT, check this
	}
	this->SetFinancial(child);
  }
  if (XMLString::compareString(elementname, X("Rights")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRightsType; // FTT, check this
	}
	this->SetRights(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AvailabilityPeriod")) == 0))
  {
	Mp7JrsAvailabilityType_AvailabilityPeriod_CollectionPtr tmp = CreateAvailabilityType_AvailabilityPeriod_CollectionType; // FTT, check this
	this->SetAvailabilityPeriod(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAvailabilityType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInstanceRef() != Dc1NodePtr())
		result.Insert(GetInstanceRef());
	if (GetDissemination() != Dc1NodePtr())
		result.Insert(GetDissemination());
	if (GetFinancial() != Dc1NodePtr())
		result.Insert(GetFinancial());
	if (GetRights() != Dc1NodePtr())
		result.Insert(GetRights());
	if (GetAvailabilityPeriod() != Dc1NodePtr())
		result.Insert(GetAvailabilityPeriod());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAvailabilityType_ExtMethodImpl.h


