
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsMediaIdentificationType_AudioDomain_CollectionType.h"
#include "Mp7JrsMediaIdentificationType_VideoDomain_CollectionType.h"
#include "Mp7JrsMediaIdentificationType_ImageDomain_CollectionType.h"
#include "Mp7JrsMediaIdentificationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsControlledTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AudioDomain

#include <assert.h>
IMp7JrsMediaIdentificationType::IMp7JrsMediaIdentificationType()
{

// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtPropInit.h

}

IMp7JrsMediaIdentificationType::~IMp7JrsMediaIdentificationType()
{
// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtPropCleanup.h

}

Mp7JrsMediaIdentificationType::Mp7JrsMediaIdentificationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaIdentificationType::~Mp7JrsMediaIdentificationType()
{
	Cleanup();
}

void Mp7JrsMediaIdentificationType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_EntityIdentifier = Mp7JrsUniqueIDPtr(); // Class
	m_AudioDomain = Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr(); // Collection
	m_VideoDomain = Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr(); // Collection
	m_ImageDomain = Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtMyPropInit.h

}

void Mp7JrsMediaIdentificationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_EntityIdentifier);
	// Dc1Factory::DeleteObject(m_AudioDomain);
	// Dc1Factory::DeleteObject(m_VideoDomain);
	// Dc1Factory::DeleteObject(m_ImageDomain);
}

void Mp7JrsMediaIdentificationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaIdentificationTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaIdentificationType
	const Dc1Ptr< Mp7JrsMediaIdentificationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMediaIdentificationType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_EntityIdentifier);
		this->SetEntityIdentifier(Dc1Factory::CloneObject(tmp->GetEntityIdentifier()));
		// Dc1Factory::DeleteObject(m_AudioDomain);
		this->SetAudioDomain(Dc1Factory::CloneObject(tmp->GetAudioDomain()));
		// Dc1Factory::DeleteObject(m_VideoDomain);
		this->SetVideoDomain(Dc1Factory::CloneObject(tmp->GetVideoDomain()));
		// Dc1Factory::DeleteObject(m_ImageDomain);
		this->SetImageDomain(Dc1Factory::CloneObject(tmp->GetImageDomain()));
}

Dc1NodePtr Mp7JrsMediaIdentificationType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMediaIdentificationType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsMediaIdentificationType::GetBase() const
{
	return m_Base;
}

Mp7JrsUniqueIDPtr Mp7JrsMediaIdentificationType::GetEntityIdentifier() const
{
		return m_EntityIdentifier;
}

Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr Mp7JrsMediaIdentificationType::GetAudioDomain() const
{
		return m_AudioDomain;
}

Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr Mp7JrsMediaIdentificationType::GetVideoDomain() const
{
		return m_VideoDomain;
}

Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr Mp7JrsMediaIdentificationType::GetImageDomain() const
{
		return m_ImageDomain;
}

void Mp7JrsMediaIdentificationType::SetEntityIdentifier(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIdentificationType::SetEntityIdentifier().");
	}
	if (m_EntityIdentifier != item)
	{
		// Dc1Factory::DeleteObject(m_EntityIdentifier);
		m_EntityIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_EntityIdentifier) m_EntityIdentifier->SetParent(m_myself.getPointer());
		if (m_EntityIdentifier && m_EntityIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_EntityIdentifier->UseTypeAttribute = true;
		}
		if(m_EntityIdentifier != Mp7JrsUniqueIDPtr())
		{
			m_EntityIdentifier->SetContentName(XMLString::transcode("EntityIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaIdentificationType::SetAudioDomain(const Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIdentificationType::SetAudioDomain().");
	}
	if (m_AudioDomain != item)
	{
		// Dc1Factory::DeleteObject(m_AudioDomain);
		m_AudioDomain = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioDomain) m_AudioDomain->SetParent(m_myself.getPointer());
		if(m_AudioDomain != Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr())
		{
			m_AudioDomain->SetContentName(XMLString::transcode("AudioDomain"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaIdentificationType::SetVideoDomain(const Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIdentificationType::SetVideoDomain().");
	}
	if (m_VideoDomain != item)
	{
		// Dc1Factory::DeleteObject(m_VideoDomain);
		m_VideoDomain = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoDomain) m_VideoDomain->SetParent(m_myself.getPointer());
		if(m_VideoDomain != Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr())
		{
			m_VideoDomain->SetContentName(XMLString::transcode("VideoDomain"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaIdentificationType::SetImageDomain(const Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaIdentificationType::SetImageDomain().");
	}
	if (m_ImageDomain != item)
	{
		// Dc1Factory::DeleteObject(m_ImageDomain);
		m_ImageDomain = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ImageDomain) m_ImageDomain->SetParent(m_myself.getPointer());
		if(m_ImageDomain != Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr())
		{
			m_ImageDomain->SetContentName(XMLString::transcode("ImageDomain"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMediaIdentificationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("EntityIdentifier")) == 0)
	{
		// EntityIdentifier is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEntityIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEntityIdentifier(p, client);
					if((p = GetEntityIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioDomain")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AudioDomain is item of type ControlledTermUseType
		// in element collection MediaIdentificationType_AudioDomain_CollectionType
		
		context->Found = true;
		Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr coll = GetAudioDomain();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaIdentificationType_AudioDomain_CollectionType; // FTT, check this
				SetAudioDomain(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("VideoDomain")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:VideoDomain is item of type ControlledTermUseType
		// in element collection MediaIdentificationType_VideoDomain_CollectionType
		
		context->Found = true;
		Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr coll = GetVideoDomain();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaIdentificationType_VideoDomain_CollectionType; // FTT, check this
				SetVideoDomain(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ImageDomain")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ImageDomain is item of type ControlledTermUseType
		// in element collection MediaIdentificationType_ImageDomain_CollectionType
		
		context->Found = true;
		Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr coll = GetImageDomain();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaIdentificationType_ImageDomain_CollectionType; // FTT, check this
				SetImageDomain(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaIdentificationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaIdentificationType");
	}
	return result;
}

XMLCh * Mp7JrsMediaIdentificationType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMediaIdentificationType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMediaIdentificationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaIdentificationType"));
	// Element serialization:
	// Class
	
	if (m_EntityIdentifier != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_EntityIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("EntityIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_EntityIdentifier->Serialize(doc, element, &elem);
	}
	if (m_AudioDomain != Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AudioDomain->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_VideoDomain != Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VideoDomain->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ImageDomain != Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ImageDomain->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMediaIdentificationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("EntityIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("EntityIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetEntityIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AudioDomain"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AudioDomain")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr tmp = CreateMediaIdentificationType_AudioDomain_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAudioDomain(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("VideoDomain"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("VideoDomain")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr tmp = CreateMediaIdentificationType_VideoDomain_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVideoDomain(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ImageDomain"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ImageDomain")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr tmp = CreateMediaIdentificationType_ImageDomain_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetImageDomain(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaIdentificationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("EntityIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetEntityIdentifier(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AudioDomain")) == 0))
  {
	Mp7JrsMediaIdentificationType_AudioDomain_CollectionPtr tmp = CreateMediaIdentificationType_AudioDomain_CollectionType; // FTT, check this
	this->SetAudioDomain(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("VideoDomain")) == 0))
  {
	Mp7JrsMediaIdentificationType_VideoDomain_CollectionPtr tmp = CreateMediaIdentificationType_VideoDomain_CollectionType; // FTT, check this
	this->SetVideoDomain(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ImageDomain")) == 0))
  {
	Mp7JrsMediaIdentificationType_ImageDomain_CollectionPtr tmp = CreateMediaIdentificationType_ImageDomain_CollectionType; // FTT, check this
	this->SetImageDomain(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaIdentificationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetEntityIdentifier() != Dc1NodePtr())
		result.Insert(GetEntityIdentifier());
	if (GetAudioDomain() != Dc1NodePtr())
		result.Insert(GetAudioDomain());
	if (GetVideoDomain() != Dc1NodePtr())
		result.Insert(GetVideoDomain());
	if (GetImageDomain() != Dc1NodePtr())
		result.Insert(GetImageDomain());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaIdentificationType_ExtMethodImpl.h


