
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtImplInclude.h


#include "Mp7JrsFinancialType_AccountItem_CollectionType.h"
#include "Mp7JrsFinancialType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsFinancialType_AccountItem_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:AccountItem

#include <assert.h>
IMp7JrsFinancialType::IMp7JrsFinancialType()
{

// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtPropInit.h

}

IMp7JrsFinancialType::~IMp7JrsFinancialType()
{
// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtPropCleanup.h

}

Mp7JrsFinancialType::Mp7JrsFinancialType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFinancialType::~Mp7JrsFinancialType()
{
	Cleanup();
}

void Mp7JrsFinancialType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_AccountItem = Mp7JrsFinancialType_AccountItem_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtMyPropInit.h

}

void Mp7JrsFinancialType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_AccountItem);
}

void Mp7JrsFinancialType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FinancialTypePtr, since we
	// might need GetBase(), which isn't defined in IFinancialType
	const Dc1Ptr< Mp7JrsFinancialType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_AccountItem);
		this->SetAccountItem(Dc1Factory::CloneObject(tmp->GetAccountItem()));
}

Dc1NodePtr Mp7JrsFinancialType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFinancialType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsFinancialType_AccountItem_CollectionPtr Mp7JrsFinancialType::GetAccountItem() const
{
		return m_AccountItem;
}

void Mp7JrsFinancialType::SetAccountItem(const Mp7JrsFinancialType_AccountItem_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFinancialType::SetAccountItem().");
	}
	if (m_AccountItem != item)
	{
		// Dc1Factory::DeleteObject(m_AccountItem);
		m_AccountItem = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AccountItem) m_AccountItem->SetParent(m_myself.getPointer());
		if(m_AccountItem != Mp7JrsFinancialType_AccountItem_CollectionPtr())
		{
			m_AccountItem->SetContentName(XMLString::transcode("AccountItem"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFinancialType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("AccountItem")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:AccountItem is item of type FinancialType_AccountItem_LocalType
		// in element collection FinancialType_AccountItem_CollectionType
		
		context->Found = true;
		Mp7JrsFinancialType_AccountItem_CollectionPtr coll = GetAccountItem();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateFinancialType_AccountItem_CollectionType; // FTT, check this
				SetAccountItem(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FinancialType_AccountItem_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFinancialType_AccountItem_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FinancialType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FinancialType");
	}
	return result;
}

XMLCh * Mp7JrsFinancialType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFinancialType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFinancialType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FinancialType"));
	// Element serialization:
	if (m_AccountItem != Mp7JrsFinancialType_AccountItem_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_AccountItem->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsFinancialType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("AccountItem"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("AccountItem")) == 0))
		{
			// Deserialize factory type
			Mp7JrsFinancialType_AccountItem_CollectionPtr tmp = CreateFinancialType_AccountItem_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetAccountItem(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFinancialType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("AccountItem")) == 0))
  {
	Mp7JrsFinancialType_AccountItem_CollectionPtr tmp = CreateFinancialType_AccountItem_CollectionType; // FTT, check this
	this->SetAccountItem(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFinancialType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAccountItem() != Dc1NodePtr())
		result.Insert(GetAccountItem());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFinancialType_ExtMethodImpl.h


