
// Extension library class
// NodeRefCollectionType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

#include <stdio.h>
#include "Mp7JrsVideoViewGraphType_CollectionType.h"
#include "Dc1FactoryDefines.h"
 
#include "Mp7JrsFactoryDefines.h"

#include "Dc1Util.h"
#include "Dc1Convert.h"
#include "Dc1ClientManager.h"

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtImplInclude.h


#include "Mp7JrsVideoViewGraphType_LocalType.h" // Collection itemtype
// Choice item type VideoViewGraphType is concrete
// Choice item type ReferenceType is concrete
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsVideoViewGraphType.h"



Mp7JrsVideoViewGraphType_CollectionType::Mp7JrsVideoViewGraphType_CollectionType()
{
	m_Item = new Dc1ValueVectorOf<Dc1NodePtr>(0); // XXX
	dontSerializeWithContentName = false; // default -- (see Customize.template::DontSetContentNameFor)

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtPropInit.h


}

Mp7JrsVideoViewGraphType_CollectionType::~Mp7JrsVideoViewGraphType_CollectionType()
{
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtPropCleanup.h


	delete m_Item;
}

void Mp7JrsVideoViewGraphType_CollectionType::DeepCopy(const Dc1NodePtr &original)
{
	const Mp7JrsVideoViewGraphType_CollectionPtr tmp = original;
	if (tmp == NULL) return;  // EXIT: passed argument of wrong type

	removeAllElements();
	for(unsigned int i = 0; i < tmp->size(); i++)
	{
		Dc1NodePtr dummy = Dc1Factory::CloneObject(tmp->elementAt(i));
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (dummy) dummy->SetParent(m_myself.getPointer());
		m_Item->addElement(dummy);
	}
}

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtMethodImpl.h


void Mp7JrsVideoViewGraphType_CollectionType::Initialize(unsigned int maxElems)
{
	m_Item->ensureExtraCapacity(maxElems);
}

void Mp7JrsVideoViewGraphType_CollectionType::addElement(const Dc1NodePtr &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_CollectionType::addElement().");
	}
	m_Item->addElement(toAdd);
	if (toAdd) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toAdd->SetParent(m_myself.getPointer());
		if (!toAdd->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toAdd->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_ADDED,
		client, toAdd);
}

void Mp7JrsVideoViewGraphType_CollectionType::setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_CollectionType::setElementAt().");
	}
	Dc1NodePtr previous = m_Item->elementAt(setAt);
	if (previous) previous->SetParent(Dc1NodePtr());
	m_Item->setElementAt(toSet, setAt);

	if (toSet) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toSet->SetParent(m_myself.getPointer());
		if (!toSet->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toSet->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, toSet, setAt);
}

void Mp7JrsVideoViewGraphType_CollectionType::insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_CollectionType::insertElementAt().");
	}
	m_Item->insertElementAt(toInsert, insertAt);
	if (toInsert) {
		// sat 2004-05-05: factor this out into method when there's a spare hour to compile ...
		toInsert->SetParent(m_myself.getPointer());
		if (!toInsert->GetContentName()) {
			// It's a LocalType (presumably) -- the content name isn't used.
			toInsert->SetContentName(XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(""));
		}
		
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_INSERTED,
		client, toInsert, insertAt);
}

/*
Dc1NodePtr Mp7JrsVideoViewGraphType_CollectionType::orphanElementAt(unsigned int orphanAt)
{
	return m_Item->orphanElementAt(orphanAt);
}
*/

void Mp7JrsVideoViewGraphType_CollectionType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_CollectionType::removeAllElements().");
	}

	for (unsigned int i = 0; i < m_Item->size(); i++) {
		// FIXME: handle elementAt(removeAt) == NULL
		m_Item->elementAt(i)->SetParent(Dc1NodePtr());
	}

	m_Item->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType_CollectionType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType_CollectionType::removeElementAt().");
	}
	// FIXME: handle elementAt(removeAt) == NULL
	m_Item->elementAt(removeAt)->SetParent(Dc1NodePtr());
	m_Item->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsVideoViewGraphType_CollectionType::containsElement(const Dc1NodePtr &toCheck)
{
	return m_Item->containsElement(toCheck);
}

unsigned int Mp7JrsVideoViewGraphType_CollectionType::curCapacity() const
{
	return (unsigned int)m_Item->curCapacity();
}

const Dc1NodePtr Mp7JrsVideoViewGraphType_CollectionType::elementAt(unsigned int getAt) const
{
	return m_Item->elementAt(getAt);
}

Dc1NodePtr Mp7JrsVideoViewGraphType_CollectionType::elementAt(unsigned int getAt)
{
	return m_Item->elementAt(getAt);
}

Dc1NodeEnum Mp7JrsVideoViewGraphType_CollectionType::GetElementsContainingType(unsigned int id) const
{
	Dc1Enumerator< Dc1NodePtr > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		Mp7JrsVideoViewGraphType_LocalPtr tmp = elementAt(i);
		if (tmp != Mp7JrsVideoViewGraphType_LocalPtr() && tmp->GetClassId() == Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType_LocalType"))) {
			// FTT this is better!
			if (tmp->GetSpaceFrequencyChild() != Mp7JrsVideoViewGraphPtr()
				&& tmp->GetSpaceFrequencyChild()->GetClassId() == id)
				{
					result.Insert(tmp->GetSpaceFrequencyChild());
					continue;
				}
			if (tmp->GetSpaceFrequencyChildRef() != Mp7JrsReferencePtr()
				&& tmp->GetSpaceFrequencyChildRef()->GetClassId() == id)
				{
					result.Insert(tmp->GetSpaceFrequencyChildRef());
					continue;
				}
		}
	}
	return result;
}
Dc1NodeEnum Mp7JrsVideoViewGraphType_CollectionType::GetElementsOfType(unsigned int id) const
{
	Dc1Enumerator< Dc1NodePtr > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		if (elementAt(i)->GetClassId() == id)
		{
			result.Insert(elementAt(i));
		}
	}
	return result;
}

Dc1NodeEnum Mp7JrsVideoViewGraphType_CollectionType::GetElements() const
{
	Dc1Enumerator< Dc1Ptr<Dc1Node> > result;
	for (unsigned int i = 0; i < size(); i++)
	{
		result.Insert(elementAt(i));
	}
	return result;
}

unsigned int Mp7JrsVideoViewGraphType_CollectionType::size() const
{
	return (unsigned int)m_Item->size();
}

void Mp7JrsVideoViewGraphType_CollectionType::ensureExtraCapacity(unsigned int length)
{
	m_Item->ensureExtraCapacity(length);
}

// JRS: addition to Xerces collection
int Mp7JrsVideoViewGraphType_CollectionType::elementIndexOf(const Dc1NodePtr &toCheck) const
{
	for(unsigned int i = 0; i < size(); i++)
	{
		if(elementAt(i) == toCheck)
		{
			return (int) i;
		}
	}
	return -1; // not found
}

XMLCh * Mp7JrsVideoViewGraphType_CollectionType::ToText() const
{
	XMLCh * buf = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode("");
	XMLCh * delim = XERCES_CPP_NAMESPACE_QUALIFIER XMLString::transcode(" ");
	for(unsigned int i = 0; i < size(); i++)
	{
		XMLCh * tmp = (elementAt(i))->ToText();
		Dc1Util::CatString(&buf, tmp);
		XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&tmp);
		if(i < size() - 1)
		{
			Dc1Util::CatString(&buf, delim);
		}
	}
	XERCES_CPP_NAMESPACE_QUALIFIER XMLString::release(&delim);
	// Note: You must release this buffer
	return buf;
}

bool Mp7JrsVideoViewGraphType_CollectionType::Parse(const XMLCh * const txt)
{
	if(txt == NULL)
	{
		return false;
	}
		
	// This is not a simpletype list ItemType and intentionally unimplemented for collections
	return false;
}

/** 
 * Item type is ElementOnly. 
 */
void Mp7JrsVideoViewGraphType_CollectionType::Serialize(XERCES_CPP_NAMESPACE_QUALIFIER DOMDocument * doc, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* parent, XERCES_CPP_NAMESPACE_QUALIFIER DOMElement** newElem)
{
	// The element we serialize into
	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* element;

	XERCES_CPP_NAMESPACE_QUALIFIER DOMElement* localNewElem = NULL;
	if (!newElem) newElem = &localNewElem;

	if (*newElem) {
		element = *newElem;
	} else if(parent) {
		element = parent;
	} else {
		element = doc->getDocumentElement();
	}


// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtPreSerialize.h


	for(unsigned int i = 0; i < size(); i++)
	{
		// Collection is not SimpleTypeList -> serialize elements
		if(this->GetContentName() != NULL && !dontSerializeWithContentName)
		{
// OLD			XERCES_CPP_NAMESPACE_QUALIFIER DOMElement * elem = doc->createElement(this->GetContentName());
      DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			*newElem = elem;
			(elementAt(i))->Serialize(doc, elem, newElem);
			element->appendChild(elem);
		}
		else
		{
			// Serialize into current node
			(elementAt(i))->Serialize(doc, element, &element);
		}
		if(i < size() - 1)
		{
			element->appendChild(doc->createTextNode(X(" ")));
		}
	}
}



bool Mp7JrsVideoViewGraphType_CollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(parent == NULL)
	{
		return false;
	}
	* current = parent; // Init with first element - check this!

	// Deserialize collection
	// Itemtype = Empty | ElementOnly
	DOMElement * ignore = NULL;
	while((parent != NULL) && (
		Dc1Util::HasNodeName(parent, X("SpaceFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004"))
// OLD3		(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChild")) == 0)
		||
		Dc1Util::HasNodeName(parent, X("SpaceFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004"))
// OLD3		(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChildRef")) == 0)
	))
	{
		Dc1NodePtr item = Mp7JrsVideoViewGraphType_LocalPtr();

		item = CreateVideoViewGraphType_LocalType; // FTT, check this
		// Sequence or Choice -- don't use "own" node
		item->Deserialize(doc, parent, current);
		// baw 2004-11-15 begin: do not set content name on local type
		// XMLCh * contentname = XMLString::replicate(parent->getNodeName());
		// item->SetContentName(contentname);
		item->SetContentName(NULL);
		// baw 2004-11-15 end
		addElement(item);
		parent = Dc1Util::GetNextAkin(* current); // Previously GetNextSibling
	}


// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_CollectionType_ExtPostDeserialize.h


	return true;
}

inline
bool am_in_collection(int next_slash, int next_bracket, int next_close_bracket)
{
	return next_bracket >= 0
		&& next_close_bracket > next_bracket + 1 // at least one digit
		&& (next_slash < 0		// either at end of string ...
			|| next_slash > next_close_bracket); // ... or collection
												 // is in current part
}

Dc1NodePtr Mp7JrsVideoViewGraphType_CollectionType::NodeFromXPath(const XMLCh *xpath) const
{
	const XMLCh *work_string = xpath;
	const XMLCh *content_name = GetContentName();
	Dc1NodePtr me = m_myself.getPointer();
	Dc1NodeCollectionPtr me_as_collection = me;
	Dc1NodePtr parent = GetParent();

	if (XMLString::startsWith(work_string, X("/"))) {
		if (!parent)
			work_string++;		// consume leading slash
		else {
			Dc1NodePtr root = parent;
			while (root->GetParent())
				root = root->GetParent();
			return root->NodeFromXPath(work_string); // EXIT: parse from root
		}
	}

	if (XMLString::stringLen(work_string) == 0)
		return me;

	if (XMLString::startsWith(work_string, X("..")))
		return UpwardWithXPath(work_string);

	XMLCh slash = X("/")[0];
	XMLCh bracket = X("[")[0];
	XMLCh close_bracket = X("]")[0];
	int next_slash = XMLString::indexOf(work_string, slash);
	int next_bracket = XMLString::indexOf(work_string, bracket);
	int next_close_bracket = XMLString::indexOf(work_string, close_bracket);

	// We're a collection -- find n, nth element, parse from there
	int n;
	if (!am_in_collection(next_slash, next_bracket, next_close_bracket))
	{
		if (!content_name) {
			// A collection can be a a choice too; there's a valid
			// test case where we needed to descend into a
			// collection for the xpath string "Name".  The
			// "!content_name" clause is just a futile attempt to
			// cut back the lossage a bit; I'm assuming that none
			// of these freak collections has it set.
			Dc1NodeEnum children = me_as_collection->GetElements();
			while (children.HasNext()) {
				Dc1NodePtr result = children.GetNext()->NodeFromXPath(xpath);
				if (result != Dc1NodePtr())
					return result;	// EXIT: success!
			}
		}
		// fall-through case: no collection index found, malformed
		// xpath string or xpath string didn't match the children.
		return Dc1NodePtr(); // EXIT
	}
	XMLCh *n_str = Dc1Util::allocate(next_close_bracket - next_bracket + 1);
	XMLString::moveChars(n_str, work_string + next_bracket + 1,
		next_close_bracket - next_bracket - 1);
	n = XMLString::parseInt(n_str) - 1;	// XPath indices start
	// with 1, ours start with
	// 0 -- adjust
	XMLString::release(&n_str);
	if (n >= 0 && n < (int)me_as_collection->size()) {
		// if the parent is a LocalType, our ContentName has to
		// match; otherwise, it's allowed to be unset.  if it's
		// set, it has to match.
		if ((content_name
				&& XMLString::startsWith(work_string, content_name)
				&& !XMLString::isAlphaNum(
					work_string[XMLString::stringLen(content_name)])))
		{
			if (next_slash != -1)
				if (dontSerializeWithContentName)
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string);
				else
					return me_as_collection->elementAt(n)->NodeFromXPath(
						work_string + next_slash + 1);
			else
				// EXIT: success!  (xpath ended with ".../elemName[n]")
				return me_as_collection->elementAt(n);
		}
		else if (!content_name)
		{
			// It's probably a local collection (collection ->
			// LocalType -> Type) -- the name will be picked up by
			// the Type node, so pass in full work string here.
			return me_as_collection->elementAt(n)->NodeFromXPath(
				work_string); // EXIT
		}
		else
			return Dc1NodePtr(); // EXIT: wrong content name
	} else
	return Dc1NodePtr();	// EXIT: index out of range

}

