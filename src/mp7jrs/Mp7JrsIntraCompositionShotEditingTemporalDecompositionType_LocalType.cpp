
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsIntraCompositionShotType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsInternalTransitionType.h"
#include "Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::~IMp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::~Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_IntraCompositionShot = Mp7JrsIntraCompositionShotPtr(); // Class
	m_IntraCompositionShotRef = Mp7JrsReferencePtr(); // Class
	m_InternalTransition = Mp7JrsInternalTransitionPtr(); // Class
	m_InternalTransitionRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_IntraCompositionShot);
	// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
	// Dc1Factory::DeleteObject(m_InternalTransition);
	// Dc1Factory::DeleteObject(m_InternalTransitionRef);
}

void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IntraCompositionShotEditingTemporalDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IIntraCompositionShotEditingTemporalDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_IntraCompositionShot);
		this->SetIntraCompositionShot(Dc1Factory::CloneObject(tmp->GetIntraCompositionShot()));
		// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
		this->SetIntraCompositionShotRef(Dc1Factory::CloneObject(tmp->GetIntraCompositionShotRef()));
		// Dc1Factory::DeleteObject(m_InternalTransition);
		this->SetInternalTransition(Dc1Factory::CloneObject(tmp->GetInternalTransition()));
		// Dc1Factory::DeleteObject(m_InternalTransitionRef);
		this->SetInternalTransitionRef(Dc1Factory::CloneObject(tmp->GetInternalTransitionRef()));
}

Dc1NodePtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsIntraCompositionShotPtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetIntraCompositionShot() const
{
		return m_IntraCompositionShot;
}

Mp7JrsReferencePtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetIntraCompositionShotRef() const
{
		return m_IntraCompositionShotRef;
}

Mp7JrsInternalTransitionPtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetInternalTransition() const
{
		return m_InternalTransition;
}

Mp7JrsReferencePtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetInternalTransitionRef() const
{
		return m_InternalTransitionRef;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetIntraCompositionShot(const Mp7JrsIntraCompositionShotPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetIntraCompositionShot().");
	}
	if (m_IntraCompositionShot != item)
	{
		// Dc1Factory::DeleteObject(m_IntraCompositionShot);
		m_IntraCompositionShot = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IntraCompositionShot) m_IntraCompositionShot->SetParent(m_myself.getPointer());
		if (m_IntraCompositionShot && m_IntraCompositionShot->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IntraCompositionShot->UseTypeAttribute = true;
		}
		if(m_IntraCompositionShot != Mp7JrsIntraCompositionShotPtr())
		{
			m_IntraCompositionShot->SetContentName(XMLString::transcode("IntraCompositionShot"));
		}
	// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
	m_IntraCompositionShotRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_InternalTransition);
	m_InternalTransition = Mp7JrsInternalTransitionPtr();
	// Dc1Factory::DeleteObject(m_InternalTransitionRef);
	m_InternalTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetIntraCompositionShotRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetIntraCompositionShotRef().");
	}
	if (m_IntraCompositionShotRef != item)
	{
		// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
		m_IntraCompositionShotRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IntraCompositionShotRef) m_IntraCompositionShotRef->SetParent(m_myself.getPointer());
		if (m_IntraCompositionShotRef && m_IntraCompositionShotRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IntraCompositionShotRef->UseTypeAttribute = true;
		}
		if(m_IntraCompositionShotRef != Mp7JrsReferencePtr())
		{
			m_IntraCompositionShotRef->SetContentName(XMLString::transcode("IntraCompositionShotRef"));
		}
	// Dc1Factory::DeleteObject(m_IntraCompositionShot);
	m_IntraCompositionShot = Mp7JrsIntraCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_InternalTransition);
	m_InternalTransition = Mp7JrsInternalTransitionPtr();
	// Dc1Factory::DeleteObject(m_InternalTransitionRef);
	m_InternalTransitionRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* choice */
// implementing setter for choice 
/* element */
void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetInternalTransition(const Mp7JrsInternalTransitionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetInternalTransition().");
	}
	if (m_InternalTransition != item)
	{
		// Dc1Factory::DeleteObject(m_InternalTransition);
		m_InternalTransition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InternalTransition) m_InternalTransition->SetParent(m_myself.getPointer());
		if (m_InternalTransition && m_InternalTransition->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InternalTransitionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InternalTransition->UseTypeAttribute = true;
		}
		if(m_InternalTransition != Mp7JrsInternalTransitionPtr())
		{
			m_InternalTransition->SetContentName(XMLString::transcode("InternalTransition"));
		}
	// Dc1Factory::DeleteObject(m_InternalTransitionRef);
	m_InternalTransitionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_IntraCompositionShot);
	m_IntraCompositionShot = Mp7JrsIntraCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
	m_IntraCompositionShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetInternalTransitionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::SetInternalTransitionRef().");
	}
	if (m_InternalTransitionRef != item)
	{
		// Dc1Factory::DeleteObject(m_InternalTransitionRef);
		m_InternalTransitionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InternalTransitionRef) m_InternalTransitionRef->SetParent(m_myself.getPointer());
		if (m_InternalTransitionRef && m_InternalTransitionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_InternalTransitionRef->UseTypeAttribute = true;
		}
		if(m_InternalTransitionRef != Mp7JrsReferencePtr())
		{
			m_InternalTransitionRef->SetContentName(XMLString::transcode("InternalTransitionRef"));
		}
	// Dc1Factory::DeleteObject(m_InternalTransition);
	m_InternalTransition = Mp7JrsInternalTransitionPtr();
	// Dc1Factory::DeleteObject(m_IntraCompositionShot);
	m_IntraCompositionShot = Mp7JrsIntraCompositionShotPtr();
	// Dc1Factory::DeleteObject(m_IntraCompositionShotRef);
	m_IntraCompositionShotRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: IntraCompositionShotEditingTemporalDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("IntraCompositionShotEditingTemporalDecompositionType_LocalType"));
	// Element serialization:
	// Class
	
	if (m_IntraCompositionShot != Mp7JrsIntraCompositionShotPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IntraCompositionShot->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IntraCompositionShot"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IntraCompositionShot->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_IntraCompositionShotRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IntraCompositionShotRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IntraCompositionShotRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IntraCompositionShotRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_InternalTransition != Mp7JrsInternalTransitionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InternalTransition->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InternalTransition"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InternalTransition->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_InternalTransitionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_InternalTransitionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("InternalTransitionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_InternalTransitionRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IntraCompositionShot"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IntraCompositionShot")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIntraCompositionShotType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIntraCompositionShot(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IntraCompositionShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IntraCompositionShotRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIntraCompositionShotRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InternalTransition"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InternalTransition")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateInternalTransitionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInternalTransition(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("InternalTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("InternalTransitionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInternalTransitionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
	} while(false);
// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("IntraCompositionShot")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIntraCompositionShotType; // FTT, check this
	}
	this->SetIntraCompositionShot(child);
  }
  if (XMLString::compareString(elementname, X("IntraCompositionShotRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetIntraCompositionShotRef(child);
  }
  if (XMLString::compareString(elementname, X("InternalTransition")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateInternalTransitionType; // FTT, check this
	}
	this->SetInternalTransition(child);
  }
  if (XMLString::compareString(elementname, X("InternalTransitionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetInternalTransitionRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetIntraCompositionShot() != Dc1NodePtr())
		result.Insert(GetIntraCompositionShot());
	if (GetIntraCompositionShotRef() != Dc1NodePtr())
		result.Insert(GetIntraCompositionShotRef());
	if (GetInternalTransition() != Dc1NodePtr())
		result.Insert(GetInternalTransition());
	if (GetInternalTransitionRef() != Dc1NodePtr())
		result.Insert(GetInternalTransitionRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsIntraCompositionShotEditingTemporalDecompositionType_LocalType_ExtMethodImpl.h


