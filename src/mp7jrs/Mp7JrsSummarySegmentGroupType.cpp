
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrszeroToOneType.h"
#include "W3CIDREFS.h"
#include "Mp7JrsSummarySegmentGroupType_Name_CollectionType.h"
#include "Mp7JrsSummarySegmentGroupType_Caption_CollectionType.h"
#include "Mp7JrsSummarySegmentGroupType_CollectionType.h"
#include "Mp7JrsSummarySegmentGroupType_CollectionType0.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsSummarySegmentGroupType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsSummarySegmentGroupType_LocalType.h" // Choice collection SummarySegment
#include "Mp7JrsSummarySegmentType.h" // Choice collection element SummarySegment
#include "Mp7JrsReferenceType.h" // Choice collection element SummarySegmentRef
#include "Mp7JrsSummarySegmentGroupType_LocalType0.h" // Choice collection SummarySegmentGroup
#include "Mp7JrsSummarySegmentGroupType.h" // Choice collection element SummarySegmentGroup

#include <assert.h>
IMp7JrsSummarySegmentGroupType::IMp7JrsSummarySegmentGroupType()
{

// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtPropInit.h

}

IMp7JrsSummarySegmentGroupType::~IMp7JrsSummarySegmentGroupType()
{
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtPropCleanup.h

}

Mp7JrsSummarySegmentGroupType::Mp7JrsSummarySegmentGroupType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSummarySegmentGroupType::~Mp7JrsSummarySegmentGroupType()
{
	Cleanup();
}

void Mp7JrsSummarySegmentGroupType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_level = 0; // Value
	m_level_Exist = false;
	m_duration = Mp7JrsmediaDurationPtr(); // Pattern
	m_duration_Exist = false;
	m_numOfKeyFrames = 0; // Value
	m_numOfKeyFrames_Exist = false;
	m_fidelity = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_fidelity->SetContent(0.0); // Use Value initialiser (xxx2)
	m_fidelity_Exist = false;
	m_themeIDs = W3CIDREFSPtr(); // Collection
	m_themeIDs_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Name = Mp7JrsSummarySegmentGroupType_Name_CollectionPtr(); // Collection
	m_Caption = Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr(); // Collection
	m_SummarySegmentGroupType_LocalType = Mp7JrsSummarySegmentGroupType_CollectionPtr(); // Collection
	m_SummarySegmentGroupType_LocalType0 = Mp7JrsSummarySegmentGroupType_Collection0Ptr(); // Collection


// begin extension included
// file Mp7JrsSummarySegmentGroupType_ExtMyPropInit.h

	// create empty name and caption collections
	m_Name = CreateSummarySegmentGroupType_Name_CollectionType;
	m_Caption = CreateSummarySegmentGroupType_Caption_CollectionType;

	// fidelity
	m_fidelity = CreatezeroToOneType;
	m_fidelity->SetContent(0.0);
// end extension included

}

void Mp7JrsSummarySegmentGroupType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	// Dc1Factory::DeleteObject(m_fidelity);
	// Dc1Factory::DeleteObject(m_themeIDs);
	// Dc1Factory::DeleteObject(m_Name);
	// Dc1Factory::DeleteObject(m_Caption);
	// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType);
	// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType0);
}

void Mp7JrsSummarySegmentGroupType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SummarySegmentGroupTypePtr, since we
	// might need GetBase(), which isn't defined in ISummarySegmentGroupType
	const Dc1Ptr< Mp7JrsSummarySegmentGroupType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSummarySegmentGroupType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existlevel())
	{
		this->Setlevel(tmp->Getlevel());
	}
	else
	{
		Invalidatelevel();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_duration); // Pattern
	if (tmp->Existduration())
	{
		this->Setduration(Dc1Factory::CloneObject(tmp->Getduration()));
	}
	else
	{
		Invalidateduration();
	}
	}
	{
	if (tmp->ExistnumOfKeyFrames())
	{
		this->SetnumOfKeyFrames(tmp->GetnumOfKeyFrames());
	}
	else
	{
		InvalidatenumOfKeyFrames();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_fidelity);
	if (tmp->Existfidelity())
	{
		this->Setfidelity(Dc1Factory::CloneObject(tmp->Getfidelity()));
	}
	else
	{
		Invalidatefidelity();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_themeIDs);
	if (tmp->ExistthemeIDs())
	{
		this->SetthemeIDs(Dc1Factory::CloneObject(tmp->GetthemeIDs()));
	}
	else
	{
		InvalidatethemeIDs();
	}
	}
		// Dc1Factory::DeleteObject(m_Name);
		this->SetName(Dc1Factory::CloneObject(tmp->GetName()));
		// Dc1Factory::DeleteObject(m_Caption);
		this->SetCaption(Dc1Factory::CloneObject(tmp->GetCaption()));
		// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType);
		this->SetSummarySegmentGroupType_LocalType(Dc1Factory::CloneObject(tmp->GetSummarySegmentGroupType_LocalType()));
		// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType0);
		this->SetSummarySegmentGroupType_LocalType0(Dc1Factory::CloneObject(tmp->GetSummarySegmentGroupType_LocalType0()));
}

Dc1NodePtr Mp7JrsSummarySegmentGroupType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSummarySegmentGroupType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsSummarySegmentGroupType::GetBase() const
{
	return m_Base;
}

int Mp7JrsSummarySegmentGroupType::Getlevel() const
{
	return m_level;
}

bool Mp7JrsSummarySegmentGroupType::Existlevel() const
{
	return m_level_Exist;
}
void Mp7JrsSummarySegmentGroupType::Setlevel(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::Setlevel().");
	}
	m_level = item;
	m_level_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::Invalidatelevel()
{
	m_level_Exist = false;
}
Mp7JrsmediaDurationPtr Mp7JrsSummarySegmentGroupType::Getduration() const
{
	return m_duration;
}

bool Mp7JrsSummarySegmentGroupType::Existduration() const
{
	return m_duration_Exist;
}
void Mp7JrsSummarySegmentGroupType::Setduration(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::Setduration().");
	}
	m_duration = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_duration) m_duration->SetParent(m_myself.getPointer());
	m_duration_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::Invalidateduration()
{
	m_duration_Exist = false;
}
unsigned Mp7JrsSummarySegmentGroupType::GetnumOfKeyFrames() const
{
	return m_numOfKeyFrames;
}

bool Mp7JrsSummarySegmentGroupType::ExistnumOfKeyFrames() const
{
	return m_numOfKeyFrames_Exist;
}
void Mp7JrsSummarySegmentGroupType::SetnumOfKeyFrames(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetnumOfKeyFrames().");
	}
	m_numOfKeyFrames = item;
	m_numOfKeyFrames_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatenumOfKeyFrames()
{
	m_numOfKeyFrames_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsSummarySegmentGroupType::Getfidelity() const
{
	return m_fidelity;
}

bool Mp7JrsSummarySegmentGroupType::Existfidelity() const
{
	return m_fidelity_Exist;
}
void Mp7JrsSummarySegmentGroupType::Setfidelity(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::Setfidelity().");
	}
	m_fidelity = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_fidelity) m_fidelity->SetParent(m_myself.getPointer());
	m_fidelity_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::Invalidatefidelity()
{
	m_fidelity_Exist = false;
}
W3CIDREFSPtr Mp7JrsSummarySegmentGroupType::GetthemeIDs() const
{
	return m_themeIDs;
}

bool Mp7JrsSummarySegmentGroupType::ExistthemeIDs() const
{
	return m_themeIDs_Exist;
}
void Mp7JrsSummarySegmentGroupType::SetthemeIDs(const W3CIDREFSPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetthemeIDs().");
	}
	m_themeIDs = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_themeIDs) m_themeIDs->SetParent(m_myself.getPointer());
	m_themeIDs_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatethemeIDs()
{
	m_themeIDs_Exist = false;
}
Mp7JrsSummarySegmentGroupType_Name_CollectionPtr Mp7JrsSummarySegmentGroupType::GetName() const
{
		return m_Name;
}

Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr Mp7JrsSummarySegmentGroupType::GetCaption() const
{
		return m_Caption;
}

Mp7JrsSummarySegmentGroupType_CollectionPtr Mp7JrsSummarySegmentGroupType::GetSummarySegmentGroupType_LocalType() const
{
		return m_SummarySegmentGroupType_LocalType;
}

Mp7JrsSummarySegmentGroupType_Collection0Ptr Mp7JrsSummarySegmentGroupType::GetSummarySegmentGroupType_LocalType0() const
{
		return m_SummarySegmentGroupType_LocalType0;
}

void Mp7JrsSummarySegmentGroupType::SetName(const Mp7JrsSummarySegmentGroupType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetName().");
	}
	if (m_Name != item)
	{
		// Dc1Factory::DeleteObject(m_Name);
		m_Name = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Name) m_Name->SetParent(m_myself.getPointer());
		if(m_Name != Mp7JrsSummarySegmentGroupType_Name_CollectionPtr())
		{
			m_Name->SetContentName(XMLString::transcode("Name"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::SetCaption(const Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetCaption().");
	}
	if (m_Caption != item)
	{
		// Dc1Factory::DeleteObject(m_Caption);
		m_Caption = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Caption) m_Caption->SetParent(m_myself.getPointer());
		if(m_Caption != Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr())
		{
			m_Caption->SetContentName(XMLString::transcode("Caption"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::SetSummarySegmentGroupType_LocalType(const Mp7JrsSummarySegmentGroupType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetSummarySegmentGroupType_LocalType().");
	}
	if (m_SummarySegmentGroupType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType);
		m_SummarySegmentGroupType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummarySegmentGroupType_LocalType) m_SummarySegmentGroupType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::SetSummarySegmentGroupType_LocalType0(const Mp7JrsSummarySegmentGroupType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetSummarySegmentGroupType_LocalType0().");
	}
	if (m_SummarySegmentGroupType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_SummarySegmentGroupType_LocalType0);
		m_SummarySegmentGroupType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummarySegmentGroupType_LocalType0) m_SummarySegmentGroupType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsSummarySegmentGroupType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsSummarySegmentGroupType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsSummarySegmentGroupType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsSummarySegmentGroupType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsSummarySegmentGroupType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsSummarySegmentGroupType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsSummarySegmentGroupType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsSummarySegmentGroupType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsSummarySegmentGroupType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsSummarySegmentGroupType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsSummarySegmentGroupType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsSummarySegmentGroupType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsSummarySegmentGroupType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsSummarySegmentGroupType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsSummarySegmentGroupType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSummarySegmentGroupType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsSummarySegmentGroupType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsSummarySegmentGroupType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSummarySegmentGroupType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSummarySegmentGroupType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 10 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("level")) == 0)
	{
		// level is simple attribute integer
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("duration")) == 0)
	{
		// duration is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfKeyFrames")) == 0)
	{
		// numOfKeyFrames is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("fidelity")) == 0)
	{
		// fidelity is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("themeIDs")) == 0)
	{
		// themeIDs is simple attribute IDREFS
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CIDREFSPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Name")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Name is item of type TextualType
		// in element collection SummarySegmentGroupType_Name_CollectionType
		
		context->Found = true;
		Mp7JrsSummarySegmentGroupType_Name_CollectionPtr coll = GetName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_Name_CollectionType; // FTT, check this
				SetName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Caption")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Caption is item of type TextualType
		// in element collection SummarySegmentGroupType_Caption_CollectionType
		
		context->Found = true;
		Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr coll = GetCaption();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_Caption_CollectionType; // FTT, check this
				SetCaption(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegment")) == 0)
	{
		// SummarySegment is contained in itemtype SummarySegmentGroupType_LocalType
		// in choice collection SummarySegmentGroupType_CollectionType

		context->Found = true;
		Mp7JrsSummarySegmentGroupType_CollectionPtr coll = GetSummarySegmentGroupType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_CollectionType; // FTT, check this
				SetSummarySegmentGroupType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->GetSummarySegment()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSummarySegmentGroupType_LocalPtr item = CreateSummarySegmentGroupType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummarySegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->SetSummarySegment(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->GetSummarySegment()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegmentRef")) == 0)
	{
		// SummarySegmentRef is contained in itemtype SummarySegmentGroupType_LocalType
		// in choice collection SummarySegmentGroupType_CollectionType

		context->Found = true;
		Mp7JrsSummarySegmentGroupType_CollectionPtr coll = GetSummarySegmentGroupType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_CollectionType; // FTT, check this
				SetSummarySegmentGroupType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->GetSummarySegmentRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSummarySegmentGroupType_LocalPtr item = CreateSummarySegmentGroupType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->SetSummarySegmentRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSummarySegmentGroupType_LocalPtr)coll->elementAt(i))->GetSummarySegmentRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegmentGroup")) == 0)
	{
		// SummarySegmentGroup is contained in itemtype SummarySegmentGroupType_LocalType0
		// in choice collection SummarySegmentGroupType_CollectionType0

		context->Found = true;
		Mp7JrsSummarySegmentGroupType_Collection0Ptr coll = GetSummarySegmentGroupType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_CollectionType0; // FTT, check this
				SetSummarySegmentGroupType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->GetSummarySegmentGroup()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSummarySegmentGroupType_Local0Ptr item = CreateSummarySegmentGroupType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummarySegmentGroupPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->SetSummarySegmentGroup(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->GetSummarySegmentGroup()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegmentGroupRef")) == 0)
	{
		// SummarySegmentGroupRef is contained in itemtype SummarySegmentGroupType_LocalType0
		// in choice collection SummarySegmentGroupType_CollectionType0

		context->Found = true;
		Mp7JrsSummarySegmentGroupType_Collection0Ptr coll = GetSummarySegmentGroupType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSummarySegmentGroupType_CollectionType0; // FTT, check this
				SetSummarySegmentGroupType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->GetSummarySegmentGroupRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsSummarySegmentGroupType_Local0Ptr item = CreateSummarySegmentGroupType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->SetSummarySegmentGroupRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsSummarySegmentGroupType_Local0Ptr)coll->elementAt(i))->GetSummarySegmentGroupRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SummarySegmentGroupType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SummarySegmentGroupType");
	}
	return result;
}

XMLCh * Mp7JrsSummarySegmentGroupType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSummarySegmentGroupType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSummarySegmentGroupType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SummarySegmentGroupType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_level_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_level);
		element->setAttributeNS(X(""), X("level"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_duration_Exist)
	{
	// Pattern
	if (m_duration != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_duration->ToText();
		element->setAttributeNS(X(""), X("duration"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_numOfKeyFrames_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_numOfKeyFrames);
		element->setAttributeNS(X(""), X("numOfKeyFrames"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_fidelity_Exist)
	{
	// Class
	if (m_fidelity != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_fidelity->ToText();
		element->setAttributeNS(X(""), X("fidelity"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_themeIDs_Exist)
	{
	// Collection
	if(m_themeIDs != W3CIDREFSPtr())
	{
		XMLCh * tmp = m_themeIDs->ToText();
		element->setAttributeNS(X(""), X("themeIDs"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Name != Mp7JrsSummarySegmentGroupType_Name_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Name->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Caption != Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Caption->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SummarySegmentGroupType_LocalType != Mp7JrsSummarySegmentGroupType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SummarySegmentGroupType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_SummarySegmentGroupType_LocalType0 != Mp7JrsSummarySegmentGroupType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SummarySegmentGroupType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSummarySegmentGroupType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("level")))
	{
		// deserialize value type
		this->Setlevel(Dc1Convert::TextToInt(parent->getAttribute(X("level"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("duration")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("duration")));
		this->Setduration(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfKeyFrames")))
	{
		// deserialize value type
		this->SetnumOfKeyFrames(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("numOfKeyFrames"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("fidelity")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("fidelity")));
		this->Setfidelity(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("themeIDs")))
	{
		// Deserialize collection type
		W3CIDREFSPtr tmp = CreateIDREFS; // FTT, check this
		tmp->Parse(parent->getAttribute(X("themeIDs")));
		this->SetthemeIDs(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Name"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Name")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentGroupType_Name_CollectionPtr tmp = CreateSummarySegmentGroupType_Name_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Caption"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Caption")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr tmp = CreateSummarySegmentGroupType_Caption_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCaption(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SummarySegment
			Dc1Util::HasNodeName(parent, X("SummarySegment"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegment")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SummarySegmentRef
			Dc1Util::HasNodeName(parent, X("SummarySegmentRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegmentRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentGroupType_CollectionPtr tmp = CreateSummarySegmentGroupType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummarySegmentGroupType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SummarySegmentGroup
			Dc1Util::HasNodeName(parent, X("SummarySegmentGroup"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroup")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupRef
			Dc1Util::HasNodeName(parent, X("SummarySegmentGroupRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSummarySegmentGroupType_Collection0Ptr tmp = CreateSummarySegmentGroupType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSummarySegmentGroupType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSummarySegmentGroupType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSummarySegmentGroupType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Name")) == 0))
  {
	Mp7JrsSummarySegmentGroupType_Name_CollectionPtr tmp = CreateSummarySegmentGroupType_Name_CollectionType; // FTT, check this
	this->SetName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Caption")) == 0))
  {
	Mp7JrsSummarySegmentGroupType_Caption_CollectionPtr tmp = CreateSummarySegmentGroupType_Caption_CollectionType; // FTT, check this
	this->SetCaption(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegment"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegment")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegmentRef")) == 0)
	)
  {
	Mp7JrsSummarySegmentGroupType_CollectionPtr tmp = CreateSummarySegmentGroupType_CollectionType; // FTT, check this
	this->SetSummarySegmentGroupType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegmentGroup"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegmentGroup")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegmentGroupRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegmentGroupRef")) == 0)
	)
  {
	Mp7JrsSummarySegmentGroupType_Collection0Ptr tmp = CreateSummarySegmentGroupType_CollectionType0; // FTT, check this
	this->SetSummarySegmentGroupType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSummarySegmentGroupType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetCaption() != Dc1NodePtr())
		result.Insert(GetCaption());
	if (GetSummarySegmentGroupType_LocalType() != Dc1NodePtr())
		result.Insert(GetSummarySegmentGroupType_LocalType());
	if (GetSummarySegmentGroupType_LocalType0() != Dc1NodePtr())
		result.Insert(GetSummarySegmentGroupType_LocalType0());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsSummarySegmentGroupType_ExtMethodImpl.h

#include "Mp7JrsTextualType.h"
#include "Mp7JrsSummarySegmentType.h"
#include "Mp7JrsSummarySegmentGroupType.h"
#include "Mp7JrsSummarySegmentGroupType_LocalType.h"
#include "Mp7JrsSummarySegmentGroupType_LocalType0.h"

#include "Mp7JrsSummarySegmentGroupType_Name_CollectionType.h"
#include "Mp7JrsSummarySegmentGroupType_Caption_CollectionType.h"
#include "Mp7JrsTextualBaseType.h"


void IMp7JrsSummarySegmentGroupType::AddSegment(Mp7JrsSummarySegmentPtr segment, Dc1ClientID client) {
	if (GetSummarySegmentGroupType_LocalType()==NULL) 
		SetSummarySegmentGroupType_LocalType(CreateSummarySegmentGroupType_CollectionType,client);
	Mp7JrsSummarySegmentGroupType_LocalPtr segLocal = CreateSummarySegmentGroupType_LocalType;
	segLocal->SetSummarySegment(segment,client);
	GetSummarySegmentGroupType_LocalType()->addElement(segLocal,client);
}

int IMp7JrsSummarySegmentGroupType::GetNrSegments() {
	if (GetSummarySegmentGroupType_LocalType()==NULL) return 0;
	else return GetSummarySegmentGroupType_LocalType()->size();

}

void IMp7JrsSummarySegmentGroupType::AddSegmentGroup(Mp7JrsSummarySegmentGroupPtr segment, Dc1ClientID client) {
	if (GetSummarySegmentGroupType_LocalType0()==NULL) 
		SetSummarySegmentGroupType_LocalType0(CreateSummarySegmentGroupType_CollectionType0,client);
	Mp7JrsSummarySegmentGroupType_Local0Ptr grpLocal = CreateSummarySegmentGroupType_LocalType0;
	grpLocal->SetSummarySegmentGroup(segment,client);
	GetSummarySegmentGroupType_LocalType0()->addElement(grpLocal,client);
}


int IMp7JrsSummarySegmentGroupType::GetNrSegmentGroups() {
	if (GetSummarySegmentGroupType_LocalType0()==NULL) return 0;
	else return GetSummarySegmentGroupType_LocalType0()->size();
}


void IMp7JrsSummarySegmentGroupType::AddName(XMLCh* name, Dc1ClientID client) {
	Mp7JrsTextualPtr text = CreateTextualType;
	text->SetContent(name,client);
	GetName()->addElement(text);
}

void IMp7JrsSummarySegmentGroupType::AddCaption(XMLCh* caption, Dc1ClientID client) {
	Mp7JrsTextualPtr text = CreateTextualType;
	text->SetContent(caption,client);
	GetCaption()->addElement(text,client);
}

////

void IMp7JrsSummarySegmentGroupType::Setfidelity(float fid, Dc1ClientID client) {
	Getfidelity()->SetContent(fid,client);
}

// end extension included


