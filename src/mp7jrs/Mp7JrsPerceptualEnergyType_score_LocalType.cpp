
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtImplInclude.h


#include "Mp7JrsPerceptualEnergyType_score_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPerceptualEnergyType_score_LocalType::IMp7JrsPerceptualEnergyType_score_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtPropInit.h

}

IMp7JrsPerceptualEnergyType_score_LocalType::~IMp7JrsPerceptualEnergyType_score_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtPropCleanup.h

}

Mp7JrsPerceptualEnergyType_score_LocalType::Mp7JrsPerceptualEnergyType_score_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptualEnergyType_score_LocalType::~Mp7JrsPerceptualEnergyType_score_LocalType()
{
	Cleanup();
}

void Mp7JrsPerceptualEnergyType_score_LocalType::Init()
{
	// Init base
	m_Base = 0;




// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtMyPropInit.h

}

void Mp7JrsPerceptualEnergyType_score_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsPerceptualEnergyType_score_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PerceptualEnergyType_score_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptualEnergyType_score_LocalType
	const Dc1Ptr< Mp7JrsPerceptualEnergyType_score_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
}

Dc1NodePtr Mp7JrsPerceptualEnergyType_score_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPerceptualEnergyType_score_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsPerceptualEnergyType_score_LocalType::SetContent(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptualEnergyType_score_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

int Mp7JrsPerceptualEnergyType_score_LocalType::GetContent() const
{
	return m_Base;
}

Dc1NodeEnum Mp7JrsPerceptualEnergyType_score_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	if (isentrypoint)
	{
		// No Dc1NodePtr subelement to get or create for PerceptualEnergyType_score_LocalType
		Dc1XPathParseContext::ErrorNoSubElement(context, "PerceptualEnergyType_score_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPerceptualEnergyType_score_LocalType::ToText() const
{
	return Dc1Convert::BinToText(m_Base);
}


bool Mp7JrsPerceptualEnergyType_score_LocalType::Parse(const XMLCh * const txt)
{
	m_Base = Dc1Convert::TextToInt(txt);
	return true;
}

bool Mp7JrsPerceptualEnergyType_score_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsPerceptualEnergyType_score_LocalType has no attributes so forward it to Parse()
	return Parse(txt);
}
XMLCh* Mp7JrsPerceptualEnergyType_score_LocalType::ContentToString() const
{
	// Mp7JrsPerceptualEnergyType_score_LocalType has no attributes so forward it to ToText()
	return ToText();
}

void Mp7JrsPerceptualEnergyType_score_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PerceptualEnergyType_score_LocalType"));
	// Element serialization:

}

bool Mp7JrsPerceptualEnergyType_score_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

  m_Base = Dc1Convert::TextToInt(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptualEnergyType_score_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptualEnergyType_score_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptualEnergyType_score_LocalType_ExtMethodImpl.h


