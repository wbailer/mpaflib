
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtImplInclude.h


#include "Mp7JrsMultimediaContentType.h"
#include "Mp7JrsMultimediaCollectionType_Collection_CollectionType.h"
#include "Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsMultimediaCollectionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsCollectionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Collection
#include "Mp7JrsStructuredCollectionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:StructuredCollection

#include <assert.h>
IMp7JrsMultimediaCollectionType::IMp7JrsMultimediaCollectionType()
{

// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtPropInit.h

}

IMp7JrsMultimediaCollectionType::~IMp7JrsMultimediaCollectionType()
{
// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtPropCleanup.h

}

Mp7JrsMultimediaCollectionType::Mp7JrsMultimediaCollectionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMultimediaCollectionType::~Mp7JrsMultimediaCollectionType()
{
	Cleanup();
}

void Mp7JrsMultimediaCollectionType::Init()
{
	// Init base
	m_Base = CreateMultimediaContentType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Collection = Mp7JrsMultimediaCollectionType_Collection_CollectionPtr(); // Collection
	m_StructuredCollection = Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtMyPropInit.h

}

void Mp7JrsMultimediaCollectionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Collection);
	// Dc1Factory::DeleteObject(m_StructuredCollection);
}

void Mp7JrsMultimediaCollectionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MultimediaCollectionTypePtr, since we
	// might need GetBase(), which isn't defined in IMultimediaCollectionType
	const Dc1Ptr< Mp7JrsMultimediaCollectionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMultimediaContentPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsMultimediaCollectionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Collection);
		this->SetCollection(Dc1Factory::CloneObject(tmp->GetCollection()));
		// Dc1Factory::DeleteObject(m_StructuredCollection);
		this->SetStructuredCollection(Dc1Factory::CloneObject(tmp->GetStructuredCollection()));
}

Dc1NodePtr Mp7JrsMultimediaCollectionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsMultimediaCollectionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMultimediaContentType > Mp7JrsMultimediaCollectionType::GetBase() const
{
	return m_Base;
}

Mp7JrsMultimediaCollectionType_Collection_CollectionPtr Mp7JrsMultimediaCollectionType::GetCollection() const
{
		return m_Collection;
}

Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr Mp7JrsMultimediaCollectionType::GetStructuredCollection() const
{
		return m_StructuredCollection;
}

// implementing setter for choice 
/* element */
void Mp7JrsMultimediaCollectionType::SetCollection(const Mp7JrsMultimediaCollectionType_Collection_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SetCollection().");
	}
	if (m_Collection != item)
	{
		// Dc1Factory::DeleteObject(m_Collection);
		m_Collection = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Collection) m_Collection->SetParent(m_myself.getPointer());
		if(m_Collection != Mp7JrsMultimediaCollectionType_Collection_CollectionPtr())
		{
			m_Collection->SetContentName(XMLString::transcode("Collection"));
		}
	// Dc1Factory::DeleteObject(m_StructuredCollection);
	m_StructuredCollection = Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMultimediaCollectionType::SetStructuredCollection(const Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SetStructuredCollection().");
	}
	if (m_StructuredCollection != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredCollection);
		m_StructuredCollection = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredCollection) m_StructuredCollection->SetParent(m_myself.getPointer());
		if(m_StructuredCollection != Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr())
		{
			m_StructuredCollection->SetContentName(XMLString::transcode("StructuredCollection"));
		}
	// Dc1Factory::DeleteObject(m_Collection);
	m_Collection = Mp7JrsMultimediaCollectionType_Collection_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsMultimediaCollectionType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsMultimediaCollectionType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsMultimediaCollectionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultimediaCollectionType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsMultimediaCollectionType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsMultimediaCollectionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsMultimediaCollectionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultimediaCollectionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsMultimediaCollectionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsMultimediaCollectionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsMultimediaCollectionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultimediaCollectionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsMultimediaCollectionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsMultimediaCollectionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsMultimediaCollectionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultimediaCollectionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsMultimediaCollectionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsMultimediaCollectionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsMultimediaCollectionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMultimediaCollectionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsMultimediaCollectionType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsMultimediaCollectionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMultimediaCollectionType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsMultimediaCollectionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Collection")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Collection is item of abstract type CollectionType
		// in element collection MultimediaCollectionType_Collection_CollectionType
		
		context->Found = true;
		Mp7JrsMultimediaCollectionType_Collection_CollectionPtr coll = GetCollection();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMultimediaCollectionType_Collection_CollectionType; // FTT, check this
				SetCollection(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredCollection")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:StructuredCollection is item of type StructuredCollectionType
		// in element collection MultimediaCollectionType_StructuredCollection_CollectionType
		
		context->Found = true;
		Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr coll = GetStructuredCollection();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMultimediaCollectionType_StructuredCollection_CollectionType; // FTT, check this
				SetStructuredCollection(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStructuredCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MultimediaCollectionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MultimediaCollectionType");
	}
	return result;
}

XMLCh * Mp7JrsMultimediaCollectionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsMultimediaCollectionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsMultimediaCollectionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MultimediaCollectionType"));
	// Element serialization:
	if (m_Collection != Mp7JrsMultimediaCollectionType_Collection_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Collection->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_StructuredCollection != Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StructuredCollection->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMultimediaCollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMultimediaContentType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Collection")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMultimediaCollectionType_Collection_CollectionPtr tmp = CreateMultimediaCollectionType_Collection_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCollection(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("StructuredCollection"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("StructuredCollection")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr tmp = CreateMultimediaCollectionType_StructuredCollection_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStructuredCollection(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMultimediaCollectionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Collection")) == 0))
  {
	Mp7JrsMultimediaCollectionType_Collection_CollectionPtr tmp = CreateMultimediaCollectionType_Collection_CollectionType; // FTT, check this
	this->SetCollection(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("StructuredCollection")) == 0))
  {
	Mp7JrsMultimediaCollectionType_StructuredCollection_CollectionPtr tmp = CreateMultimediaCollectionType_StructuredCollection_CollectionType; // FTT, check this
	this->SetStructuredCollection(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMultimediaCollectionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCollection() != Dc1NodePtr())
		result.Insert(GetCollection());
	if (GetStructuredCollection() != Dc1NodePtr())
		result.Insert(GetStructuredCollection());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMultimediaCollectionType_ExtMethodImpl.h


