
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtImplInclude.h


#include "Mp7JrsStructuredAnnotationType_Who_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_WhatObject_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_WhatAction_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_Where_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_When_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_Why_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType_How_CollectionType.h"
#include "Mp7JrsStructuredAnnotationType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTermUseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Who

#include <assert.h>
IMp7JrsStructuredAnnotationType::IMp7JrsStructuredAnnotationType()
{

// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtPropInit.h

}

IMp7JrsStructuredAnnotationType::~IMp7JrsStructuredAnnotationType()
{
// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtPropCleanup.h

}

Mp7JrsStructuredAnnotationType::Mp7JrsStructuredAnnotationType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStructuredAnnotationType::~Mp7JrsStructuredAnnotationType()
{
	Cleanup();
}

void Mp7JrsStructuredAnnotationType::Init()
{

	// Init attributes
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Who = Mp7JrsStructuredAnnotationType_Who_CollectionPtr(); // Collection
	m_WhatObject = Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr(); // Collection
	m_WhatAction = Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr(); // Collection
	m_Where = Mp7JrsStructuredAnnotationType_Where_CollectionPtr(); // Collection
	m_When = Mp7JrsStructuredAnnotationType_When_CollectionPtr(); // Collection
	m_Why = Mp7JrsStructuredAnnotationType_Why_CollectionPtr(); // Collection
	m_How = Mp7JrsStructuredAnnotationType_How_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtMyPropInit.h

}

void Mp7JrsStructuredAnnotationType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtMyPropCleanup.h


	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_Who);
	// Dc1Factory::DeleteObject(m_WhatObject);
	// Dc1Factory::DeleteObject(m_WhatAction);
	// Dc1Factory::DeleteObject(m_Where);
	// Dc1Factory::DeleteObject(m_When);
	// Dc1Factory::DeleteObject(m_Why);
	// Dc1Factory::DeleteObject(m_How);
}

void Mp7JrsStructuredAnnotationType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StructuredAnnotationTypePtr, since we
	// might need GetBase(), which isn't defined in IStructuredAnnotationType
	const Dc1Ptr< Mp7JrsStructuredAnnotationType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_Who);
		this->SetWho(Dc1Factory::CloneObject(tmp->GetWho()));
		// Dc1Factory::DeleteObject(m_WhatObject);
		this->SetWhatObject(Dc1Factory::CloneObject(tmp->GetWhatObject()));
		// Dc1Factory::DeleteObject(m_WhatAction);
		this->SetWhatAction(Dc1Factory::CloneObject(tmp->GetWhatAction()));
		// Dc1Factory::DeleteObject(m_Where);
		this->SetWhere(Dc1Factory::CloneObject(tmp->GetWhere()));
		// Dc1Factory::DeleteObject(m_When);
		this->SetWhen(Dc1Factory::CloneObject(tmp->GetWhen()));
		// Dc1Factory::DeleteObject(m_Why);
		this->SetWhy(Dc1Factory::CloneObject(tmp->GetWhy()));
		// Dc1Factory::DeleteObject(m_How);
		this->SetHow(Dc1Factory::CloneObject(tmp->GetHow()));
}

Dc1NodePtr Mp7JrsStructuredAnnotationType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsStructuredAnnotationType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsStructuredAnnotationType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsStructuredAnnotationType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsStructuredAnnotationType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsStructuredAnnotationType_Who_CollectionPtr Mp7JrsStructuredAnnotationType::GetWho() const
{
		return m_Who;
}

Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr Mp7JrsStructuredAnnotationType::GetWhatObject() const
{
		return m_WhatObject;
}

Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr Mp7JrsStructuredAnnotationType::GetWhatAction() const
{
		return m_WhatAction;
}

Mp7JrsStructuredAnnotationType_Where_CollectionPtr Mp7JrsStructuredAnnotationType::GetWhere() const
{
		return m_Where;
}

Mp7JrsStructuredAnnotationType_When_CollectionPtr Mp7JrsStructuredAnnotationType::GetWhen() const
{
		return m_When;
}

Mp7JrsStructuredAnnotationType_Why_CollectionPtr Mp7JrsStructuredAnnotationType::GetWhy() const
{
		return m_Why;
}

Mp7JrsStructuredAnnotationType_How_CollectionPtr Mp7JrsStructuredAnnotationType::GetHow() const
{
		return m_How;
}

void Mp7JrsStructuredAnnotationType::SetWho(const Mp7JrsStructuredAnnotationType_Who_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWho().");
	}
	if (m_Who != item)
	{
		// Dc1Factory::DeleteObject(m_Who);
		m_Who = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Who) m_Who->SetParent(m_myself.getPointer());
		if(m_Who != Mp7JrsStructuredAnnotationType_Who_CollectionPtr())
		{
			m_Who->SetContentName(XMLString::transcode("Who"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetWhatObject(const Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWhatObject().");
	}
	if (m_WhatObject != item)
	{
		// Dc1Factory::DeleteObject(m_WhatObject);
		m_WhatObject = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WhatObject) m_WhatObject->SetParent(m_myself.getPointer());
		if(m_WhatObject != Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr())
		{
			m_WhatObject->SetContentName(XMLString::transcode("WhatObject"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetWhatAction(const Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWhatAction().");
	}
	if (m_WhatAction != item)
	{
		// Dc1Factory::DeleteObject(m_WhatAction);
		m_WhatAction = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_WhatAction) m_WhatAction->SetParent(m_myself.getPointer());
		if(m_WhatAction != Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr())
		{
			m_WhatAction->SetContentName(XMLString::transcode("WhatAction"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetWhere(const Mp7JrsStructuredAnnotationType_Where_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWhere().");
	}
	if (m_Where != item)
	{
		// Dc1Factory::DeleteObject(m_Where);
		m_Where = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Where) m_Where->SetParent(m_myself.getPointer());
		if(m_Where != Mp7JrsStructuredAnnotationType_Where_CollectionPtr())
		{
			m_Where->SetContentName(XMLString::transcode("Where"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetWhen(const Mp7JrsStructuredAnnotationType_When_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWhen().");
	}
	if (m_When != item)
	{
		// Dc1Factory::DeleteObject(m_When);
		m_When = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_When) m_When->SetParent(m_myself.getPointer());
		if(m_When != Mp7JrsStructuredAnnotationType_When_CollectionPtr())
		{
			m_When->SetContentName(XMLString::transcode("When"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetWhy(const Mp7JrsStructuredAnnotationType_Why_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetWhy().");
	}
	if (m_Why != item)
	{
		// Dc1Factory::DeleteObject(m_Why);
		m_Why = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Why) m_Why->SetParent(m_myself.getPointer());
		if(m_Why != Mp7JrsStructuredAnnotationType_Why_CollectionPtr())
		{
			m_Why->SetContentName(XMLString::transcode("Why"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredAnnotationType::SetHow(const Mp7JrsStructuredAnnotationType_How_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredAnnotationType::SetHow().");
	}
	if (m_How != item)
	{
		// Dc1Factory::DeleteObject(m_How);
		m_How = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_How) m_How->SetParent(m_myself.getPointer());
		if(m_How != Mp7JrsStructuredAnnotationType_How_CollectionPtr())
		{
			m_How->SetContentName(XMLString::transcode("How"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStructuredAnnotationType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Who")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Who is item of type TermUseType
		// in element collection StructuredAnnotationType_Who_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_Who_CollectionPtr coll = GetWho();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_Who_CollectionType; // FTT, check this
				SetWho(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("WhatObject")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:WhatObject is item of type TermUseType
		// in element collection StructuredAnnotationType_WhatObject_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr coll = GetWhatObject();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_WhatObject_CollectionType; // FTT, check this
				SetWhatObject(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("WhatAction")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:WhatAction is item of type TermUseType
		// in element collection StructuredAnnotationType_WhatAction_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr coll = GetWhatAction();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_WhatAction_CollectionType; // FTT, check this
				SetWhatAction(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Where")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Where is item of type TermUseType
		// in element collection StructuredAnnotationType_Where_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_Where_CollectionPtr coll = GetWhere();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_Where_CollectionType; // FTT, check this
				SetWhere(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("When")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:When is item of type TermUseType
		// in element collection StructuredAnnotationType_When_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_When_CollectionPtr coll = GetWhen();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_When_CollectionType; // FTT, check this
				SetWhen(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Why")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Why is item of type TermUseType
		// in element collection StructuredAnnotationType_Why_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_Why_CollectionPtr coll = GetWhy();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_Why_CollectionType; // FTT, check this
				SetWhy(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("How")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:How is item of type TermUseType
		// in element collection StructuredAnnotationType_How_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredAnnotationType_How_CollectionPtr coll = GetHow();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredAnnotationType_How_CollectionType; // FTT, check this
				SetHow(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StructuredAnnotationType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StructuredAnnotationType");
	}
	return result;
}

XMLCh * Mp7JrsStructuredAnnotationType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsStructuredAnnotationType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsStructuredAnnotationType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StructuredAnnotationType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Who != Mp7JrsStructuredAnnotationType_Who_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Who->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_WhatObject != Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_WhatObject->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_WhatAction != Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_WhatAction->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Where != Mp7JrsStructuredAnnotationType_Where_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Where->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_When != Mp7JrsStructuredAnnotationType_When_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_When->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Why != Mp7JrsStructuredAnnotationType_Why_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Why->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_How != Mp7JrsStructuredAnnotationType_How_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_How->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsStructuredAnnotationType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Who"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Who")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_Who_CollectionPtr tmp = CreateStructuredAnnotationType_Who_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWho(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("WhatObject"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("WhatObject")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr tmp = CreateStructuredAnnotationType_WhatObject_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWhatObject(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("WhatAction"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("WhatAction")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr tmp = CreateStructuredAnnotationType_WhatAction_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWhatAction(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Where"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Where")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_Where_CollectionPtr tmp = CreateStructuredAnnotationType_Where_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWhere(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("When"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("When")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_When_CollectionPtr tmp = CreateStructuredAnnotationType_When_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWhen(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Why"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Why")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_Why_CollectionPtr tmp = CreateStructuredAnnotationType_Why_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetWhy(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("How"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("How")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredAnnotationType_How_CollectionPtr tmp = CreateStructuredAnnotationType_How_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetHow(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStructuredAnnotationType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Who")) == 0))
  {
	Mp7JrsStructuredAnnotationType_Who_CollectionPtr tmp = CreateStructuredAnnotationType_Who_CollectionType; // FTT, check this
	this->SetWho(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("WhatObject")) == 0))
  {
	Mp7JrsStructuredAnnotationType_WhatObject_CollectionPtr tmp = CreateStructuredAnnotationType_WhatObject_CollectionType; // FTT, check this
	this->SetWhatObject(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("WhatAction")) == 0))
  {
	Mp7JrsStructuredAnnotationType_WhatAction_CollectionPtr tmp = CreateStructuredAnnotationType_WhatAction_CollectionType; // FTT, check this
	this->SetWhatAction(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Where")) == 0))
  {
	Mp7JrsStructuredAnnotationType_Where_CollectionPtr tmp = CreateStructuredAnnotationType_Where_CollectionType; // FTT, check this
	this->SetWhere(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("When")) == 0))
  {
	Mp7JrsStructuredAnnotationType_When_CollectionPtr tmp = CreateStructuredAnnotationType_When_CollectionType; // FTT, check this
	this->SetWhen(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Why")) == 0))
  {
	Mp7JrsStructuredAnnotationType_Why_CollectionPtr tmp = CreateStructuredAnnotationType_Why_CollectionType; // FTT, check this
	this->SetWhy(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("How")) == 0))
  {
	Mp7JrsStructuredAnnotationType_How_CollectionPtr tmp = CreateStructuredAnnotationType_How_CollectionType; // FTT, check this
	this->SetHow(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStructuredAnnotationType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetWho() != Dc1NodePtr())
		result.Insert(GetWho());
	if (GetWhatObject() != Dc1NodePtr())
		result.Insert(GetWhatObject());
	if (GetWhatAction() != Dc1NodePtr())
		result.Insert(GetWhatAction());
	if (GetWhere() != Dc1NodePtr())
		result.Insert(GetWhere());
	if (GetWhen() != Dc1NodePtr())
		result.Insert(GetWhen());
	if (GetWhy() != Dc1NodePtr())
		result.Insert(GetWhy());
	if (GetHow() != Dc1NodePtr())
		result.Insert(GetHow());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStructuredAnnotationType_ExtMethodImpl.h


