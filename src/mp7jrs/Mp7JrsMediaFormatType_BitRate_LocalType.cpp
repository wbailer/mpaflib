
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtImplInclude.h


#include "Mp7JrsMediaFormatType_BitRate_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaFormatType_BitRate_LocalType::IMp7JrsMediaFormatType_BitRate_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtPropInit.h

}

IMp7JrsMediaFormatType_BitRate_LocalType::~IMp7JrsMediaFormatType_BitRate_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaFormatType_BitRate_LocalType::Mp7JrsMediaFormatType_BitRate_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaFormatType_BitRate_LocalType::~Mp7JrsMediaFormatType_BitRate_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Init()
{
	// Init base
	m_Base = 0;

	// Init attributes
	m_variable = false; // Value
	m_variable_Default = false; // Default value
	m_variable_Exist = false;
	m_minimum = 0; // Value
	m_minimum_Exist = false;
	m_average = 0; // Value
	m_average_Exist = false;
	m_maximum = 0; // Value
	m_maximum_Exist = false;



// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaFormatType_BitRate_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtMyPropCleanup.h


}

void Mp7JrsMediaFormatType_BitRate_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaFormatType_BitRate_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaFormatType_BitRate_LocalType
	const Dc1Ptr< Mp7JrsMediaFormatType_BitRate_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	m_Base = tmp->GetContent();
	{
	if (tmp->Existvariable())
	{
		this->Setvariable(tmp->Getvariable());
	}
	else
	{
		Invalidatevariable();
	}
	}
	{
	if (tmp->Existminimum())
	{
		this->Setminimum(tmp->Getminimum());
	}
	else
	{
		Invalidateminimum();
	}
	}
	{
	if (tmp->Existaverage())
	{
		this->Setaverage(tmp->Getaverage());
	}
	else
	{
		Invalidateaverage();
	}
	}
	{
	if (tmp->Existmaximum())
	{
		this->Setmaximum(tmp->Getmaximum());
	}
	else
	{
		Invalidatemaximum();
	}
	}
}

Dc1NodePtr Mp7JrsMediaFormatType_BitRate_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaFormatType_BitRate_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


void Mp7JrsMediaFormatType_BitRate_LocalType::SetContent(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_BitRate_LocalType::SetContent().");
	}
	if (m_Base != item)
	{
		m_Base = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

unsigned Mp7JrsMediaFormatType_BitRate_LocalType::GetContent() const
{
	return m_Base;
}
bool Mp7JrsMediaFormatType_BitRate_LocalType::Getvariable() const
{
	if (this->Existvariable()) {
		return m_variable;
	} else {
		return m_variable_Default;
	}
}

bool Mp7JrsMediaFormatType_BitRate_LocalType::Existvariable() const
{
	return m_variable_Exist;
}
void Mp7JrsMediaFormatType_BitRate_LocalType::Setvariable(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_BitRate_LocalType::Setvariable().");
	}
	m_variable = item;
	m_variable_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Invalidatevariable()
{
	m_variable_Exist = false;
}
unsigned Mp7JrsMediaFormatType_BitRate_LocalType::Getminimum() const
{
	return m_minimum;
}

bool Mp7JrsMediaFormatType_BitRate_LocalType::Existminimum() const
{
	return m_minimum_Exist;
}
void Mp7JrsMediaFormatType_BitRate_LocalType::Setminimum(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_BitRate_LocalType::Setminimum().");
	}
	m_minimum = item;
	m_minimum_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Invalidateminimum()
{
	m_minimum_Exist = false;
}
unsigned Mp7JrsMediaFormatType_BitRate_LocalType::Getaverage() const
{
	return m_average;
}

bool Mp7JrsMediaFormatType_BitRate_LocalType::Existaverage() const
{
	return m_average_Exist;
}
void Mp7JrsMediaFormatType_BitRate_LocalType::Setaverage(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_BitRate_LocalType::Setaverage().");
	}
	m_average = item;
	m_average_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Invalidateaverage()
{
	m_average_Exist = false;
}
unsigned Mp7JrsMediaFormatType_BitRate_LocalType::Getmaximum() const
{
	return m_maximum;
}

bool Mp7JrsMediaFormatType_BitRate_LocalType::Existmaximum() const
{
	return m_maximum_Exist;
}
void Mp7JrsMediaFormatType_BitRate_LocalType::Setmaximum(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_BitRate_LocalType::Setmaximum().");
	}
	m_maximum = item;
	m_maximum_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Invalidatemaximum()
{
	m_maximum_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaFormatType_BitRate_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("variable")) == 0)
	{
		// variable is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("minimum")) == 0)
	{
		// minimum is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("average")) == 0)
	{
		// average is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("maximum")) == 0)
	{
		// maximum is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaFormatType_BitRate_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaFormatType_BitRate_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMediaFormatType_BitRate_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaFormatType_BitRate_LocalType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}

bool Mp7JrsMediaFormatType_BitRate_LocalType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	// Mp7JrsMediaFormatType_BitRate_LocalType with workaround via Deserialise()
	return Dc1XPathParseContext::ContentFromString(this, txt);
}
XMLCh* Mp7JrsMediaFormatType_BitRate_LocalType::ContentToString() const
{
	// Mp7JrsMediaFormatType_BitRate_LocalType with workaround via Serialise()
	return Dc1XPathParseContext::ContentToString(this);
}

void Mp7JrsMediaFormatType_BitRate_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	{
		XMLCh * tmp = Dc1Convert::BinToText(m_Base);
		element->appendChild(doc->createTextNode(tmp));
		XMLString::release(&tmp);
	}

	assert(element);
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormatType_BitRate_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_variable_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_variable);
		element->setAttributeNS(X(""), X("variable"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_minimum_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_minimum);
		element->setAttributeNS(X(""), X("minimum"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_average_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_average);
		element->setAttributeNS(X(""), X("average"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_maximum_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_maximum);
		element->setAttributeNS(X(""), X("maximum"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsMediaFormatType_BitRate_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("variable")))
	{
		// deserialize value type
		this->Setvariable(Dc1Convert::TextToBool(parent->getAttribute(X("variable"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("minimum")))
	{
		// deserialize value type
		this->Setminimum(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("minimum"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("average")))
	{
		// deserialize value type
		this->Setaverage(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("average"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("maximum")))
	{
		// deserialize value type
		this->Setmaximum(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("maximum"))));
		* current = parent;
	}

  m_Base = Dc1Convert::TextToUnsignedInt(Dc1Util::GetElementText(parent));
  found = true;
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaFormatType_BitRate_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaFormatType_BitRate_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_BitRate_LocalType_ExtMethodImpl.h


