
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtImplInclude.h


#include "Mp7JrsHeaderType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsIntegerMatrixType.h"
#include "Mp7JrsConfusionCountType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsConfusionCountType::IMp7JrsConfusionCountType()
{

// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtPropInit.h

}

IMp7JrsConfusionCountType::~IMp7JrsConfusionCountType()
{
// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtPropCleanup.h

}

Mp7JrsConfusionCountType::Mp7JrsConfusionCountType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsConfusionCountType::~Mp7JrsConfusionCountType()
{
	Cleanup();
}

void Mp7JrsConfusionCountType::Init()
{
	// Init base
	m_Base = CreateHeaderType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_numOfDimensions = 1; // Value

	// Init elements (element, union sequence choice all any)
	
	m_Insertion = Mp7JrsintegerVectorPtr(); // Collection
	m_Deletion = Mp7JrsintegerVectorPtr(); // Collection
	m_Substitution = Mp7JrsIntegerMatrixPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtMyPropInit.h

}

void Mp7JrsConfusionCountType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Insertion);
	// Dc1Factory::DeleteObject(m_Deletion);
	// Dc1Factory::DeleteObject(m_Substitution);
}

void Mp7JrsConfusionCountType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ConfusionCountTypePtr, since we
	// might need GetBase(), which isn't defined in IConfusionCountType
	const Dc1Ptr< Mp7JrsConfusionCountType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsHeaderPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsConfusionCountType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->SetnumOfDimensions(tmp->GetnumOfDimensions());
	}
		// Dc1Factory::DeleteObject(m_Insertion);
		this->SetInsertion(Dc1Factory::CloneObject(tmp->GetInsertion()));
		// Dc1Factory::DeleteObject(m_Deletion);
		this->SetDeletion(Dc1Factory::CloneObject(tmp->GetDeletion()));
		// Dc1Factory::DeleteObject(m_Substitution);
		this->SetSubstitution(Dc1Factory::CloneObject(tmp->GetSubstitution()));
}

Dc1NodePtr Mp7JrsConfusionCountType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsConfusionCountType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsHeaderType > Mp7JrsConfusionCountType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsConfusionCountType::GetnumOfDimensions() const
{
	return m_numOfDimensions;
}

void Mp7JrsConfusionCountType::SetnumOfDimensions(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConfusionCountType::SetnumOfDimensions().");
	}
	m_numOfDimensions = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsConfusionCountType::GetInsertion() const
{
		return m_Insertion;
}

Mp7JrsintegerVectorPtr Mp7JrsConfusionCountType::GetDeletion() const
{
		return m_Deletion;
}

Mp7JrsIntegerMatrixPtr Mp7JrsConfusionCountType::GetSubstitution() const
{
		return m_Substitution;
}

void Mp7JrsConfusionCountType::SetInsertion(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConfusionCountType::SetInsertion().");
	}
	if (m_Insertion != item)
	{
		// Dc1Factory::DeleteObject(m_Insertion);
		m_Insertion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Insertion) m_Insertion->SetParent(m_myself.getPointer());
		if(m_Insertion != Mp7JrsintegerVectorPtr())
		{
			m_Insertion->SetContentName(XMLString::transcode("Insertion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConfusionCountType::SetDeletion(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConfusionCountType::SetDeletion().");
	}
	if (m_Deletion != item)
	{
		// Dc1Factory::DeleteObject(m_Deletion);
		m_Deletion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Deletion) m_Deletion->SetParent(m_myself.getPointer());
		if(m_Deletion != Mp7JrsintegerVectorPtr())
		{
			m_Deletion->SetContentName(XMLString::transcode("Deletion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConfusionCountType::SetSubstitution(const Mp7JrsIntegerMatrixPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConfusionCountType::SetSubstitution().");
	}
	if (m_Substitution != item)
	{
		// Dc1Factory::DeleteObject(m_Substitution);
		m_Substitution = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Substitution) m_Substitution->SetParent(m_myself.getPointer());
		if (m_Substitution && m_Substitution->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Substitution->UseTypeAttribute = true;
		}
		if(m_Substitution != Mp7JrsIntegerMatrixPtr())
		{
			m_Substitution->SetContentName(XMLString::transcode("Substitution"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsConfusionCountType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsConfusionCountType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsConfusionCountType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsConfusionCountType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsConfusionCountType::Invalidateid()
{
	GetBase()->Invalidateid();
}

Dc1NodeEnum Mp7JrsConfusionCountType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfDimensions")) == 0)
	{
		// numOfDimensions is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Insertion")) == 0)
	{
		// Insertion is simple element integerVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInsertion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:integerVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsintegerVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInsertion(p, client);
					if((p = GetInsertion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Deletion")) == 0)
	{
		// Deletion is simple element integerVector
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDeletion()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:integerVector")))) != empty)
			{
				// Is type allowed
				Mp7JrsintegerVectorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDeletion(p, client);
					if((p = GetDeletion()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Substitution")) == 0)
	{
		// Substitution is simple element IntegerMatrixType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSubstitution()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntegerMatrixType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIntegerMatrixPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSubstitution(p, client);
					if((p = GetSubstitution()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ConfusionCountType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ConfusionCountType");
	}
	return result;
}

XMLCh * Mp7JrsConfusionCountType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsConfusionCountType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsConfusionCountType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ConfusionCountType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_numOfDimensions);
		element->setAttributeNS(X(""), X("numOfDimensions"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	if (m_Insertion != Mp7JrsintegerVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Insertion->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Deletion != Mp7JrsintegerVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Deletion->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_Substitution != Mp7JrsIntegerMatrixPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Substitution->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Substitution"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Substitution->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsConfusionCountType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfDimensions")))
	{
		// deserialize value type
		this->SetnumOfDimensions(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("numOfDimensions"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsHeaderType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Insertion")) == 0))
		{
			// Deserialize factory type
			Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInsertion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("Deletion")) == 0))
		{
			// Deserialize factory type
			Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetDeletion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Substitution"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Substitution")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIntegerMatrixType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSubstitution(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsConfusionCountType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Insertion")) == 0))
  {
	Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
	this->SetInsertion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Deletion")) == 0))
  {
	Mp7JrsintegerVectorPtr tmp = CreateintegerVector; // FTT, check this
	this->SetDeletion(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("Substitution")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIntegerMatrixType; // FTT, check this
	}
	this->SetSubstitution(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsConfusionCountType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInsertion() != Dc1NodePtr())
		result.Insert(GetInsertion());
	if (GetDeletion() != Dc1NodePtr())
		result.Insert(GetDeletion());
	if (GetSubstitution() != Dc1NodePtr())
		result.Insert(GetSubstitution());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsConfusionCountType_ExtMethodImpl.h


