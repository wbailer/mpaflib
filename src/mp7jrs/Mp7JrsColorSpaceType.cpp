
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtImplInclude.h


#include "Mp7JrsColorSpaceType_ColorTransMat_LocalType.h"
#include "Mp7JrsColorSpaceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsColorSpaceType::IMp7JrsColorSpaceType()
{

// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtPropInit.h

}

IMp7JrsColorSpaceType::~IMp7JrsColorSpaceType()
{
// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtPropCleanup.h

}

Mp7JrsColorSpaceType::Mp7JrsColorSpaceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsColorSpaceType::~Mp7JrsColorSpaceType()
{
	Cleanup();
}

void Mp7JrsColorSpaceType::Init()
{

	// Init attributes
	m_colorReferenceFlag = false; // Value
	m_colorReferenceFlag_Default = false; // Default value
	m_colorReferenceFlag_Exist = false;
	m_type = Mp7JrsColorSpaceType_type_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_ColorTransMat = Mp7JrsColorSpaceType_ColorTransMat_LocalPtr(); // Class
	m_ColorTransMat_Exist = false;


// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtMyPropInit.h

}

void Mp7JrsColorSpaceType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_ColorTransMat);
}

void Mp7JrsColorSpaceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ColorSpaceTypePtr, since we
	// might need GetBase(), which isn't defined in IColorSpaceType
	const Dc1Ptr< Mp7JrsColorSpaceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	if (tmp->ExistcolorReferenceFlag())
	{
		this->SetcolorReferenceFlag(tmp->GetcolorReferenceFlag());
	}
	else
	{
		InvalidatecolorReferenceFlag();
	}
	}
	{
		this->Settype(tmp->Gettype());
	}
	if (tmp->IsValidColorTransMat())
	{
		// Dc1Factory::DeleteObject(m_ColorTransMat);
		this->SetColorTransMat(Dc1Factory::CloneObject(tmp->GetColorTransMat()));
	}
	else
	{
		InvalidateColorTransMat();
	}
}

Dc1NodePtr Mp7JrsColorSpaceType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsColorSpaceType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


bool Mp7JrsColorSpaceType::GetcolorReferenceFlag() const
{
	if (this->ExistcolorReferenceFlag()) {
		return m_colorReferenceFlag;
	} else {
		return m_colorReferenceFlag_Default;
	}
}

bool Mp7JrsColorSpaceType::ExistcolorReferenceFlag() const
{
	return m_colorReferenceFlag_Exist;
}
void Mp7JrsColorSpaceType::SetcolorReferenceFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSpaceType::SetcolorReferenceFlag().");
	}
	m_colorReferenceFlag = item;
	m_colorReferenceFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSpaceType::InvalidatecolorReferenceFlag()
{
	m_colorReferenceFlag_Exist = false;
}
Mp7JrsColorSpaceType_type_LocalType::Enumeration Mp7JrsColorSpaceType::Gettype() const
{
	return m_type;
}

void Mp7JrsColorSpaceType::Settype(Mp7JrsColorSpaceType_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSpaceType::Settype().");
	}
	m_type = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsColorSpaceType_ColorTransMat_LocalPtr Mp7JrsColorSpaceType::GetColorTransMat() const
{
		return m_ColorTransMat;
}

// Element is optional
bool Mp7JrsColorSpaceType::IsValidColorTransMat() const
{
	return m_ColorTransMat_Exist;
}

void Mp7JrsColorSpaceType::SetColorTransMat(const Mp7JrsColorSpaceType_ColorTransMat_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsColorSpaceType::SetColorTransMat().");
	}
	if (m_ColorTransMat != item || m_ColorTransMat_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorTransMat);
		m_ColorTransMat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorTransMat) m_ColorTransMat->SetParent(m_myself.getPointer());
		if (m_ColorTransMat && m_ColorTransMat->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType_ColorTransMat_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorTransMat->UseTypeAttribute = true;
		}
		m_ColorTransMat_Exist = true;
		if(m_ColorTransMat != Mp7JrsColorSpaceType_ColorTransMat_LocalPtr())
		{
			m_ColorTransMat->SetContentName(XMLString::transcode("ColorTransMat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsColorSpaceType::InvalidateColorTransMat()
{
	m_ColorTransMat_Exist = false;
}

Dc1NodeEnum Mp7JrsColorSpaceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 2 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("colorReferenceFlag")) == 0)
	{
		// colorReferenceFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute ColorSpaceType_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsColorSpaceType_type_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ColorTransMat")) == 0)
	{
		// ColorTransMat is simple element ColorSpaceType_ColorTransMat_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorTransMat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType_ColorTransMat_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorSpaceType_ColorTransMat_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorTransMat(p, client);
					if((p = GetColorTransMat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ColorSpaceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ColorSpaceType");
	}
	return result;
}

XMLCh * Mp7JrsColorSpaceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsColorSpaceType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsColorSpaceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ColorSpaceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_colorReferenceFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_colorReferenceFlag);
		element->setAttributeNS(X(""), X("colorReferenceFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
	// Enumeration
	if(m_type != Mp7JrsColorSpaceType_type_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsColorSpaceType_type_LocalType::ToText(m_type);
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_ColorTransMat != Mp7JrsColorSpaceType_ColorTransMat_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorTransMat->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorTransMat"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorTransMat->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsColorSpaceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("colorReferenceFlag")))
	{
		// deserialize value type
		this->SetcolorReferenceFlag(Dc1Convert::TextToBool(parent->getAttribute(X("colorReferenceFlag"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		this->Settype(Mp7JrsColorSpaceType_type_LocalType::Parse(parent->getAttribute(X("type"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorTransMat"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorTransMat")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorSpaceType_ColorTransMat_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorTransMat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsColorSpaceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("ColorTransMat")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorSpaceType_ColorTransMat_LocalType; // FTT, check this
	}
	this->SetColorTransMat(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsColorSpaceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetColorTransMat() != Dc1NodePtr())
		result.Insert(GetColorTransMat());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsColorSpaceType_ExtMethodImpl.h


