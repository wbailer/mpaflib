
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtImplInclude.h


#include "Mp7JrsMediaReviewDescriptionType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsKeywordAnnotationType.h"
#include "Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType.h"
#include "Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionType.h"
#include "Mp7JrsRatingType.h"
#include "Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsIndividualMediaReviewDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FreeTextReview
#include "Mp7JrsMediaQualityType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QualityRating

#include <assert.h>
IMp7JrsIndividualMediaReviewDescriptionType::IMp7JrsIndividualMediaReviewDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtPropInit.h

}

IMp7JrsIndividualMediaReviewDescriptionType::~IMp7JrsIndividualMediaReviewDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtPropCleanup.h

}

Mp7JrsIndividualMediaReviewDescriptionType::Mp7JrsIndividualMediaReviewDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsIndividualMediaReviewDescriptionType::~Mp7JrsIndividualMediaReviewDescriptionType()
{
	Cleanup();
}

void Mp7JrsIndividualMediaReviewDescriptionType::Init()
{
	// Init base
	m_Base = CreateMediaReviewDescriptionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Reviewer = Dc1Ptr< Dc1Node >(); // Class
	m_ReviewerRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtMyPropInit.h

}

void Mp7JrsIndividualMediaReviewDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Reviewer);
	// Dc1Factory::DeleteObject(m_ReviewerRef);
}

void Mp7JrsIndividualMediaReviewDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use IndividualMediaReviewDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IIndividualMediaReviewDescriptionType
	const Dc1Ptr< Mp7JrsIndividualMediaReviewDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaReviewDescriptionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsIndividualMediaReviewDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_Reviewer);
		this->SetReviewer(Dc1Factory::CloneObject(tmp->GetReviewer()));
		// Dc1Factory::DeleteObject(m_ReviewerRef);
		this->SetReviewerRef(Dc1Factory::CloneObject(tmp->GetReviewerRef()));
}

Dc1NodePtr Mp7JrsIndividualMediaReviewDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsIndividualMediaReviewDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMediaReviewDescriptionType > Mp7JrsIndividualMediaReviewDescriptionType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Dc1Node > Mp7JrsIndividualMediaReviewDescriptionType::GetReviewer() const
{
		return m_Reviewer;
}

Mp7JrsReferencePtr Mp7JrsIndividualMediaReviewDescriptionType::GetReviewerRef() const
{
		return m_ReviewerRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsIndividualMediaReviewDescriptionType::SetReviewer(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetReviewer().");
	}
	if (m_Reviewer != item)
	{
		// Dc1Factory::DeleteObject(m_Reviewer);
		m_Reviewer = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Reviewer) m_Reviewer->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Reviewer) m_Reviewer->UseTypeAttribute = true;
		if(m_Reviewer != Dc1Ptr< Dc1Node >())
		{
			m_Reviewer->SetContentName(XMLString::transcode("Reviewer"));
		}
	// Dc1Factory::DeleteObject(m_ReviewerRef);
	m_ReviewerRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsIndividualMediaReviewDescriptionType::SetReviewerRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetReviewerRef().");
	}
	if (m_ReviewerRef != item)
	{
		// Dc1Factory::DeleteObject(m_ReviewerRef);
		m_ReviewerRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ReviewerRef) m_ReviewerRef->SetParent(m_myself.getPointer());
		if (m_ReviewerRef && m_ReviewerRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ReviewerRef->UseTypeAttribute = true;
		}
		if(m_ReviewerRef != Mp7JrsReferencePtr())
		{
			m_ReviewerRef->SetContentName(XMLString::transcode("ReviewerRef"));
		}
	// Dc1Factory::DeleteObject(m_Reviewer);
	m_Reviewer = Dc1Ptr< Dc1Node >();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrstimePointPtr Mp7JrsIndividualMediaReviewDescriptionType::GetreviewTime() const
{
	return GetBase()->GetreviewTime();
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetreviewTime(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetreviewTime().");
	}
	GetBase()->SetreviewTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsReferencePtr Mp7JrsIndividualMediaReviewDescriptionType::GetContentRef() const
{
	return GetBase()->GetContentRef();
}

Mp7JrsKeywordAnnotationPtr Mp7JrsIndividualMediaReviewDescriptionType::GetTags() const
{
	return GetBase()->GetTags();
}

// Element is optional
bool Mp7JrsIndividualMediaReviewDescriptionType::IsValidTags() const
{
	return GetBase()->IsValidTags();
}

Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr Mp7JrsIndividualMediaReviewDescriptionType::GetFreeTextReview() const
{
	return GetBase()->GetFreeTextReview();
}

Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr Mp7JrsIndividualMediaReviewDescriptionType::GetMediaRating() const
{
	return GetBase()->GetMediaRating();
}

Mp7JrsRatingPtr Mp7JrsIndividualMediaReviewDescriptionType::GetIdentityRating() const
{
	return GetBase()->GetIdentityRating();
}

// Element is optional
bool Mp7JrsIndividualMediaReviewDescriptionType::IsValidIdentityRating() const
{
	return GetBase()->IsValidIdentityRating();
}

Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr Mp7JrsIndividualMediaReviewDescriptionType::GetQualityRating() const
{
	return GetBase()->GetQualityRating();
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetContentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetContentRef().");
	}
	GetBase()->SetContentRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetTags(const Mp7JrsKeywordAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetTags().");
	}
	GetBase()->SetTags(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::InvalidateTags()
{
	GetBase()->InvalidateTags();
}
void Mp7JrsIndividualMediaReviewDescriptionType::SetFreeTextReview(const Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetFreeTextReview().");
	}
	GetBase()->SetFreeTextReview(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetMediaRating(const Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetMediaRating().");
	}
	GetBase()->SetMediaRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetIdentityRating(const Mp7JrsRatingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetIdentityRating().");
	}
	GetBase()->SetIdentityRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::InvalidateIdentityRating()
{
	GetBase()->InvalidateIdentityRating();
}
void Mp7JrsIndividualMediaReviewDescriptionType::SetQualityRating(const Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetQualityRating().");
	}
	GetBase()->SetQualityRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsIndividualMediaReviewDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsIndividualMediaReviewDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsIndividualMediaReviewDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsIndividualMediaReviewDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsIndividualMediaReviewDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsIndividualMediaReviewDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsIndividualMediaReviewDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsIndividualMediaReviewDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Reviewer")) == 0)
	{
		// Reviewer is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetReviewer()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetReviewer(p, client);
					if((p = GetReviewer()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ReviewerRef")) == 0)
	{
		// ReviewerRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetReviewerRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetReviewerRef(p, client);
					if((p = GetReviewerRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for IndividualMediaReviewDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "IndividualMediaReviewDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsIndividualMediaReviewDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsIndividualMediaReviewDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsIndividualMediaReviewDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("IndividualMediaReviewDescriptionType"));
	// Element serialization:
	// Class
	
	if (m_Reviewer != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Reviewer->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Reviewer"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Reviewer->UseTypeAttribute = true;
		}
		m_Reviewer->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ReviewerRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ReviewerRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ReviewerRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ReviewerRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsIndividualMediaReviewDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaReviewDescriptionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Reviewer"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Reviewer")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetReviewer(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ReviewerRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ReviewerRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetReviewerRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsIndividualMediaReviewDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Reviewer")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetReviewer(child);
  }
  if (XMLString::compareString(elementname, X("ReviewerRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetReviewerRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsIndividualMediaReviewDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetReviewer() != Dc1NodePtr())
		result.Insert(GetReviewer());
	if (GetReviewerRef() != Dc1NodePtr())
		result.Insert(GetReviewerRef());
	if (GetContentRef() != Dc1NodePtr())
		result.Insert(GetContentRef());
	if (GetTags() != Dc1NodePtr())
		result.Insert(GetTags());
	if (GetFreeTextReview() != Dc1NodePtr())
		result.Insert(GetFreeTextReview());
	if (GetMediaRating() != Dc1NodePtr())
		result.Insert(GetMediaRating());
	if (GetIdentityRating() != Dc1NodePtr())
		result.Insert(GetIdentityRating());
	if (GetQualityRating() != Dc1NodePtr())
		result.Insert(GetQualityRating());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsIndividualMediaReviewDescriptionType_ExtMethodImpl.h


