
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtImplInclude.h


#include "Mp7JrsMediaLocatorType.h"
#include "Mp7JrsStreamSectionType.h"
#include "Mp7JrsInlineMediaType.h"
#include "Mp7JrsStreamLocatorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsStreamLocatorType::IMp7JrsStreamLocatorType()
{

// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtPropInit.h

}

IMp7JrsStreamLocatorType::~IMp7JrsStreamLocatorType()
{
// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtPropCleanup.h

}

Mp7JrsStreamLocatorType::Mp7JrsStreamLocatorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStreamLocatorType::~Mp7JrsStreamLocatorType()
{
	Cleanup();
}

void Mp7JrsStreamLocatorType::Init()
{
	// Init base
	m_Base = CreateMediaLocatorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_StreamSection = Mp7JrsStreamSectionPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtMyPropInit.h

}

void Mp7JrsStreamLocatorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_StreamSection);
}

void Mp7JrsStreamLocatorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StreamLocatorTypePtr, since we
	// might need GetBase(), which isn't defined in IStreamLocatorType
	const Dc1Ptr< Mp7JrsStreamLocatorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaLocatorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStreamLocatorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_StreamSection);
		this->SetStreamSection(Dc1Factory::CloneObject(tmp->GetStreamSection()));
}

Dc1NodePtr Mp7JrsStreamLocatorType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsStreamLocatorType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsMediaLocatorType > Mp7JrsStreamLocatorType::GetBase() const
{
	return m_Base;
}

Mp7JrsStreamSectionPtr Mp7JrsStreamLocatorType::GetStreamSection() const
{
		return m_StreamSection;
}

void Mp7JrsStreamLocatorType::SetStreamSection(const Mp7JrsStreamSectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStreamLocatorType::SetStreamSection().");
	}
	if (m_StreamSection != item)
	{
		// Dc1Factory::DeleteObject(m_StreamSection);
		m_StreamSection = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StreamSection) m_StreamSection->SetParent(m_myself.getPointer());
		if (m_StreamSection && m_StreamSection->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamSectionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StreamSection->UseTypeAttribute = true;
		}
		if(m_StreamSection != Mp7JrsStreamSectionPtr())
		{
			m_StreamSection->SetContentName(XMLString::transcode("StreamSection"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsStreamLocatorType::GetMediaUri() const
{
	return GetBase()->GetMediaUri();
}

Mp7JrsInlineMediaPtr Mp7JrsStreamLocatorType::GetInlineMedia() const
{
	return GetBase()->GetInlineMedia();
}

unsigned Mp7JrsStreamLocatorType::GetStreamID() const
{
	return GetBase()->GetStreamID();
}

// Element is optional
bool Mp7JrsStreamLocatorType::IsValidStreamID() const
{
	return GetBase()->IsValidStreamID();
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsStreamLocatorType::SetMediaUri(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStreamLocatorType::SetMediaUri().");
	}
	GetBase()->SetMediaUri(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsStreamLocatorType::SetInlineMedia(const Mp7JrsInlineMediaPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStreamLocatorType::SetInlineMedia().");
	}
	GetBase()->SetInlineMedia(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStreamLocatorType::SetStreamID(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStreamLocatorType::SetStreamID().");
	}
	GetBase()->SetStreamID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStreamLocatorType::InvalidateStreamID()
{
	GetBase()->InvalidateStreamID();
}

Dc1NodeEnum Mp7JrsStreamLocatorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("StreamSection")) == 0)
	{
		// StreamSection is simple element StreamSectionType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetStreamSection()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StreamSectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStreamSectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetStreamSection(p, client);
					if((p = GetStreamSection()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StreamLocatorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StreamLocatorType");
	}
	return result;
}

XMLCh * Mp7JrsStreamLocatorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStreamLocatorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStreamLocatorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StreamLocatorType"));
	// Element serialization:
	// Class
	
	if (m_StreamSection != Mp7JrsStreamSectionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StreamSection->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StreamSection"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StreamSection->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsStreamLocatorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaLocatorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StreamSection"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StreamSection")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateStreamSectionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStreamSection(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStreamLocatorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStreamLocatorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("StreamSection")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateStreamSectionType; // FTT, check this
	}
	this->SetStreamSection(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStreamLocatorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStreamSection() != Dc1NodePtr())
		result.Insert(GetStreamSection());
	if (GetInlineMedia() != Dc1NodePtr())
		result.Insert(GetInlineMedia());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsStreamLocatorType_ExtMethodImpl.h
Mp7JrsmediaTimePointPtr Mp7JrsStreamLocatorType::GetMediaTimeBase(void) const
{
	return this->GetBase()->GetMediaTimeBase();
}
void Mp7JrsStreamLocatorType::SetMediaTimeBase(const Mp7JrsmediaTimePointPtr &timePoint, Dc1ClientID client)
{
	return this->GetBase()->SetMediaTimeBase(timePoint, client);
}
// end extension included


