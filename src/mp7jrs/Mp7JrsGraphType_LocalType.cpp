
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtImplInclude.h


#include "Mp7JrsGraphType_Node_LocalType.h"
#include "Mp7JrsRelationType.h"
#include "Mp7JrsGraphType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsGraphType_LocalType::IMp7JrsGraphType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtPropInit.h

}

IMp7JrsGraphType_LocalType::~IMp7JrsGraphType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtPropCleanup.h

}

Mp7JrsGraphType_LocalType::Mp7JrsGraphType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGraphType_LocalType::~Mp7JrsGraphType_LocalType()
{
	Cleanup();
}

void Mp7JrsGraphType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Node = Mp7JrsGraphType_Node_LocalPtr(); // Class
	m_Relation = Mp7JrsRelationPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtMyPropInit.h

}

void Mp7JrsGraphType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Node);
	// Dc1Factory::DeleteObject(m_Relation);
}

void Mp7JrsGraphType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GraphType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IGraphType_LocalType
	const Dc1Ptr< Mp7JrsGraphType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Node);
		this->SetNode(Dc1Factory::CloneObject(tmp->GetNode()));
		// Dc1Factory::DeleteObject(m_Relation);
		this->SetRelation(Dc1Factory::CloneObject(tmp->GetRelation()));
}

Dc1NodePtr Mp7JrsGraphType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsGraphType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsGraphType_Node_LocalPtr Mp7JrsGraphType_LocalType::GetNode() const
{
		return m_Node;
}

Mp7JrsRelationPtr Mp7JrsGraphType_LocalType::GetRelation() const
{
		return m_Relation;
}

// implementing setter for choice 
/* element */
void Mp7JrsGraphType_LocalType::SetNode(const Mp7JrsGraphType_Node_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphType_LocalType::SetNode().");
	}
	if (m_Node != item)
	{
		// Dc1Factory::DeleteObject(m_Node);
		m_Node = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Node) m_Node->SetParent(m_myself.getPointer());
		if (m_Node && m_Node->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType_Node_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Node->UseTypeAttribute = true;
		}
		if(m_Node != Mp7JrsGraphType_Node_LocalPtr())
		{
			m_Node->SetContentName(XMLString::transcode("Node"));
		}
	// Dc1Factory::DeleteObject(m_Relation);
	m_Relation = Mp7JrsRelationPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsGraphType_LocalType::SetRelation(const Mp7JrsRelationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphType_LocalType::SetRelation().");
	}
	if (m_Relation != item)
	{
		// Dc1Factory::DeleteObject(m_Relation);
		m_Relation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relation) m_Relation->SetParent(m_myself.getPointer());
		if (m_Relation && m_Relation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Relation->UseTypeAttribute = true;
		}
		if(m_Relation != Mp7JrsRelationPtr())
		{
			m_Relation->SetContentName(XMLString::transcode("Relation"));
		}
	// Dc1Factory::DeleteObject(m_Node);
	m_Node = Mp7JrsGraphType_Node_LocalPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: GraphType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsGraphType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsGraphType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsGraphType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsGraphType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Node != Mp7JrsGraphType_Node_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Node->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Node"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Node->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Relation != Mp7JrsRelationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Relation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Relation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Relation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsGraphType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Node"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Node")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateGraphType_Node_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetNode(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Relation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Relation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRelationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRelation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGraphType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Node")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateGraphType_Node_LocalType; // FTT, check this
	}
	this->SetNode(child);
  }
  if (XMLString::compareString(elementname, X("Relation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRelationType; // FTT, check this
	}
	this->SetRelation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGraphType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetNode() != Dc1NodePtr())
		result.Insert(GetNode());
	if (GetRelation() != Dc1NodePtr())
		result.Insert(GetRelation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGraphType_LocalType_ExtMethodImpl.h


