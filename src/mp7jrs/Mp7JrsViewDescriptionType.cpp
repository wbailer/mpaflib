
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentAbstractionType.h"
#include "Mp7JrsViewDescriptionType_View_CollectionType.h"
#include "Mp7JrsViewDescriptionType_ViewSet_CollectionType.h"
#include "Mp7JrsViewDescriptionType_ViewDecomposition_CollectionType.h"
#include "Mp7JrsContentDescriptionType_Affective_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsViewDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsAffectiveType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Affective
#include "Mp7JrsViewType.h" // Element collection urn:mpeg:mpeg7:schema:2004:View
#include "Mp7JrsViewSetType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ViewSet
#include "Mp7JrsViewDecompositionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ViewDecomposition

#include <assert.h>
IMp7JrsViewDescriptionType::IMp7JrsViewDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtPropInit.h

}

IMp7JrsViewDescriptionType::~IMp7JrsViewDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtPropCleanup.h

}

Mp7JrsViewDescriptionType::Mp7JrsViewDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsViewDescriptionType::~Mp7JrsViewDescriptionType()
{
	Cleanup();
}

void Mp7JrsViewDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentAbstractionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_View = Mp7JrsViewDescriptionType_View_CollectionPtr(); // Collection
	m_ViewSet = Mp7JrsViewDescriptionType_ViewSet_CollectionPtr(); // Collection
	m_ViewDecomposition = Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtMyPropInit.h

}

void Mp7JrsViewDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_View);
	// Dc1Factory::DeleteObject(m_ViewSet);
	// Dc1Factory::DeleteObject(m_ViewDecomposition);
}

void Mp7JrsViewDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ViewDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IViewDescriptionType
	const Dc1Ptr< Mp7JrsViewDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentAbstractionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsViewDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_View);
		this->SetView(Dc1Factory::CloneObject(tmp->GetView()));
		// Dc1Factory::DeleteObject(m_ViewSet);
		this->SetViewSet(Dc1Factory::CloneObject(tmp->GetViewSet()));
		// Dc1Factory::DeleteObject(m_ViewDecomposition);
		this->SetViewDecomposition(Dc1Factory::CloneObject(tmp->GetViewDecomposition()));
}

Dc1NodePtr Mp7JrsViewDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsViewDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentAbstractionType > Mp7JrsViewDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsViewDescriptionType_View_CollectionPtr Mp7JrsViewDescriptionType::GetView() const
{
		return m_View;
}

Mp7JrsViewDescriptionType_ViewSet_CollectionPtr Mp7JrsViewDescriptionType::GetViewSet() const
{
		return m_ViewSet;
}

Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr Mp7JrsViewDescriptionType::GetViewDecomposition() const
{
		return m_ViewDecomposition;
}

// implementing setter for choice 
/* element */
void Mp7JrsViewDescriptionType::SetView(const Mp7JrsViewDescriptionType_View_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetView().");
	}
	if (m_View != item)
	{
		// Dc1Factory::DeleteObject(m_View);
		m_View = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_View) m_View->SetParent(m_myself.getPointer());
		if(m_View != Mp7JrsViewDescriptionType_View_CollectionPtr())
		{
			m_View->SetContentName(XMLString::transcode("View"));
		}
	// Dc1Factory::DeleteObject(m_ViewSet);
	m_ViewSet = Mp7JrsViewDescriptionType_ViewSet_CollectionPtr();
	// Dc1Factory::DeleteObject(m_ViewDecomposition);
	m_ViewDecomposition = Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsViewDescriptionType::SetViewSet(const Mp7JrsViewDescriptionType_ViewSet_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetViewSet().");
	}
	if (m_ViewSet != item)
	{
		// Dc1Factory::DeleteObject(m_ViewSet);
		m_ViewSet = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ViewSet) m_ViewSet->SetParent(m_myself.getPointer());
		if(m_ViewSet != Mp7JrsViewDescriptionType_ViewSet_CollectionPtr())
		{
			m_ViewSet->SetContentName(XMLString::transcode("ViewSet"));
		}
	// Dc1Factory::DeleteObject(m_View);
	m_View = Mp7JrsViewDescriptionType_View_CollectionPtr();
	// Dc1Factory::DeleteObject(m_ViewDecomposition);
	m_ViewDecomposition = Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsViewDescriptionType::SetViewDecomposition(const Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetViewDecomposition().");
	}
	if (m_ViewDecomposition != item)
	{
		// Dc1Factory::DeleteObject(m_ViewDecomposition);
		m_ViewDecomposition = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ViewDecomposition) m_ViewDecomposition->SetParent(m_myself.getPointer());
		if(m_ViewDecomposition != Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr())
		{
			m_ViewDecomposition->SetContentName(XMLString::transcode("ViewDecomposition"));
		}
	// Dc1Factory::DeleteObject(m_View);
	m_View = Mp7JrsViewDescriptionType_View_CollectionPtr();
	// Dc1Factory::DeleteObject(m_ViewSet);
	m_ViewSet = Mp7JrsViewDescriptionType_ViewSet_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsContentDescriptionType_Affective_CollectionPtr Mp7JrsViewDescriptionType::GetAffective() const
{
	return GetBase()->GetBase()->GetAffective();
}

void Mp7JrsViewDescriptionType::SetAffective(const Mp7JrsContentDescriptionType_Affective_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetAffective().");
	}
	GetBase()->GetBase()->SetAffective(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsViewDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsViewDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsViewDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsViewDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsViewDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsViewDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsViewDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsViewDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsViewDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("View")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:View is item of abstract type ViewType
		// in element collection ViewDescriptionType_View_CollectionType
		
		context->Found = true;
		Mp7JrsViewDescriptionType_View_CollectionPtr coll = GetView();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateViewDescriptionType_View_CollectionType; // FTT, check this
				SetView(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ViewSet")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ViewSet is item of type ViewSetType
		// in element collection ViewDescriptionType_ViewSet_CollectionType
		
		context->Found = true;
		Mp7JrsViewDescriptionType_ViewSet_CollectionPtr coll = GetViewSet();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateViewDescriptionType_ViewSet_CollectionType; // FTT, check this
				SetViewSet(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ViewSetType")))) != empty)
			{
				// Is type allowed
				Mp7JrsViewSetPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ViewDecomposition")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ViewDecomposition is item of abstract type ViewDecompositionType
		// in element collection ViewDescriptionType_ViewDecomposition_CollectionType
		
		context->Found = true;
		Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr coll = GetViewDecomposition();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateViewDescriptionType_ViewDecomposition_CollectionType; // FTT, check this
				SetViewDecomposition(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsViewDecompositionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ViewDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ViewDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsViewDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsViewDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsViewDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ViewDescriptionType"));
	// Element serialization:
	if (m_View != Mp7JrsViewDescriptionType_View_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_View->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ViewSet != Mp7JrsViewDescriptionType_ViewSet_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ViewSet->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ViewDecomposition != Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ViewDecomposition->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsViewDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentAbstractionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("View"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("View")) == 0))
		{
			// Deserialize factory type
			Mp7JrsViewDescriptionType_View_CollectionPtr tmp = CreateViewDescriptionType_View_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetView(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ViewSet"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ViewSet")) == 0))
		{
			// Deserialize factory type
			Mp7JrsViewDescriptionType_ViewSet_CollectionPtr tmp = CreateViewDescriptionType_ViewSet_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetViewSet(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ViewDecomposition"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ViewDecomposition")) == 0))
		{
			// Deserialize factory type
			Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr tmp = CreateViewDescriptionType_ViewDecomposition_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetViewDecomposition(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsViewDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("View")) == 0))
  {
	Mp7JrsViewDescriptionType_View_CollectionPtr tmp = CreateViewDescriptionType_View_CollectionType; // FTT, check this
	this->SetView(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ViewSet")) == 0))
  {
	Mp7JrsViewDescriptionType_ViewSet_CollectionPtr tmp = CreateViewDescriptionType_ViewSet_CollectionType; // FTT, check this
	this->SetViewSet(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ViewDecomposition")) == 0))
  {
	Mp7JrsViewDescriptionType_ViewDecomposition_CollectionPtr tmp = CreateViewDescriptionType_ViewDecomposition_CollectionType; // FTT, check this
	this->SetViewDecomposition(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsViewDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetView() != Dc1NodePtr())
		result.Insert(GetView());
	if (GetViewSet() != Dc1NodePtr())
		result.Insert(GetViewSet());
	if (GetViewDecomposition() != Dc1NodePtr())
		result.Insert(GetViewDecomposition());
	if (GetAffective() != Dc1NodePtr())
		result.Insert(GetAffective());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsViewDescriptionType_ExtMethodImpl.h


