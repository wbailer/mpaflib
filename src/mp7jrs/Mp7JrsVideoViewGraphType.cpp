
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtImplInclude.h


#include "Mp7JrsViewDecompositionType.h"
#include "Mp7JrsFrequencyViewType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsVideoViewGraphType_CollectionType.h"
#include "Mp7JrsVideoViewGraphType_CollectionType0.h"
#include "Mp7JrsSignalType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsVideoViewGraphType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsVideoViewGraphType_LocalType.h" // Choice collection SpaceFrequencyChild
#include "Mp7JrsVideoViewGraphType.h" // Choice collection element SpaceFrequencyChild
#include "Mp7JrsVideoViewGraphType_LocalType0.h" // Choice collection TimeFrequencyChild

#include <assert.h>
IMp7JrsVideoViewGraphType::IMp7JrsVideoViewGraphType()
{

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtPropInit.h

}

IMp7JrsVideoViewGraphType::~IMp7JrsVideoViewGraphType()
{
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtPropCleanup.h

}

Mp7JrsVideoViewGraphType::Mp7JrsVideoViewGraphType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVideoViewGraphType::~Mp7JrsVideoViewGraphType()
{
	Cleanup();
}

void Mp7JrsVideoViewGraphType::Init()
{
	// Init base
	m_Base = CreateViewDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_branching = 0; // Value

	// Init elements (element, union sequence choice all any)
	
	m_View = Mp7JrsFrequencyViewPtr(); // Class
	m_ViewRef = Mp7JrsReferencePtr(); // Class
	m_VideoViewGraphType_LocalType = Mp7JrsVideoViewGraphType_CollectionPtr(); // Collection
	m_VideoViewGraphType_LocalType0 = Mp7JrsVideoViewGraphType_Collection0Ptr(); // Collection


// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtMyPropInit.h

}

void Mp7JrsVideoViewGraphType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_View);
	// Dc1Factory::DeleteObject(m_ViewRef);
	// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType);
	// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType0);
}

void Mp7JrsVideoViewGraphType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VideoViewGraphTypePtr, since we
	// might need GetBase(), which isn't defined in IVideoViewGraphType
	const Dc1Ptr< Mp7JrsVideoViewGraphType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsViewDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsVideoViewGraphType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->Setbranching(tmp->Getbranching());
	}
		// Dc1Factory::DeleteObject(m_View);
		this->SetView(Dc1Factory::CloneObject(tmp->GetView()));
		// Dc1Factory::DeleteObject(m_ViewRef);
		this->SetViewRef(Dc1Factory::CloneObject(tmp->GetViewRef()));
		// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType);
		this->SetVideoViewGraphType_LocalType(Dc1Factory::CloneObject(tmp->GetVideoViewGraphType_LocalType()));
		// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType0);
		this->SetVideoViewGraphType_LocalType0(Dc1Factory::CloneObject(tmp->GetVideoViewGraphType_LocalType0()));
}

Dc1NodePtr Mp7JrsVideoViewGraphType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsVideoViewGraphType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsViewDecompositionType > Mp7JrsVideoViewGraphType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsVideoViewGraphType::Getbranching() const
{
	return m_branching;
}

void Mp7JrsVideoViewGraphType::Setbranching(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::Setbranching().");
	}
	m_branching = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsFrequencyViewPtr Mp7JrsVideoViewGraphType::GetView() const
{
		return m_View;
}

Mp7JrsReferencePtr Mp7JrsVideoViewGraphType::GetViewRef() const
{
		return m_ViewRef;
}

Mp7JrsVideoViewGraphType_CollectionPtr Mp7JrsVideoViewGraphType::GetVideoViewGraphType_LocalType() const
{
		return m_VideoViewGraphType_LocalType;
}

Mp7JrsVideoViewGraphType_Collection0Ptr Mp7JrsVideoViewGraphType::GetVideoViewGraphType_LocalType0() const
{
		return m_VideoViewGraphType_LocalType0;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsVideoViewGraphType::SetView(const Mp7JrsFrequencyViewPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetView().");
	}
	if (m_View != item)
	{
		// Dc1Factory::DeleteObject(m_View);
		m_View = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_View) m_View->SetParent(m_myself.getPointer());
		if (m_View && m_View->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyViewType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_View->UseTypeAttribute = true;
		}
		if(m_View != Mp7JrsFrequencyViewPtr())
		{
			m_View->SetContentName(XMLString::transcode("View"));
		}
	// Dc1Factory::DeleteObject(m_ViewRef);
	m_ViewRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsVideoViewGraphType::SetViewRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetViewRef().");
	}
	if (m_ViewRef != item)
	{
		// Dc1Factory::DeleteObject(m_ViewRef);
		m_ViewRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ViewRef) m_ViewRef->SetParent(m_myself.getPointer());
		if (m_ViewRef && m_ViewRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ViewRef->UseTypeAttribute = true;
		}
		if(m_ViewRef != Mp7JrsReferencePtr())
		{
			m_ViewRef->SetContentName(XMLString::transcode("ViewRef"));
		}
	// Dc1Factory::DeleteObject(m_View);
	m_View = Mp7JrsFrequencyViewPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::SetVideoViewGraphType_LocalType(const Mp7JrsVideoViewGraphType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetVideoViewGraphType_LocalType().");
	}
	if (m_VideoViewGraphType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType);
		m_VideoViewGraphType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoViewGraphType_LocalType) m_VideoViewGraphType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::SetVideoViewGraphType_LocalType0(const Mp7JrsVideoViewGraphType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetVideoViewGraphType_LocalType0().");
	}
	if (m_VideoViewGraphType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_VideoViewGraphType_LocalType0);
		m_VideoViewGraphType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoViewGraphType_LocalType0) m_VideoViewGraphType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsVideoViewGraphType::Getcomplete() const
{
	return GetBase()->Getcomplete();
}

bool Mp7JrsVideoViewGraphType::Existcomplete() const
{
	return GetBase()->Existcomplete();
}
void Mp7JrsVideoViewGraphType::Setcomplete(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::Setcomplete().");
	}
	GetBase()->Setcomplete(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::Invalidatecomplete()
{
	GetBase()->Invalidatecomplete();
}
bool Mp7JrsVideoViewGraphType::GetnonRedundant() const
{
	return GetBase()->GetnonRedundant();
}

bool Mp7JrsVideoViewGraphType::ExistnonRedundant() const
{
	return GetBase()->ExistnonRedundant();
}
void Mp7JrsVideoViewGraphType::SetnonRedundant(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetnonRedundant().");
	}
	GetBase()->SetnonRedundant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidatenonRedundant()
{
	GetBase()->InvalidatenonRedundant();
}
Mp7JrsSignalPtr Mp7JrsVideoViewGraphType::GetSource() const
{
	return GetBase()->GetSource();
}

// Element is optional
bool Mp7JrsVideoViewGraphType::IsValidSource() const
{
	return GetBase()->IsValidSource();
}

void Mp7JrsVideoViewGraphType::SetSource(const Mp7JrsSignalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetSource().");
	}
	GetBase()->SetSource(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidateSource()
{
	GetBase()->InvalidateSource();
}
XMLCh * Mp7JrsVideoViewGraphType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsVideoViewGraphType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsVideoViewGraphType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsVideoViewGraphType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsVideoViewGraphType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsVideoViewGraphType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsVideoViewGraphType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsVideoViewGraphType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsVideoViewGraphType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsVideoViewGraphType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsVideoViewGraphType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsVideoViewGraphType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsVideoViewGraphType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsVideoViewGraphType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsVideoViewGraphType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVideoViewGraphType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsVideoViewGraphType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsVideoViewGraphType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVideoViewGraphType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVideoViewGraphType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("branching")) == 0)
	{
		// branching is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("View")) == 0)
	{
		// View is simple element FrequencyViewType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetView()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:FrequencyViewType")))) != empty)
			{
				// Is type allowed
				Mp7JrsFrequencyViewPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetView(p, client);
					if((p = GetView()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ViewRef")) == 0)
	{
		// ViewRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetViewRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetViewRef(p, client);
					if((p = GetViewRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpaceFrequencyChild")) == 0)
	{
		// SpaceFrequencyChild is contained in itemtype VideoViewGraphType_LocalType
		// in choice collection VideoViewGraphType_CollectionType

		context->Found = true;
		Mp7JrsVideoViewGraphType_CollectionPtr coll = GetVideoViewGraphType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoViewGraphType_CollectionType; // FTT, check this
				SetVideoViewGraphType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->GetSpaceFrequencyChild()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoViewGraphType_LocalPtr item = CreateVideoViewGraphType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoViewGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->SetSpaceFrequencyChild(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->GetSpaceFrequencyChild()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpaceFrequencyChildRef")) == 0)
	{
		// SpaceFrequencyChildRef is contained in itemtype VideoViewGraphType_LocalType
		// in choice collection VideoViewGraphType_CollectionType

		context->Found = true;
		Mp7JrsVideoViewGraphType_CollectionPtr coll = GetVideoViewGraphType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoViewGraphType_CollectionType; // FTT, check this
				SetVideoViewGraphType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->GetSpaceFrequencyChildRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoViewGraphType_LocalPtr item = CreateVideoViewGraphType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->SetSpaceFrequencyChildRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoViewGraphType_LocalPtr)coll->elementAt(i))->GetSpaceFrequencyChildRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TimeFrequencyChild")) == 0)
	{
		// TimeFrequencyChild is contained in itemtype VideoViewGraphType_LocalType0
		// in choice collection VideoViewGraphType_CollectionType0

		context->Found = true;
		Mp7JrsVideoViewGraphType_Collection0Ptr coll = GetVideoViewGraphType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoViewGraphType_CollectionType0; // FTT, check this
				SetVideoViewGraphType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->GetTimeFrequencyChild()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoViewGraphType_Local0Ptr item = CreateVideoViewGraphType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoViewGraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsVideoViewGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->SetTimeFrequencyChild(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->GetTimeFrequencyChild()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TimeFrequencyChildRef")) == 0)
	{
		// TimeFrequencyChildRef is contained in itemtype VideoViewGraphType_LocalType0
		// in choice collection VideoViewGraphType_CollectionType0

		context->Found = true;
		Mp7JrsVideoViewGraphType_Collection0Ptr coll = GetVideoViewGraphType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateVideoViewGraphType_CollectionType0; // FTT, check this
				SetVideoViewGraphType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->GetTimeFrequencyChildRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsVideoViewGraphType_Local0Ptr item = CreateVideoViewGraphType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->SetTimeFrequencyChildRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsVideoViewGraphType_Local0Ptr)coll->elementAt(i))->GetTimeFrequencyChildRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VideoViewGraphType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VideoViewGraphType");
	}
	return result;
}

XMLCh * Mp7JrsVideoViewGraphType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVideoViewGraphType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVideoViewGraphType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VideoViewGraphType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_branching);
		element->setAttributeNS(X(""), X("branching"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_View != Mp7JrsFrequencyViewPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_View->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("View"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_View->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ViewRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ViewRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ViewRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ViewRef->Serialize(doc, element, &elem);
	}
	if (m_VideoViewGraphType_LocalType != Mp7JrsVideoViewGraphType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VideoViewGraphType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_VideoViewGraphType_LocalType0 != Mp7JrsVideoViewGraphType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_VideoViewGraphType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsVideoViewGraphType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("branching")))
	{
		// deserialize value type
		this->Setbranching(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("branching"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsViewDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("View"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("View")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFrequencyViewType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetView(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ViewRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ViewRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetViewRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChild
			Dc1Util::HasNodeName(parent, X("SpaceFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChild")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChildRef
			Dc1Util::HasNodeName(parent, X("SpaceFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SpaceFrequencyChildRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsVideoViewGraphType_CollectionPtr tmp = CreateVideoViewGraphType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVideoViewGraphType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:TimeFrequencyChild
			Dc1Util::HasNodeName(parent, X("TimeFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TimeFrequencyChild")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:TimeFrequencyChildRef
			Dc1Util::HasNodeName(parent, X("TimeFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:TimeFrequencyChildRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsVideoViewGraphType_Collection0Ptr tmp = CreateVideoViewGraphType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetVideoViewGraphType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVideoViewGraphType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("View")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFrequencyViewType; // FTT, check this
	}
	this->SetView(child);
  }
  if (XMLString::compareString(elementname, X("ViewRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetViewRef(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpaceFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpaceFrequencyChild")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SpaceFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SpaceFrequencyChildRef")) == 0)
	)
  {
	Mp7JrsVideoViewGraphType_CollectionPtr tmp = CreateVideoViewGraphType_CollectionType; // FTT, check this
	this->SetVideoViewGraphType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TimeFrequencyChild"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TimeFrequencyChild")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("TimeFrequencyChildRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("TimeFrequencyChildRef")) == 0)
	)
  {
	Mp7JrsVideoViewGraphType_Collection0Ptr tmp = CreateVideoViewGraphType_CollectionType0; // FTT, check this
	this->SetVideoViewGraphType_LocalType0(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVideoViewGraphType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVideoViewGraphType_LocalType() != Dc1NodePtr())
		result.Insert(GetVideoViewGraphType_LocalType());
	if (GetVideoViewGraphType_LocalType0() != Dc1NodePtr())
		result.Insert(GetVideoViewGraphType_LocalType0());
	if (GetSource() != Dc1NodePtr())
		result.Insert(GetSource());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	if (GetView() != Dc1NodePtr())
		result.Insert(GetView());
	if (GetViewRef() != Dc1NodePtr())
		result.Insert(GetViewRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVideoViewGraphType_ExtMethodImpl.h


