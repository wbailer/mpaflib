
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtImplInclude.h


#include "Mp7JrsSegmentOutputQCValueType_Output_CollectionType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsSpatioTemporalLocatorType.h"
#include "Mp7JrsSegmentOutputQCValueType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsOutputQCValueType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Output

#include <assert.h>
IMp7JrsSegmentOutputQCValueType::IMp7JrsSegmentOutputQCValueType()
{

// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtPropInit.h

}

IMp7JrsSegmentOutputQCValueType::~IMp7JrsSegmentOutputQCValueType()
{
// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtPropCleanup.h

}

Mp7JrsSegmentOutputQCValueType::Mp7JrsSegmentOutputQCValueType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSegmentOutputQCValueType::~Mp7JrsSegmentOutputQCValueType()
{
	Cleanup();
}

void Mp7JrsSegmentOutputQCValueType::Init()
{

	// Init attributes
	m_name = NULL; // String
	m_name_Exist = false;
	m_annotation = NULL; // String
	m_annotation_Exist = false;
	m_verificationMedia = NULL; // String
	m_verificationMedia_Exist = false;
	m_confidence = 0.0; // Value
	m_confidence_Exist = false;
	m_severity = 0; // Value
	m_severity_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Output = Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr(); // Collection
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class
	m_RegionLocator = Mp7JrsSpatioTemporalLocatorPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtMyPropInit.h

}

void Mp7JrsSegmentOutputQCValueType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtMyPropCleanup.h


	XMLString::release(&m_name); // String
	XMLString::release(&m_annotation); // String
	XMLString::release(&m_verificationMedia); // String
	// Dc1Factory::DeleteObject(m_Output);
	// Dc1Factory::DeleteObject(m_MediaTime);
	// Dc1Factory::DeleteObject(m_RegionLocator);
}

void Mp7JrsSegmentOutputQCValueType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SegmentOutputQCValueTypePtr, since we
	// might need GetBase(), which isn't defined in ISegmentOutputQCValueType
	const Dc1Ptr< Mp7JrsSegmentOutputQCValueType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_name); // String
	if (tmp->Existname())
	{
		this->Setname(XMLString::replicate(tmp->Getname()));
	}
	else
	{
		Invalidatename();
	}
	}
	{
	XMLString::release(&m_annotation); // String
	if (tmp->Existannotation())
	{
		this->Setannotation(XMLString::replicate(tmp->Getannotation()));
	}
	else
	{
		Invalidateannotation();
	}
	}
	{
	XMLString::release(&m_verificationMedia); // String
	if (tmp->ExistverificationMedia())
	{
		this->SetverificationMedia(XMLString::replicate(tmp->GetverificationMedia()));
	}
	else
	{
		InvalidateverificationMedia();
	}
	}
	{
	if (tmp->Existconfidence())
	{
		this->Setconfidence(tmp->Getconfidence());
	}
	else
	{
		Invalidateconfidence();
	}
	}
	{
	if (tmp->Existseverity())
	{
		this->Setseverity(tmp->Getseverity());
	}
	else
	{
		Invalidateseverity();
	}
	}
		// Dc1Factory::DeleteObject(m_Output);
		this->SetOutput(Dc1Factory::CloneObject(tmp->GetOutput()));
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
		// Dc1Factory::DeleteObject(m_RegionLocator);
		this->SetRegionLocator(Dc1Factory::CloneObject(tmp->GetRegionLocator()));
}

Dc1NodePtr Mp7JrsSegmentOutputQCValueType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSegmentOutputQCValueType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsSegmentOutputQCValueType::Getname() const
{
	return m_name;
}

bool Mp7JrsSegmentOutputQCValueType::Existname() const
{
	return m_name_Exist;
}
void Mp7JrsSegmentOutputQCValueType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::Setname().");
	}
	m_name = item;
	m_name_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentOutputQCValueType::Invalidatename()
{
	m_name_Exist = false;
}
XMLCh * Mp7JrsSegmentOutputQCValueType::Getannotation() const
{
	return m_annotation;
}

bool Mp7JrsSegmentOutputQCValueType::Existannotation() const
{
	return m_annotation_Exist;
}
void Mp7JrsSegmentOutputQCValueType::Setannotation(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::Setannotation().");
	}
	m_annotation = item;
	m_annotation_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentOutputQCValueType::Invalidateannotation()
{
	m_annotation_Exist = false;
}
XMLCh * Mp7JrsSegmentOutputQCValueType::GetverificationMedia() const
{
	return m_verificationMedia;
}

bool Mp7JrsSegmentOutputQCValueType::ExistverificationMedia() const
{
	return m_verificationMedia_Exist;
}
void Mp7JrsSegmentOutputQCValueType::SetverificationMedia(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::SetverificationMedia().");
	}
	m_verificationMedia = item;
	m_verificationMedia_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentOutputQCValueType::InvalidateverificationMedia()
{
	m_verificationMedia_Exist = false;
}
double Mp7JrsSegmentOutputQCValueType::Getconfidence() const
{
	return m_confidence;
}

bool Mp7JrsSegmentOutputQCValueType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsSegmentOutputQCValueType::Setconfidence(double item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::Setconfidence().");
	}
	m_confidence = item;
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentOutputQCValueType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
unsigned Mp7JrsSegmentOutputQCValueType::Getseverity() const
{
	return m_severity;
}

bool Mp7JrsSegmentOutputQCValueType::Existseverity() const
{
	return m_severity_Exist;
}
void Mp7JrsSegmentOutputQCValueType::Setseverity(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::Setseverity().");
	}
	m_severity = item;
	m_severity_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSegmentOutputQCValueType::Invalidateseverity()
{
	m_severity_Exist = false;
}
Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr Mp7JrsSegmentOutputQCValueType::GetOutput() const
{
		return m_Output;
}

Mp7JrsMediaTimePtr Mp7JrsSegmentOutputQCValueType::GetMediaTime() const
{
		return m_MediaTime;
}

Mp7JrsSpatioTemporalLocatorPtr Mp7JrsSegmentOutputQCValueType::GetRegionLocator() const
{
		return m_RegionLocator;
}

void Mp7JrsSegmentOutputQCValueType::SetOutput(const Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::SetOutput().");
	}
	if (m_Output != item)
	{
		// Dc1Factory::DeleteObject(m_Output);
		m_Output = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Output) m_Output->SetParent(m_myself.getPointer());
		if(m_Output != Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr())
		{
			m_Output->SetContentName(XMLString::transcode("Output"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsSegmentOutputQCValueType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::SetMediaTime().");
	}
	if (m_MediaTime != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	// Dc1Factory::DeleteObject(m_RegionLocator);
	m_RegionLocator = Mp7JrsSpatioTemporalLocatorPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSegmentOutputQCValueType::SetRegionLocator(const Mp7JrsSpatioTemporalLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSegmentOutputQCValueType::SetRegionLocator().");
	}
	if (m_RegionLocator != item)
	{
		// Dc1Factory::DeleteObject(m_RegionLocator);
		m_RegionLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RegionLocator) m_RegionLocator->SetParent(m_myself.getPointer());
		if (m_RegionLocator && m_RegionLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RegionLocator->UseTypeAttribute = true;
		}
		if(m_RegionLocator != Mp7JrsSpatioTemporalLocatorPtr())
		{
			m_RegionLocator->SetContentName(XMLString::transcode("RegionLocator"));
		}
	// Dc1Factory::DeleteObject(m_MediaTime);
	m_MediaTime = Mp7JrsMediaTimePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSegmentOutputQCValueType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("name")) == 0)
	{
		// name is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("annotation")) == 0)
	{
		// annotation is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("verificationMedia")) == 0)
	{
		// verificationMedia is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute double
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "double");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("severity")) == 0)
	{
		// severity is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Output")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Output is item of type OutputQCValueType
		// in element collection SegmentOutputQCValueType_Output_CollectionType
		
		context->Found = true;
		Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr coll = GetOutput();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSegmentOutputQCValueType_Output_CollectionType; // FTT, check this
				SetOutput(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:OutputQCValueType")))) != empty)
			{
				// Is type allowed
				Mp7JrsOutputQCValuePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTime")) == 0)
	{
		// MediaTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTime(p, client);
					if((p = GetMediaTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RegionLocator")) == 0)
	{
		// RegionLocator is simple element SpatioTemporalLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRegionLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpatioTemporalLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpatioTemporalLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRegionLocator(p, client);
					if((p = GetRegionLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SegmentOutputQCValueType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SegmentOutputQCValueType");
	}
	return result;
}

XMLCh * Mp7JrsSegmentOutputQCValueType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSegmentOutputQCValueType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSegmentOutputQCValueType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SegmentOutputQCValueType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_name_Exist)
	{
	// String
	if(m_name != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("name"), m_name);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_annotation_Exist)
	{
	// String
	if(m_annotation != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("annotation"), m_annotation);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_verificationMedia_Exist)
	{
	// String
	if(m_verificationMedia != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("verificationMedia"), m_verificationMedia);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_confidence);
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_severity_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_severity);
		element->setAttributeNS(X(""), X("severity"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Output != Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Output->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_RegionLocator != Mp7JrsSpatioTemporalLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RegionLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RegionLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RegionLocator->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSegmentOutputQCValueType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("name")))
	{
		// Deserialize string type
		this->Setname(Dc1Convert::TextToString(parent->getAttribute(X("name"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("annotation")))
	{
		// Deserialize string type
		this->Setannotation(Dc1Convert::TextToString(parent->getAttribute(X("annotation"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("verificationMedia")))
	{
		// Deserialize string type
		this->SetverificationMedia(Dc1Convert::TextToString(parent->getAttribute(X("verificationMedia"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// deserialize value type
		this->Setconfidence(Dc1Convert::TextToDouble(parent->getAttribute(X("confidence"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("severity")))
	{
		// deserialize value type
		this->Setseverity(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("severity"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Output"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Output")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr tmp = CreateSegmentOutputQCValueType_Output_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetOutput(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RegionLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RegionLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpatioTemporalLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRegionLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSegmentOutputQCValueType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Output")) == 0))
  {
	Mp7JrsSegmentOutputQCValueType_Output_CollectionPtr tmp = CreateSegmentOutputQCValueType_Output_CollectionType; // FTT, check this
	this->SetOutput(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  if (XMLString::compareString(elementname, X("RegionLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpatioTemporalLocatorType; // FTT, check this
	}
	this->SetRegionLocator(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSegmentOutputQCValueType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetOutput() != Dc1NodePtr())
		result.Insert(GetOutput());
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetRegionLocator() != Dc1NodePtr())
		result.Insert(GetRegionLocator());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSegmentOutputQCValueType_ExtMethodImpl.h


