
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtImplInclude.h


#include "Mp7JrstimePointType.h"
#include "Mp7JrsPersonNameType_CollectionType.h"
#include "Mp7JrsPersonNameType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPersonNameType_LocalType.h" // Choice collection GivenName
#include "Mp7JrsNameComponentType.h" // Choice collection element GivenName

#include <assert.h>
IMp7JrsPersonNameType::IMp7JrsPersonNameType()
{

// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtPropInit.h

}

IMp7JrsPersonNameType::~IMp7JrsPersonNameType()
{
// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtPropCleanup.h

}

Mp7JrsPersonNameType::Mp7JrsPersonNameType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPersonNameType::~Mp7JrsPersonNameType()
{
	Cleanup();
}

void Mp7JrsPersonNameType::Init()
{

	// Init attributes
	m_dateFrom = Mp7JrstimePointPtr(); // Pattern
	m_dateFrom_Exist = false;
	m_dateTo = Mp7JrstimePointPtr(); // Pattern
	m_dateTo_Exist = false;
	m_type = Mp7JrsPersonNameType_type_LocalType::UninitializedEnumeration;
	m_type_Exist = false;
	m_lang = NULL; // String
	m_lang_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_PersonNameType_LocalType = Mp7JrsPersonNameType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtMyPropInit.h

}

void Mp7JrsPersonNameType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_dateFrom); // Pattern
	// Dc1Factory::DeleteObject(m_dateTo); // Pattern
	XMLString::release(&m_lang); // String
	// Dc1Factory::DeleteObject(m_PersonNameType_LocalType);
}

void Mp7JrsPersonNameType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PersonNameTypePtr, since we
	// might need GetBase(), which isn't defined in IPersonNameType
	const Dc1Ptr< Mp7JrsPersonNameType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_dateFrom); // Pattern
	if (tmp->ExistdateFrom())
	{
		this->SetdateFrom(Dc1Factory::CloneObject(tmp->GetdateFrom()));
	}
	else
	{
		InvalidatedateFrom();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_dateTo); // Pattern
	if (tmp->ExistdateTo())
	{
		this->SetdateTo(Dc1Factory::CloneObject(tmp->GetdateTo()));
	}
	else
	{
		InvalidatedateTo();
	}
	}
	{
	if (tmp->Existtype())
	{
		this->Settype(tmp->Gettype());
	}
	else
	{
		Invalidatetype();
	}
	}
	{
	XMLString::release(&m_lang); // String
	if (tmp->Existlang())
	{
		this->Setlang(XMLString::replicate(tmp->Getlang()));
	}
	else
	{
		Invalidatelang();
	}
	}
		// Dc1Factory::DeleteObject(m_PersonNameType_LocalType);
		this->SetPersonNameType_LocalType(Dc1Factory::CloneObject(tmp->GetPersonNameType_LocalType()));
}

Dc1NodePtr Mp7JrsPersonNameType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPersonNameType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrstimePointPtr Mp7JrsPersonNameType::GetdateFrom() const
{
	return m_dateFrom;
}

bool Mp7JrsPersonNameType::ExistdateFrom() const
{
	return m_dateFrom_Exist;
}
void Mp7JrsPersonNameType::SetdateFrom(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType::SetdateFrom().");
	}
	m_dateFrom = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dateFrom) m_dateFrom->SetParent(m_myself.getPointer());
	m_dateFrom_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType::InvalidatedateFrom()
{
	m_dateFrom_Exist = false;
}
Mp7JrstimePointPtr Mp7JrsPersonNameType::GetdateTo() const
{
	return m_dateTo;
}

bool Mp7JrsPersonNameType::ExistdateTo() const
{
	return m_dateTo_Exist;
}
void Mp7JrsPersonNameType::SetdateTo(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType::SetdateTo().");
	}
	m_dateTo = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_dateTo) m_dateTo->SetParent(m_myself.getPointer());
	m_dateTo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType::InvalidatedateTo()
{
	m_dateTo_Exist = false;
}
Mp7JrsPersonNameType_type_LocalType::Enumeration Mp7JrsPersonNameType::Gettype() const
{
	return m_type;
}

bool Mp7JrsPersonNameType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsPersonNameType::Settype(Mp7JrsPersonNameType_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType::Settype().");
	}
	m_type = item;
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType::Invalidatetype()
{
	m_type_Exist = false;
}
XMLCh * Mp7JrsPersonNameType::Getlang() const
{
	return m_lang;
}

bool Mp7JrsPersonNameType::Existlang() const
{
	return m_lang_Exist;
}
void Mp7JrsPersonNameType::Setlang(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType::Setlang().");
	}
	m_lang = item;
	m_lang_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPersonNameType::Invalidatelang()
{
	m_lang_Exist = false;
}
Mp7JrsPersonNameType_CollectionPtr Mp7JrsPersonNameType::GetPersonNameType_LocalType() const
{
		return m_PersonNameType_LocalType;
}

void Mp7JrsPersonNameType::SetPersonNameType_LocalType(const Mp7JrsPersonNameType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPersonNameType::SetPersonNameType_LocalType().");
	}
	if (m_PersonNameType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_PersonNameType_LocalType);
		m_PersonNameType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PersonNameType_LocalType) m_PersonNameType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPersonNameType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dateFrom")) == 0)
	{
		// dateFrom is simple attribute timePointType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstimePointPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("dateTo")) == 0)
	{
		// dateTo is simple attribute timePointType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrstimePointPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute PersonNameType_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsPersonNameType_type_LocalType::Enumeration");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lang")) == 0)
	{
		// lang is simple attribute lang
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GivenName")) == 0)
	{
		// GivenName is contained in itemtype PersonNameType_LocalType
		// in choice collection PersonNameType_CollectionType

		context->Found = true;
		Mp7JrsPersonNameType_CollectionPtr coll = GetPersonNameType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonNameType_CollectionType; // FTT, check this
				SetPersonNameType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetGivenName()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonNameType_LocalPtr item = CreatePersonNameType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNameComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->SetGivenName(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetGivenName()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("LinkingName")) == 0)
	{
		// LinkingName is contained in itemtype PersonNameType_LocalType
		// in choice collection PersonNameType_CollectionType

		context->Found = true;
		Mp7JrsPersonNameType_CollectionPtr coll = GetPersonNameType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonNameType_CollectionType; // FTT, check this
				SetPersonNameType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetLinkingName()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonNameType_LocalPtr item = CreatePersonNameType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNameComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->SetLinkingName(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetLinkingName()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FamilyName")) == 0)
	{
		// FamilyName is contained in itemtype PersonNameType_LocalType
		// in choice collection PersonNameType_CollectionType

		context->Found = true;
		Mp7JrsPersonNameType_CollectionPtr coll = GetPersonNameType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonNameType_CollectionType; // FTT, check this
				SetPersonNameType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetFamilyName()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonNameType_LocalPtr item = CreatePersonNameType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNameComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->SetFamilyName(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetFamilyName()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Title")) == 0)
	{
		// Title is contained in itemtype PersonNameType_LocalType
		// in choice collection PersonNameType_CollectionType

		context->Found = true;
		Mp7JrsPersonNameType_CollectionPtr coll = GetPersonNameType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonNameType_CollectionType; // FTT, check this
				SetPersonNameType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetTitle()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonNameType_LocalPtr item = CreatePersonNameType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNameComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->SetTitle(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetTitle()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Salutation")) == 0)
	{
		// Salutation is contained in itemtype PersonNameType_LocalType
		// in choice collection PersonNameType_CollectionType

		context->Found = true;
		Mp7JrsPersonNameType_CollectionPtr coll = GetPersonNameType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePersonNameType_CollectionType; // FTT, check this
				SetPersonNameType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetSalutation()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsPersonNameType_LocalPtr item = CreatePersonNameType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:NameComponentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsNameComponentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->SetSalutation(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsPersonNameType_LocalPtr)coll->elementAt(i))->GetSalutation()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PersonNameType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PersonNameType");
	}
	return result;
}

XMLCh * Mp7JrsPersonNameType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPersonNameType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPersonNameType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PersonNameType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dateFrom_Exist)
	{
	// Pattern
	if (m_dateFrom != Mp7JrstimePointPtr())
	{
		XMLCh * tmp = m_dateFrom->ToText();
		element->setAttributeNS(X(""), X("dateFrom"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_dateTo_Exist)
	{
	// Pattern
	if (m_dateTo != Mp7JrstimePointPtr())
	{
		XMLCh * tmp = m_dateTo->ToText();
		element->setAttributeNS(X(""), X("dateTo"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Enumeration
	if(m_type != Mp7JrsPersonNameType_type_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsPersonNameType_type_LocalType::ToText(m_type);
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lang_Exist)
	{
	// String
	if(m_lang != (XMLCh *)NULL)
	{
		element->setAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("xml:lang"), m_lang);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_PersonNameType_LocalType != Mp7JrsPersonNameType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_PersonNameType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPersonNameType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dateFrom")))
	{
		Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("dateFrom")));
		this->SetdateFrom(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("dateTo")))
	{
		Mp7JrstimePointPtr tmp = CreatetimePointType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("dateTo")));
		this->SetdateTo(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		this->Settype(Mp7JrsPersonNameType_type_LocalType::Parse(parent->getAttribute(X("type"))));
		* current = parent;
	}

	// Deserialize attribute
	// Qualified namespace handling required 
	if(parent->hasAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang")))
	{
		// Deserialize string type
		this->Setlang(Dc1Convert::TextToString(parent->getAttributeNS(X("http://www.w3.org/XML/1998/namespace"), X("lang"))));
		* current = parent;
	}
	parent = Dc1Util::GetNextChild(parent);
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:GivenName
			Dc1Util::HasNodeName(parent, X("GivenName"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:GivenName")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:LinkingName
			Dc1Util::HasNodeName(parent, X("LinkingName"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:LinkingName")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:FamilyName
			Dc1Util::HasNodeName(parent, X("FamilyName"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:FamilyName")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Title
			Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Title")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Salutation
			Dc1Util::HasNodeName(parent, X("Salutation"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Salutation")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:Numeration
			Dc1Util::HasNodeName(parent, X("Numeration"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Numeration")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsPersonNameType_CollectionPtr tmp = CreatePersonNameType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPersonNameType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPersonNameType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("GivenName"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("GivenName")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("LinkingName"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("LinkingName")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("FamilyName"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("FamilyName")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Title"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Title")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Salutation"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Salutation")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Numeration"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Numeration")) == 0)
	)
  {
	Mp7JrsPersonNameType_CollectionPtr tmp = CreatePersonNameType_CollectionType; // FTT, check this
	this->SetPersonNameType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPersonNameType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPersonNameType_LocalType() != Dc1NodePtr())
		result.Insert(GetPersonNameType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPersonNameType_ExtMethodImpl.h


