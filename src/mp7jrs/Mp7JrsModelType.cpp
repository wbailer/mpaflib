
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsModelType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsModelType::IMp7JrsModelType()
{

// no includefile for extension defined 
// file Mp7JrsModelType_ExtPropInit.h

}

IMp7JrsModelType::~IMp7JrsModelType()
{
// no includefile for extension defined 
// file Mp7JrsModelType_ExtPropCleanup.h

}

Mp7JrsModelType::Mp7JrsModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsModelType::~Mp7JrsModelType()
{
	Cleanup();
}

void Mp7JrsModelType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_confidence = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_confidence->SetContent(0.0); // Use Value initialiser (xxx2)
	m_confidence_Exist = false;
	m_reliability = CreatezeroToOneType; // Create a valid class, FTT, check this
	m_reliability->SetContent(0.0); // Use Value initialiser (xxx2)
	m_reliability_Exist = false;



// no includefile for extension defined 
// file Mp7JrsModelType_ExtMyPropInit.h

}

void Mp7JrsModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_confidence);
	// Dc1Factory::DeleteObject(m_reliability);
}

void Mp7JrsModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ModelTypePtr, since we
	// might need GetBase(), which isn't defined in IModelType
	const Dc1Ptr< Mp7JrsModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_confidence);
	if (tmp->Existconfidence())
	{
		this->Setconfidence(Dc1Factory::CloneObject(tmp->Getconfidence()));
	}
	else
	{
		Invalidateconfidence();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_reliability);
	if (tmp->Existreliability())
	{
		this->Setreliability(Dc1Factory::CloneObject(tmp->Getreliability()));
	}
	else
	{
		Invalidatereliability();
	}
	}
}

Dc1NodePtr Mp7JrsModelType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsModelType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsModelType::GetBase() const
{
	return m_Base;
}

Mp7JrszeroToOnePtr Mp7JrsModelType::Getconfidence() const
{
	return m_confidence;
}

bool Mp7JrsModelType::Existconfidence() const
{
	return m_confidence_Exist;
}
void Mp7JrsModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::Setconfidence().");
	}
	m_confidence = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_confidence) m_confidence->SetParent(m_myself.getPointer());
	m_confidence_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::Invalidateconfidence()
{
	m_confidence_Exist = false;
}
Mp7JrszeroToOnePtr Mp7JrsModelType::Getreliability() const
{
	return m_reliability;
}

bool Mp7JrsModelType::Existreliability() const
{
	return m_reliability_Exist;
}
void Mp7JrsModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::Setreliability().");
	}
	m_reliability = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_reliability) m_reliability->SetParent(m_myself.getPointer());
	m_reliability_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::Invalidatereliability()
{
	m_reliability_Exist = false;
}
XMLCh * Mp7JrsModelType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsModelType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsModelType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsModelType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsModelType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsModelType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsModelType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsModelType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsModelType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsModelType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsModelType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsModelType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("confidence")) == 0)
	{
		// confidence is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("reliability")) == 0)
	{
		// reliability is simple attribute zeroToOneType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrszeroToOnePtr");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ModelType");
	}
	return result;
}

XMLCh * Mp7JrsModelType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsModelType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ModelType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_confidence_Exist)
	{
	// Class
	if (m_confidence != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_confidence->ToText();
		element->setAttributeNS(X(""), X("confidence"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_reliability_Exist)
	{
	// Class
	if (m_reliability != Mp7JrszeroToOnePtr())
	{
		XMLCh * tmp = m_reliability->ToText();
		element->setAttributeNS(X(""), X("reliability"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("confidence")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("confidence")));
		this->Setconfidence(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("reliability")))
	{
		// Deserialize class type
		Mp7JrszeroToOnePtr tmp = CreatezeroToOneType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("reliability")));
		this->Setreliability(tmp);
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsModelType_ExtMethodImpl.h


