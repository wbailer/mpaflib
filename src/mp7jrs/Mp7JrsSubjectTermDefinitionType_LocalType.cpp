
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtImplInclude.h


#include "Mp7JrsSubjectTermDefinitionType_Classification_CollectionType.h"
#include "Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionType.h"
#include "Mp7JrsSubjectTermDefinitionType_Note_CollectionType.h"
#include "Mp7JrsSubjectTermDefinitionType_Term_CollectionType.h"
#include "Mp7JrsSubjectTermDefinitionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSubjectTermDefinitionType_LocalType::IMp7JrsSubjectTermDefinitionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtPropInit.h

}

IMp7JrsSubjectTermDefinitionType_LocalType::~IMp7JrsSubjectTermDefinitionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtPropCleanup.h

}

Mp7JrsSubjectTermDefinitionType_LocalType::Mp7JrsSubjectTermDefinitionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSubjectTermDefinitionType_LocalType::~Mp7JrsSubjectTermDefinitionType_LocalType()
{
	Cleanup();
}

void Mp7JrsSubjectTermDefinitionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Classification = Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr(); // Collection
	m_Subdivision = Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr(); // Collection
	m_Note = Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr(); // Collection
	m_Term = Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSubjectTermDefinitionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Classification);
	// Dc1Factory::DeleteObject(m_Subdivision);
	// Dc1Factory::DeleteObject(m_Note);
	// Dc1Factory::DeleteObject(m_Term);
}

void Mp7JrsSubjectTermDefinitionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SubjectTermDefinitionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISubjectTermDefinitionType_LocalType
	const Dc1Ptr< Mp7JrsSubjectTermDefinitionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Classification);
		this->SetClassification(Dc1Factory::CloneObject(tmp->GetClassification()));
		// Dc1Factory::DeleteObject(m_Subdivision);
		this->SetSubdivision(Dc1Factory::CloneObject(tmp->GetSubdivision()));
		// Dc1Factory::DeleteObject(m_Note);
		this->SetNote(Dc1Factory::CloneObject(tmp->GetNote()));
		// Dc1Factory::DeleteObject(m_Term);
		this->SetTerm(Dc1Factory::CloneObject(tmp->GetTerm()));
}

Dc1NodePtr Mp7JrsSubjectTermDefinitionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSubjectTermDefinitionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr Mp7JrsSubjectTermDefinitionType_LocalType::GetClassification() const
{
		return m_Classification;
}

Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr Mp7JrsSubjectTermDefinitionType_LocalType::GetSubdivision() const
{
		return m_Subdivision;
}

Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr Mp7JrsSubjectTermDefinitionType_LocalType::GetNote() const
{
		return m_Note;
}

Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr Mp7JrsSubjectTermDefinitionType_LocalType::GetTerm() const
{
		return m_Term;
}

// implementing setter for choice 
/* element */
void Mp7JrsSubjectTermDefinitionType_LocalType::SetClassification(const Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSubjectTermDefinitionType_LocalType::SetClassification().");
	}
	if (m_Classification != item)
	{
		// Dc1Factory::DeleteObject(m_Classification);
		m_Classification = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Classification) m_Classification->SetParent(m_myself.getPointer());
		if(m_Classification != Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr())
		{
			m_Classification->SetContentName(XMLString::transcode("Classification"));
		}
	// Dc1Factory::DeleteObject(m_Subdivision);
	m_Subdivision = Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Note);
	m_Note = Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Term);
	m_Term = Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSubjectTermDefinitionType_LocalType::SetSubdivision(const Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSubjectTermDefinitionType_LocalType::SetSubdivision().");
	}
	if (m_Subdivision != item)
	{
		// Dc1Factory::DeleteObject(m_Subdivision);
		m_Subdivision = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Subdivision) m_Subdivision->SetParent(m_myself.getPointer());
		if(m_Subdivision != Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr())
		{
			m_Subdivision->SetContentName(XMLString::transcode("Subdivision"));
		}
	// Dc1Factory::DeleteObject(m_Classification);
	m_Classification = Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Note);
	m_Note = Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Term);
	m_Term = Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSubjectTermDefinitionType_LocalType::SetNote(const Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSubjectTermDefinitionType_LocalType::SetNote().");
	}
	if (m_Note != item)
	{
		// Dc1Factory::DeleteObject(m_Note);
		m_Note = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Note) m_Note->SetParent(m_myself.getPointer());
		if(m_Note != Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr())
		{
			m_Note->SetContentName(XMLString::transcode("Note"));
		}
	// Dc1Factory::DeleteObject(m_Classification);
	m_Classification = Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Subdivision);
	m_Subdivision = Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Term);
	m_Term = Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSubjectTermDefinitionType_LocalType::SetTerm(const Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSubjectTermDefinitionType_LocalType::SetTerm().");
	}
	if (m_Term != item)
	{
		// Dc1Factory::DeleteObject(m_Term);
		m_Term = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Term) m_Term->SetParent(m_myself.getPointer());
		if(m_Term != Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr())
		{
			m_Term->SetContentName(XMLString::transcode("Term"));
		}
	// Dc1Factory::DeleteObject(m_Classification);
	m_Classification = Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Subdivision);
	m_Subdivision = Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr();
	// Dc1Factory::DeleteObject(m_Note);
	m_Note = Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SubjectTermDefinitionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSubjectTermDefinitionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSubjectTermDefinitionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSubjectTermDefinitionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSubjectTermDefinitionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	if (m_Classification != Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Classification->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Subdivision != Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Subdivision->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Note != Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Note->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Term != Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Term->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSubjectTermDefinitionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Classification"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Classification")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr tmp = CreateSubjectTermDefinitionType_Classification_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClassification(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Subdivision"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Subdivision")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr tmp = CreateSubjectTermDefinitionType_Subdivision_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSubdivision(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Note"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Note")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr tmp = CreateSubjectTermDefinitionType_Note_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNote(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Term"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Term")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr tmp = CreateSubjectTermDefinitionType_Term_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTerm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSubjectTermDefinitionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Classification")) == 0))
  {
	Mp7JrsSubjectTermDefinitionType_Classification_CollectionPtr tmp = CreateSubjectTermDefinitionType_Classification_CollectionType; // FTT, check this
	this->SetClassification(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Subdivision")) == 0))
  {
	Mp7JrsSubjectTermDefinitionType_Subdivision_CollectionPtr tmp = CreateSubjectTermDefinitionType_Subdivision_CollectionType; // FTT, check this
	this->SetSubdivision(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Note")) == 0))
  {
	Mp7JrsSubjectTermDefinitionType_Note_CollectionPtr tmp = CreateSubjectTermDefinitionType_Note_CollectionType; // FTT, check this
	this->SetNote(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Term")) == 0))
  {
	Mp7JrsSubjectTermDefinitionType_Term_CollectionPtr tmp = CreateSubjectTermDefinitionType_Term_CollectionType; // FTT, check this
	this->SetTerm(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSubjectTermDefinitionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetClassification() != Dc1NodePtr())
		result.Insert(GetClassification());
	if (GetSubdivision() != Dc1NodePtr())
		result.Insert(GetSubdivision());
	if (GetNote() != Dc1NodePtr())
		result.Insert(GetNote());
	if (GetTerm() != Dc1NodePtr())
		result.Insert(GetTerm());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSubjectTermDefinitionType_LocalType_ExtMethodImpl.h


