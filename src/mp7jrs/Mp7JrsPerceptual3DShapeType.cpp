
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalType.h"
#include "Mp7JrsPerceptual3DShapeType_IsConnected_LocalType.h"
#include "Mp7JrsPerceptual3DShapeType_CollectionType.h"
#include "Mp7JrsPerceptual3DShapeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPerceptual3DShapeType_LocalType.h" // Sequence collection Volume
#include "Mp7Jrsunsigned15.h" // Sequence collection element Volume

#include <assert.h>
IMp7JrsPerceptual3DShapeType::IMp7JrsPerceptual3DShapeType()
{

// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtPropInit.h

}

IMp7JrsPerceptual3DShapeType::~IMp7JrsPerceptual3DShapeType()
{
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtPropCleanup.h

}

Mp7JrsPerceptual3DShapeType::Mp7JrsPerceptual3DShapeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPerceptual3DShapeType::~Mp7JrsPerceptual3DShapeType()
{
	Cleanup();
}

void Mp7JrsPerceptual3DShapeType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_BitsPerAttribute = Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr(); // Class with content 
	m_IsConnected = Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr(); // Class
	m_IsConnected_Exist = false;
	m_Perceptual3DShapeType_LocalType = Mp7JrsPerceptual3DShapeType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtMyPropInit.h

}

void Mp7JrsPerceptual3DShapeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_BitsPerAttribute);
	// Dc1Factory::DeleteObject(m_IsConnected);
	// Dc1Factory::DeleteObject(m_Perceptual3DShapeType_LocalType);
}

void Mp7JrsPerceptual3DShapeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use Perceptual3DShapeTypePtr, since we
	// might need GetBase(), which isn't defined in IPerceptual3DShapeType
	const Dc1Ptr< Mp7JrsPerceptual3DShapeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsPerceptual3DShapeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_BitsPerAttribute);
		this->SetBitsPerAttribute(Dc1Factory::CloneObject(tmp->GetBitsPerAttribute()));
	if (tmp->IsValidIsConnected())
	{
		// Dc1Factory::DeleteObject(m_IsConnected);
		this->SetIsConnected(Dc1Factory::CloneObject(tmp->GetIsConnected()));
	}
	else
	{
		InvalidateIsConnected();
	}
		// Dc1Factory::DeleteObject(m_Perceptual3DShapeType_LocalType);
		this->SetPerceptual3DShapeType_LocalType(Dc1Factory::CloneObject(tmp->GetPerceptual3DShapeType_LocalType()));
}

Dc1NodePtr Mp7JrsPerceptual3DShapeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsPerceptual3DShapeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsPerceptual3DShapeType::GetBase() const
{
	return m_Base;
}

Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr Mp7JrsPerceptual3DShapeType::GetBitsPerAttribute() const
{
		return m_BitsPerAttribute;
}

Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr Mp7JrsPerceptual3DShapeType::GetIsConnected() const
{
		return m_IsConnected;
}

// Element is optional
bool Mp7JrsPerceptual3DShapeType::IsValidIsConnected() const
{
	return m_IsConnected_Exist;
}

Mp7JrsPerceptual3DShapeType_CollectionPtr Mp7JrsPerceptual3DShapeType::GetPerceptual3DShapeType_LocalType() const
{
		return m_Perceptual3DShapeType_LocalType;
}

void Mp7JrsPerceptual3DShapeType::SetBitsPerAttribute(const Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType::SetBitsPerAttribute().");
	}
	if (m_BitsPerAttribute != item)
	{
		// Dc1Factory::DeleteObject(m_BitsPerAttribute);
		m_BitsPerAttribute = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_BitsPerAttribute) m_BitsPerAttribute->SetParent(m_myself.getPointer());
		if (m_BitsPerAttribute && m_BitsPerAttribute->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_BitsPerAttribute_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_BitsPerAttribute->UseTypeAttribute = true;
		}
		if(m_BitsPerAttribute != Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr())
		{
			m_BitsPerAttribute->SetContentName(XMLString::transcode("BitsPerAttribute"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType::SetIsConnected(const Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType::SetIsConnected().");
	}
	if (m_IsConnected != item || m_IsConnected_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_IsConnected);
		m_IsConnected = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IsConnected) m_IsConnected->SetParent(m_myself.getPointer());
		if (m_IsConnected && m_IsConnected->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_IsConnected_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IsConnected->UseTypeAttribute = true;
		}
		m_IsConnected_Exist = true;
		if(m_IsConnected != Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr())
		{
			m_IsConnected->SetContentName(XMLString::transcode("IsConnected"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPerceptual3DShapeType::InvalidateIsConnected()
{
	m_IsConnected_Exist = false;
}
void Mp7JrsPerceptual3DShapeType::SetPerceptual3DShapeType_LocalType(const Mp7JrsPerceptual3DShapeType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPerceptual3DShapeType::SetPerceptual3DShapeType_LocalType().");
	}
	if (m_Perceptual3DShapeType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_Perceptual3DShapeType_LocalType);
		m_Perceptual3DShapeType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Perceptual3DShapeType_LocalType) m_Perceptual3DShapeType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPerceptual3DShapeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("BitsPerAttribute")) == 0)
	{
		// BitsPerAttribute is simple element Perceptual3DShapeType_BitsPerAttribute_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetBitsPerAttribute()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_BitsPerAttribute_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetBitsPerAttribute(p, client);
					if((p = GetBitsPerAttribute()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IsConnected")) == 0)
	{
		// IsConnected is simple element Perceptual3DShapeType_IsConnected_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIsConnected()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:Perceptual3DShapeType_IsConnected_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIsConnected(p, client);
					if((p = GetIsConnected()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Volume")) == 0)
	{
		// Volume is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetVolume()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetVolume(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetVolume());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetVolume(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetVolume());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetVolume(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetVolume()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Center_X")) == 0)
	{
		// Center_X is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_X()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetCenter_X());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetCenter_X());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetCenter_X(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_X()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Center_Y")) == 0)
	{
		// Center_Y is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_Y()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetCenter_Y());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetCenter_Y());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetCenter_Y(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_Y()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Center_Z")) == 0)
	{
		// Center_Z is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_Z()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetCenter_Z());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetCenter_Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetCenter_Z());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetCenter_Z(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetCenter_Z()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_1X")) == 0)
	{
		// PCA_Axis_1X is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1X()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_1X());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_1X());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_1X(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1X()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_1Y")) == 0)
	{
		// PCA_Axis_1Y is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1Y()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_1Y());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_1Y());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_1Y(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1Y()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_1Z")) == 0)
	{
		// PCA_Axis_1Z is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1Z()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_1Z());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_1Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_1Z());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_1Z(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_1Z()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_2X")) == 0)
	{
		// PCA_Axis_2X is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2X()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_2X());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2X(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_2X());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_2X(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2X()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_2Y")) == 0)
	{
		// PCA_Axis_2Y is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2Y()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_2Y());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2Y(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_2Y());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_2Y(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2Y()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PCA_Axis_2Z")) == 0)
	{
		// PCA_Axis_2Z is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2Z()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetPCA_Axis_2Z());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetPCA_Axis_2Z(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetPCA_Axis_2Z());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetPCA_Axis_2Z(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetPCA_Axis_2Z()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max_1")) == 0)
	{
		// Max_1 is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_1()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_1(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetMax_1());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_1(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetMax_1());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetMax_1(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_1()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max_2")) == 0)
	{
		// Max_2 is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_2()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_2(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetMax_2());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_2(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetMax_2());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetMax_2(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_2()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Max_3")) == 0)
	{
		// Max_3 is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_3()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_3(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetMax_3());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetMax_3(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetMax_3());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetMax_3(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetMax_3()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Convexity")) == 0)
	{
		// Convexity is contained in itemtype Perceptual3DShapeType_LocalType
		// in sequence collection Perceptual3DShapeType_CollectionType

		context->Found = true;
		Mp7JrsPerceptual3DShapeType_CollectionPtr coll = GetPerceptual3DShapeType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
				SetPerceptual3DShapeType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 255))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetConvexity()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsPerceptual3DShapeType_LocalPtr item = CreatePerceptual3DShapeType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetConvexity(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j+1))->GetConvexity());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j))->SetConvexity(
						((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(j-1))->GetConvexity());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned15")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned15Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->SetConvexity(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsPerceptual3DShapeType_LocalPtr)coll->elementAt(i))->GetConvexity()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for Perceptual3DShapeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "Perceptual3DShapeType");
	}
	return result;
}

XMLCh * Mp7JrsPerceptual3DShapeType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsPerceptual3DShapeType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsPerceptual3DShapeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("Perceptual3DShapeType"));
	// Element serialization:
	// Class with content		
	
	if (m_BitsPerAttribute != Mp7JrsPerceptual3DShapeType_BitsPerAttribute_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_BitsPerAttribute->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("BitsPerAttribute"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_BitsPerAttribute->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_IsConnected != Mp7JrsPerceptual3DShapeType_IsConnected_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IsConnected->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IsConnected"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IsConnected->Serialize(doc, element, &elem);
	}
	if (m_Perceptual3DShapeType_LocalType != Mp7JrsPerceptual3DShapeType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Perceptual3DShapeType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPerceptual3DShapeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("BitsPerAttribute"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("BitsPerAttribute")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptual3DShapeType_BitsPerAttribute_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetBitsPerAttribute(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IsConnected"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IsConnected")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePerceptual3DShapeType_IsConnected_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIsConnected(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Volume")) == 0)
			(Dc1Util::HasNodeName(parent, X("Volume"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Center_X")) == 0)
			(Dc1Util::HasNodeName(parent, X("Center_X"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Center_Y")) == 0)
			(Dc1Util::HasNodeName(parent, X("Center_Y"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Center_Z")) == 0)
			(Dc1Util::HasNodeName(parent, X("Center_Z"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_1X")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_1X"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_1Y")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_1Y"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_1Z")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_1Z"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_2X")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_2X"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_2Y")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_2Y"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:PCA_Axis_2Z")) == 0)
			(Dc1Util::HasNodeName(parent, X("PCA_Axis_2Z"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Max_1")) == 0)
			(Dc1Util::HasNodeName(parent, X("Max_1"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Max_2")) == 0)
			(Dc1Util::HasNodeName(parent, X("Max_2"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Max_3")) == 0)
			(Dc1Util::HasNodeName(parent, X("Max_3"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Convexity")) == 0)
			(Dc1Util::HasNodeName(parent, X("Convexity"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsPerceptual3DShapeType_CollectionPtr tmp = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPerceptual3DShapeType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPerceptual3DShapeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("BitsPerAttribute")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptual3DShapeType_BitsPerAttribute_LocalType; // FTT, check this
	}
	this->SetBitsPerAttribute(child);
  }
  if (XMLString::compareString(elementname, X("IsConnected")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePerceptual3DShapeType_IsConnected_LocalType; // FTT, check this
	}
	this->SetIsConnected(child);
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsPerceptual3DShapeType_CollectionPtr tmp = CreatePerceptual3DShapeType_CollectionType; // FTT, check this
	this->SetPerceptual3DShapeType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPerceptual3DShapeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetBitsPerAttribute() != Dc1NodePtr())
		result.Insert(GetBitsPerAttribute());
	if (GetIsConnected() != Dc1NodePtr())
		result.Insert(GetIsConnected());
	if (GetPerceptual3DShapeType_LocalType() != Dc1NodePtr())
		result.Insert(GetPerceptual3DShapeType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPerceptual3DShapeType_ExtMethodImpl.h


