
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtImplInclude.h


#include "Mp7JrsVisualDSType.h"
#include "Mp7JrsDominantColorType.h"
#include "Mp7JrsScalableColorType.h"
#include "Mp7JrsColorStructureType.h"
#include "Mp7JrsColorLayoutType.h"
#include "Mp7JrsColorTemperatureType.h"
#include "Mp7JrsIlluminationInvariantColorType.h"
#include "Mp7JrsEdgeHistogramType.h"
#include "Mp7JrsHomogeneousTextureType.h"
#include "Mp7JrsTextureBrowsingType.h"
#include "Mp7JrsRegionShapeType.h"
#include "Mp7JrsContourShapeType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsStillRegionFeatureType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsStillRegionFeatureType::IMp7JrsStillRegionFeatureType()
{

// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtPropInit.h

}

IMp7JrsStillRegionFeatureType::~IMp7JrsStillRegionFeatureType()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtPropCleanup.h

}

Mp7JrsStillRegionFeatureType::Mp7JrsStillRegionFeatureType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStillRegionFeatureType::~Mp7JrsStillRegionFeatureType()
{
	Cleanup();
}

void Mp7JrsStillRegionFeatureType::Init()
{
	// Init base
	m_Base = CreateVisualDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_DominantColor = Mp7JrsDominantColorPtr(); // Class
	m_DominantColor_Exist = false;
	m_ScalableColor = Mp7JrsScalableColorPtr(); // Class
	m_ScalableColor_Exist = false;
	m_ColorStructure = Mp7JrsColorStructurePtr(); // Class
	m_ColorStructure_Exist = false;
	m_ColorLayout = Mp7JrsColorLayoutPtr(); // Class
	m_ColorLayout_Exist = false;
	m_ColorTemperature = Mp7JrsColorTemperaturePtr(); // Class
	m_ColorTemperature_Exist = false;
	m_IlluminationCompensatedColor = Mp7JrsIlluminationInvariantColorPtr(); // Class
	m_IlluminationCompensatedColor_Exist = false;
	m_Edge = Mp7JrsEdgeHistogramPtr(); // Class
	m_Edge_Exist = false;
	m_HomogeneousPattern = Mp7JrsHomogeneousTexturePtr(); // Class
	m_HomogeneousPattern_Exist = false;
	m_TextureBrowsing = Mp7JrsTextureBrowsingPtr(); // Class
	m_TextureBrowsing_Exist = false;
	m_ShapeMask = Mp7JrsRegionShapePtr(); // Class
	m_ShapeMask_Exist = false;
	m_Contour = Mp7JrsContourShapePtr(); // Class
	m_Contour_Exist = false;


// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMyPropInit.h

}

void Mp7JrsStillRegionFeatureType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_DominantColor);
	// Dc1Factory::DeleteObject(m_ScalableColor);
	// Dc1Factory::DeleteObject(m_ColorStructure);
	// Dc1Factory::DeleteObject(m_ColorLayout);
	// Dc1Factory::DeleteObject(m_ColorTemperature);
	// Dc1Factory::DeleteObject(m_IlluminationCompensatedColor);
	// Dc1Factory::DeleteObject(m_Edge);
	// Dc1Factory::DeleteObject(m_HomogeneousPattern);
	// Dc1Factory::DeleteObject(m_TextureBrowsing);
	// Dc1Factory::DeleteObject(m_ShapeMask);
	// Dc1Factory::DeleteObject(m_Contour);
}

void Mp7JrsStillRegionFeatureType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StillRegionFeatureTypePtr, since we
	// might need GetBase(), which isn't defined in IStillRegionFeatureType
	const Dc1Ptr< Mp7JrsStillRegionFeatureType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStillRegionFeatureType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidDominantColor())
	{
		// Dc1Factory::DeleteObject(m_DominantColor);
		this->SetDominantColor(Dc1Factory::CloneObject(tmp->GetDominantColor()));
	}
	else
	{
		InvalidateDominantColor();
	}
	if (tmp->IsValidScalableColor())
	{
		// Dc1Factory::DeleteObject(m_ScalableColor);
		this->SetScalableColor(Dc1Factory::CloneObject(tmp->GetScalableColor()));
	}
	else
	{
		InvalidateScalableColor();
	}
	if (tmp->IsValidColorStructure())
	{
		// Dc1Factory::DeleteObject(m_ColorStructure);
		this->SetColorStructure(Dc1Factory::CloneObject(tmp->GetColorStructure()));
	}
	else
	{
		InvalidateColorStructure();
	}
	if (tmp->IsValidColorLayout())
	{
		// Dc1Factory::DeleteObject(m_ColorLayout);
		this->SetColorLayout(Dc1Factory::CloneObject(tmp->GetColorLayout()));
	}
	else
	{
		InvalidateColorLayout();
	}
	if (tmp->IsValidColorTemperature())
	{
		// Dc1Factory::DeleteObject(m_ColorTemperature);
		this->SetColorTemperature(Dc1Factory::CloneObject(tmp->GetColorTemperature()));
	}
	else
	{
		InvalidateColorTemperature();
	}
	if (tmp->IsValidIlluminationCompensatedColor())
	{
		// Dc1Factory::DeleteObject(m_IlluminationCompensatedColor);
		this->SetIlluminationCompensatedColor(Dc1Factory::CloneObject(tmp->GetIlluminationCompensatedColor()));
	}
	else
	{
		InvalidateIlluminationCompensatedColor();
	}
	if (tmp->IsValidEdge())
	{
		// Dc1Factory::DeleteObject(m_Edge);
		this->SetEdge(Dc1Factory::CloneObject(tmp->GetEdge()));
	}
	else
	{
		InvalidateEdge();
	}
	if (tmp->IsValidHomogeneousPattern())
	{
		// Dc1Factory::DeleteObject(m_HomogeneousPattern);
		this->SetHomogeneousPattern(Dc1Factory::CloneObject(tmp->GetHomogeneousPattern()));
	}
	else
	{
		InvalidateHomogeneousPattern();
	}
	if (tmp->IsValidTextureBrowsing())
	{
		// Dc1Factory::DeleteObject(m_TextureBrowsing);
		this->SetTextureBrowsing(Dc1Factory::CloneObject(tmp->GetTextureBrowsing()));
	}
	else
	{
		InvalidateTextureBrowsing();
	}
	if (tmp->IsValidShapeMask())
	{
		// Dc1Factory::DeleteObject(m_ShapeMask);
		this->SetShapeMask(Dc1Factory::CloneObject(tmp->GetShapeMask()));
	}
	else
	{
		InvalidateShapeMask();
	}
	if (tmp->IsValidContour())
	{
		// Dc1Factory::DeleteObject(m_Contour);
		this->SetContour(Dc1Factory::CloneObject(tmp->GetContour()));
	}
	else
	{
		InvalidateContour();
	}
}

Dc1NodePtr Mp7JrsStillRegionFeatureType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsStillRegionFeatureType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDSType > Mp7JrsStillRegionFeatureType::GetBase() const
{
	return m_Base;
}

Mp7JrsDominantColorPtr Mp7JrsStillRegionFeatureType::GetDominantColor() const
{
		return m_DominantColor;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidDominantColor() const
{
	return m_DominantColor_Exist;
}

Mp7JrsScalableColorPtr Mp7JrsStillRegionFeatureType::GetScalableColor() const
{
		return m_ScalableColor;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidScalableColor() const
{
	return m_ScalableColor_Exist;
}

Mp7JrsColorStructurePtr Mp7JrsStillRegionFeatureType::GetColorStructure() const
{
		return m_ColorStructure;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidColorStructure() const
{
	return m_ColorStructure_Exist;
}

Mp7JrsColorLayoutPtr Mp7JrsStillRegionFeatureType::GetColorLayout() const
{
		return m_ColorLayout;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidColorLayout() const
{
	return m_ColorLayout_Exist;
}

Mp7JrsColorTemperaturePtr Mp7JrsStillRegionFeatureType::GetColorTemperature() const
{
		return m_ColorTemperature;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidColorTemperature() const
{
	return m_ColorTemperature_Exist;
}

Mp7JrsIlluminationInvariantColorPtr Mp7JrsStillRegionFeatureType::GetIlluminationCompensatedColor() const
{
		return m_IlluminationCompensatedColor;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidIlluminationCompensatedColor() const
{
	return m_IlluminationCompensatedColor_Exist;
}

Mp7JrsEdgeHistogramPtr Mp7JrsStillRegionFeatureType::GetEdge() const
{
		return m_Edge;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidEdge() const
{
	return m_Edge_Exist;
}

Mp7JrsHomogeneousTexturePtr Mp7JrsStillRegionFeatureType::GetHomogeneousPattern() const
{
		return m_HomogeneousPattern;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidHomogeneousPattern() const
{
	return m_HomogeneousPattern_Exist;
}

Mp7JrsTextureBrowsingPtr Mp7JrsStillRegionFeatureType::GetTextureBrowsing() const
{
		return m_TextureBrowsing;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidTextureBrowsing() const
{
	return m_TextureBrowsing_Exist;
}

Mp7JrsRegionShapePtr Mp7JrsStillRegionFeatureType::GetShapeMask() const
{
		return m_ShapeMask;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidShapeMask() const
{
	return m_ShapeMask_Exist;
}

Mp7JrsContourShapePtr Mp7JrsStillRegionFeatureType::GetContour() const
{
		return m_Contour;
}

// Element is optional
bool Mp7JrsStillRegionFeatureType::IsValidContour() const
{
	return m_Contour_Exist;
}

void Mp7JrsStillRegionFeatureType::SetDominantColor(const Mp7JrsDominantColorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetDominantColor().");
	}
	if (m_DominantColor != item || m_DominantColor_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_DominantColor);
		m_DominantColor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_DominantColor) m_DominantColor->SetParent(m_myself.getPointer());
		if (m_DominantColor && m_DominantColor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_DominantColor->UseTypeAttribute = true;
		}
		m_DominantColor_Exist = true;
		if(m_DominantColor != Mp7JrsDominantColorPtr())
		{
			m_DominantColor->SetContentName(XMLString::transcode("DominantColor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateDominantColor()
{
	m_DominantColor_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetScalableColor(const Mp7JrsScalableColorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetScalableColor().");
	}
	if (m_ScalableColor != item || m_ScalableColor_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ScalableColor);
		m_ScalableColor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ScalableColor) m_ScalableColor->SetParent(m_myself.getPointer());
		if (m_ScalableColor && m_ScalableColor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ScalableColor->UseTypeAttribute = true;
		}
		m_ScalableColor_Exist = true;
		if(m_ScalableColor != Mp7JrsScalableColorPtr())
		{
			m_ScalableColor->SetContentName(XMLString::transcode("ScalableColor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateScalableColor()
{
	m_ScalableColor_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetColorStructure(const Mp7JrsColorStructurePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetColorStructure().");
	}
	if (m_ColorStructure != item || m_ColorStructure_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorStructure);
		m_ColorStructure = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorStructure) m_ColorStructure->SetParent(m_myself.getPointer());
		if (m_ColorStructure && m_ColorStructure->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorStructureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorStructure->UseTypeAttribute = true;
		}
		m_ColorStructure_Exist = true;
		if(m_ColorStructure != Mp7JrsColorStructurePtr())
		{
			m_ColorStructure->SetContentName(XMLString::transcode("ColorStructure"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateColorStructure()
{
	m_ColorStructure_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetColorLayout(const Mp7JrsColorLayoutPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetColorLayout().");
	}
	if (m_ColorLayout != item || m_ColorLayout_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorLayout);
		m_ColorLayout = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorLayout) m_ColorLayout->SetParent(m_myself.getPointer());
		if (m_ColorLayout && m_ColorLayout->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorLayout->UseTypeAttribute = true;
		}
		m_ColorLayout_Exist = true;
		if(m_ColorLayout != Mp7JrsColorLayoutPtr())
		{
			m_ColorLayout->SetContentName(XMLString::transcode("ColorLayout"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateColorLayout()
{
	m_ColorLayout_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetColorTemperature(const Mp7JrsColorTemperaturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetColorTemperature().");
	}
	if (m_ColorTemperature != item || m_ColorTemperature_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorTemperature);
		m_ColorTemperature = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorTemperature) m_ColorTemperature->SetParent(m_myself.getPointer());
		if (m_ColorTemperature && m_ColorTemperature->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorTemperatureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorTemperature->UseTypeAttribute = true;
		}
		m_ColorTemperature_Exist = true;
		if(m_ColorTemperature != Mp7JrsColorTemperaturePtr())
		{
			m_ColorTemperature->SetContentName(XMLString::transcode("ColorTemperature"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateColorTemperature()
{
	m_ColorTemperature_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetIlluminationCompensatedColor(const Mp7JrsIlluminationInvariantColorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetIlluminationCompensatedColor().");
	}
	if (m_IlluminationCompensatedColor != item || m_IlluminationCompensatedColor_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_IlluminationCompensatedColor);
		m_IlluminationCompensatedColor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_IlluminationCompensatedColor) m_IlluminationCompensatedColor->SetParent(m_myself.getPointer());
		if (m_IlluminationCompensatedColor && m_IlluminationCompensatedColor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_IlluminationCompensatedColor->UseTypeAttribute = true;
		}
		m_IlluminationCompensatedColor_Exist = true;
		if(m_IlluminationCompensatedColor != Mp7JrsIlluminationInvariantColorPtr())
		{
			m_IlluminationCompensatedColor->SetContentName(XMLString::transcode("IlluminationCompensatedColor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateIlluminationCompensatedColor()
{
	m_IlluminationCompensatedColor_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetEdge(const Mp7JrsEdgeHistogramPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetEdge().");
	}
	if (m_Edge != item || m_Edge_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Edge);
		m_Edge = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Edge) m_Edge->SetParent(m_myself.getPointer());
		if (m_Edge && m_Edge->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EdgeHistogramType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Edge->UseTypeAttribute = true;
		}
		m_Edge_Exist = true;
		if(m_Edge != Mp7JrsEdgeHistogramPtr())
		{
			m_Edge->SetContentName(XMLString::transcode("Edge"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateEdge()
{
	m_Edge_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetHomogeneousPattern(const Mp7JrsHomogeneousTexturePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetHomogeneousPattern().");
	}
	if (m_HomogeneousPattern != item || m_HomogeneousPattern_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HomogeneousPattern);
		m_HomogeneousPattern = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HomogeneousPattern) m_HomogeneousPattern->SetParent(m_myself.getPointer());
		if (m_HomogeneousPattern && m_HomogeneousPattern->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HomogeneousTextureType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HomogeneousPattern->UseTypeAttribute = true;
		}
		m_HomogeneousPattern_Exist = true;
		if(m_HomogeneousPattern != Mp7JrsHomogeneousTexturePtr())
		{
			m_HomogeneousPattern->SetContentName(XMLString::transcode("HomogeneousPattern"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateHomogeneousPattern()
{
	m_HomogeneousPattern_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetTextureBrowsing(const Mp7JrsTextureBrowsingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetTextureBrowsing().");
	}
	if (m_TextureBrowsing != item || m_TextureBrowsing_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_TextureBrowsing);
		m_TextureBrowsing = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TextureBrowsing) m_TextureBrowsing->SetParent(m_myself.getPointer());
		if (m_TextureBrowsing && m_TextureBrowsing->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TextureBrowsing->UseTypeAttribute = true;
		}
		m_TextureBrowsing_Exist = true;
		if(m_TextureBrowsing != Mp7JrsTextureBrowsingPtr())
		{
			m_TextureBrowsing->SetContentName(XMLString::transcode("TextureBrowsing"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateTextureBrowsing()
{
	m_TextureBrowsing_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetShapeMask(const Mp7JrsRegionShapePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetShapeMask().");
	}
	if (m_ShapeMask != item || m_ShapeMask_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ShapeMask);
		m_ShapeMask = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ShapeMask) m_ShapeMask->SetParent(m_myself.getPointer());
		if (m_ShapeMask && m_ShapeMask->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionShapeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ShapeMask->UseTypeAttribute = true;
		}
		m_ShapeMask_Exist = true;
		if(m_ShapeMask != Mp7JrsRegionShapePtr())
		{
			m_ShapeMask->SetContentName(XMLString::transcode("ShapeMask"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateShapeMask()
{
	m_ShapeMask_Exist = false;
}
void Mp7JrsStillRegionFeatureType::SetContour(const Mp7JrsContourShapePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetContour().");
	}
	if (m_Contour != item || m_Contour_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Contour);
		m_Contour = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Contour) m_Contour->SetParent(m_myself.getPointer());
		if (m_Contour && m_Contour->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Contour->UseTypeAttribute = true;
		}
		m_Contour_Exist = true;
		if(m_Contour != Mp7JrsContourShapePtr())
		{
			m_Contour->SetContentName(XMLString::transcode("Contour"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidateContour()
{
	m_Contour_Exist = false;
}
XMLCh * Mp7JrsStillRegionFeatureType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsStillRegionFeatureType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsStillRegionFeatureType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegionFeatureType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsStillRegionFeatureType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsStillRegionFeatureType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsStillRegionFeatureType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsStillRegionFeatureType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsStillRegionFeatureType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsStillRegionFeatureType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsStillRegionFeatureType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsStillRegionFeatureType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsStillRegionFeatureType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsStillRegionFeatureType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsStillRegionFeatureType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStillRegionFeatureType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsStillRegionFeatureType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsStillRegionFeatureType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStillRegionFeatureType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStillRegionFeatureType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("DominantColor")) == 0)
	{
		// DominantColor is simple element DominantColorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDominantColor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDominantColorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDominantColor(p, client);
					if((p = GetDominantColor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ScalableColor")) == 0)
	{
		// ScalableColor is simple element ScalableColorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetScalableColor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ScalableColorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsScalableColorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetScalableColor(p, client);
					if((p = GetScalableColor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ColorStructure")) == 0)
	{
		// ColorStructure is simple element ColorStructureType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorStructure()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorStructureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorStructurePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorStructure(p, client);
					if((p = GetColorStructure()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ColorLayout")) == 0)
	{
		// ColorLayout is simple element ColorLayoutType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorLayout()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorLayoutType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorLayoutPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorLayout(p, client);
					if((p = GetColorLayout()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ColorTemperature")) == 0)
	{
		// ColorTemperature is simple element ColorTemperatureType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorTemperature()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorTemperatureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorTemperaturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorTemperature(p, client);
					if((p = GetColorTemperature()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IlluminationCompensatedColor")) == 0)
	{
		// IlluminationCompensatedColor is simple element IlluminationInvariantColorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetIlluminationCompensatedColor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IlluminationInvariantColorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIlluminationInvariantColorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetIlluminationCompensatedColor(p, client);
					if((p = GetIlluminationCompensatedColor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Edge")) == 0)
	{
		// Edge is simple element EdgeHistogramType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEdge()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:EdgeHistogramType")))) != empty)
			{
				// Is type allowed
				Mp7JrsEdgeHistogramPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEdge(p, client);
					if((p = GetEdge()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("HomogeneousPattern")) == 0)
	{
		// HomogeneousPattern is simple element HomogeneousTextureType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHomogeneousPattern()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HomogeneousTextureType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHomogeneousTexturePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHomogeneousPattern(p, client);
					if((p = GetHomogeneousPattern()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("TextureBrowsing")) == 0)
	{
		// TextureBrowsing is simple element TextureBrowsingType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTextureBrowsing()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextureBrowsingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextureBrowsingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTextureBrowsing(p, client);
					if((p = GetTextureBrowsing()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ShapeMask")) == 0)
	{
		// ShapeMask is simple element RegionShapeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShapeMask()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RegionShapeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRegionShapePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShapeMask(p, client);
					if((p = GetShapeMask()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Contour")) == 0)
	{
		// Contour is simple element ContourShapeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetContour()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ContourShapeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsContourShapePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetContour(p, client);
					if((p = GetContour()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StillRegionFeatureType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StillRegionFeatureType");
	}
	return result;
}

XMLCh * Mp7JrsStillRegionFeatureType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStillRegionFeatureType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStillRegionFeatureType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StillRegionFeatureType"));
	// Element serialization:
	// Class
	
	if (m_DominantColor != Mp7JrsDominantColorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_DominantColor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("DominantColor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_DominantColor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ScalableColor != Mp7JrsScalableColorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ScalableColor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ScalableColor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ScalableColor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ColorStructure != Mp7JrsColorStructurePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorStructure->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorStructure"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorStructure->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ColorLayout != Mp7JrsColorLayoutPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorLayout->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorLayout"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorLayout->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ColorTemperature != Mp7JrsColorTemperaturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorTemperature->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorTemperature"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorTemperature->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_IlluminationCompensatedColor != Mp7JrsIlluminationInvariantColorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_IlluminationCompensatedColor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("IlluminationCompensatedColor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_IlluminationCompensatedColor->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Edge != Mp7JrsEdgeHistogramPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Edge->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Edge"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Edge->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_HomogeneousPattern != Mp7JrsHomogeneousTexturePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HomogeneousPattern->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HomogeneousPattern"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HomogeneousPattern->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_TextureBrowsing != Mp7JrsTextureBrowsingPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_TextureBrowsing->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("TextureBrowsing"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_TextureBrowsing->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ShapeMask != Mp7JrsRegionShapePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ShapeMask->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ShapeMask"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ShapeMask->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Contour != Mp7JrsContourShapePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Contour->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Contour"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Contour->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsStillRegionFeatureType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("DominantColor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("DominantColor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateDominantColorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDominantColor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ScalableColor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ScalableColor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateScalableColorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetScalableColor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorStructure"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorStructure")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorStructureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorStructure(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorLayout"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorLayout")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorLayoutType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorLayout(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorTemperature"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorTemperature")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorTemperatureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorTemperature(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("IlluminationCompensatedColor"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("IlluminationCompensatedColor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateIlluminationInvariantColorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetIlluminationCompensatedColor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Edge"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Edge")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateEdgeHistogramType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetEdge(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HomogeneousPattern"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HomogeneousPattern")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateHomogeneousTextureType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHomogeneousPattern(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("TextureBrowsing"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TextureBrowsing")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextureBrowsingType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetTextureBrowsing(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ShapeMask"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ShapeMask")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRegionShapeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShapeMask(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Contour"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Contour")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateContourShapeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetContour(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStillRegionFeatureType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("DominantColor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateDominantColorType; // FTT, check this
	}
	this->SetDominantColor(child);
  }
  if (XMLString::compareString(elementname, X("ScalableColor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateScalableColorType; // FTT, check this
	}
	this->SetScalableColor(child);
  }
  if (XMLString::compareString(elementname, X("ColorStructure")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorStructureType; // FTT, check this
	}
	this->SetColorStructure(child);
  }
  if (XMLString::compareString(elementname, X("ColorLayout")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorLayoutType; // FTT, check this
	}
	this->SetColorLayout(child);
  }
  if (XMLString::compareString(elementname, X("ColorTemperature")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorTemperatureType; // FTT, check this
	}
	this->SetColorTemperature(child);
  }
  if (XMLString::compareString(elementname, X("IlluminationCompensatedColor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateIlluminationInvariantColorType; // FTT, check this
	}
	this->SetIlluminationCompensatedColor(child);
  }
  if (XMLString::compareString(elementname, X("Edge")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateEdgeHistogramType; // FTT, check this
	}
	this->SetEdge(child);
  }
  if (XMLString::compareString(elementname, X("HomogeneousPattern")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateHomogeneousTextureType; // FTT, check this
	}
	this->SetHomogeneousPattern(child);
  }
  if (XMLString::compareString(elementname, X("TextureBrowsing")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextureBrowsingType; // FTT, check this
	}
	this->SetTextureBrowsing(child);
  }
  if (XMLString::compareString(elementname, X("ShapeMask")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRegionShapeType; // FTT, check this
	}
	this->SetShapeMask(child);
  }
  if (XMLString::compareString(elementname, X("Contour")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateContourShapeType; // FTT, check this
	}
	this->SetContour(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStillRegionFeatureType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetDominantColor() != Dc1NodePtr())
		result.Insert(GetDominantColor());
	if (GetScalableColor() != Dc1NodePtr())
		result.Insert(GetScalableColor());
	if (GetColorStructure() != Dc1NodePtr())
		result.Insert(GetColorStructure());
	if (GetColorLayout() != Dc1NodePtr())
		result.Insert(GetColorLayout());
	if (GetColorTemperature() != Dc1NodePtr())
		result.Insert(GetColorTemperature());
	if (GetIlluminationCompensatedColor() != Dc1NodePtr())
		result.Insert(GetIlluminationCompensatedColor());
	if (GetEdge() != Dc1NodePtr())
		result.Insert(GetEdge());
	if (GetHomogeneousPattern() != Dc1NodePtr())
		result.Insert(GetHomogeneousPattern());
	if (GetTextureBrowsing() != Dc1NodePtr())
		result.Insert(GetTextureBrowsing());
	if (GetShapeMask() != Dc1NodePtr())
		result.Insert(GetShapeMask());
	if (GetContour() != Dc1NodePtr())
		result.Insert(GetContour());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStillRegionFeatureType_ExtMethodImpl.h


