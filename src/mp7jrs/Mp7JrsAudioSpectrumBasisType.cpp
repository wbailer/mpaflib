
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtImplInclude.h


#include "Mp7JrsAudioLLDVectorType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionType.h"
#include "Mp7JrsintegerVector.h"
#include "Mp7JrsAudioSpectrumBasisType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsAudioLLDVectorType_SeriesOfVector_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:SeriesOfVector

#include <assert.h>
IMp7JrsAudioSpectrumBasisType::IMp7JrsAudioSpectrumBasisType()
{

// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtPropInit.h

}

IMp7JrsAudioSpectrumBasisType::~IMp7JrsAudioSpectrumBasisType()
{
// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtPropCleanup.h

}

Mp7JrsAudioSpectrumBasisType::Mp7JrsAudioSpectrumBasisType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioSpectrumBasisType::~Mp7JrsAudioSpectrumBasisType()
{
	Cleanup();
}

void Mp7JrsAudioSpectrumBasisType::Init()
{
	// Init base
	m_Base = CreateAudioLLDVectorType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_loEdge = 0.0f; // Value
	m_loEdge_Default = 62.5; // Default value
	m_loEdge_Exist = false;
	m_hiEdge = 0.0f; // Value
	m_hiEdge_Default = 16000; // Default value
	m_hiEdge_Exist = false;
	m_octaveResolution = Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::UninitializedEnumeration;
	m_octaveResolution_Exist = false;



// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtMyPropInit.h

}

void Mp7JrsAudioSpectrumBasisType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsAudioSpectrumBasisType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioSpectrumBasisTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioSpectrumBasisType
	const Dc1Ptr< Mp7JrsAudioSpectrumBasisType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsAudioLLDVectorPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAudioSpectrumBasisType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->ExistloEdge())
	{
		this->SetloEdge(tmp->GetloEdge());
	}
	else
	{
		InvalidateloEdge();
	}
	}
	{
	if (tmp->ExisthiEdge())
	{
		this->SethiEdge(tmp->GethiEdge());
	}
	else
	{
		InvalidatehiEdge();
	}
	}
	{
	if (tmp->ExistoctaveResolution())
	{
		this->SetoctaveResolution(tmp->GetoctaveResolution());
	}
	else
	{
		InvalidateoctaveResolution();
	}
	}
}

Dc1NodePtr Mp7JrsAudioSpectrumBasisType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAudioSpectrumBasisType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsAudioLLDVectorType > Mp7JrsAudioSpectrumBasisType::GetBase() const
{
	return m_Base;
}

float Mp7JrsAudioSpectrumBasisType::GetloEdge() const
{
	if (this->ExistloEdge()) {
		return m_loEdge;
	} else {
		return m_loEdge_Default;
	}
}

bool Mp7JrsAudioSpectrumBasisType::ExistloEdge() const
{
	return m_loEdge_Exist;
}
void Mp7JrsAudioSpectrumBasisType::SetloEdge(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::SetloEdge().");
	}
	m_loEdge = item;
	m_loEdge_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSpectrumBasisType::InvalidateloEdge()
{
	m_loEdge_Exist = false;
}
float Mp7JrsAudioSpectrumBasisType::GethiEdge() const
{
	if (this->ExisthiEdge()) {
		return m_hiEdge;
	} else {
		return m_hiEdge_Default;
	}
}

bool Mp7JrsAudioSpectrumBasisType::ExisthiEdge() const
{
	return m_hiEdge_Exist;
}
void Mp7JrsAudioSpectrumBasisType::SethiEdge(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::SethiEdge().");
	}
	m_hiEdge = item;
	m_hiEdge_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSpectrumBasisType::InvalidatehiEdge()
{
	m_hiEdge_Exist = false;
}
Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::Enumeration Mp7JrsAudioSpectrumBasisType::GetoctaveResolution() const
{
	return m_octaveResolution;
}

bool Mp7JrsAudioSpectrumBasisType::ExistoctaveResolution() const
{
	return m_octaveResolution_Exist;
}
void Mp7JrsAudioSpectrumBasisType::SetoctaveResolution(Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::SetoctaveResolution().");
	}
	m_octaveResolution = item;
	m_octaveResolution_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSpectrumBasisType::InvalidateoctaveResolution()
{
	m_octaveResolution_Exist = false;
}
Mp7JrsfloatVectorPtr Mp7JrsAudioSpectrumBasisType::GetVector() const
{
	return GetBase()->GetVector();
}

Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr Mp7JrsAudioSpectrumBasisType::GetSeriesOfVector() const
{
	return GetBase()->GetSeriesOfVector();
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioSpectrumBasisType::SetVector(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::SetVector().");
	}
	GetBase()->SetVector(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioSpectrumBasisType::SetSeriesOfVector(const Mp7JrsAudioLLDVectorType_SeriesOfVector_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::SetSeriesOfVector().");
	}
	GetBase()->SetSeriesOfVector(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsintegerVectorPtr Mp7JrsAudioSpectrumBasisType::Getchannels() const
{
	return GetBase()->GetBase()->Getchannels();
}

bool Mp7JrsAudioSpectrumBasisType::Existchannels() const
{
	return GetBase()->GetBase()->Existchannels();
}
void Mp7JrsAudioSpectrumBasisType::Setchannels(const Mp7JrsintegerVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioSpectrumBasisType::Setchannels().");
	}
	GetBase()->GetBase()->Setchannels(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioSpectrumBasisType::Invalidatechannels()
{
	GetBase()->GetBase()->Invalidatechannels();
}

Dc1NodeEnum Mp7JrsAudioSpectrumBasisType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("loEdge")) == 0)
	{
		// loEdge is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("hiEdge")) == 0)
	{
		// hiEdge is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("octaveResolution")) == 0)
	{
		// octaveResolution is simple attribute audioSpectrumAttributeGrp_octaveResolution_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::Enumeration");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioSpectrumBasisType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioSpectrumBasisType");
	}
	return result;
}

XMLCh * Mp7JrsAudioSpectrumBasisType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioSpectrumBasisType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsAudioSpectrumBasisType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioSpectrumBasisType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_loEdge_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_loEdge);
		element->setAttributeNS(X(""), X("loEdge"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_hiEdge_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_hiEdge);
		element->setAttributeNS(X(""), X("hiEdge"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_octaveResolution_Exist)
	{
	// Enumeration
	if(m_octaveResolution != Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::ToText(m_octaveResolution);
		element->setAttributeNS(X(""), X("octaveResolution"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:

}

bool Mp7JrsAudioSpectrumBasisType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("loEdge")))
	{
		// deserialize value type
		this->SetloEdge(Dc1Convert::TextToFloat(parent->getAttribute(X("loEdge"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("hiEdge")))
	{
		// deserialize value type
		this->SethiEdge(Dc1Convert::TextToFloat(parent->getAttribute(X("hiEdge"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("octaveResolution")))
	{
		this->SetoctaveResolution(Mp7JrsaudioSpectrumAttributeGrp_octaveResolution_LocalType::Parse(parent->getAttribute(X("octaveResolution"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsAudioLLDVectorType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioSpectrumBasisType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioSpectrumBasisType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetVector() != Dc1NodePtr())
		result.Insert(GetVector());
	if (GetSeriesOfVector() != Dc1NodePtr())
		result.Insert(GetSeriesOfVector());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioSpectrumBasisType_ExtMethodImpl.h


