
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtImplInclude.h


#include "Mp7Jrsunsigned16.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsSpokenContentLatticeType_Block_Node_CollectionType.h"
#include "Mp7JrsSpokenContentLatticeType_Block_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsSpokenContentLatticeType_Block_Node_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Node

#include <assert.h>
IMp7JrsSpokenContentLatticeType_Block_LocalType::IMp7JrsSpokenContentLatticeType_Block_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtPropInit.h

}

IMp7JrsSpokenContentLatticeType_Block_LocalType::~IMp7JrsSpokenContentLatticeType_Block_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtPropCleanup.h

}

Mp7JrsSpokenContentLatticeType_Block_LocalType::Mp7JrsSpokenContentLatticeType_Block_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpokenContentLatticeType_Block_LocalType::~Mp7JrsSpokenContentLatticeType_Block_LocalType()
{
	Cleanup();
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::Init()
{

	// Init attributes
	m_defaultSpeakerInfoRef = NULL; // String
	m_num = Createunsigned16; // Create a valid class, FTT, check this
	m_num->SetContent(0); // Use Value initialiser (xxx2)
	m_audio = Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::UninitializedEnumeration;
	m_audio_Default = Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::speech; // Default enumeration
	m_audio_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_MediaTime = Mp7JrsMediaTimePtr(); // Class
	m_Node = Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_defaultSpeakerInfoRef); // String
	// Dc1Factory::DeleteObject(m_num);
	// Dc1Factory::DeleteObject(m_MediaTime);
	// Dc1Factory::DeleteObject(m_Node);
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpokenContentLatticeType_Block_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpokenContentLatticeType_Block_LocalType
	const Dc1Ptr< Mp7JrsSpokenContentLatticeType_Block_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_defaultSpeakerInfoRef); // String
		this->SetdefaultSpeakerInfoRef(XMLString::replicate(tmp->GetdefaultSpeakerInfoRef()));
	}
	{
	// Dc1Factory::DeleteObject(m_num);
		this->Setnum(Dc1Factory::CloneObject(tmp->Getnum()));
	}
	{
	if (tmp->Existaudio())
	{
		this->Setaudio(tmp->Getaudio());
	}
	else
	{
		Invalidateaudio();
	}
	}
		// Dc1Factory::DeleteObject(m_MediaTime);
		this->SetMediaTime(Dc1Factory::CloneObject(tmp->GetMediaTime()));
		// Dc1Factory::DeleteObject(m_Node);
		this->SetNode(Dc1Factory::CloneObject(tmp->GetNode()));
}

Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsSpokenContentLatticeType_Block_LocalType::GetdefaultSpeakerInfoRef() const
{
	return m_defaultSpeakerInfoRef;
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::SetdefaultSpeakerInfoRef(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_LocalType::SetdefaultSpeakerInfoRef().");
	}
	m_defaultSpeakerInfoRef = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7Jrsunsigned16Ptr Mp7JrsSpokenContentLatticeType_Block_LocalType::Getnum() const
{
	return m_num;
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::Setnum(const Mp7Jrsunsigned16Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_LocalType::Setnum().");
	}
	m_num = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_num) m_num->SetParent(m_myself.getPointer());
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::Enumeration Mp7JrsSpokenContentLatticeType_Block_LocalType::Getaudio() const
{
	if (this->Existaudio()) {
		return m_audio;
	} else {
		return m_audio_Default;
	}
}

bool Mp7JrsSpokenContentLatticeType_Block_LocalType::Existaudio() const
{
	return m_audio_Exist;
}
void Mp7JrsSpokenContentLatticeType_Block_LocalType::Setaudio(Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_LocalType::Setaudio().");
	}
	m_audio = item;
	m_audio_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::Invalidateaudio()
{
	m_audio_Exist = false;
}
Mp7JrsMediaTimePtr Mp7JrsSpokenContentLatticeType_Block_LocalType::GetMediaTime() const
{
		return m_MediaTime;
}

Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr Mp7JrsSpokenContentLatticeType_Block_LocalType::GetNode() const
{
		return m_Node;
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::SetMediaTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_LocalType::SetMediaTime().");
	}
	if (m_MediaTime != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTime);
		m_MediaTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTime) m_MediaTime->SetParent(m_myself.getPointer());
		if (m_MediaTime && m_MediaTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTime->UseTypeAttribute = true;
		}
		if(m_MediaTime != Mp7JrsMediaTimePtr())
		{
			m_MediaTime->SetContentName(XMLString::transcode("MediaTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsSpokenContentLatticeType_Block_LocalType::SetNode(const Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpokenContentLatticeType_Block_LocalType::SetNode().");
	}
	if (m_Node != item)
	{
		// Dc1Factory::DeleteObject(m_Node);
		m_Node = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Node) m_Node->SetParent(m_myself.getPointer());
		if(m_Node != Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr())
		{
			m_Node->SetContentName(XMLString::transcode("Node"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSpokenContentLatticeType_Block_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("defaultSpeakerInfoRef")) == 0)
	{
		// defaultSpeakerInfoRef is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("num")) == 0)
	{
		// num is simple attribute unsigned16
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7Jrsunsigned16Ptr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audio")) == 0)
	{
		// audio is simple attribute SpokenContentLatticeType_Block_audio_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("MediaTime")) == 0)
	{
		// MediaTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMediaTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMediaTime(p, client);
					if((p = GetMediaTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Node")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Node is item of type SpokenContentLatticeType_Block_Node_LocalType
		// in element collection SpokenContentLatticeType_Block_Node_CollectionType
		
		context->Found = true;
		Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr coll = GetNode();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateSpokenContentLatticeType_Block_Node_CollectionType; // FTT, check this
				SetNode(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 65536))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpokenContentLatticeType_Block_Node_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSpokenContentLatticeType_Block_Node_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for SpokenContentLatticeType_Block_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "SpokenContentLatticeType_Block_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsSpokenContentLatticeType_Block_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpokenContentLatticeType_Block_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpokenContentLatticeType_Block_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SpokenContentLatticeType_Block_LocalType"));
	// Attribute Serialization:
	// String
	if(m_defaultSpeakerInfoRef != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("defaultSpeakerInfoRef"), m_defaultSpeakerInfoRef);
	}
	// Attribute Serialization:
	// Class
	if (m_num != Mp7Jrsunsigned16Ptr())
	{
		XMLCh * tmp = m_num->ToText();
		element->setAttributeNS(X(""), X("num"), tmp);
		XMLString::release(&tmp);
	}
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audio_Exist)
	{
	// Enumeration
	if(m_audio != Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::ToText(m_audio);
		element->setAttributeNS(X(""), X("audio"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_MediaTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaTime->Serialize(doc, element, &elem);
	}
	if (m_Node != Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Node->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSpokenContentLatticeType_Block_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("defaultSpeakerInfoRef")))
	{
		// Deserialize string type
		this->SetdefaultSpeakerInfoRef(Dc1Convert::TextToString(parent->getAttribute(X("defaultSpeakerInfoRef"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("num")))
	{
		// Deserialize class type
		Mp7Jrsunsigned16Ptr tmp = Createunsigned16; // FTT, check this
		tmp->Parse(parent->getAttribute(X("num")));
		this->Setnum(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audio")))
	{
		this->Setaudio(Mp7JrsSpokenContentLatticeType_Block_audio_LocalType::Parse(parent->getAttribute(X("audio"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Node"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Node")) == 0))
		{
			// Deserialize factory type
			Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetNode(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpokenContentLatticeType_Block_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetMediaTime(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Node")) == 0))
  {
	Mp7JrsSpokenContentLatticeType_Block_Node_CollectionPtr tmp = CreateSpokenContentLatticeType_Block_Node_CollectionType; // FTT, check this
	this->SetNode(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpokenContentLatticeType_Block_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMediaTime() != Dc1NodePtr())
		result.Insert(GetMediaTime());
	if (GetNode() != Dc1NodePtr())
		result.Insert(GetNode());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpokenContentLatticeType_Block_LocalType_ExtMethodImpl.h


