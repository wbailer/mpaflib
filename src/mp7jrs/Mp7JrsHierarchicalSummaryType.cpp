
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtImplInclude.h


#include "Mp7JrsSummaryType.h"
#include "Mp7JrsHierarchicalSummaryType_components_CollectionType.h"
#include "Mp7JrsSummaryThemeListType.h"
#include "Mp7JrsHierarchicalSummaryType_CollectionType.h"
#include "Mp7JrsSummaryType_Name_CollectionType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsHierarchicalSummaryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Name
#include "Mp7JrsHierarchicalSummaryType_LocalType.h" // Choice collection SummarySegmentGroup
#include "Mp7JrsSummarySegmentGroupType.h" // Choice collection element SummarySegmentGroup

#include <assert.h>
IMp7JrsHierarchicalSummaryType::IMp7JrsHierarchicalSummaryType()
{

// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtPropInit.h

}

IMp7JrsHierarchicalSummaryType::~IMp7JrsHierarchicalSummaryType()
{
// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtPropCleanup.h

}

Mp7JrsHierarchicalSummaryType::Mp7JrsHierarchicalSummaryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHierarchicalSummaryType::~Mp7JrsHierarchicalSummaryType()
{
	Cleanup();
}

void Mp7JrsHierarchicalSummaryType::Init()
{
	// Init base
	m_Base = CreateSummaryType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_components = Mp7JrsHierarchicalSummaryType_components_CollectionPtr(); // Collection
	m_components_Exist = false;
	m_hierarchy = Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::UninitializedEnumeration;

	// Init elements (element, union sequence choice all any)
	
	m_SummaryThemeList = Mp7JrsSummaryThemeListPtr(); // Class
	m_SummaryThemeList_Exist = false;
	m_HierarchicalSummaryType_LocalType = Mp7JrsHierarchicalSummaryType_CollectionPtr(); // Collection


// begin extension included
// file Mp7JrsHierarchicalSummaryType_ExtMyPropInit.h

	// set by default independent
	m_hierarchy = Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::independent;

	// create an empty segment group collection
	m_HierarchicalSummaryType_LocalType = CreateHierarchicalSummaryType_CollectionType;
// end extension included

}

void Mp7JrsHierarchicalSummaryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_components);
	// Dc1Factory::DeleteObject(m_SummaryThemeList);
	// Dc1Factory::DeleteObject(m_HierarchicalSummaryType_LocalType);
}

void Mp7JrsHierarchicalSummaryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HierarchicalSummaryTypePtr, since we
	// might need GetBase(), which isn't defined in IHierarchicalSummaryType
	const Dc1Ptr< Mp7JrsHierarchicalSummaryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsSummaryPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsHierarchicalSummaryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	// Dc1Factory::DeleteObject(m_components);
	if (tmp->Existcomponents())
	{
		this->Setcomponents(Dc1Factory::CloneObject(tmp->Getcomponents()));
	}
	else
	{
		Invalidatecomponents();
	}
	}
	{
		this->Sethierarchy(tmp->Gethierarchy());
	}
	if (tmp->IsValidSummaryThemeList())
	{
		// Dc1Factory::DeleteObject(m_SummaryThemeList);
		this->SetSummaryThemeList(Dc1Factory::CloneObject(tmp->GetSummaryThemeList()));
	}
	else
	{
		InvalidateSummaryThemeList();
	}
		// Dc1Factory::DeleteObject(m_HierarchicalSummaryType_LocalType);
		this->SetHierarchicalSummaryType_LocalType(Dc1Factory::CloneObject(tmp->GetHierarchicalSummaryType_LocalType()));
}

Dc1NodePtr Mp7JrsHierarchicalSummaryType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsHierarchicalSummaryType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsSummaryType > Mp7JrsHierarchicalSummaryType::GetBase() const
{
	return m_Base;
}

Mp7JrsHierarchicalSummaryType_components_CollectionPtr Mp7JrsHierarchicalSummaryType::Getcomponents() const
{
	return m_components;
}

bool Mp7JrsHierarchicalSummaryType::Existcomponents() const
{
	return m_components_Exist;
}
void Mp7JrsHierarchicalSummaryType::Setcomponents(const Mp7JrsHierarchicalSummaryType_components_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::Setcomponents().");
	}
	m_components = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_components) m_components->SetParent(m_myself.getPointer());
	m_components_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::Invalidatecomponents()
{
	m_components_Exist = false;
}
Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::Enumeration Mp7JrsHierarchicalSummaryType::Gethierarchy() const
{
	return m_hierarchy;
}

void Mp7JrsHierarchicalSummaryType::Sethierarchy(Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::Sethierarchy().");
	}
	m_hierarchy = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSummaryThemeListPtr Mp7JrsHierarchicalSummaryType::GetSummaryThemeList() const
{
		return m_SummaryThemeList;
}

// Element is optional
bool Mp7JrsHierarchicalSummaryType::IsValidSummaryThemeList() const
{
	return m_SummaryThemeList_Exist;
}

Mp7JrsHierarchicalSummaryType_CollectionPtr Mp7JrsHierarchicalSummaryType::GetHierarchicalSummaryType_LocalType() const
{
		return m_HierarchicalSummaryType_LocalType;
}

void Mp7JrsHierarchicalSummaryType::SetSummaryThemeList(const Mp7JrsSummaryThemeListPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetSummaryThemeList().");
	}
	if (m_SummaryThemeList != item || m_SummaryThemeList_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SummaryThemeList);
		m_SummaryThemeList = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SummaryThemeList) m_SummaryThemeList->SetParent(m_myself.getPointer());
		if (m_SummaryThemeList && m_SummaryThemeList->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryThemeListType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SummaryThemeList->UseTypeAttribute = true;
		}
		m_SummaryThemeList_Exist = true;
		if(m_SummaryThemeList != Mp7JrsSummaryThemeListPtr())
		{
			m_SummaryThemeList->SetContentName(XMLString::transcode("SummaryThemeList"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidateSummaryThemeList()
{
	m_SummaryThemeList_Exist = false;
}
void Mp7JrsHierarchicalSummaryType::SetHierarchicalSummaryType_LocalType(const Mp7JrsHierarchicalSummaryType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetHierarchicalSummaryType_LocalType().");
	}
	if (m_HierarchicalSummaryType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_HierarchicalSummaryType_LocalType);
		m_HierarchicalSummaryType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HierarchicalSummaryType_LocalType) m_HierarchicalSummaryType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsSummaryType_Name_CollectionPtr Mp7JrsHierarchicalSummaryType::GetName() const
{
	return GetBase()->GetName();
}

Mp7JrsUniqueIDPtr Mp7JrsHierarchicalSummaryType::GetSourceID() const
{
	return GetBase()->GetSourceID();
}

// Element is optional
bool Mp7JrsHierarchicalSummaryType::IsValidSourceID() const
{
	return GetBase()->IsValidSourceID();
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsHierarchicalSummaryType::GetSourceLocator() const
{
	return GetBase()->GetSourceLocator();
}

// Element is optional
bool Mp7JrsHierarchicalSummaryType::IsValidSourceLocator() const
{
	return GetBase()->IsValidSourceLocator();
}

Mp7JrsReferencePtr Mp7JrsHierarchicalSummaryType::GetSourceInformation() const
{
	return GetBase()->GetSourceInformation();
}

// Element is optional
bool Mp7JrsHierarchicalSummaryType::IsValidSourceInformation() const
{
	return GetBase()->IsValidSourceInformation();
}

void Mp7JrsHierarchicalSummaryType::SetName(const Mp7JrsSummaryType_Name_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetName().");
	}
	GetBase()->SetName(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::SetSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetSourceID().");
	}
	GetBase()->SetSourceID(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidateSourceID()
{
	GetBase()->InvalidateSourceID();
}
void Mp7JrsHierarchicalSummaryType::SetSourceLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetSourceLocator().");
	}
	GetBase()->SetSourceLocator(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidateSourceLocator()
{
	GetBase()->InvalidateSourceLocator();
}
void Mp7JrsHierarchicalSummaryType::SetSourceInformation(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetSourceInformation().");
	}
	GetBase()->SetSourceInformation(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidateSourceInformation()
{
	GetBase()->InvalidateSourceInformation();
}
XMLCh * Mp7JrsHierarchicalSummaryType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsHierarchicalSummaryType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsHierarchicalSummaryType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsHierarchicalSummaryType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsHierarchicalSummaryType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsHierarchicalSummaryType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsHierarchicalSummaryType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsHierarchicalSummaryType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsHierarchicalSummaryType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsHierarchicalSummaryType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsHierarchicalSummaryType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsHierarchicalSummaryType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsHierarchicalSummaryType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsHierarchicalSummaryType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsHierarchicalSummaryType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHierarchicalSummaryType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsHierarchicalSummaryType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsHierarchicalSummaryType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHierarchicalSummaryType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsHierarchicalSummaryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  2, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("components")) == 0)
	{
		// components is simple attribute HierarchicalSummaryType_components_CollectionType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsHierarchicalSummaryType_components_CollectionPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("hierarchy")) == 0)
	{
		// hierarchy is simple attribute HierarchicalSummaryType_hierarchy_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummaryThemeList")) == 0)
	{
		// SummaryThemeList is simple element SummaryThemeListType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSummaryThemeList()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummaryThemeListType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummaryThemeListPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSummaryThemeList(p, client);
					if((p = GetSummaryThemeList()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegmentGroup")) == 0)
	{
		// SummarySegmentGroup is contained in itemtype HierarchicalSummaryType_LocalType
		// in choice collection HierarchicalSummaryType_CollectionType

		context->Found = true;
		Mp7JrsHierarchicalSummaryType_CollectionPtr coll = GetHierarchicalSummaryType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateHierarchicalSummaryType_CollectionType; // FTT, check this
				SetHierarchicalSummaryType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->GetSummarySegmentGroup()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsHierarchicalSummaryType_LocalPtr item = CreateHierarchicalSummaryType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSummarySegmentGroupPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->SetSummarySegmentGroup(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->GetSummarySegmentGroup()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SummarySegmentGroupRef")) == 0)
	{
		// SummarySegmentGroupRef is contained in itemtype HierarchicalSummaryType_LocalType
		// in choice collection HierarchicalSummaryType_CollectionType

		context->Found = true;
		Mp7JrsHierarchicalSummaryType_CollectionPtr coll = GetHierarchicalSummaryType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateHierarchicalSummaryType_CollectionType; // FTT, check this
				SetHierarchicalSummaryType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->GetSummarySegmentGroupRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsHierarchicalSummaryType_LocalPtr item = CreateHierarchicalSummaryType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->SetSummarySegmentGroupRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsHierarchicalSummaryType_LocalPtr)coll->elementAt(i))->GetSummarySegmentGroupRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HierarchicalSummaryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HierarchicalSummaryType");
	}
	return result;
}

XMLCh * Mp7JrsHierarchicalSummaryType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsHierarchicalSummaryType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsHierarchicalSummaryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HierarchicalSummaryType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_components_Exist)
	{
	// Collection
	if(m_components != Mp7JrsHierarchicalSummaryType_components_CollectionPtr())
	{
		XMLCh * tmp = m_components->ToText();
		element->setAttributeNS(X(""), X("components"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
	// Enumeration
	if(m_hierarchy != Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::ToText(m_hierarchy);
		element->setAttributeNS(X(""), X("hierarchy"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:
	// Class
	
	if (m_SummaryThemeList != Mp7JrsSummaryThemeListPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SummaryThemeList->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SummaryThemeList"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SummaryThemeList->Serialize(doc, element, &elem);
	}
	if (m_HierarchicalSummaryType_LocalType != Mp7JrsHierarchicalSummaryType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_HierarchicalSummaryType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsHierarchicalSummaryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("components")))
	{
		// Deserialize collection type
		Mp7JrsHierarchicalSummaryType_components_CollectionPtr tmp = CreateHierarchicalSummaryType_components_CollectionType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("components")));
		this->Setcomponents(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("hierarchy")))
	{
		this->Sethierarchy(Mp7JrsHierarchicalSummaryType_hierarchy_LocalType::Parse(parent->getAttribute(X("hierarchy"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsSummaryType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SummaryThemeList"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SummaryThemeList")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSummaryThemeListType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSummaryThemeList(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SummarySegmentGroup
			Dc1Util::HasNodeName(parent, X("SummarySegmentGroup"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroup")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupRef
			Dc1Util::HasNodeName(parent, X("SummarySegmentGroupRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SummarySegmentGroupRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsHierarchicalSummaryType_CollectionPtr tmp = CreateHierarchicalSummaryType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetHierarchicalSummaryType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsHierarchicalSummaryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHierarchicalSummaryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("SummaryThemeList")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSummaryThemeListType; // FTT, check this
	}
	this->SetSummaryThemeList(child);
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegmentGroup"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegmentGroup")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SummarySegmentGroupRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SummarySegmentGroupRef")) == 0)
	)
  {
	Mp7JrsHierarchicalSummaryType_CollectionPtr tmp = CreateHierarchicalSummaryType_CollectionType; // FTT, check this
	this->SetHierarchicalSummaryType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHierarchicalSummaryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSummaryThemeList() != Dc1NodePtr())
		result.Insert(GetSummaryThemeList());
	if (GetHierarchicalSummaryType_LocalType() != Dc1NodePtr())
		result.Insert(GetHierarchicalSummaryType_LocalType());
	if (GetName() != Dc1NodePtr())
		result.Insert(GetName());
	if (GetSourceID() != Dc1NodePtr())
		result.Insert(GetSourceID());
	if (GetSourceLocator() != Dc1NodePtr())
		result.Insert(GetSourceLocator());
	if (GetSourceInformation() != Dc1NodePtr())
		result.Insert(GetSourceInformation());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// begin extension included
// file Mp7JrsHierarchicalSummaryType_ExtMethodImpl.h
#include "Mp7JrsSummarySegmentGroupType.h"
#include "Mp7JrsHierarchicalSummaryType_LocalType.h"

void IMp7JrsHierarchicalSummaryType::AddSegmentGroup(Mp7JrsSummarySegmentGroupPtr segmentGroup, Dc1ClientID client) {
	Mp7JrsHierarchicalSummaryType_LocalPtr seggrp = CreateHierarchicalSummaryType_LocalType;
	seggrp->SetSummarySegmentGroup(segmentGroup,client);
	GetHierarchicalSummaryType_LocalType()->addElement(seggrp,client);
}
// end extension included


