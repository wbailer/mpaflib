
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtImplInclude.h


#include "Mp7JrsMaskType.h"
#include "Mp7JrsSceneGraphMaskType_CollectionType.h"
#include "Mp7JrsSceneGraphMaskType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsSceneGraphMaskType::IMp7JrsSceneGraphMaskType()
{

// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtPropInit.h

}

IMp7JrsSceneGraphMaskType::~IMp7JrsSceneGraphMaskType()
{
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtPropCleanup.h

}

Mp7JrsSceneGraphMaskType::Mp7JrsSceneGraphMaskType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSceneGraphMaskType::~Mp7JrsSceneGraphMaskType()
{
	Cleanup();
}

void Mp7JrsSceneGraphMaskType::Init()
{
	// Init base
	m_Base = CreateMaskType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_SceneGraphMaskType_LocalType = Mp7JrsSceneGraphMaskType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtMyPropInit.h

}

void Mp7JrsSceneGraphMaskType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_SceneGraphMaskType_LocalType);
}

void Mp7JrsSceneGraphMaskType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SceneGraphMaskTypePtr, since we
	// might need GetBase(), which isn't defined in ISceneGraphMaskType
	const Dc1Ptr< Mp7JrsSceneGraphMaskType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMaskPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsSceneGraphMaskType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_SceneGraphMaskType_LocalType);
		this->SetSceneGraphMaskType_LocalType(Dc1Factory::CloneObject(tmp->GetSceneGraphMaskType_LocalType()));
}

Dc1NodePtr Mp7JrsSceneGraphMaskType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsSceneGraphMaskType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMaskType > Mp7JrsSceneGraphMaskType::GetBase() const
{
	return m_Base;
}

Mp7JrsSceneGraphMaskType_CollectionPtr Mp7JrsSceneGraphMaskType::GetSceneGraphMaskType_LocalType() const
{
		return m_SceneGraphMaskType_LocalType;
}

void Mp7JrsSceneGraphMaskType::SetSceneGraphMaskType_LocalType(const Mp7JrsSceneGraphMaskType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSceneGraphMaskType::SetSceneGraphMaskType_LocalType().");
	}
	if (m_SceneGraphMaskType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_SceneGraphMaskType_LocalType);
		m_SceneGraphMaskType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SceneGraphMaskType_LocalType) m_SceneGraphMaskType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsSceneGraphMaskType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	if (isentrypoint)
	{
		// No Dc1NodePtr subelement to get or create for SceneGraphMaskType
		Dc1XPathParseContext::ErrorNoSubElement(context, "SceneGraphMaskType");
	}
	return result;
}

XMLCh * Mp7JrsSceneGraphMaskType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsSceneGraphMaskType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsSceneGraphMaskType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("SceneGraphMaskType"));
	// Element serialization:
	if (m_SceneGraphMaskType_LocalType != Mp7JrsSceneGraphMaskType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_SceneGraphMaskType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsSceneGraphMaskType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMaskType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:SubGraph
			Dc1Util::HasNodeName(parent, X("SubGraph"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SubGraph")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:SubGraphNum
			Dc1Util::HasNodeName(parent, X("SubGraphNum"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:SubGraphNum")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsSceneGraphMaskType_CollectionPtr tmp = CreateSceneGraphMaskType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetSceneGraphMaskType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSceneGraphMaskType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SubGraph"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SubGraph")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("SubGraphNum"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("SubGraphNum")) == 0)
	)
  {
	Mp7JrsSceneGraphMaskType_CollectionPtr tmp = CreateSceneGraphMaskType_CollectionType; // FTT, check this
	this->SetSceneGraphMaskType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSceneGraphMaskType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetSceneGraphMaskType_LocalType() != Dc1NodePtr())
		result.Insert(GetSceneGraphMaskType_LocalType());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_ExtMethodImpl.h


