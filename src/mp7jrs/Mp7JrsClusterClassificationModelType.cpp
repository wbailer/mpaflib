
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtImplInclude.h


#include "Mp7JrsClassificationModelType.h"
#include "Mp7JrsClusterClassificationModelType_CollectionType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsClusterClassificationModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsClusterClassificationModelType_LocalType.h" // Choice collection ClusterModel
#include "Mp7JrsClusterModelType.h" // Choice collection element ClusterModel
#include "Mp7JrsReferenceType.h" // Choice collection element ClusterModelRef

#include <assert.h>
IMp7JrsClusterClassificationModelType::IMp7JrsClusterClassificationModelType()
{

// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtPropInit.h

}

IMp7JrsClusterClassificationModelType::~IMp7JrsClusterClassificationModelType()
{
// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtPropCleanup.h

}

Mp7JrsClusterClassificationModelType::Mp7JrsClusterClassificationModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClusterClassificationModelType::~Mp7JrsClusterClassificationModelType()
{
	Cleanup();
}

void Mp7JrsClusterClassificationModelType::Init()
{
	// Init base
	m_Base = CreateClassificationModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ClusterClassificationModelType_LocalType = Mp7JrsClusterClassificationModelType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtMyPropInit.h

}

void Mp7JrsClusterClassificationModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ClusterClassificationModelType_LocalType);
}

void Mp7JrsClusterClassificationModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClusterClassificationModelTypePtr, since we
	// might need GetBase(), which isn't defined in IClusterClassificationModelType
	const Dc1Ptr< Mp7JrsClusterClassificationModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsClassificationModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClusterClassificationModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ClusterClassificationModelType_LocalType);
		this->SetClusterClassificationModelType_LocalType(Dc1Factory::CloneObject(tmp->GetClusterClassificationModelType_LocalType()));
}

Dc1NodePtr Mp7JrsClusterClassificationModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClusterClassificationModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsClassificationModelType > Mp7JrsClusterClassificationModelType::GetBase() const
{
	return m_Base;
}

Mp7JrsClusterClassificationModelType_CollectionPtr Mp7JrsClusterClassificationModelType::GetClusterClassificationModelType_LocalType() const
{
		return m_ClusterClassificationModelType_LocalType;
}

void Mp7JrsClusterClassificationModelType::SetClusterClassificationModelType_LocalType(const Mp7JrsClusterClassificationModelType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SetClusterClassificationModelType_LocalType().");
	}
	if (m_ClusterClassificationModelType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_ClusterClassificationModelType_LocalType);
		m_ClusterClassificationModelType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClusterClassificationModelType_LocalType) m_ClusterClassificationModelType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrsClusterClassificationModelType::Getcomplete() const
{
	return GetBase()->Getcomplete();
}

bool Mp7JrsClusterClassificationModelType::Existcomplete() const
{
	return GetBase()->Existcomplete();
}
void Mp7JrsClusterClassificationModelType::Setcomplete(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::Setcomplete().");
	}
	GetBase()->Setcomplete(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::Invalidatecomplete()
{
	GetBase()->Invalidatecomplete();
}
bool Mp7JrsClusterClassificationModelType::Getredundant() const
{
	return GetBase()->Getredundant();
}

bool Mp7JrsClusterClassificationModelType::Existredundant() const
{
	return GetBase()->Existredundant();
}
void Mp7JrsClusterClassificationModelType::Setredundant(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::Setredundant().");
	}
	GetBase()->Setredundant(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::Invalidateredundant()
{
	GetBase()->Invalidateredundant();
}
Mp7JrszeroToOnePtr Mp7JrsClusterClassificationModelType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsClusterClassificationModelType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsClusterClassificationModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsClusterClassificationModelType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsClusterClassificationModelType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsClusterClassificationModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsClusterClassificationModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsClusterClassificationModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsClusterClassificationModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsClusterClassificationModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsClusterClassificationModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsClusterClassificationModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsClusterClassificationModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsClusterClassificationModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsClusterClassificationModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsClusterClassificationModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsClusterClassificationModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsClusterClassificationModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsClusterClassificationModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsClusterClassificationModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsClusterClassificationModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClusterClassificationModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsClusterClassificationModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsClusterClassificationModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClusterClassificationModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClusterClassificationModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 9 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ClusterModel")) == 0)
	{
		// ClusterModel is contained in itemtype ClusterClassificationModelType_LocalType
		// in choice collection ClusterClassificationModelType_CollectionType

		context->Found = true;
		Mp7JrsClusterClassificationModelType_CollectionPtr coll = GetClusterClassificationModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterClassificationModelType_CollectionType; // FTT, check this
				SetClusterClassificationModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->GetClusterModel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterClassificationModelType_LocalPtr item = CreateClusterClassificationModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClusterModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->SetClusterModel(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->GetClusterModel()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClusterModelRef")) == 0)
	{
		// ClusterModelRef is contained in itemtype ClusterClassificationModelType_LocalType
		// in choice collection ClusterClassificationModelType_CollectionType

		context->Found = true;
		Mp7JrsClusterClassificationModelType_CollectionPtr coll = GetClusterClassificationModelType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClusterClassificationModelType_CollectionType; // FTT, check this
				SetClusterClassificationModelType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->GetClusterModelRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsClusterClassificationModelType_LocalPtr item = CreateClusterClassificationModelType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->SetClusterModelRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsClusterClassificationModelType_LocalPtr)coll->elementAt(i))->GetClusterModelRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClusterClassificationModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClusterClassificationModelType");
	}
	return result;
}

XMLCh * Mp7JrsClusterClassificationModelType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsClusterClassificationModelType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsClusterClassificationModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClusterClassificationModelType"));
	// Element serialization:
	if (m_ClusterClassificationModelType_LocalType != Mp7JrsClusterClassificationModelType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClusterClassificationModelType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsClusterClassificationModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsClassificationModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:ClusterModel
			Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModel")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ClusterModelRef
			Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModelRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsClusterClassificationModelType_CollectionPtr tmp = CreateClusterClassificationModelType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClusterClassificationModelType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClusterClassificationModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModel")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModelRef")) == 0)
	)
  {
	Mp7JrsClusterClassificationModelType_CollectionPtr tmp = CreateClusterClassificationModelType_CollectionType; // FTT, check this
	this->SetClusterClassificationModelType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClusterClassificationModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetClusterClassificationModelType_LocalType() != Dc1NodePtr())
		result.Insert(GetClusterClassificationModelType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClusterClassificationModelType_ExtMethodImpl.h


