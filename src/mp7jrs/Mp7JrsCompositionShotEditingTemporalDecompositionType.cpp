
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtImplInclude.h


#include "Mp7JrsTemporalSegmentDecompositionType.h"
#include "Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsCompositionShotEditingTemporalDecompositionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalType.h" // Sequence collection IntraCompositionShot
#include "Mp7JrsIntraCompositionShotType.h" // Sequence collection element IntraCompositionShot
#include "Mp7JrsReferenceType.h" // Sequence collection element IntraCompositionShotRef
#include "Mp7JrsInternalTransitionType.h" // Sequence collection element InternalTransition

#include <assert.h>
IMp7JrsCompositionShotEditingTemporalDecompositionType::IMp7JrsCompositionShotEditingTemporalDecompositionType()
{

// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtPropInit.h

}

IMp7JrsCompositionShotEditingTemporalDecompositionType::~IMp7JrsCompositionShotEditingTemporalDecompositionType()
{
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtPropCleanup.h

}

Mp7JrsCompositionShotEditingTemporalDecompositionType::Mp7JrsCompositionShotEditingTemporalDecompositionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCompositionShotEditingTemporalDecompositionType::~Mp7JrsCompositionShotEditingTemporalDecompositionType()
{
	Cleanup();
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Init()
{
	// Init base
	m_Base = CreateTemporalSegmentDecompositionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_CompositionShotEditingTemporalDecompositionType_LocalType = Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtMyPropInit.h

}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_CompositionShotEditingTemporalDecompositionType_LocalType);
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CompositionShotEditingTemporalDecompositionTypePtr, since we
	// might need GetBase(), which isn't defined in ICompositionShotEditingTemporalDecompositionType
	const Dc1Ptr< Mp7JrsCompositionShotEditingTemporalDecompositionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsTemporalSegmentDecompositionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsCompositionShotEditingTemporalDecompositionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_CompositionShotEditingTemporalDecompositionType_LocalType);
		this->SetCompositionShotEditingTemporalDecompositionType_LocalType(Dc1Factory::CloneObject(tmp->GetCompositionShotEditingTemporalDecompositionType_LocalType()));
}

Dc1NodePtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsTemporalSegmentDecompositionType > Mp7JrsCompositionShotEditingTemporalDecompositionType::GetBase() const
{
	return m_Base;
}

Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetCompositionShotEditingTemporalDecompositionType_LocalType() const
{
		return m_CompositionShotEditingTemporalDecompositionType_LocalType;
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::SetCompositionShotEditingTemporalDecompositionType_LocalType(const Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SetCompositionShotEditingTemporalDecompositionType_LocalType().");
	}
	if (m_CompositionShotEditingTemporalDecompositionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_CompositionShotEditingTemporalDecompositionType_LocalType);
		m_CompositionShotEditingTemporalDecompositionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CompositionShotEditingTemporalDecompositionType_LocalType) m_CompositionShotEditingTemporalDecompositionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsCompositionShotEditingTemporalDecompositionType::Getcriteria() const
{
	return GetBase()->GetBase()->Getcriteria();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Existcriteria() const
{
	return GetBase()->GetBase()->Existcriteria();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::Setcriteria(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::Setcriteria().");
	}
	GetBase()->GetBase()->Setcriteria(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Invalidatecriteria()
{
	GetBase()->GetBase()->Invalidatecriteria();
}
bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Getoverlap() const
{
	return GetBase()->GetBase()->Getoverlap();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Existoverlap() const
{
	return GetBase()->GetBase()->Existoverlap();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::Setoverlap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::Setoverlap().");
	}
	GetBase()->GetBase()->Setoverlap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Invalidateoverlap()
{
	GetBase()->GetBase()->Invalidateoverlap();
}
bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Getgap() const
{
	return GetBase()->GetBase()->Getgap();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Existgap() const
{
	return GetBase()->GetBase()->Existgap();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::Setgap(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::Setgap().");
	}
	GetBase()->GetBase()->Setgap(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Invalidategap()
{
	GetBase()->GetBase()->Invalidategap();
}
XMLCh * Mp7JrsCompositionShotEditingTemporalDecompositionType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsCompositionShotEditingTemporalDecompositionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsCompositionShotEditingTemporalDecompositionType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsCompositionShotEditingTemporalDecompositionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCompositionShotEditingTemporalDecompositionType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsCompositionShotEditingTemporalDecompositionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("IntraCompositionShot")) == 0)
	{
		// IntraCompositionShot is contained in itemtype CompositionShotEditingTemporalDecompositionType_LocalType
		// in sequence collection CompositionShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr coll = GetCompositionShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetCompositionShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetIntraCompositionShot()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr item = CreateCompositionShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetIntraCompositionShot(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetIntraCompositionShot());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetIntraCompositionShot(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetIntraCompositionShot());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotType")))) != empty)
			{
				// Is type allowed
				Mp7JrsIntraCompositionShotPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetIntraCompositionShot(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetIntraCompositionShot()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("IntraCompositionShotRef")) == 0)
	{
		// IntraCompositionShotRef is contained in itemtype CompositionShotEditingTemporalDecompositionType_LocalType
		// in sequence collection CompositionShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr coll = GetCompositionShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetCompositionShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetIntraCompositionShotRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr item = CreateCompositionShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetIntraCompositionShotRef(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetIntraCompositionShotRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetIntraCompositionShotRef(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetIntraCompositionShotRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetIntraCompositionShotRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetIntraCompositionShotRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("InternalTransition")) == 0)
	{
		// InternalTransition is contained in itemtype CompositionShotEditingTemporalDecompositionType_LocalType
		// in sequence collection CompositionShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr coll = GetCompositionShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetCompositionShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetInternalTransition()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr item = CreateCompositionShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetInternalTransition(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetInternalTransition());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetInternalTransition(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetInternalTransition());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InternalTransitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInternalTransitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetInternalTransition(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetInternalTransition()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("InternalTransitionRef")) == 0)
	{
		// InternalTransitionRef is contained in itemtype CompositionShotEditingTemporalDecompositionType_LocalType
		// in sequence collection CompositionShotEditingTemporalDecompositionType_CollectionType

		context->Found = true;
		Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr coll = GetCompositionShotEditingTemporalDecompositionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
				SetCompositionShotEditingTemporalDecompositionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetInternalTransitionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				// context->FreeIndexList.addElement(i); // FTT We don't allow to shift elems between different sequence items.
				++i;
			}
		}
		int start, stop;
		if(Dc1XPathParseContext::PostProcessSequenceCollection(context, i, start, stop, result))
		{
			if(i >= 0)
			{
				Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr item = CreateCompositionShotEditingTemporalDecompositionType_LocalType;
				coll->insertElementAt(item, i, client);
			}
			else // Just reorganise the existing nodes and then we can insert without creating another sequence item.
			{
				int j;
				if(start < stop)
				{
					for(j = start; j < stop; ++j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetInternalTransitionRef(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j+1))->GetInternalTransitionRef());
					}
				}
				else
				{
					for(j = start; j > stop; --j)
					{
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j))->SetInternalTransitionRef(
						((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(j-1))->GetInternalTransitionRef());
					}
				}
				i = stop;
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->SetInternalTransitionRef(p, client);
					 // Maybe some other restrictions are waiting...
					if((p = ((Mp7JrsCompositionShotEditingTemporalDecompositionType_LocalPtr)coll->elementAt(i))->GetInternalTransitionRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CompositionShotEditingTemporalDecompositionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CompositionShotEditingTemporalDecompositionType");
	}
	return result;
}

XMLCh * Mp7JrsCompositionShotEditingTemporalDecompositionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsCompositionShotEditingTemporalDecompositionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CompositionShotEditingTemporalDecompositionType"));
	// Element serialization:
	if (m_CompositionShotEditingTemporalDecompositionType_LocalType != Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_CompositionShotEditingTemporalDecompositionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsCompositionShotEditingTemporalDecompositionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsTemporalSegmentDecompositionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Sequence Collection
		if((parent != NULL) && (
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShot")) == 0)
			(Dc1Util::HasNodeName(parent, X("IntraCompositionShot"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:IntraCompositionShotRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("IntraCompositionShotRef"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:InternalTransition")) == 0)
			(Dc1Util::HasNodeName(parent, X("InternalTransition"), X("urn:mpeg:mpeg7:schema:2004")))
				||
// Old			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:InternalTransitionRef")) == 0)
			(Dc1Util::HasNodeName(parent, X("InternalTransitionRef"), X("urn:mpeg:mpeg7:schema:2004")))
				))
		{
			// Deserialize factory type
			Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr tmp = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCompositionShotEditingTemporalDecompositionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCompositionShotEditingTemporalDecompositionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
	// local type, so no need to check explicitly for a name (BAW 05 04 2004)
  {
	Mp7JrsCompositionShotEditingTemporalDecompositionType_CollectionPtr tmp = CreateCompositionShotEditingTemporalDecompositionType_CollectionType; // FTT, check this
	this->SetCompositionShotEditingTemporalDecompositionType_LocalType(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCompositionShotEditingTemporalDecompositionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCompositionShotEditingTemporalDecompositionType_LocalType() != Dc1NodePtr())
		result.Insert(GetCompositionShotEditingTemporalDecompositionType_LocalType());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCompositionShotEditingTemporalDecompositionType_ExtMethodImpl.h


