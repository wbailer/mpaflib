
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtImplInclude.h


#include "Mp7JrsPlaceType.h"
#include "Mp7JrsTimeType.h"
#include "Mp7JrsCreationType_CreationCoordinates_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsCreationType_CreationCoordinates_LocalType::IMp7JrsCreationType_CreationCoordinates_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtPropInit.h

}

IMp7JrsCreationType_CreationCoordinates_LocalType::~IMp7JrsCreationType_CreationCoordinates_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtPropCleanup.h

}

Mp7JrsCreationType_CreationCoordinates_LocalType::Mp7JrsCreationType_CreationCoordinates_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsCreationType_CreationCoordinates_LocalType::~Mp7JrsCreationType_CreationCoordinates_LocalType()
{
	Cleanup();
}

void Mp7JrsCreationType_CreationCoordinates_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Location = Mp7JrsPlacePtr(); // Class
	m_Location_Exist = false;
	m_Date = Mp7JrsTimePtr(); // Class
	m_Date_Exist = false;


// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtMyPropInit.h

}

void Mp7JrsCreationType_CreationCoordinates_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Location);
	// Dc1Factory::DeleteObject(m_Date);
}

void Mp7JrsCreationType_CreationCoordinates_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use CreationType_CreationCoordinates_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ICreationType_CreationCoordinates_LocalType
	const Dc1Ptr< Mp7JrsCreationType_CreationCoordinates_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidLocation())
	{
		// Dc1Factory::DeleteObject(m_Location);
		this->SetLocation(Dc1Factory::CloneObject(tmp->GetLocation()));
	}
	else
	{
		InvalidateLocation();
	}
	if (tmp->IsValidDate())
	{
		// Dc1Factory::DeleteObject(m_Date);
		this->SetDate(Dc1Factory::CloneObject(tmp->GetDate()));
	}
	else
	{
		InvalidateDate();
	}
}

Dc1NodePtr Mp7JrsCreationType_CreationCoordinates_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsCreationType_CreationCoordinates_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsPlacePtr Mp7JrsCreationType_CreationCoordinates_LocalType::GetLocation() const
{
		return m_Location;
}

// Element is optional
bool Mp7JrsCreationType_CreationCoordinates_LocalType::IsValidLocation() const
{
	return m_Location_Exist;
}

Mp7JrsTimePtr Mp7JrsCreationType_CreationCoordinates_LocalType::GetDate() const
{
		return m_Date;
}

// Element is optional
bool Mp7JrsCreationType_CreationCoordinates_LocalType::IsValidDate() const
{
	return m_Date_Exist;
}

void Mp7JrsCreationType_CreationCoordinates_LocalType::SetLocation(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType_CreationCoordinates_LocalType::SetLocation().");
	}
	if (m_Location != item || m_Location_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Location);
		m_Location = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Location) m_Location->SetParent(m_myself.getPointer());
		if (m_Location && m_Location->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Location->UseTypeAttribute = true;
		}
		m_Location_Exist = true;
		if(m_Location != Mp7JrsPlacePtr())
		{
			m_Location->SetContentName(XMLString::transcode("Location"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType_CreationCoordinates_LocalType::InvalidateLocation()
{
	m_Location_Exist = false;
}
void Mp7JrsCreationType_CreationCoordinates_LocalType::SetDate(const Mp7JrsTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsCreationType_CreationCoordinates_LocalType::SetDate().");
	}
	if (m_Date != item || m_Date_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Date);
		m_Date = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Date) m_Date->SetParent(m_myself.getPointer());
		if (m_Date && m_Date->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Date->UseTypeAttribute = true;
		}
		m_Date_Exist = true;
		if(m_Date != Mp7JrsTimePtr())
		{
			m_Date->SetContentName(XMLString::transcode("Date"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsCreationType_CreationCoordinates_LocalType::InvalidateDate()
{
	m_Date_Exist = false;
}

Dc1NodeEnum Mp7JrsCreationType_CreationCoordinates_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Location")) == 0)
	{
		// Location is simple element PlaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetLocation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:PlaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsPlacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetLocation(p, client);
					if((p = GetLocation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Date")) == 0)
	{
		// Date is simple element TimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetDate()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetDate(p, client);
					if((p = GetDate()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for CreationType_CreationCoordinates_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "CreationType_CreationCoordinates_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsCreationType_CreationCoordinates_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsCreationType_CreationCoordinates_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsCreationType_CreationCoordinates_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("CreationType_CreationCoordinates_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Location != Mp7JrsPlacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Location->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Location"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Location->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Date != Mp7JrsTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Date->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Date"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Date->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsCreationType_CreationCoordinates_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Location"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Location")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatePlaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetLocation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Date"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Date")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetDate(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsCreationType_CreationCoordinates_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Location")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatePlaceType; // FTT, check this
	}
	this->SetLocation(child);
  }
  if (XMLString::compareString(elementname, X("Date")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTimeType; // FTT, check this
	}
	this->SetDate(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsCreationType_CreationCoordinates_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetLocation() != Dc1NodePtr())
		result.Insert(GetLocation());
	if (GetDate() != Dc1NodePtr())
		result.Insert(GetDate());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsCreationType_CreationCoordinates_LocalType_ExtMethodImpl.h


