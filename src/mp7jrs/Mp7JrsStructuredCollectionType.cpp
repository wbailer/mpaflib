
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsStructuredCollectionType_CollectionType.h"
#include "Mp7JrsStructuredCollectionType_CollectionType0.h"
#include "Mp7JrsStructuredCollectionType_CollectionType1.h"
#include "Mp7JrsStructuredCollectionType_CollectionType2.h"
#include "Mp7JrsStructuredCollectionType_Relationships_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsStructuredCollectionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsStructuredCollectionType_LocalType.h" // Choice collection Collection
#include "Mp7JrsCollectionType.h" // Choice collection element Collection
#include "Mp7JrsReferenceType.h" // Choice collection element CollectionRef
#include "Mp7JrsStructuredCollectionType_LocalType0.h" // Choice collection CollectionModel
#include "Mp7JrsCollectionModelType.h" // Choice collection element CollectionModel
#include "Mp7JrsStructuredCollectionType_LocalType1.h" // Choice collection ClusterModel
#include "Mp7JrsClusterModelType.h" // Choice collection element ClusterModel
#include "Mp7JrsStructuredCollectionType_LocalType2.h" // Choice collection StructuredCollection
#include "Mp7JrsStructuredCollectionType.h" // Choice collection element StructuredCollection
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships

#include <assert.h>
IMp7JrsStructuredCollectionType::IMp7JrsStructuredCollectionType()
{

// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtPropInit.h

}

IMp7JrsStructuredCollectionType::~IMp7JrsStructuredCollectionType()
{
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtPropCleanup.h

}

Mp7JrsStructuredCollectionType::Mp7JrsStructuredCollectionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsStructuredCollectionType::~Mp7JrsStructuredCollectionType()
{
	Cleanup();
}

void Mp7JrsStructuredCollectionType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_StructuredCollectionType_LocalType = Mp7JrsStructuredCollectionType_CollectionPtr(); // Collection
	m_StructuredCollectionType_LocalType0 = Mp7JrsStructuredCollectionType_Collection0Ptr(); // Collection
	m_StructuredCollectionType_LocalType1 = Mp7JrsStructuredCollectionType_Collection1Ptr(); // Collection
	m_StructuredCollectionType_LocalType2 = Mp7JrsStructuredCollectionType_Collection2Ptr(); // Collection
	m_Relationships = Mp7JrsStructuredCollectionType_Relationships_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtMyPropInit.h

}

void Mp7JrsStructuredCollectionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType);
	// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType0);
	// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType1);
	// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType2);
	// Dc1Factory::DeleteObject(m_Relationships);
}

void Mp7JrsStructuredCollectionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use StructuredCollectionTypePtr, since we
	// might need GetBase(), which isn't defined in IStructuredCollectionType
	const Dc1Ptr< Mp7JrsStructuredCollectionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsStructuredCollectionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType);
		this->SetStructuredCollectionType_LocalType(Dc1Factory::CloneObject(tmp->GetStructuredCollectionType_LocalType()));
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType0);
		this->SetStructuredCollectionType_LocalType0(Dc1Factory::CloneObject(tmp->GetStructuredCollectionType_LocalType0()));
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType1);
		this->SetStructuredCollectionType_LocalType1(Dc1Factory::CloneObject(tmp->GetStructuredCollectionType_LocalType1()));
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType2);
		this->SetStructuredCollectionType_LocalType2(Dc1Factory::CloneObject(tmp->GetStructuredCollectionType_LocalType2()));
		// Dc1Factory::DeleteObject(m_Relationships);
		this->SetRelationships(Dc1Factory::CloneObject(tmp->GetRelationships()));
}

Dc1NodePtr Mp7JrsStructuredCollectionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsStructuredCollectionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsStructuredCollectionType::GetBase() const
{
	return m_Base;
}

Mp7JrsStructuredCollectionType_CollectionPtr Mp7JrsStructuredCollectionType::GetStructuredCollectionType_LocalType() const
{
		return m_StructuredCollectionType_LocalType;
}

Mp7JrsStructuredCollectionType_Collection0Ptr Mp7JrsStructuredCollectionType::GetStructuredCollectionType_LocalType0() const
{
		return m_StructuredCollectionType_LocalType0;
}

Mp7JrsStructuredCollectionType_Collection1Ptr Mp7JrsStructuredCollectionType::GetStructuredCollectionType_LocalType1() const
{
		return m_StructuredCollectionType_LocalType1;
}

Mp7JrsStructuredCollectionType_Collection2Ptr Mp7JrsStructuredCollectionType::GetStructuredCollectionType_LocalType2() const
{
		return m_StructuredCollectionType_LocalType2;
}

Mp7JrsStructuredCollectionType_Relationships_CollectionPtr Mp7JrsStructuredCollectionType::GetRelationships() const
{
		return m_Relationships;
}

void Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType(const Mp7JrsStructuredCollectionType_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType().");
	}
	if (m_StructuredCollectionType_LocalType != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType);
		m_StructuredCollectionType_LocalType = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredCollectionType_LocalType) m_StructuredCollectionType_LocalType->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType0(const Mp7JrsStructuredCollectionType_Collection0Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType0().");
	}
	if (m_StructuredCollectionType_LocalType0 != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType0);
		m_StructuredCollectionType_LocalType0 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredCollectionType_LocalType0) m_StructuredCollectionType_LocalType0->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType1(const Mp7JrsStructuredCollectionType_Collection1Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType1().");
	}
	if (m_StructuredCollectionType_LocalType1 != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType1);
		m_StructuredCollectionType_LocalType1 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredCollectionType_LocalType1) m_StructuredCollectionType_LocalType1->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType2(const Mp7JrsStructuredCollectionType_Collection2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetStructuredCollectionType_LocalType2().");
	}
	if (m_StructuredCollectionType_LocalType2 != item)
	{
		// Dc1Factory::DeleteObject(m_StructuredCollectionType_LocalType2);
		m_StructuredCollectionType_LocalType2 = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StructuredCollectionType_LocalType2) m_StructuredCollectionType_LocalType2->SetParent(m_myself.getPointer());
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::SetRelationships(const Mp7JrsStructuredCollectionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetRelationships().");
	}
	if (m_Relationships != item)
	{
		// Dc1Factory::DeleteObject(m_Relationships);
		m_Relationships = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Relationships) m_Relationships->SetParent(m_myself.getPointer());
		if(m_Relationships != Mp7JrsStructuredCollectionType_Relationships_CollectionPtr())
		{
			m_Relationships->SetContentName(XMLString::transcode("Relationships"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsStructuredCollectionType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsStructuredCollectionType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsStructuredCollectionType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsStructuredCollectionType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsStructuredCollectionType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsStructuredCollectionType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsStructuredCollectionType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsStructuredCollectionType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsStructuredCollectionType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsStructuredCollectionType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsStructuredCollectionType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsStructuredCollectionType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsStructuredCollectionType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsStructuredCollectionType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsStructuredCollectionType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsStructuredCollectionType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsStructuredCollectionType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsStructuredCollectionType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsStructuredCollectionType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsStructuredCollectionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Collection")) == 0)
	{
		// Collection is contained in itemtype StructuredCollectionType_LocalType
		// in choice collection StructuredCollectionType_CollectionType

		context->Found = true;
		Mp7JrsStructuredCollectionType_CollectionPtr coll = GetStructuredCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType; // FTT, check this
				SetStructuredCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_LocalPtr item = CreateStructuredCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->SetCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->GetCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CollectionRef")) == 0)
	{
		// CollectionRef is contained in itemtype StructuredCollectionType_LocalType
		// in choice collection StructuredCollectionType_CollectionType

		context->Found = true;
		Mp7JrsStructuredCollectionType_CollectionPtr coll = GetStructuredCollectionType_LocalType();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType; // FTT, check this
				SetStructuredCollectionType_LocalType(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_LocalPtr item = CreateStructuredCollectionType_LocalType; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->SetCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_LocalPtr)coll->elementAt(i))->GetCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CollectionModel")) == 0)
	{
		// CollectionModel is contained in itemtype StructuredCollectionType_LocalType0
		// in choice collection StructuredCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection0Ptr coll = GetStructuredCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType0; // FTT, check this
				SetStructuredCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->GetCollectionModel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local0Ptr item = CreateStructuredCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CollectionModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCollectionModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->SetCollectionModel(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->GetCollectionModel()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CollectionModelRef")) == 0)
	{
		// CollectionModelRef is contained in itemtype StructuredCollectionType_LocalType0
		// in choice collection StructuredCollectionType_CollectionType0

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection0Ptr coll = GetStructuredCollectionType_LocalType0();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType0; // FTT, check this
				SetStructuredCollectionType_LocalType0(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->GetCollectionModelRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local0Ptr item = CreateStructuredCollectionType_LocalType0; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->SetCollectionModelRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local0Ptr)coll->elementAt(i))->GetCollectionModelRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClusterModel")) == 0)
	{
		// ClusterModel is contained in itemtype StructuredCollectionType_LocalType1
		// in choice collection StructuredCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection1Ptr coll = GetStructuredCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType1; // FTT, check this
				SetStructuredCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->GetClusterModel()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local1Ptr item = CreateStructuredCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClusterModelType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClusterModelPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->SetClusterModel(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->GetClusterModel()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClusterModelRef")) == 0)
	{
		// ClusterModelRef is contained in itemtype StructuredCollectionType_LocalType1
		// in choice collection StructuredCollectionType_CollectionType1

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection1Ptr coll = GetStructuredCollectionType_LocalType1();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType1; // FTT, check this
				SetStructuredCollectionType_LocalType1(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->GetClusterModelRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local1Ptr item = CreateStructuredCollectionType_LocalType1; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->SetClusterModelRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local1Ptr)coll->elementAt(i))->GetClusterModelRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredCollection")) == 0)
	{
		// StructuredCollection is contained in itemtype StructuredCollectionType_LocalType2
		// in choice collection StructuredCollectionType_CollectionType2

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection2Ptr coll = GetStructuredCollectionType_LocalType2();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType2; // FTT, check this
				SetStructuredCollectionType_LocalType2(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->GetStructuredCollection()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local2Ptr item = CreateStructuredCollectionType_LocalType2; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsStructuredCollectionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->SetStructuredCollection(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->GetStructuredCollection()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("StructuredCollectionRef")) == 0)
	{
		// StructuredCollectionRef is contained in itemtype StructuredCollectionType_LocalType2
		// in choice collection StructuredCollectionType_CollectionType2

		context->Found = true;
		Mp7JrsStructuredCollectionType_Collection2Ptr coll = GetStructuredCollectionType_LocalType2();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_CollectionType2; // FTT, check this
				SetStructuredCollectionType_LocalType2(coll, client);
			}
			else
			{
				return result; // No collection, silently return emtpy enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}

		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = ((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->GetStructuredCollectionRef()) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				++i;
			}
		}
		if(Dc1XPathParseContext::PostProcessChoiceCollection(context, i, result))
		{	if (i >= 0)
			{
				Mp7JrsStructuredCollectionType_Local2Ptr item = CreateStructuredCollectionType_LocalType2; // FTT, check this
				coll->insertElementAt(item, i, client);
			}
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->SetStructuredCollectionRef(p, client);
					// Maybe some other restrictions are waiting...				
					if((p = ((Mp7JrsStructuredCollectionType_Local2Ptr)coll->elementAt(i))->GetStructuredCollectionRef()) != empty)
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}

		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while (result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;

	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Relationships")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Relationships is item of type GraphType
		// in element collection StructuredCollectionType_Relationships_CollectionType
		
		context->Found = true;
		Mp7JrsStructuredCollectionType_Relationships_CollectionPtr coll = GetRelationships();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateStructuredCollectionType_Relationships_CollectionType; // FTT, check this
				SetRelationships(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for StructuredCollectionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "StructuredCollectionType");
	}
	return result;
}

XMLCh * Mp7JrsStructuredCollectionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsStructuredCollectionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsStructuredCollectionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("StructuredCollectionType"));
	// Element serialization:
	if (m_StructuredCollectionType_LocalType != Mp7JrsStructuredCollectionType_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StructuredCollectionType_LocalType->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_StructuredCollectionType_LocalType0 != Mp7JrsStructuredCollectionType_Collection0Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StructuredCollectionType_LocalType0->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_StructuredCollectionType_LocalType1 != Mp7JrsStructuredCollectionType_Collection1Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StructuredCollectionType_LocalType1->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_StructuredCollectionType_LocalType2 != Mp7JrsStructuredCollectionType_Collection2Ptr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_StructuredCollectionType_LocalType2->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_Relationships != Mp7JrsStructuredCollectionType_Relationships_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Relationships->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsStructuredCollectionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:Collection
			Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:Collection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:CollectionRef
			Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStructuredCollectionType_CollectionPtr tmp = CreateStructuredCollectionType_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStructuredCollectionType_LocalType(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:CollectionModel
			Dc1Util::HasNodeName(parent, X("CollectionModel"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CollectionModel")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:CollectionModelRef
			Dc1Util::HasNodeName(parent, X("CollectionModelRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:CollectionModelRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStructuredCollectionType_Collection0Ptr tmp = CreateStructuredCollectionType_CollectionType0; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStructuredCollectionType_LocalType0(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:ClusterModel
			Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModel")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:ClusterModelRef
			Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:ClusterModelRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStructuredCollectionType_Collection1Ptr tmp = CreateStructuredCollectionType_CollectionType1; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStructuredCollectionType_LocalType1(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialise Choice Collection
		if((parent != NULL) && (
			// urn:mpeg:mpeg7:schema:2004:StructuredCollection
			Dc1Util::HasNodeName(parent, X("StructuredCollection"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StructuredCollection")) == 0)
				||
			// urn:mpeg:mpeg7:schema:2004:StructuredCollectionRef
			Dc1Util::HasNodeName(parent, X("StructuredCollectionRef"), X("urn:mpeg:mpeg7:schema:2004"))
//OLD			(XMLString::compareString(parent->getNodeName(), X("urn:mpeg:mpeg7:schema:2004:StructuredCollectionRef")) == 0)
				))
		{
			// Deserialize factory type
			Mp7JrsStructuredCollectionType_Collection2Ptr tmp = CreateStructuredCollectionType_CollectionType2; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetStructuredCollectionType_LocalType2(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Relationships"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Relationships")) == 0))
		{
			// Deserialize factory type
			Mp7JrsStructuredCollectionType_Relationships_CollectionPtr tmp = CreateStructuredCollectionType_Relationships_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetRelationships(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsStructuredCollectionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("Collection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("Collection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("CollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("CollectionRef")) == 0)
	)
  {
	Mp7JrsStructuredCollectionType_CollectionPtr tmp = CreateStructuredCollectionType_CollectionType; // FTT, check this
	this->SetStructuredCollectionType_LocalType(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("CollectionModel"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("CollectionModel")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("CollectionModelRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("CollectionModelRef")) == 0)
	)
  {
	Mp7JrsStructuredCollectionType_Collection0Ptr tmp = CreateStructuredCollectionType_CollectionType0; // FTT, check this
	this->SetStructuredCollectionType_LocalType0(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModel"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModel")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("ClusterModelRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("ClusterModelRef")) == 0)
	)
  {
	Mp7JrsStructuredCollectionType_Collection1Ptr tmp = CreateStructuredCollectionType_CollectionType1; // FTT, check this
	this->SetStructuredCollectionType_LocalType1(tmp);
	child = tmp;
  }
  // Choice Collection
  if(
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("StructuredCollection"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("StructuredCollection")) == 0)
	||
// TODO better use this	
//  (Dc1Util::HasNodeName(parent, X("StructuredCollectionRef"), X("urn:mpeg:mpeg7:schema:2004")))

// TODO Remove the old way
	(XMLString::compareString(elementname, X("StructuredCollectionRef")) == 0)
	)
  {
	Mp7JrsStructuredCollectionType_Collection2Ptr tmp = CreateStructuredCollectionType_CollectionType2; // FTT, check this
	this->SetStructuredCollectionType_LocalType2(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Relationships")) == 0))
  {
	Mp7JrsStructuredCollectionType_Relationships_CollectionPtr tmp = CreateStructuredCollectionType_Relationships_CollectionType; // FTT, check this
	this->SetRelationships(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsStructuredCollectionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetStructuredCollectionType_LocalType() != Dc1NodePtr())
		result.Insert(GetStructuredCollectionType_LocalType());
	if (GetStructuredCollectionType_LocalType0() != Dc1NodePtr())
		result.Insert(GetStructuredCollectionType_LocalType0());
	if (GetStructuredCollectionType_LocalType1() != Dc1NodePtr())
		result.Insert(GetStructuredCollectionType_LocalType1());
	if (GetStructuredCollectionType_LocalType2() != Dc1NodePtr())
		result.Insert(GetStructuredCollectionType_LocalType2());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsStructuredCollectionType_ExtMethodImpl.h


