
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtImplInclude.h


#include "Mp7JrsRatingType.h"
#include "Mp7JrsMediaReviewType_FreeTextReview_CollectionType.h"
#include "Mp7JrsRelatedMaterialType.h"
#include "Mp7JrsAgentType.h"
#include "Mp7JrsMediaReviewType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FreeTextReview

#include <assert.h>
IMp7JrsMediaReviewType::IMp7JrsMediaReviewType()
{

// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtPropInit.h

}

IMp7JrsMediaReviewType::~IMp7JrsMediaReviewType()
{
// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtPropCleanup.h

}

Mp7JrsMediaReviewType::Mp7JrsMediaReviewType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaReviewType::~Mp7JrsMediaReviewType()
{
	Cleanup();
}

void Mp7JrsMediaReviewType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Rating = Mp7JrsRatingPtr(); // Class
	m_Rating_Exist = false;
	m_FreeTextReview = Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr(); // Collection
	m_ReviewReference = Mp7JrsRelatedMaterialPtr(); // Class
	m_ReviewReference_Exist = false;
	m_Reviewer = Dc1Ptr< Dc1Node >(); // Class
	m_Reviewer_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtMyPropInit.h

}

void Mp7JrsMediaReviewType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Rating);
	// Dc1Factory::DeleteObject(m_FreeTextReview);
	// Dc1Factory::DeleteObject(m_ReviewReference);
	// Dc1Factory::DeleteObject(m_Reviewer);
}

void Mp7JrsMediaReviewType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaReviewTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaReviewType
	const Dc1Ptr< Mp7JrsMediaReviewType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidRating())
	{
		// Dc1Factory::DeleteObject(m_Rating);
		this->SetRating(Dc1Factory::CloneObject(tmp->GetRating()));
	}
	else
	{
		InvalidateRating();
	}
		// Dc1Factory::DeleteObject(m_FreeTextReview);
		this->SetFreeTextReview(Dc1Factory::CloneObject(tmp->GetFreeTextReview()));
	if (tmp->IsValidReviewReference())
	{
		// Dc1Factory::DeleteObject(m_ReviewReference);
		this->SetReviewReference(Dc1Factory::CloneObject(tmp->GetReviewReference()));
	}
	else
	{
		InvalidateReviewReference();
	}
	if (tmp->IsValidReviewer())
	{
		// Dc1Factory::DeleteObject(m_Reviewer);
		this->SetReviewer(Dc1Factory::CloneObject(tmp->GetReviewer()));
	}
	else
	{
		InvalidateReviewer();
	}
}

Dc1NodePtr Mp7JrsMediaReviewType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaReviewType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsRatingPtr Mp7JrsMediaReviewType::GetRating() const
{
		return m_Rating;
}

// Element is optional
bool Mp7JrsMediaReviewType::IsValidRating() const
{
	return m_Rating_Exist;
}

Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr Mp7JrsMediaReviewType::GetFreeTextReview() const
{
		return m_FreeTextReview;
}

Mp7JrsRelatedMaterialPtr Mp7JrsMediaReviewType::GetReviewReference() const
{
		return m_ReviewReference;
}

// Element is optional
bool Mp7JrsMediaReviewType::IsValidReviewReference() const
{
	return m_ReviewReference_Exist;
}

Dc1Ptr< Dc1Node > Mp7JrsMediaReviewType::GetReviewer() const
{
		return m_Reviewer;
}

// Element is optional
bool Mp7JrsMediaReviewType::IsValidReviewer() const
{
	return m_Reviewer_Exist;
}

void Mp7JrsMediaReviewType::SetRating(const Mp7JrsRatingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewType::SetRating().");
	}
	if (m_Rating != item || m_Rating_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Rating);
		m_Rating = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Rating) m_Rating->SetParent(m_myself.getPointer());
		if (m_Rating && m_Rating->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Rating->UseTypeAttribute = true;
		}
		m_Rating_Exist = true;
		if(m_Rating != Mp7JrsRatingPtr())
		{
			m_Rating->SetContentName(XMLString::transcode("Rating"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewType::InvalidateRating()
{
	m_Rating_Exist = false;
}
void Mp7JrsMediaReviewType::SetFreeTextReview(const Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewType::SetFreeTextReview().");
	}
	if (m_FreeTextReview != item)
	{
		// Dc1Factory::DeleteObject(m_FreeTextReview);
		m_FreeTextReview = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FreeTextReview) m_FreeTextReview->SetParent(m_myself.getPointer());
		if(m_FreeTextReview != Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr())
		{
			m_FreeTextReview->SetContentName(XMLString::transcode("FreeTextReview"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewType::SetReviewReference(const Mp7JrsRelatedMaterialPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewType::SetReviewReference().");
	}
	if (m_ReviewReference != item || m_ReviewReference_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ReviewReference);
		m_ReviewReference = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ReviewReference) m_ReviewReference->SetParent(m_myself.getPointer());
		if (m_ReviewReference && m_ReviewReference->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelatedMaterialType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ReviewReference->UseTypeAttribute = true;
		}
		m_ReviewReference_Exist = true;
		if(m_ReviewReference != Mp7JrsRelatedMaterialPtr())
		{
			m_ReviewReference->SetContentName(XMLString::transcode("ReviewReference"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewType::InvalidateReviewReference()
{
	m_ReviewReference_Exist = false;
}
void Mp7JrsMediaReviewType::SetReviewer(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaReviewType::SetReviewer().");
	}
	if (m_Reviewer != item || m_Reviewer_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Reviewer);
		m_Reviewer = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Reviewer) m_Reviewer->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Reviewer) m_Reviewer->UseTypeAttribute = true;
		m_Reviewer_Exist = true;
		if(m_Reviewer != Dc1Ptr< Dc1Node >())
		{
			m_Reviewer->SetContentName(XMLString::transcode("Reviewer"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaReviewType::InvalidateReviewer()
{
	m_Reviewer_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaReviewType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Rating")) == 0)
	{
		// Rating is simple element RatingType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRating()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RatingType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRatingPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRating(p, client);
					if((p = GetRating()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FreeTextReview")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FreeTextReview is item of type TextualType
		// in element collection MediaReviewType_FreeTextReview_CollectionType
		
		context->Found = true;
		Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr coll = GetFreeTextReview();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateMediaReviewType_FreeTextReview_CollectionType; // FTT, check this
				SetFreeTextReview(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ReviewReference")) == 0)
	{
		// ReviewReference is simple element RelatedMaterialType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetReviewReference()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RelatedMaterialType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRelatedMaterialPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetReviewReference(p, client);
					if((p = GetReviewReference()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Reviewer")) == 0)
	{
		// Reviewer is simple abstract element AgentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetReviewer()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsAgentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetReviewer(p, client);
					if((p = GetReviewer()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaReviewType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaReviewType");
	}
	return result;
}

XMLCh * Mp7JrsMediaReviewType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaReviewType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaReviewType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaReviewType"));
	// Element serialization:
	// Class
	
	if (m_Rating != Mp7JrsRatingPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Rating->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Rating"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Rating->Serialize(doc, element, &elem);
	}
	if (m_FreeTextReview != Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FreeTextReview->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_ReviewReference != Mp7JrsRelatedMaterialPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ReviewReference->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ReviewReference"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ReviewReference->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Reviewer != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Reviewer->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Reviewer"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Reviewer->UseTypeAttribute = true;
		}
		m_Reviewer->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaReviewType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Rating"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Rating")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRatingType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRating(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FreeTextReview"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FreeTextReview")) == 0))
		{
			// Deserialize factory type
			Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr tmp = CreateMediaReviewType_FreeTextReview_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFreeTextReview(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ReviewReference"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ReviewReference")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRelatedMaterialType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetReviewReference(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Reviewer"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Reviewer")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAgentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetReviewer(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaReviewType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Rating")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRatingType; // FTT, check this
	}
	this->SetRating(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FreeTextReview")) == 0))
  {
	Mp7JrsMediaReviewType_FreeTextReview_CollectionPtr tmp = CreateMediaReviewType_FreeTextReview_CollectionType; // FTT, check this
	this->SetFreeTextReview(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("ReviewReference")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRelatedMaterialType; // FTT, check this
	}
	this->SetReviewReference(child);
  }
  if (XMLString::compareString(elementname, X("Reviewer")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAgentType; // FTT, check this
	}
	this->SetReviewer(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaReviewType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetRating() != Dc1NodePtr())
		result.Insert(GetRating());
	if (GetFreeTextReview() != Dc1NodePtr())
		result.Insert(GetFreeTextReview());
	if (GetReviewReference() != Dc1NodePtr())
		result.Insert(GetReviewReference());
	if (GetReviewer() != Dc1NodePtr())
		result.Insert(GetReviewer());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaReviewType_ExtMethodImpl.h


