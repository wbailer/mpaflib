
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtImplInclude.h


#include "Mp7JrsCreationToolType.h"
#include "Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType.h"
#include "Mp7JrsRhythmicBaseType.h"
#include "Mp7JrsMeterType.h"
#include "Mp7JrsAudioSegmentType.h"
#include "Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtPropInit.h

}

IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::~IMp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::~Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Instrument = Mp7JrsCreationToolPtr(); // Class
	m_Recurrences = Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr(); // Class with content 
	m_RhythmPattern = Mp7JrsRhythmicBasePtr(); // Class
	m_Meter = Mp7JrsMeterPtr(); // Class
	m_AudioSegment = Mp7JrsAudioSegmentPtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Instrument);
	// Dc1Factory::DeleteObject(m_Recurrences);
	// Dc1Factory::DeleteObject(m_RhythmPattern);
	// Dc1Factory::DeleteObject(m_Meter);
	// Dc1Factory::DeleteObject(m_AudioSegment);
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioRhythmicPatternDS_SinglePattern_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioRhythmicPatternDS_SinglePattern_LocalType
	const Dc1Ptr< Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Instrument);
		this->SetInstrument(Dc1Factory::CloneObject(tmp->GetInstrument()));
		// Dc1Factory::DeleteObject(m_Recurrences);
		this->SetRecurrences(Dc1Factory::CloneObject(tmp->GetRecurrences()));
		// Dc1Factory::DeleteObject(m_RhythmPattern);
		this->SetRhythmPattern(Dc1Factory::CloneObject(tmp->GetRhythmPattern()));
		// Dc1Factory::DeleteObject(m_Meter);
		this->SetMeter(Dc1Factory::CloneObject(tmp->GetMeter()));
		// Dc1Factory::DeleteObject(m_AudioSegment);
		this->SetAudioSegment(Dc1Factory::CloneObject(tmp->GetAudioSegment()));
}

Dc1NodePtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsCreationToolPtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetInstrument() const
{
		return m_Instrument;
}

Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetRecurrences() const
{
		return m_Recurrences;
}

Mp7JrsRhythmicBasePtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetRhythmPattern() const
{
		return m_RhythmPattern;
}

Mp7JrsMeterPtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetMeter() const
{
		return m_Meter;
}

Mp7JrsAudioSegmentPtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetAudioSegment() const
{
		return m_AudioSegment;
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetInstrument(const Mp7JrsCreationToolPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetInstrument().");
	}
	if (m_Instrument != item)
	{
		// Dc1Factory::DeleteObject(m_Instrument);
		m_Instrument = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Instrument) m_Instrument->SetParent(m_myself.getPointer());
		if (m_Instrument && m_Instrument->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Instrument->UseTypeAttribute = true;
		}
		if(m_Instrument != Mp7JrsCreationToolPtr())
		{
			m_Instrument->SetContentName(XMLString::transcode("Instrument"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetRecurrences(const Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetRecurrences().");
	}
	if (m_Recurrences != item)
	{
		// Dc1Factory::DeleteObject(m_Recurrences);
		m_Recurrences = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Recurrences) m_Recurrences->SetParent(m_myself.getPointer());
		if (m_Recurrences && m_Recurrences->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Recurrences->UseTypeAttribute = true;
		}
		if(m_Recurrences != Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr())
		{
			m_Recurrences->SetContentName(XMLString::transcode("Recurrences"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetRhythmPattern(const Mp7JrsRhythmicBasePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetRhythmPattern().");
	}
	if (m_RhythmPattern != item)
	{
		// Dc1Factory::DeleteObject(m_RhythmPattern);
		m_RhythmPattern = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_RhythmPattern) m_RhythmPattern->SetParent(m_myself.getPointer());
		if (m_RhythmPattern && m_RhythmPattern->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RhythmicBaseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_RhythmPattern->UseTypeAttribute = true;
		}
		if(m_RhythmPattern != Mp7JrsRhythmicBasePtr())
		{
			m_RhythmPattern->SetContentName(XMLString::transcode("RhythmPattern"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetMeter(const Mp7JrsMeterPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetMeter().");
	}
	if (m_Meter != item)
	{
		// Dc1Factory::DeleteObject(m_Meter);
		m_Meter = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Meter) m_Meter->SetParent(m_myself.getPointer());
		if (m_Meter && m_Meter->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MeterType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Meter->UseTypeAttribute = true;
		}
		if(m_Meter != Mp7JrsMeterPtr())
		{
			m_Meter->SetContentName(XMLString::transcode("Meter"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetAudioSegment(const Mp7JrsAudioSegmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::SetAudioSegment().");
	}
	if (m_AudioSegment != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegment);
		m_AudioSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegment) m_AudioSegment->SetParent(m_myself.getPointer());
		if (m_AudioSegment && m_AudioSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSegment->UseTypeAttribute = true;
		}
		if(m_AudioSegment != Mp7JrsAudioSegmentPtr())
		{
			m_AudioSegment->SetContentName(XMLString::transcode("AudioSegment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Instrument")) == 0)
	{
		// Instrument is simple element CreationToolType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetInstrument()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:CreationToolType")))) != empty)
			{
				// Is type allowed
				Mp7JrsCreationToolPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetInstrument(p, client);
					if((p = GetInstrument()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Recurrences")) == 0)
	{
		// Recurrences is simple element AudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRecurrences()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRecurrences(p, client);
					if((p = GetRecurrences()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("RhythmPattern")) == 0)
	{
		// RhythmPattern is simple element RhythmicBaseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetRhythmPattern()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:RhythmicBaseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsRhythmicBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetRhythmPattern(p, client);
					if((p = GetRhythmPattern()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Meter")) == 0)
	{
		// Meter is simple element MeterType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetMeter()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MeterType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMeterPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetMeter(p, client);
					if((p = GetMeter()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioSegment")) == 0)
	{
		// AudioSegment is simple element AudioSegmentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioSegment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType")))) != empty)
			{
				// Is type allowed
				Mp7JrsAudioSegmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioSegment(p, client);
					if((p = GetAudioSegment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AudioRhythmicPatternDS_SinglePattern_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AudioRhythmicPatternDS_SinglePattern_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AudioRhythmicPatternDS_SinglePattern_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Instrument != Mp7JrsCreationToolPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Instrument->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Instrument"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Instrument->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Recurrences != Mp7JrsAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Recurrences->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Recurrences"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Recurrences->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_RhythmPattern != Mp7JrsRhythmicBasePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_RhythmPattern->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("RhythmPattern"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_RhythmPattern->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Meter != Mp7JrsMeterPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Meter->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Meter"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Meter->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSegment != Mp7JrsAudioSegmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSegment->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Instrument"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Instrument")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateCreationToolType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetInstrument(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Recurrences"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Recurrences")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRecurrences(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("RhythmPattern"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("RhythmPattern")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateRhythmicBaseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetRhythmPattern(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Meter"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Meter")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMeterType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMeter(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioSegmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Instrument")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateCreationToolType; // FTT, check this
	}
	this->SetInstrument(child);
  }
  if (XMLString::compareString(elementname, X("Recurrences")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioRhythmicPatternDS_SinglePattern_Recurrences_LocalType; // FTT, check this
	}
	this->SetRecurrences(child);
  }
  if (XMLString::compareString(elementname, X("RhythmPattern")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateRhythmicBaseType; // FTT, check this
	}
	this->SetRhythmPattern(child);
  }
  if (XMLString::compareString(elementname, X("Meter")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMeterType; // FTT, check this
	}
	this->SetMeter(child);
  }
  if (XMLString::compareString(elementname, X("AudioSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioSegmentType; // FTT, check this
	}
	this->SetAudioSegment(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInstrument() != Dc1NodePtr())
		result.Insert(GetInstrument());
	if (GetRecurrences() != Dc1NodePtr())
		result.Insert(GetRecurrences());
	if (GetRhythmPattern() != Dc1NodePtr())
		result.Insert(GetRhythmPattern());
	if (GetMeter() != Dc1NodePtr())
		result.Insert(GetMeter());
	if (GetAudioSegment() != Dc1NodePtr())
		result.Insert(GetAudioSegment());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioRhythmicPatternDS_SinglePattern_LocalType_ExtMethodImpl.h


