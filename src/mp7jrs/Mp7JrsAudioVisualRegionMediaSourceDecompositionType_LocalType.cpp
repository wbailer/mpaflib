
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtImplInclude.h


#include "Mp7JrsAudioVisualRegionType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsVideoSegmentType.h"
#include "Mp7JrsMovingRegionType.h"
#include "Mp7JrsStillRegionType.h"
#include "Mp7JrsAudioSegmentType.h"
#include "Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtPropInit.h

}

IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::~IMp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtPropCleanup.h

}

Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::~Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType()
{
	Cleanup();
}

void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr(); // Class
	m_AudioVisualRegionRef = Mp7JrsReferencePtr(); // Class
	m_VideoSegment = Mp7JrsVideoSegmentPtr(); // Class
	m_VideoSegmentRef = Mp7JrsReferencePtr(); // Class
	m_MovingRegion = Mp7JrsMovingRegionPtr(); // Class
	m_MovingRegionRef = Mp7JrsReferencePtr(); // Class
	m_StillRegion = Mp7JrsStillRegionPtr(); // Class
	m_StillRegionRef = Mp7JrsReferencePtr(); // Class
	m_AudioSegment = Mp7JrsAudioSegmentPtr(); // Class
	m_AudioSegmentRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtMyPropInit.h

}

void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	// Dc1Factory::DeleteObject(m_VideoSegment);
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	// Dc1Factory::DeleteObject(m_MovingRegion);
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	// Dc1Factory::DeleteObject(m_StillRegion);
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	// Dc1Factory::DeleteObject(m_AudioSegment);
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
}

void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AudioVisualRegionMediaSourceDecompositionType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IAudioVisualRegionMediaSourceDecompositionType_LocalType
	const Dc1Ptr< Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_AudioVisualRegion);
		this->SetAudioVisualRegion(Dc1Factory::CloneObject(tmp->GetAudioVisualRegion()));
		// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
		this->SetAudioVisualRegionRef(Dc1Factory::CloneObject(tmp->GetAudioVisualRegionRef()));
		// Dc1Factory::DeleteObject(m_VideoSegment);
		this->SetVideoSegment(Dc1Factory::CloneObject(tmp->GetVideoSegment()));
		// Dc1Factory::DeleteObject(m_VideoSegmentRef);
		this->SetVideoSegmentRef(Dc1Factory::CloneObject(tmp->GetVideoSegmentRef()));
		// Dc1Factory::DeleteObject(m_MovingRegion);
		this->SetMovingRegion(Dc1Factory::CloneObject(tmp->GetMovingRegion()));
		// Dc1Factory::DeleteObject(m_MovingRegionRef);
		this->SetMovingRegionRef(Dc1Factory::CloneObject(tmp->GetMovingRegionRef()));
		// Dc1Factory::DeleteObject(m_StillRegion);
		this->SetStillRegion(Dc1Factory::CloneObject(tmp->GetStillRegion()));
		// Dc1Factory::DeleteObject(m_StillRegionRef);
		this->SetStillRegionRef(Dc1Factory::CloneObject(tmp->GetStillRegionRef()));
		// Dc1Factory::DeleteObject(m_AudioSegment);
		this->SetAudioSegment(Dc1Factory::CloneObject(tmp->GetAudioSegment()));
		// Dc1Factory::DeleteObject(m_AudioSegmentRef);
		this->SetAudioSegmentRef(Dc1Factory::CloneObject(tmp->GetAudioSegmentRef()));
}

Dc1NodePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsAudioVisualRegionPtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetAudioVisualRegion() const
{
		return m_AudioVisualRegion;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetAudioVisualRegionRef() const
{
		return m_AudioVisualRegionRef;
}

Mp7JrsVideoSegmentPtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetVideoSegment() const
{
		return m_VideoSegment;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetVideoSegmentRef() const
{
		return m_VideoSegmentRef;
}

Mp7JrsMovingRegionPtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetMovingRegion() const
{
		return m_MovingRegion;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetMovingRegionRef() const
{
		return m_MovingRegionRef;
}

Mp7JrsStillRegionPtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetStillRegion() const
{
		return m_StillRegion;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetStillRegionRef() const
{
		return m_StillRegionRef;
}

Mp7JrsAudioSegmentPtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetAudioSegment() const
{
		return m_AudioSegment;
}

Mp7JrsReferencePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetAudioSegmentRef() const
{
		return m_AudioSegmentRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioVisualRegion(const Mp7JrsAudioVisualRegionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioVisualRegion().");
	}
	if (m_AudioVisualRegion != item)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualRegion);
		m_AudioVisualRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualRegion) m_AudioVisualRegion->SetParent(m_myself.getPointer());
		if (m_AudioVisualRegion && m_AudioVisualRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioVisualRegionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualRegion->UseTypeAttribute = true;
		}
		if(m_AudioVisualRegion != Mp7JrsAudioVisualRegionPtr())
		{
			m_AudioVisualRegion->SetContentName(XMLString::transcode("AudioVisualRegion"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioVisualRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioVisualRegionRef().");
	}
	if (m_AudioVisualRegionRef != item)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
		m_AudioVisualRegionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualRegionRef) m_AudioVisualRegionRef->SetParent(m_myself.getPointer());
		if (m_AudioVisualRegionRef && m_AudioVisualRegionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualRegionRef->UseTypeAttribute = true;
		}
		if(m_AudioVisualRegionRef != Mp7JrsReferencePtr())
		{
			m_AudioVisualRegionRef->SetContentName(XMLString::transcode("AudioVisualRegionRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetVideoSegment(const Mp7JrsVideoSegmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetVideoSegment().");
	}
	if (m_VideoSegment != item)
	{
		// Dc1Factory::DeleteObject(m_VideoSegment);
		m_VideoSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSegment) m_VideoSegment->SetParent(m_myself.getPointer());
		if (m_VideoSegment && m_VideoSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:VideoSegmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSegment->UseTypeAttribute = true;
		}
		if(m_VideoSegment != Mp7JrsVideoSegmentPtr())
		{
			m_VideoSegment->SetContentName(XMLString::transcode("VideoSegment"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetVideoSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetVideoSegmentRef().");
	}
	if (m_VideoSegmentRef != item)
	{
		// Dc1Factory::DeleteObject(m_VideoSegmentRef);
		m_VideoSegmentRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_VideoSegmentRef) m_VideoSegmentRef->SetParent(m_myself.getPointer());
		if (m_VideoSegmentRef && m_VideoSegmentRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_VideoSegmentRef->UseTypeAttribute = true;
		}
		if(m_VideoSegmentRef != Mp7JrsReferencePtr())
		{
			m_VideoSegmentRef->SetContentName(XMLString::transcode("VideoSegmentRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetMovingRegion(const Mp7JrsMovingRegionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetMovingRegion().");
	}
	if (m_MovingRegion != item)
	{
		// Dc1Factory::DeleteObject(m_MovingRegion);
		m_MovingRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MovingRegion) m_MovingRegion->SetParent(m_myself.getPointer());
		if (m_MovingRegion && m_MovingRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MovingRegionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MovingRegion->UseTypeAttribute = true;
		}
		if(m_MovingRegion != Mp7JrsMovingRegionPtr())
		{
			m_MovingRegion->SetContentName(XMLString::transcode("MovingRegion"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetMovingRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetMovingRegionRef().");
	}
	if (m_MovingRegionRef != item)
	{
		// Dc1Factory::DeleteObject(m_MovingRegionRef);
		m_MovingRegionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MovingRegionRef) m_MovingRegionRef->SetParent(m_myself.getPointer());
		if (m_MovingRegionRef && m_MovingRegionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MovingRegionRef->UseTypeAttribute = true;
		}
		if(m_MovingRegionRef != Mp7JrsReferencePtr())
		{
			m_MovingRegionRef->SetContentName(XMLString::transcode("MovingRegionRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetStillRegion(const Mp7JrsStillRegionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetStillRegion().");
	}
	if (m_StillRegion != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegion);
		m_StillRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegion) m_StillRegion->SetParent(m_myself.getPointer());
		if (m_StillRegion && m_StillRegion->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:StillRegionType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegion->UseTypeAttribute = true;
		}
		if(m_StillRegion != Mp7JrsStillRegionPtr())
		{
			m_StillRegion->SetContentName(XMLString::transcode("StillRegion"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetStillRegionRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetStillRegionRef().");
	}
	if (m_StillRegionRef != item)
	{
		// Dc1Factory::DeleteObject(m_StillRegionRef);
		m_StillRegionRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_StillRegionRef) m_StillRegionRef->SetParent(m_myself.getPointer());
		if (m_StillRegionRef && m_StillRegionRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_StillRegionRef->UseTypeAttribute = true;
		}
		if(m_StillRegionRef != Mp7JrsReferencePtr())
		{
			m_StillRegionRef->SetContentName(XMLString::transcode("StillRegionRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioSegment(const Mp7JrsAudioSegmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioSegment().");
	}
	if (m_AudioSegment != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegment);
		m_AudioSegment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegment) m_AudioSegment->SetParent(m_myself.getPointer());
		if (m_AudioSegment && m_AudioSegment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:AudioSegmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSegment->UseTypeAttribute = true;
		}
		if(m_AudioSegment != Mp7JrsAudioSegmentPtr())
		{
			m_AudioSegment->SetContentName(XMLString::transcode("AudioSegment"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegmentRef);
	m_AudioSegmentRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioSegmentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::SetAudioSegmentRef().");
	}
	if (m_AudioSegmentRef != item)
	{
		// Dc1Factory::DeleteObject(m_AudioSegmentRef);
		m_AudioSegmentRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioSegmentRef) m_AudioSegmentRef->SetParent(m_myself.getPointer());
		if (m_AudioSegmentRef && m_AudioSegmentRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioSegmentRef->UseTypeAttribute = true;
		}
		if(m_AudioSegmentRef != Mp7JrsReferencePtr())
		{
			m_AudioSegmentRef->SetContentName(XMLString::transcode("AudioSegmentRef"));
		}
	// Dc1Factory::DeleteObject(m_AudioVisualRegion);
	m_AudioVisualRegion = Mp7JrsAudioVisualRegionPtr();
	// Dc1Factory::DeleteObject(m_AudioVisualRegionRef);
	m_AudioVisualRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_VideoSegment);
	m_VideoSegment = Mp7JrsVideoSegmentPtr();
	// Dc1Factory::DeleteObject(m_VideoSegmentRef);
	m_VideoSegmentRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_MovingRegion);
	m_MovingRegion = Mp7JrsMovingRegionPtr();
	// Dc1Factory::DeleteObject(m_MovingRegionRef);
	m_MovingRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_StillRegion);
	m_StillRegion = Mp7JrsStillRegionPtr();
	// Dc1Factory::DeleteObject(m_StillRegionRef);
	m_StillRegionRef = Mp7JrsReferencePtr();
	// Dc1Factory::DeleteObject(m_AudioSegment);
	m_AudioSegment = Mp7JrsAudioSegmentPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: AudioVisualRegionMediaSourceDecompositionType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_AudioVisualRegion != Mp7JrsAudioVisualRegionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioVisualRegionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualRegionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualRegionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualRegionRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VideoSegment != Mp7JrsVideoSegmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSegment->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_VideoSegmentRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_VideoSegmentRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("VideoSegmentRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_VideoSegmentRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MovingRegion != Mp7JrsMovingRegionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MovingRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MovingRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MovingRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_MovingRegionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MovingRegionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MovingRegionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MovingRegionRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StillRegion != Mp7JrsStillRegionPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegion->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegion"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegion->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_StillRegionRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_StillRegionRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("StillRegionRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_StillRegionRef->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSegment != Mp7JrsAudioSegmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSegment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSegment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSegment->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioSegmentRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioSegmentRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioSegmentRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioSegmentRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioVisualRegionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualRegionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualRegionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateVideoSegmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("VideoSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("VideoSegmentRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetVideoSegmentRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MovingRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MovingRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMovingRegionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMovingRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MovingRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MovingRegionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMovingRegionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegion"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegion")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateStillRegionType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegion(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("StillRegionRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("StillRegionRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetStillRegionRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSegment"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSegment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateAudioSegmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSegment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioSegmentRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioSegmentRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioSegmentRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("AudioVisualRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioVisualRegionType; // FTT, check this
	}
	this->SetAudioVisualRegion(child);
  }
  if (XMLString::compareString(elementname, X("AudioVisualRegionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetAudioVisualRegionRef(child);
  }
  if (XMLString::compareString(elementname, X("VideoSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateVideoSegmentType; // FTT, check this
	}
	this->SetVideoSegment(child);
  }
  if (XMLString::compareString(elementname, X("VideoSegmentRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetVideoSegmentRef(child);
  }
  if (XMLString::compareString(elementname, X("MovingRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMovingRegionType; // FTT, check this
	}
	this->SetMovingRegion(child);
  }
  if (XMLString::compareString(elementname, X("MovingRegionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetMovingRegionRef(child);
  }
  if (XMLString::compareString(elementname, X("StillRegion")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateStillRegionType; // FTT, check this
	}
	this->SetStillRegion(child);
  }
  if (XMLString::compareString(elementname, X("StillRegionRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetStillRegionRef(child);
  }
  if (XMLString::compareString(elementname, X("AudioSegment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateAudioSegmentType; // FTT, check this
	}
	this->SetAudioSegment(child);
  }
  if (XMLString::compareString(elementname, X("AudioSegmentRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetAudioSegmentRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAudioVisualRegion() != Dc1NodePtr())
		result.Insert(GetAudioVisualRegion());
	if (GetAudioVisualRegionRef() != Dc1NodePtr())
		result.Insert(GetAudioVisualRegionRef());
	if (GetVideoSegment() != Dc1NodePtr())
		result.Insert(GetVideoSegment());
	if (GetVideoSegmentRef() != Dc1NodePtr())
		result.Insert(GetVideoSegmentRef());
	if (GetMovingRegion() != Dc1NodePtr())
		result.Insert(GetMovingRegion());
	if (GetMovingRegionRef() != Dc1NodePtr())
		result.Insert(GetMovingRegionRef());
	if (GetStillRegion() != Dc1NodePtr())
		result.Insert(GetStillRegion());
	if (GetStillRegionRef() != Dc1NodePtr())
		result.Insert(GetStillRegionRef());
	if (GetAudioSegment() != Dc1NodePtr())
		result.Insert(GetAudioSegment());
	if (GetAudioSegmentRef() != Dc1NodePtr())
		result.Insert(GetAudioSegmentRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAudioVisualRegionMediaSourceDecompositionType_LocalType_ExtMethodImpl.h


