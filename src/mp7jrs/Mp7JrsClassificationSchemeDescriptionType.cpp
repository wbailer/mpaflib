
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtImplInclude.h


#include "Mp7JrsContentManagementType.h"
#include "Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionType.h"
#include "Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsClassificationSchemeDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsClassificationSchemeType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ClassificationScheme
#include "Mp7JrsClassificationSchemeBaseType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ClassificationSchemeBase

#include <assert.h>
IMp7JrsClassificationSchemeDescriptionType::IMp7JrsClassificationSchemeDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtPropInit.h

}

IMp7JrsClassificationSchemeDescriptionType::~IMp7JrsClassificationSchemeDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtPropCleanup.h

}

Mp7JrsClassificationSchemeDescriptionType::Mp7JrsClassificationSchemeDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsClassificationSchemeDescriptionType::~Mp7JrsClassificationSchemeDescriptionType()
{
	Cleanup();
}

void Mp7JrsClassificationSchemeDescriptionType::Init()
{
	// Init base
	m_Base = CreateContentManagementType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ClassificationScheme = Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr(); // Collection
	m_ClassificationSchemeBase = Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtMyPropInit.h

}

void Mp7JrsClassificationSchemeDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ClassificationScheme);
	// Dc1Factory::DeleteObject(m_ClassificationSchemeBase);
}

void Mp7JrsClassificationSchemeDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ClassificationSchemeDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IClassificationSchemeDescriptionType
	const Dc1Ptr< Mp7JrsClassificationSchemeDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsContentManagementPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsClassificationSchemeDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ClassificationScheme);
		this->SetClassificationScheme(Dc1Factory::CloneObject(tmp->GetClassificationScheme()));
		// Dc1Factory::DeleteObject(m_ClassificationSchemeBase);
		this->SetClassificationSchemeBase(Dc1Factory::CloneObject(tmp->GetClassificationSchemeBase()));
}

Dc1NodePtr Mp7JrsClassificationSchemeDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsClassificationSchemeDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsContentManagementType > Mp7JrsClassificationSchemeDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr Mp7JrsClassificationSchemeDescriptionType::GetClassificationScheme() const
{
		return m_ClassificationScheme;
}

Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr Mp7JrsClassificationSchemeDescriptionType::GetClassificationSchemeBase() const
{
		return m_ClassificationSchemeBase;
}

// implementing setter for choice 
/* element */
void Mp7JrsClassificationSchemeDescriptionType::SetClassificationScheme(const Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeDescriptionType::SetClassificationScheme().");
	}
	if (m_ClassificationScheme != item)
	{
		// Dc1Factory::DeleteObject(m_ClassificationScheme);
		m_ClassificationScheme = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClassificationScheme) m_ClassificationScheme->SetParent(m_myself.getPointer());
		if(m_ClassificationScheme != Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr())
		{
			m_ClassificationScheme->SetContentName(XMLString::transcode("ClassificationScheme"));
		}
	// Dc1Factory::DeleteObject(m_ClassificationSchemeBase);
	m_ClassificationSchemeBase = Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsClassificationSchemeDescriptionType::SetClassificationSchemeBase(const Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeDescriptionType::SetClassificationSchemeBase().");
	}
	if (m_ClassificationSchemeBase != item)
	{
		// Dc1Factory::DeleteObject(m_ClassificationSchemeBase);
		m_ClassificationSchemeBase = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ClassificationSchemeBase) m_ClassificationSchemeBase->SetParent(m_myself.getPointer());
		if(m_ClassificationSchemeBase != Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr())
		{
			m_ClassificationSchemeBase->SetContentName(XMLString::transcode("ClassificationSchemeBase"));
		}
	// Dc1Factory::DeleteObject(m_ClassificationScheme);
	m_ClassificationScheme = Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsClassificationSchemeDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsClassificationSchemeDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsClassificationSchemeDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsClassificationSchemeDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsClassificationSchemeDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsClassificationSchemeDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsClassificationSchemeDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsClassificationSchemeDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsClassificationSchemeDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ClassificationScheme")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ClassificationScheme is item of type ClassificationSchemeType
		// in element collection ClassificationSchemeDescriptionType_ClassificationScheme_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr coll = GetClassificationScheme();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationSchemeDescriptionType_ClassificationScheme_CollectionType; // FTT, check this
				SetClassificationScheme(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ClassificationSchemeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationSchemePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ClassificationSchemeBase")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ClassificationSchemeBase is item of abstract type ClassificationSchemeBaseType
		// in element collection ClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType
		
		context->Found = true;
		Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr coll = GetClassificationSchemeBase();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType; // FTT, check this
				SetClassificationSchemeBase(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsClassificationSchemeBasePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ClassificationSchemeDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ClassificationSchemeDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsClassificationSchemeDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsClassificationSchemeDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsClassificationSchemeDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ClassificationSchemeDescriptionType"));
	// Element serialization:
	if (m_ClassificationScheme != Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClassificationScheme->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_ClassificationSchemeBase != Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ClassificationSchemeBase->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsClassificationSchemeDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsContentManagementType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
	do // Deserialize choice just one time!
	{
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ClassificationScheme"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ClassificationScheme")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr tmp = CreateClassificationSchemeDescriptionType_ClassificationScheme_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClassificationScheme(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ClassificationSchemeBase"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ClassificationSchemeBase")) == 0))
		{
			// Deserialize factory type
			Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr tmp = CreateClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetClassificationSchemeBase(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsClassificationSchemeDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ClassificationScheme")) == 0))
  {
	Mp7JrsClassificationSchemeDescriptionType_ClassificationScheme_CollectionPtr tmp = CreateClassificationSchemeDescriptionType_ClassificationScheme_CollectionType; // FTT, check this
	this->SetClassificationScheme(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ClassificationSchemeBase")) == 0))
  {
	Mp7JrsClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionPtr tmp = CreateClassificationSchemeDescriptionType_ClassificationSchemeBase_CollectionType; // FTT, check this
	this->SetClassificationSchemeBase(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsClassificationSchemeDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetClassificationScheme() != Dc1NodePtr())
		result.Insert(GetClassificationScheme());
	if (GetClassificationSchemeBase() != Dc1NodePtr())
		result.Insert(GetClassificationSchemeBase());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsClassificationSchemeDescriptionType_ExtMethodImpl.h


