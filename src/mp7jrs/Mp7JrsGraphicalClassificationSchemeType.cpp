
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtImplInclude.h


#include "Mp7JrsClassificationSchemeBaseType.h"
#include "Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionType.h"
#include "Mp7JrsClassificationSchemeBaseType_domain_CollectionType.h"
#include "Mp7JrsClassificationSchemeBaseType_Import_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsGraphicalClassificationSchemeType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsReferenceType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Import
#include "Mp7JrsGraphicalTermDefinitionType.h" // Element collection urn:mpeg:mpeg7:schema:2004:GraphicalTerm

#include <assert.h>
IMp7JrsGraphicalClassificationSchemeType::IMp7JrsGraphicalClassificationSchemeType()
{

// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtPropInit.h

}

IMp7JrsGraphicalClassificationSchemeType::~IMp7JrsGraphicalClassificationSchemeType()
{
// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtPropCleanup.h

}

Mp7JrsGraphicalClassificationSchemeType::Mp7JrsGraphicalClassificationSchemeType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsGraphicalClassificationSchemeType::~Mp7JrsGraphicalClassificationSchemeType()
{
	Cleanup();
}

void Mp7JrsGraphicalClassificationSchemeType::Init()
{
	// Init base
	m_Base = CreateClassificationSchemeBaseType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_strict = false; // Value
	m_strict_Default = false; // Default value
	m_strict_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_GraphicalTerm = Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtMyPropInit.h

}

void Mp7JrsGraphicalClassificationSchemeType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_GraphicalTerm);
}

void Mp7JrsGraphicalClassificationSchemeType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use GraphicalClassificationSchemeTypePtr, since we
	// might need GetBase(), which isn't defined in IGraphicalClassificationSchemeType
	const Dc1Ptr< Mp7JrsGraphicalClassificationSchemeType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsClassificationSchemeBasePtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsGraphicalClassificationSchemeType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existstrict())
	{
		this->Setstrict(tmp->Getstrict());
	}
	else
	{
		Invalidatestrict();
	}
	}
		// Dc1Factory::DeleteObject(m_GraphicalTerm);
		this->SetGraphicalTerm(Dc1Factory::CloneObject(tmp->GetGraphicalTerm()));
}

Dc1NodePtr Mp7JrsGraphicalClassificationSchemeType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsGraphicalClassificationSchemeType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsClassificationSchemeBaseType > Mp7JrsGraphicalClassificationSchemeType::GetBase() const
{
	return m_Base;
}

bool Mp7JrsGraphicalClassificationSchemeType::Getstrict() const
{
	if (this->Existstrict()) {
		return m_strict;
	} else {
		return m_strict_Default;
	}
}

bool Mp7JrsGraphicalClassificationSchemeType::Existstrict() const
{
	return m_strict_Exist;
}
void Mp7JrsGraphicalClassificationSchemeType::Setstrict(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::Setstrict().");
	}
	m_strict = item;
	m_strict_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::Invalidatestrict()
{
	m_strict_Exist = false;
}
Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr Mp7JrsGraphicalClassificationSchemeType::GetGraphicalTerm() const
{
		return m_GraphicalTerm;
}

void Mp7JrsGraphicalClassificationSchemeType::SetGraphicalTerm(const Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SetGraphicalTerm().");
	}
	if (m_GraphicalTerm != item)
	{
		// Dc1Factory::DeleteObject(m_GraphicalTerm);
		m_GraphicalTerm = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_GraphicalTerm) m_GraphicalTerm->SetParent(m_myself.getPointer());
		if(m_GraphicalTerm != Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr())
		{
			m_GraphicalTerm->SetContentName(XMLString::transcode("GraphicalTerm"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsGraphicalClassificationSchemeType::Geturi() const
{
	return GetBase()->Geturi();
}

void Mp7JrsGraphicalClassificationSchemeType::Seturi(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::Seturi().");
	}
	GetBase()->Seturi(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr Mp7JrsGraphicalClassificationSchemeType::Getdomain() const
{
	return GetBase()->Getdomain();
}

bool Mp7JrsGraphicalClassificationSchemeType::Existdomain() const
{
	return GetBase()->Existdomain();
}
void Mp7JrsGraphicalClassificationSchemeType::Setdomain(const Mp7JrsClassificationSchemeBaseType_domain_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::Setdomain().");
	}
	GetBase()->Setdomain(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::Invalidatedomain()
{
	GetBase()->Invalidatedomain();
}
Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr Mp7JrsGraphicalClassificationSchemeType::GetImport() const
{
	return GetBase()->GetImport();
}

void Mp7JrsGraphicalClassificationSchemeType::SetImport(const Mp7JrsClassificationSchemeBaseType_Import_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SetImport().");
	}
	GetBase()->SetImport(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsGraphicalClassificationSchemeType::Getid() const
{
	return GetBase()->GetBase()->Getid();
}

bool Mp7JrsGraphicalClassificationSchemeType::Existid() const
{
	return GetBase()->GetBase()->Existid();
}
void Mp7JrsGraphicalClassificationSchemeType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::Setid().");
	}
	GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::Invalidateid()
{
	GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsGraphicalClassificationSchemeType::GettimeBase() const
{
	return GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsGraphicalClassificationSchemeType::ExisttimeBase() const
{
	return GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsGraphicalClassificationSchemeType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SettimeBase().");
	}
	GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::InvalidatetimeBase()
{
	GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsGraphicalClassificationSchemeType::GettimeUnit() const
{
	return GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsGraphicalClassificationSchemeType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsGraphicalClassificationSchemeType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SettimeUnit().");
	}
	GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsGraphicalClassificationSchemeType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsGraphicalClassificationSchemeType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsGraphicalClassificationSchemeType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsGraphicalClassificationSchemeType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsGraphicalClassificationSchemeType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsGraphicalClassificationSchemeType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsGraphicalClassificationSchemeType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsGraphicalClassificationSchemeType::GetHeader() const
{
	return GetBase()->GetBase()->GetHeader();
}

void Mp7JrsGraphicalClassificationSchemeType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsGraphicalClassificationSchemeType::SetHeader().");
	}
	GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsGraphicalClassificationSchemeType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("strict")) == 0)
	{
		// strict is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("GraphicalTerm")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:GraphicalTerm is item of type GraphicalTermDefinitionType
		// in element collection GraphicalClassificationSchemeType_GraphicalTerm_CollectionType
		
		context->Found = true;
		Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr coll = GetGraphicalTerm();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateGraphicalClassificationSchemeType_GraphicalTerm_CollectionType; // FTT, check this
				SetGraphicalTerm(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GraphicalTermDefinitionType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGraphicalTermDefinitionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for GraphicalClassificationSchemeType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "GraphicalClassificationSchemeType");
	}
	return result;
}

XMLCh * Mp7JrsGraphicalClassificationSchemeType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsGraphicalClassificationSchemeType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsGraphicalClassificationSchemeType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("GraphicalClassificationSchemeType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_strict_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_strict);
		element->setAttributeNS(X(""), X("strict"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_GraphicalTerm != Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_GraphicalTerm->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsGraphicalClassificationSchemeType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("strict")))
	{
		// deserialize value type
		this->Setstrict(Dc1Convert::TextToBool(parent->getAttribute(X("strict"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsClassificationSchemeBaseType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("GraphicalTerm"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("GraphicalTerm")) == 0))
		{
			// Deserialize factory type
			Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr tmp = CreateGraphicalClassificationSchemeType_GraphicalTerm_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetGraphicalTerm(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsGraphicalClassificationSchemeType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("GraphicalTerm")) == 0))
  {
	Mp7JrsGraphicalClassificationSchemeType_GraphicalTerm_CollectionPtr tmp = CreateGraphicalClassificationSchemeType_GraphicalTerm_CollectionType; // FTT, check this
	this->SetGraphicalTerm(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsGraphicalClassificationSchemeType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetGraphicalTerm() != Dc1NodePtr())
		result.Insert(GetGraphicalTerm());
	if (GetImport() != Dc1NodePtr())
		result.Insert(GetImport());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsGraphicalClassificationSchemeType_ExtMethodImpl.h


