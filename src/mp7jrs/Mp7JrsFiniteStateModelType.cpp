
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtImplInclude.h


#include "Mp7JrsProbabilityModelType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsFiniteStateModelType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header

#include <assert.h>
IMp7JrsFiniteStateModelType::IMp7JrsFiniteStateModelType()
{

// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtPropInit.h

}

IMp7JrsFiniteStateModelType::~IMp7JrsFiniteStateModelType()
{
// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtPropCleanup.h

}

Mp7JrsFiniteStateModelType::Mp7JrsFiniteStateModelType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFiniteStateModelType::~Mp7JrsFiniteStateModelType()
{
	Cleanup();
}

void Mp7JrsFiniteStateModelType::Init()
{
	// Init base
	m_Base = CreateProbabilityModelType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_numOfStates = 0; // Value



// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtMyPropInit.h

}

void Mp7JrsFiniteStateModelType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrsFiniteStateModelType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FiniteStateModelTypePtr, since we
	// might need GetBase(), which isn't defined in IFiniteStateModelType
	const Dc1Ptr< Mp7JrsFiniteStateModelType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsProbabilityModelPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsFiniteStateModelType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
		this->SetnumOfStates(tmp->GetnumOfStates());
	}
}

Dc1NodePtr Mp7JrsFiniteStateModelType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsFiniteStateModelType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsProbabilityModelType > Mp7JrsFiniteStateModelType::GetBase() const
{
	return m_Base;
}

unsigned Mp7JrsFiniteStateModelType::GetnumOfStates() const
{
	return m_numOfStates;
}

void Mp7JrsFiniteStateModelType::SetnumOfStates(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SetnumOfStates().");
	}
	m_numOfStates = item;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrszeroToOnePtr Mp7JrsFiniteStateModelType::Getconfidence() const
{
	return GetBase()->GetBase()->Getconfidence();
}

bool Mp7JrsFiniteStateModelType::Existconfidence() const
{
	return GetBase()->GetBase()->Existconfidence();
}
void Mp7JrsFiniteStateModelType::Setconfidence(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::Setconfidence().");
	}
	GetBase()->GetBase()->Setconfidence(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::Invalidateconfidence()
{
	GetBase()->GetBase()->Invalidateconfidence();
}
Mp7JrszeroToOnePtr Mp7JrsFiniteStateModelType::Getreliability() const
{
	return GetBase()->GetBase()->Getreliability();
}

bool Mp7JrsFiniteStateModelType::Existreliability() const
{
	return GetBase()->GetBase()->Existreliability();
}
void Mp7JrsFiniteStateModelType::Setreliability(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::Setreliability().");
	}
	GetBase()->GetBase()->Setreliability(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::Invalidatereliability()
{
	GetBase()->GetBase()->Invalidatereliability();
}
XMLCh * Mp7JrsFiniteStateModelType::Getid() const
{
	return GetBase()->GetBase()->GetBase()->Getid();
}

bool Mp7JrsFiniteStateModelType::Existid() const
{
	return GetBase()->GetBase()->GetBase()->Existid();
}
void Mp7JrsFiniteStateModelType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::Setid().");
	}
	GetBase()->GetBase()->GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::Invalidateid()
{
	GetBase()->GetBase()->GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsFiniteStateModelType::GettimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GettimeBase();
}

bool Mp7JrsFiniteStateModelType::ExisttimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeBase();
}
void Mp7JrsFiniteStateModelType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SettimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::InvalidatetimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsFiniteStateModelType::GettimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GettimeUnit();
}

bool Mp7JrsFiniteStateModelType::ExisttimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExisttimeUnit();
}
void Mp7JrsFiniteStateModelType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SettimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::InvalidatetimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsFiniteStateModelType::GetmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeBase();
}

bool Mp7JrsFiniteStateModelType::ExistmediaTimeBase() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeBase();
}
void Mp7JrsFiniteStateModelType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SetmediaTimeBase().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::InvalidatemediaTimeBase()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsFiniteStateModelType::GetmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsFiniteStateModelType::ExistmediaTimeUnit() const
{
	return GetBase()->GetBase()->GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsFiniteStateModelType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SetmediaTimeUnit().");
	}
	GetBase()->GetBase()->GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFiniteStateModelType::InvalidatemediaTimeUnit()
{
	GetBase()->GetBase()->GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsFiniteStateModelType::GetHeader() const
{
	return GetBase()->GetBase()->GetBase()->GetHeader();
}

void Mp7JrsFiniteStateModelType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFiniteStateModelType::SetHeader().");
	}
	GetBase()->GetBase()->GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsFiniteStateModelType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("numOfStates")) == 0)
	{
		// numOfStates is simple attribute nonNegativeInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FiniteStateModelType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FiniteStateModelType");
	}
	return result;
}

XMLCh * Mp7JrsFiniteStateModelType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFiniteStateModelType::Parse(const XMLCh * const txt)
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return false;
}


void Mp7JrsFiniteStateModelType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FiniteStateModelType"));
	// Attribute Serialization:
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_numOfStates);
		element->setAttributeNS(X(""), X("numOfStates"), tmp);
		XMLString::release(&tmp);
	}
	// Element serialization:

}

bool Mp7JrsFiniteStateModelType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("numOfStates")))
	{
		// deserialize value type
		this->SetnumOfStates(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("numOfStates"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsProbabilityModelType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFiniteStateModelType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFiniteStateModelType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFiniteStateModelType_ExtMethodImpl.h


