
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtImplInclude.h


#include "Mp7JrsDType.h"
#include "Mp7JrsUniqueIDType.h"
#include "Mp7JrsTemporalSegmentLocatorType.h"
#include "Mp7JrsMediaTimeType.h"
#include "Mp7JrsTextualSummaryComponentType_FreeText_CollectionType.h"
#include "Mp7JrsTextualSummaryComponentType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FreeText

#include <assert.h>
IMp7JrsTextualSummaryComponentType::IMp7JrsTextualSummaryComponentType()
{

// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtPropInit.h

}

IMp7JrsTextualSummaryComponentType::~IMp7JrsTextualSummaryComponentType()
{
// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtPropCleanup.h

}

Mp7JrsTextualSummaryComponentType::Mp7JrsTextualSummaryComponentType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsTextualSummaryComponentType::~Mp7JrsTextualSummaryComponentType()
{
	Cleanup();
}

void Mp7JrsTextualSummaryComponentType::Init()
{
	// Init base
	m_Base = CreateDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_AudioVisualSourceID = Mp7JrsUniqueIDPtr(); // Class
	m_AudioVisualSourceID_Exist = false;
	m_AudioVisualSourceLocator = Mp7JrsTemporalSegmentLocatorPtr(); // Class
	m_AudioVisualSourceLocator_Exist = false;
	m_ComponentSourceTime = Mp7JrsMediaTimePtr(); // Class
	m_ComponentSourceTime_Exist = false;
	m_FreeText = Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr(); // Collection
	m_SyncTime = Mp7JrsMediaTimePtr(); // Class
	m_SyncTime_Exist = false;


// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtMyPropInit.h

}

void Mp7JrsTextualSummaryComponentType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_AudioVisualSourceID);
	// Dc1Factory::DeleteObject(m_AudioVisualSourceLocator);
	// Dc1Factory::DeleteObject(m_ComponentSourceTime);
	// Dc1Factory::DeleteObject(m_FreeText);
	// Dc1Factory::DeleteObject(m_SyncTime);
}

void Mp7JrsTextualSummaryComponentType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use TextualSummaryComponentTypePtr, since we
	// might need GetBase(), which isn't defined in ITextualSummaryComponentType
	const Dc1Ptr< Mp7JrsTextualSummaryComponentType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsTextualSummaryComponentType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidAudioVisualSourceID())
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSourceID);
		this->SetAudioVisualSourceID(Dc1Factory::CloneObject(tmp->GetAudioVisualSourceID()));
	}
	else
	{
		InvalidateAudioVisualSourceID();
	}
	if (tmp->IsValidAudioVisualSourceLocator())
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSourceLocator);
		this->SetAudioVisualSourceLocator(Dc1Factory::CloneObject(tmp->GetAudioVisualSourceLocator()));
	}
	else
	{
		InvalidateAudioVisualSourceLocator();
	}
	if (tmp->IsValidComponentSourceTime())
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		this->SetComponentSourceTime(Dc1Factory::CloneObject(tmp->GetComponentSourceTime()));
	}
	else
	{
		InvalidateComponentSourceTime();
	}
		// Dc1Factory::DeleteObject(m_FreeText);
		this->SetFreeText(Dc1Factory::CloneObject(tmp->GetFreeText()));
	if (tmp->IsValidSyncTime())
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		this->SetSyncTime(Dc1Factory::CloneObject(tmp->GetSyncTime()));
	}
	else
	{
		InvalidateSyncTime();
	}
}

Dc1NodePtr Mp7JrsTextualSummaryComponentType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsTextualSummaryComponentType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDType > Mp7JrsTextualSummaryComponentType::GetBase() const
{
	return m_Base;
}

Mp7JrsUniqueIDPtr Mp7JrsTextualSummaryComponentType::GetAudioVisualSourceID() const
{
		return m_AudioVisualSourceID;
}

// Element is optional
bool Mp7JrsTextualSummaryComponentType::IsValidAudioVisualSourceID() const
{
	return m_AudioVisualSourceID_Exist;
}

Mp7JrsTemporalSegmentLocatorPtr Mp7JrsTextualSummaryComponentType::GetAudioVisualSourceLocator() const
{
		return m_AudioVisualSourceLocator;
}

// Element is optional
bool Mp7JrsTextualSummaryComponentType::IsValidAudioVisualSourceLocator() const
{
	return m_AudioVisualSourceLocator_Exist;
}

Mp7JrsMediaTimePtr Mp7JrsTextualSummaryComponentType::GetComponentSourceTime() const
{
		return m_ComponentSourceTime;
}

// Element is optional
bool Mp7JrsTextualSummaryComponentType::IsValidComponentSourceTime() const
{
	return m_ComponentSourceTime_Exist;
}

Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr Mp7JrsTextualSummaryComponentType::GetFreeText() const
{
		return m_FreeText;
}

Mp7JrsMediaTimePtr Mp7JrsTextualSummaryComponentType::GetSyncTime() const
{
		return m_SyncTime;
}

// Element is optional
bool Mp7JrsTextualSummaryComponentType::IsValidSyncTime() const
{
	return m_SyncTime_Exist;
}

void Mp7JrsTextualSummaryComponentType::SetAudioVisualSourceID(const Mp7JrsUniqueIDPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualSummaryComponentType::SetAudioVisualSourceID().");
	}
	if (m_AudioVisualSourceID != item || m_AudioVisualSourceID_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSourceID);
		m_AudioVisualSourceID = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualSourceID) m_AudioVisualSourceID->SetParent(m_myself.getPointer());
		if (m_AudioVisualSourceID && m_AudioVisualSourceID->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualSourceID->UseTypeAttribute = true;
		}
		m_AudioVisualSourceID_Exist = true;
		if(m_AudioVisualSourceID != Mp7JrsUniqueIDPtr())
		{
			m_AudioVisualSourceID->SetContentName(XMLString::transcode("AudioVisualSourceID"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualSummaryComponentType::InvalidateAudioVisualSourceID()
{
	m_AudioVisualSourceID_Exist = false;
}
void Mp7JrsTextualSummaryComponentType::SetAudioVisualSourceLocator(const Mp7JrsTemporalSegmentLocatorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualSummaryComponentType::SetAudioVisualSourceLocator().");
	}
	if (m_AudioVisualSourceLocator != item || m_AudioVisualSourceLocator_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioVisualSourceLocator);
		m_AudioVisualSourceLocator = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioVisualSourceLocator) m_AudioVisualSourceLocator->SetParent(m_myself.getPointer());
		if (m_AudioVisualSourceLocator && m_AudioVisualSourceLocator->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioVisualSourceLocator->UseTypeAttribute = true;
		}
		m_AudioVisualSourceLocator_Exist = true;
		if(m_AudioVisualSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
		{
			m_AudioVisualSourceLocator->SetContentName(XMLString::transcode("AudioVisualSourceLocator"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualSummaryComponentType::InvalidateAudioVisualSourceLocator()
{
	m_AudioVisualSourceLocator_Exist = false;
}
void Mp7JrsTextualSummaryComponentType::SetComponentSourceTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualSummaryComponentType::SetComponentSourceTime().");
	}
	if (m_ComponentSourceTime != item || m_ComponentSourceTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ComponentSourceTime);
		m_ComponentSourceTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ComponentSourceTime) m_ComponentSourceTime->SetParent(m_myself.getPointer());
		if (m_ComponentSourceTime && m_ComponentSourceTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ComponentSourceTime->UseTypeAttribute = true;
		}
		m_ComponentSourceTime_Exist = true;
		if(m_ComponentSourceTime != Mp7JrsMediaTimePtr())
		{
			m_ComponentSourceTime->SetContentName(XMLString::transcode("ComponentSourceTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualSummaryComponentType::InvalidateComponentSourceTime()
{
	m_ComponentSourceTime_Exist = false;
}
void Mp7JrsTextualSummaryComponentType::SetFreeText(const Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualSummaryComponentType::SetFreeText().");
	}
	if (m_FreeText != item)
	{
		// Dc1Factory::DeleteObject(m_FreeText);
		m_FreeText = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_FreeText) m_FreeText->SetParent(m_myself.getPointer());
		if(m_FreeText != Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr())
		{
			m_FreeText->SetContentName(XMLString::transcode("FreeText"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualSummaryComponentType::SetSyncTime(const Mp7JrsMediaTimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsTextualSummaryComponentType::SetSyncTime().");
	}
	if (m_SyncTime != item || m_SyncTime_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_SyncTime);
		m_SyncTime = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SyncTime) m_SyncTime->SetParent(m_myself.getPointer());
		if (m_SyncTime && m_SyncTime->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SyncTime->UseTypeAttribute = true;
		}
		m_SyncTime_Exist = true;
		if(m_SyncTime != Mp7JrsMediaTimePtr())
		{
			m_SyncTime->SetContentName(XMLString::transcode("SyncTime"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsTextualSummaryComponentType::InvalidateSyncTime()
{
	m_SyncTime_Exist = false;
}

Dc1NodeEnum Mp7JrsTextualSummaryComponentType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("AudioVisualSourceID")) == 0)
	{
		// AudioVisualSourceID is simple element UniqueIDType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioVisualSourceID()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UniqueIDType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUniqueIDPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioVisualSourceID(p, client);
					if((p = GetAudioVisualSourceID()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioVisualSourceLocator")) == 0)
	{
		// AudioVisualSourceLocator is simple element TemporalSegmentLocatorType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioVisualSourceLocator()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TemporalSegmentLocatorType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTemporalSegmentLocatorPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioVisualSourceLocator(p, client);
					if((p = GetAudioVisualSourceLocator()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ComponentSourceTime")) == 0)
	{
		// ComponentSourceTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetComponentSourceTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetComponentSourceTime(p, client);
					if((p = GetComponentSourceTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("FreeText")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:FreeText is item of type TextualType
		// in element collection TextualSummaryComponentType_FreeText_CollectionType
		
		context->Found = true;
		Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr coll = GetFreeText();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateTextualSummaryComponentType_FreeText_CollectionType; // FTT, check this
				SetFreeText(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SyncTime")) == 0)
	{
		// SyncTime is simple element MediaTimeType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSyncTime()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaTimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSyncTime(p, client);
					if((p = GetSyncTime()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for TextualSummaryComponentType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "TextualSummaryComponentType");
	}
	return result;
}

XMLCh * Mp7JrsTextualSummaryComponentType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsTextualSummaryComponentType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsTextualSummaryComponentType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("TextualSummaryComponentType"));
	// Element serialization:
	// Class
	
	if (m_AudioVisualSourceID != Mp7JrsUniqueIDPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualSourceID->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualSourceID"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualSourceID->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_AudioVisualSourceLocator != Mp7JrsTemporalSegmentLocatorPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioVisualSourceLocator->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioVisualSourceLocator"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioVisualSourceLocator->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ComponentSourceTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ComponentSourceTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ComponentSourceTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ComponentSourceTime->Serialize(doc, element, &elem);
	}
	if (m_FreeText != Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_FreeText->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_SyncTime != Mp7JrsMediaTimePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SyncTime->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SyncTime"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SyncTime->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsTextualSummaryComponentType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualSourceID"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualSourceID")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateUniqueIDType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualSourceID(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioVisualSourceLocator"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioVisualSourceLocator")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTemporalSegmentLocatorType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioVisualSourceLocator(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ComponentSourceTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ComponentSourceTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetComponentSourceTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("FreeText"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("FreeText")) == 0))
		{
			// Deserialize factory type
			Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr tmp = CreateTextualSummaryComponentType_FreeText_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetFreeText(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SyncTime"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SyncTime")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaTimeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSyncTime(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsTextualSummaryComponentType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("AudioVisualSourceID")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateUniqueIDType; // FTT, check this
	}
	this->SetAudioVisualSourceID(child);
  }
  if (XMLString::compareString(elementname, X("AudioVisualSourceLocator")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTemporalSegmentLocatorType; // FTT, check this
	}
	this->SetAudioVisualSourceLocator(child);
  }
  if (XMLString::compareString(elementname, X("ComponentSourceTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetComponentSourceTime(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("FreeText")) == 0))
  {
	Mp7JrsTextualSummaryComponentType_FreeText_CollectionPtr tmp = CreateTextualSummaryComponentType_FreeText_CollectionType; // FTT, check this
	this->SetFreeText(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("SyncTime")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaTimeType; // FTT, check this
	}
	this->SetSyncTime(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsTextualSummaryComponentType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetAudioVisualSourceID() != Dc1NodePtr())
		result.Insert(GetAudioVisualSourceID());
	if (GetAudioVisualSourceLocator() != Dc1NodePtr())
		result.Insert(GetAudioVisualSourceLocator());
	if (GetComponentSourceTime() != Dc1NodePtr())
		result.Insert(GetComponentSourceTime());
	if (GetFreeText() != Dc1NodePtr())
		result.Insert(GetFreeText());
	if (GetSyncTime() != Dc1NodePtr())
		result.Insert(GetSyncTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsTextualSummaryComponentType_ExtMethodImpl.h


