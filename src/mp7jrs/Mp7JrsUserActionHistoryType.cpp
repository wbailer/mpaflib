
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionType.h"
#include "Mp7JrsUserActionHistoryType_UserActionList_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsUserActionHistoryType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsTimeType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ObservationPeriod
#include "Mp7JrsUserActionListType.h" // Element collection urn:mpeg:mpeg7:schema:2004:UserActionList

#include <assert.h>
IMp7JrsUserActionHistoryType::IMp7JrsUserActionHistoryType()
{

// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtPropInit.h

}

IMp7JrsUserActionHistoryType::~IMp7JrsUserActionHistoryType()
{
// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtPropCleanup.h

}

Mp7JrsUserActionHistoryType::Mp7JrsUserActionHistoryType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsUserActionHistoryType::~Mp7JrsUserActionHistoryType()
{
	Cleanup();
}

void Mp7JrsUserActionHistoryType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	

	// Init attributes
	m_protected = Mp7JrsuserChoiceType::UninitializedEnumeration;
	m_protected_Default = Mp7JrsuserChoiceType::_true; // Default enumeration
	m_protected_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_ObservationPeriod = Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr(); // Collection
	m_UserActionList = Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtMyPropInit.h

}

void Mp7JrsUserActionHistoryType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ObservationPeriod);
	// Dc1Factory::DeleteObject(m_UserActionList);
}

void Mp7JrsUserActionHistoryType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use UserActionHistoryTypePtr, since we
	// might need GetBase(), which isn't defined in IUserActionHistoryType
	const Dc1Ptr< Mp7JrsUserActionHistoryType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsUserActionHistoryType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	{
	if (tmp->Existprotected())
	{
		this->Setprotected(tmp->Getprotected());
	}
	else
	{
		Invalidateprotected();
	}
	}
		// Dc1Factory::DeleteObject(m_ObservationPeriod);
		this->SetObservationPeriod(Dc1Factory::CloneObject(tmp->GetObservationPeriod()));
		// Dc1Factory::DeleteObject(m_UserActionList);
		this->SetUserActionList(Dc1Factory::CloneObject(tmp->GetUserActionList()));
}

Dc1NodePtr Mp7JrsUserActionHistoryType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsUserActionHistoryType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsUserActionHistoryType::GetBase() const
{
	return m_Base;
}

Mp7JrsuserChoiceType::Enumeration Mp7JrsUserActionHistoryType::Getprotected() const
{
	if (this->Existprotected()) {
		return m_protected;
	} else {
		return m_protected_Default;
	}
}

bool Mp7JrsUserActionHistoryType::Existprotected() const
{
	return m_protected_Exist;
}
void Mp7JrsUserActionHistoryType::Setprotected(Mp7JrsuserChoiceType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::Setprotected().");
	}
	m_protected = item;
	m_protected_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::Invalidateprotected()
{
	m_protected_Exist = false;
}
Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr Mp7JrsUserActionHistoryType::GetObservationPeriod() const
{
		return m_ObservationPeriod;
}

Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr Mp7JrsUserActionHistoryType::GetUserActionList() const
{
		return m_UserActionList;
}

void Mp7JrsUserActionHistoryType::SetObservationPeriod(const Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SetObservationPeriod().");
	}
	if (m_ObservationPeriod != item)
	{
		// Dc1Factory::DeleteObject(m_ObservationPeriod);
		m_ObservationPeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ObservationPeriod) m_ObservationPeriod->SetParent(m_myself.getPointer());
		if(m_ObservationPeriod != Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr())
		{
			m_ObservationPeriod->SetContentName(XMLString::transcode("ObservationPeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::SetUserActionList(const Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SetUserActionList().");
	}
	if (m_UserActionList != item)
	{
		// Dc1Factory::DeleteObject(m_UserActionList);
		m_UserActionList = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_UserActionList) m_UserActionList->SetParent(m_myself.getPointer());
		if(m_UserActionList != Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr())
		{
			m_UserActionList->SetContentName(XMLString::transcode("UserActionList"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsUserActionHistoryType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsUserActionHistoryType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsUserActionHistoryType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsUserActionHistoryType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsUserActionHistoryType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsUserActionHistoryType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsUserActionHistoryType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsUserActionHistoryType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsUserActionHistoryType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsUserActionHistoryType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsUserActionHistoryType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsUserActionHistoryType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsUserActionHistoryType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsUserActionHistoryType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsUserActionHistoryType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsUserActionHistoryType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsUserActionHistoryType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsUserActionHistoryType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsUserActionHistoryType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsUserActionHistoryType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 6 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("protected")) == 0)
	{
		// protected is simple attribute userChoiceType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsuserChoiceType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ObservationPeriod")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ObservationPeriod is item of type TimeType
		// in element collection UserActionHistoryType_ObservationPeriod_CollectionType
		
		context->Found = true;
		Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr coll = GetObservationPeriod();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserActionHistoryType_ObservationPeriod_CollectionType; // FTT, check this
				SetObservationPeriod(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("UserActionList")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:UserActionList is item of type UserActionListType
		// in element collection UserActionHistoryType_UserActionList_CollectionType
		
		context->Found = true;
		Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr coll = GetUserActionList();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateUserActionHistoryType_UserActionList_CollectionType; // FTT, check this
				SetUserActionList(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:UserActionListType")))) != empty)
			{
				// Is type allowed
				Mp7JrsUserActionListPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for UserActionHistoryType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "UserActionHistoryType");
	}
	return result;
}

XMLCh * Mp7JrsUserActionHistoryType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsUserActionHistoryType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsUserActionHistoryType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("UserActionHistoryType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_protected_Exist)
	{
	// Enumeration
	if(m_protected != Mp7JrsuserChoiceType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsuserChoiceType::ToText(m_protected);
		element->setAttributeNS(X(""), X("protected"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_ObservationPeriod != Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ObservationPeriod->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_UserActionList != Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_UserActionList->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsUserActionHistoryType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("protected")))
	{
		this->Setprotected(Mp7JrsuserChoiceType::Parse(parent->getAttribute(X("protected"))));
		* current = parent;
	}

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ObservationPeriod"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ObservationPeriod")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr tmp = CreateUserActionHistoryType_ObservationPeriod_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetObservationPeriod(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("UserActionList"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("UserActionList")) == 0))
		{
			// Deserialize factory type
			Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr tmp = CreateUserActionHistoryType_UserActionList_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetUserActionList(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsUserActionHistoryType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ObservationPeriod")) == 0))
  {
	Mp7JrsUserActionHistoryType_ObservationPeriod_CollectionPtr tmp = CreateUserActionHistoryType_ObservationPeriod_CollectionType; // FTT, check this
	this->SetObservationPeriod(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("UserActionList")) == 0))
  {
	Mp7JrsUserActionHistoryType_UserActionList_CollectionPtr tmp = CreateUserActionHistoryType_UserActionList_CollectionType; // FTT, check this
	this->SetUserActionList(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsUserActionHistoryType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetObservationPeriod() != Dc1NodePtr())
		result.Insert(GetObservationPeriod());
	if (GetUserActionList() != Dc1NodePtr())
		result.Insert(GetUserActionList());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsUserActionHistoryType_ExtMethodImpl.h


