
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtImplInclude.h


#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsVisualTimeSeriesType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsVisualTimeSeriesType::IMp7JrsVisualTimeSeriesType()
{

// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtPropInit.h

}

IMp7JrsVisualTimeSeriesType::~IMp7JrsVisualTimeSeriesType()
{
// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtPropCleanup.h

}

Mp7JrsVisualTimeSeriesType::Mp7JrsVisualTimeSeriesType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsVisualTimeSeriesType::~Mp7JrsVisualTimeSeriesType()
{
	Cleanup();
}

void Mp7JrsVisualTimeSeriesType::Init()
{

	// Init attributes
	m_offset = Mp7JrsmediaDurationPtr(); // Pattern
	m_offset_Default = CreatemediaDurationType; // Default pattern FTT, check this
	m_offset_Default->Parse(X("PT0S"));
	m_offset_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_TimeIncr = Mp7JrsmediaDurationPtr(); // Pattern


// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtMyPropInit.h

}

void Mp7JrsVisualTimeSeriesType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_offset); // Pattern
	// Dc1Factory::DeleteObject(m_TimeIncr);
}

void Mp7JrsVisualTimeSeriesType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use VisualTimeSeriesTypePtr, since we
	// might need GetBase(), which isn't defined in IVisualTimeSeriesType
	const Dc1Ptr< Mp7JrsVisualTimeSeriesType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_offset); // Pattern
	if (tmp->Existoffset())
	{
		this->Setoffset(Dc1Factory::CloneObject(tmp->Getoffset()));
	}
	else
	{
		Invalidateoffset();
	}
	}
		// Dc1Factory::DeleteObject(m_TimeIncr);
		this->SetTimeIncr(Dc1Factory::CloneObject(tmp->GetTimeIncr()));
}

Dc1NodePtr Mp7JrsVisualTimeSeriesType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsVisualTimeSeriesType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsmediaDurationPtr Mp7JrsVisualTimeSeriesType::Getoffset() const
{
	if (this->Existoffset()) {
		return m_offset;
	} else {
		return m_offset_Default;
	}
}

bool Mp7JrsVisualTimeSeriesType::Existoffset() const
{
	return m_offset_Exist;
}
void Mp7JrsVisualTimeSeriesType::Setoffset(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualTimeSeriesType::Setoffset().");
	}
	m_offset = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_offset) m_offset->SetParent(m_myself.getPointer());
	m_offset_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsVisualTimeSeriesType::Invalidateoffset()
{
	m_offset_Exist = false;
}
Mp7JrsmediaDurationPtr Mp7JrsVisualTimeSeriesType::GetTimeIncr() const
{
		return m_TimeIncr;
}

void Mp7JrsVisualTimeSeriesType::SetTimeIncr(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsVisualTimeSeriesType::SetTimeIncr().");
	}
	if (m_TimeIncr != item)
	{
		// Dc1Factory::DeleteObject(m_TimeIncr);
		m_TimeIncr = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_TimeIncr) m_TimeIncr->SetParent(m_myself.getPointer());
		if (m_TimeIncr && m_TimeIncr->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_TimeIncr->UseTypeAttribute = true;
		}
		if(m_TimeIncr != Mp7JrsmediaDurationPtr())
		{
			m_TimeIncr->SetContentName(XMLString::transcode("TimeIncr"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsVisualTimeSeriesType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  1, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("offset")) == 0)
	{
		// offset is simple attribute mediaDurationType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsmediaDurationPtr");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("TimeIncr")) == 0)
	{
		// TimeIncr is simple element mediaDurationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetTimeIncr()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaDurationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsmediaDurationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetTimeIncr(p, client);
					if((p = GetTimeIncr()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for VisualTimeSeriesType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "VisualTimeSeriesType");
	}
	return result;
}

XMLCh * Mp7JrsVisualTimeSeriesType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsVisualTimeSeriesType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsVisualTimeSeriesType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("VisualTimeSeriesType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_offset_Exist)
	{
	// Pattern
	if (m_offset != Mp7JrsmediaDurationPtr())
	{
		XMLCh * tmp = m_offset->ToText();
		element->setAttributeNS(X(""), X("offset"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if(m_TimeIncr != Mp7JrsmediaDurationPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("TimeIncr");
//		m_TimeIncr->SetContentName(contentname);
		m_TimeIncr->UseTypeAttribute = this->UseTypeAttribute;
		m_TimeIncr->Serialize(doc, element);
	}

}

bool Mp7JrsVisualTimeSeriesType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("offset")))
	{
		Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("offset")));
		this->Setoffset(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("TimeIncr"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("TimeIncr")) == 0))
		{
			Mp7JrsmediaDurationPtr tmp = CreatemediaDurationType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetTimeIncr(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsVisualTimeSeriesType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("TimeIncr")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaDurationType; // FTT, check this
	}
	this->SetTimeIncr(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsVisualTimeSeriesType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetTimeIncr() != Dc1NodePtr())
		result.Insert(GetTimeIncr());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsVisualTimeSeriesType_ExtMethodImpl.h


