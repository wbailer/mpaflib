
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtImplInclude.h


#include "Mp7JrsPlaceType_GeographicPosition_Point_CollectionType.h"
#include "Mp7JrsPlaceType_GeographicPosition_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGeographicPointType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Point

#include <assert.h>
IMp7JrsPlaceType_GeographicPosition_LocalType::IMp7JrsPlaceType_GeographicPosition_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtPropInit.h

}

IMp7JrsPlaceType_GeographicPosition_LocalType::~IMp7JrsPlaceType_GeographicPosition_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtPropCleanup.h

}

Mp7JrsPlaceType_GeographicPosition_LocalType::Mp7JrsPlaceType_GeographicPosition_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPlaceType_GeographicPosition_LocalType::~Mp7JrsPlaceType_GeographicPosition_LocalType()
{
	Cleanup();
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::Init()
{

	// Init attributes
	m_datum = NULL; // String
	m_datum_Exist = false;
	m_accuracy = 0.0f; // Value
	m_accuracy_Exist = false;
	m_type = Mp7JrsPlaceType_GeographicPosition_type_LocalType::UninitializedEnumeration;
	m_type_Default = Mp7JrsPlaceType_GeographicPosition_type_LocalType::point; // Default enumeration
	m_type_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Point = Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtMyPropInit.h

}

void Mp7JrsPlaceType_GeographicPosition_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtMyPropCleanup.h


	XMLString::release(&m_datum); // String
	// Dc1Factory::DeleteObject(m_Point);
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PlaceType_GeographicPosition_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPlaceType_GeographicPosition_LocalType
	const Dc1Ptr< Mp7JrsPlaceType_GeographicPosition_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_datum); // String
	if (tmp->Existdatum())
	{
		this->Setdatum(XMLString::replicate(tmp->Getdatum()));
	}
	else
	{
		Invalidatedatum();
	}
	}
	{
	if (tmp->Existaccuracy())
	{
		this->Setaccuracy(tmp->Getaccuracy());
	}
	else
	{
		Invalidateaccuracy();
	}
	}
	{
	if (tmp->Existtype())
	{
		this->Settype(tmp->Gettype());
	}
	else
	{
		Invalidatetype();
	}
	}
		// Dc1Factory::DeleteObject(m_Point);
		this->SetPoint(Dc1Factory::CloneObject(tmp->GetPoint()));
}

Dc1NodePtr Mp7JrsPlaceType_GeographicPosition_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPlaceType_GeographicPosition_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsPlaceType_GeographicPosition_LocalType::Getdatum() const
{
	return m_datum;
}

bool Mp7JrsPlaceType_GeographicPosition_LocalType::Existdatum() const
{
	return m_datum_Exist;
}
void Mp7JrsPlaceType_GeographicPosition_LocalType::Setdatum(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_GeographicPosition_LocalType::Setdatum().");
	}
	m_datum = item;
	m_datum_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::Invalidatedatum()
{
	m_datum_Exist = false;
}
float Mp7JrsPlaceType_GeographicPosition_LocalType::Getaccuracy() const
{
	return m_accuracy;
}

bool Mp7JrsPlaceType_GeographicPosition_LocalType::Existaccuracy() const
{
	return m_accuracy_Exist;
}
void Mp7JrsPlaceType_GeographicPosition_LocalType::Setaccuracy(float item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_GeographicPosition_LocalType::Setaccuracy().");
	}
	m_accuracy = item;
	m_accuracy_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::Invalidateaccuracy()
{
	m_accuracy_Exist = false;
}
Mp7JrsPlaceType_GeographicPosition_type_LocalType::Enumeration Mp7JrsPlaceType_GeographicPosition_LocalType::Gettype() const
{
	if (this->Existtype()) {
		return m_type;
	} else {
		return m_type_Default;
	}
}

bool Mp7JrsPlaceType_GeographicPosition_LocalType::Existtype() const
{
	return m_type_Exist;
}
void Mp7JrsPlaceType_GeographicPosition_LocalType::Settype(Mp7JrsPlaceType_GeographicPosition_type_LocalType::Enumeration item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_GeographicPosition_LocalType::Settype().");
	}
	m_type = item;
	m_type_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::Invalidatetype()
{
	m_type_Exist = false;
}
Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr Mp7JrsPlaceType_GeographicPosition_LocalType::GetPoint() const
{
		return m_Point;
}

void Mp7JrsPlaceType_GeographicPosition_LocalType::SetPoint(const Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_GeographicPosition_LocalType::SetPoint().");
	}
	if (m_Point != item)
	{
		// Dc1Factory::DeleteObject(m_Point);
		m_Point = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Point) m_Point->SetParent(m_myself.getPointer());
		if(m_Point != Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr())
		{
			m_Point->SetContentName(XMLString::transcode("Point"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsPlaceType_GeographicPosition_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  3, 3 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("datum")) == 0)
	{
		// datum is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("accuracy")) == 0)
	{
		// accuracy is simple attribute float
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "float");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("type")) == 0)
	{
		// type is simple attribute PlaceType_GeographicPosition_type_LocalType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrsPlaceType_GeographicPosition_type_LocalType::Enumeration");
	}
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Point")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Point is item of type GeographicPointType
		// in element collection PlaceType_GeographicPosition_Point_CollectionType
		
		context->Found = true;
		Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr coll = GetPoint();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatePlaceType_GeographicPosition_Point_CollectionType; // FTT, check this
				SetPoint(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:GeographicPointType")))) != empty)
			{
				// Is type allowed
				Mp7JrsGeographicPointPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PlaceType_GeographicPosition_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PlaceType_GeographicPosition_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPlaceType_GeographicPosition_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPlaceType_GeographicPosition_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPlaceType_GeographicPosition_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PlaceType_GeographicPosition_LocalType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_datum_Exist)
	{
	// String
	if(m_datum != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("datum"), m_datum);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_accuracy_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_accuracy);
		element->setAttributeNS(X(""), X("accuracy"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_type_Exist)
	{
	// Enumeration
	if(m_type != Mp7JrsPlaceType_GeographicPosition_type_LocalType::UninitializedEnumeration)
	{
		XMLCh * tmp = Mp7JrsPlaceType_GeographicPosition_type_LocalType::ToText(m_type);
		element->setAttributeNS(X(""), X("type"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_Point != Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Point->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsPlaceType_GeographicPosition_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("datum")))
	{
		// Deserialize string type
		this->Setdatum(Dc1Convert::TextToString(parent->getAttribute(X("datum"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("accuracy")))
	{
		// deserialize value type
		this->Setaccuracy(Dc1Convert::TextToFloat(parent->getAttribute(X("accuracy"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("type")))
	{
		this->Settype(Mp7JrsPlaceType_GeographicPosition_type_LocalType::Parse(parent->getAttribute(X("type"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Point"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Point")) == 0))
		{
			// Deserialize factory type
			Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr tmp = CreatePlaceType_GeographicPosition_Point_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetPoint(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPlaceType_GeographicPosition_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Point")) == 0))
  {
	Mp7JrsPlaceType_GeographicPosition_Point_CollectionPtr tmp = CreatePlaceType_GeographicPosition_Point_CollectionType; // FTT, check this
	this->SetPoint(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPlaceType_GeographicPosition_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetPoint() != Dc1NodePtr())
		result.Insert(GetPoint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPlaceType_GeographicPosition_LocalType_ExtMethodImpl.h


