
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtImplInclude.h


#include "Mp7JrspaddingType.h"
#include "Mp7JrsFilterType.h"
#include "Mp7JrsSignalPlaneSampleType.h"
#include "Mp7JrsFilteringType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsFilteringType::IMp7JrsFilteringType()
{

// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtPropInit.h

}

IMp7JrsFilteringType::~IMp7JrsFilteringType()
{
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtPropCleanup.h

}

Mp7JrsFilteringType::Mp7JrsFilteringType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsFilteringType::~Mp7JrsFilteringType()
{
	Cleanup();
}

void Mp7JrsFilteringType::Init()
{

	// Init attributes
	m_xPad = Mp7JrspaddingPtr(); // Create emtpy class ptr
	m_xPad_Exist = false;
	m_yPad = Mp7JrspaddingPtr(); // Create emtpy class ptr
	m_yPad_Exist = false;
	m_zPad = Mp7JrspaddingPtr(); // Create emtpy class ptr
	m_zPad_Exist = false;
	m_tPad = Mp7JrspaddingPtr(); // Create emtpy class ptr
	m_tPad_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_Filter = Dc1Ptr< Dc1Node >(); // Class
	m_PadSize = Mp7JrsSignalPlaneSamplePtr(); // Class
	m_PadSize_Exist = false;
	m_Shift = Mp7JrsSignalPlaneSamplePtr(); // Class
	m_Shift_Exist = false;
	m_CropStart = Mp7JrsSignalPlaneSamplePtr(); // Class
	m_CropStart_Exist = false;
	m_CropEnd = Mp7JrsSignalPlaneSamplePtr(); // Class
	m_CropEnd_Exist = false;


// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMyPropInit.h

}

void Mp7JrsFilteringType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_xPad);
	// Dc1Factory::DeleteObject(m_yPad);
	// Dc1Factory::DeleteObject(m_zPad);
	// Dc1Factory::DeleteObject(m_tPad);
	// Dc1Factory::DeleteObject(m_Filter);
	// Dc1Factory::DeleteObject(m_PadSize);
	// Dc1Factory::DeleteObject(m_Shift);
	// Dc1Factory::DeleteObject(m_CropStart);
	// Dc1Factory::DeleteObject(m_CropEnd);
}

void Mp7JrsFilteringType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use FilteringTypePtr, since we
	// might need GetBase(), which isn't defined in IFilteringType
	const Dc1Ptr< Mp7JrsFilteringType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	// Dc1Factory::DeleteObject(m_xPad);
	if (tmp->ExistxPad())
	{
		this->SetxPad(Dc1Factory::CloneObject(tmp->GetxPad()));
	}
	else
	{
		InvalidatexPad();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_yPad);
	if (tmp->ExistyPad())
	{
		this->SetyPad(Dc1Factory::CloneObject(tmp->GetyPad()));
	}
	else
	{
		InvalidateyPad();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_zPad);
	if (tmp->ExistzPad())
	{
		this->SetzPad(Dc1Factory::CloneObject(tmp->GetzPad()));
	}
	else
	{
		InvalidatezPad();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_tPad);
	if (tmp->ExisttPad())
	{
		this->SettPad(Dc1Factory::CloneObject(tmp->GettPad()));
	}
	else
	{
		InvalidatetPad();
	}
	}
		// Dc1Factory::DeleteObject(m_Filter);
		this->SetFilter(Dc1Factory::CloneObject(tmp->GetFilter()));
	if (tmp->IsValidPadSize())
	{
		// Dc1Factory::DeleteObject(m_PadSize);
		this->SetPadSize(Dc1Factory::CloneObject(tmp->GetPadSize()));
	}
	else
	{
		InvalidatePadSize();
	}
	if (tmp->IsValidShift())
	{
		// Dc1Factory::DeleteObject(m_Shift);
		this->SetShift(Dc1Factory::CloneObject(tmp->GetShift()));
	}
	else
	{
		InvalidateShift();
	}
	if (tmp->IsValidCropStart())
	{
		// Dc1Factory::DeleteObject(m_CropStart);
		this->SetCropStart(Dc1Factory::CloneObject(tmp->GetCropStart()));
	}
	else
	{
		InvalidateCropStart();
	}
	if (tmp->IsValidCropEnd())
	{
		// Dc1Factory::DeleteObject(m_CropEnd);
		this->SetCropEnd(Dc1Factory::CloneObject(tmp->GetCropEnd()));
	}
	else
	{
		InvalidateCropEnd();
	}
}

Dc1NodePtr Mp7JrsFilteringType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsFilteringType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrspaddingPtr Mp7JrsFilteringType::GetxPad() const
{
	if (this->ExistxPad()) {
		return m_xPad;
	} else {
		return m_xPad_Default;
	}
}

bool Mp7JrsFilteringType::ExistxPad() const
{
	return m_xPad_Exist;
}
void Mp7JrsFilteringType::SetxPad(const Mp7JrspaddingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetxPad().");
	}
	m_xPad = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_xPad) m_xPad->SetParent(m_myself.getPointer());
	m_xPad_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidatexPad()
{
	m_xPad_Exist = false;
}
Mp7JrspaddingPtr Mp7JrsFilteringType::GetyPad() const
{
	if (this->ExistyPad()) {
		return m_yPad;
	} else {
		return m_yPad_Default;
	}
}

bool Mp7JrsFilteringType::ExistyPad() const
{
	return m_yPad_Exist;
}
void Mp7JrsFilteringType::SetyPad(const Mp7JrspaddingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetyPad().");
	}
	m_yPad = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_yPad) m_yPad->SetParent(m_myself.getPointer());
	m_yPad_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidateyPad()
{
	m_yPad_Exist = false;
}
Mp7JrspaddingPtr Mp7JrsFilteringType::GetzPad() const
{
	if (this->ExistzPad()) {
		return m_zPad;
	} else {
		return m_zPad_Default;
	}
}

bool Mp7JrsFilteringType::ExistzPad() const
{
	return m_zPad_Exist;
}
void Mp7JrsFilteringType::SetzPad(const Mp7JrspaddingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetzPad().");
	}
	m_zPad = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_zPad) m_zPad->SetParent(m_myself.getPointer());
	m_zPad_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidatezPad()
{
	m_zPad_Exist = false;
}
Mp7JrspaddingPtr Mp7JrsFilteringType::GettPad() const
{
	if (this->ExisttPad()) {
		return m_tPad;
	} else {
		return m_tPad_Default;
	}
}

bool Mp7JrsFilteringType::ExisttPad() const
{
	return m_tPad_Exist;
}
void Mp7JrsFilteringType::SettPad(const Mp7JrspaddingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SettPad().");
	}
	m_tPad = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_tPad) m_tPad->SetParent(m_myself.getPointer());
	m_tPad_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidatetPad()
{
	m_tPad_Exist = false;
}
Dc1Ptr< Dc1Node > Mp7JrsFilteringType::GetFilter() const
{
		return m_Filter;
}

Mp7JrsSignalPlaneSamplePtr Mp7JrsFilteringType::GetPadSize() const
{
		return m_PadSize;
}

// Element is optional
bool Mp7JrsFilteringType::IsValidPadSize() const
{
	return m_PadSize_Exist;
}

Mp7JrsSignalPlaneSamplePtr Mp7JrsFilteringType::GetShift() const
{
		return m_Shift;
}

// Element is optional
bool Mp7JrsFilteringType::IsValidShift() const
{
	return m_Shift_Exist;
}

Mp7JrsSignalPlaneSamplePtr Mp7JrsFilteringType::GetCropStart() const
{
		return m_CropStart;
}

// Element is optional
bool Mp7JrsFilteringType::IsValidCropStart() const
{
	return m_CropStart_Exist;
}

Mp7JrsSignalPlaneSamplePtr Mp7JrsFilteringType::GetCropEnd() const
{
		return m_CropEnd;
}

// Element is optional
bool Mp7JrsFilteringType::IsValidCropEnd() const
{
	return m_CropEnd_Exist;
}

void Mp7JrsFilteringType::SetFilter(const Dc1Ptr< Dc1Node > & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetFilter().");
	}
	if (m_Filter != item)
	{
		// Dc1Factory::DeleteObject(m_Filter);
		m_Filter = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Filter) m_Filter->SetParent(m_myself.getPointer());
		// Element is abstract, so set UseTypeAttribute to true
		if (m_Filter) m_Filter->UseTypeAttribute = true;
		if(m_Filter != Dc1Ptr< Dc1Node >())
		{
			m_Filter->SetContentName(XMLString::transcode("Filter"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::SetPadSize(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetPadSize().");
	}
	if (m_PadSize != item || m_PadSize_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PadSize);
		m_PadSize = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PadSize) m_PadSize->SetParent(m_myself.getPointer());
		if (m_PadSize && m_PadSize->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PadSize->UseTypeAttribute = true;
		}
		m_PadSize_Exist = true;
		if(m_PadSize != Mp7JrsSignalPlaneSamplePtr())
		{
			m_PadSize->SetContentName(XMLString::transcode("PadSize"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidatePadSize()
{
	m_PadSize_Exist = false;
}
void Mp7JrsFilteringType::SetShift(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetShift().");
	}
	if (m_Shift != item || m_Shift_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Shift);
		m_Shift = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Shift) m_Shift->SetParent(m_myself.getPointer());
		if (m_Shift && m_Shift->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Shift->UseTypeAttribute = true;
		}
		m_Shift_Exist = true;
		if(m_Shift != Mp7JrsSignalPlaneSamplePtr())
		{
			m_Shift->SetContentName(XMLString::transcode("Shift"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidateShift()
{
	m_Shift_Exist = false;
}
void Mp7JrsFilteringType::SetCropStart(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetCropStart().");
	}
	if (m_CropStart != item || m_CropStart_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CropStart);
		m_CropStart = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CropStart) m_CropStart->SetParent(m_myself.getPointer());
		if (m_CropStart && m_CropStart->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CropStart->UseTypeAttribute = true;
		}
		m_CropStart_Exist = true;
		if(m_CropStart != Mp7JrsSignalPlaneSamplePtr())
		{
			m_CropStart->SetContentName(XMLString::transcode("CropStart"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidateCropStart()
{
	m_CropStart_Exist = false;
}
void Mp7JrsFilteringType::SetCropEnd(const Mp7JrsSignalPlaneSamplePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsFilteringType::SetCropEnd().");
	}
	if (m_CropEnd != item || m_CropEnd_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_CropEnd);
		m_CropEnd = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_CropEnd) m_CropEnd->SetParent(m_myself.getPointer());
		if (m_CropEnd && m_CropEnd->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_CropEnd->UseTypeAttribute = true;
		}
		m_CropEnd_Exist = true;
		if(m_CropEnd != Mp7JrsSignalPlaneSamplePtr())
		{
			m_CropEnd->SetContentName(XMLString::transcode("CropEnd"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsFilteringType::InvalidateCropEnd()
{
	m_CropEnd_Exist = false;
}

Dc1NodeEnum Mp7JrsFilteringType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  4, 4 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("xPad")) == 0)
	{
		// xPad is simple attribute paddingType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspaddingPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("yPad")) == 0)
	{
		// yPad is simple attribute paddingType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspaddingPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("zPad")) == 0)
	{
		// zPad is simple attribute paddingType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspaddingPtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("tPad")) == 0)
	{
		// tPad is simple attribute paddingType
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "Mp7JrspaddingPtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Filter")) == 0)
	{
		// Filter is simple abstract element FilterType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFilter()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, 0)) != empty)
			{
				// Is type allowed
				Mp7JrsFilterPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFilter(p, client);
					if((p = GetFilter()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PadSize")) == 0)
	{
		// PadSize is simple element SignalPlaneSampleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPadSize()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSignalPlaneSamplePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPadSize(p, client);
					if((p = GetPadSize()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Shift")) == 0)
	{
		// Shift is simple element SignalPlaneSampleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetShift()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSignalPlaneSamplePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetShift(p, client);
					if((p = GetShift()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CropStart")) == 0)
	{
		// CropStart is simple element SignalPlaneSampleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCropStart()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSignalPlaneSamplePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCropStart(p, client);
					if((p = GetCropStart()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("CropEnd")) == 0)
	{
		// CropEnd is simple element SignalPlaneSampleType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCropEnd()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SignalPlaneSampleType")))) != empty)
			{
				// Is type allowed
				Mp7JrsSignalPlaneSamplePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCropEnd(p, client);
					if((p = GetCropEnd()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for FilteringType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "FilteringType");
	}
	return result;
}

XMLCh * Mp7JrsFilteringType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsFilteringType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsFilteringType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("FilteringType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_xPad_Exist)
	{
	// Class
	if (m_xPad != Mp7JrspaddingPtr())
	{
		XMLCh * tmp = m_xPad->ToText();
		element->setAttributeNS(X(""), X("xPad"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_yPad_Exist)
	{
	// Class
	if (m_yPad != Mp7JrspaddingPtr())
	{
		XMLCh * tmp = m_yPad->ToText();
		element->setAttributeNS(X(""), X("yPad"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_zPad_Exist)
	{
	// Class
	if (m_zPad != Mp7JrspaddingPtr())
	{
		XMLCh * tmp = m_zPad->ToText();
		element->setAttributeNS(X(""), X("zPad"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_tPad_Exist)
	{
	// Class
	if (m_tPad != Mp7JrspaddingPtr())
	{
		XMLCh * tmp = m_tPad->ToText();
		element->setAttributeNS(X(""), X("tPad"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	// Class
	
	if (m_Filter != Dc1Ptr< Dc1Node >())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Filter->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Filter"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
			// Abstract type, so use xsitype
			m_Filter->UseTypeAttribute = true;
		}
		m_Filter->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_PadSize != Mp7JrsSignalPlaneSamplePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PadSize->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PadSize"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PadSize->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Shift != Mp7JrsSignalPlaneSamplePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Shift->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Shift"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Shift->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CropStart != Mp7JrsSignalPlaneSamplePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CropStart->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CropStart"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CropStart->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_CropEnd != Mp7JrsSignalPlaneSamplePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_CropEnd->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("CropEnd"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_CropEnd->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsFilteringType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("xPad")))
	{
		// Deserialize class type
		Mp7JrspaddingPtr tmp = CreatepaddingType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("xPad")));
		this->SetxPad(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("yPad")))
	{
		// Deserialize class type
		Mp7JrspaddingPtr tmp = CreatepaddingType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("yPad")));
		this->SetyPad(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("zPad")))
	{
		// Deserialize class type
		Mp7JrspaddingPtr tmp = CreatepaddingType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("zPad")));
		this->SetzPad(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("tPad")))
	{
		// Deserialize class type
		Mp7JrspaddingPtr tmp = CreatepaddingType; // FTT, check this
		tmp->Parse(parent->getAttribute(X("tPad")));
		this->SettPad(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Filter"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Filter")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateFilterType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFilter(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PadSize"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PadSize")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSignalPlaneSampleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPadSize(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Shift"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Shift")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSignalPlaneSampleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetShift(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CropStart"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CropStart")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSignalPlaneSampleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCropStart(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("CropEnd"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("CropEnd")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSignalPlaneSampleType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetCropEnd(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsFilteringType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Filter")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateFilterType; // FTT, check this
	}
	this->SetFilter(child);
  }
  if (XMLString::compareString(elementname, X("PadSize")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSignalPlaneSampleType; // FTT, check this
	}
	this->SetPadSize(child);
  }
  if (XMLString::compareString(elementname, X("Shift")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSignalPlaneSampleType; // FTT, check this
	}
	this->SetShift(child);
  }
  if (XMLString::compareString(elementname, X("CropStart")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSignalPlaneSampleType; // FTT, check this
	}
	this->SetCropStart(child);
  }
  if (XMLString::compareString(elementname, X("CropEnd")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSignalPlaneSampleType; // FTT, check this
	}
	this->SetCropEnd(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsFilteringType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFilter() != Dc1NodePtr())
		result.Insert(GetFilter());
	if (GetPadSize() != Dc1NodePtr())
		result.Insert(GetPadSize());
	if (GetShift() != Dc1NodePtr())
		result.Insert(GetShift());
	if (GetCropStart() != Dc1NodePtr())
		result.Insert(GetCropStart());
	if (GetCropEnd() != Dc1NodePtr())
		result.Insert(GetCropEnd());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsFilteringType_ExtMethodImpl.h


