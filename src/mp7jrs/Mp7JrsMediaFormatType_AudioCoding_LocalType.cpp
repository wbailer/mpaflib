
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtImplInclude.h


#include "Mp7JrsControlledTermUseType.h"
#include "Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalType.h"
#include "Mp7JrsMediaFormatType_AudioCoding_Sample_LocalType.h"
#include "Mp7JrsMediaFormatType_AudioCoding_Emphasis_LocalType2.h"
#include "Mp7JrsMediaFormatType_AudioCoding_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsMediaFormatType_AudioCoding_LocalType::IMp7JrsMediaFormatType_AudioCoding_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtPropInit.h

}

IMp7JrsMediaFormatType_AudioCoding_LocalType::~IMp7JrsMediaFormatType_AudioCoding_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtPropCleanup.h

}

Mp7JrsMediaFormatType_AudioCoding_LocalType::Mp7JrsMediaFormatType_AudioCoding_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMediaFormatType_AudioCoding_LocalType::~Mp7JrsMediaFormatType_AudioCoding_LocalType()
{
	Cleanup();
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Format = Mp7JrsControlledTermUsePtr(); // Class
	m_Format_Exist = false;
	m_AudioChannels = Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr(); // Class with content 
	m_AudioChannels_Exist = false;
	m_Sample = Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr(); // Class
	m_Sample_Exist = false;
	m_Emphasis = Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr(); // Class
	m_Emphasis_Exist = false;
	m_Presentation = Mp7JrsControlledTermUsePtr(); // Class
	m_Presentation_Exist = false;


// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtMyPropInit.h

}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Format);
	// Dc1Factory::DeleteObject(m_AudioChannels);
	// Dc1Factory::DeleteObject(m_Sample);
	// Dc1Factory::DeleteObject(m_Emphasis);
	// Dc1Factory::DeleteObject(m_Presentation);
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MediaFormatType_AudioCoding_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMediaFormatType_AudioCoding_LocalType
	const Dc1Ptr< Mp7JrsMediaFormatType_AudioCoding_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidFormat())
	{
		// Dc1Factory::DeleteObject(m_Format);
		this->SetFormat(Dc1Factory::CloneObject(tmp->GetFormat()));
	}
	else
	{
		InvalidateFormat();
	}
	if (tmp->IsValidAudioChannels())
	{
		// Dc1Factory::DeleteObject(m_AudioChannels);
		this->SetAudioChannels(Dc1Factory::CloneObject(tmp->GetAudioChannels()));
	}
	else
	{
		InvalidateAudioChannels();
	}
	if (tmp->IsValidSample())
	{
		// Dc1Factory::DeleteObject(m_Sample);
		this->SetSample(Dc1Factory::CloneObject(tmp->GetSample()));
	}
	else
	{
		InvalidateSample();
	}
	if (tmp->IsValidEmphasis())
	{
		// Dc1Factory::DeleteObject(m_Emphasis);
		this->SetEmphasis(Dc1Factory::CloneObject(tmp->GetEmphasis()));
	}
	else
	{
		InvalidateEmphasis();
	}
	if (tmp->IsValidPresentation())
	{
		// Dc1Factory::DeleteObject(m_Presentation);
		this->SetPresentation(Dc1Factory::CloneObject(tmp->GetPresentation()));
	}
	else
	{
		InvalidatePresentation();
	}
}

Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetFormat() const
{
		return m_Format;
}

// Element is optional
bool Mp7JrsMediaFormatType_AudioCoding_LocalType::IsValidFormat() const
{
	return m_Format_Exist;
}

Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetAudioChannels() const
{
		return m_AudioChannels;
}

// Element is optional
bool Mp7JrsMediaFormatType_AudioCoding_LocalType::IsValidAudioChannels() const
{
	return m_AudioChannels_Exist;
}

Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetSample() const
{
		return m_Sample;
}

// Element is optional
bool Mp7JrsMediaFormatType_AudioCoding_LocalType::IsValidSample() const
{
	return m_Sample_Exist;
}

Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetEmphasis() const
{
		return m_Emphasis;
}

// Element is optional
bool Mp7JrsMediaFormatType_AudioCoding_LocalType::IsValidEmphasis() const
{
	return m_Emphasis_Exist;
}

Mp7JrsControlledTermUsePtr Mp7JrsMediaFormatType_AudioCoding_LocalType::GetPresentation() const
{
		return m_Presentation;
}

// Element is optional
bool Mp7JrsMediaFormatType_AudioCoding_LocalType::IsValidPresentation() const
{
	return m_Presentation_Exist;
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::SetFormat(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_LocalType::SetFormat().");
	}
	if (m_Format != item || m_Format_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Format);
		m_Format = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Format) m_Format->SetParent(m_myself.getPointer());
		if (m_Format && m_Format->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Format->UseTypeAttribute = true;
		}
		m_Format_Exist = true;
		if(m_Format != Mp7JrsControlledTermUsePtr())
		{
			m_Format->SetContentName(XMLString::transcode("Format"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::InvalidateFormat()
{
	m_Format_Exist = false;
}
void Mp7JrsMediaFormatType_AudioCoding_LocalType::SetAudioChannels(const Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_LocalType::SetAudioChannels().");
	}
	if (m_AudioChannels != item || m_AudioChannels_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_AudioChannels);
		m_AudioChannels = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_AudioChannels) m_AudioChannels->SetParent(m_myself.getPointer());
		if (m_AudioChannels && m_AudioChannels->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_AudioChannels_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_AudioChannels->UseTypeAttribute = true;
		}
		m_AudioChannels_Exist = true;
		if(m_AudioChannels != Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr())
		{
			m_AudioChannels->SetContentName(XMLString::transcode("AudioChannels"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::InvalidateAudioChannels()
{
	m_AudioChannels_Exist = false;
}
void Mp7JrsMediaFormatType_AudioCoding_LocalType::SetSample(const Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_LocalType::SetSample().");
	}
	if (m_Sample != item || m_Sample_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Sample);
		m_Sample = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Sample) m_Sample->SetParent(m_myself.getPointer());
		if (m_Sample && m_Sample->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Sample_LocalType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Sample->UseTypeAttribute = true;
		}
		m_Sample_Exist = true;
		if(m_Sample != Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr())
		{
			m_Sample->SetContentName(XMLString::transcode("Sample"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::InvalidateSample()
{
	m_Sample_Exist = false;
}
void Mp7JrsMediaFormatType_AudioCoding_LocalType::SetEmphasis(const Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_LocalType::SetEmphasis().");
	}
	if (m_Emphasis != item || m_Emphasis_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Emphasis);
		m_Emphasis = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Emphasis) m_Emphasis->SetParent(m_myself.getPointer());
		if (m_Emphasis && m_Emphasis->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType2"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Emphasis->UseTypeAttribute = true;
		}
		m_Emphasis_Exist = true;
		if(m_Emphasis != Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr())
		{
			m_Emphasis->SetContentName(XMLString::transcode("Emphasis"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::InvalidateEmphasis()
{
	m_Emphasis_Exist = false;
}
void Mp7JrsMediaFormatType_AudioCoding_LocalType::SetPresentation(const Mp7JrsControlledTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMediaFormatType_AudioCoding_LocalType::SetPresentation().");
	}
	if (m_Presentation != item || m_Presentation_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Presentation);
		m_Presentation = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Presentation) m_Presentation->SetParent(m_myself.getPointer());
		if (m_Presentation && m_Presentation->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Presentation->UseTypeAttribute = true;
		}
		m_Presentation_Exist = true;
		if(m_Presentation != Mp7JrsControlledTermUsePtr())
		{
			m_Presentation->SetContentName(XMLString::transcode("Presentation"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMediaFormatType_AudioCoding_LocalType::InvalidatePresentation()
{
	m_Presentation_Exist = false;
}

Dc1NodeEnum Mp7JrsMediaFormatType_AudioCoding_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Format")) == 0)
	{
		// Format is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetFormat()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetFormat(p, client);
					if((p = GetFormat()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("AudioChannels")) == 0)
	{
		// AudioChannels is simple element MediaFormatType_AudioCoding_AudioChannels_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetAudioChannels()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_AudioChannels_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetAudioChannels(p, client);
					if((p = GetAudioChannels()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Sample")) == 0)
	{
		// Sample is simple element MediaFormatType_AudioCoding_Sample_LocalType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSample()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Sample_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSample(p, client);
					if((p = GetSample()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Emphasis")) == 0)
	{
		// Emphasis is simple element MediaFormatType_AudioCoding_Emphasis_LocalType2
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetEmphasis()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaFormatType_AudioCoding_Emphasis_LocalType2")))) != empty)
			{
				// Is type allowed
				Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetEmphasis(p, client);
					if((p = GetEmphasis()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Presentation")) == 0)
	{
		// Presentation is simple element ControlledTermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPresentation()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ControlledTermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsControlledTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPresentation(p, client);
					if((p = GetPresentation()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for MediaFormatType_AudioCoding_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "MediaFormatType_AudioCoding_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsMediaFormatType_AudioCoding_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMediaFormatType_AudioCoding_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMediaFormatType_AudioCoding_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MediaFormatType_AudioCoding_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Format != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Format->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Format"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Format->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_AudioChannels != Mp7JrsMediaFormatType_AudioCoding_AudioChannels_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_AudioChannels->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("AudioChannels"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_AudioChannels->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Sample != Mp7JrsMediaFormatType_AudioCoding_Sample_LocalPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Sample->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Sample"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Sample->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Emphasis != Mp7JrsMediaFormatType_AudioCoding_Emphasis_Local2Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Emphasis->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Emphasis"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Emphasis->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_Presentation != Mp7JrsControlledTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Presentation->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Presentation"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Presentation->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsMediaFormatType_AudioCoding_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Format"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Format")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetFormat(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("AudioChannels"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("AudioChannels")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_AudioCoding_AudioChannels_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetAudioChannels(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Sample"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Sample")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_AudioCoding_Sample_LocalType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSample(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Emphasis"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Emphasis")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaFormatType_AudioCoding_Emphasis_LocalType2; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetEmphasis(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Presentation"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Presentation")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateControlledTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPresentation(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMediaFormatType_AudioCoding_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Format")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetFormat(child);
  }
  if (XMLString::compareString(elementname, X("AudioChannels")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_AudioCoding_AudioChannels_LocalType; // FTT, check this
	}
	this->SetAudioChannels(child);
  }
  if (XMLString::compareString(elementname, X("Sample")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_AudioCoding_Sample_LocalType; // FTT, check this
	}
	this->SetSample(child);
  }
  if (XMLString::compareString(elementname, X("Emphasis")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaFormatType_AudioCoding_Emphasis_LocalType2; // FTT, check this
	}
	this->SetEmphasis(child);
  }
  if (XMLString::compareString(elementname, X("Presentation")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateControlledTermUseType; // FTT, check this
	}
	this->SetPresentation(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMediaFormatType_AudioCoding_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetFormat() != Dc1NodePtr())
		result.Insert(GetFormat());
	if (GetAudioChannels() != Dc1NodePtr())
		result.Insert(GetAudioChannels());
	if (GetSample() != Dc1NodePtr())
		result.Insert(GetSample());
	if (GetEmphasis() != Dc1NodePtr())
		result.Insert(GetEmphasis());
	if (GetPresentation() != Dc1NodePtr())
		result.Insert(GetPresentation());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMediaFormatType_AudioCoding_LocalType_ExtMethodImpl.h


