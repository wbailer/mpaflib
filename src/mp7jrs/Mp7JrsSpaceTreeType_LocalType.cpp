
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtImplInclude.h


#include "Mp7JrsSpaceTreeType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsSpaceTreeType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSpaceTreeType_LocalType::IMp7JrsSpaceTreeType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtPropInit.h

}

IMp7JrsSpaceTreeType_LocalType::~IMp7JrsSpaceTreeType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtPropCleanup.h

}

Mp7JrsSpaceTreeType_LocalType::Mp7JrsSpaceTreeType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSpaceTreeType_LocalType::~Mp7JrsSpaceTreeType_LocalType()
{
	Cleanup();
}

void Mp7JrsSpaceTreeType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_Child = Mp7JrsSpaceTreePtr(); // Class
	m_ChildRef = Mp7JrsReferencePtr(); // Class


// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSpaceTreeType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Child);
	// Dc1Factory::DeleteObject(m_ChildRef);
}

void Mp7JrsSpaceTreeType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SpaceTreeType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISpaceTreeType_LocalType
	const Dc1Ptr< Mp7JrsSpaceTreeType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_Child);
		this->SetChild(Dc1Factory::CloneObject(tmp->GetChild()));
		// Dc1Factory::DeleteObject(m_ChildRef);
		this->SetChildRef(Dc1Factory::CloneObject(tmp->GetChildRef()));
}

Dc1NodePtr Mp7JrsSpaceTreeType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSpaceTreeType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsSpaceTreePtr Mp7JrsSpaceTreeType_LocalType::GetChild() const
{
		return m_Child;
}

Mp7JrsReferencePtr Mp7JrsSpaceTreeType_LocalType::GetChildRef() const
{
		return m_ChildRef;
}

// implementing setter for choice 
/* element */
void Mp7JrsSpaceTreeType_LocalType::SetChild(const Mp7JrsSpaceTreePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceTreeType_LocalType::SetChild().");
	}
	if (m_Child != item)
	{
		// Dc1Factory::DeleteObject(m_Child);
		m_Child = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Child) m_Child->SetParent(m_myself.getPointer());
		if (m_Child && m_Child->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:SpaceTreeType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Child->UseTypeAttribute = true;
		}
		if(m_Child != Mp7JrsSpaceTreePtr())
		{
			m_Child->SetContentName(XMLString::transcode("Child"));
		}
	// Dc1Factory::DeleteObject(m_ChildRef);
	m_ChildRef = Mp7JrsReferencePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSpaceTreeType_LocalType::SetChildRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSpaceTreeType_LocalType::SetChildRef().");
	}
	if (m_ChildRef != item)
	{
		// Dc1Factory::DeleteObject(m_ChildRef);
		m_ChildRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ChildRef) m_ChildRef->SetParent(m_myself.getPointer());
		if (m_ChildRef && m_ChildRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ChildRef->UseTypeAttribute = true;
		}
		if(m_ChildRef != Mp7JrsReferencePtr())
		{
			m_ChildRef->SetContentName(XMLString::transcode("ChildRef"));
		}
	// Dc1Factory::DeleteObject(m_Child);
	m_Child = Mp7JrsSpaceTreePtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SpaceTreeType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSpaceTreeType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSpaceTreeType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSpaceTreeType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSpaceTreeType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	// Class
	
	if (m_Child != Mp7JrsSpaceTreePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Child->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Child"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Child->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ChildRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ChildRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ChildRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ChildRef->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsSpaceTreeType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Child"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Child")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateSpaceTreeType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetChild(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ChildRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ChildRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetChildRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSpaceTreeType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Child")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateSpaceTreeType; // FTT, check this
	}
	this->SetChild(child);
  }
  if (XMLString::compareString(elementname, X("ChildRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetChildRef(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsSpaceTreeType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetChild() != Dc1NodePtr())
		result.Insert(GetChild());
	if (GetChildRef() != Dc1NodePtr())
		result.Insert(GetChildRef());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSpaceTreeType_LocalType_ExtMethodImpl.h


