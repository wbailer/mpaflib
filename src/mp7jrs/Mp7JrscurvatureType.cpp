
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtImplInclude.h


#include "Mp7JrscurvatureType_CollectionType.h"
#include "Mp7JrscurvatureType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrscurvatureType::IMp7JrscurvatureType()
{

// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtPropInit.h

}

IMp7JrscurvatureType::~IMp7JrscurvatureType()
{
// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtPropCleanup.h

}

Mp7JrscurvatureType::Mp7JrscurvatureType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrscurvatureType::~Mp7JrscurvatureType()
{
	Cleanup();
}

void Mp7JrscurvatureType::Init()
{
	// Init base
	m_Base = CreatecurvatureType_CollectionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	




// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtMyPropInit.h

}

void Mp7JrscurvatureType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
}

void Mp7JrscurvatureType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use curvatureTypePtr, since we
	// might need GetBase(), which isn't defined in IcurvatureType
	const Dc1Ptr< Mp7JrscurvatureType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrscurvatureType_CollectionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrscurvatureType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
}

Dc1NodePtr Mp7JrscurvatureType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrscurvatureType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrscurvatureType_CollectionType > Mp7JrscurvatureType::GetBase() const
{
	return m_Base;
}

Dc1Ptr< Mp7JrscurvatureType_CollectionType > Mp7JrscurvatureType::GetBaseCollection() const
{
	return GetBase();
}

void Mp7JrscurvatureType::Initialize(unsigned int maxElems)
{
	GetBase()->Initialize(maxElems);
}

void Mp7JrscurvatureType::addElement(const Dc1NodePtr &toAdd, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrscurvatureType::addElement().");
	}
	GetBase()->addElement(toAdd);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_ADDED,
		client, toAdd);
}

void Mp7JrscurvatureType::setElementAt(const Dc1NodePtr &toSet, unsigned int setAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrscurvatureType::setElementAt().");
	}
	GetBase()->setElementAt(toSet, setAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, toSet, setAt);
}

void Mp7JrscurvatureType::insertElementAt(const Dc1NodePtr &toInsert, unsigned int insertAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrscurvatureType::insertElementAt().");
	}
	GetBase()->insertElementAt(toInsert, insertAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_INSERTED,
		client, toInsert, insertAt);
}

void Mp7JrscurvatureType::removeAllElements(Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrscurvatureType::removeAllElements().");
	}
	GetBase()->removeAllElements();
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrscurvatureType::removeElementAt(unsigned int removeAt, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrscurvatureType::removeElementAt().");
	}
	GetBase()->removeElementAt(removeAt);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

bool Mp7JrscurvatureType::containsElement(const Dc1NodePtr &toCheck)
{
	return GetBase()->containsElement(toCheck);
}

unsigned int Mp7JrscurvatureType::curCapacity() const
{
	return GetBase()->curCapacity();
}

const Dc1NodePtr Mp7JrscurvatureType::elementAt(unsigned int getAt) const
{
	return GetBase()->elementAt(getAt);
}

Dc1NodePtr Mp7JrscurvatureType::elementAt(unsigned int getAt)
{
	return GetBase()->elementAt(getAt);
}

Dc1NodeEnum Mp7JrscurvatureType::GetElementsOfType(unsigned int id) const
{
	return GetBase()->GetElementsOfType(id);
}

Dc1NodeEnum Mp7JrscurvatureType::GetElements() const
{
	return GetBase()->GetElements();
}


unsigned int Mp7JrscurvatureType::size() const
{
	return GetBase()->size();
}

void Mp7JrscurvatureType::ensureExtraCapacity(unsigned int length)
{
	GetBase()->ensureExtraCapacity(length);
}

int Mp7JrscurvatureType::elementIndexOf(const Dc1NodePtr &toCheck) const
{
	return GetBase()->elementIndexOf(toCheck);
}


Dc1NodeEnum Mp7JrscurvatureType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	if (isentrypoint)
	{
		// No Dc1NodePtr subelement to get or create for curvatureType
		Dc1XPathParseContext::ErrorNoSubElement(context, "curvatureType");
	}
	return result;
}

XMLCh * Mp7JrscurvatureType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrscurvatureType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}

bool Mp7JrscurvatureType::ContentFromString(const XMLCh * const txt)
{
	if(! txt) return false;
	if(m_Base)
	{
		return m_Base->ContentFromString(txt);
	}
	return false;
}
XMLCh* Mp7JrscurvatureType::ContentToString() const
{
	if(m_Base)
	{
		return m_Base->ContentToString();
	}
	return (XMLCh*)NULL;
}

void Mp7JrscurvatureType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("curvatureType"));
	// Element serialization:

}

bool Mp7JrscurvatureType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

  // Extensionbase is Mp7JrscurvatureType_CollectionType
  // Deserialize cce factory type
  if(m_Base->Deserialize(doc, parent, current))
  {
	found = true;
	parent = * current;
	parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
  }
// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrscurvatureType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrscurvatureType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrscurvatureType_ExtMethodImpl.h


