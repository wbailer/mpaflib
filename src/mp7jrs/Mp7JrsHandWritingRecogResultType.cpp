
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtImplInclude.h


#include "Mp7JrsDSType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrszeroToOneType.h"
#include "Mp7JrsHandWritingRecogResultType_Result_CollectionType.h"
#include "Mp7JrsxPathRefType.h"
#include "Mp7JrsdurationType.h"
#include "Mp7JrsmediaDurationType.h"
#include "Mp7JrsDSType_Header_CollectionType.h"
#include "Mp7JrsHandWritingRecogResultType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsHeaderType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Header
#include "Mp7JrsHandWritingRecogResultType_Result_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Result

#include <assert.h>
IMp7JrsHandWritingRecogResultType::IMp7JrsHandWritingRecogResultType()
{

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtPropInit.h

}

IMp7JrsHandWritingRecogResultType::~IMp7JrsHandWritingRecogResultType()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtPropCleanup.h

}

Mp7JrsHandWritingRecogResultType::Mp7JrsHandWritingRecogResultType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsHandWritingRecogResultType::~Mp7JrsHandWritingRecogResultType()
{
	Cleanup();
}

void Mp7JrsHandWritingRecogResultType::Init()
{
	// Init base
	m_Base = CreateDSType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_HandWritingRecogInformationRef = Mp7JrsReferencePtr(); // Class
	m_HandWritingRecogInformationRef_Exist = false;
	m_Quality = Mp7JrszeroToOnePtr(); // Class with content 
	m_Quality_Exist = false;
	m_Result = Mp7JrsHandWritingRecogResultType_Result_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtMyPropInit.h

}

void Mp7JrsHandWritingRecogResultType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_HandWritingRecogInformationRef);
	// Dc1Factory::DeleteObject(m_Quality);
	// Dc1Factory::DeleteObject(m_Result);
}

void Mp7JrsHandWritingRecogResultType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use HandWritingRecogResultTypePtr, since we
	// might need GetBase(), which isn't defined in IHandWritingRecogResultType
	const Dc1Ptr< Mp7JrsHandWritingRecogResultType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsDSPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsHandWritingRecogResultType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidHandWritingRecogInformationRef())
	{
		// Dc1Factory::DeleteObject(m_HandWritingRecogInformationRef);
		this->SetHandWritingRecogInformationRef(Dc1Factory::CloneObject(tmp->GetHandWritingRecogInformationRef()));
	}
	else
	{
		InvalidateHandWritingRecogInformationRef();
	}
	if (tmp->IsValidQuality())
	{
		// Dc1Factory::DeleteObject(m_Quality);
		this->SetQuality(Dc1Factory::CloneObject(tmp->GetQuality()));
	}
	else
	{
		InvalidateQuality();
	}
		// Dc1Factory::DeleteObject(m_Result);
		this->SetResult(Dc1Factory::CloneObject(tmp->GetResult()));
}

Dc1NodePtr Mp7JrsHandWritingRecogResultType::GetBaseRoot()
{
	return GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsHandWritingRecogResultType::GetBaseRoot() const
{
	return GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsDSType > Mp7JrsHandWritingRecogResultType::GetBase() const
{
	return m_Base;
}

Mp7JrsReferencePtr Mp7JrsHandWritingRecogResultType::GetHandWritingRecogInformationRef() const
{
		return m_HandWritingRecogInformationRef;
}

// Element is optional
bool Mp7JrsHandWritingRecogResultType::IsValidHandWritingRecogInformationRef() const
{
	return m_HandWritingRecogInformationRef_Exist;
}

Mp7JrszeroToOnePtr Mp7JrsHandWritingRecogResultType::GetQuality() const
{
		return m_Quality;
}

// Element is optional
bool Mp7JrsHandWritingRecogResultType::IsValidQuality() const
{
	return m_Quality_Exist;
}

Mp7JrsHandWritingRecogResultType_Result_CollectionPtr Mp7JrsHandWritingRecogResultType::GetResult() const
{
		return m_Result;
}

void Mp7JrsHandWritingRecogResultType::SetHandWritingRecogInformationRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetHandWritingRecogInformationRef().");
	}
	if (m_HandWritingRecogInformationRef != item || m_HandWritingRecogInformationRef_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_HandWritingRecogInformationRef);
		m_HandWritingRecogInformationRef = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_HandWritingRecogInformationRef) m_HandWritingRecogInformationRef->SetParent(m_myself.getPointer());
		if (m_HandWritingRecogInformationRef && m_HandWritingRecogInformationRef->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_HandWritingRecogInformationRef->UseTypeAttribute = true;
		}
		m_HandWritingRecogInformationRef_Exist = true;
		if(m_HandWritingRecogInformationRef != Mp7JrsReferencePtr())
		{
			m_HandWritingRecogInformationRef->SetContentName(XMLString::transcode("HandWritingRecogInformationRef"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidateHandWritingRecogInformationRef()
{
	m_HandWritingRecogInformationRef_Exist = false;
}
void Mp7JrsHandWritingRecogResultType::SetQuality(const Mp7JrszeroToOnePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetQuality().");
	}
	if (m_Quality != item || m_Quality_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Quality);
		m_Quality = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Quality) m_Quality->SetParent(m_myself.getPointer());
		if (m_Quality && m_Quality->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Quality->UseTypeAttribute = true;
		}
		m_Quality_Exist = true;
		if(m_Quality != Mp7JrszeroToOnePtr())
		{
			m_Quality->SetContentName(XMLString::transcode("Quality"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidateQuality()
{
	m_Quality_Exist = false;
}
void Mp7JrsHandWritingRecogResultType::SetResult(const Mp7JrsHandWritingRecogResultType_Result_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetResult().");
	}
	if (m_Result != item)
	{
		// Dc1Factory::DeleteObject(m_Result);
		m_Result = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Result) m_Result->SetParent(m_myself.getPointer());
		if(m_Result != Mp7JrsHandWritingRecogResultType_Result_CollectionPtr())
		{
			m_Result->SetContentName(XMLString::transcode("Result"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

XMLCh * Mp7JrsHandWritingRecogResultType::Getid() const
{
	return GetBase()->Getid();
}

bool Mp7JrsHandWritingRecogResultType::Existid() const
{
	return GetBase()->Existid();
}
void Mp7JrsHandWritingRecogResultType::Setid(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::Setid().");
	}
	GetBase()->Setid(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::Invalidateid()
{
	GetBase()->Invalidateid();
}
Mp7JrsxPathRefPtr Mp7JrsHandWritingRecogResultType::GettimeBase() const
{
	return GetBase()->GettimeBase();
}

bool Mp7JrsHandWritingRecogResultType::ExisttimeBase() const
{
	return GetBase()->ExisttimeBase();
}
void Mp7JrsHandWritingRecogResultType::SettimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SettimeBase().");
	}
	GetBase()->SettimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidatetimeBase()
{
	GetBase()->InvalidatetimeBase();
}
Mp7JrsdurationPtr Mp7JrsHandWritingRecogResultType::GettimeUnit() const
{
	return GetBase()->GettimeUnit();
}

bool Mp7JrsHandWritingRecogResultType::ExisttimeUnit() const
{
	return GetBase()->ExisttimeUnit();
}
void Mp7JrsHandWritingRecogResultType::SettimeUnit(const Mp7JrsdurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SettimeUnit().");
	}
	GetBase()->SettimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidatetimeUnit()
{
	GetBase()->InvalidatetimeUnit();
}
Mp7JrsxPathRefPtr Mp7JrsHandWritingRecogResultType::GetmediaTimeBase() const
{
	return GetBase()->GetmediaTimeBase();
}

bool Mp7JrsHandWritingRecogResultType::ExistmediaTimeBase() const
{
	return GetBase()->ExistmediaTimeBase();
}
void Mp7JrsHandWritingRecogResultType::SetmediaTimeBase(const Mp7JrsxPathRefPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetmediaTimeBase().");
	}
	GetBase()->SetmediaTimeBase(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidatemediaTimeBase()
{
	GetBase()->InvalidatemediaTimeBase();
}
Mp7JrsmediaDurationPtr Mp7JrsHandWritingRecogResultType::GetmediaTimeUnit() const
{
	return GetBase()->GetmediaTimeUnit();
}

bool Mp7JrsHandWritingRecogResultType::ExistmediaTimeUnit() const
{
	return GetBase()->ExistmediaTimeUnit();
}
void Mp7JrsHandWritingRecogResultType::SetmediaTimeUnit(const Mp7JrsmediaDurationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetmediaTimeUnit().");
	}
	GetBase()->SetmediaTimeUnit(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsHandWritingRecogResultType::InvalidatemediaTimeUnit()
{
	GetBase()->InvalidatemediaTimeUnit();
}
Mp7JrsDSType_Header_CollectionPtr Mp7JrsHandWritingRecogResultType::GetHeader() const
{
	return GetBase()->GetHeader();
}

void Mp7JrsHandWritingRecogResultType::SetHeader(const Mp7JrsDSType_Header_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsHandWritingRecogResultType::SetHeader().");
	}
	GetBase()->SetHeader(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsHandWritingRecogResultType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("HandWritingRecogInformationRef")) == 0)
	{
		// HandWritingRecogInformationRef is simple element ReferenceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetHandWritingRecogInformationRef()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ReferenceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsReferencePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetHandWritingRecogInformationRef(p, client);
					if((p = GetHandWritingRecogInformationRef()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Quality")) == 0)
	{
		// Quality is simple element zeroToOneType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetQuality()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:zeroToOneType")))) != empty)
			{
				// Is type allowed
				Mp7JrszeroToOnePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetQuality(p, client);
					if((p = GetQuality()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Result")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Result is item of type HandWritingRecogResultType_Result_LocalType
		// in element collection HandWritingRecogResultType_Result_CollectionType
		
		context->Found = true;
		Mp7JrsHandWritingRecogResultType_Result_CollectionPtr coll = GetResult();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateHandWritingRecogResultType_Result_CollectionType; // FTT, check this
				SetResult(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:HandWritingRecogResultType_Result_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsHandWritingRecogResultType_Result_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for HandWritingRecogResultType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "HandWritingRecogResultType");
	}
	return result;
}

XMLCh * Mp7JrsHandWritingRecogResultType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsHandWritingRecogResultType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsHandWritingRecogResultType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogResultType"));
	// Element serialization:
	// Class
	
	if (m_HandWritingRecogInformationRef != Mp7JrsReferencePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_HandWritingRecogInformationRef->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("HandWritingRecogInformationRef"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_HandWritingRecogInformationRef->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_Quality != Mp7JrszeroToOnePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Quality->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Quality"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Quality->Serialize(doc, element, &elem);
	}
	if (m_Result != Mp7JrsHandWritingRecogResultType_Result_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Result->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsHandWritingRecogResultType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsDSType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("HandWritingRecogInformationRef"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("HandWritingRecogInformationRef")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateReferenceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetHandWritingRecogInformationRef(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Quality"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Quality")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreatezeroToOneType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetQuality(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Result"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Result")) == 0))
		{
			// Deserialize factory type
			Mp7JrsHandWritingRecogResultType_Result_CollectionPtr tmp = CreateHandWritingRecogResultType_Result_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetResult(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsHandWritingRecogResultType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("HandWritingRecogInformationRef")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateReferenceType; // FTT, check this
	}
	this->SetHandWritingRecogInformationRef(child);
  }
  if (XMLString::compareString(elementname, X("Quality")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatezeroToOneType; // FTT, check this
	}
	this->SetQuality(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Result")) == 0))
  {
	Mp7JrsHandWritingRecogResultType_Result_CollectionPtr tmp = CreateHandWritingRecogResultType_Result_CollectionType; // FTT, check this
	this->SetResult(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsHandWritingRecogResultType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetHandWritingRecogInformationRef() != Dc1NodePtr())
		result.Insert(GetHandWritingRecogInformationRef());
	if (GetQuality() != Dc1NodePtr())
		result.Insert(GetQuality());
	if (GetResult() != Dc1NodePtr())
		result.Insert(GetResult());
	if (GetHeader() != Dc1NodePtr())
		result.Insert(GetHeader());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsHandWritingRecogResultType_ExtMethodImpl.h


