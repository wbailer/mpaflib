
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtImplInclude.h


#include "Mp7JrsMediaReviewDescriptionType.h"
#include "Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType.h"
#include "Mp7JrstimePointType.h"
#include "Mp7JrsReferenceType.h"
#include "Mp7JrsKeywordAnnotationType.h"
#include "Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionType.h"
#include "Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionType.h"
#include "Mp7JrsRatingType.h"
#include "Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionType.h"
#include "Mp7JrsDescriptionMetadataType.h"
#include "Mp7JrsCompleteDescriptionType_Relationships_CollectionType.h"
#include "Mp7JrsCompleteDescriptionType_OrderingKey_CollectionType.h"
#include "Mp7JrsAggregatedMediaReviewDescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsGraphType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Relationships
#include "Mp7JrsOrderingKeyType.h" // Element collection urn:mpeg:mpeg7:schema:2004:OrderingKey
#include "Mp7JrsTextualType.h" // Element collection urn:mpeg:mpeg7:schema:2004:FreeTextReview
#include "Mp7JrsMediaQualityType.h" // Element collection urn:mpeg:mpeg7:schema:2004:QualityRating
#include "Mp7JrsTimeType.h" // Element collection urn:mpeg:mpeg7:schema:2004:ObservationPeriod

#include <assert.h>
IMp7JrsAggregatedMediaReviewDescriptionType::IMp7JrsAggregatedMediaReviewDescriptionType()
{

// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtPropInit.h

}

IMp7JrsAggregatedMediaReviewDescriptionType::~IMp7JrsAggregatedMediaReviewDescriptionType()
{
// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtPropCleanup.h

}

Mp7JrsAggregatedMediaReviewDescriptionType::Mp7JrsAggregatedMediaReviewDescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsAggregatedMediaReviewDescriptionType::~Mp7JrsAggregatedMediaReviewDescriptionType()
{
	Cleanup();
}

void Mp7JrsAggregatedMediaReviewDescriptionType::Init()
{
	// Init base
	m_Base = CreateMediaReviewDescriptionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ObservationPeriod = Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr(); // Collection
	m_ReviewCount = (0); // Value



// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtMyPropInit.h

}

void Mp7JrsAggregatedMediaReviewDescriptionType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ObservationPeriod);
}

void Mp7JrsAggregatedMediaReviewDescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use AggregatedMediaReviewDescriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IAggregatedMediaReviewDescriptionType
	const Dc1Ptr< Mp7JrsAggregatedMediaReviewDescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsMediaReviewDescriptionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsAggregatedMediaReviewDescriptionType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_ObservationPeriod);
		this->SetObservationPeriod(Dc1Factory::CloneObject(tmp->GetObservationPeriod()));
		this->SetReviewCount(tmp->GetReviewCount());
}

Dc1NodePtr Mp7JrsAggregatedMediaReviewDescriptionType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsAggregatedMediaReviewDescriptionType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsMediaReviewDescriptionType > Mp7JrsAggregatedMediaReviewDescriptionType::GetBase() const
{
	return m_Base;
}

Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetObservationPeriod() const
{
		return m_ObservationPeriod;
}

unsigned Mp7JrsAggregatedMediaReviewDescriptionType::GetReviewCount() const
{
		return m_ReviewCount;
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetObservationPeriod(const Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetObservationPeriod().");
	}
	if (m_ObservationPeriod != item)
	{
		// Dc1Factory::DeleteObject(m_ObservationPeriod);
		m_ObservationPeriod = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ObservationPeriod) m_ObservationPeriod->SetParent(m_myself.getPointer());
		if(m_ObservationPeriod != Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr())
		{
			m_ObservationPeriod->SetContentName(XMLString::transcode("ObservationPeriod"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetReviewCount(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetReviewCount().");
	}
	if (m_ReviewCount != item)
	{
		m_ReviewCount = item;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrstimePointPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetreviewTime() const
{
	return GetBase()->GetreviewTime();
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetreviewTime(const Mp7JrstimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetreviewTime().");
	}
	GetBase()->SetreviewTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsReferencePtr Mp7JrsAggregatedMediaReviewDescriptionType::GetContentRef() const
{
	return GetBase()->GetContentRef();
}

Mp7JrsKeywordAnnotationPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetTags() const
{
	return GetBase()->GetTags();
}

// Element is optional
bool Mp7JrsAggregatedMediaReviewDescriptionType::IsValidTags() const
{
	return GetBase()->IsValidTags();
}

Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetFreeTextReview() const
{
	return GetBase()->GetFreeTextReview();
}

Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetMediaRating() const
{
	return GetBase()->GetMediaRating();
}

Mp7JrsRatingPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetIdentityRating() const
{
	return GetBase()->GetIdentityRating();
}

// Element is optional
bool Mp7JrsAggregatedMediaReviewDescriptionType::IsValidIdentityRating() const
{
	return GetBase()->IsValidIdentityRating();
}

Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetQualityRating() const
{
	return GetBase()->GetQualityRating();
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetContentRef(const Mp7JrsReferencePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetContentRef().");
	}
	GetBase()->SetContentRef(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetTags(const Mp7JrsKeywordAnnotationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetTags().");
	}
	GetBase()->SetTags(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::InvalidateTags()
{
	GetBase()->InvalidateTags();
}
void Mp7JrsAggregatedMediaReviewDescriptionType::SetFreeTextReview(const Mp7JrsMediaReviewDescriptionType_FreeTextReview_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetFreeTextReview().");
	}
	GetBase()->SetFreeTextReview(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetMediaRating(const Mp7JrsMediaReviewDescriptionType_MediaRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetMediaRating().");
	}
	GetBase()->SetMediaRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetIdentityRating(const Mp7JrsRatingPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetIdentityRating().");
	}
	GetBase()->SetIdentityRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::InvalidateIdentityRating()
{
	GetBase()->InvalidateIdentityRating();
}
void Mp7JrsAggregatedMediaReviewDescriptionType::SetQualityRating(const Mp7JrsMediaReviewDescriptionType_QualityRating_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetQualityRating().");
	}
	GetBase()->SetQualityRating(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsDescriptionMetadataPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->GetDescriptionMetadata();
}

// Element is optional
bool Mp7JrsAggregatedMediaReviewDescriptionType::IsValidDescriptionMetadata() const
{
	return GetBase()->GetBase()->GetBase()->IsValidDescriptionMetadata();
}

Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetRelationships() const
{
	return GetBase()->GetBase()->GetBase()->GetRelationships();
}

Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr Mp7JrsAggregatedMediaReviewDescriptionType::GetOrderingKey() const
{
	return GetBase()->GetBase()->GetBase()->GetOrderingKey();
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetDescriptionMetadata(const Mp7JrsDescriptionMetadataPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetDescriptionMetadata().");
	}
	GetBase()->GetBase()->GetBase()->SetDescriptionMetadata(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::InvalidateDescriptionMetadata()
{
	GetBase()->GetBase()->GetBase()->InvalidateDescriptionMetadata();
}
void Mp7JrsAggregatedMediaReviewDescriptionType::SetRelationships(const Mp7JrsCompleteDescriptionType_Relationships_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetRelationships().");
	}
	GetBase()->GetBase()->GetBase()->SetRelationships(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsAggregatedMediaReviewDescriptionType::SetOrderingKey(const Mp7JrsCompleteDescriptionType_OrderingKey_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsAggregatedMediaReviewDescriptionType::SetOrderingKey().");
	}
	GetBase()->GetBase()->GetBase()->SetOrderingKey(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsAggregatedMediaReviewDescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 1 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ObservationPeriod")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:ObservationPeriod is item of type TimeType
		// in element collection AggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType
		
		context->Found = true;
		Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr coll = GetObservationPeriod();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType; // FTT, check this
				SetObservationPeriod(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TimeType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTimePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for AggregatedMediaReviewDescriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "AggregatedMediaReviewDescriptionType");
	}
	return result;
}

XMLCh * Mp7JrsAggregatedMediaReviewDescriptionType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsAggregatedMediaReviewDescriptionType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsAggregatedMediaReviewDescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("AggregatedMediaReviewDescriptionType"));
	// Element serialization:
	if (m_ObservationPeriod != Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_ObservationPeriod->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_ReviewCount);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("ReviewCount"), false);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsAggregatedMediaReviewDescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsMediaReviewDescriptionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("ObservationPeriod"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("ObservationPeriod")) == 0))
		{
			// Deserialize factory type
			Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr tmp = CreateAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetObservationPeriod(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("ReviewCount"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetReviewCount(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsAggregatedMediaReviewDescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("ObservationPeriod")) == 0))
  {
	Mp7JrsAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionPtr tmp = CreateAggregatedMediaReviewDescriptionType_ObservationPeriod_CollectionType; // FTT, check this
	this->SetObservationPeriod(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsAggregatedMediaReviewDescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetObservationPeriod() != Dc1NodePtr())
		result.Insert(GetObservationPeriod());
	if (GetContentRef() != Dc1NodePtr())
		result.Insert(GetContentRef());
	if (GetTags() != Dc1NodePtr())
		result.Insert(GetTags());
	if (GetFreeTextReview() != Dc1NodePtr())
		result.Insert(GetFreeTextReview());
	if (GetMediaRating() != Dc1NodePtr())
		result.Insert(GetMediaRating());
	if (GetIdentityRating() != Dc1NodePtr())
		result.Insert(GetIdentityRating());
	if (GetQualityRating() != Dc1NodePtr())
		result.Insert(GetQualityRating());
	if (GetDescriptionMetadata() != Dc1NodePtr())
		result.Insert(GetDescriptionMetadata());
	if (GetRelationships() != Dc1NodePtr())
		result.Insert(GetRelationships());
	if (GetOrderingKey() != Dc1NodePtr())
		result.Insert(GetOrderingKey());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsAggregatedMediaReviewDescriptionType_ExtMethodImpl.h


