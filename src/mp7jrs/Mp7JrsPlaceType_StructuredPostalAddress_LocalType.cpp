
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtImplInclude.h


#include "Mp7JrscountryCode.h"
#include "Mp7JrsTextualType.h"
#include "Mp7JrsPlaceType_StructuredPostalAddress_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IMp7JrsPlaceType_StructuredPostalAddress_LocalType::IMp7JrsPlaceType_StructuredPostalAddress_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtPropInit.h

}

IMp7JrsPlaceType_StructuredPostalAddress_LocalType::~IMp7JrsPlaceType_StructuredPostalAddress_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtPropCleanup.h

}

Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Mp7JrsPlaceType_StructuredPostalAddress_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsPlaceType_StructuredPostalAddress_LocalType::~Mp7JrsPlaceType_StructuredPostalAddress_LocalType()
{
	Cleanup();
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_StreetNumber = NULL; // Optional String
	m_StreetNumber_Exist = false;
	m_StreetName = NULL; // Optional String
	m_StreetName_Exist = false;
	m_PostalTown = NULL; // Optional String
	m_PostalTown_Exist = false;
	m_City = NULL; // Optional String
	m_City_Exist = false;
	m_StateProvinceCounty = NULL; // Optional String
	m_StateProvinceCounty_Exist = false;
	m_Country = Mp7JrscountryCodePtr(); // Pattern
	m_Country_Exist = false;
	m_PostingIdentifier = Mp7JrsTextualPtr(); // Class
	m_PostingIdentifier_Exist = false;


// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtMyPropInit.h

}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtMyPropCleanup.h


	XMLString::release(&this->m_StreetNumber);
	this->m_StreetNumber = NULL;
	XMLString::release(&this->m_StreetName);
	this->m_StreetName = NULL;
	XMLString::release(&this->m_PostalTown);
	this->m_PostalTown = NULL;
	XMLString::release(&this->m_City);
	this->m_City = NULL;
	XMLString::release(&this->m_StateProvinceCounty);
	this->m_StateProvinceCounty = NULL;
	// Dc1Factory::DeleteObject(m_Country);
	// Dc1Factory::DeleteObject(m_PostingIdentifier);
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use PlaceType_StructuredPostalAddress_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IPlaceType_StructuredPostalAddress_LocalType
	const Dc1Ptr< Mp7JrsPlaceType_StructuredPostalAddress_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidStreetNumber())
	{
		this->SetStreetNumber(XMLString::replicate(tmp->GetStreetNumber()));
	}
	else
	{
		InvalidateStreetNumber();
	}
	if (tmp->IsValidStreetName())
	{
		this->SetStreetName(XMLString::replicate(tmp->GetStreetName()));
	}
	else
	{
		InvalidateStreetName();
	}
	if (tmp->IsValidPostalTown())
	{
		this->SetPostalTown(XMLString::replicate(tmp->GetPostalTown()));
	}
	else
	{
		InvalidatePostalTown();
	}
	if (tmp->IsValidCity())
	{
		this->SetCity(XMLString::replicate(tmp->GetCity()));
	}
	else
	{
		InvalidateCity();
	}
	if (tmp->IsValidStateProvinceCounty())
	{
		this->SetStateProvinceCounty(XMLString::replicate(tmp->GetStateProvinceCounty()));
	}
	else
	{
		InvalidateStateProvinceCounty();
	}
	if (tmp->IsValidCountry())
	{
		// Dc1Factory::DeleteObject(m_Country);
		this->SetCountry(Dc1Factory::CloneObject(tmp->GetCountry()));
	}
	else
	{
		InvalidateCountry();
	}
	if (tmp->IsValidPostingIdentifier())
	{
		// Dc1Factory::DeleteObject(m_PostingIdentifier);
		this->SetPostingIdentifier(Dc1Factory::CloneObject(tmp->GetPostingIdentifier()));
	}
	else
	{
		InvalidatePostingIdentifier();
	}
}

Dc1NodePtr Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetStreetNumber() const
{
		return m_StreetNumber;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidStreetNumber() const
{
	return m_StreetNumber_Exist;
}

XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetStreetName() const
{
		return m_StreetName;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidStreetName() const
{
	return m_StreetName_Exist;
}

XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetPostalTown() const
{
		return m_PostalTown;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidPostalTown() const
{
	return m_PostalTown_Exist;
}

XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetCity() const
{
		return m_City;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidCity() const
{
	return m_City_Exist;
}

XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetStateProvinceCounty() const
{
		return m_StateProvinceCounty;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidStateProvinceCounty() const
{
	return m_StateProvinceCounty_Exist;
}

Mp7JrscountryCodePtr Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetCountry() const
{
		return m_Country;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidCountry() const
{
	return m_Country_Exist;
}

Mp7JrsTextualPtr Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetPostingIdentifier() const
{
		return m_PostingIdentifier;
}

// Element is optional
bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::IsValidPostingIdentifier() const
{
	return m_PostingIdentifier_Exist;
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStreetNumber(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStreetNumber().");
	}
	if (m_StreetNumber != item || m_StreetNumber_Exist == false)
	{
		XMLString::release(&m_StreetNumber);
		m_StreetNumber = item;
		m_StreetNumber_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidateStreetNumber()
{
	m_StreetNumber_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStreetName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStreetName().");
	}
	if (m_StreetName != item || m_StreetName_Exist == false)
	{
		XMLString::release(&m_StreetName);
		m_StreetName = item;
		m_StreetName_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidateStreetName()
{
	m_StreetName_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetPostalTown(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetPostalTown().");
	}
	if (m_PostalTown != item || m_PostalTown_Exist == false)
	{
		XMLString::release(&m_PostalTown);
		m_PostalTown = item;
		m_PostalTown_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidatePostalTown()
{
	m_PostalTown_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetCity(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetCity().");
	}
	if (m_City != item || m_City_Exist == false)
	{
		XMLString::release(&m_City);
		m_City = item;
		m_City_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidateCity()
{
	m_City_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStateProvinceCounty(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetStateProvinceCounty().");
	}
	if (m_StateProvinceCounty != item || m_StateProvinceCounty_Exist == false)
	{
		XMLString::release(&m_StateProvinceCounty);
		m_StateProvinceCounty = item;
		m_StateProvinceCounty_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidateStateProvinceCounty()
{
	m_StateProvinceCounty_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetCountry(const Mp7JrscountryCodePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetCountry().");
	}
	if (m_Country != item || m_Country_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Country);
		m_Country = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Country) m_Country->SetParent(m_myself.getPointer());
		if (m_Country && m_Country->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Country->UseTypeAttribute = true;
		}
		m_Country_Exist = true;
		if(m_Country != Mp7JrscountryCodePtr())
		{
			m_Country->SetContentName(XMLString::transcode("Country"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidateCountry()
{
	m_Country_Exist = false;
}
void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetPostingIdentifier(const Mp7JrsTextualPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsPlaceType_StructuredPostalAddress_LocalType::SetPostingIdentifier().");
	}
	if (m_PostingIdentifier != item || m_PostingIdentifier_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_PostingIdentifier);
		m_PostingIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_PostingIdentifier) m_PostingIdentifier->SetParent(m_myself.getPointer());
		if (m_PostingIdentifier && m_PostingIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_PostingIdentifier->UseTypeAttribute = true;
		}
		m_PostingIdentifier_Exist = true;
		if(m_PostingIdentifier != Mp7JrsTextualPtr())
		{
			m_PostingIdentifier->SetContentName(XMLString::transcode("PostingIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::InvalidatePostingIdentifier()
{
	m_PostingIdentifier_Exist = false;
}

Dc1NodeEnum Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Country")) == 0)
	{
		// Country is simple element countryCode
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetCountry()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:countryCode")))) != empty)
			{
				// Is type allowed
				Mp7JrscountryCodePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetCountry(p, client);
					if((p = GetCountry()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("PostingIdentifier")) == 0)
	{
		// PostingIdentifier is simple element TextualType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetPostingIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TextualType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTextualPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetPostingIdentifier(p, client);
					if((p = GetPostingIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for PlaceType_StructuredPostalAddress_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "PlaceType_StructuredPostalAddress_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsPlaceType_StructuredPostalAddress_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("PlaceType_StructuredPostalAddress_LocalType"));
	// Element serialization:
	 // String
	if(m_StreetNumber != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_StreetNumber, X("urn:mpeg:mpeg7:schema:2004"), X("StreetNumber"), true);
	 // String
	if(m_StreetName != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_StreetName, X("urn:mpeg:mpeg7:schema:2004"), X("StreetName"), true);
	 // String
	if(m_PostalTown != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_PostalTown, X("urn:mpeg:mpeg7:schema:2004"), X("PostalTown"), true);
	 // String
	if(m_City != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_City, X("urn:mpeg:mpeg7:schema:2004"), X("City"), true);
	 // String
	if(m_StateProvinceCounty != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_StateProvinceCounty, X("urn:mpeg:mpeg7:schema:2004"), X("StateProvinceCounty"), true);
	if(m_Country != Mp7JrscountryCodePtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("Country");
//		m_Country->SetContentName(contentname);
		m_Country->UseTypeAttribute = this->UseTypeAttribute;
		m_Country->Serialize(doc, element);
	}
	// Class
	
	if (m_PostingIdentifier != Mp7JrsTextualPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_PostingIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("PostingIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_PostingIdentifier->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsPlaceType_StructuredPostalAddress_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("StreetNumber"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetStreetNumber(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetStreetNumber(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("StreetName"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetStreetName(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetStreetName(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("PostalTown"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetPostalTown(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetPostalTown(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("City"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetCity(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetCity(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("StateProvinceCounty"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetStateProvinceCounty(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetStateProvinceCounty(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("Country"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Country")) == 0))
		{
			Mp7JrscountryCodePtr tmp = CreatecountryCode; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetCountry(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("PostingIdentifier"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("PostingIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTextualType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetPostingIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsPlaceType_StructuredPostalAddress_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("Country")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatecountryCode; // FTT, check this
	}
	this->SetCountry(child);
  }
  if (XMLString::compareString(elementname, X("PostingIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTextualType; // FTT, check this
	}
	this->SetPostingIdentifier(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsPlaceType_StructuredPostalAddress_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetCountry() != Dc1NodePtr())
		result.Insert(GetCountry());
	if (GetPostingIdentifier() != Dc1NodePtr())
		result.Insert(GetPostingIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsPlaceType_StructuredPostalAddress_LocalType_ExtMethodImpl.h


