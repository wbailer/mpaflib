
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtImplInclude.h


#include "Mp7JrsPreferenceConditionType.h"
#include "Mp7JrsTermUseType.h"
#include "Mp7JrsPlaceType.h"
#include "Mp7JrsPreferenceConditionType_Time_CollectionType.h"
#include "Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsPreferenceConditionType_Time_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Time

#include <assert.h>
IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtPropInit.h

}

IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::~IMp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtPropCleanup.h

}

Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::~Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType()
{
	Cleanup();
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Init()
{
	// Init base
	m_Base = CreatePreferenceConditionType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_Genre = Mp7JrsTermUsePtr(); // Class
	m_Genre_Exist = false;


// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtMyPropInit.h

}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_Genre);
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use BrowsingPreferencesType_PreferenceCondition_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IBrowsingPreferencesType_PreferenceCondition_LocalType
	const Dc1Ptr< Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsPreferenceConditionPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidGenre())
	{
		// Dc1Factory::DeleteObject(m_Genre);
		this->SetGenre(Dc1Factory::CloneObject(tmp->GetGenre()));
	}
	else
	{
		InvalidateGenre();
	}
}

Dc1NodePtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsPreferenceConditionType > Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetBase() const
{
	return m_Base;
}

Mp7JrsTermUsePtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetGenre() const
{
		return m_Genre;
}

// Element is optional
bool Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::IsValidGenre() const
{
	return m_Genre_Exist;
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetGenre(const Mp7JrsTermUsePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetGenre().");
	}
	if (m_Genre != item || m_Genre_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_Genre);
		m_Genre = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Genre) m_Genre->SetParent(m_myself.getPointer());
		if (m_Genre && m_Genre->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_Genre->UseTypeAttribute = true;
		}
		m_Genre_Exist = true;
		if(m_Genre != Mp7JrsTermUsePtr())
		{
			m_Genre->SetContentName(XMLString::transcode("Genre"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::InvalidateGenre()
{
	m_Genre_Exist = false;
}
Mp7JrsPlacePtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetPlace() const
{
	return GetBase()->GetPlace();
}

// Element is optional
bool Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::IsValidPlace() const
{
	return GetBase()->IsValidPlace();
}

Mp7JrsPreferenceConditionType_Time_CollectionPtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetTime() const
{
	return GetBase()->GetTime();
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetPlace(const Mp7JrsPlacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetPlace().");
	}
	GetBase()->SetPlace(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::InvalidatePlace()
{
	GetBase()->InvalidatePlace();
}
void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetTime(const Mp7JrsPreferenceConditionType_Time_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::SetTime().");
	}
	GetBase()->SetTime(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("Genre")) == 0)
	{
		// Genre is simple element TermUseType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetGenre()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:TermUseType")))) != empty)
			{
				// Is type allowed
				Mp7JrsTermUsePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetGenre(p, client);
					if((p = GetGenre()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for BrowsingPreferencesType_PreferenceCondition_LocalType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "BrowsingPreferencesType_PreferenceCondition_LocalType");
	}
	return result;
}

XMLCh * Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("BrowsingPreferencesType_PreferenceCondition_LocalType"));
	// Element serialization:
	// Class
	
	if (m_Genre != Mp7JrsTermUsePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_Genre->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("Genre"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_Genre->Serialize(doc, element, &elem);
	}

}

bool Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsPreferenceConditionType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("Genre"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("Genre")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateTermUseType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetGenre(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("Genre")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateTermUseType; // FTT, check this
	}
	this->SetGenre(child);
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetGenre() != Dc1NodePtr())
		result.Insert(GetGenre());
	if (GetPlace() != Dc1NodePtr())
		result.Insert(GetPlace());
	if (GetTime() != Dc1NodePtr())
		result.Insert(GetTime());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsBrowsingPreferencesType_PreferenceCondition_LocalType_ExtMethodImpl.h


