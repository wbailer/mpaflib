
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtImplInclude.h


#include "Mp7JrsVisualDType.h"
#include "Mp7JrsColorSpaceType.h"
#include "Mp7JrsColorQuantizationType.h"
#include "Mp7Jrsunsigned5.h"
#include "Mp7JrsDominantColorType_Value_CollectionType.h"
#include "Mp7JrsDominantColorType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsDominantColorType_Value_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Value

#include <assert.h>
IMp7JrsDominantColorType::IMp7JrsDominantColorType()
{

// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtPropInit.h

}

IMp7JrsDominantColorType::~IMp7JrsDominantColorType()
{
// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtPropCleanup.h

}

Mp7JrsDominantColorType::Mp7JrsDominantColorType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsDominantColorType::~Mp7JrsDominantColorType()
{
	Cleanup();
}

void Mp7JrsDominantColorType::Init()
{
	// Init base
	m_Base = CreateVisualDType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_ColorSpace = Mp7JrsColorSpacePtr(); // Class
	m_ColorSpace_Exist = false;
	m_ColorQuantization = Mp7JrsColorQuantizationPtr(); // Class
	m_ColorQuantization_Exist = false;
	m_SpatialCoherency = Mp7Jrsunsigned5Ptr(); // Class with content 
	m_Value = Mp7JrsDominantColorType_Value_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtMyPropInit.h

}

void Mp7JrsDominantColorType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_ColorSpace);
	// Dc1Factory::DeleteObject(m_ColorQuantization);
	// Dc1Factory::DeleteObject(m_SpatialCoherency);
	// Dc1Factory::DeleteObject(m_Value);
}

void Mp7JrsDominantColorType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use DominantColorTypePtr, since we
	// might need GetBase(), which isn't defined in IDominantColorType
	const Dc1Ptr< Mp7JrsDominantColorType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsVisualDPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsDominantColorType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
	if (tmp->IsValidColorSpace())
	{
		// Dc1Factory::DeleteObject(m_ColorSpace);
		this->SetColorSpace(Dc1Factory::CloneObject(tmp->GetColorSpace()));
	}
	else
	{
		InvalidateColorSpace();
	}
	if (tmp->IsValidColorQuantization())
	{
		// Dc1Factory::DeleteObject(m_ColorQuantization);
		this->SetColorQuantization(Dc1Factory::CloneObject(tmp->GetColorQuantization()));
	}
	else
	{
		InvalidateColorQuantization();
	}
		// Dc1Factory::DeleteObject(m_SpatialCoherency);
		this->SetSpatialCoherency(Dc1Factory::CloneObject(tmp->GetSpatialCoherency()));
		// Dc1Factory::DeleteObject(m_Value);
		this->SetValue(Dc1Factory::CloneObject(tmp->GetValue()));
}

Dc1NodePtr Mp7JrsDominantColorType::GetBaseRoot()
{
	return GetBase()->GetBase()->GetBase();
}

const Dc1NodePtr Mp7JrsDominantColorType::GetBaseRoot() const
{
	return GetBase()->GetBase()->GetBase();
}


Dc1Ptr< Mp7JrsVisualDType > Mp7JrsDominantColorType::GetBase() const
{
	return m_Base;
}

Mp7JrsColorSpacePtr Mp7JrsDominantColorType::GetColorSpace() const
{
		return m_ColorSpace;
}

// Element is optional
bool Mp7JrsDominantColorType::IsValidColorSpace() const
{
	return m_ColorSpace_Exist;
}

Mp7JrsColorQuantizationPtr Mp7JrsDominantColorType::GetColorQuantization() const
{
		return m_ColorQuantization;
}

// Element is optional
bool Mp7JrsDominantColorType::IsValidColorQuantization() const
{
	return m_ColorQuantization_Exist;
}

Mp7Jrsunsigned5Ptr Mp7JrsDominantColorType::GetSpatialCoherency() const
{
		return m_SpatialCoherency;
}

Mp7JrsDominantColorType_Value_CollectionPtr Mp7JrsDominantColorType::GetValue() const
{
		return m_Value;
}

void Mp7JrsDominantColorType::SetColorSpace(const Mp7JrsColorSpacePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDominantColorType::SetColorSpace().");
	}
	if (m_ColorSpace != item || m_ColorSpace_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorSpace);
		m_ColorSpace = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorSpace) m_ColorSpace->SetParent(m_myself.getPointer());
		if (m_ColorSpace && m_ColorSpace->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorSpace->UseTypeAttribute = true;
		}
		m_ColorSpace_Exist = true;
		if(m_ColorSpace != Mp7JrsColorSpacePtr())
		{
			m_ColorSpace->SetContentName(XMLString::transcode("ColorSpace"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDominantColorType::InvalidateColorSpace()
{
	m_ColorSpace_Exist = false;
}
void Mp7JrsDominantColorType::SetColorQuantization(const Mp7JrsColorQuantizationPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDominantColorType::SetColorQuantization().");
	}
	if (m_ColorQuantization != item || m_ColorQuantization_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_ColorQuantization);
		m_ColorQuantization = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_ColorQuantization) m_ColorQuantization->SetParent(m_myself.getPointer());
		if (m_ColorQuantization && m_ColorQuantization->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_ColorQuantization->UseTypeAttribute = true;
		}
		m_ColorQuantization_Exist = true;
		if(m_ColorQuantization != Mp7JrsColorQuantizationPtr())
		{
			m_ColorQuantization->SetContentName(XMLString::transcode("ColorQuantization"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDominantColorType::InvalidateColorQuantization()
{
	m_ColorQuantization_Exist = false;
}
void Mp7JrsDominantColorType::SetSpatialCoherency(const Mp7Jrsunsigned5Ptr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDominantColorType::SetSpatialCoherency().");
	}
	if (m_SpatialCoherency != item)
	{
		// Dc1Factory::DeleteObject(m_SpatialCoherency);
		m_SpatialCoherency = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_SpatialCoherency) m_SpatialCoherency->SetParent(m_myself.getPointer());
		if (m_SpatialCoherency && m_SpatialCoherency->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned5"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_SpatialCoherency->UseTypeAttribute = true;
		}
		if(m_SpatialCoherency != Mp7Jrsunsigned5Ptr())
		{
			m_SpatialCoherency->SetContentName(XMLString::transcode("SpatialCoherency"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsDominantColorType::SetValue(const Mp7JrsDominantColorType_Value_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsDominantColorType::SetValue().");
	}
	if (m_Value != item)
	{
		// Dc1Factory::DeleteObject(m_Value);
		m_Value = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_Value) m_Value->SetParent(m_myself.getPointer());
		if(m_Value != Mp7JrsDominantColorType_Value_CollectionPtr())
		{
			m_Value->SetContentName(XMLString::transcode("Value"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsDominantColorType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("ColorSpace")) == 0)
	{
		// ColorSpace is simple element ColorSpaceType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorSpace()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorSpaceType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorSpacePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorSpace(p, client);
					if((p = GetColorSpace()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("ColorQuantization")) == 0)
	{
		// ColorQuantization is simple element ColorQuantizationType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetColorQuantization()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:ColorQuantizationType")))) != empty)
			{
				// Is type allowed
				Mp7JrsColorQuantizationPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetColorQuantization(p, client);
					if((p = GetColorQuantization()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("SpatialCoherency")) == 0)
	{
		// SpatialCoherency is simple element unsigned5
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetSpatialCoherency()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:unsigned5")))) != empty)
			{
				// Is type allowed
				Mp7Jrsunsigned5Ptr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetSpatialCoherency(p, client);
					if((p = GetSpatialCoherency()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("Value")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:Value is item of type DominantColorType_Value_LocalType
		// in element collection DominantColorType_Value_CollectionType
		
		context->Found = true;
		Mp7JrsDominantColorType_Value_CollectionPtr coll = GetValue();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateDominantColorType_Value_CollectionType; // FTT, check this
				SetValue(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 8))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:DominantColorType_Value_LocalType")))) != empty)
			{
				// Is type allowed
				Mp7JrsDominantColorType_Value_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for DominantColorType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "DominantColorType");
	}
	return result;
}

XMLCh * Mp7JrsDominantColorType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsDominantColorType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsDominantColorType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("DominantColorType"));
	// Element serialization:
	// Class
	
	if (m_ColorSpace != Mp7JrsColorSpacePtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorSpace->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorSpace"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorSpace->Serialize(doc, element, &elem);
	}
	// Class
	
	if (m_ColorQuantization != Mp7JrsColorQuantizationPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_ColorQuantization->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("ColorQuantization"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_ColorQuantization->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_SpatialCoherency != Mp7Jrsunsigned5Ptr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_SpatialCoherency->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("SpatialCoherency"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_SpatialCoherency->Serialize(doc, element, &elem);
	}
	if (m_Value != Mp7JrsDominantColorType_Value_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_Value->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsDominantColorType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsVisualDType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorSpace"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorSpace")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorSpaceType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorSpace(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("ColorQuantization"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("ColorQuantization")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateColorQuantizationType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetColorQuantization(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("SpatialCoherency"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("SpatialCoherency")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = Createunsigned5; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetSpatialCoherency(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("Value"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("Value")) == 0))
		{
			// Deserialize factory type
			Mp7JrsDominantColorType_Value_CollectionPtr tmp = CreateDominantColorType_Value_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetValue(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsDominantColorType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  if (XMLString::compareString(elementname, X("ColorSpace")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorSpaceType; // FTT, check this
	}
	this->SetColorSpace(child);
  }
  if (XMLString::compareString(elementname, X("ColorQuantization")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateColorQuantizationType; // FTT, check this
	}
	this->SetColorQuantization(child);
  }
  if (XMLString::compareString(elementname, X("SpatialCoherency")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = Createunsigned5; // FTT, check this
	}
	this->SetSpatialCoherency(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("Value")) == 0))
  {
	Mp7JrsDominantColorType_Value_CollectionPtr tmp = CreateDominantColorType_Value_CollectionType; // FTT, check this
	this->SetValue(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsDominantColorType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetColorSpace() != Dc1NodePtr())
		result.Insert(GetColorSpace());
	if (GetColorQuantization() != Dc1NodePtr())
		result.Insert(GetColorQuantization());
	if (GetSpatialCoherency() != Dc1NodePtr())
		result.Insert(GetSpatialCoherency());
	if (GetValue() != Dc1NodePtr())
		result.Insert(GetValue());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsDominantColorType_ExtMethodImpl.h


