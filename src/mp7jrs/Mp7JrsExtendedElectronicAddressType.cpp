
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtImplInclude.h


#include "Mp7JrsElectronicAddressType.h"
#include "Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType.h"
#include "Mp7JrsElectronicAddressType_Telephone_CollectionType.h"
#include "Mp7JrsElectronicAddressType_Fax_CollectionType.h"
#include "Mp7JrsElectronicAddressType_Email_CollectionType.h"
#include "Mp7JrsElectronicAddressType_Url_CollectionType.h"
#include "Mp7JrsExtendedElectronicAddressType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Mp7JrsElectronicAddressType_Telephone_LocalType.h" // Element collection urn:mpeg:mpeg7:schema:2004:Telephone
#include "Mp7JrsInstantMessagingScreenNameType.h" // Element collection urn:mpeg:mpeg7:schema:2004:InstantMessagingScreenName

#include <assert.h>
IMp7JrsExtendedElectronicAddressType::IMp7JrsExtendedElectronicAddressType()
{

// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtPropInit.h

}

IMp7JrsExtendedElectronicAddressType::~IMp7JrsExtendedElectronicAddressType()
{
// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtPropCleanup.h

}

Mp7JrsExtendedElectronicAddressType::Mp7JrsExtendedElectronicAddressType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsExtendedElectronicAddressType::~Mp7JrsExtendedElectronicAddressType()
{
	Cleanup();
}

void Mp7JrsExtendedElectronicAddressType::Init()
{
	// Init base
	m_Base = CreateElectronicAddressType; // FTT, check this
	m_Base->m_myself = m_myself.getPointer();
	


	// Init elements (element, union sequence choice all any)
	
	m_InstantMessagingScreenName = Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtMyPropInit.h

}

void Mp7JrsExtendedElectronicAddressType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_Base);
	// Dc1Factory::DeleteObject(m_InstantMessagingScreenName);
}

void Mp7JrsExtendedElectronicAddressType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use ExtendedElectronicAddressTypePtr, since we
	// might need GetBase(), which isn't defined in IExtendedElectronicAddressType
	const Dc1Ptr< Mp7JrsExtendedElectronicAddressType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	// Dc1Factory::DeleteObject(m_Base);
	// FIXME: won't work with derived types
	m_Base = Dc1Factory::CloneObject(tmp->GetBase());
//	  m_Base = Mp7JrsElectronicAddressPtr(Dc1Factory::CloneObject((dynamic_cast< const Mp7JrsExtendedElectronicAddressType *>(&original))->GetBase().Ptr())); // XXX version pre-smartptr-factory
		// Dc1Factory::DeleteObject(m_InstantMessagingScreenName);
		this->SetInstantMessagingScreenName(Dc1Factory::CloneObject(tmp->GetInstantMessagingScreenName()));
}

Dc1NodePtr Mp7JrsExtendedElectronicAddressType::GetBaseRoot()
{
	return GetBase();
}

const Dc1NodePtr Mp7JrsExtendedElectronicAddressType::GetBaseRoot() const
{
	return GetBase();
}


Dc1Ptr< Mp7JrsElectronicAddressType > Mp7JrsExtendedElectronicAddressType::GetBase() const
{
	return m_Base;
}

Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr Mp7JrsExtendedElectronicAddressType::GetInstantMessagingScreenName() const
{
		return m_InstantMessagingScreenName;
}

void Mp7JrsExtendedElectronicAddressType::SetInstantMessagingScreenName(const Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedElectronicAddressType::SetInstantMessagingScreenName().");
	}
	if (m_InstantMessagingScreenName != item)
	{
		// Dc1Factory::DeleteObject(m_InstantMessagingScreenName);
		m_InstantMessagingScreenName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_InstantMessagingScreenName) m_InstantMessagingScreenName->SetParent(m_myself.getPointer());
		if(m_InstantMessagingScreenName != Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr())
		{
			m_InstantMessagingScreenName->SetContentName(XMLString::transcode("InstantMessagingScreenName"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

Mp7JrsElectronicAddressType_Telephone_CollectionPtr Mp7JrsExtendedElectronicAddressType::GetTelephone() const
{
	return GetBase()->GetTelephone();
}

Mp7JrsElectronicAddressType_Fax_CollectionPtr Mp7JrsExtendedElectronicAddressType::GetFax() const
{
	return GetBase()->GetFax();
}

Mp7JrsElectronicAddressType_Email_CollectionPtr Mp7JrsExtendedElectronicAddressType::GetEmail() const
{
	return GetBase()->GetEmail();
}

Mp7JrsElectronicAddressType_Url_CollectionPtr Mp7JrsExtendedElectronicAddressType::GetUrl() const
{
	return GetBase()->GetUrl();
}

void Mp7JrsExtendedElectronicAddressType::SetTelephone(const Mp7JrsElectronicAddressType_Telephone_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedElectronicAddressType::SetTelephone().");
	}
	GetBase()->SetTelephone(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedElectronicAddressType::SetFax(const Mp7JrsElectronicAddressType_Fax_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedElectronicAddressType::SetFax().");
	}
	GetBase()->SetFax(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedElectronicAddressType::SetEmail(const Mp7JrsElectronicAddressType_Email_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedElectronicAddressType::SetEmail().");
	}
	GetBase()->SetEmail(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsExtendedElectronicAddressType::SetUrl(const Mp7JrsElectronicAddressType_Url_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsExtendedElectronicAddressType::SetUrl().");
	}
	GetBase()->SetUrl(item, client);
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum Mp7JrsExtendedElectronicAddressType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	// Base class first
	Dc1NodeEnum result = m_Base->GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("InstantMessagingScreenName")) == 0)
	{
		// urn:mpeg:mpeg7:schema:2004:InstantMessagingScreenName is item of type InstantMessagingScreenNameType
		// in element collection ExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType
		
		context->Found = true;
		Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr coll = GetInstantMessagingScreenName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType; // FTT, check this
				SetInstantMessagingScreenName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:InstantMessagingScreenNameType")))) != empty)
			{
				// Is type allowed
				Mp7JrsInstantMessagingScreenNamePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for ExtendedElectronicAddressType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "ExtendedElectronicAddressType");
	}
	return result;
}

XMLCh * Mp7JrsExtendedElectronicAddressType::ToText() const
{
	return m_Base->ToText();
}


bool Mp7JrsExtendedElectronicAddressType::Parse(const XMLCh * const txt)
{
	return m_Base->Parse(txt);
}


void Mp7JrsExtendedElectronicAddressType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	// Don't create own node: LocalType is from Choice or Sequence
	else {
		element = parent;
	}


	// pass newElem to base class, so that it contains new node after return
	m_Base->Serialize(doc, element, newElem);
	element = *newElem;

	assert(element);
// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtPreSerialize.h


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("ExtendedElectronicAddressType"));
	// Element serialization:
	if (m_InstantMessagingScreenName != Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_InstantMessagingScreenName->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsExtendedElectronicAddressType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Extensionbase is Mp7JrsElectronicAddressType
	// Deserialize cce factory type
	if(m_Base->Deserialize(doc, parent, current))
	{
		found = true;
		parent = * current;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	else
	{ // This is new for cce only
		parent = Dc1Util::GetNextChild(parent);
	}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("InstantMessagingScreenName"), X("urn:mpeg:mpeg7:schema:2004")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("InstantMessagingScreenName")) == 0))
		{
			// Deserialize factory type
			Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr tmp = CreateExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetInstantMessagingScreenName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsExtendedElectronicAddressType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = m_Base->CreateChild(elementname, xsitype);
  if (child == NULL) { // Base couldn't create child node
	child = Dc1NodePtr();
  } else {
	return child;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("InstantMessagingScreenName")) == 0))
  {
	Mp7JrsExtendedElectronicAddressType_InstantMessagingScreenName_CollectionPtr tmp = CreateExtendedElectronicAddressType_InstantMessagingScreenName_CollectionType; // FTT, check this
	this->SetInstantMessagingScreenName(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsExtendedElectronicAddressType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetInstantMessagingScreenName() != Dc1NodePtr())
		result.Insert(GetInstantMessagingScreenName());
	if (GetTelephone() != Dc1NodePtr())
		result.Insert(GetTelephone());
	if (GetFax() != Dc1NodePtr())
		result.Insert(GetFax());
	if (GetEmail() != Dc1NodePtr())
		result.Insert(GetEmail());
	if (GetUrl() != Dc1NodePtr())
		result.Insert(GetUrl());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsExtendedElectronicAddressType_ExtMethodImpl.h


