
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtImplInclude.h


#include "Mp7JrsmediaTimePointType.h"
#include "Mp7JrsMediaRelTimePointType.h"
#include "Mp7JrsMediaRelIncrTimePointType.h"
#include "Mp7JrsfloatVector.h"
#include "Mp7JrsMosaicType_WarpingParam_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsMosaicType_WarpingParam_LocalType::IMp7JrsMosaicType_WarpingParam_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtPropInit.h

}

IMp7JrsMosaicType_WarpingParam_LocalType::~IMp7JrsMosaicType_WarpingParam_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtPropCleanup.h

}

Mp7JrsMosaicType_WarpingParam_LocalType::Mp7JrsMosaicType_WarpingParam_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsMosaicType_WarpingParam_LocalType::~Mp7JrsMosaicType_WarpingParam_LocalType()
{
	Cleanup();
}

void Mp7JrsMosaicType_WarpingParam_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr(); // Pattern
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr(); // Class
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr(); // Class with content 
	m_MotionParam = Mp7JrsfloatVectorPtr(); // Collection


// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtMyPropInit.h

}

void Mp7JrsMosaicType_WarpingParam_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	// Dc1Factory::DeleteObject(m_MotionParam);
}

void Mp7JrsMosaicType_WarpingParam_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use MosaicType_WarpingParam_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in IMosaicType_WarpingParam_LocalType
	const Dc1Ptr< Mp7JrsMosaicType_WarpingParam_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		this->SetMediaTimePoint(Dc1Factory::CloneObject(tmp->GetMediaTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		this->SetMediaRelTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelTimePoint()));
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		this->SetMediaRelIncrTimePoint(Dc1Factory::CloneObject(tmp->GetMediaRelIncrTimePoint()));
		// Dc1Factory::DeleteObject(m_MotionParam);
		this->SetMotionParam(Dc1Factory::CloneObject(tmp->GetMotionParam()));
}

Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


Mp7JrsmediaTimePointPtr Mp7JrsMosaicType_WarpingParam_LocalType::GetMediaTimePoint() const
{
		return m_MediaTimePoint;
}

Mp7JrsMediaRelTimePointPtr Mp7JrsMosaicType_WarpingParam_LocalType::GetMediaRelTimePoint() const
{
		return m_MediaRelTimePoint;
}

Mp7JrsMediaRelIncrTimePointPtr Mp7JrsMosaicType_WarpingParam_LocalType::GetMediaRelIncrTimePoint() const
{
		return m_MediaRelIncrTimePoint;
}

Mp7JrsfloatVectorPtr Mp7JrsMosaicType_WarpingParam_LocalType::GetMotionParam() const
{
		return m_MotionParam;
}

	// TODO Implement proper cleanup for nested choices (e.g. CompositionShotEditinbTemporalDecompositionType_LocalType)
// implementing setter for choice 
/* element */
void Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaTimePoint(const Mp7JrsmediaTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaTimePoint().");
	}
	if (m_MediaTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaTimePoint);
		m_MediaTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaTimePoint) m_MediaTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaTimePoint && m_MediaTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:mediaTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
		{
			m_MediaTimePoint->SetContentName(XMLString::transcode("MediaTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaRelTimePoint(const Mp7JrsMediaRelTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaRelTimePoint().");
	}
	if (m_MediaRelTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
		m_MediaRelTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelTimePoint) m_MediaRelTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelTimePoint && m_MediaRelTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
		{
			m_MediaRelTimePoint->SetContentName(XMLString::transcode("MediaRelTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
	m_MediaRelIncrTimePoint = Mp7JrsMediaRelIncrTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaRelIncrTimePoint(const Mp7JrsMediaRelIncrTimePointPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType::SetMediaRelIncrTimePoint().");
	}
	if (m_MediaRelIncrTimePoint != item)
	{
		// Dc1Factory::DeleteObject(m_MediaRelIncrTimePoint);
		m_MediaRelIncrTimePoint = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MediaRelIncrTimePoint) m_MediaRelIncrTimePoint->SetParent(m_myself.getPointer());
		if (m_MediaRelIncrTimePoint && m_MediaRelIncrTimePoint->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:mpeg:mpeg7:schema:2004:MediaRelIncrTimePointType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_MediaRelIncrTimePoint->UseTypeAttribute = true;
		}
		if(m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
		{
			m_MediaRelIncrTimePoint->SetContentName(XMLString::transcode("MediaRelIncrTimePoint"));
		}
	// Dc1Factory::DeleteObject(m_MediaTimePoint);
	m_MediaTimePoint = Mp7JrsmediaTimePointPtr();
	// Dc1Factory::DeleteObject(m_MediaRelTimePoint);
	m_MediaRelTimePoint = Mp7JrsMediaRelTimePointPtr();
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void Mp7JrsMosaicType_WarpingParam_LocalType::SetMotionParam(const Mp7JrsfloatVectorPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsMosaicType_WarpingParam_LocalType::SetMotionParam().");
	}
	if (m_MotionParam != item)
	{
		// Dc1Factory::DeleteObject(m_MotionParam);
		m_MotionParam = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_MotionParam) m_MotionParam->SetParent(m_myself.getPointer());
		if(m_MotionParam != Mp7JrsfloatVectorPtr())
		{
			m_MotionParam->SetContentName(XMLString::transcode("MotionParam"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: MosaicType_WarpingParam_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsMosaicType_WarpingParam_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsMosaicType_WarpingParam_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsMosaicType_WarpingParam_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsMosaicType_WarpingParam_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:mpeg:mpeg7:schema:2004"), X("MosaicType_WarpingParam_LocalType"));
	// Element serialization:
	if(m_MediaTimePoint != Mp7JrsmediaTimePointPtr())
	{
		// Pattern
		// CON contentname already set in element setter -- not needed here
//		XMLCh * contentname = XMLString::transcode("MediaTimePoint");
//		m_MediaTimePoint->SetContentName(contentname);
		m_MediaTimePoint->UseTypeAttribute = this->UseTypeAttribute;
		m_MediaTimePoint->Serialize(doc, element);
	}
	// Class
	
	if (m_MediaRelTimePoint != Mp7JrsMediaRelTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelTimePoint->Serialize(doc, element, &elem);
	}
	// Class with content		
	
	if (m_MediaRelIncrTimePoint != Mp7JrsMediaRelIncrTimePointPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_MediaRelIncrTimePoint->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:mpeg:mpeg7:schema:2004"), X("MediaRelIncrTimePoint"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_MediaRelIncrTimePoint->Serialize(doc, element, &elem);
	}
	if (m_MotionParam != Mp7JrsfloatVectorPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_MotionParam->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool Mp7JrsMosaicType_WarpingParam_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
		// Deserialize pattern element
	if(Dc1Util::HasNodeName(parent, X("MediaTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//			if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaTimePoint")) == 0))
		{
			Mp7JrsmediaTimePointPtr tmp = CreatemediaTimePointType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMediaTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("MediaRelIncrTimePoint"), X("urn:mpeg:mpeg7:schema:2004")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("MediaRelIncrTimePoint")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateMediaRelIncrTimePointType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetMediaRelIncrTimePoint(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
			continue;	   // For choices
		}
	} while(false);
		// Deserialise Element ValCollectionType
		if((parent != NULL) && (XMLString::compareString(parent->getNodeName()
			, X("MotionParam")) == 0))
		{
			// Deserialize factory type
			Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetMotionParam(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsMosaicType_WarpingParam_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("MediaTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreatemediaTimePointType; // FTT, check this
	}
	this->SetMediaTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelTimePointType; // FTT, check this
	}
	this->SetMediaRelTimePoint(child);
  }
  if (XMLString::compareString(elementname, X("MediaRelIncrTimePoint")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateMediaRelIncrTimePointType; // FTT, check this
	}
	this->SetMediaRelIncrTimePoint(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("MotionParam")) == 0))
  {
	Mp7JrsfloatVectorPtr tmp = CreatefloatVector; // FTT, check this
	this->SetMotionParam(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum Mp7JrsMosaicType_WarpingParam_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetMotionParam() != Dc1NodePtr())
		result.Insert(GetMotionParam());
	if (GetMediaTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaTimePoint());
	if (GetMediaRelTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelTimePoint());
	if (GetMediaRelIncrTimePoint() != Dc1NodePtr())
		result.Insert(GetMediaRelIncrTimePoint());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsMosaicType_WarpingParam_LocalType_ExtMethodImpl.h


