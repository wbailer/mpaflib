
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "Mp7JrsFactoryDefines.h"
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtImplInclude.h


#include "Mp7JrsSceneGraphMaskType_LocalType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"

#include <assert.h>
IMp7JrsSceneGraphMaskType_LocalType::IMp7JrsSceneGraphMaskType_LocalType()
{

// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtPropInit.h

}

IMp7JrsSceneGraphMaskType_LocalType::~IMp7JrsSceneGraphMaskType_LocalType()
{
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtPropCleanup.h

}

Mp7JrsSceneGraphMaskType_LocalType::Mp7JrsSceneGraphMaskType_LocalType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

Mp7JrsSceneGraphMaskType_LocalType::~Mp7JrsSceneGraphMaskType_LocalType()
{
	Cleanup();
}

void Mp7JrsSceneGraphMaskType_LocalType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_SubGraph = XMLString::transcode(""); //  Mandatory String
	m_SubGraphNum = (0); // Value



// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtMyPropInit.h

}

void Mp7JrsSceneGraphMaskType_LocalType::Cleanup()
{
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtMyPropCleanup.h


	XMLString::release(&this->m_SubGraph);
	this->m_SubGraph = NULL;
}

void Mp7JrsSceneGraphMaskType_LocalType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use SceneGraphMaskType_LocalTypePtr, since we
	// might need GetBase(), which isn't defined in ISceneGraphMaskType_LocalType
	const Dc1Ptr< Mp7JrsSceneGraphMaskType_LocalType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
		this->SetSubGraph(XMLString::replicate(tmp->GetSubGraph()));
		this->SetSubGraphNum(tmp->GetSubGraphNum());
}

Dc1NodePtr Mp7JrsSceneGraphMaskType_LocalType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr Mp7JrsSceneGraphMaskType_LocalType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * Mp7JrsSceneGraphMaskType_LocalType::GetSubGraph() const
{
		return m_SubGraph;
}

unsigned Mp7JrsSceneGraphMaskType_LocalType::GetSubGraphNum() const
{
		return m_SubGraphNum;
}

// implementing setter for choice 
/* element */
void Mp7JrsSceneGraphMaskType_LocalType::SetSubGraph(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSceneGraphMaskType_LocalType::SetSubGraph().");
	}
	if (m_SubGraph != item)
	{
		XMLString::release(&m_SubGraph);
		m_SubGraph = item;
	
	m_SubGraphNum_Valid = false;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

/* element */
void Mp7JrsSceneGraphMaskType_LocalType::SetSubGraphNum(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in Mp7JrsSceneGraphMaskType_LocalType::SetSubGraphNum().");
	}
	if (m_SubGraphNum != item)
	{
		m_SubGraphNum = item;
		m_SubGraphNum_Valid = true;
	XMLString::release(&this->m_SubGraph);
	m_SubGraph = NULL; // FTT Shouldn't it be XMLString::release()?
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

// Type: SceneGraphMaskType_LocalType: Only types with an element name can have GetOrCreate()
// Dc1NodeEnum Mp7JrsSceneGraphMaskType_LocalType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
//

XMLCh * Mp7JrsSceneGraphMaskType_LocalType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool Mp7JrsSceneGraphMaskType_LocalType::Parse(const XMLCh * const txt)
{
	return false;
}


void Mp7JrsSceneGraphMaskType_LocalType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:mpeg:mpeg7:schema:2004"));

	// Serialize Choice of Sequence collection item
	element = parent;


	// Element serialization:
	 // String
	if(m_SubGraph != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_SubGraph, X("urn:mpeg:mpeg7:schema:2004"), X("SubGraph"), false);
	if(m_SubGraphNum_Valid) // Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_SubGraphNum);
		Dc1Util::SerializeTextNode(element, doc, tmp, X("urn:mpeg:mpeg7:schema:2004"), X("SubGraphNum"), false);
		XMLString::release(&tmp);
	}

}

bool Mp7JrsSceneGraphMaskType_LocalType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	do // Deserialize choice just one time!
	{
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("SubGraph"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		// FTT memleaking		this->SetSubGraph(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetSubGraph(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	// Deserialize value element
	if(Dc1Util::HasNodeName(parent, X("SubGraphNum"), X("urn:mpeg:mpeg7:schema:2004")))
	{
		XMLCh * tmp = Dc1Util::GetElementText(parent);
		this->SetSubGraphNum(Dc1Convert::TextToUnsignedInt(tmp));
		XMLString::release(&tmp);
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		continue;	   // For choices
	}
	} while(false);
// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr Mp7JrsSceneGraphMaskType_LocalType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  return child;
 
}

Dc1NodeEnum Mp7JrsSceneGraphMaskType_LocalType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file Mp7JrsSceneGraphMaskType_LocalType_ExtMethodImpl.h


