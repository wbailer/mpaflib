
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoredescriptionType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "W3Ctime.h"
#include "EbuCoredescriptionType_description_CollectionType.h"
#include "EbuCoreentityType.h"
#include "EbuCoredescriptionType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "Dc1elementType.h" // Element collection http://purl.org/dc/elements/1.1/:description

#include <assert.h>
IEbuCoredescriptionType::IEbuCoredescriptionType()
{

// no includefile for extension defined 
// file EbuCoredescriptionType_ExtPropInit.h

}

IEbuCoredescriptionType::~IEbuCoredescriptionType()
{
// no includefile for extension defined 
// file EbuCoredescriptionType_ExtPropCleanup.h

}

EbuCoredescriptionType::EbuCoredescriptionType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoredescriptionType::~EbuCoredescriptionType()
{
	Cleanup();
}

void EbuCoredescriptionType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_length = 1; // Value
	m_length_Exist = false;
	m_geographicalScope = NULL; // String
	m_geographicalScope_Exist = false;
	m_geographicalExclusionScope = NULL; // String
	m_geographicalExclusionScope_Exist = false;
	m_startYear = 2015; // Value
	m_startYear_Exist = false;
	m_startDate = W3CdatePtr(); // Pattern
	m_startDate_Exist = false;
	m_startTime = W3CtimePtr(); // Pattern
	m_startTime_Exist = false;
	m_endYear = 2015; // Value
	m_endYear_Exist = false;
	m_endDate = W3CdatePtr(); // Pattern
	m_endDate_Exist = false;
	m_endTime = W3CtimePtr(); // Pattern
	m_endTime_Exist = false;
	m_period = NULL; // String
	m_period_Exist = false;
	m_castFlag = false; // Value
	m_castFlag_Exist = false;
	m_note = NULL; // String
	m_note_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_description = EbuCoredescriptionType_description_CollectionPtr(); // Collection
	m_attributor = EbuCoreentityPtr(); // Class
	m_attributor_Exist = false;


// no includefile for extension defined 
// file EbuCoredescriptionType_ExtMyPropInit.h

}

void EbuCoredescriptionType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoredescriptionType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_geographicalScope); // String
	XMLString::release(&m_geographicalExclusionScope); // String
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	XMLString::release(&m_period); // String
	XMLString::release(&m_note); // String
	// Dc1Factory::DeleteObject(m_description);
	// Dc1Factory::DeleteObject(m_attributor);
}

void EbuCoredescriptionType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use descriptionTypePtr, since we
	// might need GetBase(), which isn't defined in IdescriptionType
	const Dc1Ptr< EbuCoredescriptionType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	if (tmp->Existlength())
	{
		this->Setlength(tmp->Getlength());
	}
	else
	{
		Invalidatelength();
	}
	}
	{
	XMLString::release(&m_geographicalScope); // String
	if (tmp->ExistgeographicalScope())
	{
		this->SetgeographicalScope(XMLString::replicate(tmp->GetgeographicalScope()));
	}
	else
	{
		InvalidategeographicalScope();
	}
	}
	{
	XMLString::release(&m_geographicalExclusionScope); // String
	if (tmp->ExistgeographicalExclusionScope())
	{
		this->SetgeographicalExclusionScope(XMLString::replicate(tmp->GetgeographicalExclusionScope()));
	}
	else
	{
		InvalidategeographicalExclusionScope();
	}
	}
	{
	if (tmp->ExiststartYear())
	{
		this->SetstartYear(tmp->GetstartYear());
	}
	else
	{
		InvalidatestartYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startDate); // Pattern
	if (tmp->ExiststartDate())
	{
		this->SetstartDate(Dc1Factory::CloneObject(tmp->GetstartDate()));
	}
	else
	{
		InvalidatestartDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_startTime); // Pattern
	if (tmp->ExiststartTime())
	{
		this->SetstartTime(Dc1Factory::CloneObject(tmp->GetstartTime()));
	}
	else
	{
		InvalidatestartTime();
	}
	}
	{
	if (tmp->ExistendYear())
	{
		this->SetendYear(tmp->GetendYear());
	}
	else
	{
		InvalidateendYear();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endDate); // Pattern
	if (tmp->ExistendDate())
	{
		this->SetendDate(Dc1Factory::CloneObject(tmp->GetendDate()));
	}
	else
	{
		InvalidateendDate();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_endTime); // Pattern
	if (tmp->ExistendTime())
	{
		this->SetendTime(Dc1Factory::CloneObject(tmp->GetendTime()));
	}
	else
	{
		InvalidateendTime();
	}
	}
	{
	XMLString::release(&m_period); // String
	if (tmp->Existperiod())
	{
		this->Setperiod(XMLString::replicate(tmp->Getperiod()));
	}
	else
	{
		Invalidateperiod();
	}
	}
	{
	if (tmp->ExistcastFlag())
	{
		this->SetcastFlag(tmp->GetcastFlag());
	}
	else
	{
		InvalidatecastFlag();
	}
	}
	{
	XMLString::release(&m_note); // String
	if (tmp->Existnote())
	{
		this->Setnote(XMLString::replicate(tmp->Getnote()));
	}
	else
	{
		Invalidatenote();
	}
	}
		// Dc1Factory::DeleteObject(m_description);
		this->Setdescription(Dc1Factory::CloneObject(tmp->Getdescription()));
	if (tmp->IsValidattributor())
	{
		// Dc1Factory::DeleteObject(m_attributor);
		this->Setattributor(Dc1Factory::CloneObject(tmp->Getattributor()));
	}
	else
	{
		Invalidateattributor();
	}
}

Dc1NodePtr EbuCoredescriptionType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoredescriptionType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoredescriptionType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoredescriptionType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoredescriptionType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoredescriptionType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoredescriptionType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoredescriptionType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoredescriptionType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoredescriptionType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoredescriptionType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoredescriptionType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoredescriptionType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoredescriptionType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoredescriptionType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoredescriptionType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoredescriptionType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
unsigned EbuCoredescriptionType::Getlength() const
{
	return m_length;
}

bool EbuCoredescriptionType::Existlength() const
{
	return m_length_Exist;
}
void EbuCoredescriptionType::Setlength(unsigned item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::Setlength().");
	}
	m_length = item;
	m_length_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::Invalidatelength()
{
	m_length_Exist = false;
}
XMLCh * EbuCoredescriptionType::GetgeographicalScope() const
{
	return m_geographicalScope;
}

bool EbuCoredescriptionType::ExistgeographicalScope() const
{
	return m_geographicalScope_Exist;
}
void EbuCoredescriptionType::SetgeographicalScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetgeographicalScope().");
	}
	m_geographicalScope = item;
	m_geographicalScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidategeographicalScope()
{
	m_geographicalScope_Exist = false;
}
XMLCh * EbuCoredescriptionType::GetgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope;
}

bool EbuCoredescriptionType::ExistgeographicalExclusionScope() const
{
	return m_geographicalExclusionScope_Exist;
}
void EbuCoredescriptionType::SetgeographicalExclusionScope(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetgeographicalExclusionScope().");
	}
	m_geographicalExclusionScope = item;
	m_geographicalExclusionScope_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidategeographicalExclusionScope()
{
	m_geographicalExclusionScope_Exist = false;
}
int EbuCoredescriptionType::GetstartYear() const
{
	return m_startYear;
}

bool EbuCoredescriptionType::ExiststartYear() const
{
	return m_startYear_Exist;
}
void EbuCoredescriptionType::SetstartYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetstartYear().");
	}
	m_startYear = item;
	m_startYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatestartYear()
{
	m_startYear_Exist = false;
}
W3CdatePtr EbuCoredescriptionType::GetstartDate() const
{
	return m_startDate;
}

bool EbuCoredescriptionType::ExiststartDate() const
{
	return m_startDate_Exist;
}
void EbuCoredescriptionType::SetstartDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetstartDate().");
	}
	m_startDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startDate) m_startDate->SetParent(m_myself.getPointer());
	m_startDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatestartDate()
{
	m_startDate_Exist = false;
}
W3CtimePtr EbuCoredescriptionType::GetstartTime() const
{
	return m_startTime;
}

bool EbuCoredescriptionType::ExiststartTime() const
{
	return m_startTime_Exist;
}
void EbuCoredescriptionType::SetstartTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetstartTime().");
	}
	m_startTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_startTime) m_startTime->SetParent(m_myself.getPointer());
	m_startTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatestartTime()
{
	m_startTime_Exist = false;
}
int EbuCoredescriptionType::GetendYear() const
{
	return m_endYear;
}

bool EbuCoredescriptionType::ExistendYear() const
{
	return m_endYear_Exist;
}
void EbuCoredescriptionType::SetendYear(int item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetendYear().");
	}
	m_endYear = item;
	m_endYear_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidateendYear()
{
	m_endYear_Exist = false;
}
W3CdatePtr EbuCoredescriptionType::GetendDate() const
{
	return m_endDate;
}

bool EbuCoredescriptionType::ExistendDate() const
{
	return m_endDate_Exist;
}
void EbuCoredescriptionType::SetendDate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetendDate().");
	}
	m_endDate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endDate) m_endDate->SetParent(m_myself.getPointer());
	m_endDate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidateendDate()
{
	m_endDate_Exist = false;
}
W3CtimePtr EbuCoredescriptionType::GetendTime() const
{
	return m_endTime;
}

bool EbuCoredescriptionType::ExistendTime() const
{
	return m_endTime_Exist;
}
void EbuCoredescriptionType::SetendTime(const W3CtimePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetendTime().");
	}
	m_endTime = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_endTime) m_endTime->SetParent(m_myself.getPointer());
	m_endTime_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidateendTime()
{
	m_endTime_Exist = false;
}
XMLCh * EbuCoredescriptionType::Getperiod() const
{
	return m_period;
}

bool EbuCoredescriptionType::Existperiod() const
{
	return m_period_Exist;
}
void EbuCoredescriptionType::Setperiod(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::Setperiod().");
	}
	m_period = item;
	m_period_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::Invalidateperiod()
{
	m_period_Exist = false;
}
bool EbuCoredescriptionType::GetcastFlag() const
{
	return m_castFlag;
}

bool EbuCoredescriptionType::ExistcastFlag() const
{
	return m_castFlag_Exist;
}
void EbuCoredescriptionType::SetcastFlag(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::SetcastFlag().");
	}
	m_castFlag = item;
	m_castFlag_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::InvalidatecastFlag()
{
	m_castFlag_Exist = false;
}
XMLCh * EbuCoredescriptionType::Getnote() const
{
	return m_note;
}

bool EbuCoredescriptionType::Existnote() const
{
	return m_note_Exist;
}
void EbuCoredescriptionType::Setnote(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::Setnote().");
	}
	m_note = item;
	m_note_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::Invalidatenote()
{
	m_note_Exist = false;
}
EbuCoredescriptionType_description_CollectionPtr EbuCoredescriptionType::Getdescription() const
{
		return m_description;
}

EbuCoreentityPtr EbuCoredescriptionType::Getattributor() const
{
		return m_attributor;
}

// Element is optional
bool EbuCoredescriptionType::IsValidattributor() const
{
	return m_attributor_Exist;
}

void EbuCoredescriptionType::Setdescription(const EbuCoredescriptionType_description_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::Setdescription().");
	}
	if (m_description != item)
	{
		// Dc1Factory::DeleteObject(m_description);
		m_description = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_description) m_description->SetParent(m_myself.getPointer());
		if(m_description != EbuCoredescriptionType_description_CollectionPtr())
		{
			m_description->SetContentName(XMLString::transcode("description"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::Setattributor(const EbuCoreentityPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredescriptionType::Setattributor().");
	}
	if (m_attributor != item || m_attributor_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_attributor);
		m_attributor = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_attributor) m_attributor->SetParent(m_myself.getPointer());
		if (m_attributor && m_attributor->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_attributor->UseTypeAttribute = true;
		}
		m_attributor_Exist = true;
		if(m_attributor != EbuCoreentityPtr())
		{
			m_attributor->SetContentName(XMLString::transcode("attributor"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredescriptionType::Invalidateattributor()
{
	m_attributor_Exist = false;
}

Dc1NodeEnum EbuCoredescriptionType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  17, 17 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("length")) == 0)
	{
		// length is simple attribute positiveInteger
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "unsigned");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalScope")) == 0)
	{
		// geographicalScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("geographicalExclusionScope")) == 0)
	{
		// geographicalExclusionScope is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startYear")) == 0)
	{
		// startYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startDate")) == 0)
	{
		// startDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("startTime")) == 0)
	{
		// startTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endYear")) == 0)
	{
		// endYear is simple attribute gYear
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "int");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endDate")) == 0)
	{
		// endDate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("endTime")) == 0)
	{
		// endTime is simple attribute time
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CtimePtr");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("period")) == 0)
	{
		// period is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("castFlag")) == 0)
	{
		// castFlag is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("note")) == 0)
	{
		// note is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("description")) == 0)
	{
		// http://purl.org/dc/elements/1.1/:description is item of type elementType
		// in element collection descriptionType_description_CollectionType
		
		context->Found = true;
		EbuCoredescriptionType_description_CollectionPtr coll = Getdescription();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatedescriptionType_description_CollectionType; // FTT, check this
				Setdescription(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("http://purl.org/dc/elements/1.1/:elementType")))) != empty)
			{
				// Is type allowed
				Dc1elementPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("attributor")) == 0)
	{
		// attributor is simple element entityType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getattributor()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setattributor(p, client);
					if((p = Getattributor()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for descriptionType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "descriptionType");
	}
	return result;
}

XMLCh * EbuCoredescriptionType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoredescriptionType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoredescriptionType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("descriptionType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_length_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_length);
		element->setAttributeNS(X(""), X("length"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalScope_Exist)
	{
	// String
	if(m_geographicalScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalScope"), m_geographicalScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_geographicalExclusionScope_Exist)
	{
	// String
	if(m_geographicalExclusionScope != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("geographicalExclusionScope"), m_geographicalExclusionScope);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_startYear);
		element->setAttributeNS(X(""), X("startYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startDate_Exist)
	{
	// Pattern
	if (m_startDate != W3CdatePtr())
	{
		XMLCh * tmp = m_startDate->ToText();
		element->setAttributeNS(X(""), X("startDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_startTime_Exist)
	{
	// Pattern
	if (m_startTime != W3CtimePtr())
	{
		XMLCh * tmp = m_startTime->ToText();
		element->setAttributeNS(X(""), X("startTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endYear_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_endYear);
		element->setAttributeNS(X(""), X("endYear"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endDate_Exist)
	{
	// Pattern
	if (m_endDate != W3CdatePtr())
	{
		XMLCh * tmp = m_endDate->ToText();
		element->setAttributeNS(X(""), X("endDate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_endTime_Exist)
	{
	// Pattern
	if (m_endTime != W3CtimePtr())
	{
		XMLCh * tmp = m_endTime->ToText();
		element->setAttributeNS(X(""), X("endTime"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_period_Exist)
	{
	// String
	if(m_period != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("period"), m_period);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_castFlag_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_castFlag);
		element->setAttributeNS(X(""), X("castFlag"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_note_Exist)
	{
	// String
	if(m_note != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("note"), m_note);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_description != EbuCoredescriptionType_description_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_description->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_attributor != EbuCoreentityPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_attributor->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("attributor"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_attributor->Serialize(doc, element, &elem);
	}

}

bool EbuCoredescriptionType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("length")))
	{
		// deserialize value type
		this->Setlength(Dc1Convert::TextToUnsignedInt(parent->getAttribute(X("length"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalScope")))
	{
		// Deserialize string type
		this->SetgeographicalScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("geographicalExclusionScope")))
	{
		// Deserialize string type
		this->SetgeographicalExclusionScope(Dc1Convert::TextToString(parent->getAttribute(X("geographicalExclusionScope"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startYear")))
	{
		// deserialize value type
		this->SetstartYear(Dc1Convert::TextToInt(parent->getAttribute(X("startYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startDate")));
		this->SetstartDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("startTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("startTime")));
		this->SetstartTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endYear")))
	{
		// deserialize value type
		this->SetendYear(Dc1Convert::TextToInt(parent->getAttribute(X("endYear"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endDate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endDate")));
		this->SetendDate(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("endTime")))
	{
		W3CtimePtr tmp = Createtime; // FTT, check this
		tmp->Parse(parent->getAttribute(X("endTime")));
		this->SetendTime(tmp);
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("period")))
	{
		// Deserialize string type
		this->Setperiod(Dc1Convert::TextToString(parent->getAttribute(X("period"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("castFlag")))
	{
		// deserialize value type
		this->SetcastFlag(Dc1Convert::TextToBool(parent->getAttribute(X("castFlag"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("note")))
	{
		// Deserialize string type
		this->Setnote(Dc1Convert::TextToString(parent->getAttribute(X("note"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("description"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("description")) == 0))
		{
			// Deserialize factory type
			EbuCoredescriptionType_description_CollectionPtr tmp = CreatedescriptionType_description_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdescription(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("attributor"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("attributor")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateentityType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setattributor(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoredescriptionType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoredescriptionType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("description")) == 0))
  {
	EbuCoredescriptionType_description_CollectionPtr tmp = CreatedescriptionType_description_CollectionType; // FTT, check this
	this->Setdescription(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("attributor")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateentityType; // FTT, check this
	}
	this->Setattributor(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoredescriptionType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getdescription() != Dc1NodePtr())
		result.Insert(Getdescription());
	if (Getattributor() != Dc1NodePtr())
		result.Insert(Getattributor());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoredescriptionType_ExtMethodImpl.h


