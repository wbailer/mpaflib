
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCorecodecType_ExtImplInclude.h


#include "EbuCoreidentifierType.h"
#include "EbuCorecodecType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCorecodecType::IEbuCorecodecType()
{

// no includefile for extension defined 
// file EbuCorecodecType_ExtPropInit.h

}

IEbuCorecodecType::~IEbuCorecodecType()
{
// no includefile for extension defined 
// file EbuCorecodecType_ExtPropCleanup.h

}

EbuCorecodecType::EbuCorecodecType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCorecodecType::~EbuCorecodecType()
{
	Cleanup();
}

void EbuCorecodecType::Init()
{


	// Init elements (element, union sequence choice all any)
	
	m_codecIdentifier = EbuCoreidentifierPtr(); // Class
	m_codecIdentifier_Exist = false;
	m_name = NULL; // Optional String
	m_name_Exist = false;
	m_vendor = NULL; // Optional String
	m_vendor_Exist = false;
	m_version = NULL; // Optional String
	m_version_Exist = false;
	m_family = NULL; // Optional String
	m_family_Exist = false;


// no includefile for extension defined 
// file EbuCorecodecType_ExtMyPropInit.h

}

void EbuCorecodecType::Cleanup()
{
// no includefile for extension defined 
// file EbuCorecodecType_ExtMyPropCleanup.h


	// Dc1Factory::DeleteObject(m_codecIdentifier);
	XMLString::release(&this->m_name);
	this->m_name = NULL;
	XMLString::release(&this->m_vendor);
	this->m_vendor = NULL;
	XMLString::release(&this->m_version);
	this->m_version = NULL;
	XMLString::release(&this->m_family);
	this->m_family = NULL;
}

void EbuCorecodecType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use codecTypePtr, since we
	// might need GetBase(), which isn't defined in IcodecType
	const Dc1Ptr< EbuCorecodecType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	if (tmp->IsValidcodecIdentifier())
	{
		// Dc1Factory::DeleteObject(m_codecIdentifier);
		this->SetcodecIdentifier(Dc1Factory::CloneObject(tmp->GetcodecIdentifier()));
	}
	else
	{
		InvalidatecodecIdentifier();
	}
	if (tmp->IsValidname())
	{
		this->Setname(XMLString::replicate(tmp->Getname()));
	}
	else
	{
		Invalidatename();
	}
	if (tmp->IsValidvendor())
	{
		this->Setvendor(XMLString::replicate(tmp->Getvendor()));
	}
	else
	{
		Invalidatevendor();
	}
	if (tmp->IsValidversion())
	{
		this->Setversion(XMLString::replicate(tmp->Getversion()));
	}
	else
	{
		Invalidateversion();
	}
	if (tmp->IsValidfamily())
	{
		this->Setfamily(XMLString::replicate(tmp->Getfamily()));
	}
	else
	{
		Invalidatefamily();
	}
}

Dc1NodePtr EbuCorecodecType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCorecodecType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


EbuCoreidentifierPtr EbuCorecodecType::GetcodecIdentifier() const
{
		return m_codecIdentifier;
}

// Element is optional
bool EbuCorecodecType::IsValidcodecIdentifier() const
{
	return m_codecIdentifier_Exist;
}

XMLCh * EbuCorecodecType::Getname() const
{
		return m_name;
}

// Element is optional
bool EbuCorecodecType::IsValidname() const
{
	return m_name_Exist;
}

XMLCh * EbuCorecodecType::Getvendor() const
{
		return m_vendor;
}

// Element is optional
bool EbuCorecodecType::IsValidvendor() const
{
	return m_vendor_Exist;
}

XMLCh * EbuCorecodecType::Getversion() const
{
		return m_version;
}

// Element is optional
bool EbuCorecodecType::IsValidversion() const
{
	return m_version_Exist;
}

XMLCh * EbuCorecodecType::Getfamily() const
{
		return m_family;
}

// Element is optional
bool EbuCorecodecType::IsValidfamily() const
{
	return m_family_Exist;
}

void EbuCorecodecType::SetcodecIdentifier(const EbuCoreidentifierPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecodecType::SetcodecIdentifier().");
	}
	if (m_codecIdentifier != item || m_codecIdentifier_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_codecIdentifier);
		m_codecIdentifier = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_codecIdentifier) m_codecIdentifier->SetParent(m_myself.getPointer());
		if (m_codecIdentifier && m_codecIdentifier->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_codecIdentifier->UseTypeAttribute = true;
		}
		m_codecIdentifier_Exist = true;
		if(m_codecIdentifier != EbuCoreidentifierPtr())
		{
			m_codecIdentifier->SetContentName(XMLString::transcode("codecIdentifier"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecodecType::InvalidatecodecIdentifier()
{
	m_codecIdentifier_Exist = false;
}
void EbuCorecodecType::Setname(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecodecType::Setname().");
	}
	if (m_name != item || m_name_Exist == false)
	{
		XMLString::release(&m_name);
		m_name = item;
		m_name_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecodecType::Invalidatename()
{
	m_name_Exist = false;
}
void EbuCorecodecType::Setvendor(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecodecType::Setvendor().");
	}
	if (m_vendor != item || m_vendor_Exist == false)
	{
		XMLString::release(&m_vendor);
		m_vendor = item;
		m_vendor_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecodecType::Invalidatevendor()
{
	m_vendor_Exist = false;
}
void EbuCorecodecType::Setversion(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecodecType::Setversion().");
	}
	if (m_version != item || m_version_Exist == false)
	{
		XMLString::release(&m_version);
		m_version = item;
		m_version_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecodecType::Invalidateversion()
{
	m_version_Exist = false;
}
void EbuCorecodecType::Setfamily(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCorecodecType::Setfamily().");
	}
	if (m_family != item || m_family_Exist == false)
	{
		XMLString::release(&m_family);
		m_family = item;
		m_family_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCorecodecType::Invalidatefamily()
{
	m_family_Exist = false;
}

Dc1NodeEnum EbuCorecodecType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  0, 0 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Element ------------------------------------------------------------------
	if(XMLString::compareString(context->ElementName, X("codecIdentifier")) == 0)
	{
		// codecIdentifier is simple element identifierType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetcodecIdentifier()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))) != empty)
			{
				// Is type allowed
				EbuCoreidentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetcodecIdentifier(p, client);
					if((p = GetcodecIdentifier()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for codecType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "codecType");
	}
	return result;
}

XMLCh * EbuCorecodecType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCorecodecType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCorecodecType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("codecType"));
	// Element serialization:
	// Class
	
	if (m_codecIdentifier != EbuCoreidentifierPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_codecIdentifier->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("codecIdentifier"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_codecIdentifier->Serialize(doc, element, &elem);
	}
	 // String
	if(m_name != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_name, X("urn:ebu:metadata-schema:ebuCore_2014"), X("name"), true);
	 // String
	if(m_vendor != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_vendor, X("urn:ebu:metadata-schema:ebuCore_2014"), X("vendor"), true);
	 // String
	if(m_version != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_version, X("urn:ebu:metadata-schema:ebuCore_2014"), X("version"), true);
	 // String
	if(m_family != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_family, X("urn:ebu:metadata-schema:ebuCore_2014"), X("family"), true);

}

bool EbuCorecodecType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	parent = Dc1Util::GetNextChild(parent);
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("codecIdentifier"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("codecIdentifier")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateidentifierType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetcodecIdentifier(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("name"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->Setname(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setname(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("vendor"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->Setvendor(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setvendor(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("version"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->Setversion(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setversion(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("family"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->Setfamily(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->Setfamily(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCorecodecType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCorecodecType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  if (XMLString::compareString(elementname, X("codecIdentifier")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateidentifierType; // FTT, check this
	}
	this->SetcodecIdentifier(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCorecodecType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetcodecIdentifier() != Dc1NodePtr())
		result.Insert(GetcodecIdentifier());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCorecodecType_ExtMethodImpl.h


