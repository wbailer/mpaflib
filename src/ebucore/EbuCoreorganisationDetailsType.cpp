
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtImplInclude.h


#include "W3CFactoryDefines.h"
#include "W3Cdate.h"
#include "EbuCoreorganisationDetailsType_organisationName_CollectionType.h"
#include "EbuCoreorganisationDetailsType_organisationCode_CollectionType.h"
#include "EbuCoreorganisationDepartmentType.h"
#include "EbuCoreorganisationDetailsType_details_CollectionType.h"
#include "EbuCoreorganisationDetailsType_relatedInformationLink_CollectionType.h"
#include "EbuCoreorganisationDetailsType_contacts_CollectionType.h"
#include "EbuCoreorganisationDetailsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorecompoundNameType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:organisationName
#include "EbuCoreidentifierType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:organisationCode
#include "EbuCoredetailsType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:details
#include "EbuCoreorganisationDetailsType_relatedInformationLink_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink
#include "EbuCoreentityType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:contacts

#include <assert.h>
IEbuCoreorganisationDetailsType::IEbuCoreorganisationDetailsType()
{

// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtPropInit.h

}

IEbuCoreorganisationDetailsType::~IEbuCoreorganisationDetailsType()
{
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtPropCleanup.h

}

EbuCoreorganisationDetailsType::EbuCoreorganisationDetailsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreorganisationDetailsType::~EbuCoreorganisationDetailsType()
{
	Cleanup();
}

void EbuCoreorganisationDetailsType::Init()
{

	// Init attributes
	m_organisationId = NULL; // String
	m_organisationId_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_linkToLogo = NULL; // String
	m_linkToLogo_Exist = false;
	m_lastUpdate = W3CdatePtr(); // Pattern
	m_lastUpdate_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_organisationName = EbuCoreorganisationDetailsType_organisationName_CollectionPtr(); // Collection
	m_organisationCode = EbuCoreorganisationDetailsType_organisationCode_CollectionPtr(); // Collection
	m_organisationDepartment = EbuCoreorganisationDepartmentPtr(); // Class
	m_organisationDepartment_Exist = false;
	m_details = EbuCoreorganisationDetailsType_details_CollectionPtr(); // Collection
	m_relatedInformationLink = EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr(); // Collection
	m_contacts = EbuCoreorganisationDetailsType_contacts_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMyPropInit.h

}

void EbuCoreorganisationDetailsType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMyPropCleanup.h


	XMLString::release(&m_organisationId); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_linkToLogo); // String
	// Dc1Factory::DeleteObject(m_lastUpdate); // Pattern
	// Dc1Factory::DeleteObject(m_organisationName);
	// Dc1Factory::DeleteObject(m_organisationCode);
	// Dc1Factory::DeleteObject(m_organisationDepartment);
	// Dc1Factory::DeleteObject(m_details);
	// Dc1Factory::DeleteObject(m_relatedInformationLink);
	// Dc1Factory::DeleteObject(m_contacts);
}

void EbuCoreorganisationDetailsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use organisationDetailsTypePtr, since we
	// might need GetBase(), which isn't defined in IorganisationDetailsType
	const Dc1Ptr< EbuCoreorganisationDetailsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_organisationId); // String
	if (tmp->ExistorganisationId())
	{
		this->SetorganisationId(XMLString::replicate(tmp->GetorganisationId()));
	}
	else
	{
		InvalidateorganisationId();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_linkToLogo); // String
	if (tmp->ExistlinkToLogo())
	{
		this->SetlinkToLogo(XMLString::replicate(tmp->GetlinkToLogo()));
	}
	else
	{
		InvalidatelinkToLogo();
	}
	}
	{
	// Dc1Factory::DeleteObject(m_lastUpdate); // Pattern
	if (tmp->ExistlastUpdate())
	{
		this->SetlastUpdate(Dc1Factory::CloneObject(tmp->GetlastUpdate()));
	}
	else
	{
		InvalidatelastUpdate();
	}
	}
		// Dc1Factory::DeleteObject(m_organisationName);
		this->SetorganisationName(Dc1Factory::CloneObject(tmp->GetorganisationName()));
		// Dc1Factory::DeleteObject(m_organisationCode);
		this->SetorganisationCode(Dc1Factory::CloneObject(tmp->GetorganisationCode()));
	if (tmp->IsValidorganisationDepartment())
	{
		// Dc1Factory::DeleteObject(m_organisationDepartment);
		this->SetorganisationDepartment(Dc1Factory::CloneObject(tmp->GetorganisationDepartment()));
	}
	else
	{
		InvalidateorganisationDepartment();
	}
		// Dc1Factory::DeleteObject(m_details);
		this->Setdetails(Dc1Factory::CloneObject(tmp->Getdetails()));
		// Dc1Factory::DeleteObject(m_relatedInformationLink);
		this->SetrelatedInformationLink(Dc1Factory::CloneObject(tmp->GetrelatedInformationLink()));
		// Dc1Factory::DeleteObject(m_contacts);
		this->Setcontacts(Dc1Factory::CloneObject(tmp->Getcontacts()));
}

Dc1NodePtr EbuCoreorganisationDetailsType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreorganisationDetailsType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreorganisationDetailsType::GetorganisationId() const
{
	return m_organisationId;
}

bool EbuCoreorganisationDetailsType::ExistorganisationId() const
{
	return m_organisationId_Exist;
}
void EbuCoreorganisationDetailsType::SetorganisationId(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetorganisationId().");
	}
	m_organisationId = item;
	m_organisationId_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidateorganisationId()
{
	m_organisationId_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreorganisationDetailsType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreorganisationDetailsType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreorganisationDetailsType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreorganisationDetailsType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreorganisationDetailsType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreorganisationDetailsType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreorganisationDetailsType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreorganisationDetailsType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreorganisationDetailsType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreorganisationDetailsType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoreorganisationDetailsType::GetlinkToLogo() const
{
	return m_linkToLogo;
}

bool EbuCoreorganisationDetailsType::ExistlinkToLogo() const
{
	return m_linkToLogo_Exist;
}
void EbuCoreorganisationDetailsType::SetlinkToLogo(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetlinkToLogo().");
	}
	m_linkToLogo = item;
	m_linkToLogo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatelinkToLogo()
{
	m_linkToLogo_Exist = false;
}
W3CdatePtr EbuCoreorganisationDetailsType::GetlastUpdate() const
{
	return m_lastUpdate;
}

bool EbuCoreorganisationDetailsType::ExistlastUpdate() const
{
	return m_lastUpdate_Exist;
}
void EbuCoreorganisationDetailsType::SetlastUpdate(const W3CdatePtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetlastUpdate().");
	}
	m_lastUpdate = item;
	// baw 27 04 2004: make sure that node on which parent is set is not null
	if (m_lastUpdate) m_lastUpdate->SetParent(m_myself.getPointer());
	m_lastUpdate_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidatelastUpdate()
{
	m_lastUpdate_Exist = false;
}
EbuCoreorganisationDetailsType_organisationName_CollectionPtr EbuCoreorganisationDetailsType::GetorganisationName() const
{
		return m_organisationName;
}

EbuCoreorganisationDetailsType_organisationCode_CollectionPtr EbuCoreorganisationDetailsType::GetorganisationCode() const
{
		return m_organisationCode;
}

EbuCoreorganisationDepartmentPtr EbuCoreorganisationDetailsType::GetorganisationDepartment() const
{
		return m_organisationDepartment;
}

// Element is optional
bool EbuCoreorganisationDetailsType::IsValidorganisationDepartment() const
{
	return m_organisationDepartment_Exist;
}

EbuCoreorganisationDetailsType_details_CollectionPtr EbuCoreorganisationDetailsType::Getdetails() const
{
		return m_details;
}

EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr EbuCoreorganisationDetailsType::GetrelatedInformationLink() const
{
		return m_relatedInformationLink;
}

EbuCoreorganisationDetailsType_contacts_CollectionPtr EbuCoreorganisationDetailsType::Getcontacts() const
{
		return m_contacts;
}

void EbuCoreorganisationDetailsType::SetorganisationName(const EbuCoreorganisationDetailsType_organisationName_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetorganisationName().");
	}
	if (m_organisationName != item)
	{
		// Dc1Factory::DeleteObject(m_organisationName);
		m_organisationName = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_organisationName) m_organisationName->SetParent(m_myself.getPointer());
		if(m_organisationName != EbuCoreorganisationDetailsType_organisationName_CollectionPtr())
		{
			m_organisationName->SetContentName(XMLString::transcode("organisationName"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::SetorganisationCode(const EbuCoreorganisationDetailsType_organisationCode_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetorganisationCode().");
	}
	if (m_organisationCode != item)
	{
		// Dc1Factory::DeleteObject(m_organisationCode);
		m_organisationCode = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_organisationCode) m_organisationCode->SetParent(m_myself.getPointer());
		if(m_organisationCode != EbuCoreorganisationDetailsType_organisationCode_CollectionPtr())
		{
			m_organisationCode->SetContentName(XMLString::transcode("organisationCode"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::SetorganisationDepartment(const EbuCoreorganisationDepartmentPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetorganisationDepartment().");
	}
	if (m_organisationDepartment != item || m_organisationDepartment_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_organisationDepartment);
		m_organisationDepartment = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_organisationDepartment) m_organisationDepartment->SetParent(m_myself.getPointer());
		if (m_organisationDepartment && m_organisationDepartment->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDepartmentType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_organisationDepartment->UseTypeAttribute = true;
		}
		m_organisationDepartment_Exist = true;
		if(m_organisationDepartment != EbuCoreorganisationDepartmentPtr())
		{
			m_organisationDepartment->SetContentName(XMLString::transcode("organisationDepartment"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::InvalidateorganisationDepartment()
{
	m_organisationDepartment_Exist = false;
}
void EbuCoreorganisationDetailsType::Setdetails(const EbuCoreorganisationDetailsType_details_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::Setdetails().");
	}
	if (m_details != item)
	{
		// Dc1Factory::DeleteObject(m_details);
		m_details = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_details) m_details->SetParent(m_myself.getPointer());
		if(m_details != EbuCoreorganisationDetailsType_details_CollectionPtr())
		{
			m_details->SetContentName(XMLString::transcode("details"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::SetrelatedInformationLink(const EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::SetrelatedInformationLink().");
	}
	if (m_relatedInformationLink != item)
	{
		// Dc1Factory::DeleteObject(m_relatedInformationLink);
		m_relatedInformationLink = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_relatedInformationLink) m_relatedInformationLink->SetParent(m_myself.getPointer());
		if(m_relatedInformationLink != EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr())
		{
			m_relatedInformationLink->SetContentName(XMLString::transcode("relatedInformationLink"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreorganisationDetailsType::Setcontacts(const EbuCoreorganisationDetailsType_contacts_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreorganisationDetailsType::Setcontacts().");
	}
	if (m_contacts != item)
	{
		// Dc1Factory::DeleteObject(m_contacts);
		m_contacts = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_contacts) m_contacts->SetParent(m_myself.getPointer());
		if(m_contacts != EbuCoreorganisationDetailsType_contacts_CollectionPtr())
		{
			m_contacts->SetContentName(XMLString::transcode("contacts"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreorganisationDetailsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  8, 8 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("organisationId")) == 0)
	{
		// organisationId is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("linkToLogo")) == 0)
	{
		// linkToLogo is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("lastUpdate")) == 0)
	{
		// lastUpdate is simple attribute date
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "W3CdatePtr");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("organisationName")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:organisationName is item of type compoundNameType
		// in element collection organisationDetailsType_organisationName_CollectionType
		
		context->Found = true;
		EbuCoreorganisationDetailsType_organisationName_CollectionPtr coll = GetorganisationName();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateorganisationDetailsType_organisationName_CollectionType; // FTT, check this
				SetorganisationName(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:compoundNameType")))) != empty)
			{
				// Is type allowed
				EbuCorecompoundNamePtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("organisationCode")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:organisationCode is item of type identifierType
		// in element collection organisationDetailsType_organisationCode_CollectionType
		
		context->Found = true;
		EbuCoreorganisationDetailsType_organisationCode_CollectionPtr coll = GetorganisationCode();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateorganisationDetailsType_organisationCode_CollectionType; // FTT, check this
				SetorganisationCode(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:identifierType")))) != empty)
			{
				// Is type allowed
				EbuCoreidentifierPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("organisationDepartment")) == 0)
	{
		// organisationDepartment is simple element organisationDepartmentType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = GetorganisationDepartment()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDepartmentType")))) != empty)
			{
				// Is type allowed
				EbuCoreorganisationDepartmentPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					SetorganisationDepartment(p, client);
					if((p = GetorganisationDepartment()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("details")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:details is item of type detailsType
		// in element collection organisationDetailsType_details_CollectionType
		
		context->Found = true;
		EbuCoreorganisationDetailsType_details_CollectionPtr coll = Getdetails();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateorganisationDetailsType_details_CollectionType; // FTT, check this
				Setdetails(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:detailsType")))) != empty)
			{
				// Is type allowed
				EbuCoredetailsPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("relatedInformationLink")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:relatedInformationLink is item of type organisationDetailsType_relatedInformationLink_LocalType
		// in element collection organisationDetailsType_relatedInformationLink_CollectionType
		
		context->Found = true;
		EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr coll = GetrelatedInformationLink();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateorganisationDetailsType_relatedInformationLink_CollectionType; // FTT, check this
				SetrelatedInformationLink(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:organisationDetailsType_relatedInformationLink_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCoreorganisationDetailsType_relatedInformationLink_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("contacts")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:contacts is item of type entityType
		// in element collection organisationDetailsType_contacts_CollectionType
		
		context->Found = true;
		EbuCoreorganisationDetailsType_contacts_CollectionPtr coll = Getcontacts();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateorganisationDetailsType_contacts_CollectionType; // FTT, check this
				Setcontacts(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:entityType")))) != empty)
			{
				// Is type allowed
				EbuCoreentityPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for organisationDetailsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "organisationDetailsType");
	}
	return result;
}

XMLCh * EbuCoreorganisationDetailsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreorganisationDetailsType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreorganisationDetailsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("organisationDetailsType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_organisationId_Exist)
	{
	// String
	if(m_organisationId != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("organisationId"), m_organisationId);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_linkToLogo_Exist)
	{
	// String
	if(m_linkToLogo != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("linkToLogo"), m_linkToLogo);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_lastUpdate_Exist)
	{
	// Pattern
	if (m_lastUpdate != W3CdatePtr())
	{
		XMLCh * tmp = m_lastUpdate->ToText();
		element->setAttributeNS(X(""), X("lastUpdate"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_organisationName != EbuCoreorganisationDetailsType_organisationName_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_organisationName->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_organisationCode != EbuCoreorganisationDetailsType_organisationCode_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_organisationCode->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	// Class
	
	if (m_organisationDepartment != EbuCoreorganisationDepartmentPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_organisationDepartment->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("organisationDepartment"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_organisationDepartment->Serialize(doc, element, &elem);
	}
	if (m_details != EbuCoreorganisationDetailsType_details_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_details->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_relatedInformationLink != EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_relatedInformationLink->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_contacts != EbuCoreorganisationDetailsType_contacts_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_contacts->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreorganisationDetailsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("organisationId")))
	{
		// Deserialize string type
		this->SetorganisationId(Dc1Convert::TextToString(parent->getAttribute(X("organisationId"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("linkToLogo")))
	{
		// Deserialize string type
		this->SetlinkToLogo(Dc1Convert::TextToString(parent->getAttribute(X("linkToLogo"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("lastUpdate")))
	{
		W3CdatePtr tmp = Createdate; // FTT, check this
		tmp->Parse(parent->getAttribute(X("lastUpdate")));
		this->SetlastUpdate(tmp);
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("organisationName"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("organisationName")) == 0))
		{
			// Deserialize factory type
			EbuCoreorganisationDetailsType_organisationName_CollectionPtr tmp = CreateorganisationDetailsType_organisationName_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetorganisationName(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("organisationCode"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("organisationCode")) == 0))
		{
			// Deserialize factory type
			EbuCoreorganisationDetailsType_organisationCode_CollectionPtr tmp = CreateorganisationDetailsType_organisationCode_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetorganisationCode(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("organisationDepartment"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("organisationDepartment")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateorganisationDepartmentType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->SetorganisationDepartment(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("details"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("details")) == 0))
		{
			// Deserialize factory type
			EbuCoreorganisationDetailsType_details_CollectionPtr tmp = CreateorganisationDetailsType_details_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setdetails(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("relatedInformationLink"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("relatedInformationLink")) == 0))
		{
			// Deserialize factory type
			EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr tmp = CreateorganisationDetailsType_relatedInformationLink_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetrelatedInformationLink(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("contacts"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("contacts")) == 0))
		{
			// Deserialize factory type
			EbuCoreorganisationDetailsType_contacts_CollectionPtr tmp = CreateorganisationDetailsType_contacts_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setcontacts(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreorganisationDetailsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("organisationName")) == 0))
  {
	EbuCoreorganisationDetailsType_organisationName_CollectionPtr tmp = CreateorganisationDetailsType_organisationName_CollectionType; // FTT, check this
	this->SetorganisationName(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("organisationCode")) == 0))
  {
	EbuCoreorganisationDetailsType_organisationCode_CollectionPtr tmp = CreateorganisationDetailsType_organisationCode_CollectionType; // FTT, check this
	this->SetorganisationCode(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("organisationDepartment")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateorganisationDepartmentType; // FTT, check this
	}
	this->SetorganisationDepartment(child);
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("details")) == 0))
  {
	EbuCoreorganisationDetailsType_details_CollectionPtr tmp = CreateorganisationDetailsType_details_CollectionType; // FTT, check this
	this->Setdetails(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("relatedInformationLink")) == 0))
  {
	EbuCoreorganisationDetailsType_relatedInformationLink_CollectionPtr tmp = CreateorganisationDetailsType_relatedInformationLink_CollectionType; // FTT, check this
	this->SetrelatedInformationLink(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("contacts")) == 0))
  {
	EbuCoreorganisationDetailsType_contacts_CollectionPtr tmp = CreateorganisationDetailsType_contacts_CollectionType; // FTT, check this
	this->Setcontacts(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreorganisationDetailsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetorganisationName() != Dc1NodePtr())
		result.Insert(GetorganisationName());
	if (GetorganisationCode() != Dc1NodePtr())
		result.Insert(GetorganisationCode());
	if (GetorganisationDepartment() != Dc1NodePtr())
		result.Insert(GetorganisationDepartment());
	if (Getdetails() != Dc1NodePtr())
		result.Insert(Getdetails());
	if (GetrelatedInformationLink() != Dc1NodePtr())
		result.Insert(GetrelatedInformationLink());
	if (Getcontacts() != Dc1NodePtr())
		result.Insert(Getcontacts());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreorganisationDetailsType_ExtMethodImpl.h


