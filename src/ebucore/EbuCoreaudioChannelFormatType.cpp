
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtImplInclude.h


#include "EbuCoreaudioChannelFormatType_frequency_CollectionType.h"
#include "EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionType.h"
#include "EbuCoreaudioChannelFormatType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCorefrequency_LocalType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:frequency
#include "EbuCoreaudioBlockFormatType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat

#include <assert.h>
IEbuCoreaudioChannelFormatType::IEbuCoreaudioChannelFormatType()
{

// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtPropInit.h

}

IEbuCoreaudioChannelFormatType::~IEbuCoreaudioChannelFormatType()
{
// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtPropCleanup.h

}

EbuCoreaudioChannelFormatType::EbuCoreaudioChannelFormatType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoreaudioChannelFormatType::~EbuCoreaudioChannelFormatType()
{
	Cleanup();
}

void EbuCoreaudioChannelFormatType::Init()
{

	// Init attributes
	m_audioChannelFormatName = NULL; // String
	m_audioChannelFormatName_Exist = false;
	m_audioChannelFormatID = NULL; // String
	m_audioChannelFormatID_Exist = false;
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_frequency = EbuCoreaudioChannelFormatType_frequency_CollectionPtr(); // Collection
	m_audioBlockFormat = EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtMyPropInit.h

}

void EbuCoreaudioChannelFormatType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtMyPropCleanup.h


	XMLString::release(&m_audioChannelFormatName); // String
	XMLString::release(&m_audioChannelFormatID); // String
	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_frequency);
	// Dc1Factory::DeleteObject(m_audioBlockFormat);
}

void EbuCoreaudioChannelFormatType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use audioChannelFormatTypePtr, since we
	// might need GetBase(), which isn't defined in IaudioChannelFormatType
	const Dc1Ptr< EbuCoreaudioChannelFormatType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_audioChannelFormatName); // String
	if (tmp->ExistaudioChannelFormatName())
	{
		this->SetaudioChannelFormatName(XMLString::replicate(tmp->GetaudioChannelFormatName()));
	}
	else
	{
		InvalidateaudioChannelFormatName();
	}
	}
	{
	XMLString::release(&m_audioChannelFormatID); // String
	if (tmp->ExistaudioChannelFormatID())
	{
		this->SetaudioChannelFormatID(XMLString::replicate(tmp->GetaudioChannelFormatID()));
	}
	else
	{
		InvalidateaudioChannelFormatID();
	}
	}
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
		// Dc1Factory::DeleteObject(m_frequency);
		this->Setfrequency(Dc1Factory::CloneObject(tmp->Getfrequency()));
		// Dc1Factory::DeleteObject(m_audioBlockFormat);
		this->SetaudioBlockFormat(Dc1Factory::CloneObject(tmp->GetaudioBlockFormat()));
}

Dc1NodePtr EbuCoreaudioChannelFormatType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoreaudioChannelFormatType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoreaudioChannelFormatType::GetaudioChannelFormatName() const
{
	return m_audioChannelFormatName;
}

bool EbuCoreaudioChannelFormatType::ExistaudioChannelFormatName() const
{
	return m_audioChannelFormatName_Exist;
}
void EbuCoreaudioChannelFormatType::SetaudioChannelFormatName(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SetaudioChannelFormatName().");
	}
	m_audioChannelFormatName = item;
	m_audioChannelFormatName_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidateaudioChannelFormatName()
{
	m_audioChannelFormatName_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GetaudioChannelFormatID() const
{
	return m_audioChannelFormatID;
}

bool EbuCoreaudioChannelFormatType::ExistaudioChannelFormatID() const
{
	return m_audioChannelFormatID_Exist;
}
void EbuCoreaudioChannelFormatType::SetaudioChannelFormatID(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SetaudioChannelFormatID().");
	}
	m_audioChannelFormatID = item;
	m_audioChannelFormatID_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidateaudioChannelFormatID()
{
	m_audioChannelFormatID_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoreaudioChannelFormatType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoreaudioChannelFormatType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoreaudioChannelFormatType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoreaudioChannelFormatType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoreaudioChannelFormatType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoreaudioChannelFormatType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoreaudioChannelFormatType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoreaudioChannelFormatType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoreaudioChannelFormatType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoreaudioChannelFormatType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoreaudioChannelFormatType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
EbuCoreaudioChannelFormatType_frequency_CollectionPtr EbuCoreaudioChannelFormatType::Getfrequency() const
{
		return m_frequency;
}

EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr EbuCoreaudioChannelFormatType::GetaudioBlockFormat() const
{
		return m_audioBlockFormat;
}

void EbuCoreaudioChannelFormatType::Setfrequency(const EbuCoreaudioChannelFormatType_frequency_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::Setfrequency().");
	}
	if (m_frequency != item)
	{
		// Dc1Factory::DeleteObject(m_frequency);
		m_frequency = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_frequency) m_frequency->SetParent(m_myself.getPointer());
		if(m_frequency != EbuCoreaudioChannelFormatType_frequency_CollectionPtr())
		{
			m_frequency->SetContentName(XMLString::transcode("frequency"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoreaudioChannelFormatType::SetaudioBlockFormat(const EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoreaudioChannelFormatType::SetaudioBlockFormat().");
	}
	if (m_audioBlockFormat != item)
	{
		// Dc1Factory::DeleteObject(m_audioBlockFormat);
		m_audioBlockFormat = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_audioBlockFormat) m_audioBlockFormat->SetParent(m_myself.getPointer());
		if(m_audioBlockFormat != EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr())
		{
			m_audioBlockFormat->SetContentName(XMLString::transcode("audioBlockFormat"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoreaudioChannelFormatType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  7, 7 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioChannelFormatName")) == 0)
	{
		// audioChannelFormatName is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("audioChannelFormatID")) == 0)
	{
		// audioChannelFormatID is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("frequency")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:frequency is item of type frequency_LocalType
		// in element collection audioChannelFormatType_frequency_CollectionType
		
		context->Found = true;
		EbuCoreaudioChannelFormatType_frequency_CollectionPtr coll = Getfrequency();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioChannelFormatType_frequency_CollectionType; // FTT, check this
				Setfrequency(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), 2))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:frequency_LocalType")))) != empty)
			{
				// Is type allowed
				EbuCorefrequency_LocalPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("audioBlockFormat")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormat is item of type audioBlockFormatType
		// in element collection audioChannelFormatType_audioBlockFormat_CollectionType
		
		context->Found = true;
		EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr coll = GetaudioBlockFormat();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreateaudioChannelFormatType_audioBlockFormat_CollectionType; // FTT, check this
				SetaudioBlockFormat(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:audioBlockFormatType")))) != empty)
			{
				// Is type allowed
				EbuCoreaudioBlockFormatPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for audioChannelFormatType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "audioChannelFormatType");
	}
	return result;
}

XMLCh * EbuCoreaudioChannelFormatType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoreaudioChannelFormatType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoreaudioChannelFormatType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("audioChannelFormatType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioChannelFormatName_Exist)
	{
	// String
	if(m_audioChannelFormatName != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioChannelFormatName"), m_audioChannelFormatName);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_audioChannelFormatID_Exist)
	{
	// String
	if(m_audioChannelFormatID != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("audioChannelFormatID"), m_audioChannelFormatID);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_frequency != EbuCoreaudioChannelFormatType_frequency_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_frequency->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_audioBlockFormat != EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_audioBlockFormat->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoreaudioChannelFormatType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioChannelFormatName")))
	{
		// Deserialize string type
		this->SetaudioChannelFormatName(Dc1Convert::TextToString(parent->getAttribute(X("audioChannelFormatName"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("audioChannelFormatID")))
	{
		// Deserialize string type
		this->SetaudioChannelFormatID(Dc1Convert::TextToString(parent->getAttribute(X("audioChannelFormatID"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("frequency"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("frequency")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioChannelFormatType_frequency_CollectionPtr tmp = CreateaudioChannelFormatType_frequency_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->Setfrequency(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("audioBlockFormat"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("audioBlockFormat")) == 0))
		{
			// Deserialize factory type
			EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr tmp = CreateaudioChannelFormatType_audioBlockFormat_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetaudioBlockFormat(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoreaudioChannelFormatType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("frequency")) == 0))
  {
	EbuCoreaudioChannelFormatType_frequency_CollectionPtr tmp = CreateaudioChannelFormatType_frequency_CollectionType; // FTT, check this
	this->Setfrequency(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("audioBlockFormat")) == 0))
  {
	EbuCoreaudioChannelFormatType_audioBlockFormat_CollectionPtr tmp = CreateaudioChannelFormatType_audioBlockFormat_CollectionType; // FTT, check this
	this->SetaudioBlockFormat(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoreaudioChannelFormatType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (Getfrequency() != Dc1NodePtr())
		result.Insert(Getfrequency());
	if (GetaudioBlockFormat() != Dc1NodePtr())
		result.Insert(GetaudioBlockFormat());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoreaudioChannelFormatType_ExtMethodImpl.h


