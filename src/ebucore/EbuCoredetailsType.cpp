
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoredetailsType_ExtImplInclude.h


#include "EbuCoredetailsType_emailAddress_CollectionType.h"
#include "EbuCoreaddressType.h"
#include "EbuCoredetailsType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff

#include <assert.h>
IEbuCoredetailsType::IEbuCoredetailsType()
{

// no includefile for extension defined 
// file EbuCoredetailsType_ExtPropInit.h

}

IEbuCoredetailsType::~IEbuCoredetailsType()
{
// no includefile for extension defined 
// file EbuCoredetailsType_ExtPropCleanup.h

}

EbuCoredetailsType::EbuCoredetailsType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoredetailsType::~EbuCoredetailsType()
{
	Cleanup();
}

void EbuCoredetailsType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_emailAddress = EbuCoredetailsType_emailAddress_CollectionPtr(); // Collection
	m_webAddress = NULL; // Optional String
	m_webAddress_Exist = false;
	m_address = EbuCoreaddressPtr(); // Class
	m_address_Exist = false;
	m_telephoneNumber = NULL; // Optional String
	m_telephoneNumber_Exist = false;
	m_mobileTelephoneNumber = NULL; // Optional String
	m_mobileTelephoneNumber_Exist = false;


// no includefile for extension defined 
// file EbuCoredetailsType_ExtMyPropInit.h

}

void EbuCoredetailsType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoredetailsType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	// Dc1Factory::DeleteObject(m_emailAddress);
	XMLString::release(&this->m_webAddress);
	this->m_webAddress = NULL;
	// Dc1Factory::DeleteObject(m_address);
	XMLString::release(&this->m_telephoneNumber);
	this->m_telephoneNumber = NULL;
	XMLString::release(&this->m_mobileTelephoneNumber);
	this->m_mobileTelephoneNumber = NULL;
}

void EbuCoredetailsType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use detailsTypePtr, since we
	// might need GetBase(), which isn't defined in IdetailsType
	const Dc1Ptr< EbuCoredetailsType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
		// Dc1Factory::DeleteObject(m_emailAddress);
		this->SetemailAddress(Dc1Factory::CloneObject(tmp->GetemailAddress()));
	if (tmp->IsValidwebAddress())
	{
		this->SetwebAddress(XMLString::replicate(tmp->GetwebAddress()));
	}
	else
	{
		InvalidatewebAddress();
	}
	if (tmp->IsValidaddress())
	{
		// Dc1Factory::DeleteObject(m_address);
		this->Setaddress(Dc1Factory::CloneObject(tmp->Getaddress()));
	}
	else
	{
		Invalidateaddress();
	}
	if (tmp->IsValidtelephoneNumber())
	{
		this->SettelephoneNumber(XMLString::replicate(tmp->GettelephoneNumber()));
	}
	else
	{
		InvalidatetelephoneNumber();
	}
	if (tmp->IsValidmobileTelephoneNumber())
	{
		this->SetmobileTelephoneNumber(XMLString::replicate(tmp->GetmobileTelephoneNumber()));
	}
	else
	{
		InvalidatemobileTelephoneNumber();
	}
}

Dc1NodePtr EbuCoredetailsType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoredetailsType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoredetailsType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoredetailsType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoredetailsType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoredetailsType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoredetailsType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoredetailsType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoredetailsType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoredetailsType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoredetailsType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoredetailsType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoredetailsType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoredetailsType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoredetailsType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoredetailsType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoredetailsType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
EbuCoredetailsType_emailAddress_CollectionPtr EbuCoredetailsType::GetemailAddress() const
{
		return m_emailAddress;
}

XMLCh * EbuCoredetailsType::GetwebAddress() const
{
		return m_webAddress;
}

// Element is optional
bool EbuCoredetailsType::IsValidwebAddress() const
{
	return m_webAddress_Exist;
}

EbuCoreaddressPtr EbuCoredetailsType::Getaddress() const
{
		return m_address;
}

// Element is optional
bool EbuCoredetailsType::IsValidaddress() const
{
	return m_address_Exist;
}

XMLCh * EbuCoredetailsType::GettelephoneNumber() const
{
		return m_telephoneNumber;
}

// Element is optional
bool EbuCoredetailsType::IsValidtelephoneNumber() const
{
	return m_telephoneNumber_Exist;
}

XMLCh * EbuCoredetailsType::GetmobileTelephoneNumber() const
{
		return m_mobileTelephoneNumber;
}

// Element is optional
bool EbuCoredetailsType::IsValidmobileTelephoneNumber() const
{
	return m_mobileTelephoneNumber_Exist;
}

void EbuCoredetailsType::SetemailAddress(const EbuCoredetailsType_emailAddress_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SetemailAddress().");
	}
	if (m_emailAddress != item)
	{
		// Dc1Factory::DeleteObject(m_emailAddress);
		m_emailAddress = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_emailAddress) m_emailAddress->SetParent(m_myself.getPointer());
		if(m_emailAddress != EbuCoredetailsType_emailAddress_CollectionPtr())
		{
			m_emailAddress->SetContentName(XMLString::transcode("emailAddress"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::SetwebAddress(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SetwebAddress().");
	}
	if (m_webAddress != item || m_webAddress_Exist == false)
	{
		XMLString::release(&m_webAddress);
		m_webAddress = item;
		m_webAddress_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatewebAddress()
{
	m_webAddress_Exist = false;
}
void EbuCoredetailsType::Setaddress(const EbuCoreaddressPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::Setaddress().");
	}
	if (m_address != item || m_address_Exist == false)
	{
		// Dc1Factory::DeleteObject(m_address);
		m_address = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_address) m_address->SetParent(m_myself.getPointer());
		if (m_address && m_address->GetClassId() 
	!= Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType"))) {
		  // Element is concrete, so set UseTypeAttribute to true only if item is derived!
		  m_address->UseTypeAttribute = true;
		}
		m_address_Exist = true;
		if(m_address != EbuCoreaddressPtr())
		{
			m_address->SetContentName(XMLString::transcode("address"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::Invalidateaddress()
{
	m_address_Exist = false;
}
void EbuCoredetailsType::SettelephoneNumber(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SettelephoneNumber().");
	}
	if (m_telephoneNumber != item || m_telephoneNumber_Exist == false)
	{
		XMLString::release(&m_telephoneNumber);
		m_telephoneNumber = item;
		m_telephoneNumber_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatetelephoneNumber()
{
	m_telephoneNumber_Exist = false;
}
void EbuCoredetailsType::SetmobileTelephoneNumber(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoredetailsType::SetmobileTelephoneNumber().");
	}
	if (m_mobileTelephoneNumber != item || m_mobileTelephoneNumber_Exist == false)
	{
		XMLString::release(&m_mobileTelephoneNumber);
		m_mobileTelephoneNumber = item;
		m_mobileTelephoneNumber_Exist = true;
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoredetailsType::InvalidatemobileTelephoneNumber()
{
	m_mobileTelephoneNumber_Exist = false;
}

Dc1NodeEnum EbuCoredetailsType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  5, 5 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("emailAddress")) == 0)
	{
		// Some primitive nodes types cannot be handled
		Dc1XPathParseContext::ErrorUnsupportedType(context, "XMLCh *");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("address")) == 0)
	{
		// address is simple element addressType
		context->Found = true;
		Dc1XPathParseContext::PreProcessSimpleElement(context);
		
		Dc1NodePtr p;
		if((p = Getaddress()) != empty)
		{
			if(Dc1XPathParseContext::HandleSimpleElement(context, p))
			{
				result.Insert(p);
			}
		}
		else if(context->Create)
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:addressType")))) != empty)
			{
				// Is type allowed
				EbuCoreaddressPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Unkown" type is allowed
				{
					Setaddress(p, client);
					if((p = Getaddress()) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for detailsType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "detailsType");
	}
	return result;
}

XMLCh * EbuCoredetailsType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoredetailsType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoredetailsType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("detailsType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_emailAddress != EbuCoredetailsType_emailAddress_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_emailAddress->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	 // String
	if(m_webAddress != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_webAddress, X("urn:ebu:metadata-schema:ebuCore_2014"), X("webAddress"), true);
	// Class
	
	if (m_address != EbuCoreaddressPtr())
	{
		// Class
		// Create child node to serialize into, only if current type is not unknown
		// (Unknown types are serialising the whole subtree including this element)
		DOMElement * elem = NULL;
  	if(m_address->GetClassId() != Dc1Factory::GetTypeIndex(X("Dc1Unknown")))
		{
			elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), X("address"));
			const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));
			elem->setPrefix(prefix);
			
			element->appendChild(elem);
		}
		m_address->Serialize(doc, element, &elem);
	}
	 // String
	if(m_telephoneNumber != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_telephoneNumber, X("urn:ebu:metadata-schema:ebuCore_2014"), X("telephoneNumber"), true);
	 // String
	if(m_mobileTelephoneNumber != (XMLCh*)NULL) Dc1Util::SerializeTextNode(element, doc, m_mobileTelephoneNumber, X("urn:ebu:metadata-schema:ebuCore_2014"), X("mobileTelephoneNumber"), true);

}

bool EbuCoredetailsType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("emailAddress"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("emailAddress")) == 0))
		{
			// Deserialize factory type
			EbuCoredetailsType_emailAddress_CollectionPtr tmp = CreatedetailsType_emailAddress_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SetemailAddress(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("webAddress"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetwebAddress(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetwebAddress(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
		// Deserialize element
	if(Dc1Util::HasNodeName(parent, X("address"), X("urn:ebu:metadata-schema:ebuCore_2014")))
//		if((parent != NULL) && (XMLString::compareString(parent->getNodeName(), X("address")) == 0))
		{
			// We have to deal with xsi:type classes...
			// Deserialize factory type
			Dc1Ptr< Dc1Node > tmp;
			if(parent->hasAttribute(X("xsi:type")))
			{
			  NamespaceHelper result;
			  Dc1Util::GetXsiType(parent, doc, &result);
			  XMLCh * xsitype = Dc1Util::QualifiedName(&result);
			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(xsitype))));
//			  tmp = Dc1Ptr< Dc1Node >(Dc1Factory::CreateObject((Dc1Factory::GetTypeIndex(parent->getAttribute(X("xsi:type"))))));
			  if(xsitype) Dc1Util::deallocate(&xsitype);
			}else 
			{
			  tmp = CreateaddressType; // FTT, check this
			}
			tmp->Deserialize(doc, parent, current);
			
			if(parent->hasAttribute(X("xsi:type"))) {
					tmp->UseTypeAttribute = true;
			}
			this->Setaddress(tmp);
			found = true;
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("telephoneNumber"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SettelephoneNumber(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SettelephoneNumber(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
	// Deserialize string (XMLCh*) element
	if(Dc1Util::HasNodeName(parent, X("mobileTelephoneNumber"), X("urn:ebu:metadata-schema:ebuCore_2014")))
	{
		// FTT memleaking		this->SetmobileTelephoneNumber(Dc1Convert::TextToString(Dc1Util::GetElementText(parent)));
		this->SetmobileTelephoneNumber(Dc1Util::GetElementText(parent));
		found = true;
		* current = parent;
		parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
	}
// no includefile for extension defined 
// file EbuCoredetailsType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoredetailsType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("emailAddress")) == 0))
  {
	EbuCoredetailsType_emailAddress_CollectionPtr tmp = CreatedetailsType_emailAddress_CollectionType; // FTT, check this
	this->SetemailAddress(tmp);
	child = tmp;
  }
  if (XMLString::compareString(elementname, X("address")) == 0) {
	// We check for xsi:type
	if (xsitype != NULL && XMLString::stringLen(xsitype) > 0) {
	  child = Dc1NodePtr(Dc1Factory::CreateObject( Dc1Factory::GetTypeIndex(xsitype) ) );
	} else { // no xsi:type
	  child = CreateaddressType; // FTT, check this
	}
	this->Setaddress(child);
  }
  return child;
 
}

Dc1NodeEnum EbuCoredetailsType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GetemailAddress() != Dc1NodePtr())
		result.Insert(GetemailAddress());
	if (Getaddress() != Dc1NodePtr())
		result.Insert(Getaddress());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoredetailsType_ExtMethodImpl.h


