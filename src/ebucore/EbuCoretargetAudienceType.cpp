
// Extension library class
// ClassType_CPP.template

/******************************************************************************/
/* MP-AF library, created by JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.  */
/*                                                                            */
/* The copyright in this software is being made available under the BSD       */
/* License, included below. This software may be subject to other third party */
/* and contributor rights, including patent rights, and no such rights are    */
/* granted under this license.                                                */
/*                                                                            */
/* <OWNER> = DIGITAL-Institute for Information and Communication Technologies */
/* <ORGANIZATION> = JOANNEUM RESEARCH Forschungsgesellschaft m.b.H.           */
/* <YEAR> = 2015                                                              */
/* Copyright (c) <YEAR>, <OWNER>                                              */
/* All rights reserved.                                                       */
/* Redistribution and use in source and binary forms, with or without         */
/* modification, are permitted provided that the following conditions are met:*/
/*                                                                            */
/* Redistributions of source code must retain the above copyright notice, this*/
/* list of conditions and the following disclaimer.                           */
/* Redistributions in binary form must reproduce the above copyright notice,  */
/* this list of conditions and the following disclaimer in the documentation  */
/* and/or other materials provided with the distribution.                     */
/* Neither the name of the <ORGANIZATION> nor the names of its contributors   */
/* may be used to endorse or promote products derived from this software      */
/* without specific prior written permission.                                 */
/*                                                                            */
/* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS �AS IS�*/
/* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE  */
/* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE */
/* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE  */
/* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR        */
/* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF       */
/* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS   */
/* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN    */
/* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)    */
/* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGE.                                                */
/******************************************************************************/

 
// FTT TODO check whether this must be included in the include file of this class...
// For now, we leave it here
#include "EbuCoreFactoryDefines.h"
// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtImplInclude.h


#include "EbuCoretargetAudienceType_targetRegion_CollectionType.h"
#include "EbuCoretargetAudienceType_targetExclusionRegion_CollectionType.h"
#include "EbuCoretargetAudienceType.h"
#include "Dc1FactoryDefines.h"
#include "Dc1Convert.h"
#include "Dc1Util.h"
#include "Dc1ClientManager.h"
#include "Dc1XPathParseContext.h" // XPath stuff
#include "EbuCoreregionType.h" // Element collection urn:ebu:metadata-schema:ebuCore_2014:targetRegion

#include <assert.h>
IEbuCoretargetAudienceType::IEbuCoretargetAudienceType()
{

// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtPropInit.h

}

IEbuCoretargetAudienceType::~IEbuCoretargetAudienceType()
{
// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtPropCleanup.h

}

EbuCoretargetAudienceType::EbuCoretargetAudienceType()
{
	//Init();  // Must be called by factory -- base init depends on m_myself being set up
}

EbuCoretargetAudienceType::~EbuCoretargetAudienceType()
{
	Cleanup();
}

void EbuCoretargetAudienceType::Init()
{

	// Init attributes
	m_typeLabel = NULL; // String
	m_typeLabel_Exist = false;
	m_typeDefinition = NULL; // String
	m_typeDefinition_Exist = false;
	m_typeLink = NULL; // String
	m_typeLink_Exist = false;
	m_typeSource = NULL; // String
	m_typeSource_Exist = false;
	m_typeLanguage = NULL; // String
	m_typeLanguage_Exist = false;
	m_formatLabel = NULL; // String
	m_formatLabel_Exist = false;
	m_formatDefinition = NULL; // String
	m_formatDefinition_Exist = false;
	m_formatLink = NULL; // String
	m_formatLink_Exist = false;
	m_formatSource = NULL; // String
	m_formatSource_Exist = false;
	m_formatLanguage = NULL; // String
	m_formatLanguage_Exist = false;
	m_reason = NULL; // String
	m_reason_Exist = false;
	m_linkToLogo = NULL; // String
	m_linkToLogo_Exist = false;
	m_notRated = false; // Value
	m_notRated_Exist = false;
	m_adultContent = false; // Value
	m_adultContent_Exist = false;

	// Init elements (element, union sequence choice all any)
	
	m_targetRegion = EbuCoretargetAudienceType_targetRegion_CollectionPtr(); // Collection
	m_targetExclusionRegion = EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr(); // Collection


// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtMyPropInit.h

}

void EbuCoretargetAudienceType::Cleanup()
{
// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtMyPropCleanup.h


	XMLString::release(&m_typeLabel); // String
	XMLString::release(&m_typeDefinition); // String
	XMLString::release(&m_typeLink); // String
	XMLString::release(&m_typeSource); // String
	XMLString::release(&m_typeLanguage); // String
	XMLString::release(&m_formatLabel); // String
	XMLString::release(&m_formatDefinition); // String
	XMLString::release(&m_formatLink); // String
	XMLString::release(&m_formatSource); // String
	XMLString::release(&m_formatLanguage); // String
	XMLString::release(&m_reason); // String
	XMLString::release(&m_linkToLogo); // String
	// Dc1Factory::DeleteObject(m_targetRegion);
	// Dc1Factory::DeleteObject(m_targetExclusionRegion);
}

void EbuCoretargetAudienceType::DeepCopy(const Dc1NodePtr &original)
{
	// Don't use targetAudienceTypePtr, since we
	// might need GetBase(), which isn't defined in ItargetAudienceType
	const Dc1Ptr< EbuCoretargetAudienceType > tmp(original);
	if (tmp == NULL) return; // EXIT: passed argument of wrong type
	{
	XMLString::release(&m_typeLabel); // String
	if (tmp->ExisttypeLabel())
	{
		this->SettypeLabel(XMLString::replicate(tmp->GettypeLabel()));
	}
	else
	{
		InvalidatetypeLabel();
	}
	}
	{
	XMLString::release(&m_typeDefinition); // String
	if (tmp->ExisttypeDefinition())
	{
		this->SettypeDefinition(XMLString::replicate(tmp->GettypeDefinition()));
	}
	else
	{
		InvalidatetypeDefinition();
	}
	}
	{
	XMLString::release(&m_typeLink); // String
	if (tmp->ExisttypeLink())
	{
		this->SettypeLink(XMLString::replicate(tmp->GettypeLink()));
	}
	else
	{
		InvalidatetypeLink();
	}
	}
	{
	XMLString::release(&m_typeSource); // String
	if (tmp->ExisttypeSource())
	{
		this->SettypeSource(XMLString::replicate(tmp->GettypeSource()));
	}
	else
	{
		InvalidatetypeSource();
	}
	}
	{
	XMLString::release(&m_typeLanguage); // String
	if (tmp->ExisttypeLanguage())
	{
		this->SettypeLanguage(XMLString::replicate(tmp->GettypeLanguage()));
	}
	else
	{
		InvalidatetypeLanguage();
	}
	}
	{
	XMLString::release(&m_formatLabel); // String
	if (tmp->ExistformatLabel())
	{
		this->SetformatLabel(XMLString::replicate(tmp->GetformatLabel()));
	}
	else
	{
		InvalidateformatLabel();
	}
	}
	{
	XMLString::release(&m_formatDefinition); // String
	if (tmp->ExistformatDefinition())
	{
		this->SetformatDefinition(XMLString::replicate(tmp->GetformatDefinition()));
	}
	else
	{
		InvalidateformatDefinition();
	}
	}
	{
	XMLString::release(&m_formatLink); // String
	if (tmp->ExistformatLink())
	{
		this->SetformatLink(XMLString::replicate(tmp->GetformatLink()));
	}
	else
	{
		InvalidateformatLink();
	}
	}
	{
	XMLString::release(&m_formatSource); // String
	if (tmp->ExistformatSource())
	{
		this->SetformatSource(XMLString::replicate(tmp->GetformatSource()));
	}
	else
	{
		InvalidateformatSource();
	}
	}
	{
	XMLString::release(&m_formatLanguage); // String
	if (tmp->ExistformatLanguage())
	{
		this->SetformatLanguage(XMLString::replicate(tmp->GetformatLanguage()));
	}
	else
	{
		InvalidateformatLanguage();
	}
	}
	{
	XMLString::release(&m_reason); // String
	if (tmp->Existreason())
	{
		this->Setreason(XMLString::replicate(tmp->Getreason()));
	}
	else
	{
		Invalidatereason();
	}
	}
	{
	XMLString::release(&m_linkToLogo); // String
	if (tmp->ExistlinkToLogo())
	{
		this->SetlinkToLogo(XMLString::replicate(tmp->GetlinkToLogo()));
	}
	else
	{
		InvalidatelinkToLogo();
	}
	}
	{
	if (tmp->ExistnotRated())
	{
		this->SetnotRated(tmp->GetnotRated());
	}
	else
	{
		InvalidatenotRated();
	}
	}
	{
	if (tmp->ExistadultContent())
	{
		this->SetadultContent(tmp->GetadultContent());
	}
	else
	{
		InvalidateadultContent();
	}
	}
		// Dc1Factory::DeleteObject(m_targetRegion);
		this->SettargetRegion(Dc1Factory::CloneObject(tmp->GettargetRegion()));
		// Dc1Factory::DeleteObject(m_targetExclusionRegion);
		this->SettargetExclusionRegion(Dc1Factory::CloneObject(tmp->GettargetExclusionRegion()));
}

Dc1NodePtr EbuCoretargetAudienceType::GetBaseRoot()
{
	return m_myself.getPointer();
}

const Dc1NodePtr EbuCoretargetAudienceType::GetBaseRoot() const
{
	return m_myself.getPointer();
}


XMLCh * EbuCoretargetAudienceType::GettypeLabel() const
{
	return m_typeLabel;
}

bool EbuCoretargetAudienceType::ExisttypeLabel() const
{
	return m_typeLabel_Exist;
}
void EbuCoretargetAudienceType::SettypeLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettypeLabel().");
	}
	m_typeLabel = item;
	m_typeLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatetypeLabel()
{
	m_typeLabel_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GettypeDefinition() const
{
	return m_typeDefinition;
}

bool EbuCoretargetAudienceType::ExisttypeDefinition() const
{
	return m_typeDefinition_Exist;
}
void EbuCoretargetAudienceType::SettypeDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettypeDefinition().");
	}
	m_typeDefinition = item;
	m_typeDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatetypeDefinition()
{
	m_typeDefinition_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GettypeLink() const
{
	return m_typeLink;
}

bool EbuCoretargetAudienceType::ExisttypeLink() const
{
	return m_typeLink_Exist;
}
void EbuCoretargetAudienceType::SettypeLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettypeLink().");
	}
	m_typeLink = item;
	m_typeLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatetypeLink()
{
	m_typeLink_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GettypeSource() const
{
	return m_typeSource;
}

bool EbuCoretargetAudienceType::ExisttypeSource() const
{
	return m_typeSource_Exist;
}
void EbuCoretargetAudienceType::SettypeSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettypeSource().");
	}
	m_typeSource = item;
	m_typeSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatetypeSource()
{
	m_typeSource_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GettypeLanguage() const
{
	return m_typeLanguage;
}

bool EbuCoretargetAudienceType::ExisttypeLanguage() const
{
	return m_typeLanguage_Exist;
}
void EbuCoretargetAudienceType::SettypeLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettypeLanguage().");
	}
	m_typeLanguage = item;
	m_typeLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatetypeLanguage()
{
	m_typeLanguage_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetformatLabel() const
{
	return m_formatLabel;
}

bool EbuCoretargetAudienceType::ExistformatLabel() const
{
	return m_formatLabel_Exist;
}
void EbuCoretargetAudienceType::SetformatLabel(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetformatLabel().");
	}
	m_formatLabel = item;
	m_formatLabel_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateformatLabel()
{
	m_formatLabel_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetformatDefinition() const
{
	return m_formatDefinition;
}

bool EbuCoretargetAudienceType::ExistformatDefinition() const
{
	return m_formatDefinition_Exist;
}
void EbuCoretargetAudienceType::SetformatDefinition(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetformatDefinition().");
	}
	m_formatDefinition = item;
	m_formatDefinition_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateformatDefinition()
{
	m_formatDefinition_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetformatLink() const
{
	return m_formatLink;
}

bool EbuCoretargetAudienceType::ExistformatLink() const
{
	return m_formatLink_Exist;
}
void EbuCoretargetAudienceType::SetformatLink(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetformatLink().");
	}
	m_formatLink = item;
	m_formatLink_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateformatLink()
{
	m_formatLink_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetformatSource() const
{
	return m_formatSource;
}

bool EbuCoretargetAudienceType::ExistformatSource() const
{
	return m_formatSource_Exist;
}
void EbuCoretargetAudienceType::SetformatSource(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetformatSource().");
	}
	m_formatSource = item;
	m_formatSource_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateformatSource()
{
	m_formatSource_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetformatLanguage() const
{
	return m_formatLanguage;
}

bool EbuCoretargetAudienceType::ExistformatLanguage() const
{
	return m_formatLanguage_Exist;
}
void EbuCoretargetAudienceType::SetformatLanguage(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetformatLanguage().");
	}
	m_formatLanguage = item;
	m_formatLanguage_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateformatLanguage()
{
	m_formatLanguage_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::Getreason() const
{
	return m_reason;
}

bool EbuCoretargetAudienceType::Existreason() const
{
	return m_reason_Exist;
}
void EbuCoretargetAudienceType::Setreason(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::Setreason().");
	}
	m_reason = item;
	m_reason_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::Invalidatereason()
{
	m_reason_Exist = false;
}
XMLCh * EbuCoretargetAudienceType::GetlinkToLogo() const
{
	return m_linkToLogo;
}

bool EbuCoretargetAudienceType::ExistlinkToLogo() const
{
	return m_linkToLogo_Exist;
}
void EbuCoretargetAudienceType::SetlinkToLogo(XMLCh * item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetlinkToLogo().");
	}
	m_linkToLogo = item;
	m_linkToLogo_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatelinkToLogo()
{
	m_linkToLogo_Exist = false;
}
bool EbuCoretargetAudienceType::GetnotRated() const
{
	return m_notRated;
}

bool EbuCoretargetAudienceType::ExistnotRated() const
{
	return m_notRated_Exist;
}
void EbuCoretargetAudienceType::SetnotRated(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetnotRated().");
	}
	m_notRated = item;
	m_notRated_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidatenotRated()
{
	m_notRated_Exist = false;
}
bool EbuCoretargetAudienceType::GetadultContent() const
{
	return m_adultContent;
}

bool EbuCoretargetAudienceType::ExistadultContent() const
{
	return m_adultContent_Exist;
}
void EbuCoretargetAudienceType::SetadultContent(bool item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SetadultContent().");
	}
	m_adultContent = item;
	m_adultContent_Exist = true;
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::InvalidateadultContent()
{
	m_adultContent_Exist = false;
}
EbuCoretargetAudienceType_targetRegion_CollectionPtr EbuCoretargetAudienceType::GettargetRegion() const
{
		return m_targetRegion;
}

EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr EbuCoretargetAudienceType::GettargetExclusionRegion() const
{
		return m_targetExclusionRegion;
}

void EbuCoretargetAudienceType::SettargetRegion(const EbuCoretargetAudienceType_targetRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettargetRegion().");
	}
	if (m_targetRegion != item)
	{
		// Dc1Factory::DeleteObject(m_targetRegion);
		m_targetRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_targetRegion) m_targetRegion->SetParent(m_myself.getPointer());
		if(m_targetRegion != EbuCoretargetAudienceType_targetRegion_CollectionPtr())
		{
			m_targetRegion->SetContentName(XMLString::transcode("targetRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}

void EbuCoretargetAudienceType::SettargetExclusionRegion(const EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr & item, Dc1ClientID client)
{
	if (!IsLocked(client)) {
		throw Dc1Exception(DC1_ERROR, "Could not acquire lock in EbuCoretargetAudienceType::SettargetExclusionRegion().");
	}
	if (m_targetExclusionRegion != item)
	{
		// Dc1Factory::DeleteObject(m_targetExclusionRegion);
		m_targetExclusionRegion = item;
		// baw 27 04 2004: make sure that node on which parent is set is not null
		if (m_targetExclusionRegion) m_targetExclusionRegion->SetParent(m_myself.getPointer());
		if(m_targetExclusionRegion != EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr())
		{
			m_targetExclusionRegion->SetContentName(XMLString::transcode("targetExclusionRegion"));
		}
	}
	Dc1ClientManager::SendUpdateNotification(
		m_myself.getPointer(), DC1_NA_NODE_UPDATED,
		client, m_myself.getPointer());
}


Dc1NodeEnum EbuCoretargetAudienceType::GetOrCreateFromXPath(const XMLCh *xpath, Dc1ClientID client /* = DC1_UNDEFINED_CLIENT */, Dc1XPathCreation flag /* = DC1_FORCECREATE */, Dc1XPathParseContext * context /* = NULL */)
{

// ********* Number of Attributes:  14, 14 ************


	Dc1XPathParseContext c;
	bool isentrypoint = Dc1XPathParseContext::Initialise(c, &context, xpath, flag);
	
	Dc1NodeEnum result = Dc1Node::GetOrCreateFromXPath(xpath, client, flag, context);
	if(context->Found) return result; // Ready
	const Dc1NodePtr empty = Dc1NodePtr();
	// Attribute ------------------------------------------------------------------
	if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLabel")) == 0)
	{
		// typeLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeDefinition")) == 0)
	{
		// typeDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLink")) == 0)
	{
		// typeLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeSource")) == 0)
	{
		// typeSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("typeLanguage")) == 0)
	{
		// typeLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLabel")) == 0)
	{
		// formatLabel is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatDefinition")) == 0)
	{
		// formatDefinition is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLink")) == 0)
	{
		// formatLink is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatSource")) == 0)
	{
		// formatSource is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("formatLanguage")) == 0)
	{
		// formatLanguage is simple attribute language
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("reason")) == 0)
	{
		// reason is simple attribute string
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("linkToLogo")) == 0)
	{
		// linkToLogo is simple attribute anyURI
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "XMLCh *");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("notRated")) == 0)
	{
		// notRated is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Attribute ------------------------------------------------------------------
	else if(context->IsAttribute && XMLString::compareString(context->ElementName, X("adultContent")) == 0)
	{
		// adultContent is simple attribute boolean
		Dc1XPathParseContext::ErrorUnsupportedAttributeType(context, "bool");
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("targetRegion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:targetRegion is item of type regionType
		// in element collection targetAudienceType_targetRegion_CollectionType
		
		context->Found = true;
		EbuCoretargetAudienceType_targetRegion_CollectionPtr coll = GettargetRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetargetAudienceType_targetRegion_CollectionType; // FTT, check this
				SettargetRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))) != empty)
			{
				// Is type allowed
				EbuCoreregionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	// Element ------------------------------------------------------------------
	else if(XMLString::compareString(context->ElementName, X("targetExclusionRegion")) == 0)
	{
		// urn:ebu:metadata-schema:ebuCore_2014:targetExclusionRegion is item of type regionType
		// in element collection targetAudienceType_targetExclusionRegion_CollectionType
		
		context->Found = true;
		EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr coll = GettargetExclusionRegion();
		if(coll == empty) // Create one via factory
		{
			if(context->Create)
			{
				coll = CreatetargetAudienceType_targetExclusionRegion_CollectionType; // FTT, check this
				SettargetExclusionRegion(coll, client);
			}
			else
			{
				return result; // No collection, silently return empty enumeration
			}
		}
		if(!Dc1XPathParseContext::PreProcessCollection(context, coll->size(), -1))
		{
			return result;
		}
		
		int i = 0;
		Dc1NodePtr p;
		while(i < (int)coll->size())
		{
			if((p = coll->elementAt(i)) != empty)
			{
				i = Dc1XPathParseContext::HandleCollection(context, i, p, result);
			}
			else
			{
				 Dc1XPathParseContext::ErrorInvalidEmptyItem(context, i);
			}
		}
		
		if(Dc1XPathParseContext::PostProcessElementCollection(context, i, result))
		{
			if((p = Dc1XPathParseContext::CreateNode(context, Dc1Factory::GetTypeIndex(X("urn:ebu:metadata-schema:ebuCore_2014:regionType")))) != empty)
			{
				// Is type allowed
				EbuCoreregionPtr test = p;
				if(test || p->GetClassId() == 1) // Deliberate "Dc1Unkown" type is allowed
				{
					coll->insertElementAt(p, i, client); // Insert or append
					if((p = coll->elementAt(i)) != empty) // Maybe some other restrictions are waiting...
					{
						result.Insert(p);
					}
				}
				if(result.Size() == 0)
				{
					Dc1XPathParseContext::ErrorCannotSetElement(context);
				}
			}
		}
		if(result.Size() > 0 && XMLString::stringLen(context->UnparsedXPath) > 0)
		{
			Dc1NodeEnum subresult;
			while(result.HasNext())
			{
				subresult.Insert(result.GetNext()->GetOrCreateFromXPath(context->UnparsedXPath, client, flag, NULL));
			}
			return subresult;
		}		
		return result;
		
	}
	else if(isentrypoint)
	{
		// Sorry, unknown subelement name for targetAudienceType
		Dc1XPathParseContext::ErrorUnknownSubElement(context, "targetAudienceType");
	}
	return result;
}

XMLCh * EbuCoretargetAudienceType::ToText() const
{
	// for Emtpy, ElementOnly, Mixed and TextOnly containing attributes intentionally dummy implemented
	return XMLString::transcode(this->GetTypeName());
}


bool EbuCoretargetAudienceType::Parse(const XMLCh * const txt)
{
	return false;
}


void EbuCoretargetAudienceType::Serialize(DOMDocument * doc, DOMElement* parent, DOMElement** newElem)
{
	// The element we serialize in.
	DOMElement* element;

	// baw 10 02 2004: added newElem to return new node to caller from derived class
	DOMElement* localNewElem = NULL;
	if (!(newElem)) newElem = &localNewElem;
	// ---

	// Check if we should add the namespace prefix
	const XMLCh * prefix = Dc1Util::LookupNamespacePrefix(doc, X("urn:ebu:metadata-schema:ebuCore_2014"));

	// baw 06 02 2004: create elements only if no base is present 
	// baw 17 02 2004: or if the base has a type that does not create a named node
	// ftt 04 03 2011: or if the base would create a named node with the wrong namespace (base namespace instead of derived one)
	if (*newElem) 
	{
		// Element was passed in -> use it
		element = *newElem;
	} else if(parent == NULL) 
	{
		// Root node (presumably)
		element = doc->getDocumentElement();
	}
	else {
		// Create new element
		if (this->GetContentName() != (XMLCh*)NULL) {
			DOMElement * elem = doc->createElementNS(X("urn:ebu:metadata-schema:ebuCore_2014"), this->GetContentName());
			elem->setPrefix(prefix); // Might be NULL in case it is the default namespace and Archive::usePrefixForDefaultNS == false
			parent->appendChild(elem);
			element = elem;
		} else {
			// sat 2004-03-24: this should never happen
			assert(false);
		}
	}
	// baw 10 02 2004: added newElem to return new node to caller from derived class
	// sat 2004-03-18: set *newElem to parent unconditionally, not just in
	//   some branches (crashes if parent passed in, but no ContentName set)
	//   -- leaves out nodes, but at least doesn't crash.
	// baw 2004-04-19: set *newElem only if it is NULL, otherwise we would replace the value we get
	if (!(*newElem)) *newElem = element;


	Dc1Util::HandleXsiType(element, doc, this->UseTypeAttribute, X("urn:ebu:metadata-schema:ebuCore_2014"), X("targetAudienceType"));
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLabel_Exist)
	{
	// String
	if(m_typeLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLabel"), m_typeLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeDefinition_Exist)
	{
	// String
	if(m_typeDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeDefinition"), m_typeDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLink_Exist)
	{
	// String
	if(m_typeLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLink"), m_typeLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeSource_Exist)
	{
	// String
	if(m_typeSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeSource"), m_typeSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_typeLanguage_Exist)
	{
	// String
	if(m_typeLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("typeLanguage"), m_typeLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLabel_Exist)
	{
	// String
	if(m_formatLabel != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLabel"), m_formatLabel);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatDefinition_Exist)
	{
	// String
	if(m_formatDefinition != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatDefinition"), m_formatDefinition);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLink_Exist)
	{
	// String
	if(m_formatLink != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLink"), m_formatLink);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatSource_Exist)
	{
	// String
	if(m_formatSource != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatSource"), m_formatSource);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_formatLanguage_Exist)
	{
	// String
	if(m_formatLanguage != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("formatLanguage"), m_formatLanguage);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_reason_Exist)
	{
	// String
	if(m_reason != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("reason"), m_reason);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_linkToLogo_Exist)
	{
	// String
	if(m_linkToLogo != (XMLCh *)NULL)
	{
		element->setAttributeNS(X(""), X("linkToLogo"), m_linkToLogo);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_notRated_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_notRated);
		element->setAttributeNS(X(""), X("notRated"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Attribute Serialization:
		

	// Optional attriute
	if(m_adultContent_Exist)
	{
	// Value
	{
		XMLCh * tmp = Dc1Convert::BinToText(m_adultContent);
		element->setAttributeNS(X(""), X("adultContent"), tmp);
		XMLString::release(&tmp);
	}
		
	} // Optional attriute
	
	// Element serialization:
	if (m_targetRegion != EbuCoretargetAudienceType_targetRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_targetRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}
	if (m_targetExclusionRegion != EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr())
	{
		// Collection
		// No extra child node needed for choices or sequences with maxOccurs > 1
		m_targetExclusionRegion->Serialize(doc, element, NULL); // &element); - mio 041123
	}

}

bool EbuCoretargetAudienceType::Deserialize(DOMDocument * doc, DOMElement * parent, DOMElement ** current)
{
	if(!parent) return false;

	bool found = false;

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLabel")))
	{
		// Deserialize string type
		this->SettypeLabel(Dc1Convert::TextToString(parent->getAttribute(X("typeLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeDefinition")))
	{
		// Deserialize string type
		this->SettypeDefinition(Dc1Convert::TextToString(parent->getAttribute(X("typeDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLink")))
	{
		// Deserialize string type
		this->SettypeLink(Dc1Convert::TextToString(parent->getAttribute(X("typeLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeSource")))
	{
		// Deserialize string type
		this->SettypeSource(Dc1Convert::TextToString(parent->getAttribute(X("typeSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("typeLanguage")))
	{
		// Deserialize string type
		this->SettypeLanguage(Dc1Convert::TextToString(parent->getAttribute(X("typeLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLabel")))
	{
		// Deserialize string type
		this->SetformatLabel(Dc1Convert::TextToString(parent->getAttribute(X("formatLabel"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatDefinition")))
	{
		// Deserialize string type
		this->SetformatDefinition(Dc1Convert::TextToString(parent->getAttribute(X("formatDefinition"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLink")))
	{
		// Deserialize string type
		this->SetformatLink(Dc1Convert::TextToString(parent->getAttribute(X("formatLink"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatSource")))
	{
		// Deserialize string type
		this->SetformatSource(Dc1Convert::TextToString(parent->getAttribute(X("formatSource"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("formatLanguage")))
	{
		// Deserialize string type
		this->SetformatLanguage(Dc1Convert::TextToString(parent->getAttribute(X("formatLanguage"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("reason")))
	{
		// Deserialize string type
		this->Setreason(Dc1Convert::TextToString(parent->getAttribute(X("reason"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("linkToLogo")))
	{
		// Deserialize string type
		this->SetlinkToLogo(Dc1Convert::TextToString(parent->getAttribute(X("linkToLogo"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("notRated")))
	{
		// deserialize value type
		this->SetnotRated(Dc1Convert::TextToBool(parent->getAttribute(X("notRated"))));
		* current = parent;
	}

	// Deserialize attribute
	// For now, we silently assume attributeFormDefault = "unqualified"
	if(parent->hasAttribute(X("adultContent")))
	{
		// deserialize value type
		this->SetadultContent(Dc1Convert::TextToBool(parent->getAttribute(X("adultContent"))));
		* current = parent;
	}

	parent = Dc1Util::GetNextChild(parent);
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("targetRegion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("targetRegion")) == 0))
		{
			// Deserialize factory type
			EbuCoretargetAudienceType_targetRegion_CollectionPtr tmp = CreatetargetAudienceType_targetRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettargetRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
		
		// Element Collection
		if((parent != NULL) && 
		Dc1Util::HasNodeName(parent, X("targetExclusionRegion"), X("urn:ebu:metadata-schema:ebuCore_2014")))
// OLD		(XMLString::compareString(parent->getNodeName(), X("targetExclusionRegion")) == 0))
		{
			// Deserialize factory type
			EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr tmp = CreatetargetAudienceType_targetExclusionRegion_CollectionType; // FTT, check this
			tmp->Deserialize(doc, parent, current);
			this->SettargetExclusionRegion(tmp);
			found = true;
			parent = * current; // For collections
			* current = parent; // We must adjust it!
			parent = Dc1Util::GetNextAkin(parent); // Previously GetNextSibling
		}
// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtPostDeserialize.h

	return found;
}

Dc1NodePtr EbuCoretargetAudienceType::CreateChild(const XMLCh * const elementname, const XMLCh * const xsitype)
{
  assert(elementname);
  Dc1NodePtr child = Dc1NodePtr();
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("targetRegion")) == 0))
  {
	EbuCoretargetAudienceType_targetRegion_CollectionPtr tmp = CreatetargetAudienceType_targetRegion_CollectionType; // FTT, check this
	this->SettargetRegion(tmp);
	child = tmp;
  }
  // Non-Choice Collection
  if((XMLString::compareString(elementname, X("targetExclusionRegion")) == 0))
  {
	EbuCoretargetAudienceType_targetExclusionRegion_CollectionPtr tmp = CreatetargetAudienceType_targetExclusionRegion_CollectionType; // FTT, check this
	this->SettargetExclusionRegion(tmp);
	child = tmp;
  }
  return child;
 
}

Dc1NodeEnum EbuCoretargetAudienceType::GetAllChildElements() const
{
	Dc1NodeEnum result;
	if (GettargetRegion() != Dc1NodePtr())
		result.Insert(GettargetRegion());
	if (GettargetExclusionRegion() != Dc1NodePtr())
		result.Insert(GettargetExclusionRegion());
	// Note that this returns only child elements (no attributes)
	// that are Patterns, Elements or Collections!
	return result;
}

// no includefile for extension defined 
// file EbuCoretargetAudienceType_ExtMethodImpl.h


